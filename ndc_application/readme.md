# ND Counties (formerly CRMS)

Modern human resource, material and fleet management software for the web.

# License

Paradox One LLC CONFIDENTIAL
----------------------------
2012 - 2014 Paradox One LLC  
All Rights Reserved.

NOTICE: All information contained herein is, and remains  
the property of Paradox One LLC and its suppliers, if any.  
The intellectual and technical conepts contained herein  
are proprietary to Paradox One LLC and it suppliers and may  
be covered by U.S. and Foreign Patents, patents in process,  
and are protected by trade secret or copyright law. Dissemination  
of this information or reproduction of this material is strictly  
forbidden unless prior written permission is obtained from
Paradox One LLC.