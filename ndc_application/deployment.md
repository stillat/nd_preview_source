# Deployment

Previous versions of ND Counties utilized a customized dpl.oy application for deployment. All current versions should the `git-ftp` utility within a bash environment:

http://anantgarg.com/2013/09/25/git-ftp-for-windows/
https://github.com/git-ftp/git-ftp
https://github.com/git-ftp/git-ftp/man/

## Sample Configuration

The following is a sample from a working `.git/config` file:

~~~
[git-ftp]
    user = ***
    url = ndcounties.com/
    password = ***
~~~