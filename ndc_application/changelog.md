# Avalanche (NDC version 2.0 - 2014-04-08)

## Infrastructure Updates

* The services URL has changed from "https://www.ndcounties.com/" to "https://www.ndcounties.com/app/";
* The service website has been de-coupled from the service application and will remain available during service downtime;
* The core system has been re-written to provide a more stable, faster service;
* 

## User Experience Updates

* A new user interface has been designed and implemented;
* Introduction of a new "Roles" system;
* Top menu has been removed in favor of a new menu system on the left. This new menu system responds to the changing of roles;
* Item descriptions can now contain standard text formatting, such as bold, italic and underline
* 

## System Updates

* "Roads and Streets" have been removed in favor of the new "Properties" feature;
* Accounts can now have more than one user (additional fees may apply for multiple user access);
* Users can now belong to more than one service account (additional fees may apply for access to more accounts);

## Activities

* Activity "Expendetures" have been removed in favor of the new finance capabilities;
*

## Equipment Units

* Vehicle depreciation features have been temporarily removed for this version;
* Equipment units can now have "accessory" equipments defined, such as trailers or various plows;

## Work Logging

* The daily work screen has been redesigned to improve user work-flow;
* The "Road/Street" option has been removed in favor of the new "Properties" feature;
* 

## Road Types

* Road types have been removed in favor of the new "Properties" feature;

## Finance Updates

* Activity "Budgets" have been removed in favor of the new finance capabilities in the Avalanche update;
* Users can now create account categories (which allow for organization and color coding);
* Account transactions can now have one of three statuses: "Approved", "Pending" and "Canceled";
* Users can view account transaction histories from an individual accounts page;
* Account funds can be transfered between the accounts within the system. Cash accounts are required to bring money in and out of the system;