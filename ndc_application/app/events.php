<?phpuse Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;

Event::listen('auth.login', function ($user) {    $user->last_login = new DateTime;        $user->save();});Event::listen('auth.logout', function ($user) {    $user->save();});Event::listen('illuminate.query', function ($query, $bindings, $time, $name) {    if (App::environment() == 'local') {
        $data = compact('bindings', 'time', 'name');        // Format binding data for sql insertion        foreach ($bindings as $i => $binding) {
            if ($binding instanceof \DateTime) {
                $bindings[$i] = $binding->format('\'Y-m-d H:i:s\'');
            } elseif (is_string($binding)) {
                $bindings[$i] = "'$binding'";
            }
        }        // Insert bindings into query        $query = str_replace(array('%', '?'), array('%%', '%s'), $query);
        $query = vsprintf($query, $bindings);
        $databaseLog = new Logger('Database Log');
        $databaseLog->pushHandler(new StreamHandler(storage_path().'/logs/db.log', Logger::INFO));
        $databaseLog->addInfo($query, $data);

    }});
