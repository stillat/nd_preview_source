<?php

use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Input;
use ParadoxOne\NDCounties\UI\AvatarPresenter;

if (!function_exists('getUserStatusClass')) {
    function getUserStatusClass($user)
    {
        $online = 'online-background';

        if ($user->id == Auth::user()->id) {
            $online = 'is-user';
        } else {
            if ($user->last_seen !== null) {
                $lastSeenDate = new Carbon($user->last_seen);

                $currentDate = Carbon::now();
                $currentDate->subMinutes(10);

                if ($lastSeenDate->lt($currentDate)) {
                    Session::forget('userLastSeen');
                }

                if ($lastSeenDate->lt($currentDate)) {
                    $online = '';
                }
            } else {
                $online = 'inactive-background';
            }
        }

        return $online;
    }
}

Form::macro('errorMsg', function ($field) {
    $errors = Session::get('errors');

    if ($errors && $errors->has($field)) {
        $msg = $errors->first($field);

        return "<label for=\"$field\" class=\"error\">$msg</label>";
    }

    return '';
});

if (!function_exists('filters_used')) {
    function filters_used()
    {
        $filters = ['activity_filter_text', 'department_filter_text', 'district_filter_text', 'employee_filter_text', 'equipment_filter_text', 'material_filter_text', 'project_filter_text', 'property_filter_text', 'road_filter_text'];

        foreach ($filters as $filter) {
            if (strlen(Input::get($filter)) > 0) {
                return true;
            }
        }

        return false;
    }
}

HTML::macro('filterBlock', function ($filterText, $mode, $title) {
    $filterText = Input::get($filterText, '');
   if (strlen($filterText) > 0) {
       $includeMode = (Input::get($mode, 'inc') == 'inc') ? '' : ' (Excluded)';
       $count = substr_count($filterText, ', ');
       $count += 1;
       if ($count < 2) {
           $count = 1;
       }
       return '<dt style="float: left; margin-right: 5px;">'.str_plural($title, $count).$includeMode.'</dt><dd>'.$filterText.'</dd>';
   }
});

HTML::macro('sortSetting', function ($column, $name, $sortSetting) {
    $setting = '<li><div class="row"><div class="col-sm-1"><i class="glyphicon glyphicon-move cursor-pointer"></i></div><div class="col-sm-4"><label for="" class="control-label">';
    $setting .= e($name);
    $setting .= '</label></div><div class="col-sm-7"><div class="rdio rdio-success" style="display: inline; float: left;">';

    if ($sortSetting == 'asc') {
        $setting .= '<input type="radio" name="setting_'.$column.'" value="asc" id="asc'.$column.'" checked>';
    } else {
        $setting .= '<input type="radio" name="setting_'.$column.'" value="asc" id="asc'.$column.'">';
    }

    $setting .= '<label for="asc'.$column.'">Asc</label></div><div class="rdio rdio-primary" style="display: inline; float: left; margin-left: 15px">';

    if ($sortSetting == 'desc') {
        $setting .= '<input type="radio" name="setting_'.$column.'" value="desc" id="desc'.$column.'" checked>';
    } else {
        $setting .= '<input type="radio" name="setting_'.$column.'" value="desc" id="desc'.$column.'">';
    }

    $setting .= '<label for="desc'.$column.'">Desc</label></div><div class="rdio rdio-default" style="display: inline; float: left; margin-left: 15px">';

    if ($sortSetting == 'none') {
        $setting .= '<input type="radio" name="setting_'.$column.'" value="none" id="none'.$column.'" checked>';
    } else {
        $setting .= '<input type="radio" name="setting_'.$column.'" value="none" id="none'.$column.'">';
    }

    $setting .= '<label for="none'.$column.'">None</label></div></div></div></li>';

    return $setting;

});

HTML::macro('userStatus', function ($user) {
    return getUserStatusClass($user);
});

HTML::macro('addressBlock', function ($addressLine1, $addressLine2, $city, $state, $zip, $country) {
    $addressBlock = '';

    if (strlen($addressLine1) > 0) {
        $addressBlock .= '<br>'.e($addressLine1);
    }

    if (strlen($addressLine2) > 0) {
        $addressBlock .= '<br>'.e($addressLine2);
    }

    $addressBlock .= '<br>';

    if (strlen($city) > 0) {
        $addressBlock .= e($city).', ';
    }

    if (strlen($state) > 0) {
        $addressBlock .= e($state).' ';
    }

    if (strlen($zip) > 0) {
        $addressBlock .= e($zip);
    }

    if (strlen($country) > 0) {
        $addressBlock .= '<br>'.e($country);
    }

    return $addressBlock;
});


App::singleton('ParadoxOne\NDCounties\UI\AvatarPresenter', function () {
    return new AvatarPresenter();
});

HTML::macro('userCard', function ($user) {

    $presenter = App::make('ParadoxOne\NDCounties\UI\AvatarPresenter');

    $online = getUserStatusClass($user);

    return '<div class="people-item">
	<div class="media">
		<a href="' . route('admin.users.show', $user->id) . '" class="pull-left">
			<img alt="" src="'.$presenter->getAvatar($user->id, $user->email, 2) .'" class="thumbnail media-object ' . $online . '">
		</a>
		<div class="media-body">
			<a href="' . route('admin.users.show', $user->id) . '"><h4 class="person-name">' .
           htmlentities($user->first_name) . ' ' . htmlentities($user->last_name) . '</h4></a>
			<div class="text-muted"><i class="fa fa-envelope-o"></i> <a href="mailto:' . htmlentities($user->email) .
           '">' . htmlentities($user->email) . '</a></div>
			<ul class="social-list">
				<li><a href="' . htmlentities($user->email) . '" class="tooltips" data-toggle="tooltip" data-placement="top" title="" data-original-title="Email"><i class="fa fa-envelope-o"></i></a></li>
			</ul>
		</div>
	</div>
</div>';
});

HTML::macro('externalPanel', function ($title) {
    $stub = File::get(app_path() . '/ndcounties/macros/stubs/remote.stub');
    $stub = str_replace('{{ title }}', $title, $stub);

    return $stub;
});

HTML::macro('modelNaviateToolbar', function ($leftRoute, $leftData, $listRoute, $rightRoute, $rightData, $useCustom = false) {
    $returnBlock = '';

    if (!is_array($leftData)) {
        if ($leftData !== null) {
            $leftData = array($leftData);
        }
    }

    if (!is_array($rightData)) {
        if ($rightData !== null) {
            $rightData = array($rightData);
        }
    }

    if ($leftData == null) {
        $returnBlock .= '<a disabled="disabled" class="btn btn-default"><i class="fa fa-chevron-left inline-icon"></i></a>';
    } else {
        if (!$useCustom) {
            $returnBlock .= '<a href="' . route($leftRoute, $leftData) .
                            '" class="btn btn-white"><i class="fa fa-chevron-left inline-icon"></i></a>';
        } else {
            $returnBlock .= '<a href="' . $leftRoute .
                            '" class="btn btn-white"><i class="fa fa-chevron-left inline-icon"></i></a>';
        }
    }

    if ($listRoute !== null) {
        if (!$useCustom) {
            $returnBlock .= '<a href="' . route($listRoute) .
                            '" class="btn btn-white"><i class="fa fa-th-list inline-icon"></i></a>';
        } else {
            $returnBlock .= '<a href="' . $listRoute .
                            '" class="btn btn-white"><i class="fa fa-th-list inline-icon"></i></a>';
        }
    }

    if ($rightData == null) {
        $returnBlock .= '<a disabled="disabled" class="btn btn-default"><i class="fa fa-chevron-right inline-icon"></i></a>';
    } else {
        if (!$useCustom) {
            $returnBlock .= '<a href="' . route($rightRoute, $rightData) .
                            '" class="btn btn-white"><i class="fa fa-chevron-right inline-icon"></i></a>';
        } else {
            $returnBlock .= '<a href="' . $rightRoute .
                            '" class="btn btn-white"><i class="fa fa-chevron-right inline-icon"></i></a>';
        }
    }

    return $returnBlock;
});
