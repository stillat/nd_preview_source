<?php namespace App;

use Stillat\Database\Model as Model;

class AccountSettings extends Model
{
    protected $table = 'account_settings';

    public $timestamps = false;

    public $softDelete = false;
}
