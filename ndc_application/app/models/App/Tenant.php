<?php namespace App;

use Stillat\Database\Model as Model;

class Tenant extends Model
{
    protected $table = 'tenants';
}
