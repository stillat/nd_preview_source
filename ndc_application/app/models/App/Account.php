<?php namespace App;

use Stillat\Database\Model as StillatModel;

class Account extends StillatModel
{
    protected $table = 'accounts';

    public static $rules = array(
        'account_name' => 'required|unique:accounts,account_name',
    );

    public function contactInformation()
    {
        return $this->hasOne('\App\AccountSettings');
    }
}
