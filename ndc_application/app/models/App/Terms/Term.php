<?php

namespace App\Terms;

use Stillat\Database\Model as StillatModel;

class Term extends StillatModel
{
    protected $table = 'required_terms';

    public $timestamps = true;

    public $softDelete = true;

    protected $fillable = [
        'title', 'content'
    ];

    public static $rules = array(
        'title' => 'required',
        'content' => 'required|min:2',
    );
}
