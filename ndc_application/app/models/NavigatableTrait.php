<?php

trait NavigatableTrait
{
    protected $lastNextTo = -1;

    protected $lastPrevious = -1;

    public function next()
    {
        if ($this->lastNextTo == -1) {
            $this->lastNextTo = parent::next();
        }

        return $this->lastNextTo;
    }

    public function previous()
    {
        if ($this->lastPrevious == -1) {
            $this->lastPrevious = parent::previous();
        }

        return $this->lastPrevious;
    }
}
