<?php

use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableInterface;
use Cartalyst\Sentry\Users\Eloquent\User as CartalystUser;

class User extends CartalystUser implements UserInterface, RemindableInterface
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = array('password');

    /**
     * Get the unique identifier for the user.
     *
     * @return mixed
     */
    public function getAuthIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Get the password for the user.
     *
     * @return string
     */
    public function getAuthPassword()
    {
        return $this->password;
    }

    /**
     * Get the username for the user.
     *
     * @return string
     */
    public function getUsername()
    {
        return $this->getLogin();
    }

    /**
     * Get the e-mail address where password reminders are sent.
     *
     * @return string
     */
    public function getReminderEmail()
    {
        return $this->email;
    }


    public function getLastLogin($value)
    {
        return new Carbon($value);
    }

    /**
     * Returns the next record.
     *
     * @return Stillat\Common\Database\Model
     */
    public function next()
    {
        return static::where($this->getKeyName(), '>', $this->getKey())->min('id');
    }

    /**
     * Returns the previous record.
     *
     * @return Stillat\Common\Database\Model
     */
    public function previous()
    {
        return static::where($this->getKeyName(), '<', $this->getKey())->max('id');
    }

    public function tenants()
    {
        return $this->belongsToMany('App\Tenant', 'tenant_accounts', 'user_id', 'tenant_id');
    }

    public function settings()
    {
        return $this->hasOne('UserSetting');
    }

    public function getRememberToken()
    {
        return $this->remember_token;
    }

    public function setRememberToken($value)
    {
        $this->remember_token = $value;
    }

    public function getRememberTokenName()
    {
        return 'remember_token';
    }

    public function getProfileImage()
    {
    }
}
