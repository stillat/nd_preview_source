<?php

use Stillat\Database\Model as Model;

class UserSetting extends Model
{
    protected $table = 'user_settings';

    public $timestamps = false;
}
