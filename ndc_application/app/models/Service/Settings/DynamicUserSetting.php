<?php namespace Service\Settings;

use Stillat\Database\Model as Model;

class DynamicUserSetting extends Model
{
    protected $table = 'user_application_settings';

    public $timestamps = false;
}
