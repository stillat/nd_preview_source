<?php namespace Service;

use Stillat\Database\Tenant\Model as Model;

class Activity extends Model
{
    protected $table = 'activities';

    public $timestamps = true;

    public $softDelete = true;

    public static $rules = array(
        'code' => 'max:255|unique:activities,code',
        'name' => 'max:255|required|min:2',
        'description' => 'max:6553'
    );
}
