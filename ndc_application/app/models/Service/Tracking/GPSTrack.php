<?php namespace Service\Tracking;

use Stillat\Database\Tenant\Model as TenantModel;

class GPSTrack extends TenantModel
{
    protected $table = 'gps_entries';

    public $timestamps = false;

    public $softDelete = false;

    public static $rules = array(
        'latitude' => 'required|numeric',
        'longitude' => 'required|numeric',
        'altitude' => 'required|numeric',
        'date_recorded' => 'required|date',
    );
}
