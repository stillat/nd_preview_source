<?php namespace Service\Resources;

use Stillat\Database\Tenant\Model as TenantModel;

class Consumable extends TenantModel
{
    protected $table = 'consumables';

    public $timestamps = true;

    public $softDelete = true;

    public function scopeFuels($query)
    {
        return $query->where('consumable_context', '=', 1);
    }

    /**
     * Returns the next record.
     *
     * @return Stillat\Database\Model
     */
    public function nextMaterial()
    {
        return static::where('consumable_context', '=', 2)->where($this->getKeyName(), '>', $this->getKey())
                     ->min($this->getKeyName());
    }

    /**
     * Returns the previous record.
     *
     * @return Stillat\Database\Model
     */
    public function previousMaterial()
    {
        return static::where('consumable_context', '=', 2)->where($this->getKeyName(), '<', $this->getKey())
                     ->max($this->getKeyName());
    }

    /**
     * Returns the next record.
     *
     * @return Stillat\Database\Model
     */
    public function nextFuel()
    {
        return static::where('consumable_context', '=', 1)->where($this->getKeyName(), '>', $this->getKey())
                     ->min($this->getKeyName());
    }

    /**
     * Returns the previous record.
     *
     * @return Stillat\Database\Model
     */
    public function previousFuel()
    {
        return static::where('consumable_context', '=', 1)->where($this->getKeyName(), '<', $this->getKey())
                     ->max($this->getKeyName());
    }

    public static $rules = array(
        'description'              => 'max:6553',
        'name'                     => 'max:255|required|min:2',
        'code'                     => 'max:255|unique:consumables,code',
        'cost'                     => 'required|numeric',
        'unit_id'                  => 'integer',
        'initial_inventory_level'  => 'numeric',
        'desired_inventory_level'  => 'numeric',
        'critical_inventory_level' => 'numeric'
    );
}
