<?php namespace Service\Resources;

use Stillat\Database\Tenant\Model as TenantModel;

class Material extends TenantModel
{
    protected $table = 'materials';

    public $timestamps = true;

    public $softDelete = true;

    public static $rules = array(
        'code'        => 'max:255|unique:materials,code',
        'name'        => 'max:255|required|min:2',
        'description' => 'max:6553',

        'cost'        => 'required|numeric',
        'unit_id'     => 'integer'
    );
}
