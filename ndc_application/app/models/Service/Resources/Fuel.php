<?php namespace Service\Resources;

use Stillat\Database\Tenant\Model as TenantModel;

class Fuel extends TenantModel
{
    protected $table = 'fuels';

    public $timestamps = true;

    public $softDelete = true;

    public static $rules = array(
        'code'        => 'max:255|unique:fuels,code',
        'name'        => 'max:255|required|min:2',
        'description' => 'max:6553',

        'cost'        => 'required|numeric',
        'unit_id'     => 'integer'
    );
}
