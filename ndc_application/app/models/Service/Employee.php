<?php namespace Service;

use \Stillat\Database\Tenant\Model as Model;

class Employee extends Model
{
    protected $table = 'employees';

    protected $softDelete = true;

    public static $rules = array(
        'first_name' => 'max:255|required|min:2',
        'middle_name' => 'max:255|min:1',
        'last_name' => 'max:255|required|min:2',
        'code' => 'max:255|unique:employees,code',
        'gender' => 'numeric',
        'hire_date' => 'date',
        'termination_date' => 'date',

        'wage' => 'required|numeric',
        'overtime' => 'required|numeric',
        'wage_benefit' => 'required|numeric',
        'overtime_benefit' => 'required|numeric',

        'photo' => 'mimes:jpeg,jpg'
    );
}
