<?php namespace Service;

use Stillat\Database\Tenant\Model as Model;

class CountySettings extends Model
{
    protected $table = 'county_settings';

    public $timestamps = false;

    protected $fillable = ['*'];
}
