<?php namespace Service\Finances;

use Stillat\Database\Tenant\Model as Model;

class AccountCategory extends Model
{
    protected $table = 'fs_accounts_categories';

    public $timestamps = false;

    public $softDelete = false;
    
    public static $rules = array(
        'code' => 'max:255|unique:fs_accounts_categories,code',
        'name' => 'max:255|required|min:2|unique:fs_accounts_categories,name',
        'description' => 'max:6553|min:2',
        'color' => 'color'
    );
}
