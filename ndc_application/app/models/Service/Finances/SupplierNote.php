<?php namespace Service\Finances;

use Stillat\Database\Tenant\Model as Model;

class SupplierNote extends Model
{
    protected $table = 'fs_supplier_notes';

    public $timestamps = true;
}
