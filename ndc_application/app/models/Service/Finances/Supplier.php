<?php namespace Service\Finances;

use Stillat\Database\Tenant\Model as Model;

class Supplier extends Model
{
    protected $table = 'fs_suppliers';

    public $timestamps = true;

    protected $softDelete = true;

    public static $rules = array(
        'code' => 'unique:fs_suppliers,code',
        'name' => 'required|min:2',

        'photo' => 'mines:jpeg,jpg'
    );

    public function addresses()
    {
        return $this->morphMany('\Service\General\Address', 'addressable');
    }

    public function numbers()
    {
        return $this->morphMany('\Service\General\PhoneNumber', 'numberable');
    }

    public function notes()
    {
        return $this->hasMany('\Service\Finances\SupplierNote');
    }

    public function primaryAddress()
    {
        return $this->hasOne('Service\General\Address', 'id', 'primary_address_id');
    }

    public function primaryPhone()
    {
        return $this->hasOne('Service\General\PhoneNumber', 'id', 'primary_phone_id');
    }
}
