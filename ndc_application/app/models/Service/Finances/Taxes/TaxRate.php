<?php namespace Service\Finances\Taxes;

use Stillat\Database\Tenant\Model as Model;

class TaxRate extends Model
{
    protected $table = 'fs_tax_rates';

    public $timestamps = true;

    public $softDelete = true;

    public static $rules = array(
        'tax_name' => 'max:255|required|unique:fs_tax_rates,tax_name',
        'tax_rate' => 'required|numeric',
        'affects_all' => 'required',
        'affects_employees' => 'required',
        'affects_equipments' => 'required',
        'affects_fuels' => 'required',
        'affects_materials' => 'required',
    );
}
