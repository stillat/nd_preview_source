<?php namespace Service\Finances;

use Stillat\Database\Tenant\Model as Model;

class Account extends Model
{
    protected $table = 'fs_accounts';

    public $timestamps = true;

    public $softDelete = true;

    public static $rules = array(
        'code' => 'max:255|unique:fs_accounts,code',
        'name' => 'max:255|required|min:2',
        'description' => 'max:6553|min:2',
        'category' => 'exists:fs_accounts_categories,id'
    );

    public function category()
    {
        return $this->hasOne('\Service\Finances\AccountCategory', 'id', 'category_id');
    }
}
