<?php namespace Service\Finances;

use Stillat\Database\Tenant\Model as Model;

class Transaction extends Model
{
    protected $table = 'fs_transactions';

    public $timestamps = true;

    public $softDelete = true;

    public function beginAccount()
    {
        return $this->hasOne('\Service\Finances\Account', 'id', 'begin_account');
    }

    public function destination()
    {
        return $this->hasOne('\Service\Finances\Account', 'id', 'end_account');
    }
}
