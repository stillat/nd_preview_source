<?php namespace Service\Finances\Invoices;

use Stillat\Database\Tenant\Model as TenantModel;

class Invoice extends TenantModel
{
    protected $table = 'fs_invoices';

    protected $primaryKey = 'id';

    public $timestamps = true;

    public $softDelete = true;

    public function resetAndUpdate()
    {
        $this->discount = 0;
        $this->invoice_total = 0;
        $this->invoice_total_materials = 0;
        $this->invoice_total_equipment_units = 0;
        $this->invoice_total_employee_costs = 0;
        $this->invoice_total_fuels = 0;
        $this->invoice_tax_amount = 0;
        $this->invoice_sub_total = 0;
        $this->invoice_discount_total = 0;
        $this->update();
    }
}
