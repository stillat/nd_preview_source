<?php namespace Service\Finances\Invoices;

use Stillat\Database\Tenant\Model as Model;

class InvoiceWorkRecord extends Model
{
    protected $table = 'fs_invoice_work_records';

    public $timestamps = false;

    public $softDelete = false;
}
