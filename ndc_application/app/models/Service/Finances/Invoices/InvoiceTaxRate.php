<?php namespace Service\Finances\Invoices;

use Stillat\Database\Tenant\Model as Model;

class InvoiceTaxRate extends Model
{
    protected $table = 'fs_invoices_tax_rates';

    public $timestamps = false;

    public $softDelete = false;
}
