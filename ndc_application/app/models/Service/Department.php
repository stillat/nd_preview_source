<?php namespace Service;

use Stillat\Database\Tenant\Model as TenantModel;

class Department extends TenantModel
{
    protected $table = 'departments';

    public $timestamps = true;

    public $softDelete = true;

    public static $rules = array(
        'code' => 'max:255|unique:departments,code',
        'name' => 'max:255|required|min:2',
        'description' => 'max:6553',
    );
}
