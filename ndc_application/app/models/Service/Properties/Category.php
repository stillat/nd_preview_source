<?php namespace Service\Properties;

use Stillat\Database\Tenant\Model;

class Category extends Model
{
    protected $table = 'property_categories';

    public $timestamps = false;

    public $softDelete = false;

    public static $rules = [
        'category_name' => 'max:255|required|unique:property_categories,category_name'
    ];
}
