<?php namespace Service\Properties\Road;

use Stillat\Database\Tenant\Model as TenantModel;

class RoadAttributes extends TenantModel
{
    protected $table = 'property_road_properties';

    public $timestamps = false;

    public $softDelete = false;

    public function surfaceType()
    {
        return $this->hasOne('\Service\Properties\Road\SurfaceType', 'id', 'surface_type_id');
    }

    public static $rules = array(
        'road_id'         => 'exists:properties,id',
        'surface_type_id' => 'exists:property_road_surface_types,id',
        'unit_id'         => 'integer',
        'road_length'     => 'required|numeric',
    );
}
