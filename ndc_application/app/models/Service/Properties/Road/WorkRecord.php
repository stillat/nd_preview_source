<?php namespace Service\Properties\Road;

use Stillat\Database\Tenant\Model as TenantModel;

class WorkRecord extends TenantModel
{
    protected $table = 'road_work_record_logs';

    public $timestamps = false;

    public $softDelte = false;

    public static $rules = array(
        /* 'work_record_id' => 'required|exists:work_entries,id',*/
        'surface_type_id' => 'required|exists:property_road_surface_types,id',
        'road_length'     => 'required|numeric',
        'unit_id'         => 'required|numeric',
    );
}
