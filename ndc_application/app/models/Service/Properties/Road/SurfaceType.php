<?php namespace Service\Properties\Road;

use Stillat\Database\Tenant\Model as TenantModel;

class SurfaceType extends TenantModel
{
    protected $table = 'property_road_surface_types';

    public $timestamps = false;

    public $softDelete = false;

    public static $rules = array(
        'code'        => 'unique:property_road_surface_types,code',
        'name'        => 'required|min:2',
        'description' => 'max:6553',
    );
}
