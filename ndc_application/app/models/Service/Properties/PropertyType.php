<?php namespace Service\Properties;

use Stillat\Database\Tenant\Model as TenantModel;

class PropertyType extends TenantModel
{
    protected $table = 'property_types';

    public static $rules = array(
        'code'        => 'max:255|unique:property_types,code',
        'name'        => 'max:255|required|min:2',
        'description' => 'max:6553',
    );

    public function adapters()
    {
        return $this->hasMany('Service\Properties\PropertyTypeAdapter', 'property_id');
    }
}
