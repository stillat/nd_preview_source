<?php namespace Service\Properties;

use Stillat\Database\Tenant\Model as TenantModel;

class PropertyTypeAdapter extends TenantModel
{
    protected $table = 'property_adapters';
    
    public $timestamps = false;

    public $softDelete = false;

    public static $rules = array(
        'property_id' => 'exists:property_types,id'
    );
}
