<?php namespace Service\Properties;

use ParadoxOne\NDCounties\Database\DynamicRelationModel;
use Stillat\Database\Tenant\Model as TenantModel;

class Property extends DynamicRelationModel
{
    public static $rules = array(
        'code'          => 'max:255|unique:properties,code',
        'name'          => 'max:255|required|min:2',
        'description'   => 'max:6553',
        'category_id'   => 'exists:property_categories,id',
        'property_type' => 'required|integer|exists:property_types,id'
    );
    public $timestamps = true;

    public $softDelete = true;
    protected $table = 'properties';

    public function type()
    {
        return $this->hasOne('\Service\Properties\PropertyType', 'id', 'property_type');
    }

    public function category()
    {
        return $this->hasOne('\Service\Properties\Category', 'id', 'category_id');
    }

    public function children()
    {
        return $this->belongsToMany('\Service\Properties\Property', 'property_property', 'parent_property_id', 'child_property_id');
    }
}
