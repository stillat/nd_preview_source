<?php namespace Service\Properties;

use Stillat\Database\Tenant\Model as TenantModel;

class EquipmentUnit extends TenantModel
{
    protected $table = 'equipment_units';

    public $timestamps = true;

    public $softDelete = true;

    public static $rules = array(
        'code'            => 'max:255|unique:equipment_units,code',
        'name'            => 'max:255|required|min:2',
        'description'     => 'max:6553',
        'year'            => 'integer',
        'make'            => 'max:255|min:2',
        'model'           => 'max:255|min:2',
        'accessory_count' => 'integer',
        'serial_number'   => 'max:255|min:2',
        'rental_rate'     => 'required|numeric',
        'unit_id'         => 'integer',
        'purchase_cost'   => 'required|numeric',
        'salvage_value'   => 'required|numeric',
        'lifetime'        => 'numeric'
    );
}
