<?php namespace Service\Projects;

use Stillat\Database\Tenant\Model as TenantModel;

class Project extends TenantModel
{
    protected $table = 'projects';

    public $timestamps = true;

    public $softDelete = true;

    public static $rules = array(
        'code' => 'max:255|unique:projects,code',
        'name' => 'max:255|required|min:2',
        'description' => 'max:6553'
        );
}
