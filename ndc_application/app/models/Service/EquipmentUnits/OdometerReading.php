<?php namespace Service\EquipmentUnits;

use Stillat\Database\Tenant\Model as Model;

class OdometerReading extends Model
{
    protected $table = 'equipment_odometer_readings';

    public $timestamps = false;

    public $softDelete = false;

    public static $rules = array(
        'odometer_reading' => 'required|numeric',
    );
}
