<?php namespace Service\EquipmentUnits;

use Stillat\Database\Tenant\Model as Model;

class Unit extends Model
{
    protected $table = 'equipment_units';

    public $timestamps = true;

    public static $rules = array(
        'year' => 'numeric',
        'make' => 'max:255|min:2',
        'model' => 'max:255|min:2',
        'serial_number' => 'max:255|min:2',
        'rental_rate' => 'required|numeric',
        'unit_id' => 'required|numeric',
        'purchase_cost' => 'required|numeric',
        'salvage_value' => 'required|numeric',
        'lifetime' => 'numeric'
    );
}
