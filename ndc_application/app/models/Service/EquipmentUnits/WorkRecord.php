<?php namespace Service\EquipmentUnits;

use Stillat\Database\Tenant\Model as Model;

class WorkRecord extends Model
{
    protected $table = 'equipment_work_record_repairs';

    public $timestamps = false;

    public $softDelete = false;

    public static $rules = array(
        'work_record_id'         => 'required|numeric',
        'shop_costs'             => 'required|numeric',
        'purchased_costs'        => 'required|numeric',
        'commercial_labor_costs' => 'required|numeric',
        'work_description'       => 'max:6553',
    );
}
