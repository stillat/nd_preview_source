<?php namespace Service\General;

use Stillat\Database\Tenant\Model as Model;

class Address extends Model
{
    protected $table = 'addresses';

    public function addressable()
    {
        return $this->morphTo();
    }
}
