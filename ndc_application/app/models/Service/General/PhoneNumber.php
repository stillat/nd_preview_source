<?php namespace Service\General;

use Stillat\Database\Tenant\Model as Model;

class PhoneNumber extends Model
{
    protected $table = 'phone_numbers';

    public function numberable()
    {
        return $this->morphMany();
    }
}
