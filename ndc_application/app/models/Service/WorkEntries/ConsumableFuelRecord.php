<?php namespace Service\WorkEntries;

use Service\WorkEntries\ConsumableRecord;

class ConsumableFuelRecord extends ConsumableRecord
{
    public function info()
    {
        return $this->hasOne('\Service\Resources\Fuel', 'id', 'consumable_id');
    }
}
