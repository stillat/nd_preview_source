<?php namespace Service\WorkEntries;

use Stillat\Database\Tenant\Model as Model;

class ConsumableRecord extends Model
{
    protected $table = 'work_consumables';

    public $timestamps = true;

    public $softDelete = true;

    public static $rules = array(
        'consumable_type'     => 'required|max:255',
        'consumable_id'       => 'required|numeric',
        'associated_type'     => 'required|max:255',
        'associated_id'       => 'required|numeric',
        'work_entry_id'       => 'required|exists:work_entries,id',
        'measurement_unit_id' => 'required|numeric',
        'total_cost'          => 'required|numeric',
        'total_quantity'      => 'required|numeric',
        'historic_cost'       => 'required|numeric'
    );


}
