<?php namespace Service\WorkEntries;

use Service\WorkEntries\ConsumableRecord;

class ConsumableEquipmentRecord extends ConsumableRecord
{
    public function info()
    {
        return $this->hasOne('\Service\EquipmentUnits\Unit', 'id', 'consumable_id');
    }

}
