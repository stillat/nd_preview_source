<?php namespace Service\WorkEntries;

use ParadoxOne\NDCounties\Database\DynamicRelationModel;
use Stillat\Database\Tenant\Model as Model;

class WorkEntry extends DynamicRelationModel
{
    use \ParadoxOne\NDCounties\Traits\ValidatableTrait;

    public static $rules = array(
        'work_date'                         => 'required|date',
        'property_context'                  => 'required|numeric',
        'employee_id'                       => 'required|exists:employees,id',
        'activity_id'                       => 'required|exists:activities,id',
        'project_id'                        => 'required|exists:projects,id',
        'department_id'                     => 'required|exists:departments,id',
        'district_id'                       => 'required|exists:districts,id',
        'property_id'                       => 'required|numeric',
        'activity_description'              => 'max:6553',
        'employee_hours_regular_worked'     => 'required',
        'employee_hours_overtime_worked'    => 'required',

        'employee_historic_wage_regular'    => 'required',
        'employee_historic_wage_overtime'   => 'required',
        'employee_historic_fringe_regular'  => 'required',
        'employee_historic_fringe_overtime' => 'required',

        'misc_billing_quantity'             => 'required|numeric',
        'misc_billing_rate'                 => 'required|numeric',
        'misc_unit_id'                      => 'required|numeric',
        'misc_description'                  => 'max:6553',

        'total_materials_quantity'          => 'required',
        'total_equipment_units_quantity'    => 'required',
        'total_fuels_quantity'              => 'required',

        'total_materials_cost'              => 'required',
        'total_equipment_units_cost'        => 'required',
        'total_fuels_cost'                  => 'required',
        'total_equipment_and_fuels_cost'    => 'required',

        'total_misc_billing_cost'           => 'required',

        'total_employee_regular_cost'       => 'required',
        'total_employee_overtime_cost'      => 'required',
        'total_employee_combined_cost'      => 'required',
    );
    public $timestamps = true;

    public $softDelete = true;
    protected $table = 'work_entries';

    public function getValidationRules()
    {
        return self::$rules;
    }

    public function getValidationData()
    {
        return $this->getAttributes();
    }

    public function consumables()
    {
        return $this->hasMany('\Service\WorkEntries\ConsumableRecord', 'work_entry_id', 'id');
    }

    public function project()
    {
        return $this->hasOne('\Service\Projects\Project', 'id', 'project_id');
    }

    public function employee()
    {
        return $this->hasOne('\Service\Employee', 'id', 'employee_id');
    }

    public function activity()
    {
        return $this->hasOne('\Service\Activity', 'id', 'activity_id');
    }

    public function department()
    {
        return $this->hasOne('\Service\Department', 'id', 'department_id');
    }

    public function property()
    {
        if ($this->property_context == 0) {
            return $this->hasOne('\Service\Properties\Property', 'id', 'property_id');
        }

        return $this->hasOne('\Service\EquipmentUnits\Unit', 'id', 'property_id');
    }

    public function materials()
    {
        return $this->hasMany('\Service\WorkEntries\ConsumableMaterialRecord', 'work_entry_id', 'id')->where('consumable_type', '=', '4');
    }

    public function fuels()
    {
        return $this->hasMany('\Service\WorkEntries\ConsumableFuelRecord', 'work_entry_id', 'id')->where('consumable_type', '=', '3');
    }

    public function equipmentunits()
    {
        return $this->hasMany('\Service\WorkEntries\ConsumableEquipmentRecord', 'work_entry_id', 'id')->where('consumable_type', '=', '2');
    }
}
