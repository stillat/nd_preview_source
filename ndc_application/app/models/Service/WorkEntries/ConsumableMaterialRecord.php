<?php namespace Service\WorkEntries;

use Service\WorkEntries\ConsumableRecord;

class ConsumableMaterialRecord extends ConsumableRecord
{
    public function info()
    {
        return $this->hasOne('\Service\Resources\Material', 'id', 'consumable_id');
    }
}
