<?php namespace Service\InventoryManagement;

use Stillat\Database\Tenant\Model as Model;

class Adjustment extends Model
{
    protected $table = 'inventory_adjustments';

    public $timestamps = false;

    public $softDelete = false;

    public static $rules = array(
        'consumable_id'      => 'required|exists:consumables,id',
        'adjusted_by'        => 'required|numeric',
        'date_adjusted'      => 'required',
        'adjustment_context' => 'required|numeric',
        'adjustment_value'   => 'required|numeric',
        'adjustment_amount'  => 'required|numeric'
    );
}
