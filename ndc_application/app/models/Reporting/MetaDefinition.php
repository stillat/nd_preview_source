<?php namespace Reporting;

use Illuminate\Encryption\DecryptException;
use Illuminate\Support\Facades\Crypt;
use ParadoxOne\NDCounties\Contracts\Reporting\ReportDefinitionInterface;
use ParadoxOne\NDCounties\Reporting\Services\ReportExtensionService;
use Stillat\Database\Model as StillatModel;

class MetaDefinition extends StillatModel implements ReportDefinitionInterface
{
    use \NavigatableTrait;

    protected $table = 'system_reports';

    public $timestamps = false;

    public $baseReport = null;

    public static $rules = array(
        'report_name' => 'required|unique:system_reports',
        'report_description' => 'required',
        'report_uid' => 'required|unique:system_reports'
    );

    public function getHeader()
    {
        return $this->srl_header;
    }

    public function getBody()
    {
        return $this->srl_body;
    }

    public function getFooter()
    {
        return $this->srl_footer;
    }

    public function getSettings()
    {
        return $this->srl_settings;
    }

    public function getBaseReport()
    {
        return $this->baseReport;
    }

    public function isExtension()
    {
        return $this->getBaseReport() != null;
    }


    public function getAccountID()
    {
        return $this->account_id;
    }

    public function getIcon()
    {
        if (strlen($this->report_icon) == 0) {
            return 'glyphicon glyphicon-stats';
        }

        return $this->report_icon;
    }

    public function getReportID()
    {
        return $this->id;
    }


    public function getDescription()
    {
        return $this->report_description;
    }

    public function getName()
    {
        return $this->report_name;
    }

    /**
     * Indicates if the report is limited.
     *
     * @return bool
     */
    public function isLimited()
    {
        $settings = json_decode($this->getSettings());

        if (property_exists($settings, 'preparation')) {
            if (count($settings->preparation) > 0) {
                return true;
            }
        }

        return false;
    }

    /**
     * Gets encoded versions of settings.
     */
    public function getEncodedSettings()
    {
        if (strlen($this->getSettings()) == 0) {
            return '';
        }

        $settingsToEncode = json_decode($this->getSettings());

        if (property_exists($settingsToEncode, 'preparation')) {
            unset($settingsToEncode->preparation);
        }

        $settingsToEncode = json_encode($settingsToEncode);

        return base64_encode($settingsToEncode);
    }

    /**
     * Returns property type limits, formatted for UI presentation.
     */
    public function getPropertyTypeLimits()
    {
        $settings = json_decode($this->getSettings());

        if (property_exists($settings, 'property_type_limits')) {
            return base64_encode(json_encode($settings->property_type_limits));
        }

        return '';
        lk($settings);
    }


    /**
     * Factory method that will create a new model instance, applying extension techniques.
     *
     * @param int   $primaryDefinition
     * @param array $definitions
     *
     * @return MetaDefinition
     */
    public static function buildFromExtension($primaryDefinition, $definitions)
    {
        $extensionService = new ReportExtensionService;

        $primary = array_pluck_where($definitions, 'id', $primaryDefinition)[0];
        $secondary = null;

        foreach ($definitions as $def) {
            if ($def->id != $primaryDefinition) {
                $secondary = $def;
                break;
            }
        }

        return $extensionService->extend($primary, $secondary);
    }

    public function getNotificationMessage()
    {
        $settings = json_decode($this->getSettings());

        if (property_exists($settings, 'alert_message')) {
            return $settings->alert_message;
        }

        return '';
    }


    /**
     * Factory method that will return an Eloquent model from a stdClass instance.
     *
     * @param $stdClass
     *
     * @return ModelDefinition
     */
    public static function mapFromStandardClass($stdClass)
    {
        $model = new MetaDefinition;
        $model->unguard();
        $model->exists = true;
        $model->fill((array)$stdClass);

        return $model;
    }
}
