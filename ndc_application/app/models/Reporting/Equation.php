<?php namespace Reporting;

use Stillat\Database\Model as StillatModel;

class Equation extends StillatModel
{
    use \NavigatableTrait;

    protected $table = 'system_reports_default_totals';

    public $timestamps = false;

    public static $rules = array(
        'total_setting_name' => 'required',
        'formula' => 'required',
        'account_id' => 'required'
    );
}
