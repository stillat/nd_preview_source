<?php namespace Reporting;

use Stillat\Database\Model as StillatModel;

class TotalBlock extends StillatModel
{
    use \NavigatableTrait;

    protected $table = 'system_reports_total_blocks';

    public $timestamps = false;

    public static $rules = array(
        'name' => 'required',
        'description' => 'required|max:6553',
        'value' => 'required'
    );
}
