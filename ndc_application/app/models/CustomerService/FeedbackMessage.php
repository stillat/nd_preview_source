<?php namespace CustomerService;

use Stillat\Database\Model as StillatModel;

class FeedbackMessage extends StillatModel
{
    protected $table = 'feedback_messages';

    public $timestamps = true;

    public $softDelete = true;

    public static $rules = array(
        'subject' => 'required|min:2',
        'category_id' => 'integer',
        'message' => 'required|max:6553',
        'user_id' => 'required|exists:users,id'
    );

    public function author()
    {
        return $this->hasOne('\User', 'id', 'user_id');
    }
}
