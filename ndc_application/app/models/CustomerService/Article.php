<?php namespace CustomerService;

use Stillat\Database\Model as StillatModel;

class Article extends StillatModel
{
    protected $table = 'articles';

    public $timestamps = true;

    public $softdeletes = true;

    public static $rules = array(
        'title' => 'required',
        'keywords' => 'required',
        'description' => 'required',
        'slug' => 'required|unique:articles,slug',
        'content_markdown' => 'required',
        'content_markdown' => 'required'
    );
}
