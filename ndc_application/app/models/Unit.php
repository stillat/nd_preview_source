<?php

use Stillat\Database\Model as StillatModel;

class Unit extends StillatModel
{
    protected $table = 'units';

    public $timestamps = false;

    public static $rules = array(
        'code' => 'unique:units,code',
        'name' => 'required|min:2',
        'description' => 'max:6553'
    );
}
