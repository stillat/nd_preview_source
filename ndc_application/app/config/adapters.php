<?php

return array(

    /*
    |--------------------------------------------------------------------------
    | Work Log Adapaters
    |--------------------------------------------------------------------------
    |
    | The work log adapters that are available in the system should be listed
    | here. Just add the full qualified name of the adapter to 'adapters' array.
    |
    */

    'adapters' => array(
        'ParadoxOne\NDCounties\Database\Adapters\Property\RoadTypeAdapter',
        'ParadoxOne\NDCounties\Database\Adapters\Property\EquipmentRepairAdapter',
        'ParadoxOne\NDCounties\Database\Adapters\Property\EquipmentUnitAdapter',
        //'ParadoxOne\NDCounties\Database\Adapters\Property\SignTypeAdapter',
        //'ParadoxOne\NDCounties\Database\Adapters\Property\WellTypeAdapter',
        //'ParadoxOne\NDCounties\Database\Adapters\GPS\GPSAdapter',
    ),

);
