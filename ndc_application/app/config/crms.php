<?php

return array(

    'name' => 'ND Counties Genesis',
    'version' => '2.8.512-dev',
    'versionMajor' => 2,
    'versionMinor' => 8,
    'versionBuild' => 512,

    'learningCenter' => 'https://www.ndcounties.com/learn',

    'requireAdminTwoFactor' => false,

    'maintenanceMessage' => 'update'

);
