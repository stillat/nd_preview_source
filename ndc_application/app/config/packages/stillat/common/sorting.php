<?php

return array(

    /*
    |--------------------------------------------------------------------------
    | Default Sorting Driver
    |--------------------------------------------------------------------------
    |
    | This option controls the sorting driver that will be utilized by the
    | sorting manager.
    |
    | Supported: "bogo", "bubble", "cocktail", "gnome", "heap", "insertion",
    |			 "merge", "native", "quick", "selection", "shell"
    |
    */

    'driver' => 'native',

);
