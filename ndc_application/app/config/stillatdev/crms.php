<?php

return array(

    'learningCenter'          => 'https://www.ndcounties.dev/learn',

    'requireAdminTwoFactor'   => false,

    /** DB COMMANDS */
    'db_gzip_dumps'           => true,
    'db_dump_command_path'    => 'C:/Program Files/MySQL/MySQL Server 5.6/bin/',
    'db_gzip_command_path'    => 'C:/Users/John/AppData/Local/GitHub/PortableGit_ed44d00daa128db527396557813e7b68709ed0e2/bin/',
    'db_restore_command_path' => 'C:/Program Files/MySQL/MySQL Server 5.6/bin/',
    'db_openssl_command_path' => 'openssl',

);
