<?php

return array(

    'learningCenter'                  => 'http://www.ndcounties.dev/learn',

    'requireAdminTwoFactor'           => false,

    /** DB COMMANDS */
    'db_gzip_dumps'                   => true,
    'db_dump_command_path'            => 'C:/Program Files/MySQL/MySQL Server 5.6/bin/',
    'db_gzip_command_path'            => 'C:/Users/John/Downloads/cmder/vendor/msysgit/bin/',
    'db_restore_command_path'         => '',
    'db_openssl_command_path'         => 'C:/Users/John/AppData/Local/GitHub/PortableGit_7eaa994416ae7b397b2628033ac45f8ff6ac2010/bin/openssl.exe',

    'db_backup_create_limit_monthly'  => 4,
    'db_backup_restore_limit_monthly' => 4,
    'db_backup_upload_limit_monthly'  => 2

);
