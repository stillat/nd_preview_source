<?php

use Illuminate\Support\Str;
use Illuminate\Support\Facades\Auth;

if (!function_exists('show_property_breakdown')) {
    function show_property_breakdown(&$adapterManagerInstance, $data) {
        $workData = $data->getProperty()->work_data;

        if (count($workData) == 0) {
            return false;
        }

        $resultsFromManager = [];

        foreach ($workData as $adapter => $data) {
            $resultsFromManager[] = $adapterManagerInstance->shouldShowOnReports($adapter, $data);
        }

        if (in_array(true, $resultsFromManager)) {
            return true;
        }

        return false;
    }
}

if (!function_exists('search_cache_key')) {
    function search_cache_key($key)
    {
        return Auth::user()->id.'us'.Auth::user()->last_tenant.$key;
    }
}

if (!function_exists('array_strip_values')) {
    function array_strip_values($array, $valuesToRemove) {
        if (!is_array($valuesToRemove)) {
            $valuesToRemove = (array) $valuesToRemove;
        }
        return array_diff($array, $valuesToRemove);
    }
}

if (!function_exists('branding_get_inline_logo')) {
    function branding_get_inline_logo()
    {
        return '<img src="'.app_url().'/assets/img/cloud.png" alt="" style="width: 40px;">';
    }
}

if (!function_exists('make_restrictions_string')) {
    function make_restrictions_string($ids)
    {
        $return = '';

        foreach ($ids as $id) {
            $return .= '&restrictions[]='.$id;
        }

        return $return;
    }
}

if (!function_exists('export_csv')) {
    function export_csv($data, $mode = 'list', $extras = [])
    {
        return action('App\DataTools\DataExportController@getRunExport').'?'.http_build_query(array_merge(['export_data' => $data, 'export_type' => $mode], $extras));
    }
}

if (!function_exists('database_backups_path')) {
    function database_backups_path()
    {
        $path = storage_path().'/db_backups/';


        if (!file_exists($path)) {
            @mkdir($path);
        }
        return $path;
    }
}

if (!function_exists('database_backups_processing_path')) {
    function database_backups_processing_path()
    {
        $path = storage_path().'/db_backups_processing/';


        if (!file_exists($path)) {
            @mkdir($path);
        }
        return $path;
    }
}

if (!function_exists('user_exports_path')) {
    function user_exports_path($account)
    {
        $path = storage_path().'/user_exports/'.md5($account).'/';

        if (!file_exists($path)) {
            @mkdir($path);
        }

        return $path;
    }
}

if (!function_exists('limit_description')) {
    function limit_description($desc)
    {
        return Str::limit($desc, 30);
    }
}

if (!function_exists('limit_name')) {
    function limit_name($name)
    {
        return Str::limit($name, 25);
    }
}

if (!function_exists('limit_code')) {
    function limit_code($code)
    {
        return Str::limit($code, 20);
    }
}

if (!function_exists('git_ignore')) {
    /**
     * Writes a `.gitignore` file at the given path.
     * @param $path
     */
    function git_ignore($path)
    {
        file_put_contents($path, '*'.PHP_EOL.'!.gitignore');
    }
}

if (!function_exists('inventory_status')) {
    function inventory_status($current_level, $desired_level, $critical_level)
    {
        if ($current_level >= $desired_level) {
            return '<span class="label label-success">Desirable</span>';
        } elseif ($current_level < $desired_level && $current_level > $critical_level) {
            return '<span class="label label-warning" style="color: black;">Undesirable</span>';
        } else {
            return '<span class="label label-danger">Below Critical</span>';
        }
    }
}


if (!function_exists('get_update_data')) {
    /**
     * Helper function that retrieves data for an update view.
     *
     * @param $numericAdapterIdentifier
     * @param $workDataArray
     * @param $property
     * @param $default
     * @return mixed
     */
    function get_update_data($numericAdapterIdentifier, $workDataArray, $property, $default)
    {
        $object = get_data_for_update($numericAdapterIdentifier, $workDataArray);
        $value =  search_object_graph($object, $property);

        if ($value == null) {
            return $default;
        }

        return $value;
    }
}

if (!function_exists('get_data_for_update')) {
    function get_data_for_update($numericAdapterIdentifier, $workDataArray)
    {
        if ($numericAdapterIdentifier == -1) {
            $numericAdapterIdentifier = 0;
        }

        if (isset($workDataArray[$numericAdapterIdentifier])) {
            return $workDataArray[$numericAdapterIdentifier];
        }

        return null;
    }
}

if (!function_exists('get_property')) {
    function get_property(&$object, $property, $default)
    {
        if ($object !== null) {
            return $object->{$property};
        }

        return $default;
    }
}

if (!function_exists('search_object_graph')) {
    function search_object_graph($object, $path)
    {
        $graphNodes = null;

        if (is_array($path)) {
            $graphNodes = $path;
        } else {
            $graphNodes = explode('\\', $path);
        }

        foreach ($graphNodes as $node) {
            if (isset($object->{$node})) {
                if (end($graphNodes) == $node) {
                    return $object->{$node};
                }

                return search_object_graph($object->{$node}, $graphNodes);
            }
        }
    }
}

if (!function_exists('array_pluck_unique')) {
    /**
     * Pluck an array of values from an array.
     *
     * @param  array  $array
     * @param  string $value
     * @param  string $key
     * @return array
     */
    function array_pluck_unique($array, $value, $key = null)
    {
        $results = array();

        foreach ($array as $item) {
            $itemValue = is_object($item) ? $item->{$value} : $item[$value];

            if (!in_array($itemValue, $results)) {
                // If the key is "null", we will just append the value to the array and keep
                // looping. Otherwise we will key the array using the value of the key we
                // received from the developer. Then we'll return the final array form.
                if (is_null($key)) {
                    $results[] = $itemValue;
                } else {
                    $itemKey = is_object($item) ? $item->{$key} : $item[$key];

                    $results[$itemKey] = $itemValue;
                }
            }
        }

        return $results;
    }
}

if (!function_exists('array_pluck_where')) {
    /**
     * Pluck an array of values from an array.
     *
     * @param  array  $array
     * @param  string $value
     * @param  string $check
     * @return array
     */
    function array_pluck_where($array, $value, $check)
    {
        $results = array();

        foreach ($array as $item) {
            $itemValue = is_object($item) ? $item->{$value} : $item[$value];

            if ($itemValue == $check) {
                //$itemKey = is_object($item) ? $item->{$key} : $item[$key];

                $results[] = $item;
            }
        }

        return $results;
    }
}

if (!function_exists('array_pluck_first_where')) {
    /**
     * Plucks the first item from the array that matches the condition.
     *
     * @param  array  $array
     * @param  string $value
     * @param  string $check
     * @return mixed
     */
    function array_pluck_first_where($array, $value, $check)
    {
        foreach ($array as $item) {
            $itemValue = is_object($item) ? $item->{$value} : $item[$value];

            if ($itemValue == $check) {
                //$itemKey = is_object($item) ? $item->{$key} : $item[$key];

                return $item;
            }
        }
    }
}

if (!function_exists('app_url')) {
    function app_url()
    {
        return Config::get('app.url');
    }
}

if (!function_exists('nd_relate')) {
    function nd_relate(&$object, $numericIdentifier)
    {
        return $object->{'relationship_' . $numericIdentifier};
    }
}

if (!function_exists('mask')) {
    function mask($str, $start = 0, $length = null)
    {
        $mask = preg_replace("/\S/", "*", $str);
        if (is_null($length)) {
            $mask = substr($mask, $start);
            $str  = substr_replace($str, $mask, $start);
        } else {
            $mask = substr($mask, $start, $length);
            $str  = substr_replace($str, $mask, $start, $length);
        }

        return $str;
    }
}

if (!function_exists('shorten_description')) {
    function shorten_description($desc)
    {
        return substr($desc, 0, 100);
    }
}

if (!function_exists('transaction_label')) {
    function transaction_label($status)
    {
        switch ($status) {
            case 0:
                return '<span class="label label-warning">Pending</span>';
                break;
            case 1:
                return '<span class="label label-success">Approved</span>';
                break;
            case 2:
                return '<span class="label label-danger">Canceled</span>';
                break;
        }
    }
}

if (!function_exists('st')) {
    function st($data)
    {
        return strip_tags($data, '<br><p><h1><h2><h3><h4><h5><h6><strong><b><em><i><ul><ol><li><dd><u>');
    }
}

if (!function_exists('profile_image')) {
    function profile_image($userID, $email)
    {
        $userImageToken = Hash::make(md5($userID) . 'ndc_salt' . md5($email));

        if (!file_exists(public_path() . '/usedat/uimg/' . $userImageToken)) {
        }
    }
}

if (!function_exists('gender_icon')) {
    function gender_icon($gender)
    {
        switch ($gender) {
            case 0:
                return '<i class="fa fa-user"></i>';
                break;
            case 1:
                return '<i class="fa fa-male"></i>';
                break;
            case 2:
                return '<i class="fa fa-female"></i>';
                break;
            default:
                return '<i class="fa fa-user"></i>';
                break;
        }
    }
}

if (!function_exists('bool_to_icon')) {
    function bool_to_icon($bool)
    {
        if ($bool) {
            return '<span class="fa fa-check-circle text-succes"></span> Yes';
        }

        return '<span class="fa fa-minus-circle text-danger"></span> No';
    }
}

if (!function_exists('mysql_date_to_client')) {
    function mysql_date_to_client($date)
    {
        if (strlen($date) == 0) {
            return null;
        }

        return with(new Carbon($date))->format('m/d/Y');
    }
}

if (!function_exists('exists_then_or')) {
    function exists_then_or($array, $check, $then = true, $or = false)
    {
        if (array_key_exists($check, $array)) {
            if (is_object($then) and $then instanceof Closure) {
                return $then();
            }

            return $then;
        }

        if (is_object($or) and $or instanceof Closure) {
            return $or();
        }

        return $or;
    }
}

if (!function_exists('if_null_then')) {
    function if_null_then($check, $then)
    {
        if ($check === null) {
            if (is_object($then) and $then instanceof Closure) {
                return $then();
            }

            return $then;
        }

        return $check;
    }
}

if (!function_exists('if_null_then_na')) {
    function if_null_then_na($check)
    {
        if (strlen(trim($check)) == 0) {
            return 'Not available.';
        }

        return $check;
    }
}

if (!function_exists('client_active')) {
    function client_active($active)
    {
        if ($active == 1) {
            return 'Yes';
        }

        return 'No';
    }
}

if (!function_exists('mysql_date_or_null')) {
    function mysql_date_or_null($date)
    {
        if (strlen($date) > 0 and $date !== null and $date !== '') {
            return with(new Carbon($date))->toDateTimeString();
        }

        return null;
    }
}

if (!function_exists('address_string')) {
    function address_string($addressLine1, $addressLine2, $city, $state, $zip, $country)
    {
        $addressBlock = '';

        if (strlen($addressLine1) > 0) {
            $addressBlock .= e($addressLine1).', ';
        }

        if (strlen($addressLine2) > 0) {
            $addressBlock .= e($addressLine2).', ';
        }


        if (strlen($city) > 0) {
            $addressBlock .= e($city).', ';
        }

        if (strlen($state) > 0) {
            $addressBlock .= e($state).' ';
        }

        if (strlen($zip) > 0) {
            $addressBlock .= e($zip);
        }

        if (strlen($country) > 0) {
            $addressBlock .= ', '.e($country);
        }

        return $addressBlock;
    }
}

if (!function_exists('county_settings')) {
    function county_settings()
    {
        $lastTenant = Auth::user()->last_tenant;

        $countySettings = Cache::rememberForever('county_settings_'.$lastTenant, function () use ($lastTenant) {
            Tenant::assumeTenant($lastTenant);
            return Service\CountySettings::first();
        });
        return $countySettings;
    }
}

if (!function_exists('wl_reset_settings')) {
    function wl_reset_settings() {
        return App::make('\ParadoxOne\NDCounties\Settings\WorkRecordSettingsManager')->getWorkRecordResetSettings();
    }
}

if (!function_exists('date_period')) {
    function date_period($dates)
    {
        if (is_array($dates)) {
            if (count($dates) >= 2) {
                $newDates  = array_unique($dates);
                $firstDate = new Carbon($newDates[0]);
                $startDate = $firstDate;
                $endDate   = $firstDate;

                foreach ($newDates as $date) {
                    $currentDate = new Carbon($date);
                    if ($currentDate->lt($startDate)) {
                        $startDate = $currentDate;
                    }
                    if ($currentDate->gt($endDate)) {
                        $endDate = $currentDate;
                    }
                }

                return $startDate->format('F d, Y') . ' - ' . $endDate->format('F d, Y');
            } elseif (count($dates) == 1) {
                return (new Carbon($dates[0]))->format('F d, Y');
            }

            return null;
        }

        return (new Carbon($dates))->format('F d, Y');
    }
}

if (!function_exists('user_settings')) {
    function user_settings()
    {
        if (Session::has('userSettings')) {
            $sessionSettings = Session::get('userSettings');

            // Validates the user settings.
            if (count($sessionSettings) < 8 || !array_key_exists('items_per_page', $sessionSettings) ||
                !array_key_exists('form_size', $sessionSettings) || !array_key_exists('auto_expand_more_options', $sessionSettings) ||
                !array_key_exists('auto_clear_forms', $sessionSettings) || !array_key_exists('active_year', $sessionSettings) || !array_key_exists('show_animation_on_form_change', $sessionSettings)) {
                Session::forget('userSettings');
            } else {
                return $sessionSettings;
            }
        } else {
            $userSettings = Auth::user()->settings->toArray();
            Session::put('userSettings', $userSettings);

            return $userSettings;
        }
    }
}

if (!function_exists('user_per_page')) {
    function user_per_page()
    {
        return user_settings()['items_per_page'];
    }
}

if (!function_exists('user_form_size')) {
    function user_form_size()
    {
        switch (user_settings()['form_size']) {
            case 2:
                return 'input-sm';
                break;
            case 3:
                return 'input-lg';
                break;
            default:
                return '';
                break;
        }
    }
}

if (!function_exists('nd_number_format_difference_to_zero')) {
    function nd_number_format_difference_to_zero($number)
    {
        if ($number < 0) {
            return 0;
        }
        return $number;
    }
}

if (!function_exists('nd_number_format')) {
    function nd_number_format($number)
    {
        if ($number === null) {
            return null;
        }

        if ($number < 0) {
            return '(' . abs(number_format(floatval($number), county_settings()['data_decimal_places'], '.', '')) . ')';
        }

        return number_format(floatval($number), county_settings()['data_decimal_places'], '.', '');
    }
}

if (!function_exists('lk')) {
    /**
     * Dump information about variables that Laravel will like.
     *
     * @param mixed $data
     *
     * @return void|string
     */
    function lk()
    {
        if (!Kint::enabled()) {
            return null;
        }

        $args = func_get_args();
        call_user_func_array(array('Kint', 'dump'), $args);
        die;
    }
}

if (!function_exists('show_overtime')) {
    function show_overtime()
    {
        if (county_settings()['show_employee_overtime'] == true) {
            return true;
        }

        return false;
    }
}

if (!function_exists('skip_class')) {
    /**
     * Helper function for inserting skip control styles and instructions.
     *
     * @param $returnType
     * @param $check
     * @return string
     */
    function skip_class($returnType, $check) {
        if ($check && $returnType == 1) {
            return 'has-info';
        }

        if ($check && $returnType == 2) {
            return 'tabindex="-1"';
        }

        return '';
    }
}