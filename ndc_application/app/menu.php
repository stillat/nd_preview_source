<?php


use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;

$currentRole = Session::get('userRole', 0);

$protectedMenuRoutes = [
    url('work-logs/create')
];

Menu::setRenderer(new ParadoxOne\NDCounties\UI\BracketMenuRenderer);

$menu = Menu::handle('main', 'Main', array(
    'class' => 'nav nav-pills nav-stacked nav-bracket'
    ));

Event::listen('router.matched', function ($args) use ($menu, $currentRole, $protectedMenuRoutes) {
    //$path = substr($args->getCompiled()->getStaticPrefix(), 1);


    if (!in_array(Request::url(), $protectedMenuRoutes)) {
        if (Str::startsWith(Request::url(), url('settings/'))) {
            Menu::setRoutePath(url('settings/account'));
        } elseif (Str::startsWith(Request::url(), url('data-tools/'))) {
            Menu::setRoutePath(url('data-tools/export'));
        } elseif (Str::startsWith(Request::url(), action('App\Work\WorkLogsController@index'))) {
            Menu::setRoutePath(action('App\Work\WorkLogsController@index'));
        } else {
            Menu::setRoutePath(Request::url());
        }
    } else {
        Menu::setRoutePath(Request::url());
    }

    if (Auth::user() == null) {
        return Redirect::to('login');
    }

    $cacheKey = 'menu_';

    if (Auth::user()->admin) {
        $cacheKey .= 'admin';
    }

    if (!Cache::has($cacheKey)) {
        $dashboardMenu = Menu::handle('dashboard', '<i class="fa fa-dashboard"></i> <span>Overview</span>');
        $dashboardMenu->addItem(app_url(), '<i class="fa fa-dashboard"></i> General Information');
        $dashboardMenu->addItem(url('/recent-activity'), '<i class="fa fa-clock-o"></i> Recent Activity');

        if (Auth::user()->admin) {
            $dashboardMenu->addItem(url('/admin'), '<i class="fa fa-cloud"></i> Service Dashboard');
        }

        $menu->addMenu($dashboardMenu);

        if (Auth::user()->last_tenant == 0) {
            $menu->addItem(action('App\ServiceAccountController@getChoose'), '<i class="fa fa-cloud"></i> <span>Choose Service Account</span>');
        } else {
            require_once 'menus/secretary.php';
            require_once 'menus/finance.php';
        }

        if (Auth::user()->admin == true) {
            require_once 'menus/serviceAdmin.php';
        }

        $menu->addItem(url('settings/account'), '<i class="fa fa-cog"></i> <span>Settings</span>');
        $menu->addItem(url('data-tools/export'), '<i class="fa fa-wrench"></i> <span>Data Tools</span>');
    }


});
