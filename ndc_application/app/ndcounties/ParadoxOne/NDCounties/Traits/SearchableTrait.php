<?php namespace ParadoxOne\NDCounties\Traits;

use Illuminate\Support\Facades\Input;

trait SearchableTrait
{

    use AdvancedSearchPreparationTrait;

    public function search($searchTerms)
    {
        // $includeTrashed = (Input::get('it', 0) == 1);
        $includeTrashed = Input::has('it');

        if (Input::get('is', null) !== null) {
            return $this->table()->where('id', '=', Input::get('is'))->get();
        }

        if ($searchTerms === '//') {
            $builder = $this->table();

            if (!$includeTrashed) {
                $builder->whereNull('deleted_at');
            }

            return $builder->get();
        }

        $builder = $this->table()
                        ->orWhere(function ($query) use ($searchTerms) {
                            // This used to search on name and description as well.
                            $query->where('code', 'LIKE', $this->prepareSearch($searchTerms));
                        });

        if (!$includeTrashed) {
            $builder->where(function ($query) {
                $query->whereNull('deleted_at');
            });
        }

        // Hopefully this will put the shortest option first.
        $builder->orderByRaw('LENGTH(code)');

        return $builder->get();
    }
}
