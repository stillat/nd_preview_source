<?php namespace ParadoxOne\NDCounties\Traits;

use Illuminate\Support\Facades\App;

trait ClassMapRegisterableTrait
{
    public function register()
    {
        foreach ($this->newableClassMap as $contract => $implementation) {
            $this->app->bind($contract, $implementation);
        }

        foreach ($this->repositoryClassMap as $repository => $implementation) {
            $this->app->singleton($repository, function () use ($implementation) {
                // return new $implementation;
                return App::make($implementation);
            });
        }
    }
}
