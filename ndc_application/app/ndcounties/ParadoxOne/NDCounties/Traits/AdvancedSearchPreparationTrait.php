<?php

namespace ParadoxOne\NDCounties\Traits;

use Illuminate\Support\Str;

trait AdvancedSearchPreparationTrait
{

    /**
     * Prepares advanced search terms.
     *
     * @param $searchTerms
     */
    public function prepareSearch($searchTerms)
    {
        if (Str::contains($searchTerms, ['%', '_'])) {
            return '%'.$searchTerms;
        }

        return '%'.$searchTerms.'%';
    }

}