<?php namespace ParadoxOne\NDCounties\Traits;

use Illuminate\Support\Facades\App;

trait DefaultSortableTrait
{
    private function applySortingSetting($column, $setting, &$builder)
    {
        if ($setting == 'asc') {
            $builder = $builder->orderBy($column, 'asc');
        } elseif ($setting == 'desc') {
            $builder = $builder->orderBy($column, 'desc');
        }
    }

    /**
     * Applies the default settings for costable sorting systems.
     *
     * @param $sortSettingName
     * @param $builder
     */
    private function applyCostableSorting($sortSettingName, &$builder)
    {
        $sortSettings = App::make('sorting_work_entry');
        $sortSettings = json_decode($sortSettings->get($sortSettingName), true);

        foreach ($sortSettings as $column => $settings) {
            switch ($column) {
                case 'code':
                    $this->applySortingSetting('code', $settings, $builder);
                    break;
                case 'name':
                    $this->applySortingSetting('name', $settings, $builder);
                    break;
                case 'desc':
                    $this->applySortingSetting('description', $settings, $builder);
                    break;
                case 'last_updated':
                    $this->applySortingSetting('updated_at', $settings, $builder);
                    break;
                case 'date_created':
                    $this->applySortingSetting('created_at', $settings, $builder);
                    break;
                case 'cost':
                    $this->applySortingSetting('cost', $settings, $builder);
                    break;
            }
        }

        $sortSettings = null;
        unset($sortSettings);
    }

    /**
     * Applies the default settings to a query builder instance.
     *
     * @param $sortSettingName The NAME of the sort settings.
     * @param $builder
     */
    private function applySorting($sortSettingName, &$builder)
    {
        $sortSettings = App::make('sorting_work_entry');
        $sortSettings = json_decode($sortSettings->get($sortSettingName), true);

        foreach ($sortSettings as $column => $settings) {
            switch ($column) {
                case 'code':
                    $this->applySortingSetting('code', $settings, $builder);
                    break;
                case 'name':
                    $this->applySortingSetting('name', $settings, $builder);
                    break;
                case 'desc':
                    $this->applySortingSetting('description', $settings, $builder);
                    break;
                case 'last_updated':
                    $this->applySortingSetting('updated_at', $settings, $builder);
                    break;
                case 'date_created':
                    $this->applySortingSetting('created_at', $settings, $builder);
                    break;
            }
        }

        $sortSettings = null;
        unset($sortSettings);
    }
}
