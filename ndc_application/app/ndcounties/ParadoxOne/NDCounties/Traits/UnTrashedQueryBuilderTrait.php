<?php

namespace ParadoxOne\NDCounties\Traits;

use Illuminate\Support\Facades\DB;

trait UnTrashedQueryBuilderTrait
{

    private $unTrashedBuilderValidTableNames = [
        'activities', 'departments', 'districts',
        'employees', 'projects', 'fs_accounts', 'fs_tax_rates',
        'consumables', 'properties'
    ];

    private function convertArrayOfStringsToIntArray($array)
    {
        $newArray = [];

        foreach ($array as $item) {
            try {
                if (is_int((int)$item)) {
                    $newArray[] = (int)$item;
                }
            } catch (\Exception $e) {
                continue;
            }
        }

        return $newArray;
    }

    private function getRecordsThatCantBeRestored($table, $trashed, $skipName = false, $whereColumn = null, $whereOperator = null, $whereValue = null)
    {
        $trashed = $this->convertArrayOfStringsToIntArray($trashed);
        $similarRecords = $this->getUnTrashedSameAsTrashed($table, $trashed, $skipName, $whereColumn, $whereOperator, $whereValue);
        $restorationData = new \stdClass;
        $restorationData->cantBeRestoredIDs = array_pluck_unique($similarRecords, 'trashedRecordSame');
        $restorationData->recordsThatCanBeRestored = array_diff($trashed, $restorationData->cantBeRestoredIDs);
        $restorationData->similarRecords = $similarRecords;
        return $restorationData;
    }

    private function getUnTrashedSameAsTrashed($table, $trashed, $skipName = false, $whereColumn = null, $whereOperator = null, $whereValue = null)
    {

        if (!in_array($table, $this->unTrashedBuilderValidTableNames)) {
            throw new \Exception('Invalid Table Name');
        }

        if (!is_array($trashed)) {
            $trashed = array($trashed);
        }

        if (count($trashed) == 0) {
            return [];
        }

        $newTrashedItems = $this->convertArrayOfStringsToIntArray($trashed);

        $trashString = implode(',', $newTrashedItems);

        if (strlen($trashString) == 0) {
            return [];
        }

        try {
            $unTrashed = [];

            if (!$skipName) {
                $unTrashed = $this->table()->select(DB::raw(
                    '*, (select sa.id from ' . $table . ' as sa where ' . $table . '.code = sa.code and ' . $table . '.deleted_at IS NULL and sa.deleted_at IS NOT NULL LIMIT 1) as trashedRecordSame'
                ))->whereRaw(
                    'deleted_at IS NULL and code in (select code from ' . $table . ' where id in (?)) or deleted_at is null and name in (select name from ' . $table . ' where id in (?))', [$trashString, $trashString]
                );
            } else {
                $unTrashed = $this->table()->select(DB::raw(
                    '*, (select sa.id from ' . $table . ' as sa where ' . $table . '.code = sa.code and ' . $table . '.deleted_at IS NULL and sa.deleted_at IS NOT NULL LIMIT 1) as trashedRecordSame'
                ))->whereRaw(
                    'deleted_at IS NULL and code in (select code from ' . $table . ' where id in (?))', [$trashString]
                );
            }

            if ($whereColumn != null) {
                $unTrashed = $unTrashed->where($whereColumn, $whereOperator, $whereValue);
            }

            $unTrashed = $unTrashed->get();

            return $unTrashed;
        } catch (\Exception $e) {
        }

        return [];
    }

}