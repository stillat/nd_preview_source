<?php namespace ParadoxOne\NDCounties\Traits;

use Illuminate\Support\Facades\App;
use ParadoxOne\NDCounties\Contracts\MathExecutionEngineInterface;

trait MathExecutableTrait
{
    /**
     * The MathExecutionEngineInterface implementation
     *
     * @var \ParadoxOne\NDCounties\Contracts\MathExecutionEngineInterface
     */
    protected $mathEngine = null;

    /**
     * Sets the math engine implementation to be used
     *
     * @param \ParadoxOne\NDCounties\Contracts\MathExecutionEngineInterface $mathEngine
     */
    public function setMathEngine(MathExecutionEngineInterface $mathEngine = null)
    {
        if ($mathEngine == null) {
            if ($this->mathEngine == null) {
                // Here we will attempt to create one ourselves.
                $this->mathEngine = App::make('\ParadoxOne\NDCounties\Contracts\MathExecutionEngineInterface');
            }
        } else {
            // If the user supplied us with an instance of a math
            // engine we will use that instead.
            $this->mathEngine = $mathEngine;
        }
    }

    /**
     * Returns the math engine implementation being used
     *
     * @return \ParadoxOne\NDCounties\Contracts\MathExecutionEngineInterface
     */
    public function getMathEngine()
    {
        return $this->mathEngine;
    }
}
