<?php namespace ParadoxOne\NDCounties\Traits;

use Illuminate\Support\Collection;

trait ReportSettingsBuilderTrait
{
    /**
     * Build an automatic settings object.
     *
     * @param $input
     * @return mixed
     */
    private function buildAutomaticSettings($input)
    {
        $inputCollection                                              = new Collection($input);
        $automaticSettingsObject                                      = new \stdClass;
        $automaticSettingsObject->show_employee_wage                  = $inputCollection->get('show_employee_wage', 0);
        $automaticSettingsObject->show_material_breakdown             =
            $inputCollection->get('show_material_breakdown', 1);
        $automaticSettingsObject->show_material_breakdown_total       =
            $inputCollection->get('show_material_breakdown_total', 1);
        $automaticSettingsObject->show_equipment_unit_breakdown       =
            $inputCollection->get('show_equipment_unit_breakdown', 1);
        $automaticSettingsObject->show_equipment_unit_breakdown_total =
            $inputCollection->get('show_equipment_unit_breakdown_total', 1);
        $automaticSettingsObject->show_equipment_unit_rate            =
            $inputCollection->get('show_equipment_unit_rate', 1);
        $automaticSettingsObject->show_fuel_breakdown                 = $inputCollection->get('show_fuel_breakdown', 1);
        $automaticSettingsObject->show_fuel_breakdown_total           =
            $inputCollection->get('show_fuel_breakdown_total', 1);
        $automaticSettingsObject->show_property_breakdown             =
            $inputCollection->get('show_property_breakdown', 1);
        $automaticSettingsObject->show_property_breakdown_total       =
            $inputCollection->get('show_property_breakdown_total', 1);

        return $automaticSettingsObject;
    }

    /**
     * Build an automatic grouping object.
     *
     * @param $input
     * @return mixed
     */
    private function buildAutomaticGrouping($input)
    {
        $inputCollection                                  = new Collection($input);
        $automaticGroupingObject                          = new \stdClass;
        $automaticGroupingObject->active_grouping_method  = $inputCollection->get('org_strategy', 1);
        $automaticGroupingObject->generic_grouping_filter =
            $inputCollection->get('generic_grouping_column', 'activity_id');
        $automaticGroupingObject->special_grouping_column =
            $inputCollection->get('special_grouping_column', 'activity_id');
        $automaticGroupingObject->special_grouping_name   = $inputCollection->get('special_grouping_name', 'OTHER');
        $automaticGroupingObject->group_report_column     = $inputCollection->get('group_report_column', 'none');

        return $automaticGroupingObject;
    }

    /**
     * Build an engine internals object.
     *
     * @param $input
     * @return mixed
     */
    private function buildEngineInternals($input)
    {
        $inputCollection                       = new Collection($input);
        $automaticEngineInternals              = new \stdClass;
        $automaticEngineInternals->engine_mode = $inputCollection->get('engine_mode', 1);

        return $automaticEngineInternals;
    }
}
