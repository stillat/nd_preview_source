<?php namespace ParadoxOne\NDCounties\System;

use Illuminate\Support\Facades\App;
use ParadoxOne\NDCounties\Contracts\Resolvers\ConsumableTypeResolverInterface;
use ParadoxOne\NDCounties\Contracts\Unions\UnionConsumableEquipmentUnitInterface;
use ParadoxOne\NDCounties\Contracts\Unions\UnionConsumableFuelInterface;
use ParadoxOne\NDCounties\Contracts\Unions\UnionConsumableMaterialInterface;

class ConsumableTypeResolver implements ConsumableTypeResolverInterface
{
    protected $classMap = array(
        '2' => 'ParadoxOne\NDCounties\Contracts\Unions\UnionConsumableEquipmentUnitInterface',
        '3' => 'ParadoxOne\NDCounties\Contracts\Unions\UnionConsumableFuelInterface',
        '4' => 'ParadoxOne\NDCounties\Contracts\Unions\UnionConsumableMaterialInterface',
    );

    protected $propertyResolvers = array(
        '2' => 'ParadoxOne\NDCounties\Contracts\Unions\UnionPropertyHandlerEquipmentUnitInterface',
    );

    /**
     * {@inheritdoc}
     */
    public function makeInstance($consumableType)
    {
        return App::make($this->resolve(intval($consumableType)));
    }

    /**
     * {@inheritdoc}
     */
    public function makePropertyHandlerInstance($consumableType)
    {
        return App::make($this->resolvePropertyHandler(intval($consumableType)));
    }

    /**
     * {@inheritdoc}
     */
    public function resolvePropertyHandler($consumableType)
    {
        if (is_int($consumableType)) {
            if (array_key_exists($consumableType, $this->propertyResolvers)) {
                return $this->propertyResolvers[$consumableType];
            } else {
                return false;
            }
        }
    }

    /**
     * {@inheritdoc}
     */
    public function resolve($consumableType)
    {
        if (is_int($consumableType)) {
            if (array_key_exists($consumableType, $this->classMap)) {
                return $this->classMap[$consumableType];
            } else {
                return false;
            }
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getTypeIdentifiers()
    {
        return array_keys($this->classMap);
    }

    /**
     * {@inheritdoc}
     */
    public function isMaterial($consumableType)
    {
        if (is_object($consumableType)) {
            if ($consumableType instanceof UnionConsumableMaterialInterface) {
                return true;
            }
        } else {
            if (intval($consumableType) == 4) {
                return true;
            }
        }

        return false;
    }

    /**
     * {@inheritdoc}
     */
    public function isFuel($consumableType)
    {
        if (is_object($consumableType)) {
            if ($consumableType instanceof UnionConsumableFuelInterface) {
                return true;
            }
        } else {
            if (intval($consumableType) == 3) {
                return true;
            }
        }

        return false;
    }

    /**
     * {@inheritdoc}
     */
    public function isEquipmentUnit($consumableType)
    {
        if (is_object($consumableType)) {
            if ($consumableType instanceof UnionConsumableEquipmentUnitInterface) {
                return true;
            }
        } else {
            if (intval($consumableType) == 2) {
                return true;
            }
        }

        return false;
    }
}
