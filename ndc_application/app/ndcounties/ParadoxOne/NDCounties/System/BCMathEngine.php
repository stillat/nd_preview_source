<?php namespace ParadoxOne\NDCounties\System;

use ParadoxOne\NDCounties\Contracts\MathExecutionEngineInterface;

class BCMathEngine implements MathExecutionEngineInterface
{
    /**
     * This should match the decimal places that are allowed on
     * the DB schema.
     *
     * @var integer
     */
    private $scalingDecimalPlaces = 6;

    /**
     * {@inheritdoc}
     */
    public function add($a, $b)
    {
        return bcadd($a, $b, $this->scalingDecimalPlaces);
    }

    /**
     * {@inheritdoc}
     */
    public function div($a, $b)
    {
        return bcdiv($a, $b, $this->scalingDecimalPlaces);
    }

    /**
     * {@inheritdoc}
     */
    public function comp($a, $b)
    {
        return bccomp($a, $b, $this->scalingDecimalPlaces);
    }

    /**
     * {@inheritdoc}
     */
    public function mul($a, $b)
    {
        return bcmul($a, $b, $this->scalingDecimalPlaces);
    }

    /**
     * {@inheritdoc}
     */
    public function pow($a, $b)
    {
        return bcpow($a, $b, $this->scalingDecimalPlaces);
    }

    /**
     * {@inheritdoc}
     */
    public function sqrt($a)
    {
        return bcsqrt($a, $this->scalingDecimalPlaces);
    }

    /**
     * {@inheritdoc}
     */
    public function sub($a, $b)
    {
        return bcsub($a, $b, $this->scalingDecimalPlaces);
    }
}
