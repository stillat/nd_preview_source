<?php namespace ParadoxOne\NDCounties\System;

use Illuminate\Support\ServiceProvider;

class SystemServiceProvider extends ServiceProvider
{
    use \ParadoxOne\NDCounties\Traits\ClassMapRegisterableTrait;

    protected $defer = false;

    protected $newableClassMap = array();

    protected $repositoryClassMap = array(
        '\ParadoxOne\NDCounties\Contracts\MathExecutionEngineInterface' => '\ParadoxOne\NDCounties\System\BCMathEngine',
    );
}
