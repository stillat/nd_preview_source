<?php namespace ParadoxOne\NDCounties\Application;

use Illuminate\Filesystem\Filesystem;
use Symfony\Component\Process\Process;

class Updater
{
    protected $files;

    protected $workingDirectory;

    public function __construct(Filesystem $files, $workingDirectory)
    {
        $this->files = $files;
    }

    protected function getComposer()
    {
        if ($this->files->exists(app_path().'/support/composer.phar')) {
            return '"'.PHP_BINARY.'" composer.phar';
        }

        return 'composer';
    }

    public function migrateHub()
    {
        $process = $this->getProcess();
        chdir(app_path());
        $process->setCommandLine('php artisan migrate');
        $process->run();

        if ($process->isSuccessful()) {
            echo 'Hub migrated';
        } else {
            echo $process->getErrorOutput();
        }
    }

    public function updateDependencies()
    {
        $process = $this->getProcess();
        $process->setCommandLine(trim($this->getComposer()).' update');
        $process->run();

        if ($process->isSuccessful()) {
        } else {
            lk($process->getErrorOutput());
        }
    }

    public function setWorkingPath($path)
    {
        $this->workingDirectory = realpath($path);
        return $this;
    }

    protected function getProcess()
    {
        return (new Process('', $this->workingDirectory))->setTimeout(null);
    }
}
