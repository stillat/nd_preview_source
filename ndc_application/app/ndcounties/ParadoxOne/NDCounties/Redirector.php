<?php namespace ParadoxOne\NDCounties;

use App;
use Debugbar;
use Session;
use URL;

class Redirector
{
    public static function logURL($url)
    {
        Session::put('ndc_redto', $url);
    }

    public static function logCurrent()
    {
        $request = URL::getRequest();
        $uri     = $request->server('REQUEST_URI');
        $uri     = substr($uri, 4);
        if (!App::environment('production')) {
            Debugbar::addMessage($uri, 'REQUEST_URI from Redirector');
        }
        self::logURL($uri);
    }

    public static function getRoute($defaultUrl = '/')
    {
        if (Session::has('ndc_redto')) {
            return url(Session::get('ndc_redto'));
        }

        return $defaultUrl;
    }

    public static function getURL($defaultUrl = '/')
    {
        return Session::get('ndc_redto', $defaultUrl);
    }
}
