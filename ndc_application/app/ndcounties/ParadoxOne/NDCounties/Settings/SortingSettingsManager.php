<?php namespace ParadoxOne\NDCounties\Settings;

class SortingSettingsManager extends BaseSettingManager
{
    // Administrative assistant tables.
    const WORK_LOG_TABLE_SETTING_NAME    = 'work_logs';
    const ACTIVITIES_TABLE_SETTING_NAME  = 'activities';
    const DEPARTMENTS_TABLE_SETTING_NAME = 'departments';
    const DISTRICTS_TABLE_SETTING_NAME   = 'districts';
    const PROJECTS_TABLE_SETTING_NAME    = 'projects';
    const EMPLOYEES_TABLE_SETTING_NAME   = 'employees';
    const FUELS_TABLE_SETTING_NAME       = 'fuels';
    const MATERIALS_TABLE_SETTING_NAME   = 'materials';
    const PROPERTIES_TABLE_SETTING_NAME  = 'properties';

    // Finance system sorting tables.
    const FS_ACCOUNTS_TABLE_SETTING_NAME  = 'f-accounts';
    const FS_INVOICES_TABLE_SETTING_NAME  = 'f-invoices';
    const FS_TAX_RATES_TABLE_SETTING_NAME = 'f-tax-rates';


    protected $subSystem = 'sorting';

    const STANDARD_SORT_SETTINGS = '{
        "code": "asc",
        "name": "asc",
        "desc": "none",
        "last_updated": "none",
        "date_created": "none"
    }';

    const STANDARD_COSTABLE_SORT_SETTINGS = '{
        "code": "asc",
        "name": "asc",
        "desc": "none",
        "last_updated": "none",
        "date_created": "none",
        "cost": "none"
    }';

    protected $defaultSettings = [
        self::WORK_LOG_TABLE_SETTING_NAME     => '{
              "date_created": "desc",
              "work_date": "none",
              "employee_name": "none",
              "employee_regular_hours": "none",
              "employee_overtime_hours": "none",
              "employee_cost": "none",
              "project": "none",
              "activity": "none",
              "district": "none",
              "department": "none",
              "property": "none",
              "last_updated": "none"
            }',
        self::ACTIVITIES_TABLE_SETTING_NAME   => self::STANDARD_SORT_SETTINGS,
        self::DEPARTMENTS_TABLE_SETTING_NAME  => self::STANDARD_SORT_SETTINGS,
        self::DISTRICTS_TABLE_SETTING_NAME    => self::STANDARD_SORT_SETTINGS,
        self::PROJECTS_TABLE_SETTING_NAME     => '{
            "code": "asc",
            "name": "asc",
            "desc": "none",
            "last_updated": "none",
            "date_created": "none",
            "is_fema": "none"
        }',
        self::FUELS_TABLE_SETTING_NAME        => self::STANDARD_COSTABLE_SORT_SETTINGS,
        self::MATERIALS_TABLE_SETTING_NAME    => self::STANDARD_COSTABLE_SORT_SETTINGS,

        self::PROPERTIES_TABLE_SETTING_NAME   => '{
            "code": "asc",
            "name": "asc",
            "desc": "none",
            "last_updated": "none",
            "date_created": "none",
            "type": "none"
        }',

        self::FS_ACCOUNTS_TABLE_SETTING_NAME  => '{
            "code": "asc",
            "name": "asc",
            "desc": "none",
            "category": "none",
            "balance": "none",
            "last_updated": "none",
            "date_created": "none"
        }',

        self::FS_TAX_RATES_TABLE_SETTING_NAME => '{
            "name": "asc",
            "tax_rate": "none",
            "a_employee": "none",
            "a_equipment": "none",
            "a_fuels": "none",
            "a_materials": "none",
            "last_updated": "none",
            "date_created": "none"
        }',

        self::FS_INVOICES_TABLE_SETTING_NAME  => '{
            "invoice_number": "asc",
            "due_date": "none",
            "discount_rate": "none",
            "billed_account": "none",
            "paid_account": "none",
            "invoice_total": "none",
            "total_materials": "none",
            "total_equipments": "none",
            "total_employees": "none",
            "total_fuels": "none",
            "tax_total": "none",
            "sub_total": "none",
            "discount_total": "none",
            "last_updated": "none",
            "date_created": "none"
        }',

        self::EMPLOYEES_TABLE_SETTING_NAME    => '{
            "code": "asc",
            "first_name": "none",
            "middle_name": "none",
            "last_name": "none",
            "hire_date": "none",
            "termination_date": "none",
            "regular_wage": "none",
            "regular_benefits": "none",
            "overtime_wage": "none",
            "overtime_benefits": "none",
            "last_updated": "none",
            "date_created": "none"
        }'
    ];
}
