<?php namespace ParadoxOne\NDCounties\Settings;

use Illuminate\Support\Facades\Auth;
use ParadoxOne\NDCounties\Contracts\Settings\CountyWideSettingsRepositoryInterface;

class BaseCountySettingManager
{
    /**
     * The setting sub system.
     *
     * @var string
     */
    protected $subSystem = '';

    /**
     * The settings repository.
     *
     * @var ParadoxOne\NDCounties\Contracts\Settings\CountyWideSettingsRepositoryInterface
     */
    protected $settingRepository;

    /**
     * The current service account in use by the user.
     *
     * @var int
     */
    protected $currentServiceAccount = 0;

    /**
     * Holds the default values for a given setting.
     *
     * @var array
     */
    protected $defaultSettings = [];

    public function __construct(CountyWideSettingsRepositoryInterface $settingRepository)
    {
        $this->settingRepository     = $settingRepository;
        $this->currentServiceAccount = Auth::user()->last_tenant;
    }

    public function getDefaultSettings()
    {
        return $this->defaultSettings;
    }

    public function getSubSystem()
    {
        return $this->subSystem;
    }

    private function getDefaultValue($settingName, $value)
    {
        if ($value != null) {
            return $value;
        }

        if (array_key_exists($settingName, $this->defaultSettings)) {
            return $this->defaultSettings[$settingName];
        }

        return null;
    }

    public function get($settingName, $defaultValue = null)
    {
        return $this->settingRepository->getSettingValue($this->currentServiceAccount, $this->subSystem, $settingName,
                                                         $this->getDefaultValue($settingName, $defaultValue));
    }

    public function put($settingName, $value)
    {
        return $this->settingRepository->createSetting($this->currentServiceAccount, $this->subSystem, $settingName,
                                                       $value);
    }

    public function forget($settingName)
    {
        return $this->settingRepository->removeSetting($this->currentServiceAccount, $this->subSystem, $settingName);
    }

    public function set($settingName, $value)
    {
        return $this->settingRepository->setSettingValue($this->currentServiceAccount, $this->subSystem, $settingName,
                                                         $value);
    }
}
