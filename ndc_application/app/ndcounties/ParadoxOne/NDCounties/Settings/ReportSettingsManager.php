<?php namespace ParadoxOne\NDCounties\Settings;

class ReportSettingsManager extends BaseCountySettingManager
{
    const COUNTY_REPORT_SETTING_NAME = 'ct_report_settings';

    protected $subSystem = 'reporting';

    protected $defaultSettings = [
        self::COUNTY_REPORT_SETTING_NAME => '{
  "default_equation": 0,
  "show_employee_wage": false,
  "show_employee_hours_worked": true,
  "show_material_breakdown": true,
  "show_material_breakdown_total": true,

  "show_equipment_unit_breakdown": true,
  "show_equipment_unit_breakdown_total": true,

  "show_fuel_breakdown": true,
  "show_fuel_breakdown_total": true,

  "show_property_breakdown": true,
  "show_property_breakdown_total": true,

  "show_removed_filters": false,

  "show_borders_on_breakdowns": true,
  "record_breakdown_style": 1,
  "show_equipment_unit_rate": 1,

  "always_show_billing_description": false,
  "always_show_activity_description": false,
  "only_show_descriptions_if_entered": true,

  "report_print_address": false,
  "report_print_filters": true,
  "report_print_generator": false,
  "report_print_report_engine_version": false,
  "report_print_margin_settings": 1,
  "report_print_orientation": 1
}',
    ];

    public function getReportSettings()
    {
        return json_decode($this->get(self::COUNTY_REPORT_SETTING_NAME));
    }

    public function updateReportSettings($settings)
    {
        return $this->put(self::COUNTY_REPORT_SETTING_NAME, $settings);
    }
}
