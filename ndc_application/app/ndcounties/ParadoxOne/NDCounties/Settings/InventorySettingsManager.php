<?php namespace ParadoxOne\NDCounties\Settings;

class InventorySettingsManager extends BaseCountySettingManager
{
    const COUNTY_INVENTORY_MANAGEMENT_SETTING_NAME = 'ct_inventory_management';

    protected $subSystem = 'inventory';

    protected $defaultSettings = [
        self::COUNTY_INVENTORY_MANAGEMENT_SETTING_NAME => '{
  "__COMMENT": "These settings control the inventory management system interactions and behaviors.",
  "credit_inventory_on_operation_account": true,
  "auto_deduct_on_record_create": true,
  "show_inventory_management_in_consumables": true,
  "convert_removed_work_inventory_items_to_system": true
}'
    ];

    public function getInventorySettings()
    {
        return json_decode($this->get(self::COUNTY_INVENTORY_MANAGEMENT_SETTING_NAME));
    }

    public function updateInventorySettings($settings)
    {
        return $this->put(self::COUNTY_INVENTORY_MANAGEMENT_SETTING_NAME, $settings);
    }
}
