<?php namespace ParadoxOne\NDCounties\Settings;

class CountyFormattingSettingsManager extends BaseCountySettingManager
{
    const COUNTY_FORMATTING_SETTING_NAME = 'ct_formatting_public';

    protected $subSystem = 'formatting';

    protected $defaultSettings = [
        self::COUNTY_FORMATTING_SETTING_NAME => '{
  "__COMMENT": "These settings affect how the indicated data is displayed to the screen within the application",
  "useOnRecords": {
    "useOnRecords": "1"
  },
  "transform": {
    "transform": "1"
  },
  "employee": {
    "format": "1"
  },
  "fuel": {
    "format": "1"
  },
  "material": {
    "format": "1"
  },
  "activity": {
    "format": "1"
  },
  "department": {
    "format": "1"
  },
  "district": {
    "format": "1"
  },
  "project": {
    "format": "1"
  },
  "property": {
    "format": "1"
  }
}',
        'ct_formatting_heading' => '{
  "__COMMENT": "These settings affect how the indicated data is displayed to the screen within the application",
  "useOnRecords": {
    "useOnRecords": "1"
  },
  "transform": {
    "transform": "1"
  },
  "employee": {
    "format": "1"
  },
  "fuel": {
    "format": "1"
  },
  "material": {
    "format": "1"
  },
  "activity": {
    "format": "1"
  },
  "department": {
    "format": "1"
  },
  "district": {
    "format": "1"
  },
  "project": {
    "format": "1"
  },
  "property": {
    "format": "1"
  }
}'
    ];

    protected $cachedSettings = null;
    protected $cachedHeadingSettings = null;

    protected $transformation = 1;
    protected $headingTransformation = 1;

    public function getSettings($settingName, $settingValue = 'format')
    {
        if ($this->cachedSettings == null) {
            $this->cachedSettings = json_decode($this->get(self::COUNTY_FORMATTING_SETTING_NAME));
            $this->transformation = $this->cachedSettings->transform->transform;
        }

        return $this->cachedSettings->{$settingName}->{$settingValue};
    }

    public function getHeadingSettings($settingName, $settingValue = 'format')
    {
        if ($this->cachedHeadingSettings == null) {
            $this->cachedHeadingSettings = json_decode($this->get('ct_formatting_heading'));
            $this->headingTransformation = $this->cachedHeadingSettings->transform->transform;
        }

        return $this->cachedHeadingSettings->{$settingName}->{$settingValue};
    }

    public function doFormat($object, $settingName, $override)
    {
        $format = '';

        if ($override) {
            return $object->code;
        }

        switch ($this->getSettings($settingName)) {
            case 2:
                $format = $object->code;
                break;
            case 3:
                $format = $object->name;
                break;
            default:
                $format = $object->code . ' - ' . $object->name;
                break;
        }

        switch ($this->transformation) {
            case 2:
                return mb_strtoupper($format);
                break;
            case 3:
                return mb_strtolower($format);
                break;
        }

        return $format;
    }

    public function doFormatHeading($object, $settingName, $override)
    {
        $format = '';

        if ($override) {
            return $object->code;
        }

        switch ($this->getHeadingSettings($settingName)) {
            case 2:
                $format = $object->code;
                break;
            case 3:
                $format = $object->name;
                break;
            default:
                $format = $object->code . ' - ' . $object->name;
                break;
        }

        switch ($this->headingTransformation) {
            case 2:
                return mb_strtoupper($format);
                break;
            case 3:
                return mb_strtolower($format);
                break;
        }

        return $format;
    }

    public function employee($employee, $override = false)
    {
        $format = '';
        if ($override) {
            return $employee->code;
        }

        switch ($this->getSettings('employee')) {
            case 2:
                $format = $employee->code;
                break;
            case 3:
                // $format = $employee->lastName.', '.$employee->firstName;
                $format = $employee->name;
                break;
            default:
                $format = $employee->code . ' - ' . $employee->name;
                break;
        }

        switch ($this->transformation) {
            case 2:
                return mb_strtoupper($format);
                break;
            case 3:
                return mb_strtolower($format);
                break;
        }

        return $format;
    }

    public function employeeHeading($employee, $override = false)
    {
        $format = '';
        if ($override) {
            return $employee->code;
        }

        switch ($this->getHeadingSettings('employee')) {
            case 2:
                $format = $employee->code;
                break;
            case 3:
                // $format = $employee->lastName.', '.$employee->firstName;
                $format = $employee->name;
                break;
            default:
                $format = $employee->code . ' - ' . $employee->name;
                break;
        }

        switch ($this->headingTransformation) {
            case 2:
                return mb_strtoupper($format);
                break;
            case 3:
                return mb_strtolower($format);
                break;
        }

        return $format;
    }

    public function fuel($fuel, $override = false)
    {
        return $this->doFormat($fuel, 'fuel', $override);
    }

    public function fuelHeading($fuel, $override = false)
    {
        return $this->doFormatHeading($fuel, 'fuel', $override);
    }

    public function material($material, $override = false)
    {
        return $this->doFormat($material, 'material', $override);
    }

    public function materialHeading($material, $override = false)
    {
        return $this->doFormatHeading($material, 'material', $override);
    }

    public function activity($activity, $override = false)
    {
        return $this->doFormat($activity, 'activity', $override);
    }

    public function activityHeading($activity, $override = false)
    {
        return $this->doFormatHeading($activity, 'activity', $override);
    }

    public function department($department, $override = false)
    {
        return $this->doFormat($department, 'department', $override);
    }

    public function departmentHeading($department, $override = false)
    {
        return $this->doFormatHeading($department, 'department', $override);
    }

    public function other($object)
    {
        return $object->code;
    }

    public function district($district, $override = false)
    {
        return $this->doFormat($district, 'district', $override);
    }

    public function districtHeading($district, $override = false)
    {
        return $this->doFormatHeading($district, 'district', $override);
    }

    public function project($project, $override = false)
    {
        return $this->doFormat($project, 'project', $override);
    }

    public function projectHeading($project, $override = false)
    {
        return $this->doFormatHeading($project, 'project', $override);
    }

    public function equipment($equipment, $override = false)
    {
        return $this->doFormat($equipment, 'property', $override);
    }

    public function equipmentHeading($equipment, $override = false)
    {
        return $this->doFormatHeading($equipment, 'property', $override);
    }

    public function property($property, $override = false)
    {
        return $this->doFormat($property, 'property', $override);
    }

    public function propertyHeading($property, $override = false)
    {
        return $this->doFormatHeading($property, 'property', $override);
    }
}
