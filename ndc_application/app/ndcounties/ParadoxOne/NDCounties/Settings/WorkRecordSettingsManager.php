<?php namespace ParadoxOne\NDCounties\Settings;

class WorkRecordSettingsManager extends BaseSettingManager
{
    const WORK_RECORD_RESET_SETTING_NAME = 'work_logs_reset';

    protected $subSystem = 'work_records_settings';

    protected $defaultSettings = [
      self::WORK_RECORD_RESET_SETTING_NAME => '{
  "__COMMENT": "These are the settings that control how the work record form is reset.",
  "wl_reset_employee": false,
  "wl_reset_work_date": false,
  "wl_reset_activity": false,
  "wl_reset_department": false,
  "wl_reset_district": false,
  "wl_reset_property": false,
  "wl_reset_materials_and_items": false,
  "wl_reset_settings": true,
  "wl_reset_equipment_and_fuels": false,
  "wl_reset_only_fuels": false,
  "wl_keep_only_first_equip": false,
  "wl_reset_project": false,
  "wl_return_to_general": false,
  "wl_refresh_employee": false,
  "wl_enable_equipment_compatibility": false,
  "wl_enable_invoice_compatibility": false,
  "wl_optional_employee_overtime": false,
  "wl_optional_district": false
}'
    ];

    public function getWorkRecordResetSettings()
    {
        return json_decode($this->get(self::WORK_RECORD_RESET_SETTING_NAME));
    }

    public function updateWorkRecordResetSettings($settings)
    {
        return $this->put(self::WORK_RECORD_RESET_SETTING_NAME, $settings);
    }
}
