<?php namespace ParadoxOne\NDCounties\Settings;

use Illuminate\Support\Facades\Auth;
use ParadoxOne\NDCounties\Contracts\Settings\UserSettingsRepositoryInterface;

class BaseSettingManager
{
    /**
     * The setting sub system.
     *
     * @var string
     */
    protected $subSystem = '';

    /**
     * The settings repository.
     *
     * @var ParadoxOne\NDCounties\Contracts\Settings\UserSettingsRepositoryInterface
     */
    protected $settingRepository;

    /**
     * The currently authenticated user ID.
     *
     * @var int
     */
    protected $currentUserID = 0;

    /**
     * Holds the default values for a given setting.
     *
     * @var array
     */
    protected $defaultSettings = [];

    public function __construct(UserSettingsRepositoryInterface $settingRepository)
    {
        $this->settingRepository = $settingRepository;
        $this->currentUserID     = Auth::user()->id;
    }

    public function getDefaultSettings()
    {
        return $this->defaultSettings;
    }

    public function getSubSystem()
    {
        return $this->subSystem;
    }

    private function getDefaultValue($settingName, $value)
    {
        if ($value != null) {
            return $value;
        }

        if (array_key_exists($settingName, $this->defaultSettings)) {
            return $this->defaultSettings[$settingName];
        }

        return null;
    }

    public function get($settingName, $defaultValue = null)
    {
        return $this->settingRepository->getSettingValue($this->currentUserID, $this->subSystem, $settingName,
                                                         $this->getDefaultValue($settingName, $defaultValue));
    }

    public function put($settingName, $value)
    {
        return $this->settingRepository->createSetting($this->currentUserID, $this->subSystem, $settingName, $value);
    }

    public function forget($settingName)
    {
        return $this->settingRepository->removeSetting($this->currentUserID, $this->subSystem, $settingName);
    }

    public function set($settingName, $value)
    {
        return $this->settingRepository->setSettingValue($this->currentUserID, $this->subSystem, $settingName, $value);
    }

    public function forgetAll()
    {
        return $this->settingRepository->removeSubSystem($this->currentUserID, $this->subSystem);
    }
}
