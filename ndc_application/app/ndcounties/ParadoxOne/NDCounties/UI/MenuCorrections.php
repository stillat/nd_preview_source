<?php namespace ParadoxOne\NDCounties\UI;

use Illuminate\Support\Facades\Request;
use Illuminate\Support\Str;
use Stillat\Menu\Support\Facades\Menu;

class MenuCorrections
{
    public static function correct()
    {
        $currentURL = Request::url();

        // Fuels
        if (Str::startsWith($currentURL, url('fuels'))) {
            Menu::setRoutePath(url('fuels'));
        }
        // Materials
        elseif (Str::startsWith($currentURL, url('materials'))) {
            Menu::setRoutePath(url('materials'));
        }
        // Employees
        elseif (Str::startsWith($currentURL, url('employees'))) {
            Menu::setRoutePath(url('employees'));
        }
        // activities
        elseif (Str::startsWith($currentURL, url('activities'))) {
            Menu::setRoutePath(url('activities'));
        }
        // Employees
        elseif (Str::startsWith($currentURL, url('employees'))) {
            Menu::setRoutePath(url('employees'));
        }
        // departments
        elseif (Str::startsWith($currentURL, url('departments'))) {
            Menu::setRoutePath(url('departments'));
        }
        // districts
        elseif (Str::startsWith($currentURL, url('districts'))) {
            Menu::setRoutePath(url('districts'));
        }
        // projects
        elseif (Str::startsWith($currentURL, url('projects'))) {
            Menu::setRoutePath(url('projects'));
        }
        // properties
        elseif (Str::startsWith($currentURL, url('properties'))) {
            Menu::setRoutePath(url('properties'));
        }
        // work-logs
        elseif (Str::startsWith($currentURL, url('work-logs'))) {
            Menu::setRoutePath(url('work-logs'));
        }
        // finances/accounts
        elseif (Str::startsWith($currentURL, url('finances/accounts'))) {
            Menu::setRoutePath(url('finances/accounts'));
        } else {
            Menu::setRoutePath($currentURL);
        }
    }
}
