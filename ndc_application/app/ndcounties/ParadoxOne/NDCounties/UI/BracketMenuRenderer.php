<?php namespace ParadoxOne\NDCounties\UI;

use Stillat\Menu\Renderers\DefaultRenderer;

class BracketMenuRenderer extends DefaultRenderer
{
    /**
     * @param array $attributes
     * @return string
     */
    public function renderItemBody(array $attributes)
    {
        $dataAttribute = '';

        if ($attributes['level'] >= 0) {
            if (array_key_exists('data', $attributes)) {
                $data = $attributes['data'];
                foreach ($data as $key => $value) {
                    $dataAttribute .= 'data-'.$key.'="'.$value.'" ';
                }
            }
        }


        return '<a href="' . $attributes['href'] . '" '.$dataAttribute.'>' . $attributes['text'] . '</a>';
    }

    /**
     * Renders the main menu opening block
     *
     * @param  string array $options
     * @return string
     */
    public function renderMainOpen(array $attributes)
    {

        // Grab the original attributes we set on the menu. This
        // way we can manipulate them ourselves and add some of our
        // classes that we may want.
        $originalAttributes = $attributes['attributes'];

        // Grab the original CSS rules.

        $cssRules = array();

        // Just merge the original CSS rules array or string with
        // our new CSS rules array.
        if (array_key_exists('class', $originalAttributes)) {
            $cssRules = (array)$originalAttributes['class'];
        }

        // Remove some of the properties or attributes that we really
        // do not want to have show up automatically in the menu's output.
        unset($originalAttributes['class']);
        unset($originalAttributes['text']);
        unset($originalAttributes['child']);
        unset($originalAttributes['level']);

        // This checks to see if the menu has any active children. If it
        // does, we will add a new class to the menu element 'active-child'.
        if ($attributes['hasActiveChild']) {
            $cssRules = array_merge($cssRules, array('active-child', 'nav-active', 'active'));
        }

        // This variable will hold the HTML output for our attributes and
        // properties.
        $elementAttributes = '';

        // This loops through all the attributes and properties and generates
        // the corresponding HTML code for them.
        foreach ($originalAttributes as $name => $value) {
            $elementAttributes .= $this->formatAttributeValues($name, $value);
        }


        // Simply construct our HTML element.
        $menu = '';

        if ($attributes['level'] == 0) {
            // This generates the HTML code for applying CSS rules to an element.
            $cssRules = $this->formatAttributeValues('class', implode(' ', $cssRules));

            // If the 'level' is set to 0, we can be assured that the menu
            // we are generating is the base, or root, menu.
            $menu = '<ul ' . $elementAttributes . ' ' . $cssRules . '>';
        } else {
            // Here are going to add the Bootstrap 'dropdown' class to our list of classes.
            //$cssRules[] = 'dropdown';
            $cssRules[] = 'nav-parent';

            // This generates the HTML code for applying CSS rules to an element.
            $cssRules = $this->formatAttributeValues('class', implode(' ', $cssRules));

            $dataAttribute = '';

            if (array_key_exists('data', $attributes)) {
                $data = $attributes['data'];
                foreach ($data as $key => $value) {
                    $dataAttribute .= 'data-'.$key.'="'.$value.'" ';
                }
            }

            // Since the 'level' is not 0, we know we are creating a child
            // or nested menu. We can modify the output here to reflect this.
            $menu = '<li ' . $elementAttributes . ' ' . $cssRules. ' '. $dataAttribute . '>
			<a href="#" id="' . $attributes['href'] . '">' . $attributes['text'] . '</a><ul class="children">';
        }

        // Here we are going to add a

        // Return our newly created HTML element.
        return $menu;
    }
}
