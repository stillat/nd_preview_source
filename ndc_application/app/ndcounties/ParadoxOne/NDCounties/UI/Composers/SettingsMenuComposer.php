<?php namespace ParadoxOne\NDCounties\UI\Composers;

use Menu;
use Illuminate\Support\Facades\Request;
use ParadoxOne\NDCounties\UI\SettingsMenuRenderer;

class SettingsMenuComposer
{
    public function compose($view)
    {
        $accountWideSettings = Menu::handle('settings_account_wide', 'Account Settings', ['class' => 'list-unstyled settings-menu']);
        $accountWideSettings->setRenderer(new SettingsMenuRenderer());
        $reportSettings = Menu::handle('settings_report_settings', 'Report Settings', ['class' => 'list-unstyled settings-menu']);
        $reportSettings->setRenderer(new SettingsMenuRenderer());
        $userSettings = Menu::handle('settings_user_settings', 'My Settings', ['class' => 'list-unstyled settings-menu']);
        $userSettings->setRenderer(new SettingsMenuRenderer());

        $accountWideSettings->addItem(url('settings/account'), 'Contact Information');
        $accountWideSettings->addItem(url('settings/account/inventory'), 'Inventory Management');
        $accountWideSettings->addItem(url('settings/account/behavior'), 'System Behavior');

        $reportSettings->addItem(url('settings/reports/breakdown'), 'Breakdown Settings');
        $reportSettings->addItem(url('settings/reports/builder'), 'Builder Settings');
        $reportSettings->addItem(url('settings/reports/description'), 'Description Settings');
        $reportSettings->addItem(url('settings/reports/display'), 'Display Settings');
        $reportSettings->addItem(url('settings/reports/heading-display'), 'Heading Display Settings');
        $reportSettings->addItem(url('settings/reports/equations'), 'Report Formulas');
        $reportSettings->addItem(url('settings/reports/print'), 'Print Settings');

        $userSettings->addItem(url('settings/user/accessibility'), 'Accessibility');
        $userSettings->addItem(url('settings/user/contact'), 'Contact Information');
        $userSettings->addItem(url('settings/user/change-password'), 'My Password');
        $userSettings->addItem(url('settings/user/sorting'), 'Reset Sorting Settings');
    }
}
