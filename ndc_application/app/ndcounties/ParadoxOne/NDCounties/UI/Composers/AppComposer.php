<?php namespace ParadoxOne\NDCounties\UI\Composers;

use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\View;
use Menu;
use ParadoxOne\NDCounties\Contracts\Properties\PropertyCategoriesRepositoryInterface;
use ParadoxOne\NDCounties\Contracts\Properties\PropertyTypesRepositoryInterface;

class AppComposer
{
    protected $propertyCategoriesRepository;

    public function __construct(PropertyCategoriesRepositoryInterface $propertyCategoriesRepository)
    {
        $this->propertyCategoriesRepository = $propertyCategoriesRepository;
    }

    public function compose($view)
    {
        // Moved away from the bootstrap based flash messages.
        // $view->with('flashMessage', View::make('partials.flash', $view->getData()));

        // This piece of code removes the roles from the actual view itself.
        $roles = array(
            0 => '<i class="fa fa-keyboard-o inline-icon"></i> Administrative Assistant',
            1 => '<i class="fa ndcf-accounting inline-icon"></i> Accountant',
        );

        // Only show the service administrator option if the user is indeed a service administrator.
        if (Auth::user()->admin == true) {
            $roles = array_merge($roles, array(2 => '<i class="fa fa-cloud inline-icon"></i> Service Administrator'));
        }

        $currentRole = Session::get('userRole', 0);
        $cacheKey = 'menu_';

        if (Auth::user()->admin == false) {
            $cacheKey .= 'admin';

            if ($currentRole == 2) {
                $currentRole = 0;
                Session::set('userRole', 0);
            }
        }



        $menu = Cache::get($cacheKey, function () {
            return \Stillat\Menu\Support\Facades\Menu::render('main');
        });

        $view->with('serviceRoles', $roles);
        $view->with('currentRole', $currentRole);
        $view->with('mainMenu', $menu);
    }
}
