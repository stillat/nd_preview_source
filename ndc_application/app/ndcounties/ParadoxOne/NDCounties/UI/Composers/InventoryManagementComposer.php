<?php namespace ParadoxOne\NDCounties\UI\Composers;

use ParadoxOne\NDCounties\Contracts\InventoryManagement\AdjustmentRepositoryInterface;
use ParadoxOne\NDCounties\UI\Streams\Inventory\AdjustmentStream;

class InventoryManagementComposer
{
    protected $inventoryAdjustmentsRepository;

    protected $adjustmentStream;

    public function __construct(AdjustmentRepositoryInterface $inventoryAdjustmentsRepository, AdjustmentStream $adjustmentStream)
    {
        $this->inventoryAdjustmentsRepository = $inventoryAdjustmentsRepository;
        $this->adjustmentStream = $adjustmentStream;
    }

    public function compose($view)
    {
        $viewData = $view->getData();
        $adjustments = $this->inventoryAdjustmentsRepository->getAdjustments($view->consumable->id)->paginate(user_per_page());
        $this->adjustmentStream->setStreamData($adjustments);

        $recordString = make_restrictions_string(array_pluck($adjustments, 'id'));

        $view->with('inventoryAdjustments', $adjustments)->with('adjustmentStream', $this->adjustmentStream)->with('inventoryRestrictionString', $recordString);
    }
}
