<?php namespace ParadoxOne\NDCounties\UI\Composers;

use Illuminate\Support\Facades\Auth;
use ParadoxOne\NDCounties\Contracts\ActivityStreamRepositoryInterface;
use ParadoxOne\NDCounties\UI\Streams\Activity\UserActivityStream;

class ActivityStreamComposer
{
    /**
     * @var UserActivityStream
     */
    private $activityStream;
    /**
     * @var ActivityStreamRepositoryInterface
     */
    private $activityStreamRepository;

    public function __construct(UserActivityStream $activityStream, ActivityStreamRepositoryInterface $activityStreamRepository)
    {
        $this->activityStream = $activityStream;
        $this->activityStreamRepository = $activityStreamRepository;
    }

    public function compose($view)
    {
        $streamData = $this->activityStreamRepository->getRecentActivity(Auth::user()->last_tenant)->take(10)->get();
        $this->activityStream->setStreamData($streamData);

        $view->with('activityStream', $this->activityStream);
    }
}
