<?php namespace ParadoxOne\NDCounties\UI\Composers;

use Menu;

class MenuComposer
{
    public function compose($view)
    {
        Menu::handler('main', array('class' => 'nav nav-pills nav-stacked nav-main'))
            ->add('/', 'Dashboard')
            ->add('/', 'Property Management', Menu::items('properties'))
            ->add('/', 'Resource Management', Menu::items('resources'))
            ->add('/', 'Leave Feedback');

        Menu::handler('main')->getItemsAtDepth(0)->map(function ($item) {
            if ($item->hasChildren()) {
                $item->addClass('nav-parent');
                $item->getChildren()->addClass('children');
            }
        });
    }
}
