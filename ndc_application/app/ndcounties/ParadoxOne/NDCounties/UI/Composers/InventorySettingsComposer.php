<?php namespace ParadoxOne\NDCounties\UI\Composers;

use ParadoxOne\NDCounties\Settings\InventorySettingsManager;

class InventorySettingsComposer
{
    protected $inventorySettingsManager;

    public function __construct(InventorySettingsManager $inventorySettingsManager)
    {
        $this->inventorySettingsManager = $inventorySettingsManager;
    }

    public function compose($view)
    {
        $view->with('inventorySettings', $this->inventorySettingsManager->getInventorySettings());
    }
}
