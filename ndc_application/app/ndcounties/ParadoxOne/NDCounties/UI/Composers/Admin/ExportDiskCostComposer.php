<?php namespace ParadoxOne\NDCounties\UI\Composers\Admin;

use RecursiveDirectoryIterator;
use RecursiveIteratorIterator;
use FilesystemIterator;

class ExportDiskCostComposer
{
    /**
     * The maximum amount of KBs that should be allocated for users's exports.
     *
     * @var int
     */
    protected $maximumDesiredDiskCost = 2500000000;

    /**
     * The monthly cost per KB.
     *
     * @var int
     */
    protected $monthlyCostPerByte = 0.00000000133333;

    protected $exportDiskCost = 0;

    private function formatSizeUnits($bytes)
    {
        if ($bytes >= 1073741824) {
            $bytes = number_format($bytes / 1073741824, 2) . ' GB';
        } elseif ($bytes >= 1048576) {
            $bytes = number_format($bytes / 1048576, 2) . ' MB';
        } elseif ($bytes >= 1024) {
            $bytes = number_format($bytes / 1024, 2) . ' KB';
        } elseif ($bytes > 1) {
            $bytes = $bytes . ' bytes';
        } elseif ($bytes == 1) {
            $bytes = $bytes . ' byte';
        } else {
            $bytes = '0 bytes';
        }

        return $bytes;
    }

    private function GetDirectorySize($path)
    {
        $bytestotal = 0;
        $path = realpath($path);
        if ($path!==false) {
            foreach (new RecursiveIteratorIterator(new RecursiveDirectoryIterator($path, FilesystemIterator::SKIP_DOTS)) as $object) {
                $bytestotal += $object->getSize();
            }
        }
        return $bytestotal;
    }

    public function compose($view)
    {
        $this->exportDiskCost = $this->GetDirectorySize(storage_path().'/user_exports/');

        $diskPercentage = $this->exportDiskCost / $this->maximumDesiredDiskCost;
        $diskPercentage = $diskPercentage * 100;

        $costPerGB = ($this->monthlyCostPerByte * 1000000000);

        $view->with('diskCostNice', $this->formatSizeUnits($this->exportDiskCost));
        $view->with('maxDiskCostNice', $this->formatSizeUnits($this->maximumDesiredDiskCost));
        $view->with('maxDiskCost', $this->maximumDesiredDiskCost);
        $view->with('currentDiskCost', $this->exportDiskCost);
        $view->with('diskPercentage', $diskPercentage);
        $view->with('costPerGB', $costPerGB);
        $view->with('diskCostMonthly', (($this->exportDiskCost * $this->monthlyCostPerByte)));
    }
}
