<?php namespace ParadoxOne\NDCounties\UI\Composers;

use ParadoxOne\NDCounties\UI\SettingsMenuRenderer;
use Stillat\Menu\Support\Facades\Menu;

class DataToolsMenuComposer
{
    public function compose($view)
    {
        $generalSettings = Menu::handle('data_tools_general', 'General Settings', ['class' => 'list-unstyled settings-menu']);
        $generalSettings->setRenderer(new SettingsMenuRenderer());
        $generalSettings->addItem(url('data-tools/backup-and-restore'), 'Backup &amp; Restore Data');
        $generalSettings->addItem(url('data-tools/export'), 'Export Data');
        $generalSettings->addItem(url('data-tools/reassign-records'), 'Reassign Records');
    }
}
