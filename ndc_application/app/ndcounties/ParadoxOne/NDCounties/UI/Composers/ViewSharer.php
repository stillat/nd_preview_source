<?php namespace ParadoxOne\NDCounties\UI\Composers;

use Auth;
use Cache;
use ParadoxOne\NDCounties\Contracts\AccountRepositoryInterface;
use ParadoxOne\NDCounties\UI\AvatarPresenter;
use View;

class ViewSharer
{
    private $accountRepository;

    public function __construct(AccountRepositoryInterface $accountRepository)
    {
        $this->accountRepository = $accountRepository;
    }

    public function share()
    {
        $currentAccount = null;

        if (Auth::check()) {
            $lastTenant = Auth::user()->last_tenant;

            $currentAccount = $this->accountRepository->getAccount($lastTenant);

            unset($accountRepo);
            unset($lastTenant);
        }

        View::share('currentAccount', $currentAccount);
        View::share('authUser', Auth::user());

        if (Auth::check()) {
            $apo = new AvatarPresenter;
            View::share('avatarPresenter', $apo);
            View::share('userProfileImage', $apo->getAvatar(Auth::user()->id, Auth::user()->email, 2));
        }

        // This handles the "Choose another account" condition

        $showChooseAccount = false;

        if (Auth::check()) {
            if (Auth::user()->admin == true) {
                $showChooseAccount = true;
            } else {
                $accountRepo = $this->accountRepository;

                $showChooseAccount =
                    Cache::remember(Auth::user()->id . '_showChooseAccount', 10, function () use ($accountRepo) {
                        $logicalAccount = $accountRepo->getLogicalPrimaryUserAccount(Auth::user()->id);


                        if ($logicalAccount == null) {
                            return false;
                        } else {
                            if ($logicalAccount->total_service_accounts <= 1) {
                                return false;
                            } else {
                                return true;
                            }
                        }
                    });
            }
        }


        View::share('showChooseAccount', $showChooseAccount);
    }
}
