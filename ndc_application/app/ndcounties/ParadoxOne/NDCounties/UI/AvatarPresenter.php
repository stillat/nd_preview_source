<?php namespace ParadoxOne\NDCounties\UI;

use Exception;
use Illuminate\Support\Facades\Input;
use Image;
use Illuminate\Support\Facades\Validator;

class AvatarPresenter
{
    protected $hash = '';

    protected $avatarPath = '';

    protected $publicURL = '';

    protected $avatarSize = 64;

    const MODE_DATA_URL = 0;
    const MODE_IMG_TAG  = 1;
    const MODE_URL_ONLY = 2;

    public function __construct()
    {
        $this->avatarPath = public_path() . '/usedat/uimg/';
        $this->publicURL  = app_url() . '/usedat/uimg/';
    }

    public function setSize($size)
    {
        if (!is_int($size)) {
            throw new Exception('Invalid argument. Size must be an integer');
        }

        $this->avatarSize = $size;
    }

    private function constructHash($userID, $emailAddress)
    {
        return $this->hash = md5(md5($userID) . 'ndc_salt' . md5($userID + 1 + $userID));
    }

    private function constructFileHash($userID, $emailAddress)
    {
        return md5($this->constructHash($userID, $emailAddress));
    }

    public function removeProfileImage($userID, $emailAddress)
    {
        $basePath = $this->avatarPath.$this->constructFileHash($userID, $emailAddress).'.png';
        $base311Path = $this->avatarPath.$this->constructFileHash($userID, $emailAddress).'_311.png';

        try {
            @unlink($base311Path);
            @unlink($basePath);

            return true;
        } catch (Exception $e) {
        }

        return false;
    }


    private function avatarExists($userID, $emailAddress)
    {
        return file_exists($this->avatarPath . $this->constructFileHash($userID, $emailAddress) . '.png');
    }

    private function writeAvatar($userID, $emailAddress)
    {
        $avatarFilePath = $this->avatarPath . $this->constructFileHash($userID, $emailAddress) . '.png';
        $avatarFilePath311 = $this->avatarPath . $this->constructFileHash($userID, $emailAddress) . '_311.png';
        $identicon      = new Identicon;
        $identicon->generateAvatarToFile($this->constructHash($userID, $emailAddress), $this->avatarSize, null,
                                         $avatarFilePath);
        $identicon->generateAvatarToFile($this->constructHash($userID, $emailAddress), 311, null,
                                         $avatarFilePath311);
        unset($identicon);
    }

    public function createProfileImageFromInput($input, $userID, $email)
    {
        $validator = Validator::make(array('photo' => Input::file($input)), array(
            'photo' => 'mimes:jpeg,jpg,bmp,png'
        ));

        if ($validator->fails()) {
            return false;
        }


        $thumbImage = Image::make(Input::file($input))->resize($this->avatarSize, $this->avatarSize);
        $thumbImage->save($this->avatarPath.$this->constructFileHash($userID, $email).'.png');
        $largeImage = Image::make(Input::file($input))->resize(311, 311);
        $largeImage->save($this->avatarPath.$this->constructFileHash($userID, $email).'_311.png');
        $thumbImage = null;
        $largeImage     = null;
        unset($thumbImage);
        unset($largeImage);

        return true;
    }

    public function getAvatar($userID, $emailAddress, $mode = 1, $size = '')
    {
        if (!$this->avatarExists($userID, $emailAddress)) {
            $this->writeAvatar($userID, $emailAddress);
        }

        $avatarName = $this->constructFileHash($userID, $emailAddress) . $size.'.png?cb='.rand(0, 1);

        switch ($mode) {
            case self::MODE_DATA_URL:
                $identicon = new Identicon;

                return $identicon->getImageDataUri($this->constructHash($userID, $emailAddress), $this->avatarSize,
                                                   null);
                break;
            case self::MODE_URL_ONLY:
                return $this->publicURL . $avatarName;
                break;
            case self::MODE_IMG_TAG:
                return '<img src="' . $this->publicURL . $avatarName . '" alt="">';
                break;
        }
    }
}
