<?php namespace ParadoxOne\NDCounties\UI;

use Config;
use Illuminate\Support\Facades\App;
use Menu;
use Session;

class UIBuilder
{
    public static $version = '?v=14.8.0';

    public static function correctMenu()
    {
        MenuCorrections::correct();
    }

    public static $cssFiles = array(

        'application.min.css',

    );

    public static $jsFiles = array(

        'min/ndcounties.min.js'

    );

    public static $dynFiles = array(

        'angular.min.js',
        'ui.js',
        'nd.js',

    );

    public static function getBuster()
    {
        return self::$version;
    }

    public static function formatSizeUnits($bytes)
    {
        if ($bytes >= 1073741824) {
            $bytes = number_format($bytes / 1073741824, 2) . ' GB';
        } elseif ($bytes >= 1048576) {
            $bytes = number_format($bytes / 1048576, 2) . ' MB';
        } elseif ($bytes >= 1024) {
            $bytes = number_format($bytes / 1024, 2) . ' KB';
        } elseif ($bytes > 1) {
            $bytes = $bytes . ' bytes';
        } elseif ($bytes == 1) {
            $bytes = $bytes . ' byte';
        } else {
            $bytes = '0 bytes';
        }

        return $bytes;
    }

    public static function enqueueScript($script)
    {
        self::$jsFiles[] = $script;
    }

    public static function getCss()
    {
        $returnCss = '';

        $cacheBuster = self::$version;

        foreach (self::$cssFiles as $file) {
            $returnCss .= '<link href="' . Config::get('app.url') . '/assets/css/' . $file . $cacheBuster .

                '" rel="stylesheet" media="all">';
        }

        $returnCss .= '<link href="' . Config::get('app.url') .

            '/assets/css/print.css' . $cacheBuster . '" rel="stylesheet" media="print">';

        return $returnCss;
    }

    public static function getJs()
    {
        $returnJs = '';

        $cacheBuster = self::$version;

        foreach (self::$jsFiles as $file) {
            $returnJs .= '<script src="' . Config::get('app.url') . '/assets/js/' . $file . $cacheBuster .

                '" defer></script>';
        }

        return $returnJs;
    }

    public static function getDyn()
    {
        $returnJs = '';

        foreach (self::$dynFiles as $file) {
            $returnJs .= '<script src="' . Config::get('app.url') . '/assets/dyn/' . $file . '"></script>';
        }

        return $returnJs;
    }

    public static function ngScript($scriptName)
    {
        return '<script src="' . Config::get('app.url') . '/assets/dyn/' . $scriptName . '"></script>';
    }

    public static function ngController($controllerName)
    {
        return self::ngScript('controllers/' . $controllerName . '.js');
    }

    public static function getBreadcrumbs()
    {
        return strip_tags(Menu::handler('main')->breadcrumbs(function ($itemLists) {

            return $itemLists[0]; // returns first match

        })->setElement('ol')->addClass('breadcrumb')->render(), '<ol><li><a><span>');
    }

    public static function flashMessage($content, $title = '', $style = 'info', $additionalInformation = '')
    {
        if (strlen($content) > 0) {
            Session::flash('flash_message', $content);

            if (strlen($style) > 0) {
                Session::flash('flash_message_style', $style);
            }

            if (strlen($additionalInformation) > 0) {
                Session::flash('flash_message_info', $additionalInformation);
            }

            if (strlen($title) > 0) {
                Session::flash('flash_message_title', $title);
            }
        }
    }

    public static function success($content, $title = '', $additionalInformation = '')
    {
        return self::flashMessage($content, $title, 'success', $additionalInformation);
    }

    public static function info($content, $title = '', $additionalInformation = '')
    {
        return self::flashMessage($content, $title, 'info', $additionalInformation);
    }

    public static function warning($content, $title = '', $additionalInformation = '')
    {
        return self::flashMessage($content, $title, 'warning', $additionalInformation);
    }

    public static function danger($content, $title = '', $additionalInformation = '')
    {
        return self::flashMessage($content, $title, 'danger', $additionalInformation);
    }

    public static function inputErrors()
    {
        return self::danger('There were problems saving your form. Check your input and try again.',

            'There were input errors');
    }
}
