<?php namespace ParadoxOne\NDCounties\UI\Streams;

use ParadoxOne\NDCounties\Contracts\StreamBuilderInterface;

abstract class AbstractStream implements StreamBuilderInterface
{
    protected $streamData = null;

    /**
     * Sets the streams data.
     *
     * @param $data
     * @return mixed
     */
    public function setStreamData($data)
    {
        $this->streamData = $data;
    }

    /**
     * Gets the streams data.
     *
     * @return mixed
     */
    public function getStreamData()
    {
        return $this->streamData;
    }

    /**
     * Alias of getStream();
     *
     * @return mixed
     */
    public function __toString()
    {
        return $this->getStream();
    }
}
