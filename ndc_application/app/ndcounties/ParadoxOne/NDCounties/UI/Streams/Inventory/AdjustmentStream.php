<?php namespace ParadoxOne\NDCounties\UI\Streams\Inventory;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\View;
use ParadoxOne\NDCounties\Contracts\UserRepositoryInterface;
use ParadoxOne\NDCounties\UI\Streams\AbstractStream;

class AdjustmentStream extends AbstractStream
{
    protected $userRepository;

    protected $avatarPresenter;

    protected $usersList = [];

    public function __construct(UserRepositoryInterface $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    private function buildUsersList()
    {
        $findUsers       = array_pluck_unique($this->streamData, 'adjusted_by');
        $this->usersList = $this->userRepository->getUsersIn($findUsers);
    }

    private function generateDescription($item, $user)
    {
        $username = '<strong>' . e($user->first_name . ' ' . $user->last_name) . '</strong>';
        $username = str_limit($username, 20);

        switch ($item->adjustment_context) {
            case 0:
                switch ($item->adjustment_value) {
                    case 1:
                        return '<strong>ND Counties</strong> converted an old work record\'s usage with the amount ' .
                               nd_number_format($item->adjustment_amount);
                        break;
                    default:
                        return $username . ' created the consumable with an initial inventory level of ' .
                               nd_number_format($item->adjustment_amount);
                        break;
                }
                break;
            case 1:
                return $username . ' manually adjusted the consumable inventory amount with an adjustment of ' .
                       nd_number_format($item->adjustment_amount);
                break;
            case 2:
                return $username.' adjusted the inventory amount by '.nd_number_format($item->adjustment_amount).' on work record <a style="display: inline;" href="'.action('App\Work\WorkLogsController@show', $item->adjustment_value).'">#'.$item->adjustment_value.'</a>.';
                break;
            default:
                return '';
                break;
        }
    }

    /**
     * Gets the streams string representation.
     *
     * @return mixed
     */
    public function getStream()
    {
        $this->buildUsersList();

        $completeStream = '';

        foreach ($this->streamData as $dataObject) {
            $completeStream .= $this->formatItem($dataObject);
        }

        return $completeStream;
    }

    /**
     * Formats an item of the stream.
     *
     * @param $item
     *
     * @return mixed
     */
    public function formatItem($item)
    {
        $user = array_pluck_where($this->usersList, 'id', $item->adjusted_by);

        return View::make('streams.inventory.adjustment')->with('adjustment', $item)->with('user', $user[0])
                   ->with('description', $this->generateDescription($item, $user[0]))->render();
    }
}
