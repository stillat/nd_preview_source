<?php namespace ParadoxOne\NDCounties\UI\Loaders;

use Illuminate\Support\Facades\HTML;

class BackupReportLoader
{
    public function __construct()
    {
        HTML::macro('remainingLabel', function ($remaining) {
            $class = '';

            if ($remaining <= 0) {
                $class = 'default';
            } else {
                $class = 'primary';
            }

            return '<span class="label label-' . $class . '">' . e($remaining) . ' Remaining</span>';
        });

        HTML::macro('availableLabel', function ($status) {
            if ($status) {
                return '<span class="label label-success">Available</span>';
            }


            return '<span class="label label-default">Unavailable</span>';
        });
    }
}
