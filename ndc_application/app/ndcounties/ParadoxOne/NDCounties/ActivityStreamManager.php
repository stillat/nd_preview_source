<?php namespace ParadoxOne\NDCounties;

use Carbon\Carbon;
use Illuminate\Events\Dispatcher;
use Illuminate\Foundation\Application;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\ServiceProvider;
use ParadoxOne\NDCounties\Contracts\ActivityStreamRepositoryInterface;

class ActivityStreamManager extends ServiceProvider
{
    const ACTION_WORK_RECORD = 0;
    const ACTION_ACTIVITY = 1;
    const ACTION_DEPARTMENT = 2;
    const ACTION_DISTRICT = 3;
    const ACTION_PROJECT = 4;
    const ACTION_EMPLOYEE = 5;
    const ACTION_FUELS = 6;
    const ACTION_MATERIALS = 7;
    const ACTION_PROPERTY = 8;
    const ACTION_ROAD_SURFACE_TYPE = 9;
    const ACTION_PROPERTY_TYPE = 10;
    const ACTION_SETTING_CONTACT_INFORMATION = 11;
    const ACTION_SETTING_INVENTORY_MANAGEMENT = 12;
    const ACTION_SETTING_SYSTEM_BEHAVIOR = 13;
    const ACTION_SETTING_REPORT_BREAKDOWN = 14;
    const ACTION_SETTING_REPORT_DESCRIPTION = 15;
    const ACTION_SETTING_REPORT_DISPLAY = 16;
    const ACTION_SETTING_REPORT_PRINT = 17;
    const ACTION_SETTING_USER_ACCESSIBILITY = 18;
    const ACTION_SETTING_USER_CONTACT = 19;
    const ACTION_SETTING_USER_PASSWORD = 20;
    const ACTION_SETTING_USER_RESET_SORTING = 21;
    const ACTION_TAX_RATES = 22;
    const ACTION_FINANCE_ACCOUNT = 23;
    const ACTION_FINANCE_ACCOUNT_CATEGORIES = 24;

    /**
     * @var ActivityStreamRepositoryInterface
     */
    private $activityStream;

    /**
     * @var Dispatcher
     */
    private $eventDispatcher;

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->activityStream = $this->app->make('ParadoxOne\NDCounties\Contracts\ActivityStreamRepositoryInterface');
        $this->eventDispatcher = $this->app->make('Illuminate\Events\Dispatcher');

        $this->registerActivityListeners();
        $this->registerDepartmentListeners();
        $this->registerDistrictListeners();
        $this->registerProjectListeners();
        $this->registerEmployeeListeners();
        $this->registerFuelListeners();
        $this->registerMaterialListeners();
        $this->registerPropertyListeners();
        $this->registerWorkRecordListeners();
        $this->registerTaxRateListeners();
        $this->registerFinanceAccountListeners();
        $this->registerSettingsListeners();
    }

    /**
     * A helper function to log a create audit.
     *
     * @param integer $actionContext
     * @param integer $actionClass
     * @param mixed $actionMeta
     */
    private function logCreate($actionContext, $actionClass, $actionMeta)
    {
        $this->activityStream->logActivity(Auth::user()->id, Auth::user()->last_tenant, 1, $actionClass, $actionContext,
            $actionMeta, (new Carbon())->toDateString());
    }

    /**
     * A helper function to log a delete audit.
     *
     * @param integer $actionContext
     * @param integer $actionClass
     * @param mixed $actionMeta
     */
    private function logRemove($actionContext, $actionClass, $actionMeta)
    {
        $this->activityStream->logActivity(Auth::user()->id, Auth::user()->last_tenant, 3, $actionClass, $actionContext,
            $actionMeta, (new Carbon())->toDateString());
    }

    /**
     * A helper function to log an update audit.
     *
     * @param integer $actionContext
     * @param integer $actionClass
     * @param mixed $actionMeta
     */
    private function logUpdate($actionContext, $actionClass, $actionMeta)
    {
        $this->activityStream->logActivity(Auth::user()->id, Auth::user()->last_tenant, 2, $actionClass, $actionContext,
            $actionMeta, (new Carbon())->toDateString());
    }

    /**
     * A helper function to log a general audit.
     *
     * @param integer $actionContext
     * @param integer $actionClass
     * @param mixed $actionMeta
     */
    private function logGeneral($actionContext, $actionClass, $actionMeta)
    {
        $this->activityStream->logActivity(Auth::user()->id, Auth::user()->last_tenant, 0, $actionClass, $actionContext,
            $actionMeta, (new Carbon())->toDateString());
    }


    protected function registerActivityListeners()
    {
        $this->eventDispatcher->listen('structure.activity.removed', function ($activityID) {
            $this->logRemove(self::ACTION_ACTIVITY, $activityID, '');
        });

        $this->eventDispatcher->listen('structure.activity.created', function ($activity) {
            $this->logCreate(self::ACTION_ACTIVITY, $activity->id, '');
        });

        $this->eventDispatcher->listen('structure.activity.updated', function ($activity) {
            $this->logUpdate(self::ACTION_ACTIVITY, $activity->id, '');
        });
    }

    protected function registerDepartmentListeners()
    {
        $this->eventDispatcher->listen('structure.department.removed', function ($departmentID) {
            $this->logRemove(self::ACTION_DEPARTMENT, $departmentID, '');
        });

        $this->eventDispatcher->listen('structure.department.created', function ($department) {
            $this->logCreate(self::ACTION_DEPARTMENT, $department->id, '');
        });

        $this->eventDispatcher->listen('structure.department.updated', function ($department) {
            $this->logUpdate(self::ACTION_DEPARTMENT, $department->id, '');
        });
    }

    protected function registerDistrictListeners()
    {
        $this->eventDispatcher->listen('structure.district.removed', function ($districtID) {
            $this->logRemove(self::ACTION_DISTRICT, $districtID, '');
        });

        $this->eventDispatcher->listen('structure.district.created', function ($district) {
            $this->logCreate(self::ACTION_DISTRICT, $district->id, '');
        });

        $this->eventDispatcher->listen('structure.district.updated', function ($district) {
            $this->logUpdate(self::ACTION_DISTRICT, $district->id, '');
        });
    }

    protected function registerProjectListeners()
    {
        $this->eventDispatcher->listen('project.removed', function ($projectID) {
            $this->logRemove(self::ACTION_PROJECT, $projectID, '');
        });

        $this->eventDispatcher->listen('project.created', function ($project) {
            $this->logCreate(self::ACTION_PROJECT, $project->id, '');
        });

        $this->eventDispatcher->listen('project.updated', function ($project) {
            $this->logUpdate(self::ACTION_PROJECT, $project->id, '');
        });
    }

    protected function registerEmployeeListeners()
    {
        $this->eventDispatcher->listen('employee.removed', function ($employeeID) {
            $this->logRemove(self::ACTION_EMPLOYEE, $employeeID, '');
        });

        $this->eventDispatcher->listen('employee.created', function ($employee) {
            $this->logCreate(self::ACTION_EMPLOYEE, $employee->id, '');
        });

        $this->eventDispatcher->listen('employee.updated', function ($employee) {
            $this->logUpdate(self::ACTION_EMPLOYEE, $employee->id, '');
        });
    }

    protected function registerFuelListeners()
    {
        $this->eventDispatcher->listen('fuel.removed', function ($fuelID) {
            $this->logRemove(self::ACTION_FUELS, $fuelID, '');
        });

        $this->eventDispatcher->listen('fuel.created', function ($fuel) {
            $this->logCreate(self::ACTION_FUELS, $fuel->id, '');
        });

        $this->eventDispatcher->listen('fuel.updated', function ($fuel) {
            $this->logUpdate(self::ACTION_FUELS, $fuel->id, '');
        });
    }

    protected function registerMaterialListeners()
    {
        $this->eventDispatcher->listen('material.removed', function ($materialID) {
            $this->logRemove(self::ACTION_MATERIALS, $materialID, '');
        });

        $this->eventDispatcher->listen('material.created', function ($material) {
            $this->logCreate(self::ACTION_MATERIALS, $material->id, '');
        });

        $this->eventDispatcher->listen('material.updated', function ($material) {
            $this->logUpdate(self::ACTION_MATERIALS, $material->id, '');
        });
    }

    protected function registerPropertyListeners()
    {
        // PROPERTIES

        $this->eventDispatcher->listen('property.removed', function ($propertyID) {
            $this->logRemove(self::ACTION_PROPERTY, $propertyID, '');
        });

        $this->eventDispatcher->listen('property.created', function ($propertyID) {
            $this->logCreate(self::ACTION_PROPERTY, $propertyID->id, '');
        });

        $this->eventDispatcher->listen('property.updated', function ($propertyID) {
            $this->logUpdate(self::ACTION_PROPERTY, $propertyID->id, '');
        });

        // PROPERTY TYPES

        $this->eventDispatcher->listen('property.type.removed', function ($propertyTypeID) {
            $this->logRemove(self::ACTION_PROPERTY_TYPE, $propertyTypeID, '');
        });

        $this->eventDispatcher->listen('property.type.created', function ($propertyType) {
            $this->logCreate(self::ACTION_PROPERTY_TYPE, $propertyType->id, '');
        });

        $this->eventDispatcher->listen('property.type.updated', function ($propertyType) {
            $this->logUpdate(self::ACTION_PROPERTY_TYPE, $propertyType->id, '');
        });

        // ROAD SURFACE TYPES.

        $this->eventDispatcher->listen('property.road.surfacetype.removed', function ($surfaceTypeID) {
            $this->logRemove(self::ACTION_ROAD_SURFACE_TYPE, $surfaceTypeID, '');
        });

        $this->eventDispatcher->listen('property.road.surfacetype.created', function ($surfaceType) {
            $this->logCreate(self::ACTION_ROAD_SURFACE_TYPE, $surfaceType->id, '');
        });

        $this->eventDispatcher->listen('property.road.surfacetype.updated', function ($surfaceType) {
            $this->logUpdate(self::ACTION_ROAD_SURFACE_TYPE, $surfaceType->id, '');
        });
    }

    protected function registerWorkRecordListeners()
    {
        $this->eventDispatcher->listen('workrecord.created', function ($workRecordID) {
            $this->logCreate(self::ACTION_WORK_RECORD, $workRecordID, '');
        });

        $this->eventDispatcher->listen('workrecord.updated', function ($workRecordID) {
            $this->logUpdate(self::ACTION_WORK_RECORD, $workRecordID, '');
        });

        $this->eventDispatcher->listen('workrecord.removed', function ($workRecord) {
            foreach ($workRecord as $record) {
                $this->logRemove(self::ACTION_WORK_RECORD, $record, '');
            }
        });

        $this->eventDispatcher->listen('batch.dates', function ($newDate, $records) {

            $streamRecords = [];
            $when = (new Carbon())->toDateTimeString();
            foreach ($records as $record) {
                $streamRecords[] = [
                    Auth::user()->id,
                    Auth::user()->last_tenant,
                    2,
                    $record,
                    26,
                    '',
                    $when
                ];
            }
            $this->activityStream->logActivity($streamRecords);
        });

        $this->eventDispatcher->listen('batch.activity', function ($newDate, $records) {

            $streamRecords = [];
            $when = (new Carbon())->toDateTimeString();
            foreach ($records as $record) {
                $streamRecords[] = [
                    Auth::user()->id,
                    Auth::user()->last_tenant,
                    2,
                    $record,
                    27,
                    '',
                    $when
                ];
            }
            $this->activityStream->logActivity($streamRecords);
        });

        $this->eventDispatcher->listen('batch.road', function ($newDate, $records) {

            $streamRecords = [];
            $when = (new Carbon())->toDateTimeString();
            foreach ($records as $record) {
                $streamRecords[] = [
                    Auth::user()->id,
                    Auth::user()->last_tenant,
                    2,
                    $record,
                    28,
                    '',
                    $when
                ];
            }
            $this->activityStream->logActivity($streamRecords);
        });

        $this->eventDispatcher->listen('batch.project', function ($newDate, $records) {

            $streamRecords = [];
            $when = (new Carbon())->toDateTimeString();
            foreach ($records as $record) {
                $streamRecords[] = [
                    Auth::user()->id,
                    Auth::user()->last_tenant,
                    2,
                    $record,
                    29,
                    '',
                    $when
                ];
            }
            $this->activityStream->logActivity($streamRecords);
        });

        $this->eventDispatcher->listen('batch.district', function ($newDate, $records) {

            $streamRecords = [];
            $when = (new Carbon())->toDateTimeString();
            foreach ($records as $record) {
                $streamRecords[] = [
                    Auth::user()->id,
                    Auth::user()->last_tenant,
                    2,
                    $record,
                    30,
                    '',
                    $when
                ];
            }
            $this->activityStream->logActivity($streamRecords);
        });
        $this->eventDispatcher->listen('batch.department', function ($newDate, $records) {

            $streamRecords = [];
            $when = (new Carbon())->toDateTimeString();
            foreach ($records as $record) {
                $streamRecords[] = [
                    Auth::user()->id,
                    Auth::user()->last_tenant,
                    2,
                    $record,
                    31,
                    '',
                    $when
                ];
            }
            $this->activityStream->logActivity($streamRecords);
        });
    }

    protected function registerTaxRateListeners()
    {
        $this->eventDispatcher->listen('finance.taxrate.removed', function ($taxRateID) {
            $this->logRemove(self::ACTION_TAX_RATES, $taxRateID, '');
        });

        $this->eventDispatcher->listen('finance.taxrate.created', function ($taxRate) {
            $this->logCreate(self::ACTION_TAX_RATES, $taxRate->id, '');
        });

        $this->eventDispatcher->listen('finance.taxrate.updated', function ($taxRate) {
            $this->logUpdate(self::ACTION_TAX_RATES, $taxRate->id, '');
        });
    }

    protected function registerFinanceAccountListeners()
    {
        // Accounts.

        $this->eventDispatcher->listen('finance.account.removed', function ($accountID) {
            $this->logRemove(self::ACTION_FINANCE_ACCOUNT, $accountID, '');
        });

        $this->eventDispatcher->listen('finance.account.created', function ($account) {
            $this->logCreate(self::ACTION_FINANCE_ACCOUNT, $account->id, '');
        });

        $this->eventDispatcher->listen('finance.account.updated', function ($account) {
            $this->logUpdate(self::ACTION_FINANCE_ACCOUNT, $account->id, '');
        });

        // Account categories.

        $this->eventDispatcher->listen('finance.account.category.removed', function ($categoryID) {
            $this->logRemove(self::ACTION_FINANCE_ACCOUNT_CATEGORIES, $categoryID, '');
        });

        $this->eventDispatcher->listen('finance.account.category.created', function ($accountCategory) {
            $this->logCreate(self::ACTION_FINANCE_ACCOUNT_CATEGORIES, $accountCategory->id, '');
        });

        $this->eventDispatcher->listen('finance.account.category.updated', function ($accountCategory) {
            $this->logUpdate(self::ACTION_FINANCE_ACCOUNT_CATEGORIES, $accountCategory->id, '');
        });
    }

    protected function registerSettingsListeners()
    {
        // Account wide settings.

        $this->eventDispatcher->listen('settings.account.inventory.updated', function () {
            $this->logUpdate(self::ACTION_SETTING_INVENTORY_MANAGEMENT, 0, '');
        });
        $this->eventDispatcher->listen('settings.account.behavior.updated', function () {
            $this->logUpdate(self::ACTION_SETTING_SYSTEM_BEHAVIOR, 0, '');
        });
        $this->eventDispatcher->listen('settings.account.contact.updated', function () {
            $this->logUpdate(self::ACTION_SETTING_CONTACT_INFORMATION, 0, '');
        });

        // Report settings.
        $this->eventDispatcher->listen('settings.report.print.updated', function () {
            $this->logUpdate(self::ACTION_SETTING_REPORT_PRINT, 0, '');
        });
        $this->eventDispatcher->listen('settings.report.display.updated', function () {
            $this->logUpdate(self::ACTION_SETTING_REPORT_DISPLAY, 0, '');
        });
        $this->eventDispatcher->listen('settings.report.description.updated', function () {
            $this->logUpdate(self::ACTION_SETTING_REPORT_DESCRIPTION, 0, '');
        });
        $this->eventDispatcher->listen('settings.report.breakdown.updated', function () {
            $this->logUpdate(self::ACTION_SETTING_REPORT_BREAKDOWN, 0, '');
        });

        // User Settings.
        $this->eventDispatcher->listen('settings.user.accessibility.updated', function () {
            $this->logUpdate(self::ACTION_SETTING_USER_ACCESSIBILITY, 0, '');
        });
        $this->eventDispatcher->listen('settings.user.contact.updated', function () {
            $this->logUpdate(self::ACTION_SETTING_USER_CONTACT, 0, '');
        });
        $this->eventDispatcher->listen('settings.user.password.updated', function () {
            $this->logUpdate(self::ACTION_SETTING_USER_PASSWORD, 0, '');
        });
        $this->eventDispatcher->listen('settings.user.sorting.deleted', function () {
            $this->logRemove(self::ACTION_SETTING_USER_RESET_SORTING, 0, '');
        });
    }
}
