<?php namespace ParadoxOne\NDCounties\Database;

/**
 * Adapted from http://jamieonsoftware.com/post/77069282269/eager-loading-eloquent-polymorphic-associations
 */
class PolymorphicLoader
{
    /**
     * The associations
     *
     * @var array
     */
    protected $associations = array();

    public function __construct(array $associations)
    {
        $this->associations = $associations;
    }

    public function fetch($models)
    {
        $relationships = array();
        $objects       = array();

        $typeCol = "{$this->associations}_type";
        $idCol   = "{$this->associations}_type";

        foreach ($models as $model) {
            $relationships[$model->$typeCol][] = $model->$idCol;
        }

        foreach ($relationships as $class => $ids) {
            $objects[$class] = $class::findMany($ids);
        }

        foreach ($models as $model) {
            $model->setRelation($this->associations, $objects[$model->$typeCol]->find($model->$idCol));
        }

        return $models;
    }
}
