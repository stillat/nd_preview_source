<?php namespace ParadoxOne\NDCounties\Database;

use Illuminate\Support\ServiceProvider;

class InventoryServiceProvider extends ServiceProvider
{
    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('inventory.manager.create', function ($app) {
            return $app->make('\ParadoxOne\NDCounties\Database\Repositories\WorkEntries\InventoryManagers\CreateWorkInventoryManager');
        });

        $this->app->singleton('inventory.manager.destroyer', function ($app) {
            return $app->make('\ParadoxOne\NDCounties\Database\Repositories\WorkEntries\InventoryManagers\DeleteWorkInventoryManager');
        });
    }
}
