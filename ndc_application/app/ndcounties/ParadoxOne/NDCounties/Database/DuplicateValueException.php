<?php namespace ParadoxOne\NDCounties\Database;

use Stillat\Database\Exceptions\DatabaseException;

class DuplicateValueException extends DatabaseException
{
}
