<?php namespace ParadoxOne\NDCounties\Database;

use Illuminate\Support\Facades\App;
use Illuminate\Support\ServiceProvider;

class WorkEntryServiceProvider extends ServiceProvider
{
    protected $defer = false;

    protected $newableClassMap = array(
        'ParadoxOne\NDCounties\Contracts\Unions\UnionConsumableEquipmentUnitInterface' => '\ParadoxOne\NDCounties\Database\Entities\WorkEntries\ConsumableEquipmentEntity',
        'ParadoxOne\NDCounties\Contracts\Unions\UnionConsumableFuelInterface'          => '\ParadoxOne\NDCounties\Database\Entities\WorkEntries\ConsumableFuelEntity',
        'ParadoxOne\NDCounties\Contracts\Unions\UnionConsumableMaterialInterface'      => '\ParadoxOne\NDCounties\Database\Entities\WorkEntries\ConsumableMaterialEntity',
    );

    protected $repositoryClassMap = array(
        'ParadoxOne\NDCounties\Contracts\Resolvers\ConsumableTypeResolverInterface'         => '\ParadoxOne\NDCounties\System\ConsumableTypeResolver',
        'ParadoxOne\NDCounties\Contracts\WorkEntries\ConsumableWriterRepositoryInterface'   => '\ParadoxOne\NDCounties\Database\Repositories\WorkEntries\EloquentConsumableWriterRepository',
        'ParadoxOne\NDCounties\Contracts\WorkEntries\WorkEntryReaderInterface'              => '\ParadoxOne\NDCounties\Database\Repositories\WorkEntries\Readers\DatabaseWorkEntryReader',
        'ParadoxOne\NDCounties\Contracts\WorkEntries\WorkEntryWriterRepositoryInterface'    => '\ParadoxOne\NDCounties\Database\Repositories\WorkEntries\EloquentWorkEntryWriterRepository',
        'ParadoxOne\NDCounties\Contracts\WorkEntries\WorkEntryUpdaterRepositoryInterface'   => '\ParadoxOne\NDCounties\Database\Repositories\WorkEntries\Updaters\DatabaseWorkEntryUpdater',
        'ParadoxOne\NDCounties\Contracts\Unions\UnionPropertyHandlerEquipmentUnitInterface' => '\ParadoxOne\NDCounties\Database\Entities\WorkEntries\Handlers\EquipmentPropertyHandler',
        'ParadoxOne\NDCounties\Contracts\EquipmentUnits\OdometerReadingRepositoryInterface' => '\ParadoxOne\NDCounties\Database\Repositories\EquipmentUnits\OdometerReadingRepository',
        'ParadoxOne\NDCounties\Contracts\WorkEntries\WorkEntryReaderRepositoryInterface'    => '\ParadoxOne\NDCounties\Database\Repositories\WorkEntries\Builders\WorkEntryQueryBuilder',
        'ParadoxOne\NDCounties\Contracts\WorkEntries\WorkEntryDestroyerInterface'           => '\ParadoxOne\NDCounties\Database\Repositories\WorkEntries\Destroyers\DatabaseWorkEntryDestroyer',
        'ParadoxOne\NDCounties\Contracts\Properties\Roads\WorkRecordRepositoryInterface'    => '\ParadoxOne\NDCounties\Database\Repositories\Properties\Roads\WorkRecordRepository',
        'ParadoxOne\NDCounties\Contracts\WorkEntries\WorkEntryVisibilityRepositoryInterface' => '\ParadoxOne\NDCounties\Database\Repositories\WorkEntries\WorkEntryVisibilityRepository',
        'ParadoxOne\NDCounties\Contracts\WorkEntries\WorkEntryBatchUpdateRepositoryInterface' => '\ParadoxOne\NDCounties\Database\Repositories\WorkEntries\WorkEntryBatchUpdaterRepository',
    );

    public function register()
    {
        foreach ($this->newableClassMap as $contract => $implementation) {
            $this->app->bind($contract, $implementation);
        }

        foreach ($this->repositoryClassMap as $repository => $implementation) {
            $this->app->singleton($repository, function () use ($implementation) {
                // return new $implementation;
                return App::make($implementation);
            });
        }

        $this->app->singleton('\ParadoxOne\NDCounties\Database\Repositories\WorkEntries\BindingConsumableResolver');
    }
}
