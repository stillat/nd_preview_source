<?php namespace ParadoxOne\NDCounties\Database;

use Illuminate\Support\ServiceProvider;

class DatabaseServiceProvider extends ServiceProvider
{
    use \ParadoxOne\NDCounties\Traits\ClassMapRegisterableTrait;

    protected $defer = false;

    protected $newableClassMap = array();

    protected $repositoryClassMap = array(
        'ParadoxOne\NDCounties\Contracts\ActivityStreamRepositoryInterface'                      => 'ParadoxOne\NDCounties\Database\Repositories\ActivityStreamRepository',
        'ParadoxOne\NDCounties\Contracts\UserRepositoryInterface'                                => '\ParadoxOne\NDCounties\Database\Repositories\UserRepository',
        'ParadoxOne\NDCounties\Contracts\AccountRepositoryInterface'                             => '\ParadoxOne\NDCounties\Database\Repositories\AccountRepository',
        'ParadoxOne\NDCounties\Contracts\EmployeeRepositoryInterface'                            => '\ParadoxOne\NDCounties\Database\Repositories\EmployeeRepository',
        'ParadoxOne\NDCounties\Contracts\ActivityRepositoryInterface'                            => '\ParadoxOne\NDCounties\Database\Repositories\ActivityRepository',
        'ParadoxOne\NDCounties\Contracts\GenderRepositoryInterface'                              => '\ParadoxOne\NDCounties\FlatFile\GenderRepository',

        'ParadoxOne\NDCounties\Contracts\CustomerService\FeedbackMessagesRepositoryInterface'    => '\ParadoxOne\NDCounties\Database\Repositories\FeedbackCenter\FeedbackMessagesRepository',
        'ParadoxOne\NDCounties\Contracts\CustomerService\ArticlesRepositoryInterface'            => '\ParadoxOne\NDCounties\Database\Repositories\LearningCenter\ArticlesRepository',

        'ParadoxOne\NDCounties\Contracts\Finance\Taxes\TaxRateRepositoryInterface'               => '\ParadoxOne\NDCounties\Database\Repositories\Finance\Taxes\TaxRateRepository',
        'ParadoxOne\NDCounties\Contracts\Finance\SupplierRepositoryInterface'                    => '\ParadoxOne\NDCounties\Database\Repositories\Finance\SupplierRepository',
        'ParadoxOne\NDCounties\Contracts\Finance\AccountRepositoryInterface'                     => '\ParadoxOne\NDCounties\Database\Repositories\Finance\AccountRepository',
        'ParadoxOne\NDCounties\Contracts\Finance\AccountCategoriesRepositoryInterface'           => '\ParadoxOne\NDCounties\Database\Repositories\Finance\AccountCategoryRepository',
        'ParadoxOne\NDCounties\Contracts\Finance\TransactionRepositoryInterface'                 => '\ParadoxOne\NDCounties\Database\Repositories\Finance\TransactionRepository',
        'ParadoxOne\NDCounties\Contracts\Finance\TransactionStatusRepositoryInterface'           => '\ParadoxOne\NDCounties\FlatFile\TransactionStatusRepository',

        'ParadoxOne\NDCounties\Contracts\Finance\Invoices\InvoiceReaderRepositoryInterface'      => '\ParadoxOne\NDCounties\Database\Repositories\Finance\Invoices\InvoiceReaderRepository',
        'ParadoxOne\NDCounties\Contracts\Finance\Invoices\InvoiceWriterRepositoryInterface'      => '\ParadoxOne\NDCounties\Database\Repositories\Finance\Invoices\EloquentInvoiceWriterRepository',
        'ParadoxOne\NDCounties\Contracts\Finance\Invoices\InvoiceDestroyerRepositoryInterface'   => '\ParadoxOne\NDCounties\Database\Repositories\Finance\Invoices\EloquentInvoiceDestroyerRepository',
        'ParadoxOne\NDCounties\Contracts\Finance\Invoices\InvoiceUpdaterRepositoryInterface'     => '\ParadoxOne\NDCounties\Database\Repositories\Finance\Invoices\InvoiceUpdaterRepository',

        'ParadoxOne\NDCounties\Contracts\InventoryManagement\AdjustmentRepositoryInterface'      => '\ParadoxOne\NDCounties\Database\Repositories\InventoryManagement\AdjustmentRepository',

        'ParadoxOne\NDCounties\Contracts\UnitRepositoryInterface'                                => '\ParadoxOne\NDCounties\Database\Repositories\UnitRepository',
        'ParadoxOne\NDCounties\Contracts\DepartmentsRepositoryInterface'                         => '\ParadoxOne\NDCounties\Database\Repositories\DepartmentRepository',
        'ParadoxOne\NDCounties\Contracts\DistrictsRepositoryInterface'                           => '\ParadoxOne\NDCounties\Database\Repositories\DistrictRepository',
        'ParadoxOne\NDCounties\Contracts\Projects\ProjectRepositoryInterface'                    => '\ParadoxOne\NDCounties\Database\Repositories\Projects\ProjectRepository',
        'ParadoxOne\NDCounties\Contracts\EquipmentUnits\UnitsRepositoryInterface'                => '\ParadoxOne\NDCounties\Database\Repositories\EquipmentUnits\UnitsRepository',

        'ParadoxOne\NDCounties\Contracts\Properties\Roads\SurfaceTypeRepositoryInterface'        => '\ParadoxOne\NDCounties\Database\Repositories\Properties\Roads\SurfaceTypeRepository',
        'ParadoxOne\NDCounties\Contracts\Properties\Roads\RoadAttributesRepositoryInterface'     => '\ParadoxOne\NDCounties\Database\Repositories\Properties\Roads\RoadAttributesRepository',

        'ParadoxOne\NDCounties\Database\Repositories\Properties\Roads\RoadsWorkRecordRepository' => '\ParadoxOne\NDCounties\Database\Repositories\Properties\Roads\RoadsWOrkRecordRepository',
        'ParadoxOne\NDCounties\Contracts\EquipmentUnits\WorkRecordRepositoryInterface'           => '\ParadoxOne\NDCounties\Database\Repositories\EquipmentUnits\WorkRecordRepository',

        'ParadoxOne\NDCounties\Contracts\Properties\PropertyCategoriesRepositoryInterface'       => '\ParadoxOne\NDCounties\Database\Repositories\Properties\PropertyCategoriesRepository',

        'ParadoxOne\NDCounties\Contracts\Properties\PropertiesRepositoryInterface'               => '\ParadoxOne\NDCounties\Database\Repositories\Properties\PropertiesRepository',
        'ParadoxOne\NDCounties\Contracts\Properties\PropertyTypesRepositoryInterface'            => '\ParadoxOne\NDCounties\Database\Repositories\Properties\PropertyTypeRepository',

        'ParadoxOne\NDCounties\Contracts\Resources\FuelRepositoryInterface'                      => '\ParadoxOne\NDCounties\Database\Repositories\Resources\FuelRepository',
        'ParadoxOne\NDCounties\Contracts\Resources\MaterialRepositoryInterface'                  => '\ParadoxOne\NDCounties\Database\Repositories\Resources\MaterialRepository',
        'ParadoxOne\NDCounties\Contracts\Tracking\GPSTrackingRepositoryInterface'                => '\ParadoxOne\NDCounties\Database\Repositories\Tracking\GPSTrackingRepository',

        'ParadoxOne\NDCounties\Contracts\CountyMetaRepositoryInterface'                          => '\ParadoxOne\NDCounties\Database\Repositories\CountyMetaRepository',

        'ParadoxOne\NDCounties\Contracts\Settings\UserSettingsRepositoryInterface'               => '\ParadoxOne\NDCounties\Database\Repositories\Settings\UserSettingsRepository',
        'ParadoxOne\NDCounties\Contracts\Settings\CountyWideSettingsRepositoryInterface'         => '\ParadoxOne\NDCounties\Database\Repositories\Settings\CountySettingsRepository',

        'ParadoxOne\NDCounties\Contracts\Data\BackupRepositoryInterface'                         => '\ParadoxOne\NDCounties\Database\Management\MySQLBackupRepository',
        'ParadoxOne\NDCounties\Contracts\SystemInformationRepositoryInterface'                   => '\ParadoxOne\NDCounties\Database\Repositories\TenantSystemInformationRepository',
        'ParadoxOne\NDCounties\Contracts\Data\BackupLogRepositoryInterface'                      => '\ParadoxOne\NDCounties\Database\Management\BackupLogRepository',
        'ParadoxOne\NDCounties\Contracts\Analytics\SystemAnalyticsRepositoryInterface'           => '\ParadoxOne\NDCounties\Database\Repositories\Analytics\AnalyticsRepository',

    );
}
