<?php namespace ParadoxOne\NDCounties\Database;

use Illuminate\Support\ServiceProvider;

class ReportingServiceProvider extends ServiceProvider
{
    use \ParadoxOne\NDCounties\Traits\ClassMapRegisterableTrait;

    protected $defer = false;

    protected $newableClassMap = array();

    protected $repositoryClassMap = array(
        'ParadoxOne\NDCounties\Contracts\Reporting\ReportMetaDefinitionRepositoryInterface' => '\ParadoxOne\NDCounties\Database\Repositories\Reporting\ReportMetaDefinitionRepository',
        'ParadoxOne\NDCounties\Contracts\Reporting\ReportEquationsRepositoryInterface'      => '\ParadoxOne\NDCounties\Database\Repositories\Reporting\ReportEquationRepository',
        'ParadoxOne\NDCounties\Contracts\Reporting\ReportConstructorInterface'              => '\ParadoxOne\NDCounties\Reporting\ReportConstructor',
        'ParadoxOne\NDCounties\Contracts\Reporting\ReportTotalBlocksRepositoryInterface'    => '\ParadoxOne\NDCounties\Database\Repositories\Reporting\ReportTotalBlocksRepository',
        'ParadoxOne\NDCounties\Contracts\Reporting\ReportCategoryRepositoryInterface'       => '\ParadoxOne\NDCounties\FlatFile\ReportCategoryRepository',
    );
}
