<?php namespace ParadoxOne\NDCounties\Database\Repositories\Reporting;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Config;
use ParadoxOne\NDCounties\Contracts\Reporting\MultipleExtensionException;
use ParadoxOne\NDCounties\Contracts\Reporting\ReportMetaDefinitionRepositoryInterface;
use ParadoxOne\NDCounties\Database\Repositories\BaseRepository;
use ParadoxOne\NDCounties\IdentificationCode;
use Reporting\MetaDefinition;
use Whoops\Example\Exception;
use Symfony\Component\Process\Process;

class ReportMetaDefinitionRepository extends BaseRepository implements ReportMetaDefinitionRepositoryInterface
{
    protected $table = 'system_reports';

    /**
     * Creates a new record. ALIAS.
     *
     * @param  array $recordDetails
     * @return mixed
     */
    public function create(array $recordDetails = array())
    {
        return $this->createDefinition($recordDetails);
    }

    /**
     * Removes an existing record. ALIAS.
     *
     * @param  array $removeDetails
     * @return mixed
     */
    public function remove(array $removeDetails = array())
    {
        return $this->removeDefinition($removeDetails[0]);
    }

    /**
     * Updates an existing record. ALIAS.
     *
     * @param  int $recordID
     * @param  array $newRecordDetails
     * @return mixed
     */
    public function update($recordID, array $newRecordDetails)
    {
        return $this->updateDefinition($recordID, $newRecordDetails);
    }

    /**
     * Removes a definition entry.
     *
     * @param $definitionID
     * @return mixed
     */
    public function removeDefinition($definitionID)
    {
        DB::beginTransaction();
        try {
            MetaDefinition::destroy($definitionID);
            DB::commit();

            return true;
        } catch (Exception $e) {
        }

        DB::rollback();

        return false;
    }

    /**
     * Removes an account's custom report.
     *
     * Implementations must ensure that accounts can only
     * remove reports registered to their own accounts.
     *
     * @throws \Exception
     * @param $account
     * @param $reportID
     * @return mixed
     */
    public function removeCustomReport($account, $reportID)
    {
        if ($account == 0) {
            throw new \Exception('[GUARD] Cannot remove system reports this way.');
        }
        DB::beginTransaction();
        try {
            MetaDefinition::where('account_id', '=', $account)->where(function($q) use ($reportID) {
               $q->where('id', '=', $reportID);
            })->delete();
            DB::commit();

            return true;
        } catch (Exception $e) {
        }

        DB::rollback();

        return false;
    }


    private function getFilterValue($name, $mode, $values)
    {
        $filter = new \stdClass;
        $filter->name = $name;

        if ($mode == null) {
            $filter->value = null;
        } else {
            if ($mode == 'inc') {
                $filter->mode = 'include';
            } else {
                $filter->mode = 'exclude';
            }
        }

        $filter->values = array_map('intval', $values);

        return $filter;
    }

    /**
     * Creates a custom report.
     *
     * @param array $reportData
     * @return mixed
     */
    public function createCustomReport(array $reportData)
    {
        DB::beginTransaction();

        try {
            $data = new Collection($reportData);

            $baseReport = MetaDefinition::find($reportData['report_mdef']);

            $definition = new MetaDefinition;
            $definition->report_extends_uid = $baseReport->report_uid;
            $definition->report_uid = Str::upper(IdentificationCode::UUID4());
            $definition->report_icon = $baseReport->report_icon;

            $reportDescription = $data->get('new_report_description');

            if (strlen(trim($reportDescription)) == 0) {
                $reportDescription = 'This report was created by ' . Auth::user()->first_name . ' ' . Auth::user()->last_name;
            }

            $definition->report_description = $reportDescription;

            $definition->report_name = $data->get('new_report_name');
            $definition->srl_header = '<!--HEADER-->';
            $definition->srl_body = '<!--BODY-->';
            $definition->srl_footer = '<!--FOOTER-->';
            $definition->account_id = Auth::user()->last_tenant;
            $definition->report_category = 6;

            $reportSettings = new \stdClass;
            $reportSettings->__COMMENT =
                'This configuration was automatically generated when creating a custom report by ' .
                Auth::user()->first_name . ' ' . Auth::user()->last_name . ' with the user ID of ' . Auth::user()->id .
                ' on ' . Carbon::now()->toDateTimeString();
            $reportSettings->use_equation = (int)$data->get('equation');
            $reportSettings->use_total_block = null;
            $reportSettings->alert_message = "";
            $reportSettings->preparation = [];
            $reportSettings->property_type_limits = array_map('intval', $data->get('property_type_filters', []));
            $reportSettings->filters = [];
            $reportSettings->constraints = [];
            $reportSettings->sorting_columns = [];

            $reportSettings->filters[] = $this->getFilterValue('activity', $data->get('activityFilerMode', 'inc'),
                $data->get('activity_filters', []));
            $reportSettings->filters[] = $this->getFilterValue('department', $data->get('departmentFilterMode', 'inc'),
                $data->get('department_filters', []));
            $reportSettings->filters[] = $this->getFilterValue('district', $data->get('districtFilterMode', 'inc'),
                $data->get('district_filters', []));
            $reportSettings->filters[] = $this->getFilterValue('employee', $data->get('employeeFilterMode', 'inc'),
                $data->get('employee_filters', []));
            $reportSettings->filters[] = $this->getFilterValue('equipment', $data->get('equipFilterMode', 'inc'),
                $data->get('equipment_filters', []));
            $reportSettings->filters[] = $this->getFilterValue('material', $data->get('materialFilterMode', 'inc'),
                $data->get('material_filters', []));
            $reportSettings->filters[] = $this->getFilterValue('project', $data->get('projectFilterMode', 'inc'),
                $data->get('project_filters', []));
            $reportSettings->filters[] = $this->getFilterValue('property', $data->get('propertyFilterMode', 'inc'),
                $data->get('property_filters', []));
            $reportSettings->filters[] = $this->getFilterValue('road', $data->get('roadFilterMode', 'inc'),
                $data->get('road_filters', []));

            $reportSettings->filters[] =
                $this->getFilterValue('special', null, $data->get('special_group_filters', []));

            for ($i = 0; $i < count($data->get('constraint_names')); $i++) {
                $name = $data->get('constraint_names')[$i];
                $operator = $data->get('constraint_modes')[$i];
                $value = $data->get('constraint_values')[$i];

                $constraint = new \stdClass;
                $constraint->name = $name;
                $constraint->operator = $operator;
                $constraint->value = $value;
                $reportSettings->constraints[] = $constraint;
            }

            for ($i = 0; $i < count($data->get('sorting_names')); $i++) {
                $column = $data->get('sorting_names')[$i];
                $mode = $data->get('sorting_modes')[$i];

                $sort = new \stdClass;
                $sort->setting = $mode;
                $sort->column = $column;
                $reportSettings->sorting_columns[] = $sort;
            }

            $reportSettings->settings = [];

            $genericGroupingColumn = new \stdClass;
            $genericGroupingColumn->name = 'generic_grouping_column';
            $genericGroupingColumn->value = $data->get('generic_grouping_column', '');
            $reportSettings->settings[] = $genericGroupingColumn;

            $specialGroupingColumn = new \stdClass;
            $specialGroupingColumn->name = 'special_grouping_filter';
            $specialGroupingColumn->value = $data->get('special_grouping_column', '');
            $reportSettings->settings[] = $specialGroupingColumn;

            $specialGroupingColumnName = new \stdClass;
            $specialGroupingColumnName->name = 'special_grouping_column_name';
            $specialGroupingColumnName->value = $data->get('special_grouping_name', '');
            $reportSettings->settings[] = $specialGroupingColumnName;

            $groupingColumnValue = $data->get('combine_columns', '');

            if (strlen($groupingColumnValue) > 0 && $groupingColumnValue != 'none') {
                $groupingColumnSetting = new \stdClass;
                $groupingColumnSetting->name = 'combine_columns';
                $groupingColumnSetting->value = $data->get('combine_columns');
                $reportSettings->settings[] = $groupingColumnSetting;
            }

            $organizationStrategySetting = new \stdClass;
            $organizationStrategySetting->name = 'organization_strategy';

            switch (intval($data->get('org_strategy', 0))) {
                // Default
                case 1:
                    $organizationStrategySetting->value = 'default';
                    break;
                // Generic
                case 2:
                    $organizationStrategySetting->value = 'generic';
                    break;
                // Special
                case 3:
                    $organizationStrategySetting->value = 'special';
                    break;
            }

            $reportSettings->settings[] = $organizationStrategySetting;

            $showMaterialBreakdownSetting = new \stdClass;
            $showMaterialBreakdownSetting->name = 'breakdown_material';
            $showMaterialBreakdownSetting->value = (bool)$data->get('show_material_breakdown', false);
            $reportSettings->settings[] = $showMaterialBreakdownSetting;

            $showMaterialBreakdownTotalSetting = new \stdClass;
            $showMaterialBreakdownTotalSetting->name = 'breakdown_material_total';
            $showMaterialBreakdownTotalSetting->value = (bool)$data->get('show_material_breakdown_total', false);
            $reportSettings->settings[] = $showMaterialBreakdownTotalSetting;

            $showEquipmentUnitBreakdown = new \stdClass;
            $showEquipmentUnitBreakdown->name = 'breakdown_equipment';
            $showEquipmentUnitBreakdown->value = (bool)$data->get('show_equipment_unit_breakdown', false);
            $reportSettings->settings[] = $showEquipmentUnitBreakdown;

            $showEquipmentUnitTotalBreakdown = new \stdClass;
            $showEquipmentUnitTotalBreakdown->name = 'breakdown_equipment_total';
            $showEquipmentUnitTotalBreakdown->value = (bool)$data->get('show_equipment_unit_breakdown_total', false);
            $reportSettings->settings[] = $showEquipmentUnitTotalBreakdown;

            $showFuelBreakdown = new \stdClass;
            $showFuelBreakdown->name = 'breakdown_fuel';
            $showFuelBreakdown->value = (bool)$data->get('show_fuel_breakdown', false);
            $reportSettings->settings[] = $showFuelBreakdown;

            $showFuelBreakdownTotal = new \stdClass;
            $showFuelBreakdownTotal->name = 'breakdown_fuel_total';
            $showFuelBreakdownTotal->value = (bool)$data->get('show_fuel_breakdown_total', false);
            $reportSettings->settings[] = $showFuelBreakdownTotal;

            $showPropertyBreakdown = new \stdClass;
            $showPropertyBreakdown->name = 'breakdown_property';
            $showPropertyBreakdown->value = (bool)$data->get('show_property_breakdown', false);
            $reportSettings->settings[] = $showPropertyBreakdown;

            $showPropertyBreakdownTotal = new \stdClass;
            $showPropertyBreakdownTotal->name = 'breakdown_property_total';
            $showPropertyBreakdownTotal->value = (bool)$data->get('show_property_breakdown_total', false);
            $reportSettings->settings[] = $showPropertyBreakdownTotal;

            $showEquipmentUnitRate = new \stdClass;
            $showEquipmentUnitRate->name = 'equipment_show_rate';
            $showEquipmentUnitRate->value = (bool)$data->get('show_equipment_unit_rate', false);
            $reportSettings->settings[] = $showEquipmentUnitRate;

            $showEmployeeWage = new \stdClass;
            $showEmployeeWage->name = 'employee_show_rate';
            $showEmployeeWage->value = (bool)$data->get('show_employee_wage', false);
            $reportSettings->settings[] = $showEmployeeWage;

            $showEmployeeHoursWorked = new \stdClass;
            $showEmployeeHoursWorked->name = 'employee_show_hours';
            $showEmployeeHoursWorked->value = (bool)$data->get('show_employee_hours_worked', false);
            $reportSettings->settings[] = $showEmployeeHoursWorked;

            $definition->srl_settings = json_encode($reportSettings);

            $success = $definition->save();

            if ($success) {
                DB::commit();

                return true;
            }

            $this->errors = $definition->errors();
        } catch (\Exception $e) {
            lk($e);
        }

        DB::rollback();

        return false;
    }

    /**
     * Creates a new report definition.
     *
     * @param array $definition
     * @return mixed
     */
    public function createDefinition(array $definition)
    {
        DB::beginTransaction();

        try {
            $metaDefinition = new MetaDefinition;
            $metaDefinition->report_name = $definition['report_name'];
            $metaDefinition->report_icon = $definition['report_icon'];
            $metaDefinition->report_uid = $definition['ruid'];
            $metaDefinition->report_extends_uid = $definition['eruid'];
            $metaDefinition->report_description = $definition['report_description'];
            $metaDefinition->account_id = $definition['report_account'];
            $metaDefinition->report_category = $definition['report_category'];

            $metaDefinition->srl_header = $definition['srl_header'];
            $metaDefinition->srl_body = $definition['srl_body'];
            $metaDefinition->srl_footer = $definition['srl_footer'];
            $metaDefinition->srl_settings = $definition['report_srl'];

            $success = $metaDefinition->save();

            if ($success === false) {
                $this->errors = $metaDefinition->errors();
                DB::rollback();

                return false;
            }

            DB::commit();

            return true;
        } catch (\Exception $e) {
            lk($e);
        }
        DB::rollback();

        return false;
    }

    /**
     * Updates a given definition entry.
     *
     * @param       $definitionID
     * @param array $definition
     * @return mixed
     */
    public function updateDefinition($definitionID, array $definition)
    {
        DB::beginTransaction();
        try {
            $metaDefinition = MetaDefinition::findOrFail($definitionID);
            $metaDefinition->report_name = $definition['report_name'];
            $metaDefinition->report_icon = $definition['report_icon'];
            $metaDefinition->report_uid = $definition['ruid'];
            $metaDefinition->report_extends_uid = $definition['eruid'];
            $metaDefinition->report_description = $definition['report_description'];
            $metaDefinition->account_id = $definition['report_account'];
            $metaDefinition->report_category = $definition['report_category'];

            $metaDefinition->srl_header = $definition['srl_header'];
            $metaDefinition->srl_body = $definition['srl_body'];
            $metaDefinition->srl_footer = $definition['srl_footer'];
            $metaDefinition->srl_settings = $definition['report_srl'];

            $success = $metaDefinition->updateUniques();

            if ($success === false) {
                $this->errors = $metaDefinition->errors();
                DB::rollback();

                return false;
            }

            DB::commit();

            return true;
        } catch (\Exception $e) {
        }

        DB::rollback();

        return false;
    }

    /**
     * Returns a list of definitions without the SRL data.
     *
     * @return mixed
     */
    public function getDefinitions()
    {
        return DB::table('system_reports')
            ->select(DB::raw('system_reports.*'), DB::raw('accounts.account_name as account_name'))
            ->join('accounts', 'accounts.id', '=', 'system_reports.account_id')->orderBy('system_reports.report_name');
    }

    /**
     * Retrieves a definition.
     *
     * @param $definitionID
     * @return mixed
     *
     * @throws MultipleExtensionException
     */
    public function getDefinition($definitionID)
    {
        // return MetaDefinition::findOrFail($definitionID);
        $reportDefinition = MetaDefinition::whereRaw('id IN (
	?, (SELECT id FROM system_reports WHERE report_uid = (SELECT report_extends_uid FROM system_reports WHERE id = ?)))',
            [$definitionID, $definitionID])->get();

        if (count($reportDefinition) == 0) {
            throw new ModelNotFoundException;
        }

        if (count($reportDefinition) == 1) {
            return $reportDefinition[0];
        }

        if (count($reportDefinition) > 2) {
            throw new MultipleExtensionException('Reports are not allowed to extend more than one report');
        }

        return MetaDefinition::buildFromExtension($definitionID, $reportDefinition);
    }

    /**
     * Installs the default reporting system values.
     *
     * @return mixed
     */
    public function installDefaultReportingSystem()
    {
        DB::connection('mysql')->beginTransaction();

        try {
            DB::connection('mysql')->statement('SET SESSION sql_mode=\'NO_AUTO_VALUE_ON_ZERO\'');

            DB::connection('mysql')->table('system_reports')->whereIn('id', range(1, 18))->delete();
            DB::connection('mysql')->table('system_reports_default_totals')->whereIn('id', range(0, 2))->delete();

            $installationScript = file_get_contents(app_path() . '/ndcounties/config_stubs/default_reporting.txt');
            $reportInstallers = file_get_contents(app_path() . '/ndcounties/config_stubs/default_reports.txt');

            DB::connection('mysql')->unprepared($installationScript);
            DB::connection('mysql')->unprepared($reportInstallers);

            DB::connection('mysql')->commit();

            return true;
        } catch (\Exception $e) {
        }

        DB::connection('mysql')->rollback();

        return false;
    }

    /**
     * Indicates if a report exists by name.
     *
     * @param $reportName
     * @return mixed
     */
    public function reportExistsByName($reportName, $exclude = [])
    {
        $definition = MetaDefinition::where('report_name', '=', $reportName);
        $definition = $definition->whereIn('id', [0, Auth::user()->last_tenant]);

        if (count($exclude) > 0) {
            $definition = $definition->whereNotIn('id', $exclude);
        }

        $definition = $definition->first();


        if ($definition == null) {
            return false;
        }

        return true;
    }

    public function exportReportsForDownload()
    {
        set_time_limit(0);
        $dateNow = Carbon::now()->toDateString();
        $pakLocation = storage_path('report_paks/ND County Administrator Reports'.$dateNow.'.sql');
        touch($pakLocation);
        $databaseConfiguration = Config::get('database.connections.' . Config::get('database.default'));
        try{
            $dumpCommand = sprintf('"%smysqldump" --no-create-info --skip-triggers --user=%s --password=%s --host=%s --where="account_id in (0)" %s system_reports system_reports_custom_values system_reports_default_totals system_reports_total_blocks > %s',
                Config::get('crms.db_dump_command_path'),
                escapeshellarg($databaseConfiguration['username']),
                escapeshellarg($databaseConfiguration['password']),
                escapeshellarg($databaseConfiguration['host']),
                escapeshellarg($databaseConfiguration['database']),
                escapeshellarg($pakLocation)
            );

            $symfonyCommand = new Process($dumpCommand);
            $symfonyCommand->setTimeout(999999999);
            $symfonyCommand->run();

            if ($symfonyCommand->isSuccessful()) {
                return $pakLocation;
            } else {
                dd($symfonyCommand->getErrorOutput());
            }
        } catch (\Exception $e) {
            dd($e);
        }

        return false;
    }
}
