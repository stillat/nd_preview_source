<?php namespace ParadoxOne\NDCounties\Database\Repositories\Reporting;

use Illuminate\Support\Facades\DB;
use ParadoxOne\NDCounties\Contracts\Reporting\ReportTotalBlocksRepositoryInterface;
use ParadoxOne\NDCounties\Database\Repositories\BaseRepository;
use Reporting\TotalBlock;

class ReportTotalBlocksRepository extends BaseRepository implements ReportTotalBlocksRepositoryInterface
{
    protected $table = 'system_reports_total_blocks';

    /**
     * Returns a builder for all total blocks.
     *
     * @return mixed
     */
    public function getTotalBlocks()
    {
        return $this->table()->select(DB::raw('system_reports_total_blocks.*',
                                              'accounts.account_name, accounts.id as account_id'))
                    ->join('accounts', 'accounts.id', '=', 'system_reports_total_blocks.account_id');
    }


    /**
     * Gets a total block by ID.
     *
     * @param $totalBlockID
     * @return mixed
     */
    public function getTotalBlock($totalBlockID)
    {
        return $this->table()->select(DB::raw('system_reports_total_blocks.*',
                                              'accounts.account_name, accounts.id as account_id'))
                    ->join('accounts', 'accounts.id', '=', 'system_reports_total_blocks.account_id')
                    ->where('system_reports_total_blocks.id', '=', $totalBlockID)->first();
    }

    /**
     * Gets the default total block for the given account.
     *
     * @param $accountID
     * @return mixed
     */
    public function getDefaultTotalBlockForAccount($accountID)
    {
        return $this->table()->select(DB::raw('system_reports_total_blocks.*',
                                              'accounts.account_name, accounts.id as account_id'))
                    ->join('accounts', 'accounts.id', '=', 'system_reports_total_blocks.account_id')
                    ->whereIn('system_reports_total_blocks.account_id', [0, $accountID])->where('is_default', '=', 1)
                    ->orderBy('system_reports_total_blocks.account_id', 'DESC')->first();
    }

    /**
     * Sets the default total block for the given account.
     *
     * @param $accountID
     * @param $totalBlockID
     * @return mixed
     */
    public function setDefaultTotalBlockForAccount($accountID, $totalBlockID)
    {
        try {
            DB::statement('UPDATE system_reports_total_blocks SET is_default = 0 WHERE account_id = :account_id;',
                          [
                              'account_id' => $accountID
                          ]
            );

            DB::statement('UPDATE system_reports_total_blocks SET is_default = 1 WHERE id = :block_id;', [
                'block_id' => $totalBlockID
            ]);

            return true;
        } catch (\Exception $e) {
        }

        return false;
    }


    /**
     * Creates a new record.
     *
     * @param  array $recordDetails
     * @return mixed
     */
    public function create(array $recordDetails = array())
    {
        DB::beginTransaction();


        try {
            $block              = new TotalBlock;
            $block->name        = $recordDetails['name'];
            $block->value       = $recordDetails['value'];
            $block->account_id  = $recordDetails['account_id'];
            $block->description = $recordDetails['description'];

            $succcess = $block->save([
                                         'name' => 'unique:system_reports_total_blocks,name,NULL,id,account_id,' .
                                                   $block->account_id
                                     ]);

            if ($succcess === false) {
                $this->errors = $block->errors();
                DB::rollback();

                return false;
            }

            DB::commit();

            return true;
        } catch (\Exception $e) {
        }

        DB::rollback();

        return false;
    }

    /**
     * Removes an existing record.
     *
     * @param  array $removeDetails
     * @return mixed
     */
    public function remove(array $removeDetails = array())
    {
        DB::beginTransaction();
        try {
            TotalBlock::destroy(intval($removeDetails[0]));
            DB::commit();

            return true;
        } catch (\Exception $e) {
            lk($e);
        }

        DB::rollback();

        return false;
    }

    /**
     * Updates an existing record.
     *
     * @param  int   $recordID
     * @param  array $newRecordDetails
     * @return mixed
     */
    public function update($recordID, array $newRecordDetails)
    {
        DB::beginTransaction();

        try {
            $block        = TotalBlock::findOrFail($recordID);
            $block->name  = $newRecordDetails['name'];
            $block->value = $newRecordDetails['value'];

            if (array_key_exists('account_id', $newRecordDetails)) {
                $block->account_id = $newRecordDetails['account_id'];
            }

            $success = $block->updateUniques([
                                                 'name' => 'unique:system_reports_total_blocks,name,' . $recordID .
                                                           ',id,account_id,' . $block->account_id
                                             ]);

            if ($success === false) {
                $this->errors = $block->errors();

                return false;
            }

            DB::commit();

            return true;
        } catch (\Exception $e) {
            lk($e);
        }

        DB::rollback();

        return false;
    }
}
