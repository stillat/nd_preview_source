<?php namespace ParadoxOne\NDCounties\Database\Repositories\Reporting;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Str;
use ParadoxOne\NDCounties\Contracts\Reporting\MultipleExtensionException;
use ParadoxOne\NDCounties\Database\Repositories\BaseRepository;
use ParadoxOne\NDCounties\Contracts\Reporting\ReportsRepositoryInterface;
use ParadoxOne\NDCounties\IdentificationCode;
use Reporting\MetaDefinition;

class ReportsDatabaseRepository extends BaseRepository implements ReportsRepositoryInterface
{
    protected $table = 'reports';

    /**
     * Creates a new record.
     *
     * @param  array $recordDetails
     * @return mixed
     */
    public function create(array $recordDetails = array())
    {
        DB::beginTransaction();

        try {
            $metaDefinition                     = new MetaDefinition;
            $metaDefinition->report_name        = $recordDetails['report_name'];
            $metaDefinition->report_icon        = $recordDetails['report_icon'];
            $metaDefinition->report_uid         = $recordDetails['ruid'];
            $metaDefinition->report_extends_uid = $recordDetails['eruid'];
            $metaDefinition->report_description = $recordDetails['report_description'];
            $metaDefinition->account_id         = $recordDetails['report_account'];
            $metaDefinition->report_category    = $recordDetails['report_category'];

            $metaDefinition->body         = $recordDetails['srl_report'];
            $metaDefinition->srl_settings = $recordDetails['report_srl'];

            $success = $metaDefinition->save();

            if ($success === false) {
                $this->errors = $metaDefinition->errors();
                DB::rollback();

                return false;
            }

            DB::commit();

            return true;
        } catch (\Exception $e) {
            lk($e);
        }
        DB::rollback();

        return false;
    }

    /**
     * Removes an existing record.
     *
     * @param  array $removeDetails
     * @return mixed
     */
    public function remove(array $removeDetails = array())
    {
        DB::beginTransaction();
        try {
            MetaDefinition::destroy($removeDetails[0]);
            DB::commit();

            return true;
        } catch (Exception $e) {
        }

        DB::rollback();

        return false;
    }

    /**
     * Updates an existing record.
     *
     * @param  int   $recordID
     * @param  array $newRecordDetails
     * @return mixed
     */
    public function update($recordID, array $newRecordDetails)
    {
        DB::beginTransaction();
        try {
            $metaDefinition                     = MetaDefinition::findOrFail($recordID);
            $metaDefinition->report_name        = $newRecordDetails['report_name'];
            $metaDefinition->report_icon        = $newRecordDetails['report_icon'];
            $metaDefinition->report_uid         = $newRecordDetails['ruid'];
            $metaDefinition->report_extends_uid = $newRecordDetails['eruid'];
            $metaDefinition->report_description = $newRecordDetails['report_description'];
            $metaDefinition->account_id         = $newRecordDetails['report_account'];
            $metaDefinition->report_category    = $newRecordDetails['report_category'];

            $metaDefinition->body         = $newRecordDetails['srl_report'];
            $metaDefinition->srl_settings = $newRecordDetails['report_srl'];


            $success = $metaDefinition->updateUniques();

            if ($success === false) {
                $this->errors = $metaDefinition->errors();
                DB::rollback();

                return false;
            }

            DB::commit();

            return true;
        } catch (\Exception $e) {
        }

        DB::rollback();

        return false;
    }

    private function getFilterValue($name, $mode, $values)
    {
        $filter       = new \stdClass;
        $filter->name = $name;

        if ($mode == null) {
            $filter->value = null;
        } else {
            if ($mode == 'inc') {
                $filter->mode = 'include';
            } else {
                $filter->mode = 'exclude';
            }
        }

        $filter->values = array_map('intval', $values);

        return $filter;
    }

    /**
     * Creates a custom report.
     *
     * @param array $reportData
     * @return mixed
     */
    public function createCustomReport(array $reportData)
    {
        DB::beginTransaction();

        try {
            $data = new Collection($reportData);
            $baseReport = MetaDefinition::find($reportData['report_mdef']);

            $definition                     = new MetaDefinition;
            $definition->report_extends_uid = $baseReport->report_uid;
            $definition->report_uid         = Str::upper(IdentificationCode::UUID4());
            $definition->report_icon        = $baseReport->report_icon;

            $reportDescription = $data->get('new_report_description');

            if (strlen(trim($reportDescription)) == 0) {
                $reportDescription = 'This report was created by '.Auth::user()->first_name.' '.Auth::user()->last_name;
            }

            $definition->report_description = $reportDescription;

            $definition->report_name        = $data->get('new_report_name');
            $definition->body           = '<!--REPORT BODY-->';
            $definition->account_id         = Auth::user()->last_tenant;
            $definition->report_category    = 6;

            $reportSettings                       = new \stdClass;
            $reportSettings->__COMMENT            =
                'This configuration was automatically generated when creating a custom report by ' .
                Auth::user()->first_name . ' ' . Auth::user()->last_name . ' with the user ID of ' . Auth::user()->id.
                ' on '.Carbon::now()->toDateTimeString();
            $reportSettings->use_equation         = (int)$data->get('equation');
            $reportSettings->use_total_block      = null;
            $reportSettings->alert_message        = "";
            $reportSettings->preparation          = [];
            $reportSettings->property_type_limits = array_map('intval', $data->get('property_type_filters', []));
            $reportSettings->filters              = [];
            $reportSettings->constraints          = [];

            $reportSettings->filters[] = $this->getFilterValue('activity', $data->get('activityFilerMode', 'inc'),
                                                               $data->get('activity_filters', []));
            $reportSettings->filters[] = $this->getFilterValue('department', $data->get('departmentFilterMode', 'inc'),
                                                               $data->get('department_filters', []));
            $reportSettings->filters[] = $this->getFilterValue('district', $data->get('districtFilterMode', 'inc'),
                                                               $data->get('district_filters', []));
            $reportSettings->filters[] = $this->getFilterValue('employee', $data->get('employeeFilterMode', 'inc'),
                                                               $data->get('employee_filters', []));
            $reportSettings->filters[] = $this->getFilterValue('equipment', $data->get('equipFilterMode', 'inc'),
                                                               $data->get('equipment_filters', []));
            $reportSettings->filters[] = $this->getFilterValue('material', $data->get('materialFilterMode', 'inc'),
                                                               $data->get('material_filters', []));
            $reportSettings->filters[] = $this->getFilterValue('project', $data->get('projectFilterMode', 'inc'),
                                                               $data->get('project_filters', []));
            $reportSettings->filters[] = $this->getFilterValue('property', $data->get('propertyFilterMode', 'inc'),
                                                               $data->get('property_filters', []));

            $reportSettings->filters[] =
                $this->getFilterValue('special', null, $data->get('special_group_filters', []));

            for ($i = 0; $i < count($data->get('constraint_names')); $i++) {
                $name     = $data->get('constraint_names')[$i];
                $operator = $data->get('constraint_modes')[$i];
                $value    = $data->get('constraint_values')[$i];

                $constraint                    = new \stdClass;
                $constraint->name              = $name;
                $constraint->operator          = $operator;
                $constraint->value             = $value;
                $reportSettings->constraints[] = $constraint;
            }


            $reportSettings->settings = [];

            $genericGroupingColumn        = new \stdClass;
            $genericGroupingColumn->name  = 'generic_grouping_column';
            $genericGroupingColumn->value = $data->get('generic_grouping_column', '');
            $reportSettings->settings[]   = $genericGroupingColumn;

            $specialGroupingColumn        = new \stdClass;
            $specialGroupingColumn->name  = 'special_grouping_filter';
            $specialGroupingColumn->value = $data->get('special_grouping_column', '');
            $reportSettings->settings[]   = $specialGroupingColumn;

            $specialGroupingColumnName        = new \stdClass;
            $specialGroupingColumnName->name  = 'special_grouping_column_name';
            $specialGroupingColumnName->value = $data->get('special_grouping_name', '');
            $reportSettings->settings[]       = $specialGroupingColumnName;

            $groupingColumnValue = $data->get('group_report_column', 'none');

            if (strlen($groupingColumnValue) > 0 && $groupingColumnValue != 'none') {
                $groupingColumnSetting        = new \stdClass;
                $groupingColumnSetting->name  = 'grouping_column';
                $groupingColumnSetting->value = $data->get('group_report_column');
                $reportSettings->settings[]   = $groupingColumnSetting;
            }

            $organizationStrategySetting       = new \stdClass;
            $organizationStrategySetting->name = 'organization_strategy';

            switch (intval($data->get('org_strategy', 0))) {
                // Default
                case 1:
                    $organizationStrategySetting->value = 'default';
                    break;
                // Generic
                case 2:
                    $organizationStrategySetting->value = 'generic';
                    break;
                // Special
                case 3:
                    $organizationStrategySetting->value = 'special';
                    break;
            }

            $reportSettings->settings[] = $organizationStrategySetting;

            $showMaterialBreakdownSetting        = new \stdClass;
            $showMaterialBreakdownSetting->name  = 'breakdown_material';
            $showMaterialBreakdownSetting->value = (bool)$data->get('show_material_breakdown', false);
            $reportSettings->settings[]          = $showMaterialBreakdownSetting;

            $showMaterialBreakdownTotalSetting        = new \stdClass;
            $showMaterialBreakdownTotalSetting->name  = 'breakdown_material_total';
            $showMaterialBreakdownTotalSetting->value = (bool)$data->get('show_material_breakdown_total', false);
            $reportSettings->settings[]               = $showMaterialBreakdownTotalSetting;

            $showEquipmentUnitBreakdown        = new \stdClass;
            $showEquipmentUnitBreakdown->name  = 'breakdown_equipment';
            $showEquipmentUnitBreakdown->value = (bool)$data->get('show_equipment_unit_breakdown', false);
            $reportSettings->settings[]        = $showEquipmentUnitBreakdown;

            $showEquipmentUnitTotalBreakdown        = new \stdClass;
            $showEquipmentUnitTotalBreakdown->name  = 'breakdown_equipment_total';
            $showEquipmentUnitTotalBreakdown->value = (bool)$data->get('show_equipment_unit_breakdown_total', false);
            $reportSettings->settings[]             = $showEquipmentUnitTotalBreakdown;

            $showFuelBreakdown          = new \stdClass;
            $showFuelBreakdown->name    = 'breakdown_fuel';
            $showFuelBreakdown->value   = (bool)$data->get('show_fuel_breakdown', false);
            $reportSettings->settings[] = $showFuelBreakdown;

            $showFuelBreakdownTotal        = new \stdClass;
            $showFuelBreakdownTotal->name  = 'breakdown_fuel_total';
            $showFuelBreakdownTotal->value = (bool)$data->get('show_fuel_breakdown_total', false);
            $reportSettings->settings[]    = $showFuelBreakdownTotal;

            $showPropertyBreakdown        = new \stdClass;
            $showPropertyBreakdown->name  = 'breakdown_property';
            $showPropertyBreakdown->value = (bool)$data->get('show_property_breakdown', false);
            $reportSettings->settings[]   = $showPropertyBreakdown;

            $showPropertyBreakdownTotal        = new \stdClass;
            $showPropertyBreakdownTotal->name  = 'breakdown_property_total';
            $showPropertyBreakdownTotal->value = (bool)$data->get('show_property_breakdown_total', false);
            $reportSettings->settings[]        = $showPropertyBreakdownTotal;

            $showEquipmentUnitRate        = new \stdClass;
            $showEquipmentUnitRate->name  = 'equipment_show_rate';
            $showEquipmentUnitRate->value = (bool)$data->get('show_equipment_unit_rate', false);
            $reportSettings->settings[]   = $showEquipmentUnitRate;

            $showEmployeeWage           = new \stdClass;
            $showEmployeeWage->name     = 'employee_show_rate';
            $showEmployeeWage->value    = (bool)$data->get('show_employee_wage', false);
            $reportSettings->settings[] = $showEmployeeWage;

            $definition->srl_settings = json_encode($reportSettings);

            $success = $definition->save();

            if ($success) {
                DB::commit();

                return true;
            }

            $this->errors = $definition->errors();
        } catch (\Exception $e) {
            lk($e);
        }

        DB::rollback();

        return false;
    }

    /**
     * Returns a list of reports without the SRL data.
     *
     * @return mixed
     */
    public function getReports()
    {
        return DB::table('reports')
                 ->select(DB::raw('reports.*'), DB::raw('accounts.account_name as account_name'))
                 ->join('accounts', 'accounts.id', '=', 'reports.account_id');
    }


    public function get($reportID)
    {
        $reportDefinition = MetaDefinition::whereRaw('id IN (
	?, (SELECT id FROM reports WHERE report_uid = (SELECT report_extends_uid FROM reports WHERE id = ?)))',
                                                     [$reportID, $reportID])->get();


        if (count($reportDefinition) == 0) {
            throw new ModelNotFoundException;
        }

        if (count($reportDefinition) == 1) {
            return $reportDefinition[0];
        }

        if (count($reportDefinition) > 2) {
            throw new MultipleExtensionException('Reports are not allowed to extend more than one report');
        }

        return MetaDefinition::buildFromExtension($reportID, $reportDefinition);
    }

    /**
     * Indicates if a report exists by name.
     *
     * @param $reportName
     * @return mixed
     */
    public function existsByName($reportName)
    {
        $definition = MetaDefinition::where('report_name', '=', $reportName)->first();

        if ($definition == null) {
            return false;
        }

        return true;
    }

    /**
     * Installs the default reporting system values.
     *
     * @return mixed
     */
    public function installDefaultReportingSystem()
    {
        // TODO: Implement installDefaultReportingSystem() method.
    }
}
