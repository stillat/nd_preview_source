<?php namespace ParadoxOne\NDCounties\Database\Repositories\Reporting;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use ParadoxOne\NDCounties\Contracts\Reporting\ReportEquationsRepositoryInterface;
use ParadoxOne\NDCounties\Database\Repositories\BaseRepository;
use ParadoxOne\NDCounties\Settings\ReportSettingsManager;
use PHPSQL\Parser;
use Reporting\Equation;
use UI;

class ReportEquationRepository extends BaseRepository implements ReportEquationsRepositoryInterface
{
    protected $table = 'system_reports_default_totals';

    protected $settingsManager;

    public function __construct(ReportSettingsManager $manager)
    {
        $this->settingsManager = $manager;
    }

    /**
     * Creates a new record.
     *
     * @param  array $recordDetails
     * @return mixed
     */
    public function create(array $recordDetails = array())
    {
    }

    /**
     * Removes an existing record.
     *
     * @param  array $removeDetails
     * @return mixed
     */
    public function remove(array $removeDetails = array())
    {
    }

    /**
     * Updates an existing record.
     *
     * @param  int   $recordID
     * @param  array $newRecordDetails
     * @return mixed
     */
    public function update($recordID, array $newRecordDetails)
    {
    }

    /**
     * Validates a formula.
     *
     * @param $formula
     * @return mixed
     */
    public function validateFormula($formula)
    {
        $allowed = [
            'ABS', 'ACOS', 'ASIN', 'ATAN2', 'ATAN', 'CEIL', 'CEILING', 'CONV', 'COS', 'COT', 'DEGREES', 'EXP', 'FLOOR',
            'LN', 'LOG10', 'LOG2', 'MOD', 'PI', 'POW', 'POWER', 'RADIANS', 'RAND', 'ROUND', 'SIGN', 'SIN', 'SQRT',
            'TAN', 'TRUNCATE'
        ];

        $allowedExpressions = [
            'SUM', 'AVG', 'TOTAL'
        ];

        try {
            $parser = new Parser;
            $parsed = $parser->parse('SELECT ' . $formula);

            if ($parsed) {
                if (count($parsed) == 1) {
                    $check = $parsed['SELECT'];

                    if (count($check) == 1) {
                        $base_expr = $check[0]['base_expr'];

                        if (in_array($base_expr, $allowedExpressions)) {
                            return true;
                        } else {
                            return false;
                        }
                    } else {
                        return false;
                    }
                } else {
                    return false;
                }
            } else {
                $keyWords = array_keys($parsed);

                foreach ($keyWords as $keyWord) {
                    if (in_array($keyWord, $allowed) == false) {
                        return false;
                    }
                }
            }

            unset($parsed);
        } catch (\Exception $e) {
            return false;
        }


        return true;
    }


    /**
     * Creates an equation for the given account.
     *
     * @param       $accountID
     * @param array $equationInformation
     * @return mixed
     */
    public function createEquation($accountID, array $equationInformation)
    {
        DB::beginTransaction();

        try {
            if ($this->validateFormula($equationInformation['formula']) == false) {
                return false;
            }

            $equation                     = new Equation;
            $equation->total_setting_name = $equationInformation['total_setting_name'];
            $equation->formula            = trim($equationInformation['formula']);
            $equation->account_id         = $equationInformation['account_id'];

            $success = $equation->save();

            if ($success === false) {
                $this->errors = $equation->errors();
                DB::rollback();

                return false;
            }

            DB::commit();

            return true;
        } catch (\Exception $e) {
        }

        DB::rollback();

        return false;
    }

    /**
     * Updates the given equation.
     *
     * @param       $equationID
     * @param array $equationInformation
     * @return mixed
     */
    public function updateEquation($equationID, array $equationInformation)
    {
        DB::beginTransaction();

        try {
            if ($this->validateFormula($equationInformation['formula']) == false) {
                return false;
            }

            $equation                     = Equation::findOrFail($equationID);
            $equation->total_setting_name = $equationInformation['total_setting_name'];
            $equation->formula            = trim($equationInformation['formula']);

            if (array_key_exists('account_id', $equationInformation)) {
                $equation->account_id = $equationInformation['account_id'];
            }

            $success = $equation->updateUniques();
            if ($success === false) {
                $this->errors = $equation->errors();

                return false;
            }

            DB::commit();

            return true;
        } catch (\Exception $e) {
        }
        DB::rollback();

        return false;
    }

    /**
     * Gets a list of equations.
     *
     * @return mixed
     */
    public function getEquations()
    {
        return $this->table()
                    ->select(DB::raw('system_reports_default_totals.*, accounts.account_name, accounts.id as account_id'))
                    ->join('accounts', 'accounts.id', '=', 'system_reports_default_totals.account_id');
    }

    /**
     * Gets information for the given equation ID.
     *
     * @param $equationID
     * @return mixed
     */
    public function getEquation($equationID)
    {
        return $this->table()
                    ->select(DB::raw('system_reports_default_totals.*, accounts.account_name, accounts.id as account_id'))
                    ->join('accounts', 'accounts.id', '=', 'system_reports_default_totals.account_id')
                    ->where('system_reports_default_totals.id', '=', $equationID)->first();
    }

    /**
     * Removes a given equation.
     *
     * @param $equationID
     * @return mixed
     */
    public function removeEquation($equationID)
    {
        DB::beginTransaction();
        try {
            Equation::destroy($equationID);
            DB::commit();

            return true;
        } catch (\Exception $e) {
        }

        DB::rollback();

        return false;
    }

    /**
     * Retrieve the default record equation for the given account.
     *
     * @param $accountID
     * @return mixed
     */
    public function getDefaultEquationForAccount($accountID)
    {
        $reportSettings = $this->settingsManager->getReportSettings();
        return $this->getEquation($reportSettings->default_equation);
    }

    /**
     * Sets the default record equation for the given account.
     *
     * @param $accountID
     * @param $equationID
     * @return mixed120
     */
    public function setDefaultEquationForAccount($accountID, $equationID)
    {
        DB::beginTransaction();
        try {
            $setting = DB::table('county_wide_application_settings')->where('service_account_id', '=', $accountID)->where('subsystem', '=', 'reporting')->first();
            $values = json_decode($setting->setting_value);
            $values->default_equation = $equationID;

            DB::table('county_wide_application_settings')
                ->where('service_account_id', '=', $accountID)
                ->where('subsystem', '=', 'reporting')->update([
                'setting_value' => json_encode($values)
                                                                                                                                                      ]);

            DB::commit();
            return true;
        } catch (\Exception $e) {
        }
        DB::rollback();
        return false;
    }
}
