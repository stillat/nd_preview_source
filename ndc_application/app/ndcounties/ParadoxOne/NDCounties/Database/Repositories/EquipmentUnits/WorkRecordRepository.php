<?php namespace ParadoxOne\NDCounties\Database\Repositories\EquipmentUnits;

use Exception;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Event;
use ParadoxOne\NDCounties\Contracts\EquipmentUnits\OdometerReadingRepositoryInterface;
use ParadoxOne\NDCounties\Contracts\EquipmentUnits\WorkRecordRepositoryInterface;
use ParadoxOne\NDCounties\Database\Repositories\BaseTenantRepository;
use Service\EquipmentUnits\WorkRecord as EquipmentWorkRecord;

class WorkRecordRepository extends BaseTenantRepository implements WorkRecordRepositoryInterface
{
    protected $odometerReadingRepository;

    public function __construct(OdometerReadingRepositoryInterface $odometerReadingRepositoryInterface)
    {
        $this->odometerReadingRepository = $odometerReadingRepositoryInterface;
    }

    public function getRecordByID($recordID)
    {
        return EquipmentWorkRecord::findOrFail($recordID);
    }

    public function getRecordByWorkID($workID)
    {
        return EquipmentWorkRecord::where('work_record_id', '=', $workID)->firstOrFail();
    }

    /**
     * Creates a new record.
     *
     * @param  array $recordDetails
     * @return mixed
     */
    public function create(array $recordDetails = array())
    {
        $workRecord                         = new EquipmentWorkRecord;
        $workRecord->work_record_id         = $recordDetails['work_record_id'];
        $workRecord->shop_costs             = $recordDetails['shop_costs'];
        $workRecord->purchased_costs        = $recordDetails['purchased_costs'];
        $workRecord->commercial_labor_costs = $recordDetails['commercial_labor_costs'];
        $workRecord->commercial_costs       = $recordDetails['commercial_costs'];
        $workRecord->work_order_number      = $recordDetails['work_order_number'];

        $workRecord->work_description = $recordDetails['work_description'];

        DB::connection(\Tenant::getCurrentConnection())->beginTransaction();

        try {
            if ($workRecord->save()) {
                // Now we will grab the odometer readings from the input.
                $odometerReading = $recordDetails['odometer_reading'];

                if (strlen(trim($odometerReading)) > 0) {
                    $equipmentUnit = $recordDetails['equipment_unit_id'];

                    DB::connection(\Tenant::getCurrentConnection())->table('equipment_odometer_readings')
                      ->where(function ($query) {
                          $query->where('associated_record_type', '=', '2');
                      })->where(function ($query) use ($equipmentUnit, $workRecord) {
                        $query->where('equipment_id', '=', $equipmentUnit);
                        $query->where('associated_record', '=', $workRecord->work_record_id);
                    })->delete();

                    $odometerSuccess = $this->odometerReadingRepository->create([
                                                                                    'equipment_id'           => $equipmentUnit,
                                                                                    'odometer_reading'       => $odometerReading,
                                                                                    'associated_record'      => $workRecord->work_record_id,
                                                                                    'associated_record_type' => '2',
                                                                                ]);


                    // If the odometer reading creation was not successful
                    // we have to undo everything up to this point.

                    if (!$odometerSuccess) {
                        $this->errors = $this->odometerReadingRepository->errors();
                        Event::fire('workentry.record.equipment.created.failed', array($recordDetails));
                        DB::connection(\Tenant::getCurrentConnection())->rollback();

                        return false;
                    }
                }

                Event::fire('workentry.record.equipment.created', array($workRecord));
                DB::connection(\Tenant::getCurrentConnection())->commit();

                return $workRecord;
            } else {
                $this->errors = $workRecord->errors();
            }
        } catch (Exception $e) {
        }

        $this->errors = $workRecord->errors();
        Event::fire('workentry.record.equipment.created.failed', array($recordDetails));
        DB::connection(\Tenant::getCurrentConnection())->rollback();

        return false;
    }

    /**
     * Removes an existing record.
     *
     * The first element of the $removeDetails array should be the work record ID
     * of the associated record to be removed.
     *
     * @param  array $removeDetails
     * @return mixed
     */
    public function remove(array $removeDetails = array())
    {
        DB::connection(\Tenant::getCurrentConnection())->beginTransaction();

        try {
            if (array_key_exists(1, $removeDetails)) {
                Event::fire('workentry.record.equipment.removed', array($removeDetails[0]));
                $this->odometerReadingRepository->removeAssociated(2, $removeDetails[0]);
                DB::connection(\Tenant::getCurrentConnection())->commit();

                return true;
            } else {
                return true;
            }
        } catch (Exception $e) {
        }

        DB::connection(\Tenant::getCurrentConnection())->rollback();
        Event::fire('workentry.record.equipment.removed.failed', array($removeDetails[0]));

        return false;
    }

    /**
     * Updates an existing record.
     *
     * @param  int   $recordID
     * @param  array $updatedRecordDetails
     * @return mixed
     */
    public function update($recordID, array $updatedRecordDetails)
    {
        $workRecord = EquipmentWorkRecord::findOrFail($recordID);

        if ($workRecord == null) {
            throw new ModelNotFoundException;
        }

        $workRecord->shop_costs             = $updatedRecordDetails['shop_costs'];
        $workRecord->purchased_costs        = $updatedRecordDetails['purchased_costs'];
        $workRecord->commercial_labor_costs = $updatedRecordDetails['commercial_labor_costs'];
        $workRecord->commercial_costs       = $updatedRecordDetails['commercial_costs'];
        $workRecord->work_order_number      = $updatedRecordDetails['work_order_number'];

        DB::connection(\Tenant::getCurrentConnection())->beginTransaction();

        try {
            if ($workRecord->updateUniques()) {
                Event::fire('workentry.record.equipment.updated', array($workRecord));

                $odometerUpdateSuccess = $this->odometerReadingRepository->updateOdometerReadingByAssociated(2,
                                                                                                             $updatedRecordDetails['work_record_id'],
                                                                                                             $updatedRecordDetails['odometer_reading']);

                if (!$odometerUpdateSuccess) {
                    DB::connection(\Tenant::getCurrentConnection())->rollback();
                    $this->errors = $this->odometerReadingRepository->errors();

                    return false;
                }

                DB::connection(\Tenant::getCurrentConnection())->commit();

                return $workRecord;
            } else {
                $this->errors = $workRecord->errors();
            }
        } catch (Exception $e) {
        }

        DB::connection(\Tenant::getCurrentConnection())->rollback();
        Event::fire('workentry.record.equipment.updated.failed', array($recordID));

        return false;
    }

    public function isValid($validateData)
    {
        $workRecord                         = new EquipmentWorkRecord;
        $workRecord->work_record_id         = 0;
        $workRecord->shop_costs             = $validateData['shop_costs'];
        $workRecord->purchased_costs        = $validateData['purchased_costs'];
        $workRecord->commercial_labor_costs = $validateData['commercial_labor_costs'];
        $workRecord->work_description       = $validateData['work_description'];

        $success = $workRecord->validate();

        if ($success == true) {
            return true;
        }

        $this->errors = $workRecord->errors();

        return false;
    }
}
