<?php namespace ParadoxOne\NDCounties\Database\Repositories\EquipmentUnits;

use Exception;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Event;
use ParadoxOne\NDCounties\Contracts\EquipmentUnits\OdometerReadingRepositoryInterface;
use ParadoxOne\NDCounties\Database\Repositories\BaseTenantRepository;
use Service\EquipmentUnits\OdometerReading;

class OdometerReadingRepository extends BaseTenantRepository implements OdometerReadingRepositoryInterface
{
    protected $table = 'equipment_odometer_readings';

    public function create(array $odometerData = array())
    {
        $odometerReading                         = new OdometerReading;
        $odometerReading->equipment_id           = $odometerData['equipment_id'];
        $odometerReading->odometer_reading       = $odometerData['odometer_reading'];
        $odometerReading->associated_record      = $odometerData['associated_record'];
        $odometerReading->associated_record_type = $odometerData['associated_record_type'];
        if ($odometerReading->save()) {
            Event::fire('equipment.odometer.created', array($odometerReading));

            return $odometerReading;
        } else {
            $this->errors = $odometerReading->errors();

            return false;
        }
    }

    public function update($id, array $odometerData = array())
    {
        $odometerReading                         = OdometerReading::findOrFail($id);
        $odometerReading->equipment_id           = $odometerData['equipment_id'];
        $odometerReading->odometer_reading       = $odometerData['odometer_reading'];
        $odometerReading->associated_record      = $odometerData['associated_record'];
        $odometerReading->associated_record_type = $odometerData['associated_record_type'];

        if ($odometerReading->updateUniques()) {
            Event::fire('equipment.odometer.updated', array($odometerReading));

            return $odometerReading;
        } else {
            Event::fire('equipment.odometer.updated.failed', array($id));
            $this->errors = $odometerReading->errors();

            return false;
        }
    }

    public function remove(array $removeData = array())
    {
        try {
            if ($removeData[0] != 0) {
                Event::fire('equipment.odometer.removed', array($removeData[0]));
                OdometerReading::destroy($removeData[0]);

                return true;
            }
        } catch (Exception $e) {
        }

        Event::fire('equipment.odometer.removed.failed', array($removeData[0]));

        return false;
    }

    /**
     * Removes an odometer reading based on the associated type, ID and the
     * work record ID for the odometer record ID.
     *
     * @param int $associatedType
     * @param int $associatedID
     * @return boolean
     */
    public function removeAssociated($associatedType, $associatedID)
    {
        DB::connection(\Tenant::getCurrentConnection())->beginTransaction();

        try {
            OdometerReading::where('associated_record_type', '=', $associatedType)
                           ->andWhere('associated_record', '=', $associatedID)->delete();

            return true;
        } catch (Exception $e) {
        }

        DB::connection(\Tenant::getCurrentConnection())->rollback();

        return false;
    }

    /**
     * Returns the odometer reading based on the associated type and the ID
     * for the associated type.
     *
     * @param int $associatedType The associated type.
     * @param int $associatedID   The associated type's ID.
     * @return mixed
     */
    public function getOdometerReadingByAssociated($associatedType, $associatedID)
    {
        // TODO: Implement getOdometerReadingByAssociated() method.
    }

    /**
     * Updates an odometer reading based on the associated type, and the ID for the associated
     * type.
     *
     * @param int $associatedType
     * @param int $associatedID
     * @param int $newOdometerReading
     * @return mixed
     */
    public function updateOdometerReadingByAssociated($associatedType, $associatedID, $newOdometerReading)
    {
        $odometerReading                   = OdometerReading::where('associated_record_type', '=', $associatedType)
                                                            ->andWhere('associated_record', '=', $associatedID)
                                                            ->firstOrFail();
        $odometerReading->odometer_reading = $newOdometerReading;

        if ($odometerReading->updateUniques()) {
            Event::fire('equipment.odometer.updated', array($odometerReading));

            return $odometerReading;
        } else {
            Event::fire('equipment.odometer.updated.failed', array($associatedID));
            $this->errors = $odometerReading->errors();

            return false;
        }
    }
}
