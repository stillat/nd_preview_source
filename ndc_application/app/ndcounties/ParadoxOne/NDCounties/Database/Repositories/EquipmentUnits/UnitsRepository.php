<?php namespace ParadoxOne\NDCounties\Database\Repositories\EquipmentUnits;

use Exception;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\Facades\Input;
use ParadoxOne\NDCounties\Contracts\EquipmentUnits\UnitsRepositoryInterface;
use ParadoxOne\NDCounties\Database\Repositories\BaseTenantRepository;
use Service\EquipmentUnits\Unit as EquipmentUnit;

class UnitsRepository extends BaseTenantRepository implements UnitsRepositoryInterface
{
    protected $table = 'properties';

    protected $createPropertyID = 0;

    public function setCreateProperty($id)
    {
        $this->createPropertyID = $id;
    }

    public function isValid(array $data = array())
    {
        $equipmentData                = new EquipmentUnit;
        $equipmentData->year          = $data['equipment_year'];
        $equipmentData->make          = $data['equipment_make'];
        $equipmentData->model         = $data['equipment_model'];
        $equipmentData->serial_number = $data['equipment_serial_number'];
        $equipmentData->rental_rate   = $data['equipment_rental_rate'];
        $equipmentData->unit_id       = $data['equipment_rental_rate_unit'];
        $equipmentData->purchase_cost = $data['equipment_purchase_cost'];
        $equipmentData->salvage_value = $data['equipment_salvage_value'];
        $equipmentData->lifetime      = $data['equipment_lifetime'];

        $success = $equipmentData->validate();

        if ($success == true) {
            return true;
        }

        $this->errors = $equipmentData->errors();

        return false;
    }


    public function create(array $unitInformation = array())
    {
        $unit = new EquipmentUnit;

        // The ID is passed in by the handlers.
        $unit->property_id = $this->createPropertyID;

        $unit->year          = $unitInformation['equipment_year'];
        $unit->make          = $unitInformation['equipment_make'];
        $unit->model         = $unitInformation['equipment_model'];
        $unit->serial_number = $unitInformation['equipment_serial_number'];
        $unit->rental_rate   = $unitInformation['equipment_rental_rate'];
        $unit->unit_id       = $unitInformation['equipment_rental_rate_unit'];
        $unit->purchase_cost = $unitInformation['equipment_purchase_cost'];
        $unit->salvage_value = $unitInformation['equipment_salvage_value'];
        $unit->lifetime      = $unitInformation['equipment_lifetime'];

        if (strlen($unit->lifetime) == 0) {
            $unit->lifetime = null;
        }
        if (strlen($unit->year) == 0) {
            $unit->year = null;
        }

        if ($unit->save()) {
            Event::fire('property.equipment.created', array($unit));

            return $unit;
        } else {
            $this->errors = $unit->errors();

            return false;
        }
    }

    public function update($id, array $unitInformation = array())
    {
        DB::connection(\Tenant::getCurrentConnection())->beginTransaction();

        try {
            $unit                = EquipmentUnit::where('property_id', '=', $id)->firstOrFail();
            $unit->year          = $unitInformation['equipment_year'];
            $unit->make          = $unitInformation['equipment_make'];
            $unit->model         = $unitInformation['equipment_model'];
            $unit->serial_number = $unitInformation['equipment_serial_number'];
            $unit->rental_rate   = $unitInformation['equipment_rental_rate'];
            $unit->unit_id       = $unitInformation['equipment_rental_rate_unit'];
            $unit->purchase_cost = $unitInformation['equipment_purchase_cost'];
            $unit->salvage_value = $unitInformation['equipment_salvage_value'];
            $unit->lifetime      = $unitInformation['equipment_lifetime'];


            if (strlen($unit->lifetime) == 0) {
                $unit->lifetime = null;
            }
            if (strlen($unit->year) == 0) {
                $unit->year = null;
            }

            if ($unit->save()) {
                Event::fire('property.equipment.updated', array($unit));
                DB::connection(\Tenant::getCurrentConnection())->commit();

                return $unit;
            }

            $this->errors = $unit->errors();
        } catch (Exception $e) {
            lk($e);
        }

        DB::connection(\Tenant::getCurrentConnection())->rollback();

        return false;
    }

    public function remove(array $removeContext = array())
    {
        DB::connection(\Tenant::getCurrentConnection())->beginTransaction();

        try {
            if ($removeContext[0] != 0) {
                if (array_key_exists(1, $removeContext)) {
                    // This si the part that will be used when the system is trashing
                    // previously soft-deleted equipment units.
                    Event::fire('property.equipment.removed', array($removeContext[0]));
                    EquipmentUnit::where('property_id', '=', $removeContext[0])->delete();
                    DB::connection(\Tenant::getCurrentConnection())->commit();

                    return true;
                } else {
                    // Just return true. This is used when a user is only soft-deleting the parent
                    // property record.
                    return true;
                }
            }
        } catch (Exception $e) {
        }

        DB::connection(\Tenant::getCurrentConnection())->rollback();
        Event::fire('property.equipment.removed.failed', array($removeContext[0]));

        return false;
    }


    public function getModelById($id)
    {
        Event::fire('property.equipment.accessed', array($id));
        $equipment =
            $this->getUnitsBuilder()->where('properties.id', '=', $id)->first();

        if ($equipment == null) {
            throw new ModelNotFoundException;
        }

        return $equipment;
    }

    public function getUnits()
    {
        return $this->getUnitsBuilder()->get();
    }

    private function getUnitsBuilder()
    {
        return $this->table()->select(
            DB::raw('properties.*'),
            DB::raw('year,make,model,serial_number,rental_rate,unit_id,purchase_cost,salvage_value,lifetime')
        )
                    ->join('equipment_units', 'properties.id', '=', 'equipment_units.property_id');
    }

    public function getAccessoryUnits($id)
    {
    }

    public function addAccessoryUnitTo($id)
    {
    }

    public function search($searchTerms)
    {
        if ($searchTerms == '/p') {
            return [];
        }

        if ($searchTerms == '/u') {
            $searchTerms = '//';
        }

        //if (Input::has('context') and Input::get('context', 0) == 1)
        //{
        if (Input::get('is', null) !== null) {
            return [$this->getModelById(Input::get('is'))];
        }
        //}

        if ($searchTerms === '//') {
            return $this->getUnitsBuilder()->whereNull('properties.deleted_at')->get();
        }

        return $this->getUnitsBuilder()
                    ->orWhere(function ($query) use ($searchTerms) {
                        $query->where('properties.name', 'LIKE', '%' . $searchTerms . '%')
                              ->orWhere('properties.description', 'LIKE', '%' . $searchTerms . '%')
                              ->orWhere('equipment_units.serial_number', 'LIKE', '%' . $searchTerms . '%')
                              ->orWhere('properties.code', 'LIKE', '%' . $searchTerms . '%');
                    })
            ->orWhere(DB::raw('REPLACE(properties.code, \'-\', \'\')'), '=', $searchTerms)->orderByRaw('
                                CASE WHEN properties.code = \''.$searchTerms.'\' THEN 0
                                WHEN properties.code LIKE \''.$searchTerms.'%\' THEN 1
                                WHEN properties.code LIKE \'%'.$searchTerms.'%\' THEN 2
                                ELSE 3
                                END
                               ')->limit(10)
                    ->where(function ($query) {
                        $query->whereNull('properties.deleted_at');
                    })
                    ->get();
    }
}
