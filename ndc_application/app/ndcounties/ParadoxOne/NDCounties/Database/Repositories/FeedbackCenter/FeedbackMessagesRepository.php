<?php namespace ParadoxOne\NDCounties\Database\Repositories\FeedbackCenter;

use CustomerService\FeedbackMessage;
use ParadoxOne\NDCounties\Contracts\CustomerService\FeedbackMessagesRepositoryInterface;
use ParadoxOne\NDCounties\Database\Repositories\BaseRepository;

class FeedbackMessagesRepository extends BaseRepository implements FeedbackMessagesRepositoryInterface
{
    protected $table = 'feedback_messages';

    public function getMessages($limit = 10)
    {
        $messages = FeedbackMessage::with('author')->orderBy('created_at', 'DESC');

        if ($limit !== null) {
            $messages = $messages->take($limit);
        }

        return $messages->get();
    }

    public function create(array $messageInformation = array())
    {
        $message              = new FeedbackMessage;
        $message->subject     = $messageInformation['subject'];
        $message->category_id = $messageInformation['category_id'];
        $message->message     = $messageInformation['message'];
        $message->user_id     = $messageInformation['user_id'];

        if ($message->save()) {
            return $message;
        } else {
            $this->errors = $message->errors();

            return false;
        }
    }

    public function update($id, array $messageInformation = array())
    {
    }

    public function remove(array $removeContext = array())
    {
    }

    public function count()
    {
        return $this->table()->whereNull('deleted_at')->count();
    }
}
