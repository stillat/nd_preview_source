<?php namespace ParadoxOne\NDCounties\Database\Repositories;

use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use ParadoxOne\NDCounties\Contracts\ActivityStreamRepositoryInterface;

class ActivityStreamRepository extends BaseRepository implements ActivityStreamRepositoryInterface
{
    protected $table = 'audit_logs';

    private $protectedActionClasses = [18, 19, 20, 21];

    /**
     * Creates a new record.
     *
     * @param  array $recordDetails
     * @return mixed
     * @throws \Exception
     */
    public function create(array $recordDetails = array())
    {
        throw new \Exception('Not Implemented');
    }

    /**
     * Removes an existing record.
     *
     * @param  array $removeDetails
     * @return mixed
     * @throws \Exception
     */
    public function remove(array $removeDetails = array())
    {
        throw new \Exception('Not Implemented');
    }

    /**
     * Updates an existing record.
     *
     * @param  int   $recordID
     * @param  array $newRecordDetails
     * @return mixed
     * @throws \Exception
     */
    public function update($recordID, array $newRecordDetails)
    {
        throw new \Exception('Not Implemented');
    }


    /**
     * Logs an event into the audit log.
     *
     * @param $userID
     * @param $accountID
     * @param $action
     * @param $actionContext
     * @param $actionClass
     * @param $actionMeta
     * @param $performedOn
     * @return mixed
     */
    public function logActivity($userID, $accountID = null, $action = null, $actionContext = null, $actionClass = null, $actionMeta = null, $performedOn = null)
    {
        DB::beginTransaction();

        try {
            if (!is_array($userID)) {
                $this->table()->insert([
                    'associated_user'  => $userID,
                    'account_id'       => $accountID,
                    'action_performed' => $action,
                    'action_context'   => $actionContext,
                    'action_class'     => $actionClass,
                    'action_meta'      => $actionMeta,
                    'performed_on'     => (new Carbon())->toDateTimeString()
                ]);
            } else {
                $activityRecords = [];

                foreach ($userID as $record) {
                    $activityRecords[] = [
                        'associated_user'  => $record[0],
                        'account_id'       => $record[1],
                        'action_performed' => $record[2],
                        'action_context'   => $record[3],
                        'action_class'     => $record[4],
                        'action_meta'      => $record[5],
                        'performed_on'     => (new Carbon())->toDateTimeString()
                    ];
                }

                $inserts = $this->table()->insert($activityRecords);
            }


            DB::commit();

            return true;
        } catch (\Exception $e) {
            lk($e);
        }

        DB::rollback();

        return false;
    }

    /**
     * Gets the recent activity for the account.
     *
     * @param $accountID
     * @return mixed
     */
    public function getRecentActivity($accountID)
    {
        return $this->table()->whereIn('account_id', [$accountID, 0])->orderBy('performed_on', 'DESC')
                    ->whereNotIn('action_class', $this->protectedActionClasses);

        //return $this->table()->where('account_id', '=', $accountID)->orderBy('performed_on', 'DESC')->whereNotIn('action_class', $this->protectedActionClasses);
    }

    /**
     * Resets the audit logs for the given account.
     *
     * @param $accountID
     * @return mixed
     */
    public function resetAccountLogs($accountID)
    {
        DB::beginTransaction();

        try {
            $this->table()->where('account_id', '=', $accountID)->delete();

            DB::commit();

            return true;
        } catch (\Exception $e) {
        }

        DB::rollback();

        return false;
    }

    /**
     * Resets all the account longs.
     *
     * @return mixed
     */
    public function resetAllAccountLogs()
    {
        $this->table()->delete();
    }

    public function postNotification($account, $type, $message)
    {
        DB::beginTransaction();

        if (strlen(trim($message)) == 0) {
            return false;
        }

        try {
            $meta = new \stdClass;
            $meta->message = nl2br($message, true);
            $meta = json_encode($meta);
            $this->table()->insert([
                                       'associated_user'  => 0,
                                       'account_id'       => $account,
                                       'action_performed' => 0,
                                       'action_context'   => $type,
                                       'action_class'     => 25,
                                       'action_meta'      => $meta,
                                       'performed_on'     => (new Carbon())->toDateTimeString()
                                   ]);

            DB::commit();

            return true;
        } catch (\Exception $e) {
        }

        DB::rollback();

        return false;
    }
}
