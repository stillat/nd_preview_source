<?php namespace ParadoxOne\NDCounties\Database\Repositories;

use Illuminate\Support\Facades\DB;
use Stillat\Database\Repositories\BaseRepository as StillatBaseRepository;

abstract class BaseRepository extends StillatBaseRepository
{
    protected $errors = null;

    public function errors()
    {
        return $this->errors;
    }

    public function table()
    {
        return DB::table($this->table);
    }
}
