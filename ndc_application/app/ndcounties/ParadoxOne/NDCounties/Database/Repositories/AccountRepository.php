<?php namespace ParadoxOne\NDCounties\Database\Repositories;

use App\Account;
use App\AccountSettings;
use Aws\CloudFront\Exception\Exception;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\MessageBag;
use ParadoxOne\NDCounties\Contracts\AccountRepositoryInterface;
use ParadoxOne\NDCounties\Processes\ProcessManager;
use Service\CountySettings;
use Stillat\Database\Tenant\TenantManager;

class AccountRepository extends BaseRepository implements AccountRepositoryInterface
{
    /**
     * The table for the repository.
     *
     * @var string
     */
    protected $table = 'accounts';

    /**
     * {@inheritdoc}
     */
    public function addSettingsToAccount()
    {
        $settings                         = new CountySettings;
        $settings->use_districts          = 1;
        $settings->show_employee_overtime = 1;
        $settings->data_decimal_places    = 2;
        $settings->auto_process_invoice   = false;
        $settings->invoice_default_status = 2;


        if ($settings->save()) {
            return $settings;
        } else {
            $this->errors = $settings->errors();

            return false;
        }
    }

    /**
     * Adds contact settings to an account.
     *
     * @param int $account
     * @return mixed|void
     */
    public function addContactSettingsToAccount($account)
    {
        $settings             = new AccountSettings;
        $settings->account_id = $account;
        $settings->save();
    }


    public function create(array $accountInformation = array())
    {
    }

    public function remove(array $removeDetails = array())
    {
    }

    public function update($accountID, array $updateDetails = array())
    {
    }

    /**
     * {@inheritdoc}
     */
    public function getMissingAccounts($user_id)
    {
    }

    /**
     * Returns the account settings for the account ID
     *
     *
     * @param  int $id The ID of the account.
     * @return mixed
     */
    public function getAccountSettings($id)
    {
        $instance = TenantManager::instance();
        $instance->assumeTenant($id, true);
        $settings = CountySettings::on($instance->getCurrentConnection())->first();
        $instance->restoreTenant();
        unset($instance);

        return $settings;
    }


    /**
     * {@inheritdoc}
     */
    public function getAccounts($userID)
    {
        return DB::table('tenant_accounts')
                 ->select('accounts.account_name', 'accounts.created_at', 'accounts.id', 'accounts.updated_at',
                          'tenants.tenant_name', 'tenant_accounts.tenant_id', 'tenants.active')
                 ->join('tenants', 'tenant_accounts.tenant_id', '=', 'tenants.id')
                 ->join('accounts', 'tenants.id', '=', 'accounts.tenant_id')
                 ->where('tenant_accounts.user_id', '=', $userID)
                 ->where('accounts.id', '>', 0);
    }

    /**
     * {@inheritdoc}
     */
    public function getAllAccounts()
    {
        return $this->table()
                    ->select('accounts.account_name', 'accounts.id', 'accounts.created_at', 'accounts.updated_at',
                             'tenants.tenant_name', 'accounts.id', 'tenants.active', 'accounts.tenant_id')
                    ->join('tenants', 'accounts.tenant_id', '=', 'tenants.id')->where('accounts.id', '>', 0);
    }

    /**
     * Returns only the basic account information.
     *
     * @param $accountID
     * @return mixed
     */
    public function getAccountInformation($accountID)
    {
        return $this->table()->where('id', '=', $accountID)->first();
    }


    /**
     * {@inheritdoc}
     */
    public function getAccount($accountID)
    {
        return Cache::rememberForever('func_get_account_' . $accountID, function () use ($accountID) {
            return $this->table()->select('account_settings.address_line_1', 'account_settings.address_line_2',
                                          'account_settings.city', 'account_settings.state', 'account_settings.country',
                                          'account_settings.zip', 'accounts.account_name', 'accounts.id',
                                          'accounts.created_at', 'accounts.updated_at', 'tenants.tenant_name',
                                          'accounts.id', 'tenants.active', 'accounts.tenant_id',
                                          'accounts.process_running')
                        ->join('tenants', 'accounts.tenant_id', '=', 'tenants.id')
                        ->join('account_settings', 'accounts.id', '=', 'account_settings.account_id')
                        ->where('accounts.id', '=', $accountID)->first();
        });
    }

    /**
     * {@inheritdoc}
     */
    public function getLogicalPrimaryUserAccount($userID)
    {
        $serviceAccounts = $this->getAccounts($userID)->get();

        if (count($serviceAccounts) == 0) {
            return null;
        }

        $primaryAccount                         = $serviceAccounts[0];
        $primaryAccount->total_service_accounts = count($serviceAccounts);

        unset($serviceAccounts);

        return $primaryAccount;
    }


    /**
     * Updates the accounts contact settings.
     *
     * @param       $accountID
     * @param array $newSettings
     * @return mixed
     */
    public function updateAccountContactSettings($accountID, array $newSettings)
    {
        DB::beginTransaction();
        try {
            $settings                 = AccountSettings::where('account_id', '=', $accountID)->first();
            $settings->address_line_1 = $newSettings['address_line_1'];
            $settings->address_line_2 = $newSettings['address_line_2'];
            $settings->city           = $newSettings['city'];
            $settings->state          = $newSettings['state'];
            $settings->country        = $newSettings['country'];
            $settings->zip            = $newSettings['zip'];

            $account               = Account::where('id', '=', $accountID)->first();
            $account->account_name = $newSettings['account_name'];


            $accountSave = $account->updateUniques();

            if (!$accountSave) {
                if ($this->errors instanceof MessageBag) {
                    $this->errors = $this->errors->all();
                } else {
                    $this->errors = new MessageBag;
                }

                $this->errors = $this->errors->merge($account->errors());
            }


            if ($settings->save() && $accountSave) {
                Cache::forget(Auth::user()->id . '_currentAccount');
                Cache::forget('func_get_account_'.$accountID);
                Event::fire('county.settings.updated', [$settings]);
                DB::commit();
                return true;
            }
        } catch (\Exception $e) {
        }
        DB::rollback();

        return false;
    }

    /**
     * Updates the accounts data settings.
     *
     * @param       $accountID
     * @param array $newSettings
     * @return mixed
     */
    public function updateAccountDataSettings($accountID, array $newSettings)
    {
        try {
            $instance = TenantManager::instance();
            $instance->assumeTenant(Auth::user()->last_tenant);

            $useDistricts         = false;
            $showEmployeeOvertime = false;

            if (isset($newSettings['use_districts'])) {
                $useDistricts = $newSettings['use_districts'];
            }

            if (isset($newSettings['show_employee_overtime'])) {
                $showEmployeeOvertime = $newSettings['show_employee_overtime'];
            }

            $countySettings                         = CountySettings::first();
            $countySettings->use_districts          = $useDistricts;
            $countySettings->show_employee_overtime = $showEmployeeOvertime;
            $countySettings->data_decimal_places    = $newSettings['data_decimal_places'];

            if (array_key_exists('auto_process_invoice', $newSettings)) {
                $countySettings->auto_process_invoice = $newSettings['auto_process_invoice'];
            } else {
                $countySettings->auto_process_invoice = false;
            }

            $countySettings->invoice_default_status = $newSettings['default_invoice_status'];

            if ($countySettings->save()) {
                Event::fire('county.settings.updated', [$countySettings]);
                return true;
            }
        } catch (\Exception $e) {
        }

        return false;
    }

    public function updateAccountDisplaySettings(array $input)
    {
        $formatterManager = App::make('ParadoxOne\NDCounties\Settings\CountyFormattingSettingsManager');

        $newFormattingSettings = new \stdClass();
        $transformation        = $input['text_transformation'];
        $validFormats          = [1, 2, 3];

        $newFormattingSettings->transform    = new \stdClass();
        $newFormattingSettings->employee     = new \stdClass();
        $newFormattingSettings->fuel         = new \stdClass();
        $newFormattingSettings->material     = new \stdClass();
        $newFormattingSettings->activity     = new \stdClass();
        $newFormattingSettings->department   = new \stdClass();
        $newFormattingSettings->district     = new \stdClass();
        $newFormattingSettings->project      = new \stdClass();
        $newFormattingSettings->property     = new \stdClass();
        $newFormattingSettings->useOnRecords = new \stdClass();


        if (in_array($transformation, [1, 2, 3])) {
            $newFormattingSettings->transform->transform = $transformation;
        } else {
            $newFormattingSettings->transform->transform = 1;
        }


        if (in_array($input['employee_format'], $validFormats)) {
            $newFormattingSettings->employee->format = $input['employee_format'];
        } else {
            $newFormattingSettings->employee->format = 1;
        }

        if (in_array($input['material_format'], $validFormats)) {
            $newFormattingSettings->material->format = $input['material_format'];
        } else {
            $newFormattingSettings->material->format = 1;
        }

        if (in_array($input['fuel_format'], $validFormats)) {
            $newFormattingSettings->fuel->format = $input['fuel_format'];
        } else {
            $newFormattingSettings->fuel->format = 1;
        }

        if (in_array($input['activity_format'], $validFormats)) {
            $newFormattingSettings->activity->format = $input['activity_format'];
        } else {
            $newFormattingSettings->activity->format = 1;
        }

        if (in_array($input['department_format'], $validFormats)) {
            $newFormattingSettings->department->format = $input['department_format'];
        } else {
            $newFormattingSettings->department->format = 1;
        }

        if (in_array($input['district_format'], $validFormats)) {
            $newFormattingSettings->district->format = $input['district_format'];
        } else {
            $newFormattingSettings->district->format = 1;
        }

        if (in_array($input['project_format'], $validFormats)) {
            $newFormattingSettings->project->format = $input['project_format'];
        } else {
            $newFormattingSettings->project->format = 1;
        }

        if (in_array($input['property_format'], $validFormats)) {
            $newFormattingSettings->property->format = $input['property_format'];
        } else {
            $newFormattingSettings->property->format = 1;
        }

        $newFormattingSettings->useOnRecords->useOnRecords = array_key_exists('work_record_display_honor', $input);

        $formatterManager->set('ct_formatting_public', json_encode($newFormattingSettings));

        Event::fire('county.settings.updated', [$newFormattingSettings]);
        return true;
    }

    public function updateAccountHeadingDisplaySettings(array $input)
    {
        $formatterManager = App::make('ParadoxOne\NDCounties\Settings\CountyFormattingSettingsManager');

        $newFormattingSettings = new \stdClass();
        $transformation        = $input['text_transformation'];
        $validFormats          = [1, 2, 3];

        $newFormattingSettings->transform    = new \stdClass();
        $newFormattingSettings->employee     = new \stdClass();
        $newFormattingSettings->fuel         = new \stdClass();
        $newFormattingSettings->material     = new \stdClass();
        $newFormattingSettings->activity     = new \stdClass();
        $newFormattingSettings->department   = new \stdClass();
        $newFormattingSettings->district     = new \stdClass();
        $newFormattingSettings->project      = new \stdClass();
        $newFormattingSettings->property     = new \stdClass();


        if (in_array($transformation, [1, 2, 3])) {
            $newFormattingSettings->transform->transform = $transformation;
        } else {
            $newFormattingSettings->transform->transform = 1;
        }


        if (in_array($input['employee_format'], $validFormats)) {
            $newFormattingSettings->employee->format = $input['employee_format'];
        } else {
            $newFormattingSettings->employee->format = 1;
        }

        if (in_array($input['material_format'], $validFormats)) {
            $newFormattingSettings->material->format = $input['material_format'];
        } else {
            $newFormattingSettings->material->format = 1;
        }

        if (in_array($input['fuel_format'], $validFormats)) {
            $newFormattingSettings->fuel->format = $input['fuel_format'];
        } else {
            $newFormattingSettings->fuel->format = 1;
        }

        if (in_array($input['activity_format'], $validFormats)) {
            $newFormattingSettings->activity->format = $input['activity_format'];
        } else {
            $newFormattingSettings->activity->format = 1;
        }

        if (in_array($input['department_format'], $validFormats)) {
            $newFormattingSettings->department->format = $input['department_format'];
        } else {
            $newFormattingSettings->department->format = 1;
        }

        if (in_array($input['district_format'], $validFormats)) {
            $newFormattingSettings->district->format = $input['district_format'];
        } else {
            $newFormattingSettings->district->format = 1;
        }

        if (in_array($input['project_format'], $validFormats)) {
            $newFormattingSettings->project->format = $input['project_format'];
        } else {
            $newFormattingSettings->project->format = 1;
        }

        if (in_array($input['property_format'], $validFormats)) {
            $newFormattingSettings->property->format = $input['property_format'];
        } else {
            $newFormattingSettings->property->format = 1;
        }
        $formatterManager->set('ct_formatting_heading', json_encode($newFormattingSettings));

        Event::fire('county.settings.updated', [$newFormattingSettings]);
        return true;
    }


    /**
     * Sets an account current process.
     *
     * @param $accountID
     * @param $process
     * @return mixed
     */
    public function setAccountProcess($accountID, $process)
    {
        DB::beginTransaction();

        try {
            $this->table()
                 ->join('tenants', 'accounts.tenant_id', '=', 'tenants.id')
                 ->where('accounts.id', '=', $accountID)->update(['process_running' => $process]);
            DB::commit();

            return true;
        } catch (\Exception $e) {
        }
        DB::rollback();

        return false;
    }

    /**
     * Releases all running processes.
     *
     * @param $accountID
     * @return mixed
     */
    public function releaseProcesses($accountID)
    {
        DB::beginTransaction();

        try {
            $this->getTable()->where('id', '=', $accountID)
                 ->update(['process_running' => ProcessManager::PROCESS_NONE]);
            DB::commit();

            return true;
        } catch (\Exception $e) {
        }
        DB::rollback();

        return false;
    }

    /**
     * Gets all the users associated with an account.
     *
     * @param $accountID
     * @return mixed
     */
    public function getAccountUsers($accountID)
    {
        $builder = DB::table('users')->select(DB::raw('users.*'))
                     ->join('tenant_accounts', 'users.id', '=', 'tenant_accounts.user_id')
                     ->join('accounts', 'tenant_accounts.tenant_id', '=', 'accounts.tenant_id')
                     ->where('accounts.id', '=', $accountID);

        return $builder;
    }

    public function setActiveStatus($account, $isActive)
    {
        DB::table('tenants')->where('id', $account)->update(['active' => (booL)$isActive]);
    }

    public function removeAccount($accountID)
    {
        try {
            $account = $this->getAccount($accountID);
            $tenant = $account->tenant_name;
            $dropscript = 'SET FOREIGN_KEY_CHECKS=0;';

            $truncate = [
                'activities', 'addresses', 'consumables', 'county_meta', 'county_settings', 'departments', 'districts', 'employees', 'equipment_odometer_readings', 'equipment_units', 'equipment_usage', 'equipment_work_record_repairs', 'fs_accounts', 'fs_accounts_categories', 'fs_invoice_work_records', 'fs_invoices', 'fs_invoices_tax_rates', 'fs_supplier_notes', 'fs_suppliers', 'fs_tax_rates', 'fs_transactions', 'gps_entries', 'inventory_adjustments', 'migrations', 'phone_numbers', 'projects', 'properties', 'property_adapters', 'property_categories', 'property_property', 'property_road_properties', 'property_road_surface_types', 'property_types', 'road_work_record_logs', 'system_information_logs', 'work_consumables', 'work_entries'
            ];
            foreach ($truncate as $table) {
                $dropscript .= 'TRUNCATE `'.$tenant.'`.`'.$table.'`;';
            }
            $dropscript .= 'SET FOREIGN_KEY_CHECKS=1;';
            DB::connection('mysql')->table('users')->where('last_tenant', '=', $account->id)->update(['last_tenant' => 0]);
            DB::table('accounts')->where('id', '=', $account->id)->delete();
            DB::table('tenant_accounts')->where('tenant_id', '=', $account->tenant_id)->delete();
            DB::table('account_settings')->where('account_id', '=', $account->id)->delete();
            DB::table('tenants')->where('id', '=', $account->tenant_id)->delete();
            DB::table('audit_logs')->where('account_id', '=', $account->id)->delete();
            DB::table('backup_logs')->where('tenant_id', '=', $account->id)->delete();
            DB::table('system_reports')->where('account_id', '=', $account->id)->delete();
            DB::table('system_reports_default_totals')->where('account_id', '=', $account->id)->delete();
            DB::table('system_reports_total_blocks')->where('account_id', '=', $account->id)->delete();
            DB::table('county_wide_application_settings')->where('service_account_id', '=', $account->id)->delete();
            DB::unprepared($dropscript);

            return true;
        } catch (\Exception $e) {
            lk($e);
        }

        return false;
    }
}
