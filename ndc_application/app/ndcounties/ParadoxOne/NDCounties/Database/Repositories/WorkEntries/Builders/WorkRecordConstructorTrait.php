<?php namespace ParadoxOne\NDCounties\Database\Repositories\WorkEntries\Builders;

use ParadoxOne\NDCounties\Database\Entities\WorkEntries\WorkEntry;
use ParadoxOne\NDCounties\Database\Repositories\WorkEntries\InvalidWorkEntryException;

trait WorkRecordConstructorTrait
{
    /**
     * Holds the constructed work records.
     *
     * @var array
     */
    private $constructedWorkRecords = [];

    private function fixMaterialConsumableID($value)
    {
        if ($value == 1) {
            return 0;
        }

        return $value;
    }

    /**
     * Constructs the work records that will be delivered to other core
     * systems.
     */
    private function constructWorkRecords()
    {
        if (count($this->workEntryRecords) == 0) {
            return;
        }

        $equipmentAdapters = $this->adapterRecords['e'];

        foreach ($this->workEntryRecords as $record) {
            try {
                $recordMaterialConsumables =
                    array_pluck_where($this->consumableMaterialRecords, 'work_entry_id', $record->id);
                $recordOdometerReadings    =
                    array_pluck_where($this->odometerReadingRecords, 'associated_record', $record->id);

                for ($i = 0; $i < count($recordMaterialConsumables); $i++) {
                    $currentMaterialConsumableID = $recordMaterialConsumables[$i]->consumable_id;

                    try {
                        $currentMaterialConsumableID = $this->fixMaterialConsumableID($currentMaterialConsumableID);
                        $recordMaterialConsumables[$i]->material =
                            array_pluck_where($this->materialRecords, 'id', $currentMaterialConsumableID)[0];

                        $recordMaterialConsumables[$i]->material->unit = array_pluck_where($this->measurementUnits, 'id',
                                                                                           $recordMaterialConsumables[$i]->material->unit_id)[0];
                    } catch (\Exception $materialException) {
                        $weMaterialException = new InvalidWorkEntryException($materialException->getMessage().' for work entry '.$record->id, $materialException->getCode(), $materialException);
                        $weMaterialException->materialConsumable = $currentMaterialConsumableID;
                        $weMaterialException->fixedMaterialConsumableID = $this->fixMaterialConsumableID($currentMaterialConsumableID);
                        throw $weMaterialException;
                    }
                }

                $recordEquipmentConsumables =
                    array_pluck_where($this->consumableEquipmentRecords, 'work_entry_id', $record->id);

                $recordFuelConsumables = array_pluck_where($this->consumableFuelRecords, 'work_entry_id', $record->id);
                $record->rawFuels = $recordFuelConsumables;

                for ($i = 0; $i < count($recordEquipmentConsumables); $i++) {
                    $recordEquipmentConsumables[$i]->equipment = clone array_pluck_where($this->equipmentUnitRecords, 'id',
                                                                                         $recordEquipmentConsumables[$i]->consumable_id)[0];

                    $recordEquipmentConsumables[$i]->equipment->unit = array_pluck_where($this->measurementUnits, 'id',
                                                                                         $recordEquipmentConsumables[$i]->equipment->unit_id)[0];

                    // Odometer reading.

                    $odometerReading = array_pluck_where($recordOdometerReadings, 'associated_record_type', 1);
                    $odometerReading = array_pluck_where($odometerReading, 'equipment_id',
                                                         $recordEquipmentConsumables[$i]->id);


                    if (count($odometerReading) > 0) {
                        $recordEquipmentConsumables[$i]->equipment->odometer = $odometerReading[0];
                    } else {
                        $recordEquipmentConsumables[$i]->equipment->odometer = null;
                    }

                    // Equipment fuels.
                    // $equipmentRecordFuels = array_pluck_where($recordFuelConsumables, 'associated_id', $recordEquipmentConsumables[$i]->consumable_id);
                    //  d($equipmentRecordFuels);

                    $equipmentRecordFuels = array_pluck_where($recordFuelConsumables, 'bound_consumable_record',
                                                              $recordEquipmentConsumables[$i]->id);

                    //d($recordEquipmentConsumables[$i]->consumable_id);
                    //$equipmentRecordFuels = array_pluck_where($equipmentRecordFuels, 'associated_id', $recordEquipmentConsumables[$i]->consumable_id);
                    //d($equipmentRecordFuels);


                    for ($f = 0; $f < count($equipmentRecordFuels); $f++) {
                        $currentFuelID = 1;
                        try {
                            $currentFuelID = $equipmentRecordFuels[$f]->consumable_id;

                            if ($currentFuelID == 0) {
                                $currentFuelID = 1;
                            }

                            $equipmentRecordFuels[$f]->fuel       =
                                array_pluck_where($this->fuelRecords, 'id', $currentFuelID)[0];

                            $equipmentRecordFuels[$f]->fuel->unit =
                                array_pluck_where($this->measurementUnits, 'id', $equipmentRecordFuels[$f]->fuel->unit_id)[0];
                        } catch (\Exception $fuelException) {
                            $weFuelException = new InvalidWorkEntryException($fuelException->getMessage().' for work entry '.$record->id, $fuelException->getCode(), $fuelException);
                            $weFuelException->workEntryID = $record->id;
                            $weFuelException->equipmentRecordFuels = $equipmentRecordFuels;
                            throw $weFuelException;
                        }
                    }

                    $recordEquipmentConsumables[$i]->fuels = $equipmentRecordFuels;

                    unset($equipmentRecordFuels);
                    unset($odometerReading);
                }

                // Organization property.
                $record->organization_property = (object)array_pluck_where($this->propertyRecords, 'id', $record->property_organization_id)[0];

                $property = array_pluck_where($this->propertyRecords, 'id', $record->property_id)[0];
                $property = json_decode(json_encode($property), false);

                if ($record->property_context == 0 && $record->property_id != 0) {
                    // This is a property record where it's adapter data is discovered dynamically.
                    $adapters = array_pluck($property->type->adapters, 'adapter');

                    $propertyWorkRecords = [];
                    foreach ($adapters as $adapter) {
                        if ($adapter == 2) {
                            $adapter = 'e';
                        }
                        $potentialRecords = $this->adapterRecords[$adapter];

                        foreach ($potentialRecords as $precord) {
                            if ($precord->work_record_id == $record->id) {
                                $propertyWorkRecords[$adapter] = $precord;
                            }
                        }

                        $potentialRecords = null;
                        unset($potentialRecords);
                    }

                    $property->work_data = $propertyWorkRecords;
                    $propertyWorkRecords = null;
                    unset($propertyWorkRecords);
                } elseif ($record->property_context == 0 && $record->property_id == 0) {
                    // This is a property record where the user did not select a custom one.
                    $property            = json_decode(json_encode($property), false);
                    $property->work_data = null;
                } else {
                    // This is an equipment repair record.
                    if ($record->property_id != 0) {
                        $property->work_data = array_pluck_where($equipmentAdapters, 'work_record_id', $record->id);
                        $odometer_reading    =
                            array_pluck_where($this->odometerReadingRecords, 'associated_record', $record->id);
                        $odometer_reading    = array_pluck_where($odometer_reading, 'associated_record_type', 2);

                        if (count($odometer_reading) > 0) {
                            $property->work_data[0]->odometer = $odometer_reading[0];
                        } else {
                            $property->work_data[0]->odometer = null;
                        }
                    } else {
                        $property->work_data           = null;
                        // $property->work_data->odometer = null;
                    }
                }


                $this->constructedWorkRecords[] =
                    new WorkEntry($record, $recordEquipmentConsumables, $recordMaterialConsumables, $property);
                $recordMaterialConsumables      = null;
                $recordEquipmentConsumables     = null;
                $recordFuelConsumables          = null;
                $property                       = null;
                unset($recordMaterialConsumables);
                unset($recordEquipmentConsumables);
                unset($recordFuelConsumables);
                unset($property);
            } catch (\Exception $e) {
                throw $e;
                $weException = new InvalidWorkEntryException($e->getMessage().' for work entry '.$record->id, $e->getCode(), $e);
                $weException->workEntryID = $record->id;

                throw $weException;
            }
        }
    }
}
