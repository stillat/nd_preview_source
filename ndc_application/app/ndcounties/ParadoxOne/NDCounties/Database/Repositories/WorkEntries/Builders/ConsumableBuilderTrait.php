<?php namespace ParadoxOne\NDCounties\Database\Repositories\WorkEntries\Builders;

use Illuminate\Database\Connection;
use Illuminate\Support\Facades\DB;

trait ConsumableBuilderTrait
{
    /**
     * A collection of consumable records.
     *
     * @var array
     */
    protected $consumableRecords = [];

    /**
     * Indicates if consumable mode is overridden.
     *
     * @var bool
     */
    protected $consumableOverride = false;

    /**
     * Holds the consumable override groups.
     *
     * @var null
     */
    public $consumableOverrideGroups = null;

    /**
     * Holds the consumable override restrictions.
     *
     * @var null
     */
    public $consumableOverrideRestrictions = null;

    protected $internalMode = 0;

    private $workEntryReservedColumnNames = ['activity_id', 'department_id', 'district_id', 'employee_id',
                                             'project_id', 'property_id'];

    /**
     * Enables the consumable override.
     *
     * @return mixed
     */
    public function enableConsumableOverride()
    {
        $this->consumableOverride = true;
    }

    public function setInternalMode($mode)
    {
        $this->internalMode = $mode;
    }

    public function getInternalMode()
    {
        return $this->internalMode;
    }

    /**
     * Disables the consumable override.
     *
     * @return mixed
     */
    public function disableConsumableOverride()
    {
        $this->consumableOverride = false;
    }

    /**
     * Sets the consumable record grouping.
     *
     * @param $columns
     * @return mixed
     */
    public function groupConsumableRecord($columns)
    {
        $this->consumableOverrideGroups = $columns;
    }

    /**
     * Sets a consumable override restriction mode.
     *
     * @param       $type
     * @param array $restriction
     * @return mixed
     */
    public function setConsumableRestrictions($type, array $restriction)
    {
        $this->consumableOverrideRestrictions[$type] = $restriction;
    }

    /**
     * Fills the consumable records array based on work entry IDs.
     *
     * @param Connection $connection
     * @param            $workEntryIDs
     */
    private function fillConsumableRecords(Connection &$connection, $workEntryIDs)
    {
        $this->grammar->straightJoin()->forceIndexes(['work_consumables_work_entry_id_index']);
        if (count($workEntryIDs) > 0) {
            /**
             * There are a few steps to do here. Check if we are in override mode. If not, it is a general work record query.
             * If so, we are in a report. Then we have to check if there are any groupings. If there are groupings, apply an
             * aggregate query. If not, apply the normal query.
             */

            $consumableQuery = $connection->table('work_consumables')->whereIn('work_entry_id', $workEntryIDs)
                                          ->join('work_entries', 'work_consumables.work_entry_id', '=',
                                                 'work_entries.id');

            if ($this->consumableOverride == false) {
                $this->consumableRecords = $consumableQuery->select(DB::raw('work_consumables.*'))->orderBy('work_consumables.id', 'DESC')->get();
            } else {
                if ($this->consumableOverrideGroups != null) {
                    // Here we will apply the groupings.
                    foreach ($this->consumableOverrideGroups as $group) {
                        if (in_array($group, $this->workEntryReservedColumnNames)) {
                            $consumableQuery = $consumableQuery->groupBy('work_entries.' . $group);
                        } else {
                            $consumableQuery = $consumableQuery->groupBy('work_consumables.' . $group);
                        }
                    }
                }

                if ($this->consumableOverrideRestrictions != null) {
                    foreach ($this->consumableOverrideRestrictions as $restriction => $restrictionDetails) {
                        $consumableQuery =
                            $consumableQuery->where(function ($query) use ($restriction, $restrictionDetails) {
                                if ($restriction != null && count($restrictionDetails) == 2 &&
                                    $restrictionDetails[1] !== null
                                ) {
                                    $query->where('consumable_type', '=', $restriction);

                                    if ($restrictionDetails[0] == true) {
                                        $query->whereIn('consumable_id', $restrictionDetails[1]);
                                    } else {
                                        $query->whereNotIn('consumable_id', $restrictionDetails[1]);
                                    }
                                }
                            });
                    }
                }

                if ($this->consumableOverrideGroups == null) {
                    $consumableQuery = $consumableQuery->select(DB::raw('work_consumables.*'));
                } else {
                    $consumableQuery =
                        $consumableQuery->select('work_consumables.id', 'work_consumables.consumable_type',
                                                 'work_consumables.consumable_id', 'work_consumables.associated_type',
                                                 'work_consumables.associated_id',
                                                 'work_consumables.bound_consumable_record',
                                                 'work_consumables.work_entry_id',
                                                 'work_consumables.measurement_unit_id',
                                                 DB::raw('SUM(work_consumables.total_cost) as total_cost'),
                                                 DB::raw('SUM(work_consumables.total_quantity) as total_quantity'),
                                                 DB::raw('AVG(work_consumables.historic_cost) as historic_cost'),
                                                 DB::raw('SUM(work_consumables.total_tax) as total_tax'));
                }

                $this->consumableRecords = $consumableQuery->orderBy('work_consumables.id', 'DESC')->get();
            }
        }
        $this->grammar->straightJoin(false)->forceIndexes(false);

        // Memory clean up.
        unset($connection);
    }
}
