<?php namespace ParadoxOne\NDCounties\Database\Repositories\WorkEntries\Builders;

use Illuminate\Support\Facades\App;

trait MeasurementUnitBuilderTrait
{
    /**
     * The measurement units.
     *
     * @var array
     */
    protected $measurementUnits = [];

    private function fillMeasurementUnits(&$measurementUnitIDs)
    {
        // Users not changing the measurement unit is a common thing. We will do this check
        // here to avoid hitting the database for the default measurement unit.
        if (count($measurementUnitIDs) == 1 and $measurementUnitIDs[0] == 9) {
            $noUnit                   = new \stdClass;
            $noUnit->id               = 9;
            $noUnit->code             = 'none';
            $noUnit->name             = 'NA';
            $noUnit->description      = 'NA';
            $this->measurementUnits[] = $noUnit;
        } else {
            if (count($measurementUnitIDs) > 0) {
                $unitRepository         = App::make('ParadoxOne\NDCounties\Contracts\UnitRepositoryInterface');
                $this->measurementUnits = $unitRepository->whereIn($measurementUnitIDs);

                // Memory clean up.
                $unitRepository = null;
                unset($unitRepository);
            }
        }

        // Memory clean up.
        unset($measurementUnitIDs);
    }
}
