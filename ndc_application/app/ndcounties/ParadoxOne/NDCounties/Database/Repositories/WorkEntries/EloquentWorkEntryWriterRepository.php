<?php namespace ParadoxOne\NDCounties\Database\Repositories\WorkEntries;

use Carbon;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\MessageBag;
use Service\WorkEntries\WorkEntry;

class EloquentWorkEntryWriterRepository extends WorkEntryWriterRepository
{
    /**
     * The invoice writer repo.
     *
     * @var ParadoxOne\NDCounties\Contracts\Finance\Invoices\InvoiceWriterRepositoryInterface
     */
    protected $invoiceWriterRepository = null;
    private $validationErrors        = null;

    /**
     * {@inheritdoc}
     */
    public function errors()
    {
        return $this->validationErrors;
    }

    /**
     * {@inheritdoc}
     */
    public function commit()
    {
        $this->invoiceWriterRepository =
            App::make('ParadoxOne\NDCounties\Contracts\Finance\Invoices\InvoiceWriterRepositoryInterface');

        $workEntry = new WorkEntry;

        /** Identifying information. */
        $workEntry->work_date     = $this->workLogDate;
        $workEntry->employee_id   = $this->employeeID;
        $workEntry->activity_id   = $this->activityID;
        $workEntry->project_id    = $this->projectID;
        $workEntry->department_id = $this->departmentID;
        $workEntry->district_id   = $this->districtID;
        $workEntry->property_id   = $this->propertyID;

        $workEntry->property_organization_id      = $this->organizationPropertyID;
        $workEntry->property_organization_context = $this->organizationPropertyContext;

        $workEntry->activity_description = $this->activityDescription;

        /** Employee hours data */
        $workEntry->employee_hours_regular_worked  = $this->regularHoursWorked;
        $workEntry->employee_hours_overtime_worked = $this->overtimeHoursWorked;

        /** Historic data */
        $workEntry->employee_historic_wage_regular    = $this->historicRegularWage;
        $workEntry->employee_historic_wage_overtime   = $this->historicOvertimeWage;
        $workEntry->employee_historic_fringe_regular  = $this->historicRegularFringeBenefits;
        $workEntry->employee_historic_fringe_overtime = $this->historicOvertimeFringeBenefits;

        /** Miscellaneous billing data */
        $workEntry->misc_billing_quantity = $this->miscBillingQuantity;
        $workEntry->misc_billing_rate     = $this->miscBillingRate;
        $workEntry->misc_unit_id          = $this->miscBillingMeasurementUnit;
        $workEntry->misc_description      = $this->miscBillingDescription;
        $workEntry->property_context      = $this->propertyContext;
        $workEntry->credit_inventory      = $this->isCreditInventoryMode;

        /** Miscellaneous tax information */
        $workEntry->employee_taxable = $this->getEmployeeIsTaxed();

        /** Setting Data */
        $workEntry->exclude_from_report = $this->getExcludedFromReports();

        /** Aggregate report information */
        $workEntry->total_materials_quantity       = $this->calculateTotalMaterialsQuantity();
        $workEntry->total_equipment_units_quantity = $this->calculateTotalEquipmentUnitsQuantity();
        $workEntry->total_fuels_quantity           = $this->caclulateTotalFuelsQuantity();
        $workEntry->total_materials_cost           = $this->calculateTotalMaterialCost();
        $workEntry->total_equipment_units_cost     = $this->calculateTotalEquipmentUnitsCost();
        $workEntry->total_fuels_cost               = $this->calculateTotalFuelsCost();
        $workEntry->total_equipment_and_fuels_cost = $this->calculateTotalEquipmentAndFuelsCost();

        $workEntry->total_invoice = $this->invoiceOverrideTotal;

        /**
         *  if (is_string($this->invoiceOverrideTotal) && strlen($this->invoiceOverrideTotal) > 0 &&
        floatval($this->invoiceOverrideTotal) >= 0 && is_bool($this->invoiceOverrideTotal) == false
        ) {
        $workEntry->total_invoice = $this->invoiceOverrideTotal;
        } else {
        $workEntry->total_invoice = 0;
        }
         */


        $workEntry->total_employee_regular_cost  = $this->calculateRegularHoursCost();
        $workEntry->total_employee_overtime_cost = $this->calculateOvertimeHoursCost();
        $workEntry->total_employee_combined_cost = $this->caclulateCombinedHoursCost();

        $workEntry->total_misc_billing_cost = $this->calculateTotalMiscBillingTotalCost();

        $consumableValidationPassed = true;
        $workEntryValidationPassed  = false;

        $this->validationErrors = new MessageBag;

        // Here we will do the validation work. Yes.
        foreach ($this->consumableRecords as $consumable) {
            $consumableValidated = $consumable->isValid();

            if (!$consumableValidated) {
                $this->mergeValidationErrors($consumable->errors()->all());

                if ($consumableValidationPassed) {
                    $consumableValidationPassed = false;
                }
            }

            if ($this->determineIfInvoiceSet()) {

                // START: Code to calculate the totals for taxes and such.

                $associatedType = $consumable->getAssociatedType();

                // Equipment
                if ($associatedType == 2) {
                    if ($consumable->isTaxed()) {
                        $this->invoiceWriterRepository->incrementInvoiceEquipmentUnitsTaxable($consumable->getTotalCost());
                    }

                    $this->invoiceWriterRepository->incrementInvoiceEquipmentUnits($consumable->getTotalCost());
                }

                // Fuel
                if ($associatedType == 3) {
                    if ($consumable->isTaxed()) {
                        $this->invoiceWriterRepository->incrementInvoiceFuelTaxable($consumable->getTotalCost());
                    }

                    $this->invoiceWriterRepository->incrementInvoiceFuel($consumable->getTotalCost());
                }

                // Material
                if ($associatedType == 4) {
                    if ($consumable->isTaxed()) {
                        $this->invoiceWriterRepository->incrementInvoiceTotalMaterialsTaxable($consumable->getTotalCost());
                    }

                    $this->invoiceWriterRepository->incrementInvoiceTotalMaterials($consumable->getTotalCost());
                }

                // END: Code to calculate the totals for taxes and such.
            }
        }

        $presenceVerifier = App::make('validation.presence');
        $presenceVerifier->setConnection(\Tenant::getCurrentConnection());
        $workEntry->setPresenceVerifier($presenceVerifier);

        $workEntryValidationPassed = $workEntry->isValid();

        if (!$workEntryValidationPassed) {
            $this->mergeValidationErrors($workEntry->errors()->all());
        }

        if ($workEntryValidationPassed && $consumableValidationPassed) {
            // We can only start to run the inserts here IF AND ONLY IF
            // the $validationPassed variable is set to TRUE.

            DB::connection(\Tenant::getCurrentConnection())->beginTransaction();

            $success = $workEntry->save();

            $consumableSuccesses = array();

            $invoiceSuccess = $this->handleInvoiceCreation($workEntry->id);

            usort($this->consumableRecords, function ($a, $b) {
                return $a->getConsumableType() > $b->getConsumableType();
            });

            foreach ($this->consumableRecords as $consumable) {
                $consumable->setWorkEntryID($workEntry->id);

                $this->consumableWriterRepository->setConsumable($consumable);
                try {
                    $consumableSuccesses[$this->consumableWriterRepository->commit()] = 0;
                } catch (\Exception $e) {
                    // lk($e);
                }
            }


            // START: This is where we will attempt to save the
            //        property adapter handlers.

            if ($this->propertyID != 0) {
                $processor =
                    App::make('\ParadoxOne\NDCounties\Database\Repositories\WorkEntries\Processors\WriterHandlerProcessor');
                $processor->setEloquentWriter($this);
                $processor->process($workEntry->id, Input::all());
                $processor->disconnect();

                if (in_array(false, $processor->getAdapterRuns())) {
                    DB::connection(\Tenant::getCurrentConnection())->rollback();

                    return false;
                }
            }

            if ($this->propertyTotal > 0) {
                $workEntry->total_property_cost = $this->propertyTotal;
                $workEntry->save();
            }

            // END: This is the end of the property adapter handlers.
            if ($success) {
                if (!array_key_exists('0', $consumableSuccesses)) {
                    DB::connection(\Tenant::getCurrentConnection())->commit();
                    Event::fire('workrecord.created', [$workEntry->id]);

                    return true;
                } else {
                    DB::connection(\Tenant::getCurrentConnection())->rollback();

                    return false;
                }
            } else {
                DB::connection(\Tenant::getCurrentConnection())->rollback();

                return false;
            }
        } else {
            return false;
        }
    }

    public function mergeValidationErrors($errors)
    {
        if ($errors == null) {
            return;
        }

        $this->validationErrors = $this->validationErrors->merge($errors);
    }

    /**
     * Determines if an invoice has been set and needs to be created.
     *
     * @return bool Whether or not a user is creating an invoice.
     */
    protected function determineIfInvoiceSet()
    {
        $invoiceNumber = Input::get('billing_invoice_number', null);

        if ($invoiceNumber !== null) {
            if (strlen(trim($invoiceNumber)) !== 0) {
                return true;
            }
        }

        return false;
    }

    protected $invoiceIDFromWriter = null;

    /**
     * Handles the creation of the invoice.
     *
     * @param $workEntryID
     * @return bool
     * @throws HydratorException
     */
    protected function handleInvoiceCreation($workEntryID)
    {
        // We will only attempt to create an invoice if there are certain inputs set.
        if ($this->determineIfInvoiceSet()) {
            $this->invoiceWriterRepository->addWorkRecord($workEntryID);

            $this->invoiceWriterRepository->setInvoiceTotalOverride($this->invoiceOverrideTotal);

            // Set some properties on the invoice writer.
            $this->invoiceWriterRepository->setDueDate((new Carbon(Input::get('billing_invoice_due_date')))->toDateTimeString());
            $this->invoiceWriterRepository->setAccountNumber(Input::get('billing_account_number'));
            $this->invoiceWriterRepository->setPaidToAccount(Input::get('billing_paid_to_account_number'));
            $this->invoiceWriterRepository->setInvoiceNumber(Input::get('billing_invoice_number'));
            $this->invoiceWriterRepository->setDiscount(Input::get('billing_invoice_discount'));
            $this->invoiceWriterRepository->setClientNotes(Input::get('invoice_notes_to_client'));
            $this->invoiceWriterRepository->setInvoiceTerms(Input::get('invoice_terms'));
            $this->invoiceWriterRepository->setMiscBillingQuantity(Input::get('misc_billing_quantity', 0));
            $this->invoiceWriterRepository->setMiscBillingRate(Input::get('misc_billing_rate', 0));

            $this->invoiceWriterRepository->setInvoiceTotalEmployees($this->caclulateCombinedHoursCost());

            // START CODE FOR INVOICE TAX RATES
            // This code will create a list of tax rates that are being used by the system.

            // This holds the tax percentages we will need later.
            $taxPercentageEmployee  = 0;
            $taxPercentageEquipment = 0;
            $taxPercentageMaterials = 0;
            $taxPercentageFuels     = 0;

            $invoiceTaxRates          = Input::get('invoiceTaxRates');
            $invoiceHistoricRates     = Input::get('invoiceTaxHistoricRates');
            $invoiceAffectsEmployees  = Input::get('affectsEmployees');
            $invoiceAffectsEquipments = Input::get('affectsEquipments');
            $invoiceAffectsMaterials  = Input::get('affectsMaterials');
            $invoiceAffectsFuels      = Input::get('affectsFuels');
            $originalTaxRecordIDs     = [];

            if (Input::has('tax_record_id')) {
                $originalTaxRecordIDs = Input::get('tax_record_id');
            } else {
                $originalTaxRecordIDs = array_pad($originalTaxRecordIDs, count($invoiceTaxRates), '');
            }

            $checkCount = count($invoiceTaxRates);

            $this->assertTaxRateCount($checkCount, $invoiceTaxRates);
            $this->assertTaxRateCount($checkCount, $invoiceHistoricRates);
            $this->assertTaxRateCount($checkCount, $invoiceAffectsEmployees);
            $this->assertTaxRateCount($checkCount, $invoiceAffectsEquipments);
            $this->assertTaxRateCount($checkCount, $invoiceAffectsMaterials);
            $this->assertTaxRateCount($checkCount, $invoiceAffectsFuels);
            $this->assertTaxRateCount($checkCount, $originalTaxRecordIDs);

            for ($i = 0; $i < $checkCount; $i++) {
                $taxID                = $invoiceTaxRates[$i];
                $taxHistoricRate      = $invoiceHistoricRates[$i];
                $taxAffectsEmployees  = $invoiceAffectsEmployees[$i];
                $taxAffectsEquipments = $invoiceAffectsEquipments[$i];
                $taxAffectsMaterials  = $invoiceAffectsMaterials[$i];
                $taxAffectsFuels      = $invoiceAffectsFuels[$i];
                $originalTaxRecordID  = $originalTaxRecordIDs[$i];

                if ((bool)$taxAffectsEmployees) {
                    $taxPercentageEmployee += $taxHistoricRate;
                }

                if ((bool)$taxAffectsEquipments) {
                    $taxPercentageEquipment += $taxHistoricRate;
                }

                if ((bool)$taxAffectsMaterials) {
                    $taxPercentageMaterials += $taxHistoricRate;
                }

                if ((bool)$taxAffectsFuels) {
                    $taxPercentageFuels += $taxHistoricRate;
                }

                $this->invoiceWriterRepository->addTaxRate($taxID, $taxHistoricRate, $taxAffectsEmployees,
                    $taxAffectsEquipments, $taxAffectsMaterials,
                    $taxAffectsFuels, $originalTaxRecordID);
            }

            // Tell the consumable writer about the tax rates.
            $this->consumableWriterRepository->setHistoricTaxRatePercentagesFromInvoice($taxPercentageEmployee,
                $taxPercentageEquipment,
                $taxPercentageMaterials,
                $taxPercentageFuels);

            // END CODE FOR INVOICE TAX RATES


            if (Input::get('billing_invoice_number_created', 1) == 0) {
                // The invoice is actually supposed to be appended, so let's do that instead.
                $success                   = $this->invoiceWriterRepository->append();
                $this->invoiceIDFromWriter = $this->invoiceWriterRepository->publicAPIInvoiceID;
                Event::fire('invoice.appended', [$this->invoiceIDFromWriter]);

                return $success;
            } else {
                $success                   = $this->invoiceWriterRepository->commit();
                $this->invoiceIDFromWriter = $this->invoiceWriterRepository->publicAPIInvoiceID;
                Event::fire('invoice.created', [$this->invoiceIDFromWriter]);

                return $success;
            }
        }

        return true;
    }

    /**
     * Assert the tax counts are all the same.
     *
     * @param  int   $desiredCount
     * @param  array $checkArray
     * @throws HydratorException If the counts do not match.
     */
    protected function assertTaxRateCount($desiredCount, $checkArray)
    {
        if (count($checkArray) !== $desiredCount) {
            throw new HydratorException("There were inconsistencies within the tax rate records. Hydration cannot continue.");
        }
    }
}
