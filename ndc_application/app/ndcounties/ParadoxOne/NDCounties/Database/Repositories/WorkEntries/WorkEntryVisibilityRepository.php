<?php namespace ParadoxOne\NDCounties\Database\Repositories\WorkEntries;

use ParadoxOne\NDCounties\Contracts\WorkEntries\WorkEntryVisibilityRepositoryInterface;
use ParadoxOne\NDCounties\Database\Repositories\BaseTenantRepository;

class WorkEntryVisibilityRepository extends BaseTenantRepository implements WorkEntryVisibilityRepositoryInterface
{
    /**
     * Creates a new record.
     *
     * @param  array $recordDetails
     * @return mixed
     * @throws \Exception
     */
    public function create(array $recordDetails = array())
    {
        throw new \Exception('Not Implemented');
    }

    /**
     * Removes an existing record.
     *
     * @param  array $removeDetails
     * @return mixed
     * @throws \Exception
     */
    public function remove(array $removeDetails = array())
    {
        throw new \Exception('Not Implemented');
    }

    /**
     * Updates an existing record.
     *
     * @param  int   $recordID
     * @param  array $newRecordDetails
     * @return mixed
     * @throws \Exception
     */
    public function update($recordID, array $newRecordDetails)
    {
        throw new \Exception('Not Implemented');
    }

    /**
     * Hides the given records from reports.
     *
     * @param array $recordIDs
     * @return mixed
     */
    public function hideRecordsFromReports(array $recordIDs)
    {
        if (count($recordIDs) > 0) {
            $this->startTransaction();

            try {
                $this->table('work_entries')->whereIn('id', $recordIDs)
                    ->update([
                        'exclude_from_report' => true
                             ]);
                $this->commitTransaction();
                return true;
            } catch (\Exception $e) {
            }

            $this->rollbackTransaction();
            return false;
        }

        return true;
    }

    /**
     * Shows the given records to reports.
     *
     * @param array $recordIDs
     * @return mixed
     */
    public function showRecordsToReports(array $recordIDs)
    {
        if (count($recordIDs) > 0) {
            $this->startTransaction();

            try {
                $this->table('work_entries')->whereIn('id', $recordIDs)
                     ->update([
                                  'exclude_from_report' => false
                              ]);
                $this->commitTransaction();
                return true;
            } catch (\Exception $e) {
            }

            $this->rollbackTransaction();
            return false;
        }

        return true;
    }
}
