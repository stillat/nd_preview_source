<?php namespace ParadoxOne\NDCounties\Database\Repositories\WorkEntries\InventoryManagers;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use ParadoxOne\NDCounties\Contracts\InventoryManagement\AdjustmentRepositoryInterface;
use ParadoxOne\NDCounties\Settings\InventorySettingsManager;
use ParadoxOne\NDCounties\Traits\MathExecutableTrait;

abstract class AbstractInventoryProcessor
{
    const CONTEXT_SYSTEM_ADJUSTED = 0;
    const CONTEXT_MANUALLY_ADJUSTED = 1;
    const CONTEXT_WORK_ADJUSTED = 2;

    use MathExecutableTrait;

    /**
     * The ParadoxOne\NDCounties\Contracts\InventoryManagement\AdjustmentRepositoryInterface implementation.
     *
     * @var null|AdjustmentRepositoryInterface
     */
    protected $inventoryAdjustmentRepository = null;

    /**
     * The ParadoxOne\NDCounties\Settings\InventorySettingsManager implementation.
     *
     * @var null|InventorySettingsManager
     */
    protected $inventoryManagementSettings = null;

    /**
     * Holds the processed consumables and their quantities.
     *
     * @var array
     */
    protected $consumableManagementArray = [];

    /**
     * The work entry ID, if any.
     *
     * @var null
     */
    protected $workEntryID = null;

    /**
     * Indicates if the processor is supposed to credit or debit the adjustment.
     *
     * @var bool
     */
    protected $isCreditMode = false;

    protected $allInventorySettings = null;


    public function __construct(AdjustmentRepositoryInterface $inventoryAdjustmentRepository, InventorySettingsManager $inventoryManagementSettings)
    {
        $this->inventoryAdjustmentRepository = $inventoryAdjustmentRepository;
        $this->inventoryManagementSettings   = $inventoryManagementSettings;
        $this->allInventorySettings = $this->inventoryManagementSettings->getInventorySettings();

        $this->setMathEngine();
        // This part will determined if we are going to credit things instead of debit them.
        $this->isCreditMode = Input::has('credit_inventory');
    }

    /**
     * Gets a query builder instance for the given table.
     *
     * @param $tableName
     * @return mixed
     */
    protected function getTable($tableName)
    {
        return DB::connection(\Tenant::getCurrentConnection())->table($tableName);
    }

    protected function startTransaction()
    {
        return DB::connection(\Tenant::getCurrentConnection())->beginTransaction();
    }

    protected function commitTransaction()
    {
        return DB::connection(\Tenant::getCurrentConnection())->commit();
    }

    protected function rollbackTransaction()
    {
        return DB::connection(\Tenant::getCurrentConnection())->rollback();
    }

    /**
     * Sets the work entry ID.
     *
     * @param $workEntryID
     */
    public function setWorkEntryID($workEntryID)
    {
        $this->workEntryID = $workEntryID;
    }

    /**
     * Gets the work entry ID.
     *
     * @return int
     */
    public function getWorkEntryID()
    {
        return $this->workEntryID;
    }

    /**
     * Adds a consumable item to process.
     *
     * @param $consumableID
     * @param $quantity
     */
    public function addConsumableItem($consumableID, $quantity)
    {
        $consumableQuantity = 0;

        if (isset($this->consumableManagementArray[$consumableID])) {
            $consumableQuantity = $this->consumableManagementArray[$consumableID];
        }

        $this->consumableManagementArray[$consumableID] = $this->getMathEngine()->add($consumableQuantity, $this->getMathEngine()->mul('-1', $quantity));
    }

    /**
     * Gets the processed consumable items.
     *
     * This take into account the adjustment mode.
     *
     * @return array
     */
    public function getConsumableItems()
    {
        if ($this->isCreditMode) {
            $creditModeItems = [];

            foreach ($this->consumableManagementArray as $key => $value) {
                $creditModeItems[$key] = $this->getMathEngine()->mul('-1', $value);
            }

            return $creditModeItems;
        }

        return $this->consumableManagementArray;
    }

    /**
     * Sets the internal adjustment mode.
     *
     * @param $mode
     */
    public function setAdjustmentMode($mode)
    {
        $this->isCreditMode = $mode;
    }

    /**
     * Gets the internal adjustment mode.
     *
     * @return bool
     */
    public function getAdjustmentMode()
    {
        return $this->isCreditMode;
    }

    /**
     * Processes the inventory adjustment.
     *
     * @return mixed
     */
    abstract public function process();
}
