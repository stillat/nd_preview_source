<?php namespace ParadoxOne\NDCounties\Database\Repositories\WorkEntries\Builders;

trait PaginationBuilderTrait
{
    /**
     * Instructs the builder to paginate the results.
     *
     * @param int $currentPage
     * @param int $perPage
     * @return mixed
     */
    public function paginate($currentPage = 1, $perPage = 10)
    {
        $this->reportQuery       = $this->reportQuery->skip($perPage * ($currentPage - 1))->take($perPage);
    }
}
