<?php namespace ParadoxOne\NDCounties\Database\Repositories\WorkEntries\Builders;

use Illuminate\Database\Connection;
use Illuminate\Support\Facades\DB;

trait EquipmentUnitBuilderTrait
{
    /**
     * Holds the actual equipment unit records.
     *
     * @var array
     */
    protected $equipmentUnitRecords = [];

    /**
     * Holds the odometer reading records.
     *
     * @var array
     */
    protected $odometerReadingRecords = [];

    /**
     * Fills the equipment unit and odometer reading records.
     *
     * @param Connection $connection
     * @param            $equipmentRecords
     * @return array
     */
    private function fillEquipmentUnitRecords(Connection &$connection, &$equipmentRecords, &$workLogIDs)
    {
        if (count($equipmentRecords) > 0) {
            $this->equipmentUnitRecords = $connection->table('properties')->select(
                DB::raw('properties.*, property_id, year, make, model, serial_number, rental_rate, unit_id, purchase_cost, salvage_value, lifetime')
            )->where('property_type', '=', '2')
                                                     ->join('equipment_units', 'properties.id', '=',
                                                            'equipment_units.property_id')
                                                     ->whereIn('properties.id', $equipmentRecords)->get();
        }

        if (count($workLogIDs) > 0) {
            // This cannot be inside the equipment records IF check, because work record data can have an odometer reading too.
            $this->odometerReadingRecords = $connection->table('equipment_odometer_readings')
                                                       ->whereIn('associated_record', $workLogIDs)
                                                       ->whereIn('associated_record_type', [1, 2])->get();
        }

        $discoveredMeasurementUnits = [];

        foreach ($this->equipmentUnitRecords as $equipRecord) {
            $discoveredMeasurementUnits[] = $equipRecord->unit_id;
        }

        // Memory clean up.
        unset($connection);
        unset($equipmentRecords);
        unset($workLogIDs);


        return $discoveredMeasurementUnits;
    }

    /**
     * Returns a multi-dimensional array of odometer readings.
     *
     * @return mixed
     */
    public function getOdometerReadings()
    {
        $propertyOdometerReadings = array_pluck_where($this->odometerReadingRecords, 'associated_record_type', 2);
        $usageOdometerReadings    = array_pluck_where($this->odometerReadingRecords, 'associated_record_type', 1);

        return [2 => $propertyOdometerReadings, 1 => $usageOdometerReadings];
    }
}
