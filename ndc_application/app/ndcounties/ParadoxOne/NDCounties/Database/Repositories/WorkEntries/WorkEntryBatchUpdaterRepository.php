<?php

namespace ParadoxOne\NDCounties\Database\Repositories\WorkEntries;

use Illuminate\Support\Facades\DB;
use Stillat\Database\Support\Facades\Tenant;
use ParadoxOne\NDCounties\Contracts\WorkEntries\WorkEntryBatchUpdateRepositoryInterface;
use Carbon\Carbon;
use Illuminate\Support\Facades\Event;
use ParadoxOne\NDCounties\Database\Repositories\BaseTenantRepository;

class WorkEntryBatchUpdaterRepository extends BaseTenantRepository implements WorkEntryBatchUpdateRepositoryInterface
{

    /**
     * Creates a new record.
     *
     * @param  array $recordDetails
     * @return mixed
     * @throws \Exception
     */
    public function create(array $recordDetails = array())
    {
        throw new \Exception('Not Implemented');
    }

    /**
     * Removes an existing record.
     *
     * @param  array $removeDetails
     * @return mixed
     * @throws \Exception
     */
    public function remove(array $removeDetails = array())
    {
        throw new \Exception('Not Implemented');
    }

    /**
     * Updates an existing record.
     *
     * @param  int $recordID
     * @param  array $newRecordDetails
     * @return mixed
     * @throws \Exception
     */
    public function update($recordID, array $newRecordDetails)
    {
        throw new \Exception('Not Implemented');
    }

    private function getCleanArray($array)
    {
        $newArray = [];

        foreach ($array as $item) {
            try {
                if ($item !== null && is_int((int)$item)) {
                    $newArray[] = $item;
                }
            } catch (\Exception $e) {
            }
        }

        return $newArray;
    }

    protected function workRecords()
    {
        return $this->table('work_entries');
    }

    protected function updateWorkRecordValue($column, $startRecord, $finishRecord)
    {
        $this->workRecords()->where($column, '=', $finishRecord)->update([$column => $startRecord]);
    }

    public function batchUpdateDates($recordIDs, $newDate)
    {
        if (count($recordIDs) > 0) {
            $this->startTransaction();
            try {

                $newDate = (new Carbon($newDate))->toDateTimeString();
                $lastUpdatedDate = Carbon::now()->toDateTimeString();

                $this->table('work_entries')->whereIn('id', $recordIDs)
                    ->update([
                        'work_date' => $newDate,
                        'updated_at' => $lastUpdatedDate
                    ]);

                $this->commitTransaction();
                Event::fire('batch.dates', [$newDate, $recordIDs]);
                return true;
            } catch (\Exception $e) {

            }
            $this->rollbackTransaction();
            return false;
        }

        return true;
    }

    private function performBatch($table, $column, $target, $old)
    {
        if (count($old) > 0) {
            $this->startTransaction();
            $old = $this->getCleanArray($old);
            $old = array_strip_values($old, $target);

            try {

                foreach ($old as $record) {
                    $this->updateWorkRecordValue($column, $target, $record);
                }

                // Make sure the "0" records don't get deleted!
                $old = array_strip_values($old, 0);

                if (count($old) > 0) {
                    $this->table($table)->whereIn('id', $old)->delete();
                }

                $this->commitTransaction();
                return true;
            } catch (\Exception $e) {

            }

            $this->rollbackTransaction();
            return false;
        }

        return true;
    }

    private function performBatchOnConsumable($target, $old)
    {
        if (count($old) > 0) {
            $this->startTransaction();
            $old = $this->getCleanArray($old);
            $old = array_strip_values($old, $target);

            try {
                foreach ($old as $record) {
                    // Remove from consumables.
                    $this->table('work_consumables')->whereIn('consumable_type', [3, 4])->where('consumable_id', '=',
                        $record)->update([
                        'consumable_id' => $target
                    ]);
                }

                $old = array_strip_values($old, [0, 1]);

                if (count($old) > 0) {
                    $this->table('consumables')->whereIn('id', $old)->delete();
                }

                $this->commitTransaction();
                return true;
            } catch (\Exception $e) {
            }

            $this->rollbackTransaction();
            return false;
        }

        return true;
    }

    public function batchMergeAndRemoveGenericProperties($target, $old)
    {
        /**
         * Need to update the following when merging and removing generics:
         * property_property - update Ids in both parent_property_id and child_property_id
         * work_entries - update property_organization_id to new one
         * property_id - update to new one where property_context = 0
         * properties - remove by id
         */

        $target = substr($target, 5);

        if (count($old) > 0) {
            $this->startTransaction();
            $old = $this->getCleanArray($old);
            $old = array_strip_values($old, $target);

            try {
                $this->table('property_property')->whereIn('parent_property_id', $old)->update([
                    'parent_property_id' => $target
                ]);
                $this->table('property_property')->whereIn('child_property_id', $old)->update([
                    'parent_property_id' => $target
                ]);
                $this->table('work_entries')->whereIn('property_organization_id', $old)->update([
                    'property_organization_id' => $target
                ]);
                $this->table('work_entries')->where('property_context', '=', 0)->whereIn('property_id', $old)->update([
                    'property_id' => $target
                ]);
                $old = array_strip_values($old, [0, 1]);

                if (count($old) > 0) {
                    $this->table('properties')->whereIn('id', $old)->delete();
                }

                $this->commitTransaction();
                return true;
            } catch (\Exception $e) {
            }

            $this->rollbackTransaction();
            return false;

        }

        return true;
    }

    public function batchMergeAndRemoveEquipmentUnits($target, $old)
    {
        /**
         * The following tables need to be updated when merging and removing equipment units:
         * equipment_odometer_readings - update equipment_id with new one
         * work_entries - update property_id with new one where property_context = 1
         * work_consumables - update consumable_id where consumable_type = 2
         * work_consumables - updated associated_id with new one where associated_type = 3
         * equipment_units - remove old by property_id
         * properties - remove by id
         */
        $target = substr($target, 5);

        if (count($old) > 0) {
            $this->startTransaction();
            $old = $this->getCleanArray($old);
            $old = array_strip_values($old, $target);

            try {
                $this->table('work_consumables')->where('consumable_type', '=', 2)->whereIn('consumable_id',
                    $old)->update([
                    'consumable_id' => $target
                ]);
                $this->table('work_consumables')->where('consumable_type', '=', 3)->where('associated_type', '=',
                    3)->whereIn('associated_id', $old)->update([
                    'associated_id' => $target
                ]);
                $this->table('work_entries')->where('property_context', '=', 1)->whereIn('property_id', $old)->update([
                    'property_id' => $target
                ]);
                $this->table('equipment_odometer_readings')->whereIn('equipment_id', $old)->update([
                    'equipment_id' => $target
                ]);

                $old = array_strip_values($old, [0, 1]);

                if (count($old) > 0) {
                    $this->table('equipment_units')->whereIn('property_id', $old)->delete();
                    $this->table('properties')->whereIn('id', $old)->delete();
                }

                $this->commitTransaction();
                return true;
            } catch (\Exception $e) {
            }

            $this->rollbackTransaction();
            return false;

        }

        return true;
    }

    public function batchConvertRoadsToGenericProperties($roadsToConvert)
    {
        /**
         * The following tables need to be updated to convert roads to generic properties:
         * road_work_record_logs - need to remove all entries where the work entry had a road property in the $roadsToConvert
         * property_road_properties - remove all entries where road_id in $roadsToConvert
         * properties - set property_type to 0 where id in $roadsToConvert
         *
         */
        $roadsToConvert = $this->getCleanArray($roadsToConvert);

        if (count($roadsToConvert) > 0) {
            $this->startTransaction();

            try {
                DB::connection(Tenant::getCurrentConnection())->unprepared('DELETE FROM road_work_record_logs
                where work_record_id in (select id from work_entries where property_context = 0 and property_id in (SELECT id FROM properties WHERE property_type = 0))');
                $this->table('road_work_record_logs')->whereRaw('work_record_id IN (
                    SELECT id FROM work_entries WHERE property_context = 0 AND property_id IN (?)
                );')->setBindings($roadsToConvert)->delete();
                $this->table('property_road_properties')->whereIn('road_id', $roadsToConvert)->delete();
                $this->table('properties')->where('property_type', '=', 1)->whereIn('id', $roadsToConvert)->update([
                    'property_type' => 0
                ]);
                $this->commitTransaction();
                return true;
            } catch (\Exception $e) {
                dd($e->getMessage());
            }

            $this->rollbackTransaction();
            return false;
        }

        return true;
    }

    public function batchMergeAndRemoveRoads($target, $old)
    {
        $target = substr($target, 5);

        if (count($old) > 0) {
            $this->startTransaction();
            $old = $this->getCleanArray($old);
            $old = array_strip_values($old, $target);

            /**
             * Need to update the following when merging and removing roads:
             * property_road_properties - remove by road_id
             * property_property - update Ids in both parent_property_id and child_property_id
             * work_entries - update property_organization_id to new one
             * property_id - update to new one where property_context = 0
             */

            try {
                $this->table('property_property')->whereIn('parent_property_id', $old)->update([
                    'parent_property_id' => $target
                ]);
                $this->table('property_property')->whereIn('child_property_id', $old)->update([
                    'parent_property_id' => $target
                ]);
                $this->table('work_entries')->whereIn('property_organization_id', $old)->update([
                    'property_organization_id' => $target
                ]);
                $this->table('work_entries')->where('property_context', '=', 0)->whereIn('property_id', $old)->update([
                    'property_id' => $target
                ]);
                $old = array_strip_values($old, [0, 1]);

                if (count($old) > 0) {
                    $this->table('property_road_properties')->whereIn('road_id', $old)->delete();
                    $this->table('properties')->whereIn('id', $old)->delete();
                }

                $this->commitTransaction();
                return true;
            } catch (\Exception $e) {
            }

            $this->rollbackTransaction();
            return false;

        }

        return true;
    }


    public function batchMergeAndRemoveEmployees($target, $old)
    {
        return $this->performBatch('employees', 'employee_id', $target, $old);
    }

    public function batchMergeAndRemoveFuels($target, $old)
    {
        return $this->performBatchOnConsumable($target, $old);
    }

    public function batchMergeAndRemoveMaterials($target, $old)
    {
        return $this->performBatchOnConsumable($target, $old);
    }

    public function batchMergeAndRemoveActivities($targetActivity, $oldActivities)
    {
        return $this->performBatch('activities', 'activity_id', $targetActivity, $oldActivities);
    }

    public function batchMergeAndRemoveDepartments($target, $old)
    {
        return $this->performBatch('departments', 'department_id', $target, $old);
    }

    public function batchMergeAndRemoveDistricts($target, $old)
    {
        return $this->performBatch('districts', 'district_id', $target, $old);
    }

    public function batchMergeAndRemoveProjects($target, $old)
    {
        return $this->performBatch('projects', 'project_id', $target, $old);
    }

    public function batchUpdateActivities($recordIDs, $newActivity)
    {
        if (count($recordIDs) > 0) {
            $this->startTransaction();
            try {

                $lastUpdatedDate = Carbon::now()->toDateTimeString();

                $this->table('work_entries')->whereIn('id', $recordIDs)
                    ->update([
                        'activity_id' => $newActivity,
                        'updated_at' => $lastUpdatedDate
                    ]);

                $this->commitTransaction();
                Event::fire('batch.activity', [$newActivity, $recordIDs]);
                return true;
            } catch (\Exception $e) {

            }
            $this->rollbackTransaction();
            return false;
        }

        return true;
    }

    public function batchUpdateRoads($recordIDs, $newRoad)
    {
        if (count($recordIDs) > 0) {
            $this->startTransaction();
            try {

                $lastUpdatedDate = Carbon::now()->toDateTimeString();
                $this->table('work_entries')->whereIn('id', $recordIDs)
                    ->update([
                        'property_organization_id' => $newRoad,
                        'updated_at' => $lastUpdatedDate
                    ]);

                $this->commitTransaction();
                Event::fire('batch.road', [$newRoad, $recordIDs]);
                return true;
            } catch (\Exception $e) {

            }
            $this->rollbackTransaction();
            return false;
        }

        return true;
    }

    public function batchUpdateProject($recordIDs, $newProject)
    {
        if (count($recordIDs) > 0) {
            $this->startTransaction();
            try {

                $lastUpdatedDate = Carbon::now()->toDateTimeString();

                $this->table('work_entries')->whereIn('id', $recordIDs)
                    ->update([
                        'project_id' => $newProject,
                        'updated_at' => $lastUpdatedDate
                    ]);

                $this->commitTransaction();
                Event::fire('batch.project', [$newProject, $recordIDs]);
                return true;
            } catch (\Exception $e) {

            }
            $this->rollbackTransaction();
            return false;
        }

        return true;
    }

    public function batchUpdateDistrict($recordIDs, $newDistrict)
    {
        if (count($recordIDs) > 0) {
            $this->startTransaction();
            try {

                $lastUpdatedDate = Carbon::now()->toDateTimeString();

                $this->table('work_entries')->whereIn('id', $recordIDs)
                    ->update([
                        'district_id' => $newDistrict,
                        'updated_at' => $lastUpdatedDate
                    ]);

                $this->commitTransaction();
                Event::fire('batch.district', [$newDistrict, $recordIDs]);
                return true;
            } catch (\Exception $e) {

            }
            $this->rollbackTransaction();
            return false;
        }

        return true;
    }

    public function batchUpdateDepartment($recordIDs, $newDepartment)
    {
        if (count($recordIDs) > 0) {
            $this->startTransaction();
            try {

                $lastUpdatedDate = Carbon::now()->toDateTimeString();

                $this->table('work_entries')->whereIn('id', $recordIDs)
                    ->update([
                        'department_id' => $newDepartment,
                        'updated_at' => $lastUpdatedDate
                    ]);

                $this->commitTransaction();
                Event::fire('batch.department', [$newDepartment, $recordIDs]);
                return true;
            } catch (\Exception $e) {

            }
            $this->rollbackTransaction();
            return false;
        }

        return true;
    }

    public function batchRemoveEmptyConsumableRecords()
    {
        $this->startTransaction();
        try {

            $this->table('work_consumables')->whereIn('consumable_type', [3, 4])->where(function ($q) {
                $q->where('total_cost', '=', 0);
                $q->where('total_quantity', '=', 0);
            })->delete();

            $this->commitTransaction();
            Event::fire('batch.cleanedRecords');
            return true;
        } catch (\Exception $e) {
        }
        $this->rollbackTransaction();
        return false;
    }

    /**
     * Removes all unused records such as materials, projects, etc.
     *
     * @return mixed
     */
    public function batchCleanRemovedRecords()
    {
        $this->startTransaction();

        try {
            // Clean up the projects.
            $this->table('projects')->whereRaw(
                'id not in (select distinct project_id from work_entries) and id > 0 and deleted_at is not null;'
            )->delete();

            // Clean up the activities.
            $this->table('activities')->whereRaw(
                'id not in (select distinct activity_id from work_entries) and id > 0 and deleted_at is not null;'
            )->delete();

            // Clean up the departments.
            $this->table('departments')->whereRaw(
                'id not in (select distinct department_id from work_entries) and id > 0 and deleted_at is not null;'
            )->delete();

            // Clean up the districts.
            $this->table('districts')->whereRaw(
                'id not in (select distinct district_id from work_entries) and id > 0 and deleted_at is not null;'
            )->delete();

            // Clean up the employees.
            $this->table('employees')->whereRaw(
                'id not in (select distinct employee_id from work_entries) and id > 0 and deleted_at is not null;'
            )->delete();

            // Clean up the consumables.
            $this->table('consumables')->whereRaw(
                'id not in (select distinct consumable_id from work_consumables) and id > 1 and deleted_at is not null'
            )->delete();

            // Clean up the properties.
            $this->table('properties')->whereRaw(
                'id not in (select distinct property_id from work_entries)
                    and id not in (select property_organization_id from work_entries)
                    and id > 1 and deleted_at is not null'
            )->delete();

            $this->batchRemoveEmptyConsumableRecords();

            $this->commitTransaction();
            return true;
        } catch (\Exception $e) {
        }

        $this->rollbackTransaction();
        return false;
    }

}