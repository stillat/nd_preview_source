<?php namespace ParadoxOne\NDCounties\Database\Repositories\WorkEntries;

use Exception;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\App;
use ParadoxOne\NDCounties\Contracts\Resolvers\ConsumableTypeResolverInterface;

class DynamicConsumableHydrator
{
    const PROPERTY_ODOMETER_READING = 'odometer';

    /**
     * The collection of consumable records
     *
     * @var array
     */
    protected $consumableRecords = null;

    /**
     * The filtered collection of consumable records.
     *
     * This will hold the consumable records in a jagged array. The key
     * will be the type of the consumable, defined by the resolver
     *
     * @var \Illuminate\Support\Collection
     */
    protected $filteredConsumableRecords = null;

    /**
     * The resolver type implementation
     *
     * @var \ParadoxOne\NDCounties\Contracts\Resolvers\ConsumableTypeResolverInterface
     */
    protected $typeResolver = null;

    protected $inventoryCreationManager = null;

    /**
     * Returns a new instance of DynamicConsumableHydrator
     *
     * @param ConsumableTypeResolverInterface $resolver
     */
    public function __construct(ConsumableTypeResolverInterface $resolver)
    {
        $this->filteredConsumableRecords = new Collection;

        $this->typeResolver = $resolver;

        $this->inventoryCreationManager = App::make('inventory.manager.create');

        $this->prepareFilteredConsumableRecords();
    }

    /**
     * Prepares the filtered consumable records.
     *
     * @return void
     */
    private function prepareFilteredConsumableRecords()
    {
        $this->filteredConsumableRecords = new Collection;

        foreach ($this->typeResolver->getTypeIdentifiers() as $identifier) {
            $this->filteredConsumableRecords->put($identifier, new Collection);
        }
    }

    /**
     * Sets the raw request data.
     *
     * @param array The request data, usually from Input
     */
    public function setRawData(array $data)
    {
        $this->consumableRecords = new Collection($data);
        $this->prepareFilteredConsumableRecords();
    }

    /**
     * The work entry ID.
     *
     * @var int
     */
    protected $workEntryID = 0;

    /**
     * Sets the work entry ID
     *
     * @param int $id
     */
    public function setWorkEntryID($id)
    {
        $this->workEntryID = $id;
    }

    /**
     * Gets the work entry ID
     *
     * @return int
     */
    public function getWorkEntryID()
    {
        return $this->workEntryID;
    }

    /**
     * Returns the filtered consumable records
     *
     * @var \Illuminate\Support\Collection
     * @return mixed
     */
    public function getConsumables()
    {
        return $this->filteredConsumableRecords;
    }

    /**
     * Returns an array of consumable materials
     *
     * @return array
     */
    public function getMaterials()
    {
        return $this->filteredConsumableRecords->toArray()['4'];
    }

    /**
     * Returns an array of consumable fuels
     *
     * @return array
     */
    public function getFuels()
    {
        return $this->filteredConsumableRecords->toArray()['3'];
    }

    /**
     * Returns an array of consumable equipment units
     *
     * @return array
     */
    public function getEquipmentUnits()
    {
        return $this->filteredConsumableRecords->toArray()['2'];
    }

    /**
     * Returns an array of consumable records
     *
     * @return array
     */
    public function getConsumablesArray()
    {
        return array_merge($this->getFuels(), $this->getMaterials(), $this->getEquipmentUnits());
    }

    /**
     * Assert the consumable counts are all the same.
     *
     * @param  int   $desiredCount
     * @param  array $checkArray
     * @throws HydratorException If the counts do not match.
     * @return mixed
     */
    private function assertConsumableCount($desiredCount, $checkArray)
    {
        if (count($checkArray) !== $desiredCount) {
            throw new HydratorException("There were inconsistencies within the consumable records. Hydration cannot continue.");

            return false; // Just in case
        }
    }

    /**
     * Creates a collection of consumable records based on input
     *
     * @throws HydratorException If there are inconsistencies in the consumable records input.
     */
    public function hydrateConsumables()
    {
        try {
            $this->hydrateMaterials();
            $this->hydrateFuels();
            $this->hydrateEquipmentUnits();
        } catch (HydratorException $e) {
            throw $e;
        }
    }

    /**
     * Determines if a given equipment has fuels for the record.
     *
     * This assists not removing equipment units that have 0 quantity and cost
     * when they are being used to enter in fuels on a record.
     *
     * @param $boundedRecord
     * @return bool
     */
    private function equipmentUnitHasFuels($boundedRecord)
    {
        $fuels = array_pluck_where($this->filteredConsumableRecords[3]->toArray(), 'bindingConsumableRecord', $boundedRecord);
        if (count($fuels) > 0) {
            return true;
        }
        return false;
    }

    /**
     * Creates a collection of fuel records based on input
     *
     * @throws HydratorException If there are inconsistencies in the consumable records input.
     */
    private function hydrateFuels()
    {
        $fuels             = $this->consumableRecords->offsetGet('fuels');
        $fuelQuantities    = $this->consumableRecords->offsetGet('fuelQuantity');
        $fuelUnitCosts     = $this->consumableRecords->offsetGet('fuelUnitCosts');
        $fuelTotalCosts    = $this->consumableRecords->offsetGet('fuelTotalCost');
        $associatedTypeIDs = $this->consumableRecords->offsetGet('fuelAssociatedType');
        $fuelTaxed         = $this->consumableRecords->offsetGet('fuelTaxed');
        $boundedRecords    = $this->consumableRecords->offsetGet('boundFuelEquipmentRecords');
        $managingInventory = $this->consumableRecords->offsetGet('fuel_tracking_inventory');

        $recordCount = count($fuels);

        $this->assertConsumableCount($recordCount, $fuels);
        $this->assertConsumableCount($recordCount, $fuelQuantities);
        $this->assertConsumableCount($recordCount, $fuelUnitCosts);
        $this->assertConsumableCount($recordCount, $fuelTotalCosts);
        $this->assertConsumableCount($recordCount, $associatedTypeIDs);
        $this->assertConsumableCount($recordCount, $fuelTaxed);
        $this->assertConsumableCount($recordCount, $managingInventory);

        for ($i = 0; $i < $recordCount; $i++) {
            $consumableType      = 3;
            $consumableID        = $fuels[$i];
            $associatedType      = 2;
            $associatedID        = $associatedTypeIDs[$i];
            $quantity            = $fuelQuantities[$i];
            $historicCost        = $fuelUnitCosts[$i];
            $totalCost           = $fuelTotalCosts[$i];
            $bindingRecord       = $boundedRecords[$i];
            $taxed               = (bool)$fuelTaxed[$i];
            $isManagingInventory = (bool)$managingInventory[$i];

            if (!($consumableID == 0 && $quantity == 0 && $historicCost == 0 && $totalCost == 0)) {
                if ($isManagingInventory) {
                    $this->inventoryCreationManager->addConsumableItem($consumableID, $quantity);
                }

                $this->filteredConsumableRecords[$consumableType]->push(
                    $this->retrieveConsumableRecord($consumableType, $consumableID, $associatedType, $associatedID,
                                                    $historicCost, $totalCost, 0, $quantity, $bindingRecord, $taxed)
                );
            }

        }
    }

    /**
     * Creates a collection of equipment records based on input
     *
     * @throws HydratorException If there are inconsistencies in the consumable records input.
     */
    private function hydrateEquipmentUnits()
    {
        $equipmentUnits          = $this->consumableRecords->offsetGet('equipmentUnits');
        $equipmentQuantities     = $this->consumableRecords->offsetGet('equipmentQuantities');
        $equipmentRates          = $this->consumableRecords->offsetGet('equipmentRates');
        $equipmentTotalCosts     = $this->consumableRecords->offsetGet('equipmentTotalCosts');
        $equipmentOdometers      = $this->consumableRecords->offsetGet('equipmentOdometerReadings');
        $equipmentTaxed          = $this->consumableRecords->offsetGet('equipmentTaxed');
        $equipmentBindingRecords = $this->consumableRecords->offsetGet('equipmentBindingRecords');

        $recordCount = count($equipmentUnits);

        $this->assertConsumableCount($recordCount, $equipmentUnits);
        $this->assertConsumableCount($recordCount, $equipmentQuantities);
        $this->assertConsumableCount($recordCount, $equipmentRates);
        $this->assertConsumableCount($recordCount, $equipmentTotalCosts);
        $this->assertConsumableCount($recordCount, $equipmentOdometers);
        $this->assertConsumableCount($recordCount, $equipmentTaxed);
        $this->assertConsumableCount($recordCount, $equipmentBindingRecords);

        for ($i = 0; $i < $recordCount; $i++) {
            $consumableType = 2;
            $consumableID   = $equipmentUnits[$i];
            $quantity       = $equipmentQuantities[$i];
            $historicCost   = $equipmentRates[$i];
            $totalCost      = $equipmentTotalCosts[$i];
            $taxed          = (bool)$equipmentTaxed[$i];
            $bindingRecord  = $equipmentBindingRecords[$i];

            if ($this->equipmentUnitHasFuels($bindingRecord) || !($consumableID == 1 && $quantity == 0 && $historicCost == 0 && $totalCost == 0)) {
                $equipmentUnit =
                    $this->retrieveConsumableRecord($consumableType, $consumableID, 0, 0, $historicCost, $totalCost, 0,
                                                    $quantity, $bindingRecord, $taxed);
                $equipmentUnit->setProperty(self::PROPERTY_ODOMETER_READING, $equipmentOdometers[$i]);

                $this->filteredConsumableRecords[$consumableType]->push($equipmentUnit);
            }
        }
    }

    /**
     * Creates a collection of material records based on input
     *
     * @throws HydratorException If there are inconsistencies in the consumable records input.
     */
    private function hydrateMaterials()
    {
        $materials          = $this->consumableRecords->offsetGet('material');
        $materialQuantities = $this->consumableRecords->offsetGet('materialQuantity');
        $materialUnitCost   = $this->consumableRecords->offsetGet('materialUnitCost');
        $materialTotalCost  = $this->consumableRecords->offsetGet('materialTotalCost');
        $materialTaxed      = $this->consumableRecords->offsetGet('materialTaxable');
        $managingInventory  = $this->consumableRecords->offsetGet('material_tracking_inventory');


        $recordCount = count($materials);

        $this->assertConsumableCount($recordCount, $materials);
        $this->assertConsumableCount($recordCount, $materialQuantities);
        $this->assertConsumableCount($recordCount, $materialUnitCost);
        $this->assertConsumableCount($recordCount, $materialTotalCost);
        $this->assertConsumableCount($recordCount, $materialTaxed);
        $this->assertConsumableCount($recordCount, $managingInventory);

        for ($i = 0; $i < $recordCount; $i++) {
            $consumableType      = 4;
            $consumableId        = $materials[$i];
            $quantity            = $materialQuantities[$i];
            $historicCost        = $materialUnitCost[$i];
            $totalCost           = $materialTotalCost[$i];
            $taxed               = (bool)$materialTaxed[$i];
            $isManagingInventory = (bool)$managingInventory[$i];

            if (!($consumableId == 1 && $quantity == 0 && $historicCost == 0 && $totalCost == 0)) {
                if ($isManagingInventory) {
                    $this->inventoryCreationManager->addConsumableItem($consumableId, $quantity);
                }

                $this->filteredConsumableRecords[$consumableType]->push(
                    $this->retrieveConsumableRecord($consumableType, $consumableId, 0, 0, $historicCost, $totalCost, 0,
                                                    $quantity, 0, $taxed)
                );
            }
        }
    }

    private function retrieveConsumableRecord($consumableType, $consumableId, $associatedType, $associatedID, $historicCost,
                                              $totalCost, $measurementUnit, $quantity, $bindingRecordIdentifier, $taxed = 0)
    {
        $consumable = $this->typeResolver->makeInstance($consumableType);
        $consumable->setConsumableType($consumableType);
        $consumable->setConsumableID($consumableId);
        $consumable->setAssociatedType($associatedType);
        $consumable->setAssociatedID($associatedID);
        $consumable->setWorkEntryID($this->getWorkEntryID());
        $consumable->setMeasurementUnit($measurementUnit);
        $consumable->setTotalCost($totalCost);
        $consumable->setHistoricCost($historicCost);
        $consumable->setQuantity($quantity);
        $consumable->setTaxed($taxed);
        $consumable->setBindingRecord($bindingRecordIdentifier);

        return $consumable;
    }
}

class HydratorException extends Exception
{
}
