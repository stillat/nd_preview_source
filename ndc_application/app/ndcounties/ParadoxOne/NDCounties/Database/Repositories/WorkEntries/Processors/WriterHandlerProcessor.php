<?php namespace ParadoxOne\NDCounties\Database\Repositories\WorkEntries\Processors;

use ParadoxOne\NDCounties\Contracts\Properties\PropertyTypesRepositoryInterface;
use ParadoxOne\NDCounties\Database\Adapters\AdapterManager;
use ParadoxOne\NDCounties\Database\Repositories\WorkEntries\EloquentWorkEntryWriterRepository;
use ParadoxOne\NDCounties\Database\Repositories\WorkEntries\Updaters\DatabaseWorkEntryUpdater;
use stdClass;

class WriterHandlerProcessor
{
    protected $manager;

    protected $writer = null;

    protected $updater = null;

    protected $typesRepository;

    protected $adapterRuns = [];

    public function __construct(AdapterManager $manager, PropertyTypesRepositoryInterface $typesRepository)
    {
        $this->manager = $manager;

        $this->typesRepository = $typesRepository;
    }

    /**
     * Sets the writer instance. This is a REFERENCE.
     *
     * @param \ParadoxOne\NDCounties\Database\Repositories\WorkEntries\EloquentWorkEntryWriterRepository $writer
     */
    public function setEloquentWriter(EloquentWorkEntryWriterRepository &$writer)
    {
        $this->writer = $writer;
    }

    public function setEloquentUpdater(DatabaseWorkEntryUpdater &$updater)
    {
        $this->updater = $updater;
    }

    public function disconnect()
    {
        unset($this->writer);
        unset($this->updater);
        $this->typesRepository = null;
        unset($this->typesRepository);
    }

    /**
     * Returns the adapter runs.
     *
     * @return array
     */
    public function getAdapterRuns()
    {
        return $this->adapterRuns;
    }

    private function getPropertyContext()
    {
        if ($this->writer != null) {
            return $this->writer->getPropertyContext();
        }

        if ($this->updater != null) {
            return $this->updater->getPropertyContext();
        }
    }

    private function mergeValidationErrors($errors)
    {
        if ($this->writer != null) {
            $this->writer->mergeValidationErrors($errors);
        }

        if ($this->updater != null) {
            $this->updater->mergeValidationErrors($errors);
        }
    }

    private function setPropertyTotal($total)
    {
        if ($this->writer != null) {
            $this->writer->setPropertyTotal($total);
        }

        if ($this->updater != null) {
            $this->updater->setPropertyTotal($total);
        }
    }

    public function process($workEntryID, $data)
    {
        $handlerObject       = new stdClass;
        $handlerObject->id   = $workEntryID;
        $handlerObject->data = $data;

        $adapters = $this->manager->getAdapters();

        if ($this->getPropertyContext() != 1) {
            // This is used when the property set in the form is an actual property.

            $temporaryPropertyType = $this->typesRepository->getModelById($data['record_property_type'], 0);

            $typeAdapters = $temporaryPropertyType->adapters->lists('id');

            // Clean up the property type.
            $temporaryPropertyType = null;
            unset($temporaryPropertyType);
        } else {
            // This is hard-coding the expected type adapters for the equipment units.
            $typeAdapters = [-1];
        }

        $propertyTotal = 0;

        foreach ($adapters as $adapter) {
            if (in_array($adapter->getAdapterNumericIdentifier(), $typeAdapters)) {
                $handler = $adapter->getCreatePropertyHandler();

                $success = $handler->handleLog($handlerObject);

                if ($success) {
                    $propertyTotal += $handler->getRecordPropertyTotal();
                }

                $this->adapterRuns[$adapter->getAdapterNumericIdentifier()] = $success;

                if ($success === false) {
                    $this->mergeValidationErrors($handler->logErrors());
                }
            }
        }

        $this->setPropertyTotal($propertyTotal);
    }
}
