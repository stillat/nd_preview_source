<?php namespace ParadoxOne\NDCounties\Database\Repositories\WorkEntries\Builders;

trait OrganizationStrategyBuilderTrait
{
    protected $activityIDs = null;

    protected $employeeIDs = null;

    protected $departmentIDs = null;

    protected $districtIDs = null;

    protected $projectIDs = null;

    /**
     * Gets the property IDs returned.
     *
     * @return mixed
     */
    public function getProperties()
    {
        return $this->propertyIDs;
    }

    /**
     * Gets the activity IDs returned.
     *
     * @return mixed
     */
    public function getActivities()
    {
        if ($this->activityIDs == null) {
            $this->activityIDs = array_pluck_unique($this->workEntryRecords, 'activity_id');
        }

        return $this->activityIDs;
    }

    /**
     * Gets the employee IDs returned.
     *
     * @return mixed
     */
    public function getEmployees()
    {
        if ($this->employeeIDs == null) {
            $this->employeeIDs = array_pluck_unique($this->workEntryRecords, 'employee_id');
        }

        return $this->employeeIDs;
    }

    /**
     * Gets the department IDs returned.
     *
     * @return mixed
     */
    public function getDepartments()
    {
        if ($this->departmentIDs == null) {
            $this->departmentIDs = array_pluck_unique($this->workEntryRecords, 'department_id');
        }

        return $this->departmentIDs;
    }

    /**
     * Gets the district IDs returned.
     *
     * @return mixed
     */
    public function getDistricts()
    {
        if ($this->districtIDs == null) {
            $this->districtIDs = array_pluck_unique($this->workEntryRecords, 'district_id');
        }

        return $this->districtIDs;
    }

    /**
     * Gets the project IDs returned.
     *
     * @return mixed
     */
    public function getProjects()
    {
        if ($this->projectIDs == null) {
            $this->projectIDs = array_pluck_unique($this->workEntryRecords, 'project_id');
        }

        return $this->projectIDs;
    }

    /**
     * Gets the equipment unit IDs returned.
     *
     * @return mixed
     */
    public function getEquipmentUnits()
    {
        return $this->equipmentIDs;
    }
}
