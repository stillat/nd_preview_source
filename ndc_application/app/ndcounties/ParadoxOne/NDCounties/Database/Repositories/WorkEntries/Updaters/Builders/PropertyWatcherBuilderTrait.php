<?php namespace ParadoxOne\NDCounties\Database\Repositories\WorkEntries\Updaters\Builders;

use Illuminate\Support\Facades\Input;

trait PropertyWatcherBuilderTrait
{
    /**
     * Indicates if the property was changed.
     *
     * @return bool
     */
    public function propertyWasChanged()
    {
        return (Input::get('property_was_changed', 0) == 1);
    }

    /**
     * Indicates if the property was updated.
     *
     * @return bool
     */
    public function propertyWasUpdated()
    {
        return (Input::get('property_was_updated', 0) == 1);
    }

    /**
     * Indicates if the property was reset to the default property or equipment.
     *
     * @return bool
     */
    private function propertyWasResetToDefault()
    {
        if ($this->propertyWasChanged() && in_array($this->propertyID, [0, 1])) {
            return true;
        }

        return false;
    }
}
