<?php namespace ParadoxOne\NDCounties\Database\Repositories\WorkEntries\Builders;

use Illuminate\Database\Connection;

trait MaterialBuilderTrait
{
    /**
     * The actual material records.
     *
     * @var array
     */
    protected $materialRecords = [];

    /**
     * Fills the material records.
     *
     * @param Connection $connection
     * @param            $materialRecords
     * @return array
     */
    private function fillMaterialRecords(Connection &$connection, &$materialRecords)
    {
        // Remove any record of 0 from the array. This should not be there.
        // 0 is used for fuels.
        $materialRecords = array_diff($materialRecords, [0]);

        // If there are no items in the array, let's add the default material record
        // there.
        if (count($materialRecords) == 0) {
            $materialRecords[] = 0;
        }

        if (!in_array(0, $materialRecords)) {
            $materialRecords[] = 0;
        }

        $lastRecord = null;
        list($lastRecord) = array_slice($materialRecords, -1);

        if (count($materialRecords) == 1 and $lastRecord == 1) {
            $material              = new \stdClass;
            $material->id          = 0;
            $material->code        = 'NA';
            $material->name        = 'NA';
            $material->description = 'NA';
            $material->unit_id     = 9;
            $material->cost        = 0;

            $this->materialRecords[] = $material;
        } else {
            if (count($materialRecords) > 0) {
                $this->materialRecords = $connection->table('consumables')->where('consumable_context', '=', 2)->whereIn('id', $materialRecords)->get();
            }
        }

        $discoveredMeasurementUnits = [];

        foreach ($this->materialRecords as $matRecord) {
            $discoveredMeasurementUnits[] = $matRecord->unit_id;
        }

        // Memory clean up.
        unset($connection);
        unset($materialRecords);

        return $discoveredMeasurementUnits;
    }
}
