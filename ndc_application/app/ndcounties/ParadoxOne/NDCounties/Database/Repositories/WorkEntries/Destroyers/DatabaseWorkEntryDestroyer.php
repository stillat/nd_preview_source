<?php namespace ParadoxOne\NDCounties\Database\Repositories\WorkEntries\Destroyers;

use Carbon;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Event;
use ParadoxOne\NDCounties\Contracts\Finance\Invoices\InvoiceUpdaterRepositoryInterface;
use ParadoxOne\NDCounties\Contracts\WorkEntries\WorkEntryDestroyerInterface;
use Stillat\Database\Tenant\TenantManager;

class DatabaseWorkEntryDestroyer implements WorkEntryDestroyerInterface
{
    /**
     * The database connection
     *
     * @var \Illuminate\Database\Connection
     */
    protected $db = null;

    protected $validationErrors = null;

    protected $invoiceUpdaterInstance = null;

    public function __construct(InvoiceUpdaterRepositoryInterface $invoiceUpdater)
    {
        $tenantManagerInstance = TenantManager::instance();
        $tenantManagerInstance->assumeTenant(Auth::user()->last_tenant);
        $this->db = DB::connection($tenantManagerInstance->getCurrentConnection());

        $this->invoiceUpdaterInstance = $invoiceUpdater;

        $tenantManagerInstance = null;
        unset($tenantManagerInstance);
    }

    public $someRecordsLocked = false;

    /**
     * Destroy a work record.
     *
     * @param $workRecordID
     * @param $softDelete
     * @return mixed
     */
    public function destroy($workRecordID, $softDelete = false)
    {
        if (!is_array($workRecordID)) {
            $workRecordID = (array)$workRecordID;
        }

        if (count($workRecordID) == 0) {
            return true;
        }
        $workRecordsThatShouldNotBeRemoved = $this->db->table('work_entries')->where('record_status', '=', 5)->whereIn('id', $workRecordID)->lists('id');
        $workRecordsThatShouldNotBeRemoved = array_map('strval', $workRecordsThatShouldNotBeRemoved);

        $workRecordID = array_diff($workRecordID, $workRecordsThatShouldNotBeRemoved);

        // One more check.

        if (count($workRecordsThatShouldNotBeRemoved) > 0) {
            $this->someRecordsLocked = true;
        }

        if (count($workRecordID) == 0) {
            return true;
        }

        $this->db->beginTransaction();

        try {
            if ($softDelete) {
                $deleteTime = Carbon::now()->toDateTimeString();

                $this->db->table('work_consumables')->whereIn('work_entry_id', $workRecordID)
                         ->update(array('deleted_at' => $deleteTime));
                $this->db->table('work_entries')->whereIn('id', $workRecordID)
                         ->update(array('deleted_at' => $deleteTime));

                $processor =
                    App::make('ParadoxOne\NDCounties\Database\Repositories\WorkEntries\Processors\DestroyerHandlerProcessor');
                $processor->setDestroyer($this);
                $processor->process($workRecordID, $softDelete);

                if (in_array(false, $processor->getAdapterRuns())) {
                    $this->db->rollBack();

                    return false;
                }

                $processor = null;
                unset($processor);

                $this->db->commit();
                Event::fire('workrecord.removed', [$workRecordID]);

                return true;
            } else {
                foreach ($workRecordID as $recordID) {
                    // Handle the invoice data.
                    $this->invoiceUpdaterInstance->touchInvoicesForDestroy($recordID);
                }

                // Remove the core work data.
                $this->db->table('work_consumables')->whereIn('work_entry_id', $workRecordID)->delete();
                $this->db->table('work_entries')->whereIn('id', $workRecordID)->delete();

                // Handle the processor data.
                $processor =
                    App::make('ParadoxOne\NDCounties\Database\Repositories\WorkEntries\Processors\DestroyerHandlerProcessor');
                $processor->setDestroyer($this);
                $processor->process($workRecordID, $softDelete);

                // Handle the adapter data.
                if (in_array(false, $processor->getAdapterRuns())) {
                    $this->db->rollBack();

                    return false;
                }

                $processor = null;
                unset($processor);

                $this->invoiceUpdaterInstance->cleanupInvoiceLinks();

                $this->db->commit();

                Event::fire('workrecord.removed', [$workRecordID]);

                return true;
            }
        } catch (\Exception $e) {
            d($e);
            exit;
        }

        $this->db->rollBack();
    }

    public function mergeValidationErrors($errors)
    {
        if ($errors == null) {
            return;
        }

        $this->validationErrors = $this->validationErrors->merge($errors);
    }

    public function errors()
    {
        return $this->validationErrors;
    }
}
