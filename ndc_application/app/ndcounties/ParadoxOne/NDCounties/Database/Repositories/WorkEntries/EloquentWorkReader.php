<?php namespace ParadoxOne\NDCounties\Database\Repositories\WorkEntries;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Paginator;
use ParadoxOne\NDCounties\Contracts\WorkEntries\WorkEntryReaderInterface;
use Service\EquipmentUnits\Unit as EquipmentUnit;
use Service\Properties\Property;
use Service\WorkEntries\WorkEntry;

class EloquentWorkReader implements WorkEntryReaderInterface
{
    protected $paginationPerPage = 0;

    protected $paginationCurrentPage = 0;

    protected $modelData = null;

    protected $entryEquipmentProperties = [];

    protected $entryProperties = [];

    protected $paginationLinks = [];

    /**
     * Sets the pagination details.
     *
     * @param int $perPage
     * @param int $currentPage
     * @return mixed
     */
    public function setPagination($perPage, $currentPage)
    {
        $this->paginationPerPage     = $perPage;
        $this->paginationCurrentPage = $currentPage;
    }

    /**
     * Returns the equipment property ID's for the set of work records.
     *
     * @return array
     */
    private function getEquipmentProperties()
    {
        return array_pluck_unique(array_pluck_where($this->modelData, 'property_context', 1), 'property_id');
    }

    /**
     * Returns the actual property ID's for the set of work records.
     *
     * @return array
     */
    private function getActualProperties()
    {
        return array_pluck_unique(array_pluck_where($this->modelData, 'property_context', 0), 'property_id');
    }

    private function fillEquipmentProperties()
    {
        $equipmentProperties = $this->getEquipmentProperties();

        if (count($equipmentProperties) > 0) {
            $this->entryEquipmentProperties = EquipmentUnit::whereIn('id', $equipmentProperties)->get()->toArray();
        } else {
            $this->entryEquipmentProperties = [];
        }
    }

    private function fillActualProperties()
    {
        $actualProperties = $this->getActualProperties();

        if (count($actualProperties) > 0) {
            $this->entryProperties = Property::whereIn('id', $actualProperties)->get()->toArray();
        } else {
            $this->entryProperties = [];
        }
    }

    private function lookupProperty($context, $propertyID)
    {
        if ($context == 0) {
            return array_pluck_where($this->entryProperties, 'id', $propertyID);
        } elseif ($context == 1) {
            return array_pluck_where($this->entrEquipmentProperties, 'id', $propertyID);
        }
    }

    /**
     * Fills the reader's model data.
     *
     * @return void
     */
    private function fillModelData()
    {
        $this->modelData = WorkEntry::select(DB::raw('work_entries.*'),
            // Employee information.
                                             'employees.first_name as employee_first_name',
                                             'employees.last_name as employee_last_name',
                                             'employees.code as employee_code',
                                             'employees.id as employee_id',

            // Activity information.
                                             'activities.name as activity_name',
                                             'activities.code as activity_code',
                                             'activities.id as activity_id',

            // Project information.
                                             'projects.name as project_name',
                                             'projects.code as project_code',
                                             'projects.id as project_id',

            // Department information.
                                             'departments.name as department_name',
                                             'departments.code as department_code',
                                             'departments.id as department_id',

            // District information.
                                             'districts.name as district_name',
                                             'districts.code as district_code',
                                             'districts.id as district_id'

        )->with(['materials.info', 'equipmentunits.info'])
                                    ->join('employees', 'employee_id', '=', 'employees.id')
                                    ->join('activities', 'activity_id', '=', 'activities.id')
                                    ->join('projects', 'project_id', '=', 'projects.id')
                                    ->join('departments', 'department_id', '=', 'departments.id')
                                    ->join('districts', 'district_id', '=', 'districts.id')
                                    ->paginate($this->paginationPerPage);

        $this->paginationLinks = $this->modelData->links();
    }

    public function links()
    {
        return $this->paginationLinks;
    }

    /**
     * Returns the collection of records.
     *
     * @return mixed
     */
    public function getRecords()
    {
        $this->fillModelData();
        $this->fillActualProperties();
        $this->fillEquipmentProperties();

        $tempRecords = [];

        foreach ($this->modelData as $key => $dataObject) {
            $temp                      = new \stdClass;
            $temp->property            =
                $this->lookupProperty($dataObject->property_context, $dataObject->property_id)[0];
            $temp->equipmentunits      = $dataObject->equipmentunits;
            $temp->employee_first_name = $dataObject->employee_first_name;
            $temp->employee_last_name  = $dataObject->employee_last_name;
            $temp->employee_code       = $dataObject->employee_code;
            $temp->employee_id         = $dataObject->employee_id;

            $temp->employee_hours_regular_worked  = $dataObject->employee_hours_regular_worked;
            $temp->employee_hours_overtime_worked = $dataObject->employee_hours_overtime_worked;
            $temp->total_employee_combined_cost   = $dataObject->total_employee_combined_cost;

            $temp->activity_name = $dataObject->activity_name;
            $temp->activity_code = $dataObject->activity_code;
            $temp->activity_id   = $dataObject->activity_id;

            $temp->project_name = $dataObject->project_name;
            $temp->project_code = $dataObject->project_code;
            $temp->project_id   = $dataObject->project_id;

            $temp->department_name = $dataObject->department_name;
            $temp->department_code = $dataObject->department_code;
            $temp->department_id   = $dataObject->department_id;

            $temp->district_name = $dataObject->district_name;
            $temp->district_code = $dataObject->district_code;
            $temp->district_id   = $dataObject->district_id;
            $temp->date_entered  = $dataObject->date_entered;

            $temp->materials = $dataObject->materials;

            $tempRecords[] = $temp;
        }


        $this->modelData                = null;
        $this->entryEquipmentProperties = null;
        $this->entryProperties          = null;
        unset($this->modelData);
        unset($this->entryEquipmentProperties);
        unset($this->entryProperties);


        // Just return model data for now.
        return $tempRecords;
    }
}
