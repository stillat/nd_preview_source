<?php namespace ParadoxOne\NDCounties\Database\Repositories\WorkEntries;

class InvalidWorkEntryException extends \Exception
{
    public $workEntryID = 0;
}
