<?php namespace ParadoxOne\NDCounties\Database\Repositories\WorkEntries\InventoryManagers;

class DeleteWorkInventoryManager extends AbstractInventoryProcessor
{
    /**
     * Processes the inventory adjustment.
     *
     * @return mixed
     */
    public function process()
    {
        $this->startTransaction();

        try {
            if ($this->allInventorySettings->convert_removed_work_inventory_items_to_system) {
                // This will retain all the inventory adjustment details.

                $this->getTable('inventory_adjustments')
                             ->where('adjustment_context', '=', 2)
                             ->where('adjustment_value', '=', $this->workEntryID[0])
                             ->update([
                                          'adjustment_context' => 0,
                                          'adjustment_value'   => 1
                                      ]);

                $this->commitTransaction();

                return true;
            } else {
                // We will need this to refresh the inventory levels later.
                $consumables = $this->getTable('inventory_adjustments')->select('consumable_id')
                                    ->where('adjustment_context', '=', 2)
                                    ->where('adjustment_value', '=', $this->workEntryID[0])->lists('consumable_id');

                $this->getTable('inventory_adjustments')->where('adjustment_context', '=', 2)
                     ->where('adjustment_value', '=', $this->workEntryID[0])->delete();

                foreach ($consumables as $consumableID) {
                    $this->inventoryAdjustmentRepository->refreshInventoryLevel($consumableID);
                }

                $this->commitTransaction();

                return true;
            }
        } catch (\Exception $e) {
            $this->rollbackTransaction();
        }

        return false;
    }
}
