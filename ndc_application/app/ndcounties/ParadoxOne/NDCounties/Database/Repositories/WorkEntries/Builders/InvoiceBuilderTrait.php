<?php namespace ParadoxOne\NDCounties\Database\Repositories\WorkEntries\Builders;

use Illuminate\Support\Facades\DB;
use Whoops\Example\Exception;

trait InvoiceBuilderTrait
{
    /**
     * Returns a collection of work records for a given invoice.
     *
     * @param $invoiceID
     * @return mixed
     */
    public function forInvoice($invoiceID)
    {
        $invoices = [];

        if (is_array($invoiceID) && count($invoiceID) > 0) {
            $invoices = $invoiceID;
        } elseif (is_numeric($invoiceID)) {
            $invoices = array($invoiceID);
        } else {
            return [];
        }

        // This will allow us to use the same instance of the reader for multiple updates later on.
        foreach ($this->reportQuery->joins as $key => $join) {
            if ($join->table == 'fs_invoice_work_records') {
                unset($this->reportQuery->joins[$key]);
            }
        }

        $this->mergeSelectionQueue([DB::raw('fs_invoice_work_records.invoice_id as invoice_id')]);

        $this->reportQuery = $this->reportQuery->join('fs_invoice_work_records', 'work_entries.id', '=',
            'fs_invoice_work_records.work_record_id')
                                               ->whereIn('fs_invoice_work_records.invoice_id', $invoices);

        return $this;
    }
}
