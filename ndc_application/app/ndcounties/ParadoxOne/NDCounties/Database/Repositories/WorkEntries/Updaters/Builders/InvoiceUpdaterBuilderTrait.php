<?php namespace ParadoxOne\NDCounties\Database\Repositories\WorkEntries\Updaters\Builders;

use Carbon\Carbon;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Input;

trait InvoiceUpdaterBuilderTrait
{
    /**
     * Indicates if the invoice was removed.
     *
     * @var bool
     */
    protected $invoiceWasRemoved = false;

    /**
     * The original invoice ID (before any updates).
     *
     * @var null
     */
    protected $originalInvoiceID = null;

    /**
     * The current invoice ID (if any).
     *
     * @var null
     */
    protected $currentInvoiceNumber = null;

    /**
     * Indicates if the invoice is being created new.
     *
     * @var bool
     */
    protected $invoiceBeingCreated = false;

    /**
     * The invoice updater repository implementation.
     *
     * @var \ParadoxOne\NDCounties\Contracts\Finance\Invoices\InvoiceUpdaterRepositoryInterface
     */
    protected $invoiceUpdaterRepository;

    /**
     * This function handles removing and tax lines the user wants to get rid of.
     */
    private function handleTaxRateLineItemsRemoval()
    {
        $taxLinesToRemove = [];

        if (Input::has('removed_tax_lines')) {
            $taxLinesToRemove = Input::get('removed_tax_lines');
        }

        if (count($taxLinesToRemove) > 0) {
            $this->invoiceWriterRepository->removeTaxLinesFromInvoice($taxLinesToRemove);
        }
    }

    private function handleInvoiceUpdate()
    {
        // Create some repositories we will need.
        $this->invoiceUpdaterRepository =
            App::make('ParadoxOne\NDCounties\Contracts\Finance\Invoices\InvoiceUpdaterRepositoryInterface');

        // This needs to detect if an invoice was removed. If so, we need to remove the work record
        // the invoice was associated with and refresh. We can do this by faking the remove work log process.

        // Determine if the invoice was removed.
        if (Input::get('invoice_was_removed', 0) == 1) {
            $this->invoiceWasRemoved = true;
        }

        // Get what the original invoice ID was (if any).
        if (Input::has('original_invoice_id') && strlen(Input::get('original_invoice_id')) > 0) {
            $this->originalInvoiceID = Input::get('original_invoice_id');
        }

        // Get the current invoice number (if any).
        $this->currentInvoiceNumber = Input::get('billing_invoice_number', null);

        if (Input::get('billing_invoice_number_created', 0) == 1) {
            $this->invoiceBeingCreated = true;
        }

        if ($this->invoiceWasRemoved) {
            // Remove the work record from the original invoice and refresh that invoice.
            try {
                $this->invoiceUpdaterRepository->touchInvoicesForDestroy($this->getWorkEntryID());
            } catch (\Exception $e) {
                lk($e);
            }
        }

        if ($this->currentInvoiceNumber != null) {
            $this->invoiceWriterRepository =
                App::make('ParadoxOne\NDCounties\Contracts\Finance\Invoices\InvoiceWriterRepositoryInterface');

            // We need to make sure that the original invoice ID and the current ID are not the same. If they are
            // we cannot trash the associated records.
            if ($this->originalInvoiceID != null && $this->originalInvoiceID != $this->currentInvoiceNumber) {
                $this->invoiceUpdaterRepository->touchInvoicesForDestroy($this->getWorkEntryID());
            }

            try {

                // First let's handle any annoying tax rates we might have to remove.
                $this->handleTaxRateLineItemsRemoval();

                // This is all we have to do for both create and appending. The `handleInvoiceCreation` method
                // is smart enough to know what to do.
                $this->handleInvoiceCreation($this->getWorkEntryID());

                // Here we need to pass in some invoice information so the updater can update the
                // invoice terms, notes, etc.
                $invoiceUpdateMetaData = new \stdClass;
                $invoiceUpdateMetaData->client_notes    = Input::get('invoice_notes_to_client', '');
                $invoiceUpdateMetaData->invoice_terms   = Input::get('invoice_terms', '');
                $invoiceUpdateMetaData->discount_amount = null;
                $invoiceUpdateMetaData->billed_account  = null;
                $invoiceUpdateMetaData->paid_account    = null;

                $invoiceUpdateMetaData->discount_amount        = Input::get('billing_invoice_discount', 0);
                $invoiceUpdateMetaData->due_date =
                    (new Carbon(Input::get('billing_invoice_due_date')))->toDateTimeString();

                $this->invoiceUpdaterRepository->refreshInformation($this->invoiceIDFromWriter, $invoiceUpdateMetaData);
            } catch (\Exception $e) {
                lk($e);
            }
        }
    }
}
