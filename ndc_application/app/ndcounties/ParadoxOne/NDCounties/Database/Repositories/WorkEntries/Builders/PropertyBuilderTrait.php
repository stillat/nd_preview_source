<?php namespace ParadoxOne\NDCounties\Database\Repositories\WorkEntries\Builders;

use Illuminate\Database\Connection;
use Illuminate\Support\Facades\App;

trait PropertyBuilderTrait
{
    /**
     * Holds the actual property records.
     *
     * @var array
     */
    protected $propertyRecords = [];

    /**
     * Holds the adapter records.
     *
     * @var array
     */
    protected $adapterRecords = [];

    /**
     * Fills the properties.
     *
     * @param Connection $connection
     * @param            $propertyIDs
     * @param            $workLogIDs
     */
    private function fillProperties(Connection &$connection, &$propertyIDs, &$workLogIDs)
    {
        if (count($workLogIDs) == 0) {
            return;
        }

        if (count($propertyIDs) == 1 and $propertyIDs[0] == 0) {
            $property              = new \stdClass;
            $property->id          = 0;
            $property->code        = 'NA';
            $property->name        = 'NA';
            $property->description = 'NA';

            $this->propertyRecords[] = $property;
        } else {
            if (count($propertyIDs) > 0) {
                $this->propertyRecords =
                    App::make('ParadoxOne\NDCounties\Contracts\Properties\PropertiesRepositoryInterface')
                       ->getPropertiesIn($propertyIDs)->toArray();
            }
        }

        $adapterManager = App::make('ParadoxOne\NDCounties\Database\Adapters\AdapterManager');

        foreach ($adapterManager->getAdapters() as $adapter) {
            $identifier = $adapter->getAdapterNumericIdentifier();

            if ($identifier == -1) {
                $identifier = 'e';
            }

            if ($adapter->getWorkColumnName() !== null) {
                $this->adapterRecords[$identifier] =
                    $connection->table($adapter->getTableName())->whereIn($adapter->getWorkColumnName(), $workLogIDs)
                               ->get();
            }

            unset($identifier);
        }
        // Memory clean up.
        unset($connection);
        unset($propertyIDs);
        unset($workLogIDs);
    }
}
