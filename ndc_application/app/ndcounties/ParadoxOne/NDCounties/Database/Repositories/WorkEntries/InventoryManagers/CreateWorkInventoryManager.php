<?php namespace ParadoxOne\NDCounties\Database\Repositories\WorkEntries\InventoryManagers;

use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

class CreateWorkInventoryManager extends AbstractInventoryProcessor
{
    /**
     * Processes the inventory adjustment (for record updates).
     *
     * @return mixed
     */
    public function processUpdates()
    {
        if (!$this->allInventorySettings->auto_deduct_on_record_create) {
            return true;
        }

        $previousAdjustments = $this->getTable('inventory_adjustments')->where('adjustment_context', '=', 2)
                                    ->where('adjustment_value', '=', $this->workEntryID)->get();

        // If there is nothing to process, we are just going to bail here.
        if (count($previousAdjustments) == 0 && count($this->getConsumableItems()) == 0) {
            return true;
        }

        // Holds the adjustments that need to be made.
        $modifiedAdjustments = [];


        try {
            foreach (array_keys($this->getConsumableItems()) as $consumableID) {
                $existingAdjustmentRecord = array_pluck_where($previousAdjustments, 'consumable_id', $consumableID);

                if (count($existingAdjustmentRecord) > 0) {
                    // This check will help us avoid unnecessary updates.
                    if ($existingAdjustmentRecord[0]->adjustment_amount !== $this->getConsumableItems()[$consumableID]) {
                        // This will create a new array. The values are the adjustment ID and the second value is the amount.
                        $modifiedAdjustments[$consumableID] =
                            [$existingAdjustmentRecord[0]->id, $this->getConsumableItems()[$consumableID]];
                    }

                    // Now we need to remove any adjustments from the internal array.
                    unset($this->consumableManagementArray[$consumableID]);
                }
            }
        } catch (\Exception $e) {
        }


        $this->startTransaction();

        $doCommit = true;

        try {
            // Here we will process the modified adjustments.
            if (count($modifiedAdjustments) > 0) {
                foreach ($modifiedAdjustments as $consumableID => $adjustment) {
                    $this->getTable('inventory_adjustments')->where('id', '=', $adjustment[0])
                         ->update(['adjustment_amount' => $adjustment[1]]);

                    // Refresh the consumable.
                    $this->inventoryAdjustmentRepository->refreshInventoryLevel($consumableID);
                }
            }
        } catch (\Exception $e) {
            $doCommit = false;
            $this->rollbackTransaction();
        }

        if ($doCommit) {
            $this->commitTransaction();
        }


        // Let's call the `process()` method to process any inserts we need to do.
        if (count($this->consumableManagementArray) > 0) {
            $this->process();
        }
    }

    /**
     * Processes the inventory adjustment.
     *
     * @return mixed
     */
    public function process()
    {
        // If we are not supposed to automatically manage the inventory status, we should just bail.
        if (!$this->allInventorySettings->auto_deduct_on_record_create) {
            return true;
        }

        if (count($this->consumableManagementArray) > 0) {
            $this->startTransaction();

            try {
                $batchedInserts = [];

                foreach ($this->getConsumableItems() as $consumableID => $batchAmount) {
                    $batchedInserts[] = [
                        'date_adjusted'      => Carbon::now()->toDateTimeString(),
                        'adjusted_by'        => Auth::user()->id,
                        'consumable_id'      => $consumableID,
                        'adjustment_context' => self::CONTEXT_WORK_ADJUSTED,
                        'adjustment_value'   => $this->workEntryID,
                        'adjustment_amount'  => $batchAmount
                    ];
                }

                // Process the batched inventory adjustments.
                $this->getTable('inventory_adjustments')->insert($batchedInserts);

                foreach (array_keys($this->consumableManagementArray) as $consumableID) {
                    $this->inventoryAdjustmentRepository->refreshInventoryLevel($consumableID);
                }

                $this->commitTransaction();

                return true;
            } catch (\Exception $e) {
            }

            $this->rollbackTransaction();

            return false;
        }

        // There were no consumables in the management array, just return true.
        return true;
    }
}
