<?php namespace ParadoxOne\NDCounties\Database\Repositories\WorkEntries\Builders;

trait TotalResultsCalculatorTrait
{
    protected $totalResults = null;

    public function getCount()
    {
        if ($this->totalResults !== null) {
            return $this->totalResults;
        }

        return count($this->constructedWorkRecords);
    }
}
