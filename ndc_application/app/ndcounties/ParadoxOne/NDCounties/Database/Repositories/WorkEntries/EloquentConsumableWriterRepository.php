<?php namespace ParadoxOne\NDCounties\Database\Repositories\WorkEntries;

use Exception;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\MessageBag;
use ParadoxOne\NDCounties\Contracts\Resolvers\ConsumableTypeResolverInterface;
use ParadoxOne\NDCounties\Contracts\WorkEntries\ConsumableRecordInterface;
use ParadoxOne\NDCounties\Contracts\WorkEntries\ConsumableWriterRepositoryInterface;
use Service\WorkEntries\ConsumableRecord;

class EloquentConsumableWriterRepository implements ConsumableWriterRepositoryInterface
{
    protected $consumableRecord = null;

    protected $consumableResolver = null;

    private $success = false;

    protected $employeeTaxPercentage = 0;

    protected $equipmentTaxPercentage = 0;

    protected $materialTaxPercentage = 0;

    protected $fuelTaxPercentage = 0;

    private $validationErrors = null;

    public function __construct(ConsumableTypeResolverInterface $consumableResolver)
    {
        $this->consumableResolver = $consumableResolver;
    }

    /**
     * Sets the tax percentages that should be used when creating the consumable record.
     *
     * @param $employeeTaxPercentage
     * @param $equipmentTaxPercentage
     * @param $materialTaxPercentage
     * @param $fuelTaxPercentage
     * @return mixed
     */
    public function setHistoricTaxRatePercentagesFromInvoice($employeeTaxPercentage, $equipmentTaxPercentage, $materialTaxPercentage, $fuelTaxPercentage)
    {
        $this->employeeTaxPercentage  = $employeeTaxPercentage;
        $this->equipmentTaxPercentage = $equipmentTaxPercentage;
        $this->materialTaxPercentage  = $materialTaxPercentage;
        $this->fuelTaxPercentage      = $fuelTaxPercentage;
    }


    public function setConsumable(ConsumableRecordInterface $record)
    {
        $this->consumableRecord = $record;
    }

    /**
     * Performs the actual update on a consumable record.
     *
     * @param $validateOnly
     * @return bool
     */
    private function doUpdateRecord($validateOnly)
    {
        try {
            if (!$validateOnly) {
                DB::connection(\Tenant::getCurrentConnection())->beginTransaction();
            }

            $bindingResolver =
                App::make('\ParadoxOne\NDCounties\Database\Repositories\WorkEntries\BindingConsumableResolver');

            $consumable                      = ConsumableRecord::findOrFail($this->consumableRecord->getRecordID());
            $consumable->consumable_type     = $this->consumableRecord->getConsumableType();
            $consumable->consumable_id       = $this->consumableRecord->getConsumableID();
            $consumable->associated_type     = $this->consumableRecord->getAssociatedType();
            $consumable->associated_id       = $this->consumableRecord->getAssociatedID();
            $consumable->work_entry_id       = $this->consumableRecord->getWorkEntryID();
            $consumable->measurement_unit_id = $this->consumableRecord->getMeasurementUnit();
            $consumable->total_cost          = $this->consumableRecord->getTotalCost();
            $consumable->total_quantity      = $this->consumableRecord->getQuantity();
            $consumable->historic_cost       = $this->consumableRecord->getHistoricCost();

            $totalTax = 0;

            if ($this->consumableRecord->isTaxed() == true) {
                $consumable->taxable = 1;

                // Apply some tax stuff.
                switch ($this->consumableRecord->getConsumableType()) {
                    case 2:
                        $totalTax = $this->equipmentTaxPercentage * $this->consumableRecord->getTotalCost();
                        break;
                    case 3:
                        $totalTax = $this->fuelTaxPercentage * $this->consumableRecord->getTotalCost();
                        break;
                    case 4:
                        $totalTax = $this->materialTaxPercentage * $this->consumableRecord->getTotalCost();
                        break;
                }
            } else {
                $consumable->taxable = 0;
                $totalTax = 0;
            }

            $consumable->total_tax = $totalTax;

            /* // We do not need this code when we are simply updating the record. The code that inserts the record
             * // already has set the bound record ID.
             *
             * if ($this->consumableRecord->getConsumableType() == 3)
             * {
             *    $consumable->bound_consumable_record = $bindingResolver->getBinding(2, $this->consumableRecord->getBindingRecord());
             * }
             */

            $success          = false;
            $handlerSucceeded = true;

            if ($validateOnly) {
                $success = $consumable->validate();
            } else {
                $success = $consumable->updateUniques();
                $bindingResolver->setBinding($this->consumableRecord->getConsumableType(),
                                             $this->consumableRecord->getBindingRecord(), $consumable->id);
            }

            if (!$success) {
                $this->validationErrors = $consumable->errors();
            }

            if ($this->consumableResolver->resolvePropertyHandler($this->consumableRecord->getAssociatedType()) !==
                false
            ) {
                // If there was a handler found, get an instance and run it's `handle` method.
                $handler =
                    $this->consumableResolver->makePropertyHandlerInstance($this->consumableRecord->getAssociatedType());

                $handlerSuccess = $handler->handle($this->consumableRecord, $validateOnly);

                if (!$handlerSuccess) {
                    if ($this->validationErrors == null) {
                        $this->validationErrors = new MessageBag;
                    }

                    $this->validationErrors = $this->validationErrors->merge($this->handlerSuccess->errors->all());
                }
            }

            if (!$validateOnly) {
                if ($handlerSucceeded && $success) {
                    DB::connection(\Tenant::getCurrentConnection())->commit();

                    return true;
                } else {
                    DB::connection(\Tenant::getCurrentConnection())->rollback();

                    return false;
                }
            }
        } catch (Exception $updateException) {
            $this->success = false;
        }

        return $this->success;
    }

    /**
     * Updates or creates a consumable record.
     *
     * @param bool $validateOnly
     * @return mixed
     */
    public function updateOrCreate($validateOnly = false)
    {
        if ($this->consumableRecord->getRecordID() == "") {
            // If the record ID is set to an empty string, we must proceed as if we were
            // inserting the consumable record. For this, we will just return the results of the `commit`
            // method.
            return $this->commit($validateOnly);
        }

        // If we made it this far, we are in the update mode.
        return $this->doUpdateRecord($validateOnly);
    }


    public function commit($validateOnly = false)
    {
        try {
            if (!$validateOnly) {
                DB::connection(\Tenant::getCurrentConnection())->beginTransaction();
            }

            $bindingResolver =
                App::make('\ParadoxOne\NDCounties\Database\Repositories\WorkEntries\BindingConsumableResolver');


            $consumable                      = new ConsumableRecord;
            $consumable->consumable_type     = $this->consumableRecord->getConsumableType();
            $consumable->consumable_id       = $this->consumableRecord->getConsumableID();
            $consumable->associated_type     = $this->consumableRecord->getAssociatedType();
            $consumable->associated_id       = $this->consumableRecord->getAssociatedID();
            $consumable->work_entry_id       = $this->consumableRecord->getWorkEntryID();
            $consumable->measurement_unit_id = $this->consumableRecord->getMeasurementUnit();
            $consumable->total_cost          = $this->consumableRecord->getTotalCost();
            $consumable->total_quantity      = $this->consumableRecord->getQuantity();
            $consumable->historic_cost       = $this->consumableRecord->getHistoricCost();

            if ($this->consumableRecord->isTaxed() === true) {
                $consumable->taxable = true;

                // Apply some tax stuff.
                switch ($this->consumableRecord->getConsumableType()) {
                    case 2:
                        $consumable->total_tax =
                            $this->equipmentTaxPercentage * $this->consumableRecord->getTotalCost();
                        break;
                    case 3:
                        $consumable->total_tax = $this->fuelTaxPercentage * $this->consumableRecord->getTotalCost();
                        break;
                    case 4:
                        $consumable->total_tax = $this->materialTaxPercentage * $this->consumableRecord->getTotalCost();
                        break;
                }
            } else {
                $consumable->taxable = false;
            }

            if ($this->consumableRecord->getConsumableType() == 3) {
                $consumable->bound_consumable_record =
                    $bindingResolver->getBinding(2, $this->consumableRecord->getBindingRecord());
            }

            $success = false;
            // Set the handler succeeded to true by default, since there may not be a consumable
            // property handler.
            $handlerSucceeded = true;


            // If the writer repository has only been instructed to validate, it is most likely that the
            // work writer repository is attempting to gather all of the validation errors for the current
            // user session before attempting to write the record.
            if ($validateOnly) {
                $success = $consumable->validate();
            } else {
                $success = $consumable->save();
                $bindingResolver->setBinding($this->consumableRecord->getConsumableType(),
                                             $this->consumableRecord->getBindingRecord(), $consumable->id);
            }

            // If there was a problem with saving/validating the consumable record, set the validation
            // errors property of the writer repository.
            if (!$success) {
                $this->validationErrors = $consumable->errors();
            }

            // This code attempts to load a handler for the current consumable type. The handlers are like plugins that
            // handle additional data for the consumable type.
            if ($this->consumableResolver->resolvePropertyHandler($this->consumableRecord->getAssociatedType()) !==
                false
            ) {

                // If there was a handler found, get an instance and run its `handle` method.
                $handler =
                    $this->consumableResolver->makePropertyHandlerInstance($this->consumableRecord->getAssociatedType());

                $this->consumableRecord->learned_id = $consumable->id;
                // We are also going to pass in the `$validateOnly` value to the handler, which will indicate whether or not
                // the handler should actually affect the persistent storage.
                $handlerSuccess = $handler->handle($this->consumableRecord, $validateOnly);
                if (!$handlerSuccess) {
                    if ($this->validationErrors == null) {
                        $this->validationErrors = new MessageBag;
                    }

                    // If the handler could not succeed without errors, we are just going to merge the errors from the
                    // property handler into the error messages of the consumable resolver. This will allow any calling
                    // code to be notified of any errors throughout the operating stack.
                    $this->validationErrors = $this->validationErrors->merge($this->handlerSuccess->errors()->all());
                }
            }

            if (!$validateOnly) {
                if ($handlerSucceeded && $success) {
                    DB::connection(\Tenant::getCurrentConnection())->commit();

                    return true;
                } else {
                    DB::connection(\Tenant::getCurrentConnection())->rollback();

                    return false;
                }
            }
        } catch (Exception $e) {
            $this->success = false;
        }

        return $this->success;
    }
}
