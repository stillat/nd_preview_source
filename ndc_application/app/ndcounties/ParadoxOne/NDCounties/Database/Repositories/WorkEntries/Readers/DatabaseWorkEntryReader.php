<?php namespace ParadoxOne\NDCounties\Database\Repositories\WorkEntries\Readers;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use ParadoxOne\NDCounties\Contracts\WorkEntries\ParadoxOne;
use ParadoxOne\NDCounties\Contracts\WorkEntries\WorkEntryReaderInterface;
use ParadoxOne\NDCounties\Contracts\WorkEntries\WorkEntryReaderRepositoryInterface;
use ParadoxOne\NDCounties\Database\Entities\WorkEntry;
use ParadoxOne\NDCounties\Database\Repositories\WorkEntries\WorkEntryNotFoundException;

class DatabaseWorkEntryReader implements WorkEntryReaderInterface
{
    /**
     * The current pagination page.
     *
     * @var int
     */
    private $paginationCurrentPage = 0;

    /**
     * The number of results per page.
     *
     * @var int
     */
    private $paginationResultsPerPage = 10;

    /**
     * The work reader implementation.
     *
     * @var ParadoxOne\NDCounties\Contracts\WorkEntries\WorkEntryReaderRepositoryInterface
     */
    private $workReaderRepository = null;

    public function __construct(WorkEntryReaderRepositoryInterface $workReader)
    {
        $this->workReaderRepository = $workReader;
    }

    public function independentReader()
    {
        $this->workReaderRepository = clone($this->workReaderRepository);
    }

    /**
     * Gets the reader used internally.
     *
     * @return ParadoxOne\NDCounties\Contracts\WorkEntries\WorkEntryReaderRepositoryInterface
     */
    public function &reader()
    {
        return $this->workReaderRepository;
    }


    /**
     * Sets the pagination details.
     *
     * @param int $perPage
     * @param int $currentPage
     * @return mixed
     */
    public function setPagination($perPage, $currentPage)
    {
        $this->paginationResultsPerPage = $perPage;
        $this->paginationCurrentPage    = $currentPage;

        $this->workReaderRepository->paginate($currentPage, $perPage);

        return $this;
    }

    /**
     * Returns the collection of records.
     *
     * @return mixed
     */
    public function getRecords()
    {
        return $this->workReaderRepository->getRecords();
    }

    /**
     * Returns the total count of records.
     *
     * @return mixed
     */
    public function count()
    {
        return $this->workReaderRepository->getCount();
    }

    /**
     * Returns a single record.
     *
     * @param $id
     * @return mixed
     * @throws ModelNotFoundException
     */
    public function getSingleRecord($id)
    {
        $record = $this->workReaderRepository->withDefaults()->where('id', '=', $id)->getRecords();

        if (count($record) == 0) {
            throw new WorkEntryNotFoundException;
        }

        $record = $record[0];

        // Some queries to get the next/previous record.
        $neighbors        = $this->reader()->getNextAndPreviousForRecord($id);
        $record->previous = $neighbors[0];
        $record->next     = $neighbors[1];

        return $record;
    }

    /**
     * Returns a measurement unit from a central store.
     *
     * @param $unitID
     * @return mixed
     */
    public function getMeasurementUnit($unitID)
    {
        return $this->reader()->getMeasurementUnitFromCache($unitID);
    }

    public function getLastSavedRecordID()
    {
        $query = clone $this->reader()->getQuery();
        $record = $query->select('id')->max('id');
        return $record;
    }

}
