<?php namespace ParadoxOne\NDCounties\Database\Repositories\WorkEntries;

use Illuminate\Database\Eloquent\ModelNotFoundException;

class WorkEntryNotFoundException extends ModelNotFoundException
{
}
