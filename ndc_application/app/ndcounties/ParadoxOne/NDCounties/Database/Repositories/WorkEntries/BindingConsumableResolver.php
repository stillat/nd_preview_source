<?php namespace ParadoxOne\NDCounties\Database\Repositories\WorkEntries;

class BindingConsumableResolver
{
    /**
     * The available consumable bindings.
     *
     * @var array
     */
    protected $consumableResolverRecord = [];

    /**
     * Sets a binding for the consumable records.
     *
     * @param $associatedType
     * @param $identifier
     * @param $consumableRecord
     */
    public function setBinding($associatedType, $identifier, $consumableRecord)
    {
        $this->consumableResolverRecord[$associatedType][$identifier] = $consumableRecord;
    }

    /**
     * Returns a consumable binding.
     *
     * @param $associatedType
     * @param $identifier
     * @return int
     */
    public function getBinding($associatedType, $identifier)
    {
        if (isset($this->consumableResolverRecord[$associatedType]) and
            isset($this->consumableResolverRecord[$associatedType][$identifier])
        ) {
            return $this->consumableResolverRecord[$associatedType][$identifier];
        }

        return 0;
    }
}
