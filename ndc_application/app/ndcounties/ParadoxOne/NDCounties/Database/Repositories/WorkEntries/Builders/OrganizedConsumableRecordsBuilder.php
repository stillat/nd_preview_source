<?php namespace ParadoxOne\NDCounties\Database\Repositories\WorkEntries\Builders;

trait OrganizedConsumableRecordsBuilder
{
    private function cloneObjectAttributes(&$obj1, &$obj2)
    {
        foreach (get_object_vars($obj2) as $k => $v) {
            $obj1->{$k} = $v;
        }
    }

    protected $organizedRecords = [];

    public function getOrganizedConsumableRecords()
    {
        $this->getRecords();
        if ($this->consumableOverride == true) {
            // Let's handle the equipment units.
            if ($this->internalMode == 3) {
                foreach ($this->consumableFuelRecords as $recordID => $actualRecord) {

                    if ($actualRecord->consumable_id == 0) {
                        $actualRecord->consumable_id = 1;
                    }

                    $workRecord                         =
                        array_pluck_where($this->workEntryRecords, 'id', $actualRecord->work_entry_id);
                    $fuelRecord                         =
                        array_pluck_where($this->fuelRecords, 'id', $actualRecord->consumable_id);
                    $actualRecord->consumable_record_id = $actualRecord->id;
                    $this->cloneObjectAttributes($actualRecord, $workRecord[0]);

                        $actualRecord->id         = $actualRecord->consumable_record_id;
                        $actualRecord->fuel       = $fuelRecord[0];
                        $this->organizedRecords[] = $actualRecord;
                }

                return $this->organizedRecords;
            }
        }


        foreach ($this->consumableRecords as $recordID => $actualRecord) {
            $workRecord                         =
                array_pluck_where($this->workEntryRecords, 'id', $actualRecord->work_entry_id);
            $actualRecord->consumable_record_id = $actualRecord->id;
            $this->cloneObjectAttributes($actualRecord, $workRecord[0]);

            $actualRecord->id         = $actualRecord->consumable_record_id;
            $this->organizedRecords[] = $actualRecord;
        }

        return $this->organizedRecords;
    }
}
