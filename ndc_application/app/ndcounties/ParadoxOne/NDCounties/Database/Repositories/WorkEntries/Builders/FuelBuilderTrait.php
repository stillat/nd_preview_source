<?php namespace ParadoxOne\NDCounties\Database\Repositories\WorkEntries\Builders;

use Illuminate\Database\Connection;

trait FuelBuilderTrait
{
    /**
     * The actual fuel records.
     *
     * @var array
     */
    protected $fuelRecords = [];

    /**
     * Fills the fuel records.
     *
     * @param Connection $connection
     * @param            $fuelRecords
     * @return array
     */
    private function fillFuelRecords(Connection &$connection, &$fuelRecords)
    {
        if (count($fuelRecords) == 0) {
            $fuelRecords[] = 1;
        }

        if (!in_array(1, $fuelRecords)) {
            $fuelRecords[] = 1;
        }

        if (count($fuelRecords) == 1 and $fuelRecords[0] == 1) {
            $fuel              = new \stdClass;
            $fuel->id          = 1;
            $fuel->code        = 'NA';
            $fuel->name        = 'NA';
            $fuel->description = 'NA';
            $fuel->unit_id     = 9;
            $fuel->cost        = 0;

            $this->fuelRecords[] = $fuel;
        } else {
            if (count($fuelRecords) > 0) {
                $this->fuelRecords = $connection->table('consumables')->where('consumable_context', '=', 1)->whereIn('id', $fuelRecords)->get();
            }
        }

        $discoveredMeasurementUnits = [];

        foreach ($this->fuelRecords as $fRecord) {
            $discoveredMeasurementUnits[] = $fRecord->unit_id;
        }

        // Memory clean up.
        unset($connection);
        unset($fuelRecords);

        return $discoveredMeasurementUnits;
    }
}
