<?php namespace ParadoxOne\NDCounties\Database\Repositories\WorkEntries;

use Carbon\Carbon;
use Illuminate\Support\Facades\App;
use ParadoxOne\NDCounties\Contracts\Resolvers\ConsumableTypeResolverInterface;
use ParadoxOne\NDCounties\Contracts\WorkEntries\ConsumableRecordInterface;
use ParadoxOne\NDCounties\Contracts\WorkEntries\ConsumableWriterRepositoryInterface;
use ParadoxOne\NDCounties\Contracts\WorkEntries\WorkEntryWriterRepositoryInterface;

abstract class WorkEntryWriterRepository implements WorkEntryWriterRepositoryInterface
{
    use \ParadoxOne\NDCounties\Traits\MathExecutableTrait;

    /**
     * The employee ID
     *
     * @var integer
     */
    protected $employeeID = 0;

    /**
     * The activity ID
     *
     * @var integer
     */
    protected $activityID = 0;

    /**
     * The project ID
     *
     * @var integer
     */
    protected $projectID = 0;

    /**
     * The department ID
     *
     * @var integer
     */
    protected $departmentID = 0;

    /**
     * The district ID
     *
     * @var integer
     */
    protected $districtID = 0;

    /**
     * The property ID
     *
     * @var integer
     */
    protected $propertyID = 0;

    /**
     * The property context ID
     *
     * @var int
     */
    protected $propertyContext = 0;

    protected $organizationPropertyID = 0;

    protected $organizationPropertyContext = 0;

    /**
     * The activity description
     *
     * @var string
     */
    protected $activityDescription = "";

    /**
     * The regular hours worked.
     *
     * @var float
     */
    protected $regularHoursWorked = 0.0;

    /**
     * The overtime hours worked
     *
     * @var float
     */
    protected $overtimeHoursWorked = 0.0;

    /**
     * Historic regular hourly wage
     *
     * @var float
     */
    protected $historicRegularWage = 0.0;

    /**
     * Historic overtime wage
     *
     * @var float
     */
    protected $historicOvertimeWage = 0.0;

    /**
     * Historic hourly fringe benefits
     *
     * @var float
     */
    protected $historicRegularFringeBenefits = 0.0;

    /**
     * Historic overtime fringe benefits
     *
     * @var float
     */
    protected $historicOvertimeFringeBenefits = 0.0;

    /**
     * An array of ConsumableRecordInterface
     *
     * @var array
     */
    protected $consumableRecords = array();

    /**
     * The date of the work log.
     *
     * @var string
     */
    protected $workLogDate = "";

    /**
     * The ConsumableTypeResolverInterface implementation
     *
     * @var ConsumableTypeResolverInterface
     */
    protected $consumableResolver = null;

    /**
     * The ConsumableWriterRepositoryInterface implementation
     *
     * @var ParadoxOne\NDCounties\Contracts\WorkEntries\ConsumableWriterRepositoryInterface
     */
    protected $consumableWriterRepository = null;

    /**
     * The misc billing quantity.
     *
     * @var integer
     */
    protected $miscBillingQuantity = 0;

    /**
     * The misc billing measurement unit.
     *
     * @var integer
     */
    protected $miscBillingMeasurementUnit = 0;

    /**
     * The misc billing rate.
     *
     * @var integer
     */
    protected $miscBillingRate = 0;

    /**
     * The misc billing description.
     *
     * @var string
     */
    protected $miscBillingDescription = '';

    /**
     * @var bool
     */
    protected $excludedFromReports = false;

    /**
     * Indicates whether or not the employee is taxed.
     *
     * @var bool
     */
    protected $employeeIsTaxed = false;

    /**
     * The property total.
     *
     * @var decimal
     */
    protected $propertyTotal = 0;

    /**
     * The invoice override total.
     *
     * @var int
     */
    protected $invoiceOverrideTotal = 0;

    /**
     * {@inheritdoc}
     *
     */
    public function __construct(ConsumableTypeResolverInterface $resolver,
                                ConsumableWriterRepositoryInterface $consumableWriter)
    {
        $this->consumableResolver         = $resolver;
        $this->consumableWriterRepository = $consumableWriter;

        // Set the math engine.
        $this->setMathEngine(null);
    }

    private function safetyCheckInputValue($checkValue, $defaultValue = 0)
    {
        if (is_string($checkValue) and strlen($checkValue) == 0) {
            return $defaultValue;
        }

        return $checkValue;
    }

    /**
     * Gets the property total.
     *
     * @return mixed
     */
    public function getPropertyTotal()
    {
        return $this->propertyTotal;
    }

    /**
     * Sets the property total for the work record.
     *
     * @param $propertyTotal
     * @return mixed
     */
    public function setPropertyTotal($propertyTotal)
    {
        $this->propertyTotal = $propertyTotal;
    }


    /**
     * Sets whether a record should be excluded from reports.
     *
     * @param $excluded
     * @return mixed
     */
    public function setExcludedFromReports($excluded)
    {
        $this->excludedFromReports = !$excluded;
    }

    /**
     * Gets whether or not a record should be excluded from reports.
     *
     * @return mixed
     */
    public function getExcludedFromReports()
    {
        return $this->excludedFromReports;
    }

    /**
     * Sets whether or not the employee is taxed.
     *
     * @param $taxed
     * @return mixed
     */
    public function setEmployeeIsTaxed($taxed)
    {
        $this->employeeIsTaxed = $taxed;
    }

    /**
     * Indicates whether or not the employee is taxed.
     *
     * @return mixed]
     */
    public function getEmployeeIsTaxed()
    {
        return $this->employeeIsTaxed;
    }

    /**
     * {@inheritdoc}
     */
    public function setEmployee($employeeID)
    {
        $this->employeeID = $this->safetyCheckInputValue($employeeID);
    }

    /**
     * {@inheritdoc}
     */
    public function getEmployee()
    {
        return $this->employeeID;
    }

    /**
     * {@inheritdoc}
     */
    public function setHoursWorked($hoursWorked, $overtimeHours)
    {
        $this->regularHoursWorked = $hoursWorked;

        $this->overtimeHoursWorked = $overtimeHours;
    }

    /**
     * {@inheritdoc}
     */
    public function getRegularHoursWorked()
    {
        return $this->regularHoursWorked;
    }

    /**
     * {@inheritdoc}
     */
    public function getOvertimeHoursWorked()
    {
        return $this->overtimeHoursWorked;
    }

    /**
     * {@inheritdoc}
     */
    public function setHistoricWageData($regularWage, $overtimeWage)
    {
        $this->historicRegularWage = $regularWage;

        $this->historicOvertimeWage = $overtimeWage;
    }

    /**
     * {@inheritdoc}
     */
    public function getHistoricRegularWage()
    {
        return $this->historicRegularWage;
    }

    /**
     * {@inheritdoc}
     */
    public function getHistoricOvertimeWage()
    {
        return $this->historicOvertimeWage;
    }

    /**
     * {@inheritdoc}
     */
    public function setHistoricFringeBenefits($regularFringe, $overtimeFringe)
    {
        $this->historicRegularFringeBenefits = $regularFringe;

        $this->historicOvertimeFringeBenefits = $overtimeFringe;
    }

    /**
     * {@inheritdoc}
     */
    public function getHistoricRegularFringeBenefits()
    {
        return $this->historicRegularFringeBenefits;
    }

    /**
     * {@inheritdoc}
     */
    public function getHistoricOvertimeFringeBenefits()
    {
        return $this->historicOvertimeFringeBenefits;
    }

    /**
     * {@inheritdoc}
     */
    public function setConsumableWriter(ConsumableWriterRepositoryInterface $consumableWriter)
    {
        $this->consumableWriterRepository = $consumableWriter;
    }

    /**
     * {@inheritdoc}
     */
    public function setActivity($activityID)
    {
        $this->activityID = $this->safetyCheckInputValue($activityID);
    }

    /**
     * {@inheritdoc}
     */
    public function getActivity()
    {
        return $this->activityID;
    }

    /**
     * {@inheritdoc}
     */
    public function setProject($projectID)
    {
        $this->projectID = $this->safetyCheckInputValue($projectID);
    }

    /**
     * {@inheritdoc}
     */
    public function getProject()
    {
        return $this->projectID;
    }

    /**
     * {@inheritdoc}
     */
    public function setDepartment($departmentID)
    {
        $this->departmentID = $this->safetyCheckInputValue($departmentID);
    }

    /**
     * {@inheritdoc}
     */
    public function getDepartment()
    {
        return $this->departmentID;
    }

    /**
     * {@inheritdoc}
     */
    public function setDistrict($districtID)
    {
        $this->districtID = $this->safetyCheckInputValue($districtID);
    }

    /**
     * {@inheritdoc}
     */
    public function getDistrict()
    {
        return $this->districtID;
    }

    public function setOrganizationProperty($propertyID)
    {
        $this->organizationPropertyID = $propertyID;
    }

    public function getOrganizationProperty()
    {
        return $this->organizationPropertyID;
    }

    public function setOrganizationPropertyContext($propertyContext)
    {
        $this->organizationPropertyContext = $propertyContext;
    }

    public function getOrganizationPropertyContext()
    {
        return $this->organizationPropertyContext;
    }


    /**
     * {@inheritdoc}
     */
    public function setProperty($propertyID)
    {
        $this->propertyID = $this->safetyCheckInputValue($propertyID);
    }

    /**
     * {@inheritdoc}
     */
    public function getProperty()
    {
        return $this->propertyID;
    }

    /**
     * Sets the property context value.
     *
     * @param  int $propertyContext
     * @return void
     */
    public function setPropertyContext($propertyContext)
    {
        if (is_string($propertyContext) and strlen($propertyContext) == 0) {
            $this->propertyContext = 0;
        } else {
            if ($propertyContext > 1) {
                $this->propertyContext = 0;
            }
        }

        $this->propertyContext = $propertyContext;
    }

    /**
     * Gets the property context value.
     *
     * @return int
     */
    public function getPropertyContext()
    {
        return $this->propertyContext;
    }


    /**
     * {@inheritdoc}
     */
    public function setActivityDescription($description)
    {
        $this->activityDescription = $description;
    }

    /**
     * {@inheritdoc}
     */
    public function getActivityDescription()
    {
        return $this->activityDescription;
    }

    /**
     * {@inheritdoc}
     */
    public function attachConsumable(ConsumableRecordInterface $consumable)
    {
        $this->consumableRecords[] = $consumable;
    }

    /**
     * {@inheritdoc}
     */
    public function getConsumables()
    {
        return $this->consumableRecords;
    }

    /**
     * {@inheritdoc}
     */
    public function setMiscBillingQuantity($billingQuantity)
    {
        $this->miscBillingQuantity = $billingQuantity;
    }

    /**
     * {@inheritdoc}
     */
    public function getMiscBillingQuantity()
    {
        return $this->miscBillingQuantity;
    }

    /**
     * {@inheritdoc}
     */
    public function setMiscBillingUnit($unitID)
    {
        $this->miscBillingMeasurementUnit = $this->safetyCheckInputValue($unitID, 9);
    }

    /**
     * {@inheritdoc}
     */
    public function getMiscBillingUnit()
    {
        return $this->miscBillingMeasurementUnit;
    }

    /**
     * {@inheritdoc}
     */
    public function setMiscBillingRate($rate)
    {
        $this->miscBillingRate = $rate;
    }

    /**
     * {@inheritdoc}
     */
    public function getMiscBillingRate()
    {
        return $this->miscBillingRate;
    }

    /**
     * {@inheritdoc}
     */
    public function setMiscBillingDescription($description)
    {
        $this->miscBillingDescription = $description;
    }

    /**
     * {@inheritdoc}
     */
    public function getMiscBillingDescription()
    {
        return $this->miscBillingDescription;
    }

    /**
     * {@inheritdoc}
     */
    public function setWorkDate($date)
    {
        // Use Carbon to do some heavy lifting to
        // convert the date to a MySQL format.
        $this->workLogDate = (new Carbon($date))->toDateTimeString();
    }

    /**
     * {@inheritdoc}
     */
    public function getWorkDate()
    {
        return $this->workLogDate;
    }

    /**
     * Holds the total material quantity
     *
     * @var decimal
     */
    private $totalMaterialQuantity = 0;

    /**
     * Holds the total equipment units quantity
     *
     * @var decimal
     */
    private $totalEquipmentUnitsQuantity = 0;

    /**
     * Holds the total fuels quantity
     *
     * @var decimal
     */
    private $totalFuelsQuantity = 0;

    /**
     * Holds the total regular hour cost
     *
     * @var decimal
     */
    private $employeeRegularHourCost = 0;

    /**
     * Holds the total overtime hour cost
     *
     * @var decimal
     */
    private $employeeOvertimeHourCost = 0;

    /**
     * Holds the total material cost
     *
     * @var decimal
     */
    private $totalMaterialCost = 0;

    /**
     * Holds the total equipment unit cost
     *
     * @var decimal
     */
    private $totalEquipmentUnitsCost = 0;

    /**
     * Holds the total fuels cost
     *
     * @var decimal
     */
    private $totalFuelsCost = 0;

    protected $isCreditInventoryMode = false;

    /**
     * Calculates the total miscellaneous billing total cost.
     *
     * @return decimal The miscellaneous billing cost.
     */
    public function calculateTotalMiscBillingTotalCost()
    {
        $engine = $this->getMathEngine();

        $billingTotal = $engine->mul($this->miscBillingRate, $this->miscBillingQuantity);

        unset($engine);

        return $billingTotal;
    }

    /**
     * Calculates the total material quantities.
     *
     * @return decimal Total material quantities
     */
    public function calculateTotalMaterialsQuantity()
    {
        $engine = $this->getMathEngine();

        foreach ($this->consumableRecords as $record) {
            if ($this->consumableResolver->isMaterial($record->getConsumableType())) {
                $this->totalMaterialQuantity = $engine->add($this->totalMaterialQuantity, $record->getQuantity());
            }
        }

        unset($engine);

        return $this->totalMaterialQuantity;
    }

    /**
     * Calculates the total equipment units hours miles
     *
     * @return int Equipment unit hours miles
     */
    public function calculateTotalEquipmentUnitsQuantity()
    {
        $engine = $this->getMathEngine();

        foreach ($this->consumableRecords as $record) {
            if ($this->consumableResolver->isEquipmentUnit($record->getConsumableType())) {
                $this->totalEquipmentUnitsQuantity =
                    $engine->add($this->totalEquipmentUnitsQuantity, $record->getQuantity());
            }
        }


        unset($engine);

        return $this->totalEquipmentUnitsQuantity;
    }

    /**
     * Calculate total fuel quantities
     *
     * @return decimal Total fuel quantities
     */
    public function caclulateTotalFuelsQuantity()
    {
        $engine = $this->getMathEngine();

        foreach ($this->consumableRecords as $record) {
            if ($this->consumableResolver->isFuel($record->getConsumableType())) {
                $this->totalFuelsQuantity = $engine->add($this->totalFuelsQuantity, $record->getQuantity());
            }
        }

        unset($engine);

        return $this->totalFuelsQuantity;
    }

    /**
     * Calculate regular hourly cost
     *
     * Calculated with historic_wage_regular * employee_hours_regular + CALC of fringe
     *
     * @return string The cost for the regular hours worked
     */
    public function calculateRegularHoursCost()
    {
        $engine = $this->getMathEngine();

        $regularHourlyCost     = $engine->mul($this->historicRegularWage, $this->regularHoursWorked);
        $regularHourlyBenefits = $engine->mul($this->historicRegularFringeBenefits, $this->regularHoursWorked);
        $regularHourlyTotal    = $engine->add($regularHourlyCost, $regularHourlyBenefits);

        // Clean up
        unset($regularHourlyBenefits);
        unset($regularHourlyCost);
        unset($engine);

        return $regularHourlyTotal;
    }

    /**
     * Calculate overtime hourly cost
     *
     * Calculate with historic_wage_overtime * employee_hours_overtime + CALC of fringe
     *
     * @return string The cost for the overtime hours worked
     */
    public function calculateOvertimeHoursCost()
    {
        $engine = $this->getMathEngine();

        $overtimeHourlyCost     = $engine->mul($this->historicOvertimeWage, $this->overtimeHoursWorked);
        $overtimeHourlyBenefits = $engine->mul($this->historicOvertimeFringeBenefits, $this->overtimeHoursWorked);
        $overtimeHourlyTotal    = $engine->add($overtimeHourlyCost, $overtimeHourlyBenefits);

        // Clean up
        unset($overtimeHourlyBenefits);
        unset($overtimeHourlyCost);
        unset($engine);

        return $overtimeHourlyTotal;
    }

    /**
     * Sets the invoice override total.
     *
     * @param $total
     * @return mixed
     */
    public function setInvoiceOverrideTotal($total)
    {
        $this->invoiceOverrideTotal = $total;
    }

    /**
     * Gets the invoice override total.
     *
     * @return mixed
     */
    public function getInvoiceOverrideTotal()
    {
        return $this->invoiceOverrideTotal;
    }


    /**
     * Returns the sum of overtime cost and regular cost.
     *
     * This function also includes fringe benefits
     *
     * @return string Total employee hour cost.
     */
    public function caclulateCombinedHoursCost()
    {
        $engine = $this->getMathEngine();

        $total = $engine->add($this->calculateRegularHoursCost(), $this->calculateOvertimeHoursCost());

        unset($engine);

        return $total;
    }

    /**
     * Calculates the total cost for all materials
     *
     * @return decimal Total material cost
     */
    public function calculateTotalMaterialCost()
    {
        $engine = $this->getMathEngine();

        foreach ($this->consumableRecords as $record) {
            if ($this->consumableResolver->isMaterial($record->getConsumableType())) {
                $this->totalMaterialCost = $engine->add($this->totalMaterialCost, $record->getTotalCost());
            }
        }

        unset($engine);

        return $this->totalMaterialCost;
    }

    /**
     * Calculates the total cost of the equipment units
     *
     * Calculated by taking the equipment rental rate * the
     * equipment use.
     *
     * @return decimal Cost of the equipment units
     */
    public function calculateTotalEquipmentUnitsCost()
    {
        $engine = $this->getMathEngine();

        foreach ($this->consumableRecords as $record) {
            if ($this->consumableResolver->isEquipmentUnit($record->getConsumableType())) {
                $this->totalEquipmentUnitsCost = $engine->add($this->totalEquipmentUnitsCost, $record->getTotalCost());
            }
        }

        unset($engine);

        return $this->totalEquipmentUnitsCost;
    }

    /**
     * Calculates the total cost of all fuels used
     *
     * @return decimal Cost of the fuels used
     */
    public function calculateTotalFuelsCost()
    {
        $engine = $this->getMathEngine();

        foreach ($this->consumableRecords as $record) {
            if ($this->consumableResolver->isFuel($record->getConsumableType())) {
                $this->totalFuelsCost = $engine->add($this->totalFuelsCost, $record->getTotalCost());
            }
        }

        unset($engine);

        return $this->totalFuelsCost;
    }

    /**
     * Calculates the total equipment unit and fuel costs
     *
     * Provides a SUM of equipment unit costs and fuel costs. This
     * is because fuels are often used in conjunction with equipment
     * units.
     *
     * @return decimal Combined cost of equipment units and fuels.
     */
    public function calculateTotalEquipmentAndFuelsCost()
    {
        $engine = $this->getMathEngine();

        $result = $engine->add($this->calculateTotalFuelsCost(), $this->calculateTotalEquipmentUnitsCost());

        unset($engine);

        return $result;
    }

    public function setIsCreditInventoryMode($mode)
    {
        $this->isCreditInventoryMode = $mode;
    }


    abstract public function commit();
}
