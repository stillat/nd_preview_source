<?php namespace ParadoxOne\NDCounties\Database\Repositories\WorkEntries;

use ParadoxOne\NDCounties\Contracts\Properties\PropertyTypesRepositoryInterface;
use ParadoxOne\NDCounties\Database\Adapters\AdapterManager;
use stdClass;

class WriterHandlerProcessor
{
    protected $manager;

    protected $writer;

    protected $typesRepository;

    protected $adapterRuns = [];

    public function __construct(AdapterManager $manager, PropertyTypesRepositoryInterface $typesRepository)
    {
        $this->manager = $manager;

        $this->typesRepository = $typesRepository;
    }

    /**
     * Sets the writer instance. This is a REFERENCE.
     *
     * @param \ParadoxOne\NDCounties\Database\Repositories\WorkEntries\EloquentWorkEntryWriterRepository $writer
     */
    public function setEloquentWriter(EloquentWorkEntryWriterRepository &$writer)
    {
        $this->writer = $writer;
    }

    public function disconnect()
    {
        unset($this->writer);
        $this->typesRepository = null;
        unset($this->typesRepository);
    }

    /**
     * Returns the adapter runs.
     *
     * @return array
     */
    public function getAdapterRuns()
    {
        return $this->adapterRuns;
    }

    public function process($workEntryID, $data)
    {
        $handlerObject       = new stdClass;
        $handlerObject->id   = $workEntryID;
        $handlerObject->data = $data;

        $adapters = $this->manager->getAdapters();

        if ($this->writer->getPropertyContext() != 1) {
            // This is used when the property set in the form is an actual property.

            $temporaryPropertyType = $this->typesRepository->getModelById($data['record_property_type'], 0);

            $typeAdapters = $temporaryPropertyType->adapters->lists('id');

            // Clean up the property type.
            $temporaryPropertyType = null;
            unset($temporaryPropertyType);
        } else {
            // This is hard-coding the expected type adapters for the equipment units.
            $typeAdapters = [-1];
        }

        foreach ($adapters as $adapter) {
            if (in_array($adapter->getAdapterNumericIdentifier(), $typeAdapters)) {
                $handler = $adapter->getCreatePropertyHandler();

                $success = $handler->handleLog($handlerObject);

                $this->adapterRuns[$adapter->getAdapterNumericIdentifier()] = $success;

                if ($success === false) {
                    $this->writer->mergeValidationErrors($handler->logErrors());
                }
            }
        }
    }
}
