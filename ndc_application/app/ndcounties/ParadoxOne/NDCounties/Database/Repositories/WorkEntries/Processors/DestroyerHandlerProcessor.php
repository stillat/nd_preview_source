<?php namespace ParadoxOne\NDCounties\Database\Repositories\WorkEntries\Processors;

use Illuminate\Support\Facades\DB;
use ParadoxOne\NDCounties\Database\Adapters\AdapterManager;

class DestroyerHandlerProcessor
{
    protected $manager;

    protected $adapterRuns = [];

    protected $destroyer = null;

    public function __construct(AdapterManager $manager)
    {
        $this->manager = $manager;
    }

    public function setDestroyer(&$destroyer)
    {
        $this->destroyer = $destroyer;
    }

    public function disconnect()
    {
        unset($this->destroyer);
    }

    /**
     * Refreshes all work entry property data.
     *
     * NOTE: THIS REMOVES ALL WORK PROPERTY DATA
     *
     * @param $workEntryID
     */
    public function refreshWorkEntryPropertyData($workEntryID)
    {
        foreach ($this->manager->getAdapters() as $adapter) {
            if ($adapter->getTableName() != null) {
                if ($adapter->getWorkColumnName() != null) {
                    DB::connection(\Tenant::getCurrentConnection())->table($adapter->getTableName())
                      ->where($adapter->getWorkColumnName(), '=', $workEntryID)->delete();
                }
            }
        }
    }

    public function getAdapterRuns()
    {
        return $this->adapterRuns;
    }

    public function process($workEntryID, $softDelete = false)
    {
        $handlerObject             = new \stdClass;
        $handlerObject->trueDelete = $softDelete;
        $handlerObject->id         = $workEntryID;

        $adapters = $this->manager->getAdapters();

        // Just run through the adapters and attempt a destroy.
        foreach ($adapters as $adapter) {
            $handler = $adapter->getDeletePropertyHandler();

            if ($handler !== null) {
                $success                                                    = $handler->handle($handlerObject);
                $this->adapterRuns[$adapter->getAdapterNumericIdentifier()] = $success;

                if ($success === false) {
                    $this->destroyer->mergeValidationErrors($handler->logErrors());
                }
            }
        }
    }
}
