<?php namespace ParadoxOne\NDCounties\Database\Repositories\WorkEntries\Builders;

trait RecordRestrictionBuilderTrait
{
    protected $restrictionColumns = [];

    public function setRestrictionColumns(array $columns)
    {
        $this->restrictionColumns = $columns;
    }
}
