<?php namespace ParadoxOne\NDCounties\Database\Repositories\WorkEntries\Builders;

use Illuminate\Support\Facades\DB;

trait DefaultSelectionBuilderTrait
{
    /**
     * Returns the default selection statements for the work entries table.
     *
     * @return array
     */
    private function getDefaultSelectionStatements()
    {
        return [
            DB::raw('work_entries.*'),
        ];
    }

    /**
     * Returns the default selection statements for the activities.
     *
     * @return array
     */
    private function getActivitySelectionStatements()
    {
        return [
            'act.id as activity_id',
            'act.code as activity_code',
            'act.name as activity_name',
        ];
    }

    /**
     * Returns the default selection statements for the departments.
     *
     * @return array
     */
    private function getDepartmentSelectionStatements()
    {
        return [
            'dept.id as department_id',
            'dept.code as department_code',
            'dept.name as department_name',
        ];
    }

    /**
     * Returns the default selection statements for the districts.
     *
     * @return array
     */
    private function getDistrictSelectionStatements()
    {
        return [
            'dist.id as district_id',
            'dist.code as district_code',
            'dist.name as district_name',
        ];
    }

    /**
     * Returns the default selection statements for the employees.
     *
     * @return array
     */
    private function getEmployeeSelectionStatements()
    {
        return [
            'emp.id as employee_id',
            'emp.first_name as employee_first_name',
            'emp.last_name as employee_last_name',
            'emp.middle_name as employee_middle_name',
            'emp.code as employee_code',
        ];
    }

    /**
     * Returns the default selection statements for the projects.
     *
     * @return array
     */
    private function getProjectSelectionStatements()
    {
        return [
            'proj.id as project_id',
            'proj.code as project_code',
            'proj.name as project_name',
        ];
    }

    private function getPropertySelectionStatements()
    {
        return [
            'prop.id as property_id',
            'prop.name as property_name',
            'prop.code as property_code',
        ];
    }

    private function getOrganizationPropertySelectionStatements()
    {
        return [
            'prop_o.id as property_organization_id',
            'prop_o.name as property_organization_name',
            'prop_o.code as property_organization_code',
        ];
    }
}
