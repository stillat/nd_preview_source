<?php namespace ParadoxOne\NDCounties\Database\Repositories\WorkEntries\Builders;

use Illuminate\Support\Facades\App;

trait SortingManagerTrait
{

    /**
     * Determines if the reader will use the report settings.
     *
     * @var bool
     */
    protected $usingReportSortSettings = false;

    /**
     * The report sorting settings.
     *
     * @var array
     */
    protected $reportSortingSettings = [];

    private function applySortingSetting($column, $setting)
    {
        if ($setting == 'asc') {
            $this->reportQuery = $this->reportQuery->orderBy($column, 'asc');
        } elseif ($setting == 'desc') {
            $this->reportQuery = $this->reportQuery->orderBy($column, 'desc');
        }
    }

    /**
     * Sets the reporting sort settings.
     *
     * If no sorting is supplied, implementations should apply the users
     * default work sort settings.
     *
     * @param $reportSortSettings
     * @return mixed
     */
    public function setReportSortSettings($reportSortSettings)
    {
        if (count($reportSortSettings) > 0) {
            $this->usingReportSortSettings = true;
            $this->reportSortingSettings = $reportSortSettings;
        } else {
            $this->usingReportSortSettings = false;
        }
    }

    private function applySorting()
    {
        if ($this->usingReportSortSettings == false) {
            $sortSettings = App::make('sorting_work_entry');
            $sortSettings = json_decode($sortSettings->get($sortSettings::WORK_LOG_TABLE_SETTING_NAME), true);
        } else {
            $sortSettings = $this->reportSortingSettings;
        }

        if ($this->legacyMode) {
            foreach ($sortSettings as $column => $settings) {
                switch ($column) {
                    case 'date_created':
                        $this->applySortingSetting('work_entries.created_at', $settings);
                        break;
                    case 'last_updated':
                        $this->applySortingSetting('updated_at', $settings);
                        break;
                    case 'work_date':
                        $this->applySortingSetting('work_date', $settings);
                        break;
                    case 'employee_name':
                        $this->applySortingSetting('emp.code', $settings);
                        break;
                    case 'employee_regular_hours':
                        $this->applySortingSetting('work_entries.employee_hours_regular_worked', $settings);
                        break;
                    case 'employee_overtime_hours':
                        $this->applySortingSetting('work_entries.employee_hours_overtime_worked', $settings);
                        break;
                    case 'employee_cost':
                        $this->applySortingSetting('work_entries.total_employee_combined_cost', $settings);
                        break;
                    case 'project':
                        $this->applySortingSetting('proj.code', $settings);
                        break;
                    case 'activity':
                        $this->applySortingSetting('act.code', $settings);
                        break;
                    case 'district':
                        $this->applySortingSetting('dist.code', $settings);
                        break;
                    case 'department':
                        $this->applySortingSetting('dept.code', $settings);
                        break;
                    case 'property':
                        $this->applySortingSetting('prop_o.code', $settings);
                        break;
                }
            }
        } else {
            // New sorting here
            foreach ($sortSettings as $column => $settings) {
                switch ($column) {
                    case 'date_created':
                        $this->applySortingSetting('work_entries.created_at', $settings);
                        break;
                    case 'last_updated':
                        $this->applySortingSetting('work_entries.updated_at', $settings);
                        break;
                    case 'work_date':
                        $this->applySortingSetting('work_entries.work_date', $settings);
                        break;
                    case 'employee_name':
                        $this->applySortingSetting('employee_code', $settings);
                        break;
                    case 'employee_regular_hours':
                        $this->applySortingSetting('work_entries.employee_hours_regular_worked', $settings);
                        break;
                    case 'employee_overtime_hours':
                        $this->applySortingSetting('work_entries.employee_hours_overtime_worked', $settings);
                        break;
                    case 'employee_cost':
                        $this->applySortingSetting('work_entries.total_employee_combined_cost', $settings);
                        break;
                    case 'project':
                        $this->applySortingSetting('project_code', $settings);
                        break;
                    case 'activity':
                        $this->applySortingSetting('activity_code', $settings);
                        break;
                    case 'district':
                        $this->applySortingSetting('district_code', $settings);
                        break;
                    case 'department':
                        $this->applySortingSetting('department_code', $settings);
                        break;
                    case 'property':
                        $this->applySortingSetting('property_organization_code', $settings);
                        break;
                }
            }
        }
    }
}
