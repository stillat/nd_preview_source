<?php namespace ParadoxOne\NDCounties\Database\Repositories\WorkEntries\Builders;

use Illuminate\Support\Facades\DB;

trait ReportBuilderTrait
{
    protected $isReportMode = false;

    protected $reportRecordTotals = null;

    protected $reportGrandRecordsTotalQuery = null;

    /**
     * Sets if the reader is in report mode.
     *
     * @param $isReportMode
     * @return mixed
     */
    public function setReportMode($isReportMode)
    {
        $this->isReportMode = $isReportMode;
    }

    /**
     * Gets the report record totals.
     *
     * @return mixed
     */
    public function getReportTotals()
    {
        return $this->reportRecordTotals;
    }

    /**
     * Sets the total query for the grand totals.
     *
     * @param $query
     * @return mixed
     */
    public function setReportRecordTotalQuery($query)
    {
        $this->reportGrandRecordsTotalQuery = $query;
    }

    private function applyReportRecordQuery()
    {
        if (count($this->workEntryIDs) == 0) {
            $recordTotals                                       = new \stdClass;
            $recordTotals->grand_employee_hours_regular_worked  = 0;
            $recordTotals->grand_employee_hours_overtime_worked = 0;
            $recordTotals->grand_employee_hours_total_worked    = 0;
            $recordTotals->grand_misc_billing_quantity          = 0;
            $recordTotals->grand_total_materials_quantity       = 0;
            $recordTotals->grand_total_equipment_units_quantity = 0;
            $recordTotals->grand_total_fuels_quantity           = 0;
            $recordTotals->grand_total_materials_cost           = 0;
            $recordTotals->grand_total_equipment_units_cost     = 0;
            $recordTotals->grand_total_fuels_cost               = 0;
            $recordTotals->grand_total_equipment_and_fuels_cost = 0;
            $recordTotals->grand_total_misc_billing_cost        = 0;
            $recordTotals->grand_total_property_cost            = 0;
            $recordTotals->grand_total_employee_regular_cost    = 0;
            $recordTotals->grand_total_employee_overtime_cost   = 0;
            $recordTotals->grand_total_employee_combined_cost   = 0;
            $recordTotals->grand_record_totals                  = 0;

            $this->reportRecordTotals = $recordTotals;

            return;
        }

        if (count($this->reportQuery->groups) > 0) {
            // We need to remove the groups from the query to get the correct aggregate totals.
            // This was a bitch to track down.
            $newQuery           = $this->reportQuery;
            $newQuery->havings =  null;
            $newQuery->groups   = null;
            // TODO: CHECK LEGACY
            $newQuery->orders   = null;
            $allWorkRecords     = array_pluck_unique($newQuery->select('work_entries.id')->get(), 'id');
            $this->workEntryIDs = $allWorkRecords;
        }

        if (count($this->workEntryIDs) == 0) {
            $this->workEntryIDs = [0];
        }
        $totals = $this->table('work_entries')->whereIn('work_entries.id', $this->workEntryIDs)
                       ->select(
                           DB::raw('SUM(employee_hours_regular_worked) as grand_employee_hours_regular_worked'),
                           DB::raw('SUM(employee_hours_overtime_worked) as grand_employee_hours_overtime_worked'),
                           DB::raw('SUM(employee_hours_regular_worked + employee_hours_overtime_worked) as grand_employee_hours_total_worked'),
                           DB::raw('SUM(misc_billing_quantity) as grand_misc_billing_quantity'),
                           DB::raw('SUM(total_materials_quantity) as grand_total_materials_quantity'),
                           DB::raw('SUM(total_equipment_units_quantity) as grand_total_equipment_units_quantity'),
                           DB::raw('SUM(total_fuels_quantity) as grand_total_fuels_quantity'),
                           DB::raw('SUM(total_materials_cost) as grand_total_materials_cost'),
                           DB::raw('SUM(total_equipment_units_cost) as grand_total_equipment_units_cost'),
                           DB::raw('SUM(total_fuels_cost) as grand_total_fuels_cost'),
                           DB::raw('SUM(total_equipment_and_fuels_cost) as grand_total_equipment_and_fuels_cost'),
                           DB::raw('SUM(total_misc_billing_cost) as grand_total_misc_billing_cost'),
                           DB::raw('SUM(total_property_cost) as grand_total_property_cost'),
                           DB::raw('SUM(total_employee_regular_cost) as grand_total_employee_regular_cost'),
                           DB::raw('SUM(total_employee_overtime_cost) as grand_total_employee_overtime_cost'),
                           DB::raw('SUM(total_employee_combined_cost) as grand_total_employee_combined_cost'),
                           DB::raw($this->reportGrandRecordsTotalQuery)
                       )->first();

        $this->reportRecordTotals = $totals;
    }

    /**
     * Holds the property header.
     *
     * Recomputing can be expensive; new readers should be used
     * between retrievals.
     *
     * @var null|string
     */
    protected $cachedPropertyHeader = null;

    /**
     * Intelligently returns the proper report header.
     *
     * @return string
     */
    public function getReportPropertyHeader()
    {
        if ($this->cachedPropertyHeader !== null) {
            return $this->cachedPropertyHeader;
        }

        $header = 'Property';

        $roadProperty = array_pluck_first_where($this->workEntryRecords, 'property_organization_context', 0);
        $repairProperty = array_pluck_first_where($this->workEntryRecords, 'property_context', 1);

        // If this is 0, we know that the result set only has NA records for ROAD.
        $sumOfRoadRecords = 0;
        $repairObserved = false;
        $roadLengthObserved = false;


        foreach ($this->workEntryRecords as $r) {
            // Add road IDs together.
            if (isset($r->property_organization_id)) {
                $sumOfRoadRecords += $r->property_organization_context;
            }

            if (isset($r->property_context)) {
                if ($r->property_context == 1) {
                    $repairObserved = true;
                }
                if ($r->property_context == 0) {
                    $roadLengthObserved = true;
                }
            }
        }

        if ($repairObserved && $sumOfRoadRecords == 0 && $roadLengthObserved == false) {
            $header = 'Equipment Unit (Repair)';
        }

        if (!$repairObserved && ($sumOfRoadRecords > 0 || $roadLengthObserved)) {
            return 'Road';
        }

        $this->cachedPropertyHeader = $header;
        return $header;
    }

}
