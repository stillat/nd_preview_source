<?php namespace ParadoxOne\NDCounties\Database\Repositories\WorkEntries\Updaters;

use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\MessageBag;
use ParadoxOne\NDCounties\Contracts\WorkEntries\WorkEntryUpdaterRepositoryInterface;
use ParadoxOne\NDCounties\Database\Repositories\WorkEntries\EloquentWorkEntryWriterRepository;
use Service\WorkEntries\WorkEntry;

class DatabaseWorkEntryUpdater extends EloquentWorkEntryWriterRepository implements WorkEntryUpdaterRepositoryInterface
{
    use \ParadoxOne\NDCounties\Database\Repositories\WorkEntries\Updaters\Builders\PropertyWatcherBuilderTrait;
    use \ParadoxOne\NDCounties\Database\Repositories\WorkEntries\Updaters\Builders\InvoiceUpdaterBuilderTrait;

    private $validationErrors = null;

    /**
     * Indicates if the updater should actually commit any changes.
     *
     * @var bool
     */
    protected $pretendMode = false;

    /**
     * The work entry ID.
     *
     * @var int
     */
    protected $workEntryID = -1;

    /**
     * The fuels that need to be removed.
     *
     * @var array
     */
    protected $fuelsToRemove = [];

    /**
     * The materials that need to be removed.
     *
     * @var array
     */
    protected $materialsToRemove = [];

    /**
     * The equipment units that need to be removed.
     *
     * @var array
     */
    protected $equipmentUnitsToRemove = [];

    protected $invoiceTotalDifference = 0;

    protected $invoiceTotalNeedsUpdating = false;

    public function mergeValidationErrors($errors)
    {
        if ($errors == null) {
            return;
        }

        $this->validationErrors = $this->validationErrors->merge($errors);
    }

    /**
     * Returns a query builder for the work consumables table.
     *
     * @return mixed
     */
    private function consumableTable()
    {
        return DB::connection(\Tenant::getCurrentConnection())->table('work_consumables');
    }

    /**
     * Returns a query builder for the work table.
     *
     * @return mixed
     */
    private function workTable()
    {
        return DB::connection(\Tenant::getCurrentConnection())->table('work_entries');
    }

    private function getTable($table)
    {
        return DB::connection(\Tenant::getCurrentConnection())->table($table);
    }

    /**
     * Performs the steps to remove the fuels.
     */
    private function doRemoveFuels()
    {
        if (count($this->fuelsToRemove) > 0) {
            $this->consumableTable()->whereIn('id', $this->fuelsToRemove)->delete();
        }
    }

    /**
     * Performs the steps to remove the materials.
     */
    private function doRemoveMaterials()
    {
        if (count($this->materialsToRemove) > 0) {
            $this->consumableTable()->whereIn('id', $this->materialsToRemove)->delete();
        }
    }

    /**
     * Performs the steps to remove the equipment units.
     */
    private function doRemoveEquipmentUnits()
    {
        if (count($this->equipmentUnitsToRemove) > 0) {
            // This piece of code removes all the fuels associated with the equipment records.
            $this->consumableTable()->whereIn('bound_consumable_record', $this->equipmentUnitsToRemove)
                 ->where(function ($query) {
                     $query->where('work_entry_id', '=', $this->getWorkEntryID());
                 })->delete();

            // Just a quick hard-coded snippet to remove the associated odometer readings. There is no reason to load
            // the adapter framework to accomplish this.
            $this->getTable('equipment_odometer_readings')->whereIn('equipment_id', $this->equipmentUnitsToRemove)
                 ->where(function ($query) {
                     $query->where('associated_record', '=', $this->getWorkEntryID());
                 })
                 ->where(function ($query) {
                     $query->where('associated_record_type', '=', '1');
                 })->delete();

            // Remove the actual equipment units.
            $this->consumableTable()->whereIn('id', $this->equipmentUnitsToRemove)->delete();
        }
    }

    /**
     * Helper function that calls the other consumable removal functions.
     *
     * @throws EquipmentUpdateErrorException
     * @throws FuelUpdateErrorException
     * @throws MaterialUpdateErrorException
     */
    private function doRemoveAnyConsumables()
    {
        try {
            $this->doRemoveFuels();
        } catch (\Exception $e) {
            throw new FuelUpdateErrorException($e->getMessage());
        }

        try {
            $this->doRemoveEquipmentUnits();
        } catch (\Exception $e) {
            throw new EquipmentUpdateErrorException($e->getMessage());
        }

        try {
            $this->doRemoveMaterials();
        } catch (\Exception $e) {
            throw new MaterialUpdateErrorException($e->getMessage());
        }
    }

    /**
     * Sets the work entry ID.
     *
     * @param $workEntryID
     * @return mixed
     */
    public function setWorkEntryID($workEntryID)
    {
        $this->workEntryID = $workEntryID;
    }

    /**
     * Gets the work entry ID.
     *
     * @return mixed
     */
    public function getWorkEntryID()
    {
        return $this->workEntryID;
    }


    /**
     * Indicate that a fuel should be removed.
     *
     * @param $fuelID
     * @return mixed
     */
    public function notifyFuelRemoval($fuelID)
    {
        $this->fuelsToRemove[] = $fuelID;
    }

    /**
     * Indicate that a material should be removed.
     *
     * @param $materialID
     * @return mixed
     */
    public function notifyMaterialRemoval($materialID)
    {
        $this->materialsToRemove[] = $materialID;
    }

    /**
     * Indicate that an equipment unit and all its fuels should be removed.
     *
     * @param $equipmentUnitID
     * @return mixed
     */
    public function notifyEquipmentUnitRemoval($equipmentUnitID)
    {
        $this->equipmentUnitsToRemove[] = $equipmentUnitID;
    }

    /**
     * Sets the pretend mode setting.
     *
     * @param $mode
     */
    public function setPretendMode($mode)
    {
        $this->pretendMode = $mode;
    }

    /**
     * Gets the pretend mode setting value.
     *
     * @return bool
     */
    public function getPretendMode()
    {
        return $this->pretendMode;
    }


    public function commit()
    {
        $originalInvoiceTotal    = 0;
        $originalPropertyID      = 0;
        $originalPropertyContext = 0;

        DB::connection(\Tenant::getCurrentConnection())->beginTransaction();

        try {
            $workEntry = WorkEntry::findOrFail($this->getWorkEntryID());

            if ($workEntry->record_status == 5) {
                throw new LockedRecordException;
            }

            /** Identifying information. */
            $workEntry->work_date     = $this->workLogDate;
            $workEntry->employee_id   = $this->employeeID;
            $workEntry->activity_id   = $this->activityID;
            $workEntry->project_id    = $this->projectID;
            $workEntry->department_id = $this->departmentID;
            $workEntry->district_id   = $this->districtID;
            $originalPropertyID       = $workEntry->property_id;
            $workEntry->property_id   = $this->propertyID;

            $workEntry->property_organization_id      = $this->organizationPropertyID;
            $workEntry->property_organization_context = $this->organizationPropertyContext;


            $workEntry->activity_description = $this->activityDescription;

            /** Employee hours data */
            $workEntry->employee_hours_regular_worked  = $this->regularHoursWorked;
            $workEntry->employee_hours_overtime_worked = $this->overtimeHoursWorked;

            /** Historic data */
            $workEntry->employee_historic_wage_regular    = $this->historicRegularWage;
            $workEntry->employee_historic_wage_overtime   = $this->historicOvertimeWage;
            $workEntry->employee_historic_fringe_regular  = $this->historicRegularFringeBenefits;
            $workEntry->employee_historic_fringe_overtime = $this->historicOvertimeFringeBenefits;

            /** Miscellaneous billing data */
            $workEntry->misc_billing_quantity = $this->miscBillingQuantity;
            $workEntry->misc_billing_rate     = $this->miscBillingRate;
            $workEntry->misc_unit_id          = $this->miscBillingMeasurementUnit;
            $workEntry->misc_description      = $this->miscBillingDescription;
            $workEntry->credit_inventory      = $this->isCreditInventoryMode;

            $originalPropertyContext     = $workEntry->property_context;
            $workEntry->property_context = $this->propertyContext;

            /** Miscellaneous tax information */
            $workEntry->employee_taxable = $this->getEmployeeIsTaxed();

            /** Setting Data */
            $workEntry->exclude_from_report = $this->getExcludedFromReports();

            /** Aggregate report information */
            $workEntry->total_materials_quantity       = $this->calculateTotalMaterialsQuantity();
            $workEntry->total_equipment_units_quantity = $this->calculateTotalEquipmentUnitsQuantity();
            $workEntry->total_fuels_quantity           = $this->caclulateTotalFuelsQuantity();
            $workEntry->total_materials_cost           = $this->calculateTotalMaterialCost();
            $workEntry->total_equipment_units_cost     = $this->calculateTotalEquipmentUnitsCost();
            $workEntry->total_fuels_cost               = $this->calculateTotalFuelsCost();
            $workEntry->total_equipment_and_fuels_cost = $this->calculateTotalEquipmentAndFuelsCost();

            // Hold the original invoice total temporarily.
            $originalInvoiceTotal = $workEntry->total_invoice;

            if (is_string($this->invoiceOverrideTotal) && strlen($this->invoiceOverrideTotal) > 0 &&
                floatval($this->invoiceOverrideTotal) >= 0 && is_bool($this->invoiceOverrideTotal) == false
            ) {
                $workEntry->total_invoice = $this->invoiceOverrideTotal;
            } else {
                $workEntry->total_invoice = 0;
            }

            $workEntry->total_employee_regular_cost  = $this->calculateRegularHoursCost();
            $workEntry->total_employee_overtime_cost = $this->calculateOvertimeHoursCost();
            $workEntry->total_employee_combined_cost = $this->caclulateCombinedHoursCost();

            $workEntry->total_misc_billing_cost = $this->calculateTotalMiscBillingTotalCost();

            // START Consumable removal processes.
            $this->doRemoveAnyConsumables();
            // END Consumable removal process.

            // Handle the invoice update process.
            $this->handleInvoiceUpdate();

            $this->validationErrors = new MessageBag;

            // Hold any errors that the consumable writer might throw.
            $consumableValidationPassed = true;

            foreach ($this->consumableRecords as $consumable) {
                $consumableValidated = $consumable->isValid();

                if (!$consumableValidated) {
                    $this->mergeValidationErrors($consumable->errors()->all());

                    if ($consumableValidationPassed) {
                        $consumableValidationPassed = false;
                    }
                }
            }

            $presenceVerifier = App::make('validation.presence');
            $presenceVerifier->setConnection(\Tenant::getCurrentConnection());
            $workEntry->setPresenceVerifier($presenceVerifier);

            $workEntryValidationPassed = $workEntry->isValid();

            // Holds if the work entry update was a success. Default to FALSE.
            $success = false;

            if (!$workEntryValidationPassed) {
                $this->mergeValidationErrors($workEntry->errors()->all());
            }

            if ($workEntryValidationPassed && $consumableValidationPassed) {
                $success             = $workEntry->update();
                $consumableSuccesses = array();

                usort($this->consumableRecords, function ($a, $b) {
                    return $a->getConsumableType() > $b->getConsumableType();
                });

                foreach ($this->consumableRecords as $consumable) {
                    $consumable->setWorkEntryID($workEntry->id);
                    $this->consumableWriterRepository->setConsumable($consumable);
                    try {
                        $consumableSuccesses[$this->consumableWriterRepository->updateOrCreate()] = 0;
                    } catch (\Exception $consumableUpdateException) {
                    }
                }

                if ($this->propertyWasChanged() || $this->propertyWasUpdated()) {
                    // We will need the insert and removal adapter processors.
                    $writerProcessor  =
                        App::make('\ParadoxOne\NDCounties\Database\Repositories\WorkEntries\Processors\WriterHandlerProcessor');
                    $destroyProcessor =
                        App::make('\ParadoxOne\NDCounties\Database\Repositories\WorkEntries\Processors\DestroyerHandlerProcessor');


                    $writerProcessor->setEloquentUpdater($this);
                    $destroyProcessor->setDestroyer($this);

                    $destroyProcessor->refreshWorkEntryPropertyData($this->workEntryID);
                    $writerProcessor->process($this->workEntryID, Input::all());

                    if (in_array(false, $writerProcessor->getAdapterRuns())) {
                        DB::connection(\Tenant::getCurrentConnection())->rollback();
                    }

                    // Disconnect the processors.
                    $writerProcessor->disconnect();
                    $destroyProcessor->disconnect();
                }

                // Update the property total.
                if ($this->propertyTotal > 0) {
                    $workEntry->total_property_cost = $this->propertyTotal;
                    $workEntry->save();
                }
            }

            // Return result(s).
            if ($success) {
                // Allows developers to run the updater to check features without
                // affecting the actual records.
                if ($this->pretendMode) {
                    DB::connection(\Tenant::getCurrentConnection())->rollback();
                } else {
                    \DB::connection(\Tenant::getCurrentConnection())->commit();
                    \DB::connection(\Tenant::getCurrentConnection())->commit();
                    \DB::connection(\Tenant::getCurrentConnection())->commit();
                    \DB::connection(\Tenant::getCurrentConnection())->commit();

                    \DB::connection(\Tenant::getCurrentConnection())->commit();
                }

                Event::fire('workrecord.updated', [$workEntry->id]);

                return true;
            }
        } catch (\Exception $generalException) {
            // General exception here.
        }

        DB::connection(\Tenant::getCurrentConnection())->rollback();

        return false;
    }

    /**
     * Returns a collection of error messages to the user.
     *
     * @return \Illuminate\Support\MessageBag
     */
    public function errors()
    {
        return $this->validationErrors;
    }
}

class EquipmentUpdateErrorException extends \Exception
{
}

class MaterialUpdateErrorException extends \Exception
{
}

class FuelUpdateErrorException extends \Exception
{
}
