<?php namespace ParadoxOne\NDCounties\Database\Repositories;

use Exception;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\Facades\Validator;
use ParadoxOne\NDCounties\Contracts\ActivityRepositoryInterface;
use ParadoxOne\NDCounties\IdentificationCode as IDCode;
use ParadoxOne\NDCounties\Settings\SortingSettingsManager;
use ParadoxOne\NDCounties\Traits\DefaultSortableTrait;
use ParadoxOne\NDCounties\Traits\SearchableTrait;
use ParadoxOne\NDCounties\Traits\UnTrashedQueryBuilderTrait;
use Service\Activity;

class ActivityRepository extends BaseTenantRepository implements ActivityRepositoryInterface
{
    use SearchableTrait;
    use DefaultSortableTrait;
    use UnTrashedQueryBuilderTrait;

    protected $errors = null;

    /**
     * The table for the repository.
     *
     * @var string
     */
    protected $table = 'activities';

    public function remove(array $removeData = array())
    {
        $this->startTransaction();
        $removeData = array_strip_values($removeData, 0);
        try {
            foreach ($removeData as $activity) {
                Event::fire('structure.activity.removed', $activity);
            }
            Activity::destroy($removeData);
            $this->commitTransaction();
            return true;
        } catch (Exception $e) {
        }

        foreach ($removeData as $activity) {
            Event::fire('structure.activity.removed.failed', $activity);
        }

        $this->rollbackTransaction();

        return false;

        //Activity::destroy(intval($removeData[0]));
    }

    public function update($id, array $activityData = array())
    {
        $activity = Activity::findOrFail($id);
        $this->startTransaction();

        $activity->name        = $activityData['name'];
        $activity->description = $activityData['description'];
        $activity->code        = IDCode::prepareFrom($activityData['code']);

        //$activity->updateUniques();

        $validationRules = Activity::$rules;
        $validationRules['code'] = Activity::$rules['code'] . ',' . $activity->id . ',id,deleted_at,NULL';
        $validationRules['name'] = Activity::$rules['name'] . ',' . $activity->id . ',id,deleted_at,NULL';

        if ($activity->save($validationRules)
        ) {
            Event::fire('structure.activity.updated', array($activity));

            $this->commitTransaction();

            return $activity;
        } else {
            $this->errors = $activity->errors();
        }

        $this->rollbackTransaction();

        return false;
    }

    public function create(array $accountData = array())
    {
        $this->startTransaction();
        $activity              = new Activity;
        $activity->name        = $accountData['name'];
        $activity->description = $accountData['description'];
        $activity->code        = IDCode::prepareFrom($accountData['code']);


        $validationRules = Activity::$rules;
        $validationRules['code'] = Activity::$rules['code']. ',NULL,id,deleted_at,NULL';
        $validationRules['name'] = Activity::$rules['name']. ',NULL,id,deleted_at,NULL';

        if ($activity->validate($validationRules)) {
            // See if we have any thing that can be restored.
            $possibleRestore = Activity::withTrashed()->where('code', '=', $activity->code)->first();
            if ($possibleRestore !== null) {
                $possibleRestore->name = $activity->name;
                $possibleRestore->description = $activity->description;
                $possibleRestore->deleted_at = null;
                $validationRules = Activity::$rules;
                $validationRules['code'] = Activity::$rules['code'] . ',' . $activity->id . ',id,deleted_at,NULL';
                $saved = $possibleRestore->save($validationRules);
                Event::fire('structure.activity.created', array($possibleRestore));
                $this->commitTransaction();

                return $possibleRestore;
            }
        }

        if ($activity->save($validationRules)
        ) {
            Event::fire('structure.activity.created', array($activity));
            $this->commitTransaction();

            return $activity;
        } else {
            $this->errors = $activity->errors();
        }

        $this->rollbackTransaction();

        return false;
    }

    public function errors()
    {
        return $this->errors;
    }

    private function getSortedBuilder()
    {
        $builder = $this->table()->select('*');

        $this->applySorting(SortingSettingsManager::ACTIVITIES_TABLE_SETTING_NAME, $builder);

        return $builder;
    }

    public function getAll()
    {
        return $this->getSortedBuilder()->whereNull('deleted_at');
    }

    public function getAllWithTrashed()
    {
        return $this->getSortedBuilder();
    }


    public function getModelById($id)
    {
        Event::fire('structure.activity.accessed', array($id));

        return Activity::findOrFail($id);
    }

    public function unTrash($activities)
    {
        $this->startTransaction();

        if (!is_array($activities)) {
            $activities = (array)$activities;
        }

        if (count($activities) == 0) {
            return false;
        }

        try {
            $restoreData = $this->getRecordsThatCantBeRestored($this->table, $activities);

            if (count($restoreData->recordsThatCanBeRestored) == 0) {
                // If we've reached this point, there are no records that can be restored
                // due to conflicts.
                Event::fire('structure.activity.restored.conflictsDetected', [$restoreData->similarRecords]);
                $this->rollbackTransaction();

                // Return true because of reasons.
                return true;
            }

            $this->table()->whereIn('id', $activities)->update([
               'deleted_at' => null
            ]);
            Event::fire('structure.activity.restored', [$activities]);
            $this->commitTransaction();
            return true;
        } catch (\Exception $e) {
        }

        $this->rollbackTransaction();

        return false;
    }


}
