<?php namespace ParadoxOne\NDCounties\Database\Repositories\Settings;

use ParadoxOne\NDCounties\Contracts\Settings\CountyWideSettingsRepositoryInterface;

class CountySettingsRepository extends UserSettingsRepository implements CountyWideSettingsRepositoryInterface
{
    protected $table = 'county_wide_application_settings';

    protected $foreignKey = 'service_account_id';
}
