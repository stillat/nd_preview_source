<?php namespace ParadoxOne\NDCounties\Database\Repositories\Settings;

use ParadoxOne\NDCounties\Contracts\Settings\UserSettingsRepositoryInterface;
use ParadoxOne\NDCounties\Database\Repositories\BaseRepository;

class UserSettingsRepository extends BaseRepository implements UserSettingsRepositoryInterface
{
    protected $table = 'user_application_settings';

    protected $foreignKey = 'user_id';

    /**
     * Creates a new record.
     *
     * @param  array $recordDetails
     * @return mixed
     */
    public function create(array $recordDetails = array())
    {
    }

    /**
     * Removes an existing record.
     *
     * @param  array $removeDetails
     * @return mixed
     */
    public function remove(array $removeDetails = array())
    {
    }

    /**
     * Updates an existing record.
     *
     * @param  int   $recordID
     * @param  array $newRecordDetails
     * @return mixed
     */
    public function update($recordID, array $newRecordDetails)
    {
    }


    /**
     * Gets a setting value.
     *
     * @param $userID
     * @param $subSystem
     * @param $settingName
     * @param $defaultValue
     * @return mixed
     */
    public function getSettingValue($userID, $subSystem, $settingName, $defaultValue = null)
    {
        $value =
            $this->table()->select('setting_value')->where($this->foreignKey, '=', $userID)
                 ->where('subsystem', '=', $subSystem)
                 ->where('setting_name', '=', $settingName)->first();

        if ($value == null && $defaultValue != null) {
            $this->createSetting($userID, $subSystem, $settingName, $defaultValue);

            return $defaultValue;
        } elseif ($value == null && $defaultValue == null) {
            return null;
        }

        return $value->setting_value;
    }

    /**
     * Removes a setting from the user.
     *
     * @param $userID
     * @param $subSystem
     * @param $settingName
     * @return mixed
     */
    public function removeSetting($userID, $subSystem, $settingName)
    {
        return $this->table()->where($this->foreignKey, '=', $userID)->where('subsystem', '=', $subSystem)
                    ->where('setting_name', '=', $settingName)->delete();
    }

    /**
     * Removes an entire setting sub-system.
     *
     * @param $userID
     * @param $subSystem
     * @return mixed
     */
    public function removeSubSystem($userID, $subSystem)
    {
        return $this->table()->where($this->foreignKey, '=', $userID)->where('subsystem', '=', $subSystem)->delete();
    }


    /**
     * Creates a setting for the given user.
     *
     * @param $userID
     * @param $subSystem
     * @param $settingName
     * @param $initialValue
     * @return mixed|void
     * @throws \Exception
     */
    public function createSetting($userID, $subSystem, $settingName, $initialValue)
    {
        if ($initialValue == null) {
            throw new \Exception('Initial value cannot be null');
        }

        $doesValueExist = $this->getSettingValue($userID, $subSystem, $settingName, null);

        if ($doesValueExist == null) {
            $success = $this->table()->insert([
                                                  $this->foreignKey => $userID,
                                                  'subsystem'       => $subSystem,
                                                  'setting_name'    => $settingName,
                                                  'setting_value'   => $initialValue
                                              ]);

            if ($success) {
                return $initialValue;
            } else {
                return false;
            }
        } else {
            if ($settingName !== $doesValueExist) {
                $success = $this->setSettingValue($userID, $subSystem, $settingName, $initialValue);

                if ($success || $success === 0) {
                    return $initialValue;
                } else {
                    return false;
                }
            }

            return $doesValueExist;
        }
    }

    /**
     * Sets the setting value.
     *
     * @param $userID
     * @param $subSystem
     * @param $settingName
     * @param $newValue
     * @return mixed
     */
    public function setSettingValue($userID, $subSystem, $settingName, $newValue)
    {
        return $this->table()->where($this->foreignKey, '=', $userID)
                    ->where('subsystem', '=', $subSystem)->where('setting_name', '=', $settingName)
                    ->update(['setting_value' => $newValue]);
    }
}
