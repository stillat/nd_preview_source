<?php namespace ParadoxOne\NDCounties\Database\Repositories\Tracking;

use Illuminate\Support\Facades\Event;
use ParadoxOne\NDCounties\Contracts\Tracking\GPSTrackingRepositoryInterface;
use ParadoxOne\NDCounties\Database\Repositories\BaseTenantRepository;
use Service\Tracking\GPSTrack;

class GPSTrackingRepository extends BaseTenantRepository implements GPSTrackingRepositoryInterface
{
    protected $table = 'gps_entries';

    public function create(array $trackInfo = array())
    {
        $track                 = new GPSTrack;
        $track->latitude       = $trackInfo['latitude'];
        $track->longitude      = $trackInfo['longitude'];
        $track->altitude       = $trackInfo['altitude'];
        $track->date_recorded  = $trackInfo['date_recorded'];
        $track->trackable_type = $trackInfo['trackable_type'];
        $track->trackable_id   = $trackInfo['trackable_id'];

        if ($track->save()) {
            Event::fire('gpstrack.created', array($track));

            return $track;
        } else {
            $this->errors = $track->errors();

            return false;
        }
    }

    public function update($id, array $trackInfo = array())
    {
        $track                 = GPSTrack::findOrFail($id);
        $track->latitude       = $trackInfo['latitude'];
        $track->longitude      = $trackInfo['longitude'];
        $track->altitude       = $trackInfo['altitude'];
        $track->date_recorded  = $trackInfo['date_recorded'];
        $track->trackable_type = $trackInfo['trackable_type'];
        $track->trackable_id   = $trackInfo['trackable_id'];

        if ($track->save()) {
            Event::fire('gpstrack', array($track));

            return $track;
        } else {
            $this->errors = $track->errors();

            return false;
        }
    }

    public function remove(array $removeContext = array())
    {
        Event::fire('gpstrack.removed', array($removeContext[0]));
        GPSTrack::destroy(intval($removeContext[0]));
    }
}
