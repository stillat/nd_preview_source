<?php namespace ParadoxOne\NDCounties\Database\Repositories\Projects;

use Exception;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Event;
use ParadoxOne\NDCounties\Contracts\Projects\ProjectRepositoryInterface;
use ParadoxOne\NDCounties\Database\Repositories\BaseTenantRepository;
use ParadoxOne\NDCounties\IdentificationCode as IDCode;
use ParadoxOne\NDCounties\Settings\SortingSettingsManager;
use ParadoxOne\NDCounties\Traits\DefaultSortableTrait;
use ParadoxOne\NDCounties\Traits\SearchableTrait;
use ParadoxOne\NDCounties\Traits\UnTrashedQueryBuilderTrait;
use Service\Projects\Project;

class ProjectRepository extends BaseTenantRepository implements ProjectRepositoryInterface
{
    use SearchableTrait;
    use DefaultSortableTrait;
    use UnTrashedQueryBuilderTrait;

    protected $table = 'projects';

    public function getModelById($id)
    {
        return Project::findOrFail($id);
    }

    private function applyProjectSorting(&$builder)
    {
        $sortSettings = App::make('sorting_work_entry');
        $sortSettings = json_decode($sortSettings->get(SortingSettingsManager::PROJECTS_TABLE_SETTING_NAME), true);

        foreach ($sortSettings as $column => $settings) {
            switch ($column) {
                case 'code':
                    $this->applySortingSetting('code', $settings, $builder);
                    break;
                case 'name':
                    $this->applySortingSetting('name', $settings, $builder);
                    break;
                case 'desc':
                    $this->applySortingSetting('description', $settings, $builder);
                    break;
                case 'last_updated':
                    $this->applySortingSetting('updated_at', $settings, $builder);
                    break;
                case 'date_created':
                    $this->applySortingSetting('created_at', $settings, $builder);
                    break;
                case 'is_fema':
                    $this->applySortingSetting('fema_project', $settings, $builder);
                    break;
            }
        }

        $sortSettings = null;
        unset($sortSettings);
    }

    private function getSortedBuilder()
    {
        $builder = $this->table()->select('id', 'code', 'name', 'description', 'fema_project', 'deleted_at');
        $this->applyProjectSorting($builder);
        return $builder;
    }

    public function getProjects()
    {
        return $this->getSortedBuilder()->whereNull('deleted_at');
    }

    public function getAll()
    {
        return $this->getProjects();
    }

    public function getAllWithTrashed()
    {
        return $this->getSortedBuilder();
    }


    public function create(array $projectData = array())
    {
        $this->startTransaction();
        $project               = new Project;
        $project->name         = $projectData['name'];
        $project->description  = $projectData['description'];
        $project->code         = IDCode::prepareFrom($projectData['code']);
        $project->fema_project = exists_then_or($projectData, 'fema_project');

        $validationRules = Project::$rules;
        $validationRules['code'] = Project::$rules['code'] . ',NULL,id,deleted_at,NULL';
        $validationRules['name'] = Project::$rules['name'] . ',NULL,id,deleted_at,NULL';

        if ($project->validate($validationRules)) {
            // See if we have any thing that can be restored.
            $possibleRestore = Project::withTrashed()->where('code', '=', $project->code)->first();
            if ($possibleRestore !== null) {
                $possibleRestore->name = $project->name;
                $possibleRestore->description = $project->description;
                $possibleRestore->fema_project = exists_then_or($projectData, 'fema_project');
                $possibleRestore->deleted_at = null;
                $validationRules = Project::$rules;
                $validationRules['code'] = Project::$rules['code'] . ',' . $project->id . ',id,deleted_at,NULL';
                $saved = $possibleRestore->save($validationRules);
                Event::fire('project.created', array($possibleRestore));
                $this->commitTransaction();
                return $possibleRestore;
            }
        }

        if ($project->save($validationRules)
        ) {
            Event::fire('project.created', array($project));
            $this->commitTransaction();
            return $project;
        } else {
            $this->errors = $project->errors();
        }
        $this->rollbackTransaction();
        return false;
    }

    public function update($id, array $projectData = array())
    {
        $project               = Project::findOrFail($id);
        $this->startTransaction();
        $project->name         = $projectData['name'];
        $project->description  = $projectData['description'];
        $project->code         = IDCode::prepareFrom($projectData['code']);
        $project->fema_project = exists_then_or($projectData, 'fema_project');

        $validationRules = Project::$rules;
        $validationRules['code'] = Project::$rules['code'] . ',' . $project->id . ',id,deleted_at,NULL';
        $validationRules['name'] = Project::$rules['name'] . ',' . $project->id . ',id,deleted_at,NULL';

        if ($project->save($validationRules)
        ) {
            Event::fire('project.updated', array($project));

            $this->commitTransaction();
            return $project;
        } else {
            Event::fire('project.updated.failed', array($id));
            $this->errors = $project->errors();
        }

        $this->rollbackTransaction();
        return false;
    }

    public function remove(array $removeData = array())
    {
        $this->startTransaction();
        $removeData = array_strip_values($removeData, 0);
        try {
            foreach ($removeData as $project) {
                Event::fire('project.removed', $project);
            }
            Project::destroy($removeData);
            $this->commitTransaction();
            return true;
        } catch (Exception $e) {
        }

        foreach ($removeData as $project) {
            Event::fire('project.removed.failed', $project);
        }

        $this->rollbackTransaction();

        return false;

    }

    public function unTrash($projects)
    {
        $this->startTransaction();

        if (!is_array($projects)) {
            $projects = (array)$projects;
        }

        if (count($projects) == 0) {
            return false;
        }

        try {
            $restoreData = $this->getRecordsThatCantBeRestored($this->table, $projects);

            if (count($restoreData->recordsThatCanBeRestored) == 0) {
                // If we've reached this point, there are no records that can be restored
                // due to conflicts.
                Event::fire('project.restored.conflictsDetected', [$restoreData->similarRecords]);
                $this->rollbackTransaction();

                // Return true because of reasons.
                return true;
            }

            $this->table()->whereIn('id', $projects)->update([
                'deleted_at' => null
            ]);
            Event::fire('project.restored', [$projects]);
            $this->commitTransaction();
            return true;
        } catch (\Exception $e) {
        }

        $this->rollbackTransaction();

        return false;
    }

}
