<?php namespace ParadoxOne\NDCounties\Database\Repositories\Analytics;

use Illuminate\Support\Facades\DB;
use ParadoxOne\NDCounties\Contracts\Analytics\SystemAnalyticsRepositoryInterface;
use ParadoxOne\NDCounties\Database\NotImplementedException;
use ParadoxOne\NDCounties\Database\Repositories\BaseRepository;

class AnalyticsRepository extends BaseRepository implements SystemAnalyticsRepositoryInterface
{
    protected $hiddenSchemas = [

    ];

    /**
     * Creates a new record.
     *
     * @param  array $recordDetails
     * @return mixed
     * @throws NotImplementedException
     */
    public function create(array $recordDetails = array())
    {
        throw new NotImplementedException;
    }

    /**
     * Removes an existing record.
     *
     * @param  array $removeDetails
     * @return mixed
     * @throws NotImplementedException
     */
    public function remove(array $removeDetails = array())
    {
        throw new NotImplementedException;
    }

    /**
     * Updates an existing record.
     *
     * @param  int   $recordID
     * @param  array $newRecordDetails
     * @return mixed
     * @throws NotImplementedException
     */
    public function update($recordID, array $newRecordDetails)
    {
        throw new NotImplementedException;
    }

    /**
     * Gets the system usage analytics.
     *
     * @return mixed
     */
    public function getSystemUsageAnalytics()
    {
        $builder = DB::select(DB::raw("SELECT table_schema 'database_name',
    sum( data_length + index_length ) / 1024 / 1024 `size`,
    sum( data_free )/ 1024 / 1024 'free_size'
FROM information_schema.TABLES WHERE table_schema NOT IN ('mysql', 'information_schema', 'sys', 'world', 'performance_schema')
AND table_schema LIKE 'ndcounty_%'
OR table_schema LIKE 'nd_%'
OR table_schema IN ('genesis')
GROUP BY table_schema ORDER BY `size` DESC"));

        return $builder;
    }
}
