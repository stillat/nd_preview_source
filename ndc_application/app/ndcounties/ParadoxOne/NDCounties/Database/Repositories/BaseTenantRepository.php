<?php namespace ParadoxOne\NDCounties\Database\Repositories;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\MessageBag;
use Stillat\Database\Tenant\Repositories\TenantRepository;

abstract class BaseTenantRepository extends TenantRepository
{
    protected $errors = null;

    public function startTransaction()
    {
        DB::connection($this->getConnection())->beginTransaction();
    }

    public function commitTransaction()
    {
        DB::connection($this->getConnection())->commit();
    }

    public function rollbackTransaction()
    {
        DB::connection($this->getConnection())->rollback();
    }

    public function errors()
    {
        return $this->errors;
    }

    public function mergeErrors($additionalErrors)
    {
        if (is_null($this->errors)) {
            $this->errors = $additionalErrors;
        } else {
            $this->errors =
                new MessageBag(array_merge_recursive($this->errors->toArray(), $additionalErrors->toArray()));
        }
    }


    public function count()
    {
        return $this->table()->whereNull('deleted_at')->count();
    }

    public function table($table = null)
    {
        if ($table == null) {
            return DB::connection($this->getConnection())->table($this->table);
        }

        return DB::connection($this->getConnection())->table($table);
    }
}
