<?php namespace ParadoxOne\NDCounties\Database\Repositories\Resources\Traits;

use Illuminate\Database\Eloquent\ModelNotFoundException;

trait ConsumableAccessDirectTrait
{
    public function getConsumableByID($id)
    {
        $builder = $this->table()->select('id', 'name', 'code', 'description', 'cost', 'unit_id', 'tracking_inventory', 'desired_inventory_level', 'current_inventory_level', 'critical_inventory_level')->whereNull('deleted_at')
            ->where('id', '=', $id)->first();

        if ($builder == null) {
            throw new ModelNotFoundException;
        }

        return $builder;
    }
}
