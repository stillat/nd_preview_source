<?php namespace ParadoxOne\NDCounties\Database\Repositories\Resources;

use Carbon\Carbon;
use Exception;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\Facades\Input;
use ParadoxOne\NDCounties\Contracts\InventoryManagement\AdjustmentRepositoryInterface;
use ParadoxOne\NDCounties\Contracts\Resources\FuelRepositoryInterface;
use ParadoxOne\NDCounties\Database\Repositories\BaseTenantRepository;
use ParadoxOne\NDCounties\Database\Repositories\Resources\Traits\ConsumableAccessDirectTrait;
use ParadoxOne\NDCounties\IdentificationCode as IDCode;
use ParadoxOne\NDCounties\Settings\SortingSettingsManager;
use ParadoxOne\NDCounties\Traits\DefaultSortableTrait;
use ParadoxOne\NDCounties\Traits\UnTrashedQueryBuilderTrait;
use Service\Resources\Consumable;
use Service\Resources\Fuel;

class FuelRepository extends BaseTenantRepository implements FuelRepositoryInterface
{
    use ConsumableAccessDirectTrait;
    use DefaultSortableTrait;
    use UnTrashedQueryBuilderTrait;

    protected $table = 'consumables';

    protected $inventoryAdjustmentRepositoryInterface;

    /**
     * @param AdjustmentRepositoryInterface $inventoryAdjustmentRepositoryInterface
     */
    public function __construct(AdjustmentRepositoryInterface $inventoryAdjustmentRepositoryInterface)
    {
        parent::__construct();
        $this->inventoryAdjustmentRepositoryInterface = $inventoryAdjustmentRepositoryInterface;
    }

    private function getSortedBuilder()
    {
        $builder =
            $this->table()->select('id', 'name', 'code', 'description', 'cost', 'unit_id', 'tracking_inventory',
                                   'desired_inventory_level', 'current_inventory_level', 'critical_inventory_level', 'deleted_at')
                 ->where('consumable_context', '=', 1);
        $this->applyCostableSOrting(SortingSettingsManager::FUELS_TABLE_SETTING_NAME, $builder);

        return $builder;
    }

    public function getFuels()
    {
        return $this->getSortedBuilder()->whereNull('deleted_at');
    }

    public function getAll()
    {
        return $this->getFuels();
    }

    public function getAllWithTrashed()
    {
        return $this->getSortedBuilder();
    }

    public function getModelById($id)
    {
        Event::fire('fuel.accessed', array($id));

        return Consumable::where('consumable_context', '=', 1)->where('id', '=', $id)->firstOrFail();
    }

    public function create(array $fuelInfo = array())
    {
        $this->startTransaction();
        $fuel                     = new Consumable;
        $fuel->code               = IDCode::prepareFrom($fuelInfo['code']);
        $fuel->name               = $fuelInfo['name'];
        $fuel->description        = $fuelInfo['description'];
        $fuel->unit_id            = $fuelInfo['unit_id'];
        $fuel->cost               = $fuelInfo['cost'];
        $fuel->consumable_context = 1;

        if (array_key_exists('manage_inventory', $fuelInfo)) {
            $fuel->tracking_inventory       = true;
            $fuel->current_inventory_level  = $fuelInfo['initial_inventory_level'];
            $fuel->desired_inventory_level  = $fuelInfo['desired_inventory_level'];
            $fuel->critical_inventory_level = $fuelInfo['critical_inventory_level'];
        }

        $validationRules         = Consumable::$rules;
        $validationRules['code'] = Consumable::$rules['code'] . ',NULL,id,consumable_context,1,deleted_at,NULL';
        $validationRules['name'] = Consumable::$rules['name'] . ',NULL,id,consumable_context,1,deleted_at,NULL';

        if ($fuel->validate($validationRules)) {
            $possibleRestore = Consumable::withTrashed()->where('consumable_context', '=', '1')->where('code', '=', $fuel->code)->first();
            if ($possibleRestore != null) {
                $possibleRestore->name               = $fuelInfo['name'];
                $possibleRestore->description        = $fuelInfo['description'];
                $possibleRestore->unit_id            = $fuelInfo['unit_id'];
                $possibleRestore->cost               = $fuelInfo['cost'];
                $possibleRestore->deleted_at = null;
                $possibleRestore->consumable_context = 1;
                if (array_key_exists('manage_inventory', $fuelInfo)) {
                    $possibleRestore->tracking_inventory       = true;
                    $possibleRestore->current_inventory_level  = $fuelInfo['initial_inventory_level'];
                    $possibleRestore->desired_inventory_level  = $fuelInfo['desired_inventory_level'];
                    $possibleRestore->critical_inventory_level = $fuelInfo['critical_inventory_level'];
                }
                $validationRules         = Consumable::$rules;
                $validationRules['code'] =
                    Consumable::$rules['code'] . ',' . $possibleRestore->id . ',id,consumable_context,1,deleted_at,NULL';
                $saved = $possibleRestore->save($validationRules);
                Event::fire('fuel.created', array($possibleRestore));
                $this->inventoryAdjustmentRepositoryInterface->removeAdjustmentsForConsumable($possibleRestore->id);
                if ($possibleRestore->tracking_inventory) {
                    // Now we have to process an inventory adjustment.'
                    $adjustmentData                     = new \stdClass;
                    $adjustmentData->date_adjusted      = Carbon::now();
                    $adjustmentData->adjusted_by        = Auth::user()->id;
                    $adjustmentData->consumable_id      = $possibleRestore->id;
                    $adjustmentData->adjustment_context = 0;
                    $adjustmentData->adjustment_value   = 0;
                    $adjustmentData->adjustment_amount  = $possibleRestore->current_inventory_level;

                    $this->inventoryAdjustmentRepositoryInterface->addAdjustment($adjustmentData);
                }
                $this->commitTransaction();
                return $possibleRestore;
            }
        }

        if ($fuel->save($validationRules)) {
            Event::fire('fuel.created', array($fuel));

            if ($fuel->tracking_inventory) {
                // Now we have to process an inventory adjustment.'
                $adjustmentData                     = new \stdClass;
                $adjustmentData->date_adjusted      = Carbon::now();
                $adjustmentData->adjusted_by        = Auth::user()->id;
                $adjustmentData->consumable_id      = $fuel->id;
                $adjustmentData->adjustment_context = 0;
                $adjustmentData->adjustment_value   = 0;
                $adjustmentData->adjustment_amount  = $fuel->current_inventory_level;

                $this->inventoryAdjustmentRepositoryInterface->addAdjustment($adjustmentData);
            }

            $this->commitTransaction();

            return $fuel;
        } else {
            $this->errors = $fuel->errors();
        }
        $this->rollbackTransaction();

        return false;
    }

    public function update($id, array $fuelInfo = array())
    {
        $fuel = Consumable::findOrFail($id);
        $this->startTransaction();
        $fuel->code        = IDCode::prepareFrom($fuelInfo['code']);
        $fuel->name        = $fuelInfo['name'];
        $fuel->description = $fuelInfo['description'];
        $fuel->unit_id     = $fuelInfo['unit_id'];
        $fuel->cost        = $fuelInfo['cost'];

        if (array_key_exists('manage_inventory', $fuelInfo)) {
            $fuel->tracking_inventory       = true;
            $fuel->desired_inventory_level  = $fuelInfo['desired_inventory_level'];
            $fuel->critical_inventory_level = $fuelInfo['critical_inventory_level'];
        }

        $validationRules         = Consumable::$rules;
        $validationRules['code'] =
            Consumable::$rules['code'] . ',' . $fuel->id . ',id,consumable_context,1,deleted_at,NULL';
        $validationRules['name'] =
            Consumable::$rules['name'] . ',' . $fuel->id . ',id,consumable_context,1,deleted_at,NULL';

        if ($fuel->save($validationRules)) {
            Event::fire('fuel.updated', array($fuel));
            $this->commitTransaction();

            return $fuel;
        } else {
            $this->errors = $fuel->errors();
        }

        $this->rollbackTransaction();

        return false;
    }

    public function remove(array $removeContext = array())
    {
        $this->startTransaction();
        $removeData = array_strip_values($removeContext, [0,1]);
        try {
            foreach ($removeData as $fuel) {
                Event::fire('fuel.removed', [$fuel]);
            }
            Consumable::destroy($removeData);
            $this->commitTransaction();
            return true;
        } catch (Exception $e) {
        }
        $this->rollbackTransaction();
        foreach ($removeData as $fuel) {
            Event::fire('fuel.removed.removed', [$fuel]);
        }
        return false;


        //Fuel::destroy(intval($removeContext[0]));
    }

    public function search($searchTerms)
    {
        $includeTrashed = Input::has('it');

        if (Input::get('is', null) !== null) {
            return $this->table()->where('id', '=', Input::get('is'))->get();
        }

        if ($searchTerms === '//') {
            $builder = $this->table()->where('consumable_context', '=', 1);

            if (!$includeTrashed) {
                $builder->whereNull('deleted_at');
            }

            return $builder->get();
        }

        $builder = $this->table()
                        ->orWhere(function ($query) use ($searchTerms) {
                            $query->where('name', 'LIKE', '%' . $searchTerms . '%')
                                  ->orWhere('description', 'LIKE', '%' . $searchTerms . '%')
                                  ->orWhere('code', 'LIKE', '%' . $searchTerms . '%');
                        })
                        ->where(function ($query) {
                            $query->where('consumable_context', '=', 1);
                        });

        if (!$includeTrashed) {
            $builder->where(function ($query) {
                $query->whereNull('deleted_at');
            });
        }

        return $builder->get();
    }

    public function unTrash($fuels)
    {
        $this->startTransaction();

        if (!is_array($fuels)) {
            $fuels = (array)$fuels;
        }

        if (count($fuels) == 0) {
            return false;
        }

        try {
            $restoreData = $this->getRecordsThatCantBeRestored($this->table, $fuels, false, 'consumable_context', '=', '1');

            if (count($restoreData->recordsThatCanBeRestored) == 0) {
                // If we've reached this point, there are no records that can be restored
                // due to conflicts.
                Event::fire('fuel.restored.conflictsDetected', [$restoreData->similarRecords]);
                $this->rollbackTransaction();

                // Return true because of reasons.
                return true;
            }

            $this->table()->whereIn('id', $fuels)->where('consumable_context', '=', 1)->update([
                'deleted_at' => null
            ]);
            Event::fire('fuel.restored', [$fuels]);
            $this->commitTransaction();
            return true;
        } catch (\Exception $e) {
            dd($e->getMessage());
        }

        $this->rollbackTransaction();

        return false;
    }
}
