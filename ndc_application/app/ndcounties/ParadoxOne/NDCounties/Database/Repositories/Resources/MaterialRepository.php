<?php namespace ParadoxOne\NDCounties\Database\Repositories\Resources;

use Carbon\Carbon;
use Exception;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\Facades\Input;
use ParadoxOne\NDCounties\Contracts\InventoryManagement\AdjustmentRepositoryInterface;
use ParadoxOne\NDCounties\Contracts\Resources\MaterialRepositoryInterface;
use ParadoxOne\NDCounties\Database\Repositories\BaseTenantRepository;
use ParadoxOne\NDCounties\Database\Repositories\Resources\Traits\ConsumableAccessDirectTrait;
use ParadoxOne\NDCounties\IdentificationCode as IDCode;
use ParadoxOne\NDCounties\Settings\SortingSettingsManager;
use ParadoxOne\NDCounties\Traits\DefaultSortableTrait;
use ParadoxOne\NDCounties\Traits\UnTrashedQueryBuilderTrait;
use Service\Resources\Consumable;

class MaterialRepository extends BaseTenantRepository implements MaterialRepositoryInterface
{
    use ConsumableAccessDirectTrait;
    use DefaultSortableTrait;
    use UnTrashedQueryBuilderTrait;

    protected $table = 'consumables';

    protected $inventoryAdjustmentRepositoryInterface;

    public function __construct(AdjustmentRepositoryInterface $inventoryAdjustmentRepositoryInterface)
    {
        parent::__construct();
        $this->inventoryAdjustmentRepositoryInterface = $inventoryAdjustmentRepositoryInterface;
    }

    private function getSortedBuilder()
    {
        $builder = $this->table()->select('id', 'name', 'code', 'description', 'cost', 'unit_id', 'tracking_inventory',
                                          'desired_inventory_level', 'current_inventory_level',
                                          'critical_inventory_level', 'deleted_at')
                        ->where('consumable_context', '=', 2);
        $this->applyCostableSorting(SortingSettingsManager::MATERIALS_TABLE_SETTING_NAME, $builder);

        return $builder;
    }

    public function getMaterials()
    {
        return $this->getSortedBuilder()->whereNull('deleted_at');
    }

    public function getAll()
    {
        return $this->getMaterials();
    }

    public function getAllWithTrashed()
    {
        return $this->getSortedBuilder();
    }


    public function getModelById($id)
    {
        Event::fire('material.accessed', array($id));

        return Consumable::where('consumable_context', '=', 2)->where('id', '=', $id)->firstOrFail();
    }

    public function create(array $materialInfo = array())
    {
        $this->startTransaction();
        $material                     = new Consumable;
        $material->code               = IDCode::prepareFrom($materialInfo['code']);
        $material->name               = $materialInfo['name'];
        $material->description        = $materialInfo['description'];
        $material->unit_id            = $materialInfo['unit_id'];
        $material->cost               = $materialInfo['cost'];
        $material->consumable_context = 2;

        if (array_key_exists('manage_inventory', $materialInfo)) {
            $material->tracking_inventory       = true;
            $material->current_inventory_level  = $materialInfo['initial_inventory_level'];
            $material->desired_inventory_level  = $materialInfo['desired_inventory_level'];
            $material->critical_inventory_level = $materialInfo['critical_inventory_level'];
        }

        $validationRules         = Consumable::$rules;
        $validationRules['code'] = Consumable::$rules['code'] . ',NULL,id,consumable_context,2,deleted_at,NULL';
        $validationRules['name'] = Consumable::$rules['name'] . ',NULL,id,consumable_context,2,deleted_at,NULL';

        if ($material->validate($validationRules)) {
            $possibleRestore = Consumable::withTrashed()->where('consumable_context', '=', '2')->where('code', '=', $material->code)->first();
            if ($possibleRestore != null) {
                $possibleRestore->name               = $materialInfo['name'];
                $possibleRestore->description        = $materialInfo['description'];
                $possibleRestore->unit_id            = $materialInfo['unit_id'];
                $possibleRestore->cost               = $materialInfo['cost'];
                $possibleRestore->deleted_at = null;
                $possibleRestore->consumable_context = 2;
                if (array_key_exists('manage_inventory', $materialInfo)) {
                    $possibleRestore->tracking_inventory       = true;
                    $possibleRestore->current_inventory_level  = $materialInfo['initial_inventory_level'];
                    $possibleRestore->desired_inventory_level  = $materialInfo['desired_inventory_level'];
                    $possibleRestore->critical_inventory_level = $materialInfo['critical_inventory_level'];
                }
                $validationRules         = Consumable::$rules;
                $validationRules['code'] =
                    Consumable::$rules['code'] . ',' . $possibleRestore->id . ',id,consumable_context,1,deleted_at,NULL';
                $saved = $possibleRestore->save($validationRules);
                Event::fire('material.created', array($possibleRestore));
                $this->inventoryAdjustmentRepositoryInterface->removeAdjustmentsForConsumable($possibleRestore->id);
                if ($possibleRestore->tracking_inventory) {
                    // Now we have to process an inventory adjustment.'
                    $adjustmentData                     = new \stdClass;
                    $adjustmentData->date_adjusted      = Carbon::now();
                    $adjustmentData->adjusted_by        = Auth::user()->id;
                    $adjustmentData->consumable_id      = $possibleRestore->id;
                    $adjustmentData->adjustment_context = 0;
                    $adjustmentData->adjustment_value   = 0;
                    $adjustmentData->adjustment_amount  = $possibleRestore->current_inventory_level;

                    $this->inventoryAdjustmentRepositoryInterface->addAdjustment($adjustmentData);
                }
                $this->commitTransaction();
                return $possibleRestore;
            }
        }

        if ($material->save($validationRules)
        ) {
            Event::fire('material.created', array($material));

            if ($material->tracking_inventory) {
                // Now we have to process an inventory adjustment.'
                $adjustmentData                     = new \stdClass;
                $adjustmentData->date_adjusted      = Carbon::now();
                $adjustmentData->adjusted_by        = Auth::user()->id;
                $adjustmentData->consumable_id      = $material->id;
                $adjustmentData->adjustment_context = 0;
                $adjustmentData->adjustment_value   = 0;
                $adjustmentData->adjustment_amount  = $material->current_inventory_level;

                $this->inventoryAdjustmentRepositoryInterface->addAdjustment($adjustmentData);
            }
            $this->commitTransaction();

            return $material;
        } else {
            $this->errors = $material->errors();
        }

        $this->rollbackTransaction();

        return false;
    }

    public function update($id, array $materialInfo = array())
    {
        $material = Consumable::findOrFail($id);
        $this->startTransaction();
        $material->code        = IDCode::prepareFrom($materialInfo['code']);
        $material->name        = $materialInfo['name'];
        $material->description = $materialInfo['description'];
        $material->unit_id     = $materialInfo['unit_id'];
        $material->cost        = $materialInfo['cost'];

        if (array_key_exists('manage_inventory', $materialInfo)) {
            $material->tracking_inventory       = true;
            $material->desired_inventory_level  = $materialInfo['desired_inventory_level'];
            $material->critical_inventory_level = $materialInfo['critical_inventory_level'];
        }

        $validationRules         = Consumable::$rules;
        $validationRules['code'] =
            Consumable::$rules['code'] . ',' . $material->id . ',id,consumable_context,2,deleted_at,NULL';
        $validationRules['name'] =
            Consumable::$rules['name'] . ',' . $material->id . ',id,consumable_context,2,deleted_at,NULL';

        if ($material->save($validationRules)) {
            Event::fire('material.updated', array($material));
            $this->commitTransaction();

            return $material;
        } else {
            $this->errors = $material->errors();
        }

        $this->rollbackTransaction();

        return false;
    }

    public function remove(array $removeContext = array())
    {
        $this->startTransaction();
        $removeData = array_strip_values($removeContext, [0,1]);
        try {
            foreach ($removeData as $material) {
                Event::fire('material.removed', [$material]);
            }
            Consumable::destroy($removeData);
            $this->commitTransaction();
            return true;
        } catch (Exception $e) {
        }

        $this->rollbackTransaction();
        foreach ($removeData as $material) {
            Event::fire('material.removed.removed', [$material]);
        }

        return false;

        //Material::destroy(intval($removeContext[0]));
    }

    public function search($searchTerms)
    {
        $includeTrashed = Input::has('it');

        if (Input::get('is', null) !== null) {
            return $this->table()->where('id', '=', Input::get('is'))->get();
        }

        if ($searchTerms === '//') {
            $builder = $this->table()->where('consumable_context', '=', 2);

            if (!$includeTrashed) {
                $builder->whereNull('deleted_at');
            }

            return $builder->get();
        }

        $builder = $this->table()
                        ->orWhere(function ($query) use ($searchTerms) {
                            $query->where('name', 'LIKE', '%' . $searchTerms . '%')
                                  ->orWhere('description', 'LIKE', '%' . $searchTerms . '%')
                                  ->orWhere('code', 'LIKE', '%' . $searchTerms . '%');
                        })
                        ->where(function ($query) {
                            $query->where('consumable_context', '=', 2);
                        });

        if (!$includeTrashed) {
            $builder->where(function ($query) {
                $query->whereNull('deleted_at');
            });
        }

        return $builder->get();
    }

    public function unTrash($materials)
    {
        $this->startTransaction();

        if (!is_array($materials)) {
            $materials = (array)$materials;
        }

        if (count($materials) == 0) {
            return false;
        }

        try {
            $restoreData = $this->getRecordsThatCantBeRestored($this->table, $materials, false, 'consumable_context', '=', '2');

            if (count($restoreData->recordsThatCanBeRestored) == 0) {
                // If we've reached this point, there are no records that can be restored
                // due to conflicts.
                Event::fire('material.restored.conflictsDetected', [$restoreData->similarRecords]);
                $this->rollbackTransaction();

                // Return true because of reasons.
                return true;
            }

            $this->table()->whereIn('id', $materials)->where('consumable_context', '=', 2)->update([
                'deleted_at' => null
            ]);
            Event::fire('material.restored', [$materials]);
            $this->commitTransaction();
            return true;
        } catch (\Exception $e) {
        }

        $this->rollbackTransaction();

        return false;
    }

}
