<?php namespace ParadoxOne\NDCounties\Database\Repositories;

use Illuminate\Support\Facades\DB;
use ParadoxOne\NDCounties\Contracts\CountyMetaRepositoryInterface;

class CountyMetaRepository extends BaseTenantRepository implements CountyMetaRepositoryInterface
{
    protected $table = 'county_meta';

    /**
     * Updates the county's meta information.
     *
     * @param $highestFuelCount
     * @param $highestMaterialCount
     * @param $highestEquipmentCount
     * @return mixed
     */
    public function updateMetaInformation($highestFuelCount, $highestMaterialCount, $highestEquipmentCount)
    {
        $this->table()->update(array(
                                   'record_highest_fuel_count'      => $highestFuelCount,
                                   'record_highest_material_count'  => $highestMaterialCount,
                                   'record_highest_equipment_count' => $highestEquipmentCount
                               ));
    }

    /**
     * Get the actual meta information from the backing store.
     *
     * @return mixed
     */
    public function getActualMetaInformation()
    {
        $actualValues = DB::connection(\Tenant::getCurrentConnection())
                          ->select(DB::raw('SELECT coalesce((SELECT MAX(consumableMaterialCount) as maxMaterialCount FROM (( SELECT count(id) as consumableMaterialCount FROM work_consumables WHERE consumable_type = 4 GROUP BY work_entry_id ) as derivedMaterialTableHere)), 0) as highestMaterial, coalesce((SELECT MAX(consumableFuelCount) as maxFuelCount FROM (( SELECT count(id) as consumableFuelCount FROM work_consumables WHERE consumable_type = 3 GROUP BY work_entry_id ) as derivedFuelTableHere)), 0) as highestFuel, coalesce((SELECT MAX(consumableEquipmentCount) as maxEquipmentCount FROM (( SELECT count(id) as consumableEquipmentCount FROM work_consumables WHERE consumable_type = 2 GROUP BY work_entry_id ) as derivedEquipmentTableHere)), 0) as highestEquipment'));

        return (object)$actualValues[0];
    }


    /**
     * Returns the county's meta information.
     *
     * @return mixed
     */
    public function getMetaInformation()
    {
        $metaData = $this->table()->first();

        if ($metaData == null) {
            // We need to make one.
            $actualData = $this->getActualMetaInformation();
            $this->table()->insert([
                                       'record_highest_fuel_count'      => $actualData->highestFuel,
                                       'record_highest_material_count'  => $actualData->highestMaterial,
                                       'record_highest_equipment_count' => $actualData->highestEquipment
                                   ]);

            $metaData                                 = new \stdClass();
            $metaData->record_highest_fuel_count      = $actualData->highestFuel;
            $metaData->record_highest_material_count  = $actualData->highestMaterial;
            $metaData->record_highest_equipment_count = $actualData->highestEquipment;
        }

        return $metaData;
    }


    /**
     * Creates a new record.
     *
     * @param  array $recordDetails
     * @return mixed
     */
    public function create(array $recordDetails = array())
    {
        // TODO: Implement create() method.
    }

    /**
     * Removes an existing record.
     *
     * @param  array $removeDetails
     * @return mixed
     */
    public function remove(array $removeDetails = array())
    {
        // TODO: Implement remove() method.
    }

    /**
     * Updates an existing record.
     *
     * @param  int   $recordID
     * @param  array $newRecordDetails
     * @return mixed
     */
    public function update($recordID, array $newRecordDetails)
    {
        // TODO: Implement update() method.
    }
}
