<?php namespace ParadoxOne\NDCounties\Database\Repositories\Properties\Roads;

use Exception;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\Facades\Input;
use ParadoxOne\NDCounties\Contracts\Properties\Roads\SurfaceTypeRepositoryInterface;
use ParadoxOne\NDCounties\Database\Repositories\BaseTenantRepository;
use ParadoxOne\NDCounties\IdentificationCode as IDCode;
use Service\Properties\Road\SurfaceType;

class SurfaceTypeRepository extends BaseTenantRepository implements SurfaceTypeRepositoryInterface
{
    use \ParadoxOne\NDCounties\Traits\SearchableTrait;

    protected $table = 'property_road_surface_types';

    protected $roadSurfacePivotTable = 'property_roads_to_surface_types';

    private function pivotTable()
    {
        return DB::connection($this->getConnection())->table($this->roadSurfacePivotTable);
    }

    public function getSurfaceTypes()
    {
        return $this->table()->select('id', 'code', 'name', 'description')->orderBy('code', 'asc')->whereNull('deleted_at');
    }

    public function getSurfaceTypesWhereIn(array $ids)
    {
        return $this->table()->orderBy('code', 'asc')->whereIn('id', $ids)->get();
    }


    public function getModelById($id)
    {
        Event::fire('property.road.surfacetype.accessed', array($id));

        return SurfaceType::findOrFail($id);
    }

    public function addSurfaceToRoad($surfaceID, $roadID)
    {
        DB::connection(\Tenant::getCurrentConnection())->beginTransaction();

        try {
            // First, lets remove anything already set on this road property type.
            $this->removeSurfaceFromRoad($roadID);

            $this->pivotTable()->insert(array(
                                            'road_id'         => $roadID,
                                            'surface_type_id' => $surfaceID,
                                        ));
            DB::connection(\Tenant::getCurrentConnection())->commit();
            Event::fire('property.road.surfacetype.associated', array($surfaceID, $roadID));

            return true;
        } catch (Exception $e) {
            Event::fire('property.road.surfacetype.associated.failed', array($surfaceID, $roadID));
            DB::connection(\Tenant::getCurrentConnection())->rollback();

            return false;
        }
    }

    public function removeSurfaceFromRoad($road)
    {
        DB::connection(\Tenant::getCurrentConnection())->beginTransaction();
        try {
            $this->pivotTable()->where('road_id', '=', $road)->delete();
            DB::connection(\Tenant::getCurrentConnection())->commit();
            Event::fire('property.road.surfacetype.associated.removed', array($surfaceID, $roadID));

            return true;
        } catch (Exception $e) {
            Event::fire('property.road.surfacetype.associated.removed.failed', array($surfaceID, $roadID));
            DB::connection(\Tenant::getCurrentConnection())->rollback();

            return false;
        }
    }

    public function create(array $surfaceTypeInfo = array())
    {
        $surfaceType              = new SurfaceType;
        $surfaceType->name        = $surfaceTypeInfo['name'];
        $surfaceType->description = $surfaceTypeInfo['description'];
        $surfaceType->code        = IDCode::prepareFrom($surfaceTypeInfo['code']);

        $validationRules = SurfaceType::$rules;
        $validationRules['code'] = SurfaceType::$rules['code'].',NULL,id,deleted_at,NULL';

        DB::connection(\Tenant::getCurrentConnection())->beginTransaction();

        try {
            if ($surfaceType->save($validationRules)) {
                DB::connection(\Tenant::getCurrentConnection())->commit();
                Event::fire('property.road.surfacetype.created', array($surfaceType));

                return $surfaceType;
            } else {
                DB::connection(\Tenant::getCurrentConnection())->rollback();
                $this->errors = $surfaceType->errors();

                return false;
            }
        } catch (Exception $e) {
            DB::connection(\Tenant::getCurrentConnection())->rollback();
            return false;
        }
    }

    public function update($id, array $surfaceTypeInfo = array())
    {
        $surfaceType              = SurfaceType::findOrFail($id);
        $surfaceType->name        = $surfaceTypeInfo['name'];
        $surfaceType->description = $surfaceTypeInfo['description'];
        $surfaceType->code        = IDCode::prepareFrom($surfaceTypeInfo['code']);

        DB::connection(\Tenant::getCurrentConnection())->beginTransaction();

        try {
            $validationRules = SurfaceType::$rules;
            $validationRules['code'] = SurfaceType::$rules['code'].','.$surfaceType->id.',id,deleted_at,NULL';

            if ($surfaceType->updateUniques()) {
                Event::fire('property.road.surfaceType.updated', array($surfaceType));
                DB::connection(\Tenant::getCurrentConnection())->commit();

                return $surfaceType;
            } else {
                $this->errors = $surfaceType->errors();
            }
        } catch (Exception $e) {
        }

        DB::connection(\Tenant::getCurrentConnection())->rollback();

        return false;
    }

    public function remove(array $removeContext = array())
    {
        DB::connection(\Tenant::getCurrentConnection())->beginTransaction();

        try {
            if ($removeContext[0] != 0) {
                Event::fire('property.road.surfaceType.removed', array($removeContext[0]));
                $type             = $this->getModelById($removeContext[0]);
                $type->deleted_at = $type->freshTimeStamp();
                $result = $type->updateUniques();
                DB::connection(\Tenant::getCurrentConnection())->commit();

                return true;
            }
        } catch (Exception $e) {
        }

        DB::connection(\Tenant::getCurrentConnection())->rollback();
        Event::fire('property.road.surfaceType.removed.failed', array($removeContext[0]));

        return false;
    }
}
