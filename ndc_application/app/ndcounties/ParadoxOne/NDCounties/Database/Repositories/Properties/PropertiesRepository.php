<?php namespace ParadoxOne\NDCounties\Database\Repositories\Properties;

use Exception;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\Facades\Input;
use ParadoxOne\NDCounties\Contracts\Properties\PropertiesRepositoryInterface;
use ParadoxOne\NDCounties\Database\Adapters\AdapterManager;
use ParadoxOne\NDCounties\Database\Repositories\BaseTenantRepository;
use ParadoxOne\NDCounties\IdentificationCode as IDCode;
use ParadoxOne\NDCounties\Settings\SortingSettingsManager;
use ParadoxOne\NDCounties\Traits\AdvancedSearchPreparationTrait;
use ParadoxOne\NDCounties\Traits\DefaultSortableTrait;
use ParadoxOne\NDCounties\Traits\UnTrashedQueryBuilderTrait;
use Service\Properties\Property;
use stdClass;

class PropertiesRepository extends BaseTenantRepository implements PropertiesRepositoryInterface
{
    use DefaultSortableTrait, AdvancedSearchPreparationTrait;
    use UnTrashedQueryBuilderTrait;

    protected $table = 'properties';

    protected $propertyPivotTable = 'property_property';

    protected $manager;

    public function __construct(AdapterManager $manager)
    {
        parent::__construct();

        $this->manager = $manager;
    }

    public function pivotTable()
    {
        return DB::connection($this->getConnection())->table($this->propertyPivotTable);
    }

    public function getModelById($id)
    {
        Event::fire('property.accessed', array($id));
        if ($this->updateToRestore) {
            return Property::withTrashed()->with(array_merge($this->manager->getRequiredRelationships(), ['type.adapters', 'category', 'children']))
                ->findOrFail($id);
        }

        return Property::with(array_merge($this->manager->getRequiredRelationships(), ['type.adapters', 'category', 'children']))
            ->findOrFail($id);
    }

    private function applyPropertiesSorting(&$builder)
    {
        $sortSettings = App::make('sorting_work_entry');
        $sortSettings = json_decode($sortSettings->get(SortingSettingsManager::PROPERTIES_TABLE_SETTING_NAME), true);

        foreach ($sortSettings as $column => $settings) {
            switch ($column) {
                case 'code':
                    $this->applySortingSetting('properties.code', $settings, $builder);
                    break;
                case 'name':
                    $this->applySortingSetting('properties.name', $settings, $builder);
                    break;
                case 'desc':
                    $this->applySortingSetting('properties.description', $settings, $builder);
                    break;
                case 'last_updated':
                    $this->applySortingSetting('properties.updated_at', $settings, $builder);
                    break;
                case 'date_created':
                    $this->applySortingSetting('properties.created_at', $settings, $builder);
                    break;
                case 'type':
                    if ($settings == 'asc') {
                        $builder = $builder->orderBy('property_types.code', 'asc');
                    } elseif ($settings == 'desc') {
                        $builder = $builder->orderBy('property_types.code', 'desc');
                    }
                    break;
            }
        }
    }

    private function getSortedBuilder()
    {
        // ->with(['type','type.adapters', 'children'])
        // with($this->manager->getRequiredRelationships())
        $builder =
            Property::with(array_merge($this->manager->getRequiredRelationships(), ['category', 'type.adapters', 'children']))
                    ->join('property_types', 'property_types.id', '=', 'properties.property_type')
                    ->select(DB::raw('properties.id, properties.category_id, properties.property_type, properties.code, properties.description, properties.name, properties.exclusively_owned, properties.deleted_at, properties.created_at, properties.updated_at'));
        $this->applyPropertiesSorting($builder);

        return $builder;
    }

    private function getNormalBuilder()
    {
        $builder =
            Property::with(array_merge($this->manager->getRequiredRelationships(), ['category', 'type.adapters', 'children']))
                ->join('property_types', 'property_types.id', '=', 'properties.property_type')
                ->select(DB::raw('properties.id, properties.category_id, properties.property_type, properties.code, properties.description, properties.name, properties.exclusively_owned, properties.deleted_at, properties.created_at, properties.updated_at'));
        return $builder;
    }

    private $includeTrashed = false;

    private function getPropertiesForSearch()
    {
        if ($this->includeTrashed) {
            return $this->getPropertiesWithTrashed();
        }

        return $this->getNormalBuilder();
    }

    public function getProperties()
    {
        // So sneaky.
        if ($this->includeTrashed) {
            return $this->getPropertiesWithTrashed();
        }

        return $this->getSortedBuilder()->whereNull('properties.deleted_at');
    }

    public function getAll()
    {
        return $this->getProperties();
    }


    public function getPropertiesWithTrashed()
    {
        $builder =
            Property::withTrashed()->with(array_merge($this->manager->getRequiredRelationships(), ['category', 'type.adapters', 'children']))
                ->join('property_types', 'property_types.id', '=', 'properties.property_type')
                ->select(DB::raw('properties.id, properties.category_id, properties.property_type, properties.code, properties.description, properties.name, properties.exclusively_owned, properties.deleted_at, properties.created_at, properties.updated_at'));
        $this->applyPropertiesSorting($builder);

        return $builder;
    }

    public function getAllWithTrashed()
    {
        return $this->getPropertiesWithTrashed();
    }

    public function getPropertiesIn(array $properties)
    {
        return Property::with(array_merge($this->manager->getRequiredRelationships(), ['type.adapters']))
                       ->whereIn('id', $properties)->withTrashed()->get();
    }


    public function getProperty($id)
    {
        Event::fire('property.accessed', array($id));

        return $this->getModelById($id);
    }

    public function unTrash($properties)
    {
        $this->startTransaction();

        if (!is_array($properties)) {
            $activities = (array)$properties;
        }

        if (count($properties) == 0) {
            return false;
        }

        try {
            $restoreData = $this->getRecordsThatCantBeRestored($this->table, $properties);
            if (count($restoreData->recordsThatCanBeRestored) == 0) {
                // If we've reached this point, there are no records that can be restored
                // due to conflicts.
                Event::fire('property.restored.conflictsDetected', [$restoreData->similarRecords]);
                $this->rollbackTransaction();

                // Return true because of reasons.
                return true;
            }

            $this->table()->whereIn('id', $properties)->update([
                'deleted_at' => null
            ]);
            Event::fire('property.restored', [$properties]);
            $this->commitTransaction();
            return true;
        } catch (\Exception $e) {
        }

        $this->rollbackTransaction();

        return false;
    }

    protected $updateToRestore = false;

    public function create(array $propertyInfo = array())
    {
        $property                = new Property;
        $property->name          = $propertyInfo['name'];
        $property->description   = $propertyInfo['description'];
        $property->code          = IDCode::prepareFrom($propertyInfo['code']);
        $property->property_type = $propertyInfo['property_type'];
        $property->category_id   = $propertyInfo['category_id'];

        // Create the handler object (mainly be used below)
        $handlerObject       = new stdClass;
        $handlerObject->data = $propertyInfo;
        $handlerObject->id   = 0;

        // We need to get the adapters for the given property type for the
        // adapter handlers below.
        $typesRepository       =
            App::make('ParadoxOne\NDCounties\Contracts\Properties\PropertyTypesRepositoryInterface');
        $temporaryPropertyType = $typesRepository->getModelById($propertyInfo['property_type']);

        $typesRepository = null;
        unset($typesRepository);

        $typeAdapters = $temporaryPropertyType->adapters->lists('adapter');

        $temporaryPropertyType = null;
        unset($temporaryPropertyType);

        $this->startTransaction();

        // If the exclusively_owned flag is set, we will indicate this in the database
        // by setting it on the model.
        if (isset($propertyInfo['exclusively_owned']) and $propertyInfo['exclusively_owned']) {
            $property->exclusively_owned = true;
        }

        $adapters = $this->manager->getAdapters();

        try {
            $validationRules         = Property::$rules;
            $validationRules['code'] = Property::$rules['code'] . ',NULL,id,property_type,' .
                                       $propertyInfo['property_type'] . ',deleted_at,NULL';

            if ($property->validate($validationRules)) {
                $possibleRestore = Property::withTrashed()->where('property_type', '=', $property->property_type)->where('code', '=', $property->code)->first();
                if ($possibleRestore != null) {
                    $this->rollbackTransaction();
                    $this->updateToRestore = true;
                    return $this->update($possibleRestore->id, $propertyInfo);
                }
            }

            if ($property->save($validationRules)
            ) {
                Event::fire('property.created', array($property));

                $adapterRuns       = array();
                $handlerObject->id = $property->id;

                foreach ($adapters as $adapter) {
                    if (in_array($adapter->getAdapterNumericIdentifier(), $typeAdapters)) {
                        // Since the adapter matches the current property type, we can
                        // attempt to run the appropriate handlers.
                        $handler = $adapter->getCreatePropertyHandler();


                        $success                                              = $handler->handle($handlerObject);
                        $adapterRuns[$adapter->getAdapterNumericIdentifier()] = $success;

                        if ($success === false) {
                            $this->mergeErrors($handler->errors());
                        }
                    }
                }

                if (in_array(false, $adapterRuns)) {
                    // If there was an error with the adapter handlers, we are going to bail.
                    $this->rollbackTransaction();

                    return false;
                }

                $this->commitTransaction();
                return $property;
            } else {
                // We know there are errors with the property itself, but want to
                // also display any errors with the adapters.
                foreach ($adapters as $adapter) {
                    // typeAdapters
                    if (in_array($adapter->getAdapterNumericIdentifier(), $typeAdapters)) {
                        // Let's do a dry run of the adapters, even though we are not going to be
                        // saving anything.
                        $dryHandler    = $adapter->getCreatePropertyHandler();
                        $dryRunSuccess = $dryHandler->handle($handlerObject, true);

                        if ($dryRunSuccess === false) {
                            $this->mergeErrors($dryHandler->errors());
                        }
                    }
                }
                $this->mergeErrors($property->errors());
            }
        } catch (Exception $e) {
            dd($e);
        }

        $this->rollbackTransaction();
        return false;
    }

    public function update($id, array $propertyInfo = array())
    {
        $property = $this->getModelById($id);
        $property->name        = $propertyInfo['name'];
        $property->description = $propertyInfo['description'];
        $property->code        = IDCode::prepareFrom($propertyInfo['code']);
        $property->category_id = $propertyInfo['category_id'];
        //$property->property_type = $propertyInfo['property_type'];

        if ($this->updateToRestore) {
            $property->deleted_at = null;
        }

        $handlerObject       = new stdClass;
        $handlerObject->data = $propertyInfo;
        $handlerObject->id   = $id;

        $this->startTransaction();

        $adapters     = $this->manager->getAdapters();
        $typeAdapters = $property->type->adapters->lists('id');

        try {
            $validationRules         = Property::$rules;
            $validationRules['code'] = Property::$rules['code'] . ',' . $property->id . ',id,property_type,' .
                                       $property->property_type . ',deleted_at,NULL';

            if ($property->save($validationRules)
            ) {
                if ($this->updateToRestore) {
                    Event::fire('property.created', array($property));
                } else {
                    Event::fire('property.updated', array($property));
                }

                $adapterRuns = array();
                foreach ($adapters as $adapter) {
                    if (in_array($adapter->getAdapterNumericIdentifier(), $typeAdapters)) {
                        $handler                                              = $adapter->getUpdatePropertyHandler();
                        $success                                              = $handler->handle($handlerObject);
                        $adapterRuns[$adapter->getAdapterNumericIdentifier()] = $success;

                        if ($success === false) {
                            $this->mergeErrors($handler->errors());
                        }
                    }
                }

                if (in_array(false, $adapterRuns)) {
                    $this->rollbackTransaction();

                    return false;
                }

                $this->commitTransaction();
                return $property;
            } else {
                // Again, we know there are errors with the property itself, but we want to
                // also display any errors with the adapters to the user.
                foreach ($adapters as $adapter) {
                    if (in_array($adapter->getAdapterNumericIdentifier(), $typeAdapters)) {
                        // Let's do a dry run of the adapters, even though we are not going to be
                        // saving any changes.
                        $dryHandler    = $adapter->getUpdatePropertyHandler();
                        $dryRunSuccess = $dryHandler->handle($handlerObject, true);

                        if ($dryRunSuccess === false) {
                            $this->mergeErrors($dryHandler->error());
                        }
                    }
                }

                $this->mergeErrors($property->errors());
            }
        } catch (Exception $e) {
        }
        $this->rollbackTransaction();

        return false;
    }

    public function remove(array $removeContext = array())
    {
        $overallSuccess = true;
        $removeContext = array_strip_values($removeContext, [0,1]);
        $adapters = $this->manager->getAdapters();
        // We will attempt to remove each property one at a time.
        foreach ($removeContext as $propertyID) {
            $this->startTransaction();
            try {
                // Here we are implementing the soft deleting mechanisms
                // our selves. This helps to avoid the issues of knowing
                // when Eloquent is soft-deleting vs force deleting.
                $property = $this->getModelById($propertyID);
                Property::destroy($propertyID);

                $typeAdapters              = $property->type->adapters->lists('id');
                $handlerObject             = new stdClass;
                $handlerObject->id         = $property->id;
                $handlerObject->trueDelete = false;

                $adapterRuns = array();

                foreach ($adapters as $adapter) {
                    if (in_array($adapter->getAdapterNumericIdentifier(), $typeAdapters)) {
                        $handler                                              = $adapter->getDeletePropertyHandler();
                        $success                                              = $handler->handle($handlerObject);
                        $adapterRuns[$adapter->getAdapterNumericIdentifier()] = $success;

                        if ($success === false) {
                            $this->mergeErrors($handler->errors());
                        }
                    }
                }

                if (in_array(false, $adapterRuns)) {
                    $overallSuccess = false;
                    $this->rollbackTransaction();
                    continue;
                }
                Event::fire('property.removed', [$propertyID]);
                $property->save();
                $this->commitTransaction();
                continue;
            } catch (\Exception $e) {
                Event::fire('property.removed.failed', [$propertyID]);
                $this->rollbackTransaction();
            }
        }
        return $overallSuccess;
    }

    public $duplicateChildrenDuringAttachment = false;

    public function attachChildProperty($parentPropertyId, $childPropertyId)
    {
        DB::connection(\Tenant::getCurrentConnection())->beginTransaction();

        try {
            $childProperties = $childPropertyId;

            $existingChildren = $this->table('property_property')->where('parent_property_id', '=', $parentPropertyId)
                                     ->lists('child_property_id');


            if (is_array($childPropertyId)) {
                $childProperties = null;

                foreach ($childPropertyId as $key => $value) {
                    $value = substr($value, 5);
                    if ($value !== '0' and $value !== $parentPropertyId and !in_array($value, $existingChildren)) {
                        $childProperties[] = $value;
                    } elseif (in_array($value, $existingChildren)) {
                        $this->duplicateChildrenDuringAttachment = true;
                    }
                }
            }

            // This function just attaches the child property to the
            // parent property.
            if ($childProperties !== null and count($childProperties) > 0) {
                $property = $this->getModelById($parentPropertyId);
                $property->children()->attach($childProperties);
                DB::connection(\Tenant::getCurrentConnection())->commit();
            }

            return true;
        } catch (Exception $e) {
            DB::connection(\Tenant::getCurrentConnection())->rollback();
            d($e);
            exit;

            return false;
        }
    }

    public function detachChildProperty($parentPropertyId, $removeChildProperty)
    {
        // This function just detaches the child property to the
        // parent property.
        $property = $this->getModelById($parentPropertyId);
        $property->children()->detach($removeChildProperty);
    }

    public function createChildProperty($parentPropertyId, array $childPropertyData)
    {
        // Just call the create method for normal properties. All that is going to be
        // different is that we are going to set the exclusively_owned flag.
        $childProperty = $this->create(array_merge($childPropertyData, array('exclusively_owned' => true)));

        if ($childProperty !== false) {
            // If the child property was successfully created, we will just go ahead and attach it.
            $this->attachChildProperty($parentPropertyId, $childProperty->id);
            Event::fire('property.child.created', array($parentPropertyId, $childProperty));

            return $childProperty;
        } else {
            $this->errors = $this->errors();

            return false;
        }
    }

    public function updateChildProperty($childPropertyId, array $updateInformation)
    {
        // Just call the create method for normal properties. All that is going to be
        // different is that we are going to set the exclusively_owned flag.
        $updatedChildProperty = $this->create(array_merge($childPropertyData, array('exclusively_owned' => true)));

        if ($updateChildProperty !== false) {
            Event::fire('property.child.updated', array($updatedChildProperty));

            return $updatedChildProperty;
        } else {
            $this->errors = $this->errors();

            return false;
        }
    }

    public function removeChildProperty(array $removeContext)
    {
        // We first have to detach the child property.
        $this->detachChildFromAll($removeContext[[0]]);

        Event::fire('property.child.removed', $removeContext);

        // We can just call the remove method to remove this property.
        return $this->remove($removeContext);
    }

    public function detachChildFromAll($childPropertyId)
    {
        Event::fire('property.child.detached', array($childPropertyId));

        return $this->pivotTable()->where('child_property_id')->delete();
    }

    public function removeAllChildrenFromParent($parentPropertyId)
    {
        Event::fire('property.child.detached', array($parentPropertyId));

        return $this->pivotTable()->where('parent_property_id')->delete();
    }

    /**
     * Filters a list of properties and removes any children property that exists in the exclusions array.
     *
     * @param array $exclusions
     * @param       $propertiesList
     */
    protected function filterChildrenPropertyExclusions(array $exclusions, $propertiesList)
    {
        foreach ($propertiesList as $propertyKey => $propertyValue) {
            foreach ($propertyValue->children as $childKey => $childPropertyValue) {
                if (in_array($childPropertyValue->id, $exclusions, true) == true) {
                    unset($propertyValue->children[$childKey]);
                }
            }
        }

        return $propertiesList;
    }

    public function searchRoads($searchTerms)
    {
        if ($searchTerms = '//') {
            $subSearchTerms = $this->getProperties()->where('exclusively_owned', '=', '0')->where('property_type', '=', 1);
            return $subSearchTerms->get();
        }

        $subSearchTerms = $this->getPropertiesForSearch()
            ->where('exclusively_owned', '=', '0')->where('property_type', '=', 1)
            ->where(function ($query) use ($searchTerms) {
                $query->where('properties.name', 'LIKE', $this->prepareSearch($searchTerms))
                    ->orWhere('properties.description', 'LIKE', $this->prepareSearch($searchTerms))
                    ->orWhere('properties.code', 'LIKE', $this->prepareSearch($searchTerms))
                    ->orWhere(DB::raw('REPLACE(properties.code, \'-\', \'\')'), '=', $searchTerms);
            })->orderByRaw('LENGTH(properties.code)')->limit(10);

        return $subSearchTerms->get();
    }

    public function searchGeneric($searchTerms)
    {
        if ($searchTerms = '//') {
            $subSearchTerms = $this->getProperties()->where('exclusively_owned', '=', '0')->where('property_type', '=', 0);
            return $subSearchTerms->get();
        }

        $subSearchTerms = $this->getPropertiesForSearch()
            ->where('exclusively_owned', '=', '0')->where('property_type', '=', 0)
            ->where(function ($query) use ($searchTerms) {
                $query->where('properties.name', 'LIKE', $this->prepareSearch($searchTerms))
                    ->orWhere('properties.description', 'LIKE', $this->prepareSearch($searchTerms))
                    ->orWhere('properties.code', 'LIKE', $this->prepareSearch($searchTerms))
                    ->orWhere(DB::raw('REPLACE(properties.code, \'-\', \'\')'), '=', $searchTerms);
            })->orderByRaw('LENGTH(properties.code)')->limit(10);

        return $subSearchTerms->get();
    }

    public function searchEquipmentUnits($searchTerms)
    {
        if ($searchTerms = '//') {
            $subSearchTerms = $this->getProperties()->where('exclusively_owned', '=', '0')->where('property_type', '=', 2);
            return $subSearchTerms->get();
        }

        $subSearchTerms = $this->getPropertiesForSearch()
            ->where('exclusively_owned', '=', '0')->where('property_type', '=', 2)
            ->where(function ($query) use ($searchTerms) {
                $query->where('properties.name', 'LIKE', $this->prepareSearch($searchTerms))
                    ->orWhere('properties.description', 'LIKE', $this->prepareSearch($searchTerms))
                    ->orWhere('properties.code', 'LIKE', $this->prepareSearch($searchTerms))
                    ->orWhere(DB::raw('REPLACE(properties.code, \'-\', \'\')'), '=', $searchTerms);
            })->orderByRaw('LENGTH(properties.code)')->limit(10);

        return $subSearchTerms->get();
    }

    public function search($searchTerms, $limitType = 0)
    {
        $this->includeTrashed = Input::has('it');
        $findOnlyEquipmentUnits = false;
        $findOnlyProperties     = false;

        if ($limitType == 0) {
            if ($searchTerms === '/u') {
                $findOnlyEquipmentUnits = true;
            }

            if ($searchTerms === '/p') {
                $findOnlyProperties = true;
            }
        }

        if (Input::has('context') and Input::get('context', 0) == 0) {
            if (Input::get('is', null) !== null) {
                return [$this->getModelById(Input::get('is'))];
                // return $this->table()->where('id', '=', Input::get('is'))->get();
            }
        }

        $excludes = explode(',', Input::get('exclude', ''));

        if (count($excludes) > 1) {
            $excludes = array_filter($excludes);

            // This pass will remove any numeric values that might have snuck in and will also convert
            // any prefixed values.
            foreach ($excludes as $key => $excludedItem) {
                if (is_numeric($excludedItem) || $excludedItem == '') {
                    unset($excludes[$key]);
                } else {
                    $nameWithoutPropertyPrefix = substr($excludedItem, 5);

                    if (in_array(intval($nameWithoutPropertyPrefix), $excludes, true) == false) {
                        $excludes[$key] = intval($nameWithoutPropertyPrefix);
                    }
                }
            }

            // This pass will remove any strings that remain in the exclusion array.
            foreach ($excludes as $key => $excludedItem) {
                if (is_numeric($excludedItem) == false) {
                    unset($excludes[$key]);
                }
            }

            // This will add the default properties to the exclusion list.
            if (in_array(0, $excludes, true) == false) {
                $excludes[] = 0;
            }

            if (in_array(1, $excludes, true) == false) {
                $excludes[] = 1;
            }
        }

        if ($findOnlyEquipmentUnits) {
            $subSearchTerms =
                $this->getProperties()->where('exclusively_owned', '=', '0')->where('property_type', '=', 2);

            if (count($excludes) > 1) {
                $subSearchTerms = $subSearchTerms->whereNotIn('properties.id', $excludes);
            }

            return $subSearchTerms->get();
        }

        if ($findOnlyProperties) {
            $subSearchTerms =
                $this->getProperties()->where('exclusively_owned', '=', '0')->where('property_type', '<>', 2);

            if (count($excludes) > 1) {
                $subSearchTerms = $subSearchTerms->whereNotIn('properties.id', $excludes);

                return $this->filterChildrenPropertyExclusions($excludes, $subSearchTerms->get());
            }

            return $subSearchTerms->get();
        }


        if ($searchTerms === '//') {
            // return $this->table()->whereNull('deleted_at')->where('exclusively_owned', '=', '0')->whereNotIn('id', $excludes)->get();

            $subSearchTerms = $this->getProperties()->where('exclusively_owned', '=', '0');

            if (count($excludes) > 1) {
                $subSearchTerms = $subSearchTerms->whereNotIn('properties.id', $excludes);

                return $this->filterChildrenPropertyExclusions($excludes, $subSearchTerms->get());
            }

            if ($limitType != 0) {
                if ($limitType == 1) {
                    $subSearchTerms->where(function($query) {
                       $query->whereIn('property_type', [0,1]);
                    });
                } else {
                    $subSearchTerms->where('property_type', '=', $limitType);
                }
            }

            return $subSearchTerms->get();
        }

        $subSearchTerms = $this->getPropertiesForSearch()
                               ->where(function ($query) use ($searchTerms) {
                                   $query->where('properties.name', 'LIKE', $this->prepareSearch($searchTerms))
                                         ->orWhere('properties.description', 'LIKE', $this->prepareSearch($searchTerms))
                                         ->orWhere('properties.code', 'LIKE', $this->prepareSearch($searchTerms))
                                   ->orWhere(DB::raw('REPLACE(properties.code, \'-\', \'\')'), '=', $searchTerms);
                               })->orderByRaw('LENGTH(properties.code)')->limit(10);

        if ($limitType != 0) {
            if ($limitType == 1) {
                $subSearchTerms->where(function($query) {
                    $query->whereIn('property_type', [0,1]);
                });
            } else {
                $subSearchTerms->where('property_type', '=', $limitType);
            }
        }

        if (count($excludes) > 1) {
            $subSearchTerms = $subSearchTerms->whereNotIn('properties.id', $excludes);

            return $this->filterChildrenPropertyExclusions($excludes, $subSearchTerms->get());
        }
        return $subSearchTerms->get();
    }

    /**
     * Un-nests a global property from it's parent property.
     *
     * @param $parentPropertyID
     * @param $nestedPropertyID
     * @return mixed
     */
    public function unNestGlobalProperty($parentPropertyID, $nestedPropertyID)
    {
        $this->startTransaction();

        try {
            $this->table('property_property')->where('parent_property_id', '=', $parentPropertyID)
                 ->where('child_property_id', '=', $nestedPropertyID)->delete();

            $this->commitTransaction();

            return true;
        } catch (\Exception $e) {
        }

        $this->rollbackTransaction();

        return false;
    }

    /**
     * Un-nests an exclusive property and makes it into a global property.
     *
     * @param $nestedPropertyID
     * @return mixed
     */
    public function unNestExclusiveProperty($nestedPropertyID)
    {
        $this->startTransaction();

        try {
            $this->table('property_property')->where('child_property_id', '=', $nestedPropertyID)->delete();
            $this->table('properties')->where('id', '=', $nestedPropertyID)->update(['exclusively_owned' => false]);

            $this->commitTransaction();
            return true;
        } catch (\Exception $e) {
        }

        $this->rollbackTransaction();

        return false;
    }
}
