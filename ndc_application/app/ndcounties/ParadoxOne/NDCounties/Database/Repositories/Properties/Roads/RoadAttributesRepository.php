<?php namespace ParadoxOne\NDCounties\Database\Repositories\Properties\Roads;

use Exception;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\Facades\Input;
use ParadoxOne\NDCounties\Contracts\Properties\Roads\RoadAttributesRepositoryInterface;
use ParadoxOne\NDCounties\Database\Repositories\BaseTenantRepository;
use Service\Properties\Road\RoadAttributes;

class RoadAttributesRepository extends BaseTenantRepository implements RoadAttributesRepositoryInterface
{
    protected $table = 'property_road_properties';

    protected $roadID = 0;

    public function setRoad($roadID)
    {
        $this->roadID = $roadID;
    }

    public function isValid(array $data = array())
    {
        $attributes                  = new RoadAttributes;
        $attributes->road_id         = $this->roadID;
        $attributes->surface_type_id = $data['surface_type_id'];
        $attributes->road_length     = $data['road_length'];
        $attributes->unit_id         = $data['unit_id'];

        $success = $attributes->validate();

        if ($success == true) {
            return true;
        }

        $this->errors = $attributes->errors();

        return false;
    }

    public function create(array $data = array())
    {
        $attributes                  = new RoadAttributes;
        $attributes->road_id         = $this->roadID;
        $attributes->surface_type_id = $data['surface_type_id'];
        $attributes->road_length     = $data['road_length'];
        $attributes->unit_id         = $data['unit_id'];

        DB::connection(\Tenant::getCurrentConnection())->beginTransaction();
        try {
            if ($attributes->save()) {
                DB::connection(\Tenant::getCurrentConnection())->commit();
                Event::fire('property.road.attributes.created', array($attributes));

                return $attributes;
            } else {
                $this->errors = $attributes->errors();
            }
        } catch (Exception $e) {
        }

        $this->errors = $attributes->errors();

        Event::fire('property.road.attributes.failed', array($data));
        DB::connection(\Tenant::getCurrentConnection())->rollback();

        return false;
    }

    public function update($id, array $data = array())
    {
        $attributes = RoadAttributes::where('road_id', $this->roadID)->firstOrFail();

        if ($attributes == null) {
            throw new ModelNotFoundException;
        }

        $attributes->surface_type_id = $data['surface_type_id'];
        $attributes->road_length     = $data['road_length'];
        $attributes->unit_id         = $data['unit_id'];

        DB::connection(\Tenant::getCurrentConnection())->beginTransaction();

        try {
            if ($attributes->updateUniques()) {
                Event::fire('property.road.attributes.updated', array($attributes));
                DB::connection(\Tenant::getCurrentConnection())->commit();

                return $attributes;
            } else {
                $this->errors = $attributes->errors();
            }
        } catch (Exception $e) {
        }


        DB::connection(\Tenant::getCurrentConnection())->rollback();
        Event::fire('property.road.attributes.updated.failed', array($data));

        return false;
    }

    public function remove(array $removeContext = array())
    {
        DB::connection(\Tenant::getCurrentConnection())->beginTransaction();

        try {
            if ($removeContext[0] != 0) {
                if (array_key_exists(1, $removeContext)) {
                    // This is the part that will be used when the system is trashing
                    // previously soft-deleted road records.
                    Event::fire('property.road.attributes.removed', array($removeContext[0]));
                    RoadAttributes::where('road_id', '=', $removeContext[0])->delete();
                    DB::connection(\Tenant::getCurrentConnection())->commit();

                    return true;
                } else {
                    // Just return true. This is used when a user is only soft-deleting
                    // the parent road record.
                    return true;
                }
            }
        } catch (Exception $e) {
        }

        DB::connection(\Tenant::getCurrentConnection())->rollback();
        Event::fire('property.road.attributes.removed.failed', array($removeContext[0]));

        return false;
    }
}
