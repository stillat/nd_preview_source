<?php namespace ParadoxOne\NDCounties\Database\Repositories\Properties;

use Exception;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Event;
use ParadoxOne\NDCounties\Contracts\Properties\PropertyTypesRepositoryInterface;
use ParadoxOne\NDCounties\Database\Repositories\BaseTenantRepository;
use ParadoxOne\NDCounties\IdentificationCode as IDCode;
use Service\Properties\PropertyType;

class PropertyTypeRepository extends BaseTenantRepository implements PropertyTypesRepositoryInterface
{
    protected $table = 'property_types';

    public function getPropertyTypes()
    {
        return PropertyType::with(['adapters'])->orderBy('code', 'asc');

        //return $this->table()->select('id', 'code', 'name', 'description')->orderBy('code', 'asc');
    }

    public function getPropertyTypesForReportScope()
    {
        return [
            0 => 'General Road (Most Common)',
            1 => 'Roads (with Length Data)',
            2 => 'Equipment Repairs'
        ];
    }

    public function getModelById($id)
    {
        Event::fire('property.type.accessed', array($id));

        return PropertyType::with(['adapters'])->findOrFail($id);
    }

    public function create(array $propertyTypeInfo = array())
    {
        $propertyType              = new PropertyType;
        $propertyType->name        = $propertyTypeInfo['name'];
        $propertyType->description = $propertyTypeInfo['description'];
        $propertyType->code        = IDCode::prepareFrom($propertyTypeInfo['code']);

        $adapters = $propertyTypeInfo['propertyAdapters'];

        DB::connection(\Tenant::getCurrentConnection())->beginTransaction();

        if ($propertyType->save()) {

            // START: Code to insert the relationships between the properties and the adapters.
            $adaptersToInsert = [];

            foreach ($adapters as $adapter) {
                $adaptersToInsert[] = ['property_id' => $propertyType->id, 'adapter' => $adapter];
            }

            DB::connection(\Tenant::getCurrentConnection())->table('property_adapters')->insert($adaptersToInsert);

            // END: Code to insert adapter/properties relationships.

            // Commit the database changes.
            DB::connection(\Tenant::getCurrentConnection())->commit();

            Event::fire('property.type.created', array($propertyType));

            return $propertyType;
        } else {
            DB::connection(\Tenant::getCurrentConnection())->rollback();
            $this->errors = $propertyType->errors();

            return false;
        }
    }

    public function update($id, array $propertyTypeInfo = array())
    {
        $propertyType              = PropertyType::findOrFail($id);
        $propertyType->name        = $propertyTypeInfo['name'];
        $propertyType->description = $propertyTypeInfo['description'];
        $propertyType->code        = IDCode::prepareFrom($propertyTypeInfo['code']);


        DB::connection(\Tenant::getCurrentConnection())->beginTransaction();

        try {
            if ($propertyType->updateUniques()) {
                DB::connection(\Tenant::getCurrentConnection())->commit();
                Event::fire('property.type.updated', array($propertyType));

                return $propertyType;
            }
        } catch (Exception $e) {
        }

        DB::connection(\Tenant::getCurrentConnection())->rollback();
        $this->errors = $propertyType->errors();

        return false;
    }

    public function remove(array $removeContext = array())
    {
        DB::connection(\Tenant::getCurrentConnection())->beginTransaction();

        try {
            if ($removeContext[0] != 0) {
                PropertyType::destroy($removeContext[0]);
                Event::fire('property.type.removed', array($removeContext[0]));
                DB::connection(\Tenant::getCurrentConnection())->commit();

                return true;
            }
        } catch (Exception $e) {
        }

        DB::connection(\Tenant::getCurrentConnection())->rollback();
        Event::fire('property.type.removed.failed', array($removeContext[0]));

        return false;

        //PropertyType::destroy(intval($removeContext[0]));
    }
}
