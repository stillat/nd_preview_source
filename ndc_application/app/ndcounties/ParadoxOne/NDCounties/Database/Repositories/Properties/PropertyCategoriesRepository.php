<?php namespace ParadoxOne\NDCounties\Database\Repositories\Properties;

use Illuminate\Support\Facades\Event;
use Illuminate\Support\Facades\DB;
use ParadoxOne\NDCounties\Contracts\Properties\PropertyCategoriesRepositoryInterface;
use ParadoxOne\NDCounties\Database\Repositories\BaseTenantRepository;
use Service\Properties\Category;

class PropertyCategoriesRepository extends BaseTenantRepository implements PropertyCategoriesRepositoryInterface
{
    protected $table = 'property_categories';

    /**
     * Creates a new record.
     *
     * @param  array $recordDetails
     * @return mixed
     */
    public function create(array $recordDetails = array())
    {
        $this->startTransaction();

        $category                = new Category;
        $category->category_name = $recordDetails['category_name'];

        $validationRules                  = Category::$rules;
        $validationRules['category_name'] = Category::$rules['category_name'];


        if ($category->save($validationRules)) {
            Event::fire('properties.category.created', array($category));
            $this->commitTransaction();

            return $category;
        }

        $this->errors = $category->errors();
        $this->rollbackTransaction();

        return false;
    }

    /**
     * Removes an existing record.
     *
     * @param  array $removeDetails
     * @return mixed
     */
    public function remove(array $removeDetails = array())
    {
        $this->startTransaction();
        try {
            if ($removeDetails[0] != 0) {
                Category::destroy($removeDetails[0]);

                // This will set all the properties that have the category to 'Uncategorized'.
                $this->table('properties')
                     ->where('category_id', '=', $removeDetails[0])
                     ->update([
                                  'category_id' => 0
                              ]);

                $this->commitTransaction();

                return true;
            }
        } catch (\Exception $e) {
        }

        Event::fire('property.category.removed.failed', [$removeDetails[0]]);
    }

    /**
     * Updates an existing record.
     *
     * @param  int   $recordID
     * @param  array $newRecordDetails
     * @return mixed
     */
    public function update($recordID, array $newRecordDetails)
    {
        $category = Category::findOrFail($recordID);
        $this->startTransaction();

        $category->category_name = $newRecordDetails['category_name'];

        $validationRules = Category::$rules;
        $validationRules['category_name'] = Category::$rules['category_name'] . ',' . $category->id;

        if ($category->save($validationRules)) {
            Event::fire('property.category.updated', [$category]);
            $this->commitTransaction();

            return $category;
        }

        $this->errors = $category->errors();
        $this->rollbackTransaction();

        return false;
    }

    public function getCategories()
    {
        return $this->table('property_categories')
                    ->select('property_categories.id', 'property_categories.category_name', DB::raw(
                        '(select count(id) from properties where properties.category_id = property_categories.id) as property_count'
                    ))
                    ->orderBy('category_name', 'ASC');
    }

    public function getCategory($id)
    {
        return Category::findOrFail($id);
    }
}
