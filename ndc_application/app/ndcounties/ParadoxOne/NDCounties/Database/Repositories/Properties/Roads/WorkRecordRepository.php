<?php namespace ParadoxOne\NDCounties\Database\Repositories\Properties\Roads;

use Exception;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Event;
use ParadoxOne\NDCounties\Contracts\Properties\Roads\WorkRecordRepositoryInterface;
use ParadoxOne\NDCounties\Database\Repositories\BaseTenantRepository;
use Service\Properties\Road\WorkRecord as RoadWorkRecord;

class WorkRecordRepository extends BaseTenantRepository implements WorkRecordRepositoryInterface
{
    public function getRecordByID($recordID)
    {
        return RoadWorkRecord::findOrFail($recordID);
    }

    public function getRecordByWorkID($workID)
    {
        return RoadWorkRecord::where('work_record_id', '=', $workID)->firstOrFail();
    }

    /**
     * Determines if the work record is valid.
     *
     * @param  array $data
     * @return mixed
     */
    public function isValid($data)
    {
        $workRecord                  = new RoadWorkRecord;
        $workRecord->work_record_id  = 0;
        $workRecord->surface_type_id = $data['surface_type_id'];
        $workRecord->road_length     = $data['road_length'];
        $workRecord->unit_id         = $data['unit_id'];

        $success = $workRecord->validate();

        if ($success == true) {
            return true;
        }

        $this->errors = $workRecord->errors();

        return false;
    }

    /**
     * Creates a new record.
     *
     * @param  array $recordDetails
     * @return mixed
     */
    public function create(array $recordDetails = array())
    {
        $workRecord                  = new RoadWorkRecord;
        $workRecord->work_record_id  = $recordDetails['work_record_id'];
        $workRecord->surface_type_id = $recordDetails['surface_type_id'];
        $workRecord->road_length     = $recordDetails['road_length'];
        $workRecord->unit_id         = $recordDetails['unit_id'];

        DB::connection(\Tenant::getCurrentConnection())->beginTransaction();

        try {
            if ($workRecord->save()) {
                DB::connection(\Tenant::getCurrentConnection())->commit();

                Event::fire('workentry.record.road.created', array($workRecord));

                return $workRecord;
            } else {
                $this->errors = $workRecord->errors();
            }
        } catch (Exception $e) {
        }

        $this->errors = $workRecord->errors();
        Event::fire('workentry.record.road.created.failed', array($recordDetails));
        DB::connection(\Tenant::getCurrentConnection())->rollback();

        return false;
    }

    /**
     * Removes an existing record.
     *
     * @param  array $removeDetails
     * @return mixed
     */
    public function remove(array $removeDetails = array())
    {
        DB::connection(\Tenant::getCurrentConnection())->beginTransaction();

        try {
            if ($removeDetails[0] != 0) {
                if (array_key_exists(1, $removeDetails)) {
                    Event::fire('workentry.record.road.removed', $array($removeDetails[0]));
                    RoadWorkRecord::where('work_record_id', '=', $removeDetails[0])->delete();
                    DB::connection(\Tenant::getCurrentConnection())->commit();

                    return true;
                } else {
                    return true;
                }
            }
        } catch (Exception $e) {
        }

        DB::connection(\Tenant::getCurrentConnection())->rollback();
        Event::fire('workentry.record.road.removed.failed', array($removeDetails[0]));

        return false;
    }

    /**
     * Updates an existing record.
     *
     * @param  int   $recordID
     * @param  array $newRecordDetails
     * @return mixed
     */
    public function update($recordID, array $newRecordDetails)
    {
        $workRecord = RoadWorkRecord::findOrFail($recordID);

        if ($workRecord == null) {
            throw new ModelNotFoundException;
        }

        $workRecord->surface_type_id = $newRecordDetails['surface_type_id'];
        $workRecord->road_length     = $newRecordDetails['road_length'];
        $workRecord->unit_id         = $newRecordDetails['unit_id'];

        DB::connection(\Tenant::getCurrentConnection())->beginTransaction();

        try {
            if ($workRecord->updateUniques()) {
                Event::fire('workentry.record.road.updated', array($workRecord));

                return $workRecord;
            } else {
                $this->errors = $workRecord->errors();
            }
        } catch (Exception $e) {
        }

        DB::connection(\Tenant::getCurrentConnection())->rollback();
        Event::fire('workentry.record.road.updated.failed', array($newRecordDetails));

        return false;
    }
}
