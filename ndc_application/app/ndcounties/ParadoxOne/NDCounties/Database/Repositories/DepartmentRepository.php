<?php namespace ParadoxOne\NDCounties\Database\Repositories;

use Exception;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Event;
use ParadoxOne\NDCounties\Contracts\DepartmentsRepositoryInterface;
use ParadoxOne\NDCounties\IdentificationCode as IDCode;
use ParadoxOne\NDCounties\Settings\SortingSettingsManager;
use ParadoxOne\NDCounties\Traits\DefaultSortableTrait;
use ParadoxOne\NDCounties\Traits\SearchableTrait;
use ParadoxOne\NDCounties\Traits\UnTrashedQueryBuilderTrait;
use Service\Department;

class DepartmentRepository extends BaseTenantRepository implements DepartmentsRepositoryInterface
{
    use SearchableTrait;
    use DefaultSortableTrait;
    use UnTrashedQueryBuilderTrait;

    protected $table = 'departments';

    private function getSortedBuilder()
    {
        $builder = $this->table()->select('id', 'name', 'code', 'description', 'deleted_at');
        $this->applySorting(SortingSettingsManager::DEPARTMENTS_TABLE_SETTING_NAME, $builder);
        return $builder;
    }

    public function getDepartments()
    {
        return $this->getSortedBuilder()->whereNull('deleted_at');
    }

    public function getAll()
    {
        return $this->getDepartments();
    }

    public function getAllWithTrashed()
    {
        return $this->getSortedBuilder();
    }


    public function getModelById($id)
    {
        Event::fire('structure.department.accessed', array($id));

        return Department::findOrFail($id);
    }

    public function create(array $departmentData = array())
    {
        $this->startTransaction();
        $department              = new Department;
        $department->name        = $departmentData['name'];
        $department->description = $departmentData['description'];
        $department->code        = IDCode::prepareFrom($departmentData['code']);

        $validationRules = Department::$rules;
        $validationRules['code'] = Department::$rules['code'] . ',NULL,id,deleted_at,NULL';
        $validationRules['name'] = Department::$rules['name'] . ',NULL,id,deleted_at,NULL';

        if ($department->validate($validationRules)) {
            $possibleRestore = Department::withTrashed()->where('code', '=', $department->code)->first();
            if ($possibleRestore !== null) {
                $possibleRestore->name = $department->name;
                $possibleRestore->description = $department->description;
                $possibleRestore->deleted_at = null;
                $validationRules = Department::$rules;
                $validationRules['code'] = Department::$rules['code'] . ',' . $department->id . ',id,deleted_at,NULL';
                $saved = $possibleRestore->save($validationRules);
                Event::fire('structure.department.created', array($possibleRestore));
                $this->commitTransaction();
                return $possibleRestore;
            }
        }

        if ($department->save($validationRules)
        ) {
            Event::fire('structure.department.created', array($department));
            $this->commitTransaction();
            return $department;
        } else {
            $this->errors = $department->errors();
        }
        $this->rollbackTransaction();
        return false;
    }

    public function update($id, array $departmentData = array())
    {
        $department              = Department::findOrFail($id);
        $this->startTransaction();
        $department->name        = $departmentData['name'];
        $department->description = $departmentData['description'];
        $department->code        = IDCode::prepareFrom($departmentData['code']);

        $validationRules = Department::$rules;
        $validationRules['code'] = Department::$rules['code'] . ',' . $department->id . ',id,deleted_at,NULL';
        $validationRules['name'] = Department::$rules['name'] . ',' . $department->id . ',id,deleted_at,NULL';

        if ($department->save($validationRules)
        ) {
            Event::fire('structure.department.updated', array($department));
            $this->commitTransaction();
            return $department;
        } else {
            $this->errors = $department->errors();
        }
        $this->rollbackTransaction();
        return false;
    }

    public function remove(array $removeData = array())
    {
        $this->startTransaction();
        $removeData = array_strip_values($removeData, 0);
        try {
            foreach ($removeData as $department) {
                Event::fire('structure.department.removed', $department);
            }
            Department::destroy($removeData);
            $this->commitTransaction();
            return true;
        } catch (Exception $e) {
        }

        foreach ($removeData as $department) {
            Event::fire('structure.department.removed.failed', $department);
        }
        $this->rollbackTransaction();
        return false;

        //Department::destroy(intval($removeData[0]));
    }

    public function unTrash($departments)
    {
        $this->startTransaction();

        if (!is_array($departments)) {
            $departments = (array)$departments;
        }

        if (count($departments) == 0) {
            return false;
        }

        try {

            $restoreData = $this->getRecordsThatCantBeRestored($this->table, $departments);

            if (count($restoreData->recordsThatCanBeRestored) == 0) {
                // If we've reached this point, there are no records that can be restored
                // due to conflicts.
                Event::fire('structure.department.restored.conflictsDetected', [$restoreData->similarRecords]);
                $this->rollbackTransaction();

                // Return true because of reasons.
                return true;
            }

            $this->table()->whereIn('id', $departments)->update([
                'deleted_at' => null
            ]);
            Event::fire('structure.department.restored', [$departments]);
            $this->commitTransaction();
            return true;
        } catch (\Exception $e) {
        }

        $this->rollbackTransaction();

        return false;
    }

}
