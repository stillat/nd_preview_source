<?php namespace ParadoxOne\NDCounties\Database\Repositories;

use Exception;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Event;
use ParadoxOne\NDCounties\Contracts\DistrictsRepositoryInterface;
use ParadoxOne\NDCounties\IdentificationCode as IDCode;
use ParadoxOne\NDCounties\Settings\SortingSettingsManager;
use ParadoxOne\NDCounties\Traits\DefaultSortableTrait;
use ParadoxOne\NDCounties\Traits\SearchableTrait;
use ParadoxOne\NDCounties\Traits\UnTrashedQueryBuilderTrait;
use Service\District;

class DistrictRepository extends BaseTenantRepository implements DistrictsRepositoryInterface
{
    use SearchableTrait;
    use DefaultSortableTrait;
    use UnTrashedQueryBuilderTrait;

    protected $table = 'districts';

    private function getSortedBuilder()
    {
        $builder = $this->table()->select('id', 'name', 'code', 'description', 'deleted_at');
        $this->applySorting(SortingSettingsManager::DISTRICTS_TABLE_SETTING_NAME, $builder);
        return $builder;
    }

    public function getDistricts()
    {
        return $this->getSortedBuilder()->whereNull('deleted_at');
    }

    public function getAll()
    {
        return $this->getDistricts();
    }

    public function getAllWithTrashed()
    {
        return $this->getSortedBuilder();
    }


    public function getModelById($id)
    {
        Event::fire('structure.district.accessed', array($id));

        return District::findOrFail($id);
    }

    public function create(array $districtData = array())
    {
        $this->startTransaction();
        $district              = new District;
        $district->name        = $districtData['name'];
        $district->description = $districtData['description'];
        $district->code        = IDCode::prepareFrom($districtData['code']);

        $validationRules = District::$rules;
        $validationRules['code'] = District::$rules['code'] . ',NULL,id,deleted_at,NULL';
        $validationRules['name'] = District::$rules['name'] . ',NULL,id,deleted_at,NULL';

        if ($district->validate($validationRules)) {
            // See if we have any thing that can be restored.
            $possibleRestore = District::withTrashed()->where('code', '=', $district->code)->first();
            if ($possibleRestore !== null) {
                $possibleRestore->name = $district->name;
                $possibleRestore->description = $district->description;
                $possibleRestore->deleted_at = null;
                $validationRules = District::$rules;
                $validationRules['code'] = District::$rules['code'] . ',' . $possibleRestore->id . ',id,deleted_at,NULL';
                $saved = $possibleRestore->save($validationRules);
                Event::fire('structure.district.created', array($possibleRestore));
                $this->commitTransaction();
                return $possibleRestore;
            }
        }

        if ($district->save($validationRules)
        ) {
            Event::fire('structure.district.created', array($district));
            $this->commitTransaction();
            return $district;
        } else {
            $this->errors = $district->errors();
        }

        $this->rollbackTransaction();
        return false;
    }

    public function update($id, array $districtData = array())
    {
        $district              = District::findOrFail($id);
        $this->startTransaction();
        $district->name        = $districtData['name'];
        $district->description = $districtData['description'];
        $district->code        = IDCode::prepareFrom($districtData['code']);

        $validationRules = District::$rules;
        $validationRules['code'] = District::$rules['code'] . ',' . $district->id . ',id,deleted_at,NULL';
        $validationRules['name'] = District::$rules['name'] . ',' . $district->id . ',id,deleted_at,NULL';

        if ($district->save($validationRules)
        ) {
            Event::fire('structure.district.updated', array($district));
            $this->commitTransaction();
            return $district;
        } else {
            $this->errors = $district->errors();
        }

        $this->rollbackTransaction();
        return false;
    }

    public function remove(array $removeData = array())
    {
        $this->startTransaction();
        $removeData = array_strip_values($removeData, 0);
        try {
            foreach ($removeData as $district) {
                Event::fire('structure.district.removed', $district);
            }
            District::destroy($removeData);
            $this->commitTransaction();
            return true;
        } catch (Exception $e) {
        }

        foreach ($removeData as $district) {
            Event::fire('structure.district.removed.failed', $district);
        }
        $this->rollbackTransaction();
        return false;
        //District::destroy(intval($removeData[0]));
    }

    public function unTrash($districts)
    {
        $this->startTransaction();

        if (!is_array($districts)) {
            $districts = (array)$districts;
        }

        if (count($districts) == 0) {
            return false;
        }

        try {
            $restoreData = $this->getRecordsThatCantBeRestored($this->table, $districts);

            if (count($restoreData->recordsThatCanBeRestored) == 0) {
                // If we've reached this point, there are no records that can be restored
                // due to conflicts.
                Event::fire('structure.district.restored.conflictsDetected', [$restoreData->similarRecords]);
                $this->rollbackTransaction();

                // Return true because of reasons.
                return true;
            }

            $this->table()->whereIn('id', $districts)->update([
                'deleted_at' => null
            ]);
            Event::fire('structure.district.restored', [$districts]);
            $this->commitTransaction();
            return true;
        } catch (\Exception $e) {
            dd($e->getMessage());
        }

        $this->rollbackTransaction();

        return false;
    }

}
