<?php namespace ParadoxOne\NDCounties\Database\Repositories;

use Carbon;
use Exception;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Image;
use ParadoxOne\NDCounties\Contracts\EmployeeRepositoryInterface;
use ParadoxOne\NDCounties\IdentificationCode as IDCode;
use ParadoxOne\NDCounties\Settings\SortingSettingsManager;
use ParadoxOne\NDCounties\Traits\DefaultSortableTrait;
use ParadoxOne\NDCounties\Traits\UnTrashedQueryBuilderTrait;
use Service\Employee;

class EmployeeRepository extends BaseTenantRepository implements EmployeeRepositoryInterface
{
    use DefaultSortableTrait;
    use UnTrashedQueryBuilderTrait;

    protected $errors = null;

    /**
     * The table for the repository.
     *
     * @var string
     */
    protected $table = 'employees';

    public function remove(array $removeData = array())
    {
        $this->startTransaction();
        $removeData = array_strip_values($removeData, 0);
        try {
            foreach ($removeData as $employee) {
                Event::fire('employee.removed', [$employee]);
            }
            Employee::destroy($removeData);
            $this->commitTransaction();
            return true;
        } catch (Exception $e) {
        }
        $this->rollbackTransaction();
        foreach ($removeData as $employee) {
            Event::fire('employee.removed.failed', [$employee]);
        }
        return false;
    }

    public function update($id, array $employeeData = array())
    {
        // Update details first.
        $employee = Employee::findOrFail($id);
        $this->startTransaction();
        $employee->first_name       = $employeeData['first_name'];
        $employee->middle_name      = $employeeData['middle_name'];
        $employee->last_name        = $employeeData['last_name'];
        $employee->code             = IDCode::prepareFrom($employeeData['code']);
        $employee->gender           = $employeeData['gender'];
        $employee->wage             = $employeeData['wage'];
        $employee->overtime         = $employeeData['overtime'];
        $employee->wage_benefit     = $employeeData['wage_benefit'];
        $employee->overtime_benefit = $employeeData['overtime_benefit'];
        $employee->termination_date = mysql_date_or_null($employeeData['termination_date']);
        $employee->hire_date        = mysql_date_or_null($employeeData['hire_date']);

        if (Input::hasFile('photo')) {
            $validator = Validator::make(array('photo' => Input::file('photo')), array(
                'photo' => 'mimes:jpeg,jpg,bmp,png'
            ));


            if ($validator->fails()) {
                $this->errors = $validator->errors();

                return false;
            }

            //dd(public_path().get_internal_image_path($id));
            //mkdir(public_path().'/usedat/'.md5(\Session::get('activeServiceAccountID')).'/');
            //
            $directoryName = 'usedat/' . md5(Session::get('activeServiceAccountID')) . '/employee' . md5($id) . '/';

            if (!file_exists(public_path() . '/' . $directoryName)) {
                //dd(public_path().'/'.$directoryName);
                mkdir(public_path() . '/' . $directoryName, 0777, true);
            }


            $thumbnailImage = Image::make(Input::file('photo'))->resize(64, 64);
            $thumbnailImage->save(public_path() . '/' . $directoryName . '/thumbnail.png');
            $largeImage = Image::make(Input::file('photo'))->resize(391, 391);
            $largeImage->save(public_path() . '/' . $directoryName . '/large.png');

            $thumbnailImage = null;
            $largeImage     = null;
            unset($thumbnailImage);
            unset($largeImage);

            $employee->profile_image = $directoryName . '/thumbnail.png';
            $employee->large_image   = $directoryName . '/large.png';
        }

        $validationRules         = Employee::$rules;
        $validationRules['code'] = Employee::$rules['code'] . ',' . $employee->id . ',id,deleted_at,NULL';

        if ($employee->save($validationRules)
        ) {
            Event::fire('employee.updated', array($employee));
            $this->commitTransaction();

            return $employee;
        } else {
            $this->errors = $employee->errors();
        }

        $this->rollbackTransaction();

        return false;
    }

    public function create(array $employeeData = array())
    {
        $this->startTransaction();
        $employee                   = new Employee;
        $employee->first_name       = $employeeData['first_name'];
        $employee->middle_name      = $employeeData['middle_name'];
        $employee->last_name        = $employeeData['last_name'];
        $employee->code             = IDCode::prepareFrom($employeeData['code']);
        $employee->gender           = $employeeData['gender'];
        $employee->wage             = $employeeData['wage'];
        $employee->overtime         = $employeeData['overtime'];
        $employee->wage_benefit     = $employeeData['wage_benefit'];
        $employee->overtime_benefit = $employeeData['overtime_benefit'];

        $validationRules         = Employee::$rules;
        $validationRules['code'] = Employee::$rules['code'] . ',NULL,id,deleted_at,NULL';


        if ($employee->validate($validationRules)) {
            // See if we have any thing that can be restored.
            $possibleRestore = Employee::withTrashed()->where('code', '=', $employee->code)->first();
            if ($possibleRestore !== null) {
                $possibleRestore->first_name       = $employeeData['first_name'];
                $possibleRestore->middle_name      = $employeeData['middle_name'];
                $possibleRestore->last_name        = $employeeData['last_name'];
                $possibleRestore->gender           = $employeeData['gender'];
                $possibleRestore->wage             = $employeeData['wage'];
                $possibleRestore->overtime         = $employeeData['overtime'];
                $possibleRestore->wage_benefit     = $employeeData['wage_benefit'];
                $possibleRestore->overtime_benefit = $employeeData['overtime_benefit'];
                $possibleRestore->deleted_at = null;
                $validationRules = Employee::$rules;
                $validationRules['code'] = Employee::$rules['code'] . ',' . $possibleRestore->id . ',id,deleted_at,NULL';
                $saved = $possibleRestore->save($validationRules);
                Event::fire('employee.created', array($possibleRestore));
                $this->commitTransaction();

                return $possibleRestore;
            }
        }

        if ($employee->save($validationRules)
        ) {
            Event::fire('employee.created', array($employee));
            $this->commitTransaction();

            return $employee;
        } else {
            $this->errors = $employee->errors();
        }

        $this->rollbackTransaction();

        return false;
    }

    public function errors()
    {
        return $this->errors;
    }

    private function applyEmployeeSorting(&$builder)
    {
        $sortSettings = App::make('sorting_work_entry');
        $sortSettings = json_decode($sortSettings->get(SortingSettingsManager::EMPLOYEES_TABLE_SETTING_NAME), true);

        foreach ($sortSettings as $column => $settings) {
            switch ($column) {
                case 'code':
                    $this->applySortingSetting('code', $settings, $builder);
                    break;
                case 'last_updated':
                    $this->applySortingSetting('updated_at', $settings, $builder);
                    break;
                case 'date_created':
                    $this->applySortingSetting('created_at', $settings, $builder);
                    break;
                case 'first_name':
                    $this->applySortingSetting('first_name', $settings, $builder);
                    break;
                case 'middle_name':
                    $this->applySortingSetting('middle_name', $settings, $builder);
                    break;
                case 'last_name':
                    $this->applySortingSetting('last_name', $settings, $builder);
                    break;
                case 'hire_date':
                    $this->applySortingSetting('hire_date', $settings, $builder);
                    break;
                case 'termination_date':
                    $this->applySortingSetting('termination_date', $settings, $builder);
                    break;
                case 'regular_wage':
                    $this->applySortingSetting('wage', $settings, $builder);
                    break;
                case 'regular_benefits':
                    $this->applySortingSetting('wage_benefit', $settings, $builder);
                    break;
                case 'overtime_wage':
                    $this->applySortingSetting('overtime', $settings, $builder);
                    break;
                case 'overtime_benefits':
                    $this->applySortingSetting('overtime_benefit', $settings, $builder);
                    break;
            }
        }

        $sortSettings = null;
        unset($sortSettings);
    }

    private function getSortedBuilder()
    {
        $builder = $this->table()->select('id', 'first_name', 'last_name', 'code', 'wage', 'overtime', 'wage_benefit',
                                          'overtime_benefit', 'profile_image', 'deleted_at');
        $this->applyEmployeeSorting($builder);

        return $builder;
    }

    public function getAll()
    {
        return $this->getSortedBuilder()->whereNull('deleted_at');
    }

    public function getAllWithTrashed()
    {
        return $this->getSortedBuilder();
    }


    public function getModelByID($id)
    {
        Event::fire('employee.accessed', array($id));

        return Employee::findOrFail($id);
    }

    public function search($searchTerms)
    {
        $includeTrashed = Input::has('it');

        if (Input::get('is', null) !== null) {
            return $this->table()->select('id', 'code',
                                          DB::raw("CONCAT_WS(', ', last_name, first_name) as name"),
                                          'first_name',
                                          'last_name',
                                          'hire_date',
                                          'wage',
                                          'overtime', 'wage_benefit', 'overtime_benefit')
                        ->where('id', '=', Input::get('is'))->get();
        }

        if ($searchTerms === '//') {
            $builder = $this->table()->select('id', 'code',
                                              DB::raw("CONCAT_WS(', ', last_name, first_name) as name"),
                                              'first_name',
                                              'last_name',
                                              'hire_date',
                                              'wage',
                                              'overtime', 'wage_benefit', 'overtime_benefit'
            );

            if (!$includeTrashed) {
                $builder->whereNull('deleted_at');
            }

            return $builder->get();
        }

        $builder = $this->table()->select('id', 'code',
                                          DB::raw("CONCAT_WS(', ', last_name, first_name) as name"),
                                          'first_name',
                                          'last_name',
                                          'hire_date',
                                          'wage',
                                          'overtime', 'wage_benefit', 'overtime_benefit'
        )
                        ->orWhere(function ($query) use ($searchTerms) {
                            $query->where('first_name', 'LIKE', '%' . $searchTerms . '%')
                                  ->orWhere('last_name', 'LIKE', '%' . $searchTerms . '%')
                                  ->orWhere('code', 'LIKE', '%' . $searchTerms . '%');
                        });

        if (!$includeTrashed) {
            $builder->where(function ($query) {
                $query->whereNull('deleted_at');
            });
        }

        return $builder->get();
    }

    public function unTrash($employees)
    {
        $this->startTransaction();

        if (!is_array($employees)) {
            $employees = (array)$employees;
        }

        if (count($employees) == 0) {
            return false;
        }

        try {
            $restoreData = $this->getRecordsThatCantBeRestored($this->table, $employees, true);
            if (count($restoreData->recordsThatCanBeRestored) == 0) {
                // If we've reached this point, there are no records that can be restored
                // due to conflicts.
                Event::fire('employee.restored.conflictsDetected', [$restoreData->similarRecords]);
                $this->rollbackTransaction();

                // Return true because of reasons.
                return true;
            }

            $this->table()->whereIn('id', $employees)->update([
                'deleted_at' => null
            ]);
            Event::fire('employee.restored', [$employees]);
            $this->commitTransaction();
            return true;
        } catch (\Exception $e) {
        }

        $this->rollbackTransaction();

        return false;
    }

}
