<?php namespace ParadoxOne\NDCounties\Database\Repositories;

use Cartalyst\Sentry\Facades\Laravel\Sentry;
use Cartalyst\Sentry\Users\UserExistsException;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\MessageBag;
use ParadoxOne\NDCounties\Contracts\UserRepositoryInterface;
use User;
use UserSetting;

class UserRepository extends BaseRepository implements UserRepositoryInterface
{
    /**
     * The table for the repository.
     *
     * @var string
     */
    protected $table = 'users';

    public function getUsers()
    {
        return User::all();
    }


    /**
     * Creates a new ND Counties user.
     *
     * @param  array $userInformation
     * @return User
     */
    public function create(array $userInformation = array())
    {
        $user = Sentry::getUserProvider()->create(
            $userInformation
        );

        $userSettings          = new UserSetting;
        $userSettings->user_id = $user->id;
        $userSettings->save();

        return $user;
    }

    public function addSettingsToAccount($userID)
    {
        try {
            $userSettings          = new UserSetting;
            $userSettings->user_id = $userID;
            $userSettings->save();

            return $userSettings;
        } catch (\Exception $e) {
        }

        return false;
    }

    public function getLastCreatedUser()
    {
        return User::orderBy('id', 'desc')->first();
    }


    public function resetPassword($user, $newPassword)
    {
        try {
            $user           = Sentry::getUserProvider()->findById($user);
            $user->password = $newPassword;
            $user->save();

            return true;
        } catch (\Exception $e) {
        }

        return false;
    }

    public function update($userID, array $userInformation = array())
    {
        $errors = new MessageBag();

        try {
            $v = \Validator::make($userInformation, array(
                'email' => 'required|email',
                'first_name' => 'required',
                'last_name' => 'required'
            ));

            if ($v->fails()) {
                $this->errors = $v->errors();
                return false;
            }



            $user             = \Cartalyst\Sentry\Facades\Laravel\Sentry::findUserById($userID);
            $user->first_name = $userInformation['first_name'];
            $user->last_name  = $userInformation['last_name'];
            $user->email      = $userInformation['email'];

            if ($user->save()) {
                return true;
            } else {
                return false;
            }
        } catch (UserExistsException $userExists) {
            $errors->add('email', 'A user with that email address already exists.');
        } catch (\Exception $e) {
            lk($e);
        }

        $this->errors = $errors;
        return false;
    }

    public function remove(array $removeDetails = array())
    {
    }

    /**
     * Returns an array of account and tenants.
     *
     * @return mixed
     */
    public function getAccounts($userID)
    {
        return DB::table('tenant_accounts')
                 ->select('accounts.account_name', 'accounts.created_at', 'accounts.updated_at',
                          'tenants.tenant_name', 'tenant_accounts.tenant_id', 'accounts.id', DB::raw('tenants.active as tenant_active'))
                 ->join('tenants', 'tenant_accounts.tenant_id', '=', 'tenants.id')
                 ->join('accounts', 'tenants.id', '=', 'accounts.tenant_id')
                 ->where('tenant_accounts.user_id', '=', $userID);
    }

    public function getUsersIn($requestedUsers)
    {
        return DB::table('users')->select('first_name', 'last_name', 'id', 'email')->whereIn('id', $requestedUsers)->get();
    }

    public function updateUserSettings($userID, array $settingsData)
    {
        try {
            $userSettings = UserSetting::where('user_id', '=', $userID)->firstOrFail();
            $userSettings->items_per_page = $settingsData['items_per_page'];
            $userSettings->form_size = $settingsData['form_size'];
            $userSettings->auto_expand_more_options = $settingsData['auto_expand_more_options'];
            $userSettings->auto_clear_forms = $settingsData['auto_clear_forms'];
            $userSettings->show_animation_on_form_change = $settingsData['show_animation_on_form_change'];
            $userSettings->update();
            return true;
        } catch (\Exception $e) {
        }

        return false;
    }
}
