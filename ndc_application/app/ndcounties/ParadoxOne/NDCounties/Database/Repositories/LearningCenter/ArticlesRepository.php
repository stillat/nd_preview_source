<?php namespace ParadoxOne\NDCounties\Database\Repositories\LearningCenter;

use CustomerService\Article;
use Michelf\Markdown;
use ParadoxOne\NDCounties\Contracts\CustomerService\ArticlesRepositoryInterface;
use ParadoxOne\NDCounties\Database\Repositories\BaseRepository;

class ArticlesRepository extends BaseRepository implements ArticlesRepositoryInterface
{
    protected $table = 'articles';

    public function create(array $articleInformation = array())
    {
        $article                   = new Article;
        $article->title            = $articleInformation['title'];
        $article->slug             = $articleInformation['slug'];
        $article->keywords         = $articleInformation['keywords'];
        $article->description      = $articleInformation['description'];
        $article->content_markdown = $articleInformation['content'];
        $article->content_html     = Markdown::defaultTransform($articleInformation['content']);

        if ($article->save()) {
            return $article;
        } else {
            $this->errors = $article->errors();

            return false;
        }
    }

    public function update($id, array $messageInformation = array())
    {
    }

    public function remove(array $removeContext = array())
    {
    }

    public function count()
    {
        return $this->table()->whereNull('deleted_at')->count();
    }
}
