<?php namespace ParadoxOne\NDCounties\Database\Repositories\Finance\Invoices;

use ParadoxOne\NDCounties\Contracts\Finance\Invoices\InvoiceDestroyerRepositoryInterface;

class EloquentInvoiceDestroyerRepository implements InvoiceDestroyerRepositoryInterface
{
    /**
     * Deducts an array of work entries from their given invoices.
     *
     * @param array $workEntries
     * @return mixed
     */
    public function deduct(array $workEntries)
    {
        if (count($workEntries) == 0) {
            return false;
        }
    }
}
