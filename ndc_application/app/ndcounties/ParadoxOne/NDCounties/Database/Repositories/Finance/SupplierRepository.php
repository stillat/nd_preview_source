<?php namespace ParadoxOne\NDCounties\Database\Repositories\Finance;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Image;
use ParadoxOne\NDCounties\Contracts\Finance\SupplierRepositoryInterface;
use ParadoxOne\NDCounties\Database\Repositories\BaseTenantRepository;
use ParadoxOne\NDCounties\IdentificationCode as IDCode;
use Service\Finances\Supplier;
use Service\Finances\SupplierNote;

class SupplierRepository extends BaseTenantRepository implements SupplierRepositoryInterface
{
    protected $table = 'fs_suppliers';

    protected $errors = null;

    public function getModelByID($id)
    {
        return Supplier::findOrFail($id);
    }

    public function getNotes($supplierID)
    {
        return DB::connection($this->connectionName)->table('fs_supplier_notes')->select('id', 'note', 'created_at')
                 ->where('supplier_id', '=', $supplierID);
    }

    public function getNote($noteID)
    {
        return SupplierNote::findOrFail($noteID);
    }

    public function getSuppliers()
    {
        return $this->table()->select('id', 'code', 'name', 'active', 'created_at', 'updated_at')
                    ->orderBy('code', 'ASC')->whereNull('deleted_at')->orderBy('code', 'asc');
    }

    public function create(array $supplierData = array())
    {
        $supplier       = new Supplier;
        $supplier->name = $supplierData['name'];

        if (array_key_exists('active', $supplierData)) {
            $supplier->active = $supplierData['active'];
        } else {
            $supplier->active = false;
        }

        $supplier->code = IDCode::prepareFrom($supplierData['code']);

        if ($supplier->save()) {
            return $supplier;
        } else {
            $this->errors = $supplier->errors();

            return false;
        }
    }

    public function createNote($supplierID, array $noteInformation)
    {
        $note              = new SupplierNote;
        $note->note        = $noteInformation['note'];
        $note->supplier_id = $supplierID;

        if ($note->save()) {
            return $note;
        } else {
            $this->errors = $note->errors();

            return false;
        }
    }

    public function remove(array $supplierData = array())
    {
        Supplier::destroy(intval($supplierData[0]));
    }

    public function update($id, array $supplierData = array())
    {
        $supplier       = Supplier::findOrFail($id);
        $supplier->name = $supplierData['name'];
        $supplier->code = IDCode::prepareFrom($supplierData['code']);

        if (array_key_exists('active', $supplierData)) {
            $supplier->active = true;
        } else {
            $supplier->active = false;
        }

        if (Input::hasFile('photo')) {
            $validator = Validator::make(array('photo' => Input::file('photo')), array(
                'photo' => 'mimes:jpeg,bmp,png'
            ));

            if ($validator->fails()) {
                $this->errors = $validator->errors();

                return false;
            }

            $directoryName = 'usedat/' . md5(Session::get('activeServiceAccountID')) . '/supplier' . md5($id) . '/';

            if (!file_exists(public_path() . '/' . $directoryName)) {
                //dd(public_path().'/'.$directoryName);
                mkdir(public_path() . '/' . $directoryName, 0777, true);
            }

            $thumbnailImage = Image::make(Input::file('photo'))->resize(64, 64);
            $thumbnailImage->save(public_path() . '/' . $directoryName . '/thumbnail.png');
            $largeImage = Image::make(Input::file('photo'))->resize(391, 391);
            $largeImage->save(public_path() . '/' . $directoryName . '/large.png');

            $thumbnailImage = null;
            $largeImage     = null;
            unset($thumbnailImage);
            unset($largeImage);

            $supplier->thumbnail_image = $directoryName . '/thumbnail.png';
            $supplier->large_image     = $directoryName . '/large.png';
        }

        if ($supplier->updateUniques()) {
            return $supplier;
        } else {
            $this->errors = $supplier->errors();

            return false;
        }
    }
}
