<?php namespace ParadoxOne\NDCounties\Database\Repositories\Finance\Invoices;

use Exception;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use ParadoxOne\NDCounties\Contracts\Finance\Invoices\InvoiceReaderRepositoryInterface;
use ParadoxOne\NDCounties\Contracts\Finance\Invoices\InvoiceUpdaterRepositoryInterface;
use ParadoxOne\NDCounties\Contracts\Finance\TransactionRepositoryInterface;
use ParadoxOne\NDCounties\Contracts\WorkEntries\WorkEntryReaderInterface;
use ParadoxOne\NDCounties\Database\DuplicateValueException;
use Service\Finances\Invoices\Invoice;
use Stillat\Database\Tenant\TenantManager;

class InvoiceUpdaterRepository extends InvoiceWriterRepository implements InvoiceUpdaterRepositoryInterface
{
    /**
     * Indicates if the invoice will self destruct when it has no work records.
     *
     * @var bool
     */
    protected $selfDestructOnEmpty = true;

    /**
     * The invoice ID.
     *
     * @var int
     */
    protected $invoiceID = null;

    /**
     * The actual invoice instance.
     *
     * @var null
     */
    protected $invoiceInstance = null;

    /**
     * Holds the invoice tax rates.
     *
     * @var null
     */
    protected $invoiceTaxRates = [];

    /**
     * The invoice reader implementation.
     *
     * @var \ParadoxOne\NDCounties\Contracts\Finance\Invoices\InvoiceReaderRepositoryInterface
     */
    protected $invoiceReader = null;

    /**
     * The work reader instance.
     *
     * @var null|\ParadoxOne\NDCounties\Contracts\WorkEntries\WorkEntryReaderInterface
     */
    protected $workReader = null;

    protected $transactionRepository;

    public function __construct(InvoiceReaderRepositoryInterface $invoiceReader, WorkEntryReaderInterface $workReader,
                                TransactionRepositoryInterface $transactionRepository)
    {
        parent::__construct();
        $this->invoiceReader         = $invoiceReader;
        $this->workReader            = $workReader;
        $this->transactionRepository = $transactionRepository;
    }

    /**
     * Gets a query builder instance for the given table name.
     *
     * @param $tableName
     */
    private function table($tableName)
    {
        return DB::connection(\Tenant::getCurrentConnection())->table($tableName);
    }

    private function startTransaction()
    {
        DB::connection(\Tenant::getCurrentConnection())->beginTransaction();
    }

    private function dbCommit()
    {
        DB::connection(\Tenant::getCurrentConnection())->commit();
    }

    private function rollback()
    {
        DB::connection(\Tenant::getCurrentConnection())->rollback();
    }


    /**
     * Refreshes the invoice's information.
     *
     * @param int   $invoiceID
     * @param mixed $invoiceDataInformation
     *
     * @return mixed
     */
    public function refreshInformation($invoiceID, $invoiceDataInformation)
    {
        $this->startTransaction();

        try {
            $this->invoiceID       = $invoiceID;
            $this->invoiceInstance = $this->invoiceReader->getInvoiceByID($invoiceID);
            $workRecords           = $this->workReader->reader()->withDefaults()->forInvoice($invoiceID)->getRecords();

            // Here we need to decide if we will self destruct or reset everything to `0`.
            if (count($workRecords) == 0) {
                // This handles the self destruction setting.
                if ($this->selfDestructOnEmpty) {
                    // Remove the invoice entry.
                    $this->table('fs_invoices')->where('id', '=', $invoiceID)->delete();

                    // Remove the invoice tax rate information.
                    $this->table('fs_invoices_tax_rates')->where('invoice_id', '=', $invoiceID)->delete();
                    $this->dbCommit();

                    // We need to return here otherwise we will receive errors below.
                    return true;
                } else {
                    // Here we will just reset everything to a value of '0'. Pretty simple.
                    $invoiceModel = Invoice::findOrFail($invoiceID);
                    $invoiceModel->resetAndUpdate(); // Here we will use the model's API.
                    $this->dbCommit();

                    return true;
                }
            }

            // Grab a model instance we can work with directly.
            $invoiceModel = Invoice::findOrFail($invoiceID);

            // This will update the invoice's meta information only if the data object
            // is not set to null. This allows us to handle updates and destroys without
            // having to expose multiple APIs.
            if ($invoiceDataInformation != null) {
                // Grab the new invoice data information from the user.

                if ($invoiceDataInformation->discount_amount != null) {
                    $invoiceModel->discount = $invoiceDataInformation->discount_amount;
                }

                if ($invoiceDataInformation->billed_account != null) {
                    $invoiceModel->account_number = $invoiceDataInformation->billed_account;
                }

                if ($invoiceDataInformation->paid_account != null) {
                    $invoiceModel->paid_to_account = $invoiceDataInformation->paid_account;
                }

                if ($invoiceDataInformation->due_date != null) {
                    $invoiceModel->due_date = $invoiceDataInformation->due_date;
                }

                $invoiceModel->client_notes  = $invoiceDataInformation->client_notes;
                $invoiceModel->invoice_terms = $invoiceDataInformation->invoice_terms;
            }

            $newInvoiceTotal           = 0;
            $newInvoiceTotalMaterials  = 0;
            $newInvoiceTotalEquipments = 0;
            $newInvoiceTotalEmployees  = 0;
            $newInvoiceTotalFuels      = 0;
            $newInvoiceTaxAmount       = 0;
            $newInvoiceSubTotal        = 0;
            $newInvoiceDiscountTotal   = 0;

            // Consumable stuff here.
            foreach ($workRecords as $workRecord) {
                $recordTotals = $workRecord->getRecordTotals();

                $recordSubTotal  = 0;
                $recordTaxAmount = 0;

                $newInvoiceSubTotal   = $this->mathEngine->add($newInvoiceSubTotal, $recordTotals->fuelsCost);
                $newInvoiceTotalFuels = $this->mathEngine->add($newInvoiceTotalFuels, $recordTotals->fuelsCost);
                $recordSubTotal       = $this->mathEngine->add($recordSubTotal, $recordTotals->fuelsCost);

                $newInvoiceSubTotal       = $this->mathEngine->add($newInvoiceSubTotal, $recordTotals->materialCost);
                $newInvoiceTotalMaterials =
                    $this->mathEngine->add($newInvoiceTotalMaterials, $recordTotals->materialCost);
                $recordSubTotal           = $this->mathEngine->add($recordSubTotal, $recordTotals->materialCost);


                $newInvoiceSubTotal        =
                    $this->mathEngine->add($newInvoiceSubTotal, $recordTotals->equipmentUnitCost);
                $newInvoiceTotalEquipments =
                    $this->mathEngine->add($newInvoiceTotalEquipments, $recordTotals->equipmentUnitCost);
                $recordSubTotal            = $this->mathEngine->add($recordSubTotal, $recordTotals->equipmentUnitCost);


                $newInvoiceSubTotal       =
                    $this->mathEngine->add($newInvoiceSubTotal, $recordTotals->employeeCombined);
                $newInvoiceTotalEmployees =
                    $this->mathEngine->add($newInvoiceTotalEmployees, $recordTotals->employeeCombined);
                $recordSubTotal           = $this->mathEngine->add($recordSubTotal, $recordTotals->employeeCombined);


                $newInvoiceSubTotal = $this->mathEngine->add($newInvoiceSubTotal, $recordTotals->miscBilling);
                $recordSubTotal     = $this->mathEngine->add($recordSubTotal, $recordTotals->miscBilling);


                // Tax stuff
                foreach ($workRecord->getMaterials() as $mat) {
                    $recordTaxAmount = $this->mathEngine->add($recordTaxAmount, $mat->total_tax);
                }

                foreach ($workRecord->getFuels() as $fuel) {
                    $recordTaxAmount = $this->mathEngine->add($recordTaxAmount, $fuel->total_tax);
                }

                foreach ($workRecord->getEquipmentUnits() as $unit) {
                    $recordTaxAmount = $this->mathEngine->add($recordTaxAmount, $unit->total_tax);
                }
                // End tax stuff.

                // Discount stuff.
                $discountPercentage = $this->mathEngine->div($invoiceModel->discount, 100);
                $recordDiscount     = $this->mathEngine->mul($discountPercentage, $recordSubTotal);

                $newInvoiceDiscountTotal = $this->mathEngine->add($newInvoiceDiscountTotal, $recordDiscount);

                $newInvoiceTaxAmount = $this->mathEngine->add($newInvoiceTaxAmount, $recordTaxAmount);

                if ($recordTotals->invoiceOverride > 0) {
                    // This handles the override settings.
                    $newInvoiceTotal = $this->mathEngine->add($newInvoiceTotal, $recordTotals->invoiceOverride);
                } else {
                    // This handles the default calculated method.
                    $recordTotal = $this->mathEngine->sub($recordSubTotal, $recordDiscount);
                    $recordTotal = $this->mathEngine->add($recordTotal, $recordTaxAmount);

                    $newInvoiceTotal = $this->mathEngine->add($newInvoiceTotal, $recordTotal);
                }
            }
            // End consumable stuff.


            $invoiceModel->invoice_total                 = $newInvoiceTotal;
            $invoiceModel->invoice_total_materials       = $newInvoiceTotalMaterials;
            $invoiceModel->invoice_total_equipment_units = $newInvoiceTotalEquipments;
            $invoiceModel->invoice_total_employee_costs  = $newInvoiceTotalEmployees;
            $invoiceModel->invoice_total_fuels           = $newInvoiceTotalFuels;
            $invoiceModel->invoice_tax_amount            = $newInvoiceTaxAmount;
            $invoiceModel->invoice_sub_total             = $newInvoiceSubTotal;
            $invoiceModel->invoice_discount_total        = $newInvoiceDiscountTotal;

            $updateSuccess = $invoiceModel->update();
            if ($updateSuccess) {
                $this->dbCommit();

                return true;
            }
        } catch (\Exception $e) {
            lk('sdf', $e);
            lk($e);
        }

        $this->rollback();

        return false;
    }

    /**
     * Refreshes the finance account information.
     *
     * @param array $accounts
     */
    private function refreshFinanceAccounts(array $accounts)
    {
        // We will just ignore the $accounts. We can accomplish a balance reset
        // by taking advantage of the transaction repository.
        $this->transactionRepository->refreshAccountBalances();
    }

    /**
     * Sets whether or not the invoice removes itself when it has no
     * work records.
     *
     * @param $doesSelfDestruct
     *
     * @return mixed
     */
    public function setSelfDestructOnEmpty($doesSelfDestruct)
    {
        $this->selfDestructOnEmpty = $doesSelfDestruct;
    }

    /**
     * Gets the self destruct setting.
     *
     * @return mixed
     */
    public function getSelfDestructOnEmpty()
    {
        return $this->selfDestructOnEmpty;
    }

    /**
     * {@inheritdoc}
     */
    public function commit()
    {
        // TODO: Implement commit() method.
    }

    /**
     * Appends the invoice record instead of committing.
     *
     * @return mixed
     * @throws \Exception
     */
    public function append()
    {
        throw new \Exception('Feature not supported');
    }

    public function cleanupInvoiceLinks()
    {
        DB::connection(\Tenant::getCurrentConnection())
          ->statement(DB::raw('DELETE FROM fs_invoice_work_records WHERE work_record_id NOT IN (select id from work_entries);'));
    }

    /**
     * Removes the work record from any invoices.
     *
     * @param $workRecordID
     *
     * @return mixed
     */
    public function touchInvoicesForDestroy($workRecordID)
    {
        // $workRecordID = intval($workRecordID[0]);
        $affectedInvoice = $this->invoiceReader->getInvoiceForWorkRecord($workRecordID);
        // If there is no invoice, then just return null and be done with it.
        if ($affectedInvoice == null) {
            return null;
        }

        // We are going to remove any transactions associated with the work entry.
        $this->table('fs_transactions')->where('work_record_id', '=', $workRecordID)->delete();

        $accountsToAdjust   = [];
        $accountsToAdjust[] = $affectedInvoice->paidToAccount;
        $accountsToAdjust[] = $affectedInvoice->billedToAccount;

        // Remove the work record from the invoice(s).
        // $this->removeWorkRecord($workRecordID);
        $this->table('fs_invoice_work_records')->where('work_record_id', '=', $workRecordID)->delete();

        $this->refreshFinanceAccounts($accountsToAdjust);

        // Refresh the invoice.
        $this->refreshInformation($affectedInvoice->id, null);
    }

    /**
     * Removes a work record from an invoice.
     *
     * @param $invoiceID
     * @param $workRecordID
     *
     * @return int|null|bool Returns the invoice ID affected on success, false on failure. Returns null if there was no
     *                       failure, and no affected invoice records.
     */
    public function removeWorkRecordFromInvoice($invoiceID, $workRecordID)
    {
        $this->startTransaction();

        try {
            $affectedRecords = $this->table('fs_invoice_work_records')->where('work_record_id', '=', $workRecordID)
                                    ->where('invoice_id', '=', $invoiceID)->delete();

            $this->dbCommit();

            if ($affectedRecords == 0) {
                return null;
            }

            return $invoiceID;
        } catch (\Exception $e) {
        }

        $this->rollback();

        return false;
    }

    /**
     * Unlocks a given invoice.
     *
     * @param $invoiceID
     * @return mixed
     */
    public function unlockInvoice($invoiceID)
    {
        $this->startTransaction();

        try {
            $this->table('fs_invoices')->where('id', '=', $invoiceID)->update(['invoice_status' => 0]);
            $this->table('work_entries')
                 ->join('fs_invoice_work_records', 'work_entries.id', '=', 'fs_invoice_work_records.work_record_id')
                 ->where('fs_invoice_work_records.invoice_id', '=', $invoiceID)
                 ->update(['work_entries.record_status' => 1]);

            $this->dbCommit();

            return true;
        } catch (\Exception $e) {
        }

        $this->rollback();

        return false;
    }

    /**
     * Locks a given invoice.
     *
     * @param $invoiceID
     * @return mixed
     */
    public function lockInvoice($invoiceID)
    {
        $this->startTransaction();

        try {
            $this->table('fs_invoices')->where('id', '=', $invoiceID)->update(['invoice_status' => 5]);
            $this->table('work_entries')
                 ->join('fs_invoice_work_records', 'work_entries.id', '=', 'fs_invoice_work_records.work_record_id')
                 ->where('fs_invoice_work_records.invoice_id', '=', $invoiceID)
                 ->update(['work_entries.record_status' => 5]);

            $this->dbCommit();

            return true;
        } catch (\Exception $e) {
        }

        $this->rollback();

        return false;
    }

    /**
     * Changes an invoices accounts.
     *
     * @param $invoiceID
     * @param $newBillToAccount
     * @param $newPayToAccount
     * @return mixed
     */
    public function changeInvoiceAccounts($invoiceID, $newBillToAccount, $newPayToAccount)
    {
        $this->startTransaction();

        try {

            // First let's update the invoices.
            $this->table('fs_invoices')->where('id', '=', $invoiceID)
                 ->update([
                              'account_number'  => $newBillToAccount,
                              'paid_to_account' => $newPayToAccount
                          ]);


            // Secondly, we need to update the primary transactions.
            $this->table('fs_transactions')->where('associated_context', '=', 1)->where(function ($query) use ($invoiceID) {
                $query->where('associated_value', '=', $invoiceID);
            })->where(function ($query) {
                $query->where('is_primary_transaction', '=', 1);
            })->update([
                'begin_account' => $newBillToAccount,
                'end_account' => $newPayToAccount
                                   ]);



            // Thirdly, we need to update the secondary transactions.
            $this->table('fs_transactions')->where('associated_context', '=', 1)->where(function ($query) use ($invoiceID) {
                $query->where('associated_value', '=', $invoiceID);
            })->where(function ($query) {
                $query->where('is_primary_transaction', '=', 0);
            })->update([
                           'begin_account' => $newPayToAccount,
                           'end_account' => $newBillToAccount
                       ]);

            $this->refreshFinanceAccounts([]);

            $this->dbCommit();


            return true;
        } catch (\Exception $e) {
        }

        $this->rollback();

        return false;
    }

    /**
     * @param $invoiceID
     * @param $newInvoiceNumber
     * @return bool
     * @throws DuplicateValueException
     */
    public function changeInvoiceNumber($invoiceID, $newInvoiceNumber)
    {
        // Removes spaces from the invoice number.
        $newInvoiceNumber = str_replace(' ', '_', $newInvoiceNumber);

        $recordCollisionCheck = $this->table('fs_invoices')->where('invoice_number', '=', $newInvoiceNumber)->where(function ($query) use ($invoiceID) {
            $query->where('id', '<>', $invoiceID);
        })->count();

        if ($recordCollisionCheck !== 0) {
            throw new DuplicateValueException('The invoice number `'.$newInvoiceNumber.'` already exists.');
        }

        $this->startTransaction();

        try {
            $this->table('fs_invoices')->where('id', '=', $invoiceID)->update(['invoice_number' => $newInvoiceNumber]);

            $this->dbCommit();
            return true;
        } catch (Exception $e) {
        }

        $this->rollback();
        return false;
    }
}
