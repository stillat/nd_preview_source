<?php namespace ParadoxOne\NDCounties\Database\Repositories\Finance\Invoices;

use Carbon;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use ParadoxOne\NDCounties\Contracts\Finance\Invoices\InvoiceWriterRepositoryInterface;

abstract class InvoiceWriterRepository implements InvoiceWriterRepositoryInterface
{
    use \ParadoxOne\NDCounties\Traits\MathExecutableTrait;

    /**
     * The invoice due date.
     *
     * @var string
     */
    protected $invoiceDueDate = null;

    protected $workRecords = array();

    /**
     * The invoice account number.
     *
     * @var int
     */
    protected $invoiceAccountNumber = null;

    /**
     * The invoice account number that the invoice was paid to.
     *
     * @var int
     */
    protected $invoicePaidToAccountNumber = 0;

    /**
     * The invoice number.
     *
     * @var string
     */
    protected $invoiceNumber = null;

    /**
     * The invoice discount amount.
     *
     * @var float
     */
    protected $invoiceDiscountAmount = 0.0;

    /**
     * The invoice client notes.
     *
     * @var string
     */
    protected $invoiceClientNotes = '';

    /**
     * The invoice terms.
     *
     * @var string
     */
    protected $invoiceTerms = '';

    /**
     * The invoice tax rates.
     *
     * @var [type]
     */
    protected $invoiceTaxRates = null;

    /**
     * Holds the tax rates that we need to actually update, not just insert.
     *
     * @var null
     */
    protected $invoiceUpdateTaxRates = null;

    /**
     * The employee total.
     *
     * @var float
     */
    protected $invoiceEmployeeTotal = 0.0;

    /**
     * The equipment total.
     *
     * @var float
     */
    protected $invoiceEquipmentTotal = 0.0;

    /**
     * The fuel total.
     *
     * @var float
     */
    protected $invoiceFuelTotal = 0.0;

    /**
     * The material total.
     *
     * @var float
     */
    protected $invoiceMaterialTotal = 0.0;

    /**
     * The total employee taxable amount.
     *
     * @var float
     */
    protected $invoiceEmployeeTotalTaxable = 0.0;

    /**
     * The total equipment total taxable amount.
     *
     * @var float
     */
    protected $invoiceEquipmentTotalTaxable = 0.0;

    /**
     * The total fuel total taxable amount.
     *
     * @var float
     */
    protected $invoiceFuelTotalTaxable = 0.0;

    /**
     * The total material total taxable amount.
     *
     * @var float
     */
    protected $invoiceMaterialTotalTaxable = 0.0;

    /**
     * The tax rates for employees.
     *
     * @var float
     */
    protected $invoiceEmployeeTaxRateTotal = 0.0;

    /**
     * The tax rates for equipments.
     *
     * @var float
     */
    protected $invoiceEquipmentTaxRateTotal = 0.0;

    /**
     * The tax rates for fuels.
     *
     * @var float
     */
    protected $invoiceFuelTaxRateTotal = 0.0;

    /**
     * The tax rates for materials.
     *
     * @var float
     */
    protected $invoiceMaterialTaxRateTotal = 0.0;

    /**
     * The misc billing rate.
     *
     * @var int
     */
    protected $miscBillingRate = 0;

    /**
     * The misc billing quantity.
     *
     * @var int
     */
    protected $miscBillingQuantity = 0;

    /**
     * Holds the invoice total override.
     *
     * @var bool|decimal
     */
    protected $invoiceTotalOverride = false;

    /**
     * Returns a new instance of the invoice writer repository.
     */
    public function __construct()
    {
        $this->invoiceTaxRates = new Collection;

        $this->setMathEngine();
    }

    /**
     * {@inheritdoc}
     */
    public function setDueDate($date)
    {
        $this->invoiceDueDate = (new Carbon($date))->toDateTimeString();
    }

    /**
     * {@inheritdoc}
     */
    public function getDueDate()
    {
        return $this->invoiceDueDate;
    }

    /**
     * {@inheritdoc}
     */
    public function setAccountNumber($account)
    {
        $this->invoiceAccountNumber = $account;
    }

    /**
     * Sets the finance paid to account.
     *
     * This is the account that the invoice is PAID to.
     *
     * @param $account
     * @return mixed
     */
    public function setPaidToAccount($account)
    {
        $this->invoicePaidToAccountNumber = $account;
    }

    /**
     * Gets the finance account number that the invoice is paid to.
     *
     * @return mixed
     */
    public function getPaidToAccount()
    {
        return $this->invoicePaidToAccountNumber;
    }

    /**
     * {@inheritdoc}
     */
    public function getAccountNumber()
    {
        return $this->invoiceAccountNumber;
    }

    /**
     * {@inheritdoc}
     */
    public function setInvoiceNumber($invoiceNumber)
    {
        $this->invoiceNumber = $invoiceNumber;
    }

    /**
     * {@inheritdoc}
     */
    public function getInvoiceNumber()
    {
        return $this->invoiceNumber;
    }

    /**
     * Sets the misc billing quantity.
     *
     * @param $quantity
     * @return mixed
     */
    public function setMiscBillingQuantity($quantity)
    {
        $this->miscBillingQuantity = $quantity;
    }

    /**
     * Sets the misc billing rate.
     *
     * @param $rate
     * @return mixed
     */
    public function setMiscBillingRate($rate)
    {
        $this->miscBillingRate = $rate;
    }


    /**
     * {@inheritdoc}
     */
    public function setDiscount($discount = 0.0)
    {
        $this->invoiceDiscountAmount = $discount;
    }

    /**
     * {@inheritdoc}
     */
    public function getDiscount()
    {
        return $this->invoiceDiscountAmount;
    }

    /**
     * {@inheritdoc}
     */
    public function setClientNotes($clientNotes)
    {
        $this->invoiceClientNotes = $clientNotes;
    }

    /**
     * {@inheritdoc}
     */
    public function getClientNotes()
    {
        return $this->invoiceClientNotes;
    }

    /**
     * {@inheritdoc}
     */
    public function setInvoiceTerms($invoiceTerms)
    {
        $this->invoiceTerms = $invoiceTerms;
    }

    /**
     * {@inheritdoc}
     */
    public function getInvoiceTerms()
    {
        return $this->invoiceTerms;
    }

    /**
     * {@inheritdoc}
     */
    public function addTaxRate($taxRateID, $historicTaxRate, $affectsEmployees, $affectsEquipments, $affectsFuels, $affectsMaterials, $originalID = '')
    {
        if (strlen($originalID) > 0) {
            $this->invoiceUpdateTaxRates[] =
                array($taxRateID, $historicTaxRate, $affectsEmployees, $affectsEquipments, $affectsFuels,
                      $affectsMaterials, $originalID);
        } else {
            $this->invoiceTaxRates[] =
                array($taxRateID, $historicTaxRate, $affectsEmployees, $affectsEquipments, $affectsFuels,
                      $affectsMaterials);
        }

        if ($affectsEmployees == 1) {
            $this->invoiceEmployeeTaxRateTotal += $historicTaxRate;
        }

        if ($affectsEquipments == 1) {
            $this->invoiceEquipmentTaxRateTotal += $historicTaxRate;
        }

        if ($affectsFuels == 1) {
            $this->invoiceFuelTaxRateTotal += $historicTaxRate;
        }

        if ($affectsMaterials == 1) {
            $this->invoiceMaterialTaxRateTotal += $historicTaxRate;
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getTaxRates()
    {
        return $this->invoiceTaxRates->all();
    }

    /**
     * {@inheritdoc}
     */
    public function setInvoiceTotal($invoiceTotal)
    {
    }

    /**
     * {@inheritdoc}
     */
    public function getInvoiceTotal()
    {
    }

    /**
     * Sets the invoice total employee cost.
     *
     * @param $employeeTotal
     *
     * @return mixed
     */
    public function setInvoiceTotalEmployees($employeeTotal)
    {
        $this->invoiceEmployeeTotal = $employeeTotal;
    }

    /**
     * Gets the invoice total employee cost.
     *
     * @return mixed
     */
    public function getInvoiceTotalEmployees()
    {
        return $this->invoiceEmployeeTotal;
    }


    /**
     * {@inheritdoc}
     */
    public function setInvoiceTotalMaterials($materialsTotal)
    {
        $this->invoiceMaterialTotal = $materialsTotal;
    }

    /**
     * {@inheritdoc}
     */
    public function getInvoiceTotalMaterials()
    {
        return $this->invoiceMaterialTotal;
    }

    /**
     * {@inheritdoc}
     */
    public function setInvoiceTotalEquipmentUnits($equipmentUnitsTotal)
    {
        $this->invoiceEquipmentTotal = $equipmentUnitsTotal;
    }

    /**
     * {@inheritdoc}
     */
    public function getInvoiceTotalEquipmentUnits()
    {
        return $this->invoiceEquipmentTotal;
    }

    /**
     * {@inheritdoc}
     */
    public function setInvoiceTotalFuels($fuelsTotal)
    {
        $this->invoiceFuelTotal = $fuelsTotal;
    }

    /**
     * {@inheritdoc}
     */
    public function getInvoiceTotalFuels()
    {
        return $this->invoiceFuelTotal;
    }

    /**
     * {@inheritdoc}
     */
    public function incrementInvoiceTotal($incrementAmount)
    {
    }

    /**
     * {@inheritdoc}
     */
    public function incrementInvoiceTotalMaterials($incrementAmount)
    {
        $this->invoiceMaterialTotal = $this->mathEngine->add($this->invoiceMaterialTotal, $incrementAmount);
    }

    /**
     * {@inheritdoc}
     */
    public function incrementInvoiceTotalMaterialsTaxable($incrementAmount)
    {
        $this->invoiceMaterialTotalTaxable =
            $this->mathEngine->add($this->invoiceMaterialTotalTaxable, $incrementAmount);
    }

    /**
     * {@inheritdoc}
     */
    public function incrementInvoiceEquipmentUnits($incrementAmount)
    {
        $this->invoiceEquipmentTotal = $this->mathEngine->add($this->invoiceEquipmentTotal, $incrementAmount);
    }

    /**
     * {@inheritdoc}
     */
    public function incrementInvoiceEquipmentUnitsTaxable($incrementAmount)
    {
        $this->invoiceEquipmentTotalTaxable =
            $this->mathEngine->add($this->invoiceEquipmentTotalTaxable, $incrementAmount);
    }

    /**
     * {@inheritdoc}
     */
    public function incrementInvoiceFuel($incrementAmount)
    {
        $this->invoiceFuelTotal = $this->mathEngine->add($this->invoiceFuelTotal, $incrementAmount);
    }

    /**
     * {@inheritdoc}
     */
    public function incrementInvoiceFuelTaxable($incrementAmount)
    {
        $this->invoiceFuelTotalTaxable = $this->mathEngine->add($this->invoiceFuelTotalTaxable, $incrementAmount);
    }

    /**
     * {@inheritdoc}
     */
    public function addWorkRecord($workRecordID)
    {
        $this->workRecords[$workRecordID] = $workRecordID;
    }

    /**
     * {@inheritdoc}
     */
    public function removeWorkRecord($workRecordID)
    {
        DB::connection(\Tenant::getCurrentConnection())->beginTransaction();

        try {
            DB::connection(\Tenant::getCurrentConnection())->table('fs_invoice_work_records')->where('work_record_id')
              ->delete();
            DB::connection(\Tenant::getCurrentConnection())->commit();

            return true;
        } catch (\Exception $e) {
        }

        DB::connection(\Tenant::getCurrentConnection())->rollback();

        return false;
    }

    /**
     * Sets the invoice total override.
     *
     * @param $totalOverride
     * @return mixed
     */
    public function setInvoiceTotalOverride($totalOverride)
    {
        $this->invoiceTotalOverride = $totalOverride;
    }

    /**
     * Gets the invoice total override.
     *
     * @return mixed
     */
    public function getInvoiceTotalOverride()
    {
        return $this->invoiceTotalOverride;
    }

    /**
     * Removes a collection of tax lines from an invoice.
     *
     * @param array $taxLines
     * @return mixed
     */
    public function removeTaxLinesFromInvoice(array $taxLines)
    {
        DB::connection(\Tenant::getCurrentConnection())->beginTransaction();

        try {
            DB::connection(\Tenant::getCurrentConnection())->table('fs_invoices_tax_rates')->whereIn('id', $taxLines)
              ->delete();
            DB::connection(\Tenant::getCurrentConnection())->commit();

            return true;
        } catch (\Exception $e) {
        }

        DB::connection(\Tenant::getCurrentConnection())->rollback();

        return false;
    }

    /**
     * Syncs the invoice's balance automatically.
     *
     * @param $invoiceID
     * @return mixed
     */
    public function syncInvoiceBalance($invoiceID)
    {
        /*
         $this->table('fs_transactions')->where('associated_context', '=', 1)->where(function($query) use ($invoiceID)
            {
                $query->where('associated_value', '=', $invoiceID);
            })->where(function($query) {
                $query->where('is_primary_transaction', '=', 1);
            })->update([
                'begin_account' => $newBillToAccount,
                'end_account' => $newPayToAccount
                                   ]);
         */
        DB::connection(\Tenant::getCurrentConnection())->beginTransaction();
        try {
            $paymentsMade = DB::connection(\Tenant::getCurrentConnection())->table('fs_transactions')
                              ->where('associated_context', '=', 1)->where(function ($query) use ($invoiceID) {
                    $query->where('associated_value', '=', $invoiceID);
                })->where(function ($query) {
                    $query->where('is_primary_transaction', '=', 1);
                })->where(function ($query) {
                    $query->where('transaction_status', '=', 1);
                })->select(DB::raw('SUM(amount) as payments_made'))->first();

            $invoiceTotal =
                DB::connection(\Tenant::getCurrentConnection())->table('fs_invoices')->where('id', '=', $invoiceID)
                  ->select('invoice_total')->first();


            $newBalance = $invoiceTotal->invoice_total - $paymentsMade->payments_made;


            DB::connection(\Tenant::getCurrentConnection())->table('fs_invoices')->where('id', '=', $invoiceID)
              ->update(['cached_invoice_balance' => $newBalance]);


            DB::connection(\Tenant::getCurrentConnection())->commit();

            return true;
        } catch (Exception $e) {
            lk($e);
        }

        DB::connection(\Tenant::getCurrentConnection())->rollback();

        return false;
    }


    /**
     * {@inheritdoc}
     */
    abstract public function commit();
}
