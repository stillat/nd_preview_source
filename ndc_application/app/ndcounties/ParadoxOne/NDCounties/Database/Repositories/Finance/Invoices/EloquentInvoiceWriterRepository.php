<?php namespace ParadoxOne\NDCounties\Database\Repositories\Finance\Invoices;

use Exception;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\Facades\Input;
use Service\Finances\Invoices\Invoice;
use Service\Finances\Invoices\InvoiceTaxRate;
use Service\Finances\Invoices\InvoiceWorkRecord;

class EloquentInvoiceWriterRepository extends InvoiceWriterRepository
{
    public function calculateTaxAmount()
    {
        $totalTax = 0;

        $employeeTax  = $this->mathEngine->mul($this->invoiceEmployeeTaxRateTotal, $this->invoiceEmployeeTotal);
        $equipmentTax =
            $this->mathEngine->mul($this->invoiceEquipmentTaxRateTotal, $this->invoiceEquipmentTotalTaxable);
        $materialTax  = $this->mathEngine->mul($this->invoiceMaterialTaxRateTotal, $this->invoiceMaterialTotalTaxable);
        $fuelTax      = $this->mathEngine->mul($this->invoiceFuelTaxRateTotal, $this->invoiceFuelTotalTaxable);

        $totalTax = $this->mathEngine->add($totalTax, $employeeTax);
        $totalTax = $this->mathEngine->add($totalTax, $equipmentTax);
        $totalTax = $this->mathEngine->add($totalTax, $materialTax);
        $totalTax = $this->mathEngine->add($totalTax, $fuelTax);

        return $totalTax;
    }

    public function calculateSubTotal()
    {
        $subTotal = 0;

        $subTotal = $this->mathEngine->add($subTotal, $this->invoiceEmployeeTotal);
        $subTotal = $this->mathEngine->add($subTotal, $this->invoiceEquipmentTotal);
        $subTotal = $this->mathEngine->add($subTotal, $this->invoiceFuelTotal);
        $subTotal = $this->mathEngine->add($subTotal, $this->invoiceMaterialTotal);

        $miscTotal = $this->mathEngine->mul($this->miscBillingRate, $this->miscBillingQuantity);
        $subTotal  = $this->mathEngine->add($subTotal, $miscTotal);

        return $subTotal;
    }

    public function calculateDiscountAmount($subTotal)
    {
        $percentage = $this->mathEngine->div($this->invoiceDiscountAmount, 100);

        return $this->mathEngine->mul($percentage, $subTotal);
    }

    public $publicAPIInvoiceID = null;

    /**
     * {@inheritdoc}
     */
    public function append()
    {
        DB::connection(\Tenant::getCurrentConnection())->beginTransaction();

        try {
            $invoiceID                = Input::get('billing_invoice_number');
            $this->publicAPIInvoiceID = $invoiceID;
            $taxAmount                = $this->calculateTaxAmount();
            $subTotal                 = $this->calculateSubTotal();
            $discount                 = $this->calculateDiscountAmount($subTotal);

            $invoiceTotal = $this->mathEngine->sub($subTotal, $discount);
            $invoiceTotal = $this->mathEngine->add($invoiceTotal, $taxAmount);

            if ($this->invoiceTotalOverride !== false) {
                // Override the invoice total here.
                $invoiceTotal = $this->invoiceTotalOverride;
            }

            $appendedInvoice                                = Invoice::find($invoiceID);
            $appendedInvoice->invoice_total                 =
                $this->mathEngine->add($invoiceTotal, $appendedInvoice->invoice_total);
            $appendedInvoice->invoice_total_materials       =
                $this->mathEngine->add($this->invoiceMaterialTotal, $appendedInvoice->invoice_total_materials);
            $appendedInvoice->invoice_total_employee_costs  =
                $this->mathEngine->add($this->invoiceEmployeeTotal, $appendedInvoice->invoice_total_employee_costs);
            $appendedInvoice->invoice_total_equipment_units =
                $this->mathEngine->add($this->invoiceEquipmentTotal, $appendedInvoice->invoice_total_equipment_units);
            $appendedInvoice->invoice_total_fuels           =
                $this->mathEngine->add($this->invoiceFuelTotal, $appendedInvoice->invoice_total_fuels);
            $appendedInvoice->invoice_sub_total             =
                $this->mathEngine->add($subTotal, $appendedInvoice->invoice_sub_total);
            $appendedInvoice->invoice_discount_total        =
                $this->mathEngine->add($discount, $appendedInvoice->invoice_discount_total);
            $appendedInvoice->invoice_tax_amount            =
                $this->mathEngine->add($taxAmount, $appendedInvoice->invoice_tax_amount);
            $appendedInvoice->client_notes                  = $this->invoiceClientNotes;
            $appendedInvoice->invoice_terms                 = $this->invoiceTerms;

            $appendedInvoice->save();

            if ($this->invoiceTaxRates != null) {
                $this->writeNewInvoiceTaxRates($invoiceID);
            }

            if ($this->invoiceUpdateTaxRates != null) {
                $this->updateExistingInvoiceTaxRates();
            }

            foreach ($this->workRecords as $workRecord) {
                // Let's trash any old work record/invoice pairings that migh exist.
                DB::connection(\Tenant::getCurrentConnection())->table('fs_invoice_work_records')
                  ->where('invoice_id', '=', $appendedInvoice->id)->where(function ($query) use ($workRecord) {
                        $query->where('work_record_id', '=', $workRecord);
                    })->delete();
                try {
                    $invoiceWorkRecord                 = new InvoiceWorkRecord;
                    $invoiceWorkRecord->invoice_id     = $appendedInvoice->id;
                    $invoiceWorkRecord->work_record_id = $workRecord;
                    $invoiceWorkRecord->save();
                } catch (Exception $e) {
                    DB::connection(\Tenant::getCurrentConnection())->rollback();
                    throw $e;
                }
            }


            DB::connection(\Tenant::getCurrentConnection())->commit();
            $this->syncInvoiceBalance($invoiceID);
            return true;
        } catch (Exception $e) {
            DB::connection(\Tenant::getCurrentConnection())->rollback();

            return false;
        }
    }

    /**
     * Updates existing tax rate records.
     *
     * @throws Exception
     */
    private function updateExistingInvoiceTaxRates()
    {
        // This loop goes through and updates EXISTING tax records.
        foreach ($this->invoiceUpdateTaxRates as $taxRate) {
            DB::connection(\Tenant::getCurrentConnection())->beginTransaction();

            try {
                $taxRecordID = $taxRate[6];

                DB::connection(\Tenant::getCurrentConnection())->table('fs_invoices_tax_rates')
                  ->where('id', '=', $taxRecordID)
                  ->update([
                               'tax_id'            => $taxRate[0],
                               'historic_tax_rate' => $taxRate[1]
                           ]);

                DB::connection(\Tenant::getCurrentConnection())->commit();
            } catch (Exception $e) {
                DB::connection(\Tenant::getCurrentConnection())->rollback();
                throw $e;
            }
        }
    }

    /**
     * Writes new invoice tax rate records.
     *
     * @param $invoiceID
     * @throws Exception
     */
    private function writeNewInvoiceTaxRates($invoiceID)
    {
        // This loop goes and adds any NEW tax records.
        foreach ($this->invoiceTaxRates as $taxRate) {
            DB::connection(\Tenant::getCurrentConnection())->beginTransaction();
            try {
                $newInvoiceTaxRate                    = new InvoiceTaxRate;
                $newInvoiceTaxRate->invoice_id        = $invoiceID;
                $newInvoiceTaxRate->historic_tax_rate = $taxRate[1];
                $newInvoiceTaxRate->tax_total         = 0;
                $newInvoiceTaxRate->employee_total    = 0;
                $newInvoiceTaxRate->fuel_total        = 0;
                $newInvoiceTaxRate->equipment_total   = 0;
                $newInvoiceTaxRate->material_total    = 0;
                $newInvoiceTaxRate->save();
                DB::connection(\Tenant::getCurrentConnection())->commit();
            } catch (Exception $e) {
                DB::connection(\Tenant::getCurrentConnection())->rollback();
                throw $e;
            }
        }
    }

    /**
     * {@inheritdoc}
     */
    public function commit()
    {
        DB::connection(\Tenant::getCurrentConnection())->beginTransaction();

        try {
            $taxAmount = $this->calculateTaxAmount();
            $subTotal  = $this->calculateSubTotal();
            $discount  = $this->calculateDiscountAmount($subTotal);

            $invoiceTotal = $this->mathEngine->sub($subTotal, $discount);
            $invoiceTotal = $this->mathEngine->add($invoiceTotal, $taxAmount);

            $newInvoice                                = new Invoice;
            $newInvoice->due_date                      = $this->invoiceDueDate;
            $newInvoice->invoice_number                = $this->invoiceNumber;
            $newInvoice->account_number                = $this->invoiceAccountNumber;
            $newInvoice->paid_to_account               = $this->invoicePaidToAccountNumber;
            $newInvoice->discount                      = $this->invoiceDiscountAmount;
            $newInvoice->invoice_total                 = $invoiceTotal;
            $newInvoice->invoice_total_materials       = $this->invoiceMaterialTotal;
            $newInvoice->invoice_total_employee_costs  = $this->invoiceEmployeeTotal;
            $newInvoice->invoice_total_equipment_units = $this->invoiceEquipmentTotal;
            $newInvoice->invoice_total_fuels           = $this->invoiceFuelTotal;
            $newInvoice->invoice_tax_amount            = $taxAmount;
            $newInvoice->invoice_sub_total             = $subTotal;
            $newInvoice->invoice_discount_total        = $discount;
            $newInvoice->client_notes                  = $this->invoiceClientNotes;
            $newInvoice->invoice_terms                 = $this->invoiceTerms;
            $newInvoice->save();
            $this->publicAPIInvoiceID = $newInvoice->id;

            $this->writeNewInvoiceTaxRates($newInvoice->id);

            foreach ($this->workRecords as $workRecord) {
                try {
                    $invoiceWorkRecord                 = new InvoiceWorkRecord;
                    $invoiceWorkRecord->invoice_id     = $newInvoice->id;
                    $invoiceWorkRecord->work_record_id = $workRecord;
                    $invoiceWorkRecord->save();
                } catch (Exception $e) {
                    DB::connection(\Tenant::getCurrentConnection())->rollback();
                    throw $e;
                }
            }
            // Raise the event required.
            if (count($this->workRecords) == 1) {
                $requiredWorkRecordID = array_pop($this->workRecords);
                Event::fire('invoice.automated_work.created', array($newInvoice, $requiredWorkRecordID));
            }

            DB::connection(\Tenant::getCurrentConnection())->commit();
            $this->syncInvoiceBalance($newInvoice->id);
            return true;
        } catch (Exception $e) {
            DB::connection(\Tenant::getCurrentConnection())->rollback();

            return false;
        }
    }
}
