<?php namespace ParadoxOne\NDCounties\Database\Repositories\Finance;

use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use ParadoxOne\NDCounties\Contracts\Finance\TransactionRepositoryInterface;
use ParadoxOne\NDCounties\Database\Repositories\BaseTenantRepository;
use ParadoxOne\NDCounties\IdentificationCode as IDCode;
use Service\Finances\Transaction;

class TransactionRepository extends BaseTenantRepository implements TransactionRepositoryInterface
{
    protected $table = 'fs_transactions';

    private $refreshAccountBalancesQuery = 'SET SQL_SAFE_UPDATES = 0;update fs_accounts set fs_accounts.balance = IFNULL((select sum(fs_transactions.amount) from fs_transactions where fs_transactions.begin_account = fs_accounts.id and fs_transactions.transaction_status = 1), 0);SET SQL_SAFE_UPDATES = 1;';

    public function create(array $createInfo = array())
    {
        return false;
    }

    public function update($id, array $updateInfo = array())
    {
        return false;
    }

    public function remove(array $removeContext = array())
    {
        return false;
    }

    public function getAccountTransactions($accountID)
    {
        return Transaction::where('begin_account', '=', $accountID)
                          ->orWhere('end_account', '=', $accountID)
                          ->orderBy('created_at', 'desc')
                          ->with('destination')
                          ->with('beginAccount');
    }

    public function getInvoiceTransactions($invoiceID, $paidToAccount)
    {
        return Transaction::orderBy('created_at', 'desc')->with('destination')->with('beginAccount')
            ->where('associated_context', '=', 1)->where('associated_value', '=', $invoiceID)
            ->where(function ($query) use ($paidToAccount) {
                $query->where('end_account', '=', $paidToAccount);
            })->where('is_primary_transaction', '=', true);
    }

    public function getModelByID($id)
    {
        return Transaction::findOrFail($id);
    }

    /**
     * Refreshes all account balances.
     *
     * @return mixed
     */
    public function refreshAccountBalances()
    {
        DB::connection($this->getConnection())->beginTransaction();

        try {
            DB::connection($this->getConnection())
              ->statement('UPDATE fs_accounts SET balance = IFNULL((SELECT SUM(fs_transactions.amount) FROM fs_transactions WHERE end_account = fs_accounts.id AND transaction_status = 1), 0) WHERE id > 0;');

            DB::connection($this->getConnection())->commit();

            return true;
        } catch (\Exception $e) {
        }

        DB::connection($this->getConnection())->rollback();

        return false;
    }


    /**
     * Updates the status indicators for the given transactions.
     *
     * @param array $transactions
     * @param       $newStatus
     * @return mixed
     */
    public function updateTransactionStatuses(array $transactions, $newStatus)
    {
        if (count($transactions) == 0) {
            return false;
        }

        $validStatuses =
            array_keys(App::make('ParadoxOne\NDCounties\Contracts\Finance\TransactionStatusRepositoryInterface')
                          ->getStatuses());

        if (!in_array($newStatus, $validStatuses)) {
            return false;
        }

        DB::connection($this->getConnection())->beginTransaction();

        try {
            $batches = $this->table()->whereIn('id', $transactions)->lists('batch');
            $this->table()->whereIn('batch', $batches)->update([
                                                                   'transaction_status' => $newStatus
                                                               ]);
            DB::connection($this->getConnection())->commit();

            return true;
        } catch (\Exception $e) {
        }

        DB::connection($this->getConnection())->rollback();

        return false;
    }


    public function transferAmountToAccount($beginAccount, $destination, $amount,
                                            $authorizedBy, $transactionMessage, $transactionStatus, $workRecordID, $context = 0, $contextValue = 0)
    {
        try {
            DB::connection($this->getConnection())->beginTransaction();

            if ($transactionStatus == 1) {
                DB::connection($this->getConnection())->table('fs_accounts')->where('id', '=', $beginAccount)
                  ->decrement('balance', $amount);
                DB::connection($this->getConnection())->table('fs_accounts')->where('id', '=', $destination)
                  ->increment('balance', $amount);
            }

            // This is where we will store the actual transaction record.
            $batchNumber = IDCode::prepareFrom('', 50);

            $transactionOne                     = new Transaction;
            $transactionOne->batch              = $batchNumber;
            $transactionOne->begin_account      = $beginAccount;
            $transactionOne->end_account        = $destination;
            $transactionOne->amount             = $amount;
            $transactionOne->authorized_by      = $authorizedBy;
            $transactionOne->transaction_status = $transactionStatus;
            $transactionOne->description        = $transactionMessage;
            $transactionOne->work_record_id     = $workRecordID;
            $transactionOne->associated_context = $context;
            $transactionOne->associated_value = $contextValue;
            $transactionOne->is_primary_transaction = true;
            $transactionOne->save();

            $transactionTwo                     = new Transaction;
            $transactionTwo->batch              = $batchNumber;
            $transactionTwo->begin_account      = $destination;
            $transactionTwo->end_account        = $beginAccount;
            $transactionTwo->amount             = ((-1) * $amount);
            $transactionTwo->authorized_by      = $authorizedBy;
            $transactionTwo->transaction_status = $transactionStatus;
            $transactionTwo->description        = $transactionMessage;
            $transactionTwo->work_record_id     = $workRecordID;
            $transactionTwo->associated_context = $context;
            $transactionTwo->associated_value = $contextValue;
            $transactionTwo->is_primary_transaction = false;
            $transactionTwo->save();

            DB::connection($this->getConnection())->commit();

            return $batchNumber;
        } catch (Exception $e) {
            return false;
        }
    }
}
