<?php namespace ParadoxOne\NDCounties\Database\Repositories\Finance;

use Exception;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Image;
use ParadoxOne\NDCounties\Contracts\Finance\AccountRepositoryInterface;
use ParadoxOne\NDCounties\Database\Repositories\BaseTenantRepository;
use ParadoxOne\NDCounties\IdentificationCode as IDCode;
use ParadoxOne\NDCounties\Settings\SortingSettingsManager;
use ParadoxOne\NDCounties\Traits\AdvancedSearchPreparationTrait;
use ParadoxOne\NDCounties\Traits\DefaultSortableTrait;
use ParadoxOne\NDCounties\Traits\UnTrashedQueryBuilderTrait;
use Service\Finances\Account;

class AccountRepository extends BaseTenantRepository implements AccountRepositoryInterface
{
    use DefaultSortableTrait, AdvancedSearchPreparationTrait, UnTrashedQueryBuilderTrait;

    protected $table = 'fs_accounts';

    public function getModelByID($id)
    {
        return Account::with('category')->findOrFail($id);
    }

    private function applyAccountsSorting(&$builder)
    {
        $sortSettings = App::make('sorting_work_entry');
        $sortSettings = json_decode($sortSettings->get(SortingSettingsManager::FS_ACCOUNTS_TABLE_SETTING_NAME), true);

        foreach ($sortSettings as $column => $settings) {
            switch ($column) {
                case 'code':
                    $this->applySortingSetting('code', $settings, $builder);
                    break;
                case 'name':
                    $this->applySortingSetting('name', $settings, $builder);
                    break;
                case 'desc':
                    $this->applySortingSetting('description', $settings, $builder);
                    break;
                case 'last_updated':
                    $this->applySortingSetting('updated_at', $settings, $builder);
                    break;
                case 'date_created':
                    $this->applySortingSetting('created_at', $settings, $builder);
                    break;
                case 'category':
                    if ($settings == 'asc') {
                        $builder = $builder->orderBy('fs_accounts_categories.code', 'asc');
                    } elseif ($settings == 'desc') {
                        $builder = $builder->orderBy('fs_accounts_categories.code', 'desc');
                    }
                    break;
                case 'balance':
                    if ($settings == 'asc') {
                        $builder = $builder->orderBy('fs_accounts.balance', 'asc');
                    } elseif ($settings == 'desc') {
                        $builder = $builder->orderBy('fs_accounts.balance', 'desc');
                    }
                    break;
            }
        }

        $sortSettings = null;
        unset($sortSettings);
    }

    public function getAccounts()
    {
        $builder = $this->table()->select('fs_accounts.id', 'fs_accounts.code', 'fs_accounts.name', 'balance', 'deleted_at',
            'fs_accounts.description', 'created_at',
            'fs_accounts_categories.id as category_id',
            'fs_accounts_categories.name AS category', 'fs_accounts_categories.color',
            'fs_accounts_categories.code as prefix', DB::raw('CONCAT_WS("-", fs_accounts_categories.code, fs_accounts.name) as prefixed_name'))
                        ->join('fs_accounts_categories', 'fs_accounts.category_id', '=', 'fs_accounts_categories.id')
                        ->whereNull('deleted_at');
        $this->applyAccountsSorting($builder);

        return $builder;
    }

    public function getAccountsWithTrashed()
    {
        $builder = $this->table()->select('fs_accounts.id', 'fs_accounts.code', 'fs_accounts.name', 'balance', 'deleted_at',
            'fs_accounts.description', 'created_at',
            'fs_accounts_categories.id as category_id',
            'fs_accounts_categories.name AS category', 'fs_accounts_categories.color',
            'fs_accounts_categories.code as prefix', DB::raw('CONCAT_WS("-", fs_accounts_categories.code, fs_accounts.name) as prefixed_name'))
            ->join('fs_accounts_categories', 'fs_accounts.category_id', '=', 'fs_accounts_categories.id');
        $this->applyAccountsSorting($builder);

        return $builder;
    }

    public function search($searchTerms)
    {
        // $includeTrashed = (Input::get('it', 0) == 1);
        $includeTrashed = Input::has('it');
        if (Input::get('is', null) !== null) {
            return $this->table()->where('id', '=', Input::get('is'))->get();
        }

        if ($searchTerms === '//') {
            $builder = $this->table();

            if (!$includeTrashed) {
                $builder->whereNull('deleted_at');
            }

            return $builder->get();
        }

        $builder = $this->table()
            ->orWhere(function ($query) use ($searchTerms) {
                // This used to search on name and description as well.
                $query->where('code', 'LIKE', $this->prepareSearch($searchTerms));
                $query->orWhere('name', 'LIKE', $this->prepareSearch($searchTerms));
            });

        if (!$includeTrashed) {
            $builder->where(function ($query) {
                $query->whereNull('deleted_at');
            });
        }

        // Hopefully this will put the shortest option first.
        $builder->orderByRaw('LENGTH(code)');

        return $builder->get();
    }

    public function create(array $accountInformation = array())
    {
        $this->startTransaction();
        $account                 = new Account;
        $account->code           = IDCode::prepareFrom($accountInformation['code']);
        $account->name           = $accountInformation['name'];
        $account->description    = $accountInformation['description'];
        $account->category_id    = $accountInformation['category'];
        $account->address_line_1 = $accountInformation['address_line_1'];
        $account->address_line_2 = $accountInformation['address_line_2'];
        $account->city           = $accountInformation['city'];
        $account->state          = $accountInformation['state'];
        $account->country        = $accountInformation['country'];
        $account->zip            = $accountInformation['zip'];

        if (array_key_exists('cash_account', $accountInformation)) {
            $account->is_cash_account = true;
        } else {
            $account->is_cash_account = false;
        }

        $validationRules         = Account::$rules;
        $validationRules['code'] = Account::$rules['code'] . ',NULL,id,category_id,' . $accountInformation['category'] .
                                   ',deleted_at,NULL';
        $validationRules['name'] = Account::$rules['name'] . ',NULL,id,category_id,' . $accountInformation['category'] .
                                   ',deleted_at,NULL';

        if ($account->validate($validationRules)) {
            $possibleRestore = Account::withTrashed()->where('category_id', '=', $account->category_id)->where('code', '=', $account->code)->first();
            if ($possibleRestore != null) {
                $possibleRestore->name           = $accountInformation['name'];
                $possibleRestore->description    = $accountInformation['description'];
                $possibleRestore->address_line_1 = $accountInformation['address_line_1'];
                $possibleRestore->address_line_2 = $accountInformation['address_line_2'];
                $possibleRestore->city           = $accountInformation['city'];
                $possibleRestore->state          = $accountInformation['state'];
                $possibleRestore->country        = $accountInformation['country'];
                $possibleRestore->zip            = $accountInformation['zip'];
                $possibleRestore->deleted_at = null;

                if (array_key_exists('cash_account', $accountInformation)) {
                    $possibleRestore->is_cash_account = true;
                } else {
                    $possibleRestore->is_cash_account = false;
                }

                $validationRules         = Account::$rules;
                $validationRules['code'] = Account::$rules['code'] . ',' . $possibleRestore->id . ',id,category_id,' .
                    $accountInformation['category'] . ',deleted_at,NULL';
                $validationRules['name'] = Account::$rules['name'] . ',' . $possibleRestore->id . ',id,category_id,' .
                    $accountInformation['category'] . ',deleted_at,NULL';
                $saved = $possibleRestore->save($validationRules);
                $this->commitTransaction();
                Event::fire('finance.accounts.created', [$possibleRestore]);

                return $possibleRestore;
            }
        }

        if ($account->save($validationRules)
        ) {
            $this->commitTransaction();
            Event::fire('finance.accounts.created', [$account]);

            return $account;
        } else {
            $this->errors = $account->errors();
        }

        $this->rollbackTransaction();

        return false;
    }

    public function remove(array $removeDetails = array())
    {
        $this->startTransaction();
        try {
            if ($removeDetails[0] != 0) {
                Account::destroy(intval($removeDetails[0]));
                Event::fire('finance.account.removed', array($removeDetails[0]));
                $this->commitTransaction();

                return true;
            }
        } catch (Exception $e) {
        }

        $this->rollbackTransaction();

        return false;
    }

    public function update($accountID, array $updateDetails = array())
    {
        $account = Account::findOrFail($accountID);
        $this->startTransaction();
        $account->name        = $updateDetails['name'];
        $account->code        = IDCode::prepareFrom($updateDetails['code']);
        $account->description = $updateDetails['description'];
        $account->category_id = $updateDetails['category'];

        $account->address_line_1 = $updateDetails['address_line_1'];
        $account->address_line_2 = $updateDetails['address_line_2'];
        $account->city           = $updateDetails['city'];
        $account->state          = $updateDetails['state'];
        $account->country        = $updateDetails['country'];
        $account->zip            = $updateDetails['zip'];


        if (array_key_exists('cash_account', $updateDetails)) {
            $account->is_cash_account = true;
        } else {
            $account->is_cash_account = false;
        }

        $validationRules         = Account::$rules;
        $validationRules['code'] = Account::$rules['code'] . ',' . $accountID . ',id,category_id,' .
                                   $updateDetails['category'] . ',deleted_at,NULL';
        $validationRules['name'] = Account::$rules['name'] . ',' . $accountID . ',id,category_id,' .
                                   $updateDetails['category'] . ',deleted_at,NULL';

        if ($account->save($validationRules)) {
            Event::fire('finance.account.updated', [$account]);
            $this->commitTransaction();

            return $account;
        } else {
            $this->errors = $account->errors();
        }

        $this->rollbackTransaction();

        return false;
    }

    public function unTrash($financeAccounts)
    {
        $this->startTransaction();

        if (!is_array($financeAccounts)) {
            $financeAccounts = (array)$financeAccounts;
        }

        if (count($financeAccounts) == 0) {
            return false;
        }

        try {
            $restoreData = $this->getRecordsThatCantBeRestored($this->table, $financeAccounts);

            if (count($restoreData->recordsThatCanBeRestored) == 0) {
                // If we've reached this point, there are no records that can be restored
                // due to conflicts.
                Event::fire('finance.account.restored.conflictsDetected', [$restoreData->similarRecords]);
                $this->rollbackTransaction();

                // Return true because of reasons.
                return true;
            }

            $this->table()->whereIn('id', $financeAccounts)->update([
                'deleted_at' => null
            ]);
            Event::fire('finance.account.restored', [$financeAccounts]);
            $this->commitTransaction();
            return true;
        } catch (\Exception $e) {
        }

        $this->rollbackTransaction();

        return false;
    }

}
