<?php namespace ParadoxOne\NDCounties\Database\Repositories\Finance;

use ParadoxOne\NDCounties\Contracts\Finance\InvoiceRepositoryInterface;
use ParadoxOne\NDCounties\Database\Repositories\BaseTenantRepository;

class InvoiceRepository extends BaseTenantRepository implements InvoiceRepositoryInterface
{
    protected $table = 'fs_invoices';

    public function create(array $invoiceData = array())
    {
    }

    public function remove(array $removeData = array())
    {
    }

    public function update($invoiceID, array $updateData = array())
    {
    }
}
