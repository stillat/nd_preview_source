<?php namespace ParadoxOne\NDCounties\Database\Repositories\Finance\Invoices\Builders;

trait InformationRefreshTrait
{
    /**
     * Indicates if invoices self destruct.
     *
     * @var bool
     */
    protected $doesSelfDestruct = true;

    /**
     * Refreshes the invoice's information.
     *
     * @param int $invoiceID
     * @return mixed
     */
    public function refreshInformation($invoiceID)
    {
    }

    /**
     * Sets whether or not the invoice removes itself when it has no
     * work records.
     *
     * @param $doesSelfDestruct
     * @return mixed
     */
    public function setSelfDestructOnEmpty($doesSelfDestruct)
    {
        $this->doesSelfDestruct = $doesSelfDestruct;
    }

    /**
     * Gets the self destruct setting.
     *
     * @return mixed
     */
    public function getSelfDestructOnEmpty()
    {
        return $this->doesSelfDestruct;
    }
}
