<?php namespace ParadoxOne\NDCounties\Database\Repositories\Finance\Taxes;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\App;
use ParadoxOne\NDCounties\Settings\SortingSettingsManager;
use ParadoxOne\NDCounties\Contracts\Finance\Taxes\TaxRateRepositoryInterface;
use ParadoxOne\NDCounties\Database\Repositories\BaseTenantRepository;
use ParadoxOne\NDCounties\Traits\DefaultSortableTrait;
use ParadoxOne\NDCounties\Traits\UnTrashedQueryBuilderTrait;
use Service\Finances\Taxes\TaxRate;

class TaxRateRepository extends BaseTenantRepository implements TaxRateRepositoryInterface
{
    use DefaultSortableTrait;
    use UnTrashedQueryBuilderTrait;

    protected $table = 'fs_tax_rates';

    private function applyTaxRatesSorting(&$builder)
    {
        $sortSettings = App::make('sorting_work_entry');
        $sortSettings = json_decode($sortSettings->get(SortingSettingsManager::FS_TAX_RATES_TABLE_SETTING_NAME), true);

        foreach ($sortSettings as $column => $settings) {
            switch ($column) {
                case 'name':
                    $this->applySortingSetting('tax_name', $settings, $builder);
                    break;
                case 'last_updated':
                    $this->applySortingSetting('updated_at', $settings, $builder);
                    break;
                case 'date_created':
                    $this->applySortingSetting('created_at', $settings, $builder);
                    break;
                case 'tax_rate':
                    if ($settings == 'asc') {
                        $builder = $builder->orderBy('fs_tax_rates.tax_rate', 'asc');
                    } elseif ($settings == 'desc') {
                        $builder = $builder->orderBy('fs_tax_rates.tax_rate', 'desc');
                    }
                    break;
                case 'a_employee':
                    if ($settings == 'asc') {
                        $builder = $builder->orderBy('fs_tax_rates.affects_employees', 'asc');
                    } elseif ($settings == 'desc') {
                        $builder = $builder->orderBy('fs_tax_rates.affects_employees', 'desc');
                    }
                    break;
                case 'a_equipment':
                    if ($settings == 'asc') {
                        $builder = $builder->orderBy('fs_tax_rates.affects_equipments', 'asc');
                    } elseif ($settings == 'desc') {
                        $builder = $builder->orderBy('fs_tax_rates.affects_equipments', 'desc');
                    }
                    break;
                case 'a_fuels':
                    if ($settings == 'asc') {
                        $builder = $builder->orderBy('fs_tax_rates.affects_fuels', 'asc');
                    } elseif ($settings == 'desc') {
                        $builder = $builder->orderBy('fs_tax_rates.affects_fuels', 'desc');
                    }
                    break;
                case 'a_materials':
                    if ($settings == 'asc') {
                        $builder = $builder->orderBy('fs_tax_rates.affects_materials', 'asc');
                    } elseif ($settings == 'desc') {
                        $builder = $builder->orderBy('fs_tax_rates.affects_materials', 'desc');
                    }
                    break;
            }
        }
    }


    /**
     * {@inheritdoc}
     */
    public function getTaxRates()
    {
        $builder = $this->table()
                    ->select('id', 'tax_name', 'tax_rate', 'affects_all', 'affects_employees', 'affects_equipments',
                             'affects_fuels', 'affects_materials', 'deleted_at')->whereNull('deleted_at');
        $this->applyTaxRatesSorting($builder);
        return $builder;
    }

    public function getTaxRatesWithTrashed()
    {
        $builder = $this->table()
            ->select('id', 'tax_name', 'tax_rate', 'affects_all', 'affects_employees', 'affects_equipments',
                'affects_fuels', 'affects_materials', 'deleted_at');
        $this->applyTaxRatesSorting($builder);
        return $builder;
    }

    public function unTrash($taxRates)
    {
        $this->startTransaction();

        if (!is_array($taxRates)) {
            $taxRates = (array)$taxRates;
        }

        if (count($taxRates) == 0) {
            return false;
        }

        try {
            $restoreData = $this->getRecordsThatCantBeRestored($this->table, $taxRates);

            if (count($restoreData->recordsThatCanBeRestored) == 0) {
                // If we've reached this point, there are no records that can be restored
                // due to conflicts.
                Event::fire('finance.taxrate.restored.conflictsDetected', [$restoreData->similarRecords]);
                $this->rollbackTransaction();

                // Return true because of reasons.
                return true;
            }

            $this->table()->whereIn('id', $taxRates)->update([
                'deleted_at' => null
            ]);
            Event::fire('finance.taxrate.restored', [$taxRates]);
            $this->commitTransaction();
            return true;
        } catch (\Exception $e) {
        }

        $this->rollbackTransaction();

        return false;
    }

    /**
     * {@inheritdoc}
     */
    public function getTaxRatesForEmployees()
    {
        return $this->getTaxRates()->where('affects_employees', '=', true)->orWhere('affects_all', '=', true);
    }

    /**
     * {@inheritdoc}
     */
    public function getTaxRatesForEquipmentUnits()
    {
        return $this->getTaxRates()->where('affects_equipments', '=', true)->orWhere('affects_all', '=', true);
    }

    /**
     * {@inheritdoc}
     */
    public function getTaxRatesForFuels()
    {
        return $this->getTaxRates()->where('affects_fuels', '=', true)->orWhere('affects_all', '=', true);
    }

    /**
     * {@inheritdoc}
     */
    public function getTaxRatesForMaterials()
    {
        return $this->getTaxRates()->where('affects_materials', '=', true)->orWhere('affects_all', '=', true);
    }

    /**
     * {@inheritdoc}
     */
    public function search($searchTerms)
    {
        $includeTrashed = Input::has('it');

        if (Input::get('is', null) !== null) {
            return $this->getTaxRates()
                        ->select(DB::raw('id, tax_name as code, tax_name as name, tax_rate, affects_all, affects_employees, affects_equipments, affects_fuels, affects_materials'))
                        ->where('id', '=', Input::get('is'))->get();
        }

        if ($searchTerms === '//') {
            return $this->getTaxRates()
                        ->select(DB::raw('id, tax_name as code, tax_name as name, tax_rate, affects_all, affects_employees, affects_equipments, affects_fuels, affects_materials'))
                        ->get();
        }

        if ($includeTrashed) {
            $taxRates = $this->getTaxRatesWithTrashed();
        } else {
            $taxRates = $this->getTaxRates();
        }

        return $taxRates->select(DB::raw('id, tax_name as code, tax_name as name, tax_rate, affects_all, affects_employees, affects_equipments, affects_fuels, affects_materials'))
                    ->where('tax_name', 'LIKE', '%' . $searchTerms . '%')->get();
    }

    /**
     * {@inheritdoc}
     */
    public function getTaxRateByID($id)
    {
        Event::fire('finance.taxrate.accessed', array($id));

        return TaxRate::findOrFail($id);
    }

    public function create(array $taxRateData = array())
    {
        $this->startTransaction();
        $taxRate           = new TaxRate;
        $taxRate->tax_name = $taxRateData['tax_name'];
        $taxRate->tax_rate = $taxRateData['tax_rate'];

        $taxRate->affects_all        = exists_then_or($taxRateData, 'affects_all');
        $taxRate->affects_employees  = exists_then_or($taxRateData, 'affects_employees');
        $taxRate->affects_equipments = exists_then_or($taxRateData, 'affects_equipments');
        $taxRate->affects_fuels      = exists_then_or($taxRateData, 'affects_fuels');
        $taxRate->affects_materials  = exists_then_or($taxRateData, 'affects_materials');

        if ($taxRate->affects_all) {
            $taxRate->affects_employees = true;
            $taxRate->affects_equipments = true;
            $taxRate->affects_fuels = true;
            $taxRate->affects_materials = true;
        }

        $validationRules = TaxRate::$rules;
        $validationRules['tax_name'] = TaxRate::$rules['tax_name'] . ',NULL,id,deleted_at,NULL';

        if ($taxRate->validate($validationRules)) {
            $possibleRestore = TaxRate::withTrashed()->where('tax_name', '=', $taxRate->tax_name)->first();
            if ($possibleRestore != null) {
                $possibleRestore->tax_rate = $taxRateData['tax_rate'];

                $possibleRestore->affects_all        = exists_then_or($taxRateData, 'affects_all');
                $possibleRestore->affects_employees  = exists_then_or($taxRateData, 'affects_employees');
                $possibleRestore->affects_equipments = exists_then_or($taxRateData, 'affects_equipments');
                $possibleRestore->affects_fuels      = exists_then_or($taxRateData, 'affects_fuels');
                $possibleRestore->affects_materials  = exists_then_or($taxRateData, 'affects_materials');
                $possibleRestore->deleted_at = null;

                if ($possibleRestore->affects_all) {
                    $possibleRestore->affects_employees = true;
                    $possibleRestore->affects_equipments = true;
                    $possibleRestore->affects_fuels = true;
                    $possibleRestore->affects_materials = true;
                }
                $validationRules = TaxRate::$rules;
                $validationRules['tax_name'] = TaxRate::$rules['tax_name'] . ',' . $possibleRestore->id .',id,deleted_at,NULL';
                $possibleRestore->save($validationRules);
                $this->commitTransaction();
                Event::fire('finance.taxrate.created', array($possibleRestore));
                return $possibleRestore;
            }
        }

        if ($taxRate->save($validationRules)
        ) {
            $this->commitTransaction();
            Event::fire('finance.taxrate.created', array($taxRate));
            return $taxRate;
        } else {
            $this->errors = $taxRate->errors();
        }

        $this->rollbackTransaction();
        return false;
    }

    public function update($id, array $taxRateData = array())
    {
        $taxRate           = TaxRate::findOrFail($id);
        $this->startTransaction();
        $taxRate->tax_name = $taxRateData['tax_name'];
        $taxRate->tax_rate = $taxRateData['tax_rate'];

        $taxRate->affects_all        = exists_then_or($taxRateData, 'affects_all');
        $taxRate->affects_employees  = exists_then_or($taxRateData, 'affects_employees');
        $taxRate->affects_equipments = exists_then_or($taxRateData, 'affects_equipments');
        $taxRate->affects_fuels      = exists_then_or($taxRateData, 'affects_fuels');
        $taxRate->affects_materials  = exists_then_or($taxRateData, 'affects_materials');

        if ($taxRate->affects_all) {
            $taxRate->affects_employees = true;
            $taxRate->affects_equipments = true;
            $taxRate->affects_fuels = true;
            $taxRate->affects_materials = true;
        }

        $validationRules = TaxRate::$rules;
        $validationRules['tax_name'] = TaxRate::$rules['tax_name'] . ',' . $taxRate->id .',id,deleted_at,NULL';

        if ($taxRate->save($validationRules)
        ) {
            $this->commitTransaction();
            Event::fire('finance.taxrate.updated', array($taxRate));
            return $taxRate;
        } else {
            $this->errors = $taxRate->errors();
        }

        $this->rollbackTransaction();
        return false;
    }

    public function remove(array $removeData = array())
    {
        $this->startTransaction();
        try {
            if ($removeData[0] != 0) {
                TaxRate::destroy($removeData[0]);
                $this->commitTransaction();
                Event::fire('finance.taxrate.removed', array($removeData[0]));
                return true;
            }
        } catch (Exception $e) {
        }

        $this->rollbackTransaction();
        Event::fire('finance.taxrate.removed.failed', array($removeData[0]));
        return false;
    }
}
