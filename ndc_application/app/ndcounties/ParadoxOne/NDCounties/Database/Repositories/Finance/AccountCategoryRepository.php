<?php namespace ParadoxOne\NDCounties\Database\Repositories\Finance;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Image;
use ParadoxOne\NDCounties\Contracts\Finance\AccountCategoriesRepositoryInterface;
use ParadoxOne\NDCounties\Database\Repositories\BaseTenantRepository;
use ParadoxOne\NDCounties\IdentificationCode as IDCode;
use Service\Finances\AccountCategory;

class AccountCategoryRepository extends BaseTenantRepository implements AccountCategoriesRepositoryInterface
{
    protected $table = 'fs_accounts_categories';

    public function getCategories()
    {
        return $this->table()->select('id', 'code', 'name', 'description', 'color')->orderBy('code', 'asc');
    }

    public function getCategory($id)
    {
        return $this->table()->where('id', '=', intval($id))->get();
    }

    public function getModelByID($id)
    {
        return AccountCategory::findOrFail($id);
    }

    public function create(array $categoryInformation = array())
    {
        $this->startTransaction();
        $category              = new AccountCategory;
        $category->code        = IDCode::prepareFrom($categoryInformation['code']);
        $category->name        = $categoryInformation['name'];
        $category->description = $categoryInformation['description'];
        $category->color       = $categoryInformation['color'];

        Validator::extend('color', 'ParadoxOne\NDCounties\Validators\ValidatorEngine@validateColor');

        $validationRules = AccountCategory::$rules;

        if ($category->save($validationRules)) {
            Event::fire('finance.account.category.created', [$category]);
            $this->commitTransaction();
            return $category;
        } else {
            $this->errors = $category->errors();
        }

        $this->rollbackTransaction();
        return false;
    }

    public function remove(array $removeDetails = array())
    {
        $this->startTransaction();
        try {
            if ($removeDetails[0] != 0) {
                AccountCategory::destroy(intval($removeDetails[0]));
                Event::fire('finance.account.category.removed', [$removeDetails[0]]);
                $this->commitTransaction();
                return true;
            }
        } catch (\Exception $e) {
        }

        $this->rollbackTransaction();
        return false;
    }

    public function update($categoryID, array $updateDetails = array())
    {
        $category              = AccountCategory::findOrFail($categoryID);
        $this->startTransaction();
        $category->code        = IDCode::prepareFrom($updateDetails['code']);
        $category->name        = $updateDetails['name'];
        $category->description = $updateDetails['description'];
        $category->color       = $updateDetails['color'];

        Validator::extend('color', 'ParadoxOne\NDCounties\Validators\ValidatorEngine@validateColor');

        $validationRules = AccountCategory::$rules;

        if ($category->save($validationRules)) {
            Event::fire('finance.account.category.updated', [$category]);
            $this->commitTransaction();
            return $category;
        } else {
            $this->errors = $category->errors();
        }

        $this->rollbackTransaction();
        return false;
    }
}
