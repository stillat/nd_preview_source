<?php namespace ParadoxOne\NDCounties\Database\Repositories\InventoryManagement;

use Illuminate\Support\Facades\DB;
use ParadoxOne\NDCounties\Contracts\InventoryManagement\AdjustmentRepositoryInterface;
use ParadoxOne\NDCounties\Database\Repositories\BaseTenantRepository;
use Service\InventoryManagement\Adjustment;

class AdjustmentRepository extends BaseTenantRepository implements AdjustmentRepositoryInterface
{
    protected $table = 'inventory_adjustments';

    public function removeAdjustmentsForConsumable($consumableID)
    {
        $this->startTransaction();

        try {
            $this->table()->where('consumable_id','=', $consumableID)->delete();
            $this->commitTransaction();
            return true;
        } catch (\Exception $e) {

        }
        $this->rollbackTransaction();
        return false;
    }


    /**
     * Adds a new adjustment to the inventory system.
     *
     * @param $adjustmentData
     * @return mixed
     */
    public function addAdjustment($adjustmentData)
    {
        $this->startTransaction();

        try {
            $adjustment                     = new Adjustment;
            $adjustment->date_adjusted      = $adjustmentData->date_adjusted;
            $adjustment->adjusted_by        = $adjustmentData->adjusted_by;
            $adjustment->consumable_id      = $adjustmentData->consumable_id;
            $adjustment->adjustment_context = $adjustmentData->adjustment_context;
            $adjustment->adjustment_value   = $adjustmentData->adjustment_value;
            $adjustment->adjustment_amount  = $adjustmentData->adjustment_amount;
            if ($adjustment->save()) {
                $this->refreshInventoryLevel($adjustment->consumable_id);
                $this->commitTransaction();

                return true;
            } else {
                $this->errors = $adjustment->errors();
            }
        } catch (\Exception $e) {
        }

        $this->rollbackTransaction();

        return false;
    }

    /**
     * Removes an adjustment from the system.
     *
     * @param $adjustmentID
     * @return mixed
     */
    public function removeAdjustment($adjustmentID)
    {
        $this->startTransaction();

        try {
            $this->table()->where('id', '=', $adjustmentID)->delete();
            $this->commitTransaction();

            return true;
        } catch (\Exception $e) {
        }

        $this->rollbackTransaction();

        return false;
    }

    /**
     * Refreshes the DB cached inventory level for the given consumable.
     *
     * @param $consumableID
     * @return mixed
     */
    public function refreshInventoryLevel($consumableID)
    {
        $currentInventoryLevel = $this->table()->select(DB::raw('SUM(adjustment_amount) as inventory_level'))->where('consumable_id', '=', $consumableID)->first();

        if ($currentInventoryLevel->inventory_level != null) {
            $currentInventoryLevel = $currentInventoryLevel->inventory_level;
        } else {
            $currentInventoryLevel = 0;
        }

        $this->startTransaction();

        try {
            $this->table('consumables')->where('id', '=', $consumableID)->update([
                'current_inventory_level' => $currentInventoryLevel
                                                                                 ]);


            $this->commitTransaction();
            return true;
        } catch (\Exception $e) {
        }


        $this->rollbackTransaction();
        return false;
    }

    /**
     * Gets a builder instance returning adjustments for the given consumable.
     *
     * @param $consumableID
     * @return mixed
     */
    public function getAdjustments($consumableID)
    {
        return $this->table()->where('consumable_id', '=', $consumableID)->orderBy('date_adjusted', 'DESC');
    }

    /**
     * Creates a new record.
     *
     * @param  array $recordDetails
     * @return mixed
     */
    public function create(array $recordDetails = array())
    {
        return $this->addAdjustment($recordDetails);
    }

    /**
     * Removes an existing record.
     *
     * @param  array $removeDetails
     * @return mixed
     * @throws \Exception - Not implemented.
     */
    public function remove(array $removeDetails = array())
    {
        foreach ($removeDetails as $detail) {
            $this->removeAdjustment($detail);
        }
    }

    /**
     * Updates an existing record.
     *
     * @param  int   $recordID
     * @param  array $newRecordDetails
     * @return mixed
     * @throws \Exception - Not implemented.
     */
    public function update($recordID, array $newRecordDetails)
    {
        throw new \Exception('Not implemented');
    }
}
