<?php namespace ParadoxOne\NDCounties\Database\Repositories;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use ParadoxOne\NDCounties\Contracts\UnitRepositoryInterface;
use ParadoxOne\NDCounties\IdentificationCode as IDCode;
use Unit;

class UnitRepository extends BaseRepository implements UnitRepositoryInterface
{
    protected $table = 'units';

    public function getModelById($id)
    {
        return Unit::findOrFail($id);
    }

    public function create(array $unitData = array())
    {
        $unit              = new Unit;
        $unit->code        = IDCode::prepareFrom($unitData['code']);
        $unit->name        = $unitData['name'];
        $unit->description = $unitData['description'];

        if ($unit->save()) {
            return $unit;
        } else {
            $this->errors = $unit->errors();

            return false;
        }
    }

    public function remove(array $removeContext = array())
    {
        Unit::destroy(intval($removeContext[0]));
    }

    public function update($unitID, array $unitInformation = array())
    {
        $unit              = Unit::findOrFail($unitID);
        $unit->code        = IDCode::prepareFrom($unitInformation['code']);
        $unit->name        = $unitInformation['name'];
        $unit->description = $unitInformation['description'];

        if ($unit->updateUniques()) {
            return $unit;
        } else {
            $this->errors = $unit->errors();

            return false;
        }
    }

    /**
     * Returns a collection of units.
     *
     * @return array $units
     */
    public function getUnits()
    {
        return $this->table()->select('id', 'name', 'code', 'description');
    }

    public function whereIn($measurementUnits)
    {
        return $this->table()->whereIn('id', $measurementUnits)->get();
    }


    public function search($searchTerms)
    {
        if (Input::get('is', null) !== null) {
            return $this->table()->where('id', '=', Input::get('is'))->get();
        }

        if ($searchTerms === '//') {
            return $this->table()->get();
        }

        return $this->table()
                    ->orWhere(function ($query) use ($searchTerms) {
                        $query->where('name', 'LIKE', '%' . $searchTerms . '%')
                              ->orWhere('description', 'LIKE', '%' . $searchTerms . '%')
                              ->orWhere('code', 'LIKE', '%' . $searchTerms . '%');
                    })
                    ->get();
    }
}
