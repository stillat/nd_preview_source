<?php namespace ParadoxOne\NDCounties\Database\Repositories;

use Carbon\Carbon;
use Illuminate\Support\Facades\Config;
use ParadoxOne\NDCounties\Contracts\SystemInformationRepositoryInterface;
use ParadoxOne\NDCounties\Database\Database;
use ParadoxOne\NDCounties\Database\NotImplementedException;

class TenantSystemInformationRepository extends BaseTenantRepository implements SystemInformationRepositoryInterface
{
    protected $table = 'system_information_logs';

    /**
     * Creates a new record.
     *
     * @param  array $recordDetails
     * @return mixed
     * @throws NotImplementedException
     */
    public function create(array $recordDetails = array())
    {
        throw new NotImplementedException;
    }

    /**
     * Removes an existing record.
     *
     * @param  array $removeDetails
     * @return mixed
     * @throws NotImplementedException
     */
    public function remove(array $removeDetails = array())
    {
        throw new NotImplementedException;
    }

    /**
     * Updates an existing record.
     *
     * @param  int   $recordID
     * @param  array $newRecordDetails
     * @return mixed
     * @throws NotImplementedException
     */
    public function update($recordID, array $newRecordDetails)
    {
        throw new NotImplementedException;
    }

    /**
     * Creates a new system information log.
     *
     * @return mixed
     */
    private function createNewSystemInformationLog()
    {
        $defaultCreateLimit  = Config::get('crms.db_backup_create_limit_monthly', 4);
        $defaultRestoreLimit = Config::get('crms.db_backup_restore_limit_monthly', 4);
        $defaultUploadLimit  = Config::get('crms.db_backup_upload_limit_monthly', 2);
        $databaseVersion     = Database::getVersion();

        $createData = [
            'backup_upload_limits'   => $defaultUploadLimit,
            'backup_create_limits'   => $defaultCreateLimit,
            'backup_restore_backups' => $defaultRestoreLimit,
            'database_version'       => $databaseVersion,
            'configured_on'          => Database::getNowDate()
        ];

        $createdID = $this->table()->insertGetId($createData);

        $createData     = (object)$createData;
        $createData->id = $createdID;

        return $createData;
    }

    public function bringDatabaseVersionCurrent()
    {
        $this->table()->update(['database_version' => Database::getVersion()]);
    }


    /**
     * Gets the database version for a given tenant account.
     *
     * @return mixed
     */
    public function getDatabaseVersion()
    {
        return $this->getSystemInformation()->database_version;
    }

    /**
     * Updates the system information for a given tenant.
     *
     * @param array $systemInformation
     * @return mixed
     */
    public function updateSystemInformation(array $systemInformation)
    {
        $this->table()->update([
                                   'backup_upload_limits'   => $systemInformation['backup_upload_limits'],
                                   'backup_create_limits'   => $systemInformation['backup_create_limits'],
                                   'backup_restore_backups' => $systemInformation['backup_restore_backups'],
                                   'database_version'       => $systemInformation['database_version'],
                                   'configured_on'          => $systemInformation['configured_on']
                               ]);
    }

    public function getSystemInformation()
    {
        $systemInformation = $this->table()->orderBy('id', 'desc')->first();

        if ($systemInformation == null) {
            return $this->createNewSystemInformationLog();
        }

        return $systemInformation;
    }
}
