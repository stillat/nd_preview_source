<?php namespace ParadoxOne\NDCounties\Database\Entities\WorkEntries;

use ParadoxOne\NDCounties\Contracts\Unions\UnionConsumableEquipmentUnitInterface;

class ConsumableEquipmentEntity extends AbstractConsumable implements UnionConsumableEquipmentUnitInterface
{
    /**
     * {@inheritdoc}
     */
    public function getAssociatedType()
    {
        return 2;
    }
}
