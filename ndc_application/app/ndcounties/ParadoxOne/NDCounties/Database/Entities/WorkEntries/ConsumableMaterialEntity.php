<?php namespace ParadoxOne\NDCounties\Database\Entities\WorkEntries;

use ParadoxOne\NDCounties\Contracts\Unions\UnionConsumableMaterialInterface;

class ConsumableMaterialEntity extends AbstractConsumable implements UnionConsumableMaterialInterface
{
    /**
     * {@inheritdoc}
     */
    public function getAssociatedType()
    {
        return 4;
    }
}
