<?php namespace ParadoxOne\NDCounties\Database\Entities\WorkEntries\Handlers;

use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use ParadoxOne\NDCounties\Contracts\Unions\UnionPropertyHandlerEquipmentUnitInterface;
use ParadoxOne\NDCounties\Contracts\WorkEntries\ConsumableRecordInterface;
use ParadoxOne\NDCounties\Database\Repositories\WorkEntries\DynamicConsumableHydrator;

class EquipmentPropertyHandler implements UnionPropertyHandlerEquipmentUnitInterface
{
    protected $validationErrors = null;

    protected $odometerReading = null;

    protected $odometerRepository = null;

    protected $consumable;

    private function doInsertMode()
    {
        if ($this->odometerReading !== null) {
            $odometerReading = intval(trim($this->odometerReading));
            if ((strlen($odometerReading) !== 0) && ($odometerReading !== 0)) {
                $success = $this->odometerRepository->create(array(
                                                                 'equipment_id'           => $this->consumable->learned_id,
                                                                 'odometer_reading'       => $odometerReading,
                                                                 'associated_record'      => $this->consumable->getWorkEntryID(),
                                                                 'associated_record_type' => '1',
                                                             ));

                if ($success) {
                    return true;
                }

                $this->validationErrors = $this->odometerRepository->errors();

                return false;
            }
        }

        return true;
    }

    private function doUpdateMode()
    {
        if ($this->odometerReading == "" || $this->odometerReading == null) {
            DB::connection(\Tenant::getCurrentConnection())->table('equipment_odometer_readings')
              ->where('equipment_id', '=', $this->consumable->getRecordID())->where(function ($query) {
                    $query->where('associated_record_type', '=', 1);
                })->delete();
        } else {
            $existsTest = DB::connection(\Tenant::getCurrentConnection())->table('equipment_odometer_readings')
                            ->where('equipment_id', '=', $this->consumable->getRecordID())
                            ->where(function ($query) {
                                $query->where('associated_record_type', '=', 1);
                            })->count();

            if ($existsTest) {
                $result = DB::connection(\Tenant::getCurrentConnection())->table('equipment_odometer_readings')
                            ->where('equipment_id', '=', $this->consumable->getRecordID())
                            ->where(function ($query) {
                                $query->where('associated_record_type', '=', 1);
                            })->update(['odometer_reading' => $this->odometerReading]);
            } else {
                $t = $this->odometerRepository->create(array(
                                                           'equipment_id'           => $this->consumable->getRecordID(),
                                                           'odometer_reading'       => $this->odometerReading,
                                                           'associated_record'      => $this->consumable->getWorkEntryID(),
                                                           'associated_record_type' => '1',
                                                       ));
            }
        }
    }

    public function handle(ConsumableRecordInterface $consumable, $validateOnly = false)
    {
        $this->consumable = $consumable;

        $this->odometerRepository =
            App::make('ParadoxOne\NDCounties\Contracts\EquipmentUnits\OdometerReadingRepositoryInterface');

        $this->odometerReading = $consumable->getProperty(DynamicConsumableHydrator::PROPERTY_ODOMETER_READING, null);

        $updateMode = $consumable->getProperty('IS_UPDATING', false);

        if ($updateMode) {
            return $this->doUpdateMode();
        }

        return $this->doInsertMode();
    }

    public function errors()
    {
        return $this->validationErrors;
    }
}
