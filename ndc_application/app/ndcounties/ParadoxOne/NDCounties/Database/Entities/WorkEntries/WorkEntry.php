<?php namespace ParadoxOne\NDCounties\Database\Entities\WorkEntries;

use Carbon;
use ParadoxOne\NDCounties\Contracts\WorkEntries\WorkEntryRecordInterface;
use Illuminate\Support\Facades\App;

/**
 * Class WorkEntry
 *
 * @package ParadoxOne\NDCounties\Database\Entities
 */
class WorkEntry implements WorkEntryRecordInterface
{
    /**
     * Holds the employee information.
     *
     * @var null
     */
    private $employeeInformation = null;

    /**
     * Holds the employee's historic information information.
     *
     * @var null
     */
    private $employeeHistoricInformation = null;

    /**
     * Holds the activity information.
     *
     * @var null
     */
    private $activityInformation = null;

    /**
     * Holds the district information.
     *
     * @var null
     */
    private $districtInformation = null;

    /**
     * Holds the project information.
     *
     * @var null
     */
    private $projectInformation = null;

    /**
     * Holds the department information.
     *
     * @var null
     */
    private $departmentInformation = null;

    /**
     * Holds the property information.
     *
     * @var null
     */
    private $propertyInformation = null;

    /**
     * Holds the property context.
     *
     * @var null
     */
    private $propertyContext = null;

    /**
     * The work date.
     *
     * @var null
     */
    private $workDate = null;

    /**
     * Holds the misc billing information.
     *
     * @var null
     */
    private $miscellaneousBillingInformation = null;

    /**
     * Holds the totals information.
     *
     * @var null
     */
    private $recordTotals = null;

    protected $notAvailableRecordHolder = null;

    /**
     * Holds the work ID.
     *
     * @var int
     */
    private $workID = 0;

    /**
     * Holds the equipment units.
     *
     * @var null
     */
    private $equipments = null;

    /**
     * Holds the material information.
     *
     * @var null
     */
    private $materials = null;

    /**
     * Holds the activity description;
     *
     * @var null
     */
    private $activityDescription = null;

    /**
     * Indicates if the record is excluded form reports.
     *
     * @var bool
     */
    private $excludedFromReports = false;

    /**
     * The raw record data.
     *
     * @var null
     */
    public $rawData = null;

    private $collapsedFuels = [];

    /**
     * Creates a new instance of WorkEntry.
     *
     * @param $rawData
     * @param $equipments
     * @param $materials
     * @param $property
     */
    public function __construct($rawData, $equipments, $materials, $property)
    {
        // Misc information.
        $this->workDate            = (new Carbon($rawData->work_date))->toDateString();
        $this->workID              = $rawData->id;
        $this->activityDescription = $rawData->activity_description;

        $this->equipments = $equipments;
        $this->materials  = $materials;

        // Employee
        $this->employeeInformation             = new \stdClass;
        $this->employeeInformation->firstName  = $rawData->employee_first_name;
        $this->employeeInformation->lastName   = $rawData->employee_last_name;
        $this->employeeInformation->middleName = $rawData->employee_middle_name;
        $this->employeeInformation->name       = $rawData->employee_last_name . ', ' . $rawData->employee_first_name;
        $this->employeeInformation->code       = $rawData->employee_code;
        $this->employeeInformation->id         = $rawData->employee_id;


        // Historic wage information.
        $this->employeeHistoricInformation                   = new \stdClass;
        $this->employeeHistoricInformation->regular          = $rawData->employee_historic_wage_regular;
        $this->employeeHistoricInformation->overtime         = $rawData->employee_historic_wage_overtime;
        $this->employeeHistoricInformation->regularBenefits  = $rawData->employee_historic_fringe_regular;
        $this->employeeHistoricInformation->overtimeBenefits = $rawData->employee_historic_fringe_overtime;

        $this->excludedFromReports = $rawData->exclude_from_report;

        // Activity information.
        $this->activityInformation       = new \stdClass;
        $this->activityInformation->id   = $rawData->activity_id;
        $this->activityInformation->code = $rawData->activity_code;
        $this->activityInformation->name = $rawData->activity_name;

        // District information.
        $this->districtInformation       = new \stdClass;
        $this->districtInformation->id   = $rawData->district_id;
        $this->districtInformation->name = $rawData->district_name;
        $this->districtInformation->code = $rawData->district_code;

        // Department information.
        $this->departmentInformation       = new \stdClass;
        $this->departmentInformation->id   = $rawData->department_id;
        $this->departmentInformation->name = $rawData->department_name;
        $this->departmentInformation->code = $rawData->department_code;

        // Project information.
        $this->projectInformation       = new \stdClass;
        $this->projectInformation->id   = $rawData->project_id;
        $this->projectInformation->name = $rawData->project_name;
        $this->projectInformation->code = $rawData->project_code;

        // Property information.
        $this->propertyContext     = $rawData->property_context;
        $this->propertyInformation = $property;

        // Misc billing information.
        $this->miscellaneousBillingInformation              = new \stdClass;
        $this->miscellaneousBillingInformation->quantity    = $rawData->misc_billing_quantity;
        $this->miscellaneousBillingInformation->rate        = $rawData->misc_billing_rate;
        $this->miscellaneousBillingInformation->unit_id     = $rawData->misc_unit_id;
        $this->miscellaneousBillingInformation->description = $rawData->misc_description;

        // Totals information.
        $this->recordTotals                        = new \stdClass;
        $this->recordTotals->materialQuantity      = $rawData->total_materials_quantity;
        $this->recordTotals->materialCost          = $rawData->total_materials_cost;
        $this->recordTotals->fuelsQuantity         = $rawData->total_fuels_quantity;
        $this->recordTotals->fuelsCost             = $rawData->total_fuels_cost;
        $this->recordTotals->fuelEquipmentCost     = $rawData->total_equipment_and_fuels_cost;
        $this->recordTotals->equipmentUnitQuantity = $rawData->total_equipment_units_quantity;
        $this->recordTotals->equipmentUnitCost     = $rawData->total_equipment_units_cost;
        $this->recordTotals->miscBilling           = $rawData->total_misc_billing_cost;
        $this->recordTotals->employeeRegular       = $rawData->total_employee_regular_cost;
        $this->recordTotals->employeeOvertime      = $rawData->total_employee_overtime_cost;
        $this->recordTotals->employeeCombined      = $rawData->total_employee_combined_cost;
        $this->recordTotals->employeeRegularHours  = $rawData->employee_hours_regular_worked;
        $this->recordTotals->employeeOvertimeHours = $rawData->employee_hours_overtime_worked;
        $this->recordTotals->employeeTotalHours    = number_format($rawData->employee_hours_regular_worked + $rawData->employee_hours_overtime_worked, 6);

        $this->recordTotals->invoiceOverride       = object_get($rawData, 'total_invoice', 0);

        $this->rawData             = $rawData;
        $this->rawData->equipments = $equipments;
        $this->rawData->materials  = $materials;
        $this->collapsedFuels = $this->collapseFuels($rawData->rawFuels);
    }

    /**
     * Gets the raw data.
     *
     * @return mixed
     */
    public function &getRawData()
    {
        return $this->rawData;
    }

    private function collapseFuels(&$fuelsToCollapse)
    {
        $fuelIds = array_pluck_unique($fuelsToCollapse, 'consumable_id');
        $collapsedFuels = [];

        foreach ($fuelIds as $fuelID) {
            $fuels = array_pluck_where($fuelsToCollapse, 'consumable_id', $fuelID);
            $newFuel = null;
            if (count($fuels) > 0) {
                $newFuel = clone $fuels[0];
            } else {
                continue;
            }
            $newFuel->total_cost = array_sum(array_pluck($fuels, 'total_cost'));
            $newFuel->total_quantity = array_sum(array_pluck($fuels, 'total_quantity'));
            $newFuel->total_tax = array_sum(array_pluck($fuels, 'total_tax'));
            $newFuel->historic_cost = array_sum(array_pluck($fuels, 'historic_cost')) / count($fuels);
            $collapsedFuels[] = $newFuel;
        }

        return $collapsedFuels;
    }

    public function getGroupedFuels()
    {
        return $this->collapsedFuels;
    }

    /**
     * Indicates if the record is excluded from reporting.
     *
     * @return mixed
     */
    public function isExcludedFromReports()
    {
        return $this->excludedFromReports;
    }

    /**
     * Indicates if the employee information was taxed.
     *
     * @return mixed
     */
    public function getEmployeeIsTaxed()
    {
        return $this->rawData->employee_taxable;
    }


    /**
     * Returns the record employee.
     *
     * @return mixed
     */
    public function &getEmployee()
    {
        return $this->employeeInformation;
    }

    /**
     * Returns the record activity.
     *
     * @return mixed
     */
    public function &getActivity()
    {
        return $this->activityInformation;
    }

    /**
     * Returns the record district.
     *
     * @return mixed
     */
    public function &getDistrict()
    {
        return $this->districtInformation;
    }

    /**
     * Returns the record project.
     *
     * @return mixed
     */
    public function &getProject()
    {
        return $this->projectInformation;
    }

    /**
     * Returns the record equipment units.
     *
     * @return mixed
     */
    public function &getEquipmentUnits()
    {
        return $this->equipments;
    }

    public function &getOrganizationProperty()
    {
        return $this->rawData->organization_property;
    }

    public function &getPreferredProperty()
    {
        if ($this->rawData->organization_property->id == 0 && $this->propertyInformation->id != 0) {
            return $this->getProperty();
        }

        return $this->getOrganizationProperty();
    }

    /**
     * This function will return an HTML-safe string that represents how this
     * report will be displayed on reports.
     *
     * This function takes the formatter settings into consideration.
     */
    public function getReportPropertyDisplay()
    {
        $formatter = App::make('formatter');
        $orgIsSpecified = false;
        $propertyReplace = '';

        $orgIsSpecified = false;

        if ($this->rawData->property_organization_id > 1 && ($this->rawData->property_organization_id != $this->rawData->property_id)) {
            $property        = new \stdClass();
            $property->code  = $this->rawData->organization_property->code;
            $property->name  = $this->rawData->organization_property->name;
            $propertyReplace = e($formatter->property($property));
            $orgIsSpecified = true;

            unset($property);
        }

        if ($this->rawData->property_id > 1) {
            $property        = new \stdClass();
            $property->code  = $this->rawData->property_code;
            $property->name  = $this->rawData->property_name;

            if ($orgIsSpecified) {
                $propertyReplace .= '<strong>&nbsp;/</strong><br>';
            }

            $propertyReplace .= e($formatter->property($property));
            unset($property);
        }

        if ($this->rawData->property_id <= 1 && $this->rawData->property_organization_id <= 1) {
            $this->notAvailableRecordHolder       = new \stdClass;
            $this->notAvailableRecordHolder->code = 'NA';
            $this->notAvailableRecordHolder->name = 'Not Available';

            $propertyReplace = $formatter->property($this->notAvailableRecordHolder);
        }

        return $propertyReplace;
    }

    /**
     * Returns the record property.
     *
     * @return mixed
     */
    public function &getProperty()
    {
        return $this->propertyInformation;
    }

    /**
     * Indicates if the record property is an equipment unit.
     *
     * @return mixed
     */
    public function isEquipmentProperty()
    {
        if ($this->propertyContext == 0) {
            return false;
        }

        return true;
    }

    /**
     * Returns the record department.
     *
     * @return mixed
     */
    public function &getDepartment()
    {
        return $this->departmentInformation;
    }

    /**
     * Returns the record materials.
     *
     * @return mixed
     */
    public function &getMaterials()
    {
        return $this->materials;
    }

    /**
     * Returns the record fuels.
     *
     * @return mixed
     */
    public function &getFuels()
    {
        $tempFuels = [];

        foreach ($this->equipments as $equipmentUnit) {
            $tempFuels = array_merge($equipmentUnit->fuels, $tempFuels);
        }

        return $tempFuels;
    }

    /**
     * Gets the misc billing information.
     *
     * @return mixed
     */
    public function &getMiscBilling()
    {
        return $this->miscellaneousBillingInformation;
    }

    /**
     * Returns the employee's historic wage information.
     *
     * @return mixed
     */
    public function &getHistoricWageInformation()
    {
        return $this->employeeHistoricInformation;
    }

    /**
     * Returns the record's totals.
     *
     * @return mixed
     */
    public function &getRecordTotals()
    {
        return $this->recordTotals;
    }

    /**
     * Gets the record work date.
     *
     * @return mixed
     */
    public function getWorkDate()
    {
        return $this->workDate;
    }

    public function getManagerWorkDate()
    {
        return (new Carbon($this->workDate))->format('m/j/Y');
    }

    /**
     * Returns the record ID.
     *
     * @return int
     */
    public function getWorkID()
    {
        return $this->workID;
    }

    /**
     * Returns the activity description.
     *
     * @return mixed
     */
    public function getActivityDescription()
    {
        return $this->activityDescription;
    }

    public function objectGraphSearch($for, $value)
    {
        if (object_get($this->rawData, $for) == $value) {
            return true;
        }

        return false;
    }

    public function resetToGroupingName($group)
    {
        $this->employeeInformation->firstName  = $group;
        $this->employeeInformation->lastName   = $group;
        $this->employeeInformation->middleName = $group;
        $this->employeeInformation->name       = $group;
        $this->employeeInformation->code       = $group;

        $this->activityInformation->code = $group;
        $this->activityInformation->name = $group;

        $this->districtInformation->name = $group;
        $this->districtInformation->code = $group;

        $this->departmentInformation->name = $group;
        $this->departmentInformation->code = $group;

        $this->projectInformation->name = $group;
        $this->projectInformation->code = $group;

        $this->propertyInformation->name = $group;
        $this->propertyInformation->code = $group;

        $this->rawData->property_id          = -1;
        $this->rawData->activity_code        = $group;
        $this->rawData->activity_id          = -1;
        $this->rawData->activity_name        = $group;
        $this->rawData->employee_first_name  = $group;
        $this->rawData->employee_last_name   = $group;
        $this->rawData->employee_middle_name = $group;
        $this->rawData->employee_code        = $group;
        $this->rawData->employee_id          = -1;
        $this->rawData->project_id           = -1;
        $this->rawData->project_code         = $group;
        $this->rawData->project_name         = $group;
        $this->rawData->district_id          = -1;
        $this->rawData->district_code        = $group;
        $this->rawData->department_id        = -1;
        $this->rawData->department_code      = $group;
        $this->rawData->department_name      = $group;
        $this->rawData->property_name        = $group;
        $this->rawData->property_id          = -1;
        $this->rawData->property_code        = $group;
    }

    public function refresh()
    {
        $this->miscellaneousBillingInformation->quantity = $this->rawData->misc_billing_quantity;

        // Totals information.
        $this->recordTotals->materialQuantity      = $this->rawData->total_materials_quantity;
        $this->recordTotals->materialCost          = $this->rawData->total_materials_cost;
        $this->recordTotals->fuelsQuantity         = $this->rawData->total_fuels_quantity;
        $this->recordTotals->fuelsCost             = $this->rawData->total_fuels_cost;
        $this->recordTotals->fuelEquipmentCost     = $this->rawData->total_equipment_and_fuels_cost;
        $this->recordTotals->equipmentUnitQuantity = $this->rawData->total_equipment_units_quantity;
        $this->recordTotals->equipmentUnitCost     = $this->rawData->total_equipment_units_cost;
        $this->recordTotals->miscBilling           = $this->rawData->total_misc_billing_cost;
        $this->recordTotals->employeeRegular       = $this->rawData->total_employee_regular_cost;
        $this->recordTotals->employeeOvertime      = $this->rawData->total_employee_overtime_cost;
        $this->recordTotals->employeeCombined      = $this->rawData->total_employee_combined_cost;
        $this->recordTotals->employeeRegularHours  = $this->rawData->employee_hours_regular_worked;
        $this->recordTotals->employeeOvertimeHours = $this->rawData->employee_hours_overtime_worked;
        $this->recordTotals->employeeTotalHours    =
            $this->rawData->employee_hours_regular_worked + $this->rawData->employee_hours_overtime_worked;
    }
}
