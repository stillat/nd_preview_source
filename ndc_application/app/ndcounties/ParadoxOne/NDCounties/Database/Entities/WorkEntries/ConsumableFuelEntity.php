<?php namespace ParadoxOne\NDCounties\Database\Entities\WorkEntries;

use ParadoxOne\NDCounties\Contracts\Unions\UnionConsumableFuelInterface;

class ConsumableFuelEntity extends AbstractConsumable implements UnionConsumableFuelInterface
{
    /**
     * {@inheritdoc}
     */
    public function getAssociatedType()
    {
        return 3;
    }
}
