<?php namespace ParadoxOne\NDCounties\Database\Entities\WorkEntries;

use Illuminate\Support\Collection;
use ParadoxOne\NDCounties\Contracts\WorkEntries\ConsumableRecordInterface;

abstract class AbstractConsumable implements ConsumableRecordInterface
{
    use \ParadoxOne\NDCounties\Traits\ValidatableTrait;

    protected $recordID = -1;

    protected $consumableType = null;

    protected $consumableID = 0;

    protected $associatedType = null;

    protected $associatedID = 0;

    protected $workEntryID = 0;

    protected $measurementUnit = 0;

    protected $totalCost = '';

    protected $historicCost = '';

    protected $quantity = '';

    protected $taxed = false;

    protected $properties;

    public $bindingConsumableRecord = 0;

    public function __construct()
    {
        $this->properties = new Collection;
    }

    /**
     * Sets the record ID.
     *
     * @param $id
     * @return mixed
     */
    public function setRecordID($id)
    {
        $this->recordID = $id;
    }

    /**
     * Gets the record ID.
     *
     * @return mixed
     */
    public function getRecordID()
    {
        return $this->recordID;
    }


    /**
     * Sets the binding record.
     *
     * @param $bindingRecord
     * @return mixed
     */
    public function setBindingRecord($bindingRecord)
    {
        $this->bindingConsumableRecord = $bindingRecord;
    }

    /**
     * Gets the binding record.
     *
     * @return mixed
     */
    public function getBindingRecord()
    {
        return $this->bindingConsumableRecord;
    }


    /**
     * {@inheritdoc}
     */
    public function setProperty($property, $value)
    {
        $this->properties[$property] = $value;
    }

    /**
     * {@inheritdoc}
     */
    public function getProperty($property, $default = null)
    {
        if ($this->properties->has($property)) {
            return $this->properties[$property];
        }

        return $default;
    }

    /**
     * {@inheritdoc}
     */
    public function getProperties()
    {
        return $this->properties;
    }

    /**
     * {@inheritdoc}
     */
    public function setConsumableType($type)
    {
        $this->consumableType = $type;
    }

    /**
     * {@inheritdoc}
     */
    public function getConsumableType()
    {
        return $this->consumableType;
    }

    /**
     * {@inheritdoc}
     */
    public function setConsumableID($id)
    {
        $this->consumableID = $id;
    }

    /**
     * {@inheritdoc}
     */
    public function getConsumableID()
    {
        return $this->consumableID;
    }

    /**
     * {@inheritdoc}
     */
    public function setAssociatedType($type)
    {
        $this->associatedType = $type;
    }

    /**
     * {@inheritdoc}
     */
    public function getAssociatedType()
    {
        return $this->associatedType;
    }

    /**
     * {@inheritdoc}
     */
    public function setAssociatedID($id)
    {
        $this->associatedID = $id;
    }

    /**
     * {@inheritdoc}
     */
    public function getAssociatedID()
    {
        return $this->associatedID;
    }

    /**
     * {@inheritdoc}
     */
    public function nothingAssociated()
    {
        if ($this->associatedID == 0) {
            return true;
        }

        return false;
    }

    /**
     * {@inheritdoc}
     */
    public function setWorkEntryID($id)
    {
        $this->workEntryID = $id;
    }

    /**
     * {@inheritdoc}
     */
    public function getWorkEntryID()
    {
        return $this->workEntryID;
    }

    /**
     * {@inheritdoc}
     */
    public function setMeasurementUnit($id)
    {
        $this->measurementUnit = $id;
    }

    /**
     * {@inheritdoc}
     */
    public function getMeasurementUnit()
    {
        return $this->measurementUnit;
    }

    /**
     * {@inheritdoc}
     */
    public function setTotalCost($cost)
    {
        $this->totalCost = $cost;
    }

    /**
     * {@inheritdoc}
     */
    public function getTotalCost()
    {
        return $this->totalCost;
    }

    /**
     * {@inheritdoc}
     */
    public function setHistoricCost($cost)
    {
        $this->historicCost = $cost;
    }

    /**
     * {@inheritdoc}
     */
    public function getHistoricCost()
    {
        return $this->historicCost;
    }

    /**
     * {@inheritdoc}
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;
    }

    /**
     * {@inheritdoc}
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * {@inheritdoc}
     */
    public function setTaxed($taxed)
    {
        $this->taxed = $taxed;
    }

    /**
     * {@inheritdoc}
     */
    public function isTaxed()
    {
        return $this->taxed;
    }

    /**
     * {@inheritdoc}
     */
    public function getValidationRules()
    {
        return array(
            'consumable_type'     => 'required|in:2,3,4',
            'measurement_unit_id' => 'required',
            'total_cost'          => 'required|numeric',
            'total_quantity'      => 'required|numeric',
            'historic_cost'       => 'required|numeric'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function getValidationData()
    {
        return array(
            'consumable_type'     => $this->getConsumableType(),
            'measurement_unit_id' => $this->getMeasurementUnit(),
            'total_cost'          => $this->getTotalCost(),
            'total_quantity'      => $this->getQuantity(),
            'historic_cost'       => $this->getHistoricCost()
        );
    }
}
