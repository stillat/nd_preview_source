<?php namespace ParadoxOne\NDCounties\Database\Management\IO;

class LargeFileUtilities
{
    /**
     * Prepends the given content to a file that already exists.
     *
     * @param $content
     * @param $file
     * @param $temporaryFileLocation
     */
    public static function prepend($content, $file, $temporaryFileLocation)
    {
        $streamContext = stream_context_create();

        $filePointer = fopen($file, 'r', 1, $streamContext);
        $temporaryFile = $temporaryFileLocation.md5($content);

        file_put_contents($temporaryFile, $content);
        file_put_contents($temporaryFile, $filePointer, FILE_APPEND);
        fclose($filePointer);
        unlink($file);

        rename($temporaryFile, $file);
    }
}
