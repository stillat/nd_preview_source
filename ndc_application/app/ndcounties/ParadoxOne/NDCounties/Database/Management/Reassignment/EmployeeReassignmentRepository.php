<?php namespace ParadoxOne\NDCounties\Database\Management\Reassignment;

use ParadoxOne\NDCounties\Contracts\Data\RecordReassignmentRepositoryInterface;

class EmployeeReassignmentRepository extends BaseAssignmentRepository implements RecordReassignmentRepositoryInterface
{
    /**
     * Reassigns the provided record pairs.
     *
     * @param array $startRecords
     * @param array $finishRecords
     * @return mixed
     */
    public function reassignRecordPairs(array $startRecords, array $finishRecords)
    {
        $this->startTransaction();

        try {
            $reassignPairs = $this->cleanRecordPairs($startRecords, $finishRecords);

            foreach ($reassignPairs as $pair) {
                $startRecord  = $pair[0];
                $finishRecord = $pair[1];

                $this->workRecords()->where('employee_id', '=', $startRecord)->update(['employee_id' => $finishRecord]);
            }

            $this->commitTransaction();

            return true;
        } catch (\Exception $e) {
        }

        $this->rollbackTransaction();
        return false;
    }
}
