<?php namespace ParadoxOne\NDCounties\Database\Management\Reassignment;

use ParadoxOne\NDCounties\Contracts\Data\RecordReassignmentRepositoryInterface;
use ParadoxOne\NDCounties\Database\NotImplementedException;
use ParadoxOne\NDCounties\Database\Repositories\BaseTenantRepository;

abstract class BaseAssignmentRepository extends BaseTenantRepository implements RecordReassignmentRepositoryInterface
{
    /**
     * Creates a new record.
     *
     * @param  array $recordDetails
     * @return mixed
     * @throws NotImplementedException
     */
    public function create(array $recordDetails = array())
    {
        throw new NotImplementedException;
    }

    /**
     * Removes an existing record.
     *
     * @param  array $removeDetails
     * @return mixed
     * @throws NotImplementedException
     */
    public function remove(array $removeDetails = array())
    {
        throw new NotImplementedException;
    }

    /**
     * Updates an existing record.
     *
     * @param  int   $recordID
     * @param  array $newRecordDetails
     * @return mixed
     * @throws NotImplementedException
     */
    public function update($recordID, array $newRecordDetails)
    {
        throw new NotImplementedException;
    }


    /**
     * Gets work records table builder.
     *
     * @return mixed
     */
    protected function workRecords()
    {
        return $this->table('work_entries');
    }

    protected function consumableFuels()
    {
        return $this->table('work_consumables')->where('consumable_type', '=', 3);
    }

    protected function consumableMaterials()
    {
        return $this->table('work_consumables')->where('consumable_type', '=', 4);
    }

    /**
     * Cleans the record pairs and formats them for manipulation.
     *
     * @param array $startRecords
     * @param array $finishRecords
     * @return array
     */
    protected function cleanRecordPairs(array $startRecords, array $finishRecords)
    {
        $newRecordPairs = [];

        for ($i = 0; $i < count($startRecords); $i++) {
            $startRecord  = intval($startRecords[$i]);
            $finishRecord = intval($finishRecords[$i]);

            if ($startRecord != $finishRecord && strlen($finishRecord) > 0) {
                $newRecordPairs[] = [$startRecord, $finishRecord];
            }
        }

        return $newRecordPairs;
    }
}
