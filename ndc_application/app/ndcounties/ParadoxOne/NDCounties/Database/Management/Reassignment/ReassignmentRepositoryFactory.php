<?php namespace ParadoxOne\NDCounties\Database\Management\Reassignment;

use Illuminate\Support\Facades\App;

class ReassignmentRepositoryFactory
{
    public static $reassignmentRepositories = [
        1 => '\ParadoxOne\NDCounties\Database\Management\Reassignment\ActivityReassignmentRepository',
        2 => '\ParadoxOne\NDCounties\Database\Management\Reassignment\DepartmentReassignmentRepository',
        3 => '\ParadoxOne\NDCounties\Database\Management\Reassignment\DistrictReassignmentRepository',
        4 => '\ParadoxOne\NDCounties\Database\Management\Reassignment\EmployeeReassignmentRepository',
        5 => '\ParadoxOne\NDCounties\Database\Management\Reassignment\FuelReassignmentRepository',
        6 => '\ParadoxOne\NDCounties\Database\Management\Reassignment\MaterialReassignmentRepository',
        7 => '\ParadoxOne\NDCounties\Database\Management\Reassignment\ProjectReassignmentRepository'
    ];

    /**
     * Creates a reassignment repository.
     *
     * @param $requestedRepository
     * @return \ParadoxOne\NDCounties\Contracts\Data\RecordReassignmentRepositoryInterface
     * @throws \Exception
     */
    public static function getRepository($requestedRepository)
    {
        $requestedRepository = intval($requestedRepository);
        if (array_key_exists($requestedRepository, self::$reassignmentRepositories)) {
            return App::make(self::$reassignmentRepositories[$requestedRepository]);
        }

        throw new \Exception('Requested repository does not exist.');
    }
}
