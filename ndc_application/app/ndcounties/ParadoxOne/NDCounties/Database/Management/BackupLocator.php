<?php namespace ParadoxOne\NDCounties\Database\Management;

use Carbon\Carbon;

class BackupLocator
{
    /**
     * Gets a backup location.
     *
     * @param $tenantID
     * @return string
     */
    public function getBackupLocation($tenantID)
    {
        $backupLocation = database_backups_path().md5($tenantID).'/';

        if (!file_exists($backupLocation)) {
            @mkdir($backupLocation);
        }
        return $backupLocation;
    }

    public function getBackupProcessingLocation($tenantID)
    {
        $backupLocation = database_backups_processing_path().md5($tenantID).'/';

        if (!file_exists($backupLocation)) {
            @mkdir($backupLocation);
        }
        return $backupLocation;
    }

    /**
     * Creates a backup file name.
     *
     * @param string $extension
     * @param string $prefix
     * @return string
     */
    public function getBackupFileName($extension = '.sql', $prefix = 'DIF_')
    {
        $fileName = Carbon::now()->toDateTimeString();
        $fileName = str_replace(':', '_', $fileName);
        return $prefix.$fileName.$extension;
    }

    /**
     * Determines if a file is compressed.
     *
     * @param $fileName
     * @return bool
     */
    public function isBackupCompressed($fileName)
    {
        return pathinfo($fileName, PATHINFO_EXTENSION) === "gz";
    }

    /**
     * Returns the non-compressed file name for a given path.
     *
     * @param $fileName
     * @return string
     */
    public function getUncompressedFileName($fileName)
    {
        return preg_replace('"\.gz$"', '', $fileName);
    }
}
