<?php namespace ParadoxOne\NDCounties\Database\Management;

use Carbon\Carbon;
use Illuminate\Support\Str;
use ParadoxOne\NDCounties\UI\UIBuilder;

class BackupModel
{
    protected $mode = 'Differential';

    protected $createdOn = '';

    protected $backupSize = 0;

    protected $backupName = '';

    public function __construct($fileInfo)
    {
        if (Str::startsWith($fileInfo->getFileName(), 'DIF_')) {
            $this->mode = 'Differential';
        } else {
            $this->mode = 'Incremental';
        }

        $parts = explode('.', $fileInfo->getFileName());
        array_pop($parts);

        if (Str::endsWith($fileInfo, '.gz')) {
            array_pop($parts);
        }

        $this->createdOn = $fileInfo->getMTime();
        $this->createdOn = Carbon::createFromTimestamp($this->createdOn);
        $this->backupSize = $fileInfo->getSize();
        $this->backupName = $fileInfo->getFileName();
    }

    public function getSize()
    {
        return $this->backupSize;
    }

    public function getMode()
    {
        return $this->mode;
    }

    public function getCreateDate()
    {
        return $this->createdOn;
    }

    public function getName()
    {
        return $this->backupName;
    }
}
