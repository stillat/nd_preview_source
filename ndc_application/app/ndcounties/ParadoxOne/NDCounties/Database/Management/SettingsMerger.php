<?php namespace ParadoxOne\NDCounties\Database\Management;

use Illuminate\Support\Facades\DB;
use ParadoxOne\NDCounties\Settings\CountyFormattingSettingsManager;
use ParadoxOne\NDCounties\Settings\InventorySettingsManager;
use ParadoxOne\NDCounties\Settings\ReportSettingsManager;
use ParadoxOne\NDCounties\Settings\SortingSettingsManager;
use ParadoxOne\NDCounties\Settings\WorkRecordSettingsManager;

class SettingsMerger
{
    protected $managers = [];

    public function __construct(CountyFormattingSettingsManager $countyFormattingManager, InventorySettingsManager $inventoryManager,
                                ReportSettingsManager $reportsManager, SortingSettingsManager $sortingManager, WorkRecordSettingsManager $workRecordSettingsManager
    ) {
        $this->managers[$countyFormattingManager->getSubSystem()]   = $countyFormattingManager->getDefaultSettings();
        $this->managers[$inventoryManager->getSubSystem()]          = $inventoryManager->getDefaultSettings();
        $this->managers[$reportsManager->getSubSystem()]            = $reportsManager->getDefaultSettings();
        $this->managers[$sortingManager->getSubSystem()]            = $sortingManager->getDefaultSettings();
        $this->managers[$workRecordSettingsManager->getSubSystem()] = $workRecordSettingsManager->getDefaultSettings();
    }

    public function merge()
    {
        DB::beginTransaction();

        try {
            $countyWideApplicationSettings = DB::table('county_wide_application_settings')->get();
            $userSettings                  = DB::table('user_application_settings')->get();
            $this->mergeSettings($countyWideApplicationSettings, 'county_wide_application_settings');
            $this->mergeSettings($userSettings, 'user_application_settings');

            DB::commit();
            return true;
        } catch (\Exception $e) {
        }

        DB::rollback();
        return false;
    }

    private function mergeSettings($settings, $tableName)
    {
        foreach ($settings as $key => $setting) {
            $subSystem               = $setting->subsystem;
            $managerSettingValue     = json_decode($this->managers[$subSystem][$setting->setting_name]);
            $currentSettingValue     = json_decode($setting->setting_value);
            $setting->needs_updating = false;
            foreach ($managerSettingValue as $settingName => $settingValue) {
                if (!property_exists($currentSettingValue, $settingName)) {
                    $setting->needs_updating             = true;
                    $currentSettingValue->{$settingName} = $settingValue;
                }
            }

            $setting->setting_value = json_encode($currentSettingValue);
            $settings[$key]         = $setting;
        }

        foreach ($settings as $setting) {
            if ($setting->needs_updating) {
                DB::table($tableName)->where('id', '=', $setting->id)->update([
                                                                                  'setting_value' => $setting->setting_value
                                                                              ]);
            }
        }
    }
}
