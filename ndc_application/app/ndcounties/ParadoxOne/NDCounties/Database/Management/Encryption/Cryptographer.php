<?php namespace ParadoxOne\NDCounties\Database\Management\Encryption;

use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\File;
use Symfony\Component\Process\Process;

final class Cryptographer
{
    private $outputFolder = '';

    private $encrypter;

    private $cleanupFiles = [];

    public function __construct()
    {
        $this->encrypter = new AESEncrypter('bbc2ed08abe0a8e79c4f1a98d64b87f6', '392,>/cn&38wifMa');
    }

    public function setOutputFolder($location)
    {
        $this->outputFolder = $location;
    }

    private function getEncryptedName($name)
    {
        $parts = explode('.', $name);
        array_pop($parts);
        if (Config::get('crms.db_gzip_dumps', false) && count($parts) > 1) {
            array_pop($parts);
        }

        $parts[] = '.ndsql';
        $parts = implode('', $parts);

        return str_replace('\\', '/', $parts);
    }

    public function decrypt($filename)
    {
        $replace = '.sql';

        if (Config::get('crms.db_gzip_dumps', false)) {
            $replace .= '.gz';
        }

        $target = str_replace('.ndsql', $replace, $filename);

        try {
            $this->encrypter->decrypt_file($filename, $target);
            $this->cleanupFiles[] = $filename;
            return $target;
        } catch (\Exception $e) {
        }

        return false;
    }

    public function encrypt($fileName)
    {
        $newFileName = $this->getEncryptedName($fileName);

        try {
            $this->encrypter->encrypt_file($fileName, $newFileName);
            $this->cleanupFiles[] = $newFileName;
            return $newFileName;
        } catch (\Exception $e) {
        }

        return false;
    }

    public function cleanupEncrypt()
    {
        File::delete($this->cleanupFiles);
    }
}
