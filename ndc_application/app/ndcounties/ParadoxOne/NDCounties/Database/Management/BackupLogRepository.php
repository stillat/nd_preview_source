<?php namespace ParadoxOne\NDCounties\Database\Management;

use Illuminate\Support\Facades\DB;
use ParadoxOne\NDCounties\Contracts\Data\BackupLogRepositoryInterface;
use ParadoxOne\NDCounties\Database\Database;
use ParadoxOne\NDCounties\Database\NotImplementedException;
use ParadoxOne\NDCounties\Database\Repositories\BaseRepository;

class BackupLogRepository extends BaseRepository implements BackupLogRepositoryInterface
{
    /**
     * The table name.
     *
     * @var string
     */
    protected $table = 'backup_logs';

    /**
     * Creates a new record.
     *
     * @param  array $recordDetails
     * @return mixed
     * @throws NotImplementedException
     */
    public function create(array $recordDetails = array())
    {
        throw new NotImplementedException('Not Implemented');
    }

    /**
     * Removes an existing record.
     *
     * @param  array $removeDetails
     * @return mixed
     * @throws NotImplementedException
     */
    public function remove(array $removeDetails = array())
    {
        throw new NotImplementedException('Not Implemented');
    }

    /**
     * Updates an existing record.
     *
     * @param  int   $recordID
     * @param  array $newRecordDetails
     * @return mixed
     * @throws NotImplementedException
     */
    public function update($recordID, array $newRecordDetails)
    {
        throw new NotImplementedException('Not Implemented');
    }


    /**
     * Logs a backup operation.
     *
     * @param array $backupDetails
     * @return mixed
     */
    public function logBackup(array $backupDetails)
    {
        $this->table()->insert($backupDetails);
    }

    private function createRecord($user, $backupName, $account, $status)
    {
        $this->logBackup([
                             'tenant_id'        => $account,
                             'created_by'       => $user->id,
                             'created_by_admin' => $user->admin,
                             'backup_name'      => $backupName,
                             'status'           => $status,
                             'created_on'       => Database::getNowDate()
                         ]);
    }

    public function logCreate($user, $backupName, $account)
    {
        $this->createRecord($user, $backupName, $account, self::STATUS_BACKUP_CREATED);
    }

    public function logCreateFailed($user, $backupName, $account)
    {
        $this->createRecord($user, $backupName, $account, self::STATUS_BACKUP_CREATE_FAILED);
    }

    public function logRestore($user, $backupName, $account)
    {
        $this->createRecord($user, $backupName, $account, self::STATUS_BACKUP_RESTORED);
    }

    public function logRestoreFailed($user, $backupName, $account)
    {
        $this->createRecord($user, $backupName, $account, self::STATUS_BACKUP_RESTORED_FAILED);
    }

    public function logRemove($user, $backupName, $account)
    {
        $this->createRecord($user, $backupName, $account, self::STATUS_BACKUP_REMOVE);
    }

    public function logRemoveFailed($user, $backupName, $account)
    {
        $this->createRecord($user, $backupName, $account, self::STATUS_BACKUP_REMOVE_FAILED);
    }

    public function logVersionUpdated($user, $backupName, $account)
    {
        $this->createRecord($user, $backupName, $account, self::STATUS_BACKUP_VERSION_UPDATED);
    }

    public function logSystemSynced($user, $backupName, $account)
    {
        $this->createRecord($user, $backupName, $account, self::STATUS_BACKUP_VERSION_SYNCED);
    }

    public function logSystemSyncedFailed($user, $backupName, $account)
    {
        $this->createRecord($user, $backupName, $account, self::STATUS_BACKUP_VERSION_SYNC_FAILED);
    }

    public function logDownload($user, $backupName, $account)
    {
        $this->createRecord($user, $backupName, $account, self::STATUS_BACKUP_DOWNLOADED);
    }

    public function logDownloadFailed($user, $backupName, $account)
    {
        $this->createRecord($user, $backupName, $account, self::STATUS_BACKUP_DOWNLOADED_FAILED);
    }

    public function logUpload($user, $backupName, $account)
    {
        $this->createRecord($user, $backupName, $account, self::STATUS_BACKUP_UPLOADED);
    }

    public function logUploadFailed($user, $backupName, $account)
    {
        $this->createRecord($user, $backupName, $account, self::STATUS_BACKUP_UPLOADED_FAILED);
    }

    /**
     * Gets the backup statistics for a given tenant.
     *
     * @param  int $tenantID
     * @return mixed
     */
    public function getBackupStatistics($tenantID)
    {
        $bindings = array_fill(0, 16, $tenantID);

        $value = $this->table()->select(DB::raw(
            '
	(SELECT COUNT(id) FROM backup_logs WHERE tenant_id = ? AND created_by_admin = 0 AND status = 5 AND MONTH(created_on) = MONTH(NOW()) AND YEAR(created_on) = YEAR(NOW())) as removedBackupsByUsers,
    (SELECT COUNT(id) FROM backup_logs WHERE tenant_id = ? AND created_by_admin = 1 AND status = 5 AND MONTH(created_on) = MONTH(NOW()) AND YEAR(created_on) = YEAR(NOW())) as removedBackupsByAdmins,
    (SELECT COUNT(id) FROM backup_logs WHERE tenant_id = ? AND created_by_admin = 0 AND status = 6 AND MONTH(created_on) = MONTH(NOW()) AND YEAR(created_on) = YEAR(NOW())) as failedRemovedBackupsByUsers,
    (SELECT COUNT(id) FROM backup_logs WHERE tenant_id = ? AND created_by_admin = 1 AND status = 6 AND MONTH(created_on) = MONTH(NOW()) AND YEAR(created_on) = YEAR(NOW())) as failedRemovedBackupsByAdmins,
    (SELECT COUNT(id) FROM backup_logs WHERE tenant_id = ? AND created_by_admin = 0 AND status = 3 AND MONTH(created_on) = MONTH(NOW()) AND YEAR(created_on) = YEAR(NOW())) as restoredBackupsByUsers,
    (SELECT COUNT(id) FROM backup_logs WHERE tenant_id = ? AND created_by_admin = 1 AND status = 3 AND MONTH(created_on) = MONTH(NOW()) AND YEAR(created_on) = YEAR(NOW())) as restoredBackupsByAdmins,
    (SELECT COUNT(id) FROM backup_logs WHERE tenant_id = ? AND created_by_admin = 0 AND status = 4 AND MONTH(created_on) = MONTH(NOW()) AND YEAR(created_on) = YEAR(NOW())) as failedRestoredBackupsByUsers,
    (SELECT COUNT(id) FROM backup_logs WHERE tenant_id = ? AND created_by_admin = 1 AND status = 4 AND MONTH(created_on) = MONTH(NOW()) AND YEAR(created_on) = YEAR(NOW())) as failedRestoredBackupsByAdmins,
    (SELECT COUNT(id) FROM backup_logs WHERE tenant_id = ? AND created_by_admin = 0 AND status = 1 AND MONTH(created_on) = MONTH(NOW()) AND YEAR(created_on) = YEAR(NOW())) as createdBackupsByUsers,
    (SELECT COUNT(id) FROM backup_logs WHERE tenant_id = ? AND created_by_admin = 1 AND status = 1 AND MONTH(created_on) = MONTH(NOW()) AND YEAR(created_on) = YEAR(NOW())) as createdBackupsByAdmins,
    (SELECT COUNT(id) FROM backup_logs WHERE tenant_id = ? AND created_by_admin = 0 AND status = 2 AND MONTH(created_on) = MONTH(NOW()) AND YEAR(created_on) = YEAR(NOW())) as failedCreatedBackupsByUsers,
    (SELECT COUNT(id) FROM backup_logs WHERE tenant_id = ? AND created_by_admin = 1 AND status = 2 AND MONTH(created_on) = MONTH(NOW()) AND YEAR(created_on) = YEAR(NOW())) as failedCreatedBackupsByAdmins,
    (SELECT COUNT(id) FROM backup_logs WHERE tenant_id = ? AND created_by_admin = 0 AND status = 9 AND MONTH(created_on) = MONTH(NOW()) AND YEAR(created_on) = YEAR(NOW())) as uploadedBackupsByUsers,
    (SELECT COUNT(id) FROM backup_logs WHERE tenant_id = ? AND created_by_admin = 1 AND status = 9 AND MONTH(created_on) = MONTH(NOW()) AND YEAR(created_on) = YEAR(NOW())) as uploadedBackupsByAdmins,
    (SELECT COUNT(id) FROM backup_logs WHERE tenant_id = ? AND created_by_admin = 0 AND status = 10 AND MONTH(created_on) = MONTH(NOW()) AND YEAR(created_on) = YEAR(NOW())) as failedUploadedBackupsByUsers,
    (SELECT COUNT(id) FROM backup_logs WHERE tenant_id = ? AND created_by_admin = 1 AND status = 10 AND MONTH(created_on) = MONTH(NOW()) AND YEAR(created_on) = YEAR(NOW())) as failedUploadedBackupsByAdmins
'
        ))->setBindings($bindings);

        // Nice little hack
        $value->from = null;

        return $value->get()[0];
    }
}
