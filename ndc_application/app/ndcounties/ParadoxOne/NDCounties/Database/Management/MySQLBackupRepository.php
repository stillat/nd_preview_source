<?php namespace ParadoxOne\NDCounties\Database\Management;

use Carbon\Carbon;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\File;
use ParadoxOne\NDCounties\Contracts\AccountRepositoryInterface;
use ParadoxOne\NDCounties\Contracts\Data\BackupLogRepositoryInterface;
use ParadoxOne\NDCounties\Contracts\Data\BackupRepositoryInterface;
use ParadoxOne\NDCounties\Contracts\SystemInformationRepositoryInterface;
use ParadoxOne\NDCounties\Database\Database;
use ParadoxOne\NDCounties\Database\Management\Encryption\Cryptographer;
use ParadoxOne\NDCounties\Database\Management\IO\LargeFileUtilities;
use ParadoxOne\NDCounties\Processes\ProcessManager;
use Stillat\Database\Tenant\TenantManager;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Process\Process;

class MySQLBackupRepository implements BackupRepositoryInterface
{
    protected $tenantAccount = null;

    protected $backupLocator = null;

    protected $backupLocation = '';

    protected $tenantManager;

    protected $backupDatabaseName = null;

    protected $accountManager = null;

    protected $backupLogger = null;

    protected $lastCreatedFile = '';

    protected $tenantSystemInformationRepository = null;

    public function __construct(BackupLocator $locator, AccountRepositoryInterface $accountRepository, BackupLogRepositoryInterface $backupLogger,
                                SystemInformationRepositoryInterface $systemInformation)
    {
        $this->backupLocator                     = $locator;
        $this->tenantManager                     = TenantManager::instance();
        $this->accountManager                    = $accountRepository;
        $this->backupLogger                      = $backupLogger;
        $this->tenantSystemInformationRepository = $systemInformation;
    }

    /**
     * Sets the tenant account.
     *
     * @param $tenantID
     * @throws InvalidArgumentException
     */
    public function setTenantAccount($tenantID)
    {
        if (is_integer($tenantID)) {
            $this->tenantAccount      = $tenantID;
            $this->backupLocation     = $this->backupLocator->getBackupLocation($tenantID);
            $this->backupDatabaseName = $this->tenantManager->getTierNameWithPrefix($tenantID);
        } else {
            throw new \InvalidArgumentException('Tenant ID must be an integer. ' . gettype($tenantID) . ' given.');
        }
    }

    /**
     * Gets the tenant account.
     *
     * @return integer
     */
    public function getTenantAccount()
    {
        return $this->tenantAccount;
    }

    public function createBackup()
    {
        set_time_limit(0);

        try {
            $this->accountManager->setAccountProcess($this->tenantAccount,
                                                     ProcessManager::PROCESS_DATABASE_BACKUP_CREATE);
            $databaseConfiguration = Config::get('database.connections.' . Config::get('database.default'));

            $backupFileLocation = $this->backupLocation . $this->backupLocator->getBackupFileName();

            $dumpCommand = sprintf('"%smysqldump" --user=%s --password=%s --host=%s %s > %s',
                                   Config::get('crms.db_dump_command_path'),
                                   escapeshellarg($databaseConfiguration['username']),
                                   escapeshellarg($databaseConfiguration['password']),
                                   escapeshellarg($databaseConfiguration['host']),
                                   escapeshellarg($this->backupDatabaseName),
                                   escapeshellarg($backupFileLocation)
            );

            $symfonyCommand = new Process($dumpCommand);
            $symfonyCommand->setTimeout(999999999);
            $symfonyCommand->run();

            if ($symfonyCommand->isSuccessful()) {

                // Now we need to add the ND Counties version number.
                LargeFileUtilities::prepend('/*NDCV:[' . Database::getVersion() . ']*/', $backupFileLocation,
                                            $this->backupLocation);

                if (Config::get('crms.db_gzip_dumps', false)) {
                    $gzipCommand = sprintf('"%sgzip" -9 %s',
                                           Config::get('crms.db_gzip_command_path'),
                                           escapeshellarg($backupFileLocation));
                    $gzipCommand = new Process($gzipCommand);

                    $gzipCommand->setTimeout(999999999);
                    $gzipCommand->run();

                    if (!$gzipCommand->isSuccessful()) {
                        $this->accountManager->releaseProcesses($this->tenantAccount);
                        @unlink($backupFileLocation);

                        $this->backupLogger->logCreateFailed(Auth::user(), basename($backupFileLocation),
                                                             $this->tenantAccount);
                        dd($gzipCommand->getErrorOutput());
                        return false;
                    }
                }

                $this->accountManager->releaseProcesses($this->tenantAccount);
                $this->lastCreatedFile = basename($backupFileLocation);
                $this->backupLogger->logCreate(Auth::user(), basename($backupFileLocation), $this->tenantAccount);

                return true;
            }
        } catch (\Exception $e) {
        }

        $this->accountManager->releaseProcesses($this->tenantAccount);

        $this->backupLogger->logCreateFailed(Auth::user(), basename($backupFileLocation), $this->tenantAccount);

        return false;
    }

    public function getBackups()
    {
        $files = File::allFiles($this->backupLocation);

        $modelFiles = [];

        foreach ($files as $file) {
            if (is_file($file) && (!is_dir($file))) {
                if (str_is('*.sql', $file) || str_is('*.sql.gz', $file)) {
                    $modelFiles[] = new BackupModel($file);
                }
            }
        }

        $modelFiles = array_reverse($modelFiles);

        $files = null;
        unset($files);

        return $modelFiles;
    }

    public function removeBackup($backupKey)
    {
        $location = realpath($this->backupLocation . '/' . $backupKey);

        if (strlen($location) < strlen($this->backupLocation)) {
            $this->backupLogger->logRemoveFailed(Auth::user(), $backupKey, $this->tenantAccount);

            return false;
        }

        $wasSuccessful = true;

        if (file_exists($location)) {
            $wasSuccessful = @unlink($location);
        }

        if ($wasSuccessful) {
            $this->backupLogger->logRemove(Auth::user(), $backupKey, $this->tenantAccount);
        } else {
            $this->backupLogger->logRemoveFailed(Auth::user(), $backupKey, $this->tenantAccount);
        }

        return $wasSuccessful;
    }

    public function restoreBackup($backupKey)
    {
        $location         = realpath($this->backupLocation . '/' . $backupKey);
        $originalLocation = $location;

        if (strlen($location) < strlen($this->backupLocation)) {
            return false;
        }


        if (file_exists($location)) {
            if ($this->backupLocator->isBackupCompressed($location)) {
                $location = $this->decompress($location);
            }

            $restoreVersion = Database::extractVersion(fgets(fopen($location, 'r')));

            $versionIsOlder = (intval($restoreVersion) < intval(Database::getVersion()));

            if (intval($restoreVersion) > intval(Database::getVersion())) {
                throw new IncompatibleRestoreTargetException('Restore file is from too new of a version of ND Counties');
            } elseif ($versionIsOlder) {
                // We need to proceed with caution, since the restore file is from an older version.
                // We will create a backup first.
                set_time_limit(0);
                $this->backupLogger->logCreate(Database::getSystemUser(), $this->lastCreatedFile, $this->tenantAccount);
                $this->createBackup();
            }

            set_time_limit(0);

            $databaseConfiguration = Config::get('database.connections.' . Config::get('database.default'));

            $restoreCommand = sprintf('"%smysql" --user=%s --password=%s --host=%s %s < %s',
                                      Config::get('crms.db_dump_command_path'),
                                      escapeshellarg($databaseConfiguration['username']),
                                      escapeshellarg($databaseConfiguration['password']),
                                      escapeshellarg($databaseConfiguration['host']),
                                      escapeshellarg($this->backupDatabaseName),
                                      escapeshellarg($location)
            );

            $restoreCommand = new Process($restoreCommand);
            $restoreCommand->setTimeout(999999999);
            $restoreCommand->run();

            if ($this->backupLocator->isBackupCompressed($originalLocation)) {
                $this->removeTemporaryRestoreFiles($originalLocation);
            }

            if ($restoreCommand->isSuccessful()) {
                if ($versionIsOlder) {
                    // Need to migrate the database, just in case.
                    $databaseName = TenantManager::instance()->getTierNameWithPrefix($this->tenantAccount);
                    try {
                        Artisan::call('migrate', [
                            '--database' => $databaseName,
                            '--path'     => 'app/database/tenants/'
                        ]);
                        TenantManager::instance()->restoreLaravel();
                        $this->backupLogger->logSystemSynced(Database::getSystemUser(), $this->lastCreatedFile,
                                                             $this->tenantAccount);
                    } catch (\Exception $e) {
                        $this->backupLogger->logSystemSyncedFailed(Database::getSystemUser(), $this->lastCreatedFile,
                                                                   $this->tenantAccount);
                    }
                }

                $this->backupLogger->logRestore(Auth::user(), basename($originalLocation), $this->tenantAccount);
                $this->tenantSystemInformationRepository->bringDatabaseVersionCurrent();
            } else {
                $this->backupLogger->logRestoreFailed(Auth::user(), basename($originalLocation), $this->tenantAccount);
            }

            return $restoreCommand->isSuccessful();
        }

        return true;
    }

    private function removeTemporaryRestoreFiles($file)
    {
        $status               = true;
        $fileNameDeCompressed = $this->backupLocator->getUncompressedFileName($file);

        if ($file !== $fileNameDeCompressed) {
            $status = File::delete($fileNameDeCompressed);
        }

        return $status;
    }

    private function decompress($file)
    {
        $fileNameDeCompressed = $this->backupLocator->getUncompressedFileName($file);
        $deCompressCommand    = sprintf('"%sgzip" -dc %s > %s',
                                        Config::get('crms.db_gzip_command_path'),
                                        escapeshellarg($file),
                                        escapeshellarg($fileNameDeCompressed));

        $deCompressCommand = new Process($deCompressCommand);

        $deCompressCommand->setTimeout(999999999);

        $status = $deCompressCommand->run();

        return $fileNameDeCompressed;
    }

    public function prepareUploadedBackup(UploadedFile $backupFile)
    {
        $filesToRemove = [];
        $filesToRemove[] = $this->backupLocator->getBackupProcessingLocation($this->tenantAccount).$backupFile->getClientOriginalName();
        $backupFile->move($this->backupLocator->getBackupProcessingLocation($this->tenantAccount), $backupFile->getClientOriginalName());

        // Need to decrypt.
        $cryptographer = new Cryptographer;
        $createdFile = $cryptographer->decrypt($this->backupLocator->getBackupProcessingLocation($this->tenantAccount).$backupFile->getClientOriginalName());

        if ($createdFile !== false) {
            $originalName = $createdFile;
            if (Config::get('crms.db_gzip_dumps', false)) {
                // Now we need to decompress that shit.
                $createdFile = $this->decompress($createdFile);
                $filesToRemove[] = $createdFile;
            }

            if (file_exists($createdFile)) {
                // Now we need to check the version system to make sure its legit.
                $version = Database::extractVersion(fgets(fopen($createdFile, 'r')));
                if ($version !== null) {
                    $copyToFileName = $backupFile->getClientOriginalName();

                    if (Config::get('crms.db_gzip_dumps', false)) {
                        $copyToFileName = str_replace('.ndsql', '.sql.gz', $copyToFileName);
                    } else {
                        $copyToFileName = str_replace('.ndsql', '.sql', $copyToFileName);
                    }
                    File::move($originalName, $this->backupLocator->getBackupLocation($this->tenantAccount).$copyToFileName);
                    File::delete($filesToRemove);
                    return true;
                }
            }
        }

        File::delete($filesToRemove);
        return false;
    }
}
