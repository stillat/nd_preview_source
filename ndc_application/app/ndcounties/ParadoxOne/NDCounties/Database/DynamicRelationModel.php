<?php namespace ParadoxOne\NDCounties\Database;

use Illuminate\Support\Facades\App;
use Stillat\Database\Tenant\Model as TenantModel;

class DynamicRelationModel extends TenantModel
{
    protected $adapterManagerInstance = null;

    private function dynamicRelationCreateAdapaterManager()
    {
        if (is_null($this->adapterManagerInstance)) {
            $this->adapterManagerInstance = App::make('ParadoxOne\NDCounties\Database\Adapters\AdapterManager');
        }
    }

    public function __call($method, $parameters)
    {
        $this->dynamicRelationCreateAdapaterManager();

        $relationships = $this->adapterManagerInstance->getRelationships();

        if (array_key_exists($method, $relationships)) {
            $func = $relationships[$method];

            return $func($this);
        }

        return parent::__call($method, $parameters);
    }
}
