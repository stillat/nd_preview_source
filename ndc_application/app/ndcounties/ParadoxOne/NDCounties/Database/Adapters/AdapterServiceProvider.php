<?php namespace ParadoxOne\NDCounties\Database\Adapters;

use Illuminate\Support\Facades\Config;
use Illuminate\Support\ServiceProvider;

class AdapterServiceProvider extends ServiceProvider
{
    protected $defer = false;

    public function register()
    {
        $this->registerAdapterManager();
    }

    private function registerAdapterManager()
    {
        $adapters = Config::get('adapters.adapters', []);

        $this->app->singleton('ParadoxOne\NDCounties\Database\Adapters\AdapterManager', function () use ($adapters) {
            return new AdapterManager($adapters);
        });
    }
}
