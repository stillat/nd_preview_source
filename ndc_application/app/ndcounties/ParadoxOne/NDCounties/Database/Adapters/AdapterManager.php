<?php namespace ParadoxOne\NDCounties\Database\Adapters;

use Illuminate\Support\Facades\App;

class AdapterManager
{
    /**
     * An array of adapter names, usually from the configuration.
     *
     * @var array
     */
    private $registeredAdapters = [];

    /**
     * An array of adapter instances.
     *
     * @var array
     */
    private $adapterInstances = [];

    private $filteredAdapters = [];

    /**
     * Returns a new instance of AdapterManager
     *
     * @param array $adapters The adapters (ideally from a configuration file).
     */
    public function __construct(array $adapters)
    {
        $this->registeredAdapters = $adapters;

        foreach ($this->registeredAdapters as $adapter) {
            $instance = App::make($adapter);
            $instance->boot();

            $this->adapterInstances[$adapter]                                 = $instance;
            $this->filteredAdapters[$instance->getAdapterNumericIdentifier()] = $instance;
        }
    }

    /**
     * Returns a collection of adapter instances.
     *
     * @return array
     */
    public function getAdapters()
    {
        return $this->filteredAdapters;
    }

    public function getSortedAdapters()
    {
        $adapters = $this->getAdapters();

        usort($adapters, function ($a, $b) {
            return strcmp($a->getName(), $b->getName());
        });

        return $adapters;
    }

    public function getRelationships()
    {
        $relationships = array();

        foreach ($this->adapterInstances as $adapter) {
            if (count($adapter->getPropertyRelationships()) > 0) {
                foreach ($adapter->getPropertyRelationships() as $relationshipName => $func) {
                    $relationships['relationship_' . $relationshipName] = $func;
                }
            }
        }

        return $relationships;
    }

    public function getRequiredRelationships()
    {
        $requiredRelationships = array();

        foreach ($this->adapterInstances as $adapter) {
            if (count($adapter->requires()) > 0) {
                foreach ($adapter->requires() as $requiredRelationship) {
                    $requiredRelationships[] = 'relationship_' . $requiredRelationship;
                }
            }
        }

        return $requiredRelationships;
    }

    public function getListHeaders($adapters)
    {
        $listHeaders = '';

        foreach ($this->getSortedAdapters() as $adapter) {
            if (in_array($adapter->getAdapterNumericIdentifier(), $adapters)) {
                $listHeaders .= $adapter->getListTableHeaders();
            }
        }

        return $listHeaders;
    }

    public function getListData($adapters, $data)
    {
        $listData = '';

        foreach ($this->getSortedAdapters() as $adapter) {
            if (in_array($adapter->getAdapterNumericIdentifier(), $adapters)) {
                $listData .= $adapter->formatListTableHeaders($data);
            }
        }

        return $listData;
    }

    public function formatPropertyWidget($adapter, $data)
    {
        if (array_key_exists($adapter, $this->filteredAdapters)) {
            return $this->filteredAdapters[$adapter]->formatPropertyView($data);
        }

        if ($adapter == '-1' || $adapter == 'e') {
            return $this->filteredAdapters[-1]->formatPropertyView($data);
        }

        return null;
    }

    public function getExportData($adapter, $data)
    {
        if (array_key_exists($adapter, $this->filteredAdapters)) {
            return $this->filteredAdapters[$adapter]->formatExportArray($data);
        }

        if ($adapter == '-1' || $adapter == 'e') {
            return $this->filteredAdapters[-1]->formatExportArray($data);
        }
    }

    public function formatReportView($adapter, $data)
    {
        if (array_key_exists($adapter, $this->filteredAdapters)) {
            return $this->filteredAdapters[$adapter]->formatReportView($data);
        }

        if ($adapter == '-1' || $adapter == 'e') {
            return $this->filteredAdapters[-1]->formatReportView($data);
        }

        return null;
    }

    public function shouldShowOnReports($adapter, $data)
    {
        if (array_key_exists($adapter, $this->filteredAdapters)) {
            return $this->filteredAdapters[$adapter]->shouldShowOnReports($data);
        }

        if ($adapter == '-1' || $adapter == 'e') {
            return $this->filteredAdapters[-1]->shouldShowOnReports($data);
        }

        return null;
    }

    public function formatPropertyInvoice($adapter, $data)
    {
        if (array_key_exists($adapter, $this->filteredAdapters)) {
            return $this->filteredAdapters[$adapter]->formatInvoiceListing($data);
        }

        if ($adapter == '-1' || $adapter == 'e') {
            return $this->filteredAdapters[-1]->formatInvoiceListing($data);
        }

        return null;
    }
}
