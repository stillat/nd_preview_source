<?php namespace ParadoxOne\NDCounties\Database\Adapters\GPS;

use ParadoxOne\NDCounties\Contracts\Extensibility\PropertyTypes\PropertyAdapterInterface;

class GPSAdapter implements PropertyAdapterInterface
{
    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'Global Positioning System (GPS) Data';
    }

    /**
     * {@inheritdoc}
     */
    public function getDescription()
    {
        return 'Allows a property to track a latitude, longitude and altitude points for a given property type.';
    }

    /**
     * {@inheritdoc}
     */
    public function getIsHidden()
    {
        return false;
    }

    /**
     * {@inheritdoc}
     */
    public function getAdapterNumericIdentifier()
    {
        return 2;
    }

    /**
     * {@inheritdoc}
     */
    public function getInsertView()
    {
        return 'app.work.adapters.gps.insert';
    }
}
