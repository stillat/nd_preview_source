<?php namespace ParadoxOne\NDCounties\Database\Adapters\Property\Handlers;

use ParadoxOne\NDCounties\Contracts\Extensibility\GenericHandlerInterface;
use ParadoxOne\NDCounties\Contracts\Properties\Roads\RoadAttributesRepositoryInterface;

class RoadPropertyUpdateHandler implements GenericHandlerInterface
{
    protected $attributesRepository;

    public function __construct(RoadAttributesRepositoryInterface $attributesRepository)
    {
        $this->attributesRepository = $attributesRepository;
    }

    public function logErrors()
    {
        // TODO: Implement logErrors() method.
    }

    public function getRecordPropertyTotal()
    {
        return 0;
    }


    public function handle($object, $validateOnly = false)
    {
        $surfaceType = $object->data['road_property_surface_type'];
        $roadLength  = $object->data['road_property_road_length'];
        $lengthUnit  = $object->data['road_property_length_unit'];

        $lengthUnit = intval($lengthUnit);

        $updateData = array('surface_type_id' => $surfaceType, 'road_length' => $roadLength, 'unit_id' => $lengthUnit);
        $this->attributesRepository->setRoad($object->id);

        if ($validateOnly) {
            $isValid = $this->attributesRepository->isValid($updateData);

            if ($isValid === false) {
                return false;
            } else {
                return true;
            }
        }

        $success = $this->attributesRepository->update($object->id, $updateData);

        if ($success === false) {
            return false;
        }

        return true;
    }

    public function handleLog($object, $validateOnly = false)
    {
        return true;
    }

    public function errors()
    {
        return $this->attributesRepository->errors();
    }
}
