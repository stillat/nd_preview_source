<?php namespace ParadoxOne\NDCounties\Database\Adapters\Property;

use ParadoxOne\NDCounties\Contracts\Extensibility\PropertyTypes\PropertyAdapterInterface;

class SignTypeAdapter implements PropertyAdapterInterface
{
    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'Sign Property Data';
    }

    /**
     * {@inheritdoc}
     */
    public function getDescription()
    {
        return 'Allows a property to track data related to signs, such as retro-reflectivity and distance from road.';
    }

    /**
     * {@inheritdoc}
     */
    public function getIsHidden()
    {
        return false;
    }

    /**
     * {@inheritdoc}
     */
    public function getAdapterNumericIdentifier()
    {
        return 3;
    }

    /**
     * {@inheritdoc}
     */
    public function getInsertView()
    {
        return 'app.work.adapters.road.insert';
    }
}
