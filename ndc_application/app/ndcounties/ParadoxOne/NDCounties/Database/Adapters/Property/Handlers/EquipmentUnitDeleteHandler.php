<?php namespace ParadoxOne\NDCounties\Database\Adapters\Property\Handlers;

use ParadoxOne\NDCounties\Contracts\EquipmentUnits\UnitsRepositoryInterface;
use ParadoxOne\NDCounties\Contracts\Extensibility\GenericHandlerInterface;

class EquipmentUnitDeleteHandler implements GenericHandlerInterface
{
    /**
     * The equipment units repository.
     *
     * @var ParadoxOne\NDCounties\Contracts\EquipmentUnits\UnitsRepositoryInterface
     */
    protected $equipmentUnitsRepository;

    public function __construct(UnitsRepositoryInterface $equipmentUnitsRepository)
    {
        $this->equipmentUnitsRepository = $equipmentUnitsRepository;
    }

    public function getRecordPropertyTotal()
    {
        return 0;
    }

    public function handle($object, $validateOnly = false)
    {
        if ($object->trueDelete == true) {
            // We need to pass the true in to the equipment units repository to get it
            // to remove the equipment unit data.

            $this->equipmentUnitsRepository->remove(array($object->id, true));
        }

        // The system is not going to remove the equipment unit information (such as salvage value, etc).
        // Because of this we will keep the associated data in case we need to recover it later with the
        // property.
        return true;
    }

    public function handleLog($object, $validateOnly = false)
    {
        return true;
    }

    public function logErrors()
    {
        return [];
    }

    public function errors()
    {
        return $this->equipmentUnitsRepository->errors();
    }
}
