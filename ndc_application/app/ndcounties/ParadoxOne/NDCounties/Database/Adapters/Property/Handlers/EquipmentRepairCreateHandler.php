<?php namespace ParadoxOne\NDCounties\Database\Adapters\Property\Handlers;

use ParadoxOne\NDCounties\Contracts\EquipmentUnits\WorkRecordRepositoryInterface;
use ParadoxOne\NDCounties\Contracts\Extensibility\GenericHandlerInterface;

class EquipmentRepairCreateHandler implements GenericHandlerInterface
{
    use \ParadoxOne\NDCounties\Traits\MathExecutableTrait;

    protected $workRecordRepository;

    protected $propertyTotal = 0;

    public function __construct(WorkRecordRepositoryInterface $workRecordRepository)
    {
        $this->workRecordRepository = $workRecordRepository;
    }

    public function handle($object, $validateOnly = false)
    {
        return true;
    }

    public function getRecordPropertyTotal()
    {
        return $this->propertyTotal;
    }


    public function handleLog($object, $validateOnly = false)
    {
        $createData = [
            'work_record_id'         => $object->id,
            'work_order_number'      => $object->data['equipment_repair_work_order_number'],
            'odometer_reading'       => $object->data['equipment_repair_odometer_reading'],
            'shop_costs'             => $object->data['equipment_repair_shop_parts'],
            'purchased_costs'        => $object->data['equipment_repair_invoice_parts'],
            'commercial_labor_costs' => $object->data['equipment_repair_contractor_labor'],
            'commercial_costs'       => $object->data['equipment_repair_contractor_costs'],
            'work_description'       => $object->data['equipment_repair_work_description'],
            'equipment_unit_id'      => $object->data['record_actual_property'],
        ];

        if ($validateOnly) {
            $isValid = $this->workRecordRepository->isValid($createData);

            if ($isValid === false) {
                return false;
            } else {
                return true;
            }
        }

        $success = $this->workRecordRepository->create($createData);
        if ($success == false) {
            return false;
        }

        $this->setMathEngine();
        $propertyTotal       = 0;
        $propertyTotal       = $this->getMathEngine()->add($object->data['equipment_repair_shop_parts'],
                                                           $object->data['equipment_repair_invoice_parts']);
        $propertyTotal       =
            $this->getMathEngine()->add($propertyTotal, $object->data['equipment_repair_contractor_labor']);
        $propertyTotal       =
            $this->getMathEngine()->add($propertyTotal, $object->data['equipment_repair_contractor_costs']);
        $this->propertyTotal = $propertyTotal;
        unset($propertyTotal);

        return true;
    }

    public function logErrors()
    {
        return $this->workRecordRepository->errors();
    }

    public function errors()
    {
        return [];
    }
}
