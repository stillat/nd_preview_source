<?php namespace ParadoxOne\NDCounties\Database\Adapters\Property;

use Illuminate\Support\Facades\App;
use ParadoxOne\NDCounties\Contracts\Extensibility\PropertyTypes\PropertyAdapterInterface;

class EquipmentUnitAdapter implements PropertyAdapterInterface
{
    /**
     * Called when the adapter is loaded.
     *
     * @return void
     */
    public function boot()
    {
        // TODO: Implement boot() method.
    }

    /**
     * Gets the backing table name.
     *
     * @return mixed
     */
    public function getTableName()
    {
        return 'equipment_work_record_repairs';
    }

    /**
     * Gets the backing work column name.
     *
     * @return mixed
     */
    public function getWorkColumnName()
    {
        return null;
    }

    /**
     * Gets the name of the property adapater.
     *
     * @return string The name of the adapater.
     */
    public function getName()
    {
        return 'Equipment Unit Property';
    }

    /**
     * Gets the description of the property adapter.
     *
     * @return string The description for the adapter.
     */
    public function getDescription()
    {
        return 'Allows users to track equipment unit information, such as make, model, rental rate and salvage value.';
    }

    /**
     * Gets the adapter's numeric identifier.
     *
     * @return int The identifier.
     */
    public function getAdapterNumericIdentifier()
    {
        return 2;
    }

    /**
     * Gets the adapters insert view.
     *
     * @return string The view name.
     */
    public function getInsertView()
    {
        return null;
    }

    /**
     * Gets the adapters update view.
     *
     * @return mixed
     */
    public function getUpdateView()
    {
        return null;
    }

    /**
     * Gets the adapaters create property view.
     *
     * This is used to inject additional data into the
     * create property process.
     *
     * @return string The view name.
     */
    public function getCreatePropertyView()
    {
        return 'app.work.adapters.equipment.unit_create';
    }

    /**
     * Gets the adapters create property handler.
     *
     * @return string The fully-qualified class name of the handler.
     */
    public function getCreatePropertyHandler()
    {
        return App::make('\ParadoxOne\NDCounties\Database\Adapters\Property\Handlers\EquipmentUnitCreateHandler');
    }

    /**
     * Gets the adapters update property view.
     *
     * @return string The view name.
     */
    public function getUpdatePropertyView()
    {
        return 'app.work.adapters.equipment.unit_edit';
    }

    /**
     * Gets the adapters update property handler.
     *
     * @return string The fully-qualified class name of the handler.
     */
    public function getUpdatePropertyHandler()
    {
        return App::make('ParadoxOne\NDCounties\Database\Adapters\Property\Handlers\EquipmentUnitUpdateHandler');
    }

    /**
     * Gets the adapters delete property handler.
     *
     * @return string The fully-qualified class name of the handler.
     */
    public function getDeletePropertyHandler()
    {
        return App::make('ParadoxOne\NDCounties\Database\Adapters\Property\Handlers\EquipmentUnitDeleteHandler');
    }

    /**
     * Determines if the adatper should be hidden.
     *
     * @return bool
     */
    public function getIsHidden()
    {
        return true;
    }

    /**
     * Returns the relationship that should be used for fetching data
     * using the property APIs.
     *
     * @return array
     */
    public function getPropertyRelationships()
    {
        return [
            $this->getAdapterNumericIdentifier() => function ($self) {
                return $self->hasOne('\Service\EquipmentUnits\Unit', 'property_id', 'id');
            }
        ];
    }

    /**
     * Returns an array of the relationships the adapter provides.
     *
     * @return array
     */
    public function requires()
    {
        return [
            $this->getAdapterNumericIdentifier()
        ];
    }

    /**
     * Returns a string of table headers to be used when listing the data.
     *
     * @return string
     */
    public function getListTableHeaders()
    {
        return '<th>Serial Number</th><th class="text-right">Rental Rate</th><th class="text-right">Salvage Value</th><th class="text-right">Purchase Cost</th><th>Year</th><th>Make</th><th>Model</th>';
    }

    /**
     * Returns an array of headers to be used when exporting data.
     *
     * @return mixed
     */
    public function getExportHeaders()
    {
        return ['Equip. Serial Number', 'Equip. Rental Rate', 'Equip. Salvage Value', 'Equip. Purchase Cost', 'Equip. Year', 'Equip. Make', 'Equip. Model'];
    }

    /**
     * Returns an array of data to be used when building export data.
     *
     * @param $data
     * @return mixed
     */
    public function formatExportArray($data)
    {
        $equipmentData = $data->{'relationship_'.$this->getAdapterNumericIdentifier()};

        $exportData = [];
        $exportData[] = $equipmentData->serial_number;
        $exportData[] = $equipmentData->rental_rate;
        $exportData[] = $equipmentData->salvage_value;
        $exportData[] = $equipmentData->purchase_cost;
        $exportData[] = $equipmentData->year;
        $exportData[] = $equipmentData->make;
        $exportData[] = $equipmentData->model;

        return $exportData;
    }


    /**
     * Generates a HTML fragment to display the data.
     *
     * @param  mixed $data
     * @return string
     */
    public function formatListTableHeaders($data)
    {
        $equipmentData = $data->{'relationship_' . $this->getAdapterNumericIdentifier()};
        $extraData     = '';

        $extraData .= '<td>' . e($equipmentData->serial_number) . '</td>';
        $extraData .= '<td class="text-right">' . e(nd_number_format($equipmentData->rental_rate)) . '</td>';
        $extraData .= '<td class="text-right">' . e(nd_number_format($equipmentData->salvage_value)) . '</td>';
        $extraData .= '<td class="text-right">' . e(nd_number_format($equipmentData->purchase_cost)) . '</td>';
        $extraData .= '<td>' . e($equipmentData->year) . '</td>';
        $extraData .= '<td>' . e($equipmentData->make) . '</td>';
        $extraData .= '<td>' . e($equipmentData->model) . '</td>';


        return $extraData;
    }

    /**
     * Generates a HTML fragment to display in property views.
     *
     * @param $data
     * @return string
     */
    public function formatPropertyView($data)
    {
        // TODO: Implement formatPropertyView() method.
    }

    /**
     * Generates a HTML fragment to display on invoices.
     *
     * @param $data
     * @return mixed
     */
    public function formatInvoiceListing($data)
    {
        // TODO: Implement formatInvoiceListing() method.
    }

    public function formatReportView($data)
    {
        // TODO: Implement formatReportView() method.
    }

    public function shouldShowOnReports($data)
    {
        return true;
    }
}
