<?php namespace ParadoxOne\NDCounties\Database\Adapters\Property;

use ParadoxOne\NDCounties\Contracts\Extensibility\PropertyTypes\PropertyAdapterInterface;

class WellTypeAdapter implements PropertyAdapterInterface
{
    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'Oil Well Data';
    }

    /**
     * {@inheritdoc}
     */
    public function getDescription()
    {
        return 'Allows a property to track crucial information for oil wells.';
    }

    /**
     * {@inheritdoc}
     */
    public function getIsHidden()
    {
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function getAdapterNumericIdentifier()
    {
        return 300;
    }

    /**
     * {@inheritdoc}
     */
    public function getInsertView()
    {
        return 'app.work.adapters.road.insert';
    }
}
