<?php namespace ParadoxOne\NDCounties\Database\Adapters\Property;

use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\View;
use ParadoxOne\NDCounties\Contracts\Extensibility\PropertyTypes\PropertyAdapterInterface;
use UI;

class EquipmentRepairAdapter implements PropertyAdapterInterface
{
    /**
     * Called when the adapter is loaded.
     *
     * @return void
     */
    public function boot()
    {
        return;
    }

    /**
     * Gets the name of the property adapater.
     *
     * @return string The name of the adapater.
     */
    public function getName()
    {
        return 'Equipment Repair Data';
    }

    /**
     * Gets the backing table name.
     *
     * @return mixed
     */
    public function getTableName()
    {
        return 'equipment_work_record_repairs';
    }

    /**
     * Gets the backing work column name.
     *
     * @return mixed
     */
    public function getWorkColumnName()
    {
        return 'work_record_id';
    }


    /**
     * Gets the description of the property adapter.
     *
     * @return string The description for the adapter.
     */
    public function getDescription()
    {
        return 'Allows a user to track equipment repair costs.';
    }

    /**
     * Gets the adapter's numeric identifier.
     *
     * @return int The identifier.
     */
    public function getAdapterNumericIdentifier()
    {
        return -1;
    }

    /**
     * Gets the adapters insert view.
     *
     * @return string The view name.
     */
    public function getInsertView()
    {
        return 'app.work.adapters.equipment.repair_create';
    }

    /**
     * Gets the adapters update view.
     *
     * @return mixed
     */
    public function getUpdateView()
    {
        return 'app.work.adapters.equipment.repair_update';
    }

    /**
     * Gets the adapaters create property view.
     *
     * This is used to inject additional data into the
     * create property process.
     *
     * @return string The view name.
     */
    public function getCreatePropertyView()
    {
        return null;
    }

    /**
     * Gets the adapters update property view.
     *
     * @return string The view name.
     */
    public function getUpdatePropertyView()
    {
        return null;
    }

    /**
     * Gets the adapters create property handler.
     *
     * @return string The fully-qualified class name of the handler.
     */
    public function getCreatePropertyHandler()
    {
        return App::make('ParadoxOne\NDCounties\Database\Adapters\Property\Handlers\EquipmentRepairCreateHandler');
    }

    /**
     * Gets the adapters update property handler.
     *
     * @return string The fully-qualified class name of the handler.
     */
    public function getUpdatePropertyHandler()
    {
        // TODO: Implement getUpdatePropertyHandler() method.
    }

    /**
     * Gets the adapters delete property handler.
     *
     * @return string The fully-qualified class name of the handler.
     */
    public function getDeletePropertyHandler()
    {
        return App::make('ParadoxOne\NDCounties\Database\Adapters\Property\Handlers\EquipmentRepairDeleteHandler');
    }

    /**
     * Determines if the adatper should be hidden.
     *
     * @return bool
     */
    public function getIsHidden()
    {
        return true;
    }

    /**
     * Returns the relationship that should be used for fetching data
     * using the property APIs.
     *
     * @return array
     */
    public function getPropertyRelationships()
    {
        return [];
    }

    /**
     * Returns an array of the relationships the adapter provides.
     *
     * @return array
     */
    public function requires()
    {
        return [];
    }

    /**
     * Returns a string of table headers to be used when listing the data.
     *
     * @return string
     */
    public function getListTableHeaders()
    {
        return null;
    }

    /**
     * Returns an array of headers to be used when exporting data.
     *
     * @return mixed
     */
    public function getExportHeaders()
    {
        return [];
    }

    /**
     * Generates a HTML fragment to display the data.
     *
     * @param  mixed $data
     * @return string
     */
    public function formatListTableHeaders($data)
    {
        return null;
    }

    /**
     * Returns an array of data to be used when building export data.
     *
     * @param $data
     * @return mixed
     */
    public function formatExportArray($data)
    {
        return [];
    }

    /**
     * Generates a HTML fragment to display in property views.
     *
     * @param $data
     * @return string
     */
    public function formatPropertyView($data)
    {
        return View::make('app.work.adapters.equipment.widget')->with('data', $data);
    }

    /**
     * Generates a HTML fragment to display on invoices.
     *
     * @param $data
     * @return mixed
     */
    public function formatInvoiceListing($data)
    {
        // TODO: Implement formatInvoiceListing() method.
    }

    public function formatReportView($data)
    {
        return View::make('app.reporting.adapters.equip_repair')->with('data', $data[0])->with('record', $data[1])
                   ->with('settings', $data[2])->render();
    }

    public function shouldShowOnReports($data)
    {
        $showOnReport = false;

        if (strlen(trim($data->work_order_number)) > 0) {
            $showOnReport = true;
        }

        if (strlen(trim($data->work_description)) > 0) {
            $showOnReport = true;
        }

        if (property_exists($data, 'odometer') && $data->odometer !== null && strlen(trim($data->odometer->odometer_reading)) > 0)
        {
            $showOnReport = true;
        }

        if (($data->commercial_costs + $data->commercial_labor_costs + $data->shop_costs + $data->purchased_costs) > 0) {
            $showOnReport = true;
        }

        return $showOnReport;
    }
}
