<?php namespace ParadoxOne\NDCounties\Database\Adapters\Property\Handlers;

use ParadoxOne\NDCounties\Contracts\Extensibility\GenericHandlerInterface;
use ParadoxOne\NDCounties\Contracts\Properties\Roads\RoadAttributesRepositoryInterface;
use ParadoxOne\NDCounties\Contracts\Properties\Roads\WorkRecordRepositoryInterface;
use ParadoxOne\NDCounties\Database\Repositories\Properties\Roads\RoadsWorkRecordRepository;

class RoadPropertyCreateHandler implements GenericHandlerInterface
{
    protected $attributesRepository;

    protected $roadWorkRecordRepository;

    public function __construct(RoadAttributesRepositoryInterface $attributesRepository, WorkRecordRepositoryInterface $roadsWorkRecordRepository)
    {
        $this->attributesRepository     = $attributesRepository;
        $this->roadWorkRecordRepository = $roadsWorkRecordRepository;
    }

    public function handle($object, $validateOnly = false)
    {
        $surfaceType = $object->data['road_property_surface_type'];
        $roadLength  = $object->data['road_property_road_length'];
        $lengthUnit  = $object->data['road_property_length_unit'];

        $createData = array('surface_type_id' => $surfaceType, 'road_length' => $roadLength, 'unit_id' => $lengthUnit);
        $this->attributesRepository->setRoad($object->id);

        if ($validateOnly) {
            // Set the road to 0 during the validate only method.
            $this->attributesRepository->setRoad(0);

            $isValid = $this->attributesRepository->isValid($createData);

            if ($isValid === false) {
                return false;
            } else {
                return true;
            }
        }

        $success = $this->attributesRepository->create($createData);

        if ($success === false) {
            return false;
        }

        return true;
    }

    public function getRecordPropertyTotal()
    {
        return 0;
    }

    public function handleLog($object, $validateOnly = false)
    {
        $surfaceType = $object->data['road_property_surface_type'];
        $roadLength  = $object->data['road_property_road_length'];
        $lengthUnit  = $object->data['road_property_length_unit'];

        $createData =
            array('work_record_id' => $object->id, 'surface_type_id' => $surfaceType, 'road_length' => $roadLength,
                  'unit_id'        => $lengthUnit);

        if ($validateOnly) {
            $isValid = $this->roadWorkRecordRepository->isValid($createData);

            if ($isValid === false) {
                return false;
            } else {
                return true;
            }
        }

        $success = $this->roadWorkRecordRepository->create($createData);

        if ($success === false) {
            return false;
        }

        return true;
    }

    public function logErrors()
    {
        return $this->roadWorkRecordRepository->errors();
    }

    public function errors()
    {
        return $this->attributesRepository->errors();
    }
}
