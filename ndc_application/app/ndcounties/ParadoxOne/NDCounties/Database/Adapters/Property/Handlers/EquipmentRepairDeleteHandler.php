<?php namespace ParadoxOne\NDCounties\Database\Adapters\Property\Handlers;

use ParadoxOne\NDCounties\Contracts\Extensibility\GenericHandlerInterface;
use ParadoxOne\NDCounties\Database\Repositories\EquipmentUnits\WorkRecordRepository;

class EquipmentRepairDeleteHandler implements GenericHandlerInterface
{
    protected $equipmentWorkEntryRepository;

    public function __construct(WorkRecordRepository $equipmentWorkEntryRepository)
    {
        $this->equipmentWorkEntryRepository = $equipmentWorkEntryRepository;
    }

    public function getRecordPropertyTotal()
    {
        return 0;
    }


    public function handle($object, $validateOnly = false)
    {
        $details    = [];
        $details[0] = $object->id;

        if ($object->trueDelete == false) {
            $details[1] = true;
        }

        return $this->equipmentWorkEntryRepository->remove($details);
    }

    public function handleLog($object, $validateOnly = false)
    {
        return true;
    }

    public function logErrors()
    {
        // TODO: Implement logErrors() method.
    }

    public function errors()
    {
        // TODO: Implement errors() method.
    }
}
