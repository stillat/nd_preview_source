<?php namespace ParadoxOne\NDCounties\Database\Adapters\Property\Handlers;

use ParadoxOne\NDCounties\Contracts\EquipmentUnits\UnitsRepositoryInterface;
use ParadoxOne\NDCounties\Contracts\Extensibility\GenericHandlerInterface;

class EquipmentUnitUpdateHandler implements GenericHandlerInterface
{
    /**
     * The equipment units repository.
     *
     * @var ParadoxOne\NDCounties\Contracts\EquipmentUnits\UnitsRepositoryInterface
     */
    protected $equipmentUnitsRepository;

    public function __construct(UnitsRepositoryInterface $equipmentUnitsRepository)
    {
        $this->equipmentUnitsRepository = $equipmentUnitsRepository;
    }

    public function getRecordPropertyTotal()
    {
        return 0;
    }

    public function handle($object, $validateOnly = false)
    {
        if ($validateOnly) {
            $isValid = $this->equipmentUnitsRepository->isValid($object->data);

            if ($isValid === false) {
                return false;
            } else {
                return true;
            }
        }

        $success = $this->equipmentUnitsRepository->update($object->id, $object->data);

        if ($success === false) {
            return false;
        }

        return true;
    }

    public function handleLog($object, $validateOnly = false)
    {
        return true;
    }

    public function logErrors()
    {
        return [];
    }

    public function errors()
    {
        return $this->equipmentUnitsRepository->errors();
    }
}
