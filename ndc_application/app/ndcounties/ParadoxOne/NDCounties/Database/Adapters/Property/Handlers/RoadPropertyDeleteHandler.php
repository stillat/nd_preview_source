<?php namespace ParadoxOne\NDCounties\Database\Adapters\Property\Handlers;

use ParadoxOne\NDCounties\Contracts\Extensibility\GenericHandlerInterface;
use ParadoxOne\NDCounties\Contracts\Properties\Roads\RoadAttributesRepositoryInterface;

class RoadPropertyDeleteHandler implements GenericHandlerInterface
{
    protected $attributesRepository;

    public function __construct(RoadAttributesRepositoryInterface $attributesRepository)
    {
        $this->attributesRepository = $attributesRepository;
    }

    public function handle($object, $validateOnly = false)
    {
        if ($object->trueDelete == true) {
            // We need to pass the true in to the attributes repository to get it
            // to remove the road attributes record.
            $this->attributesRepository->remove(array($object->id, true));
        }

        // The system is not truly removing the property record. Because of this
        // we will keep the associated data in case we need to recover it later
        // with the property.
        return true;
    }

    public function handleLog($object, $validateOnly = false)
    {
    }

    public function getRecordPropertyTotal()
    {
        return 0;
    }

    public function logErrors()
    {
        // TODO: Implement logErrors() method.
    }


    public function errors()
    {
        return $this->attributesRepository->errors();
    }
}
