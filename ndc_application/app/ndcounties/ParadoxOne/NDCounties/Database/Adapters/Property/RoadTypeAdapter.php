<?php namespace ParadoxOne\NDCounties\Database\Adapters\Property;

use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\View;
use ParadoxOne\NDCounties\Contracts\Extensibility\PropertyTypes\PropertyAdapterInterface;
use UI;

class RoadTypeAdapter implements PropertyAdapterInterface
{
    /**
     * {@inheritdoc}
     *
     * @return void
     */
    public function boot()
    {
        UI::enqueueScript('adapters/road.min.js');
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'Road Property Data';
    }

    /**
     * Gets the backing table name.
     *
     * @return mixed
     */
    public function getTableName()
    {
        return 'road_work_record_logs';
    }

    /**
     * Gets the backing work column name.
     *
     * @return mixed
     */
    public function getWorkColumnName()
    {
        return 'work_record_id';
    }


    /**
     * {@inheritdoc}
     */
    public function getDescription()
    {
        return 'Allows a property to track road-specific data, such as road length and surface type.';
    }

    /**
     * {@inheritdoc}
     */
    public function getIsHidden()
    {
        return false;
    }

    /**
     * {@inheritdoc}
     */
    public function getAdapterNumericIdentifier()
    {
        return 1;
    }

    /**
     * {@inheritdoc}
     */
    public function getInsertView()
    {
        return 'app.work.adapters.road.log_create';
    }

    /**
     * Gets the adapters update view.
     *
     * @return mixed
     */
    public function getUpdateView()
    {
        return 'app.work.adapters.road.log_update';
    }


    /**
     * {@inheritdoc}
     */
    public function getCreatePropertyView()
    {
        return 'app.work.adapters.road.create_property';
    }

    /**
     * {@inheritdoc}
     */
    public function getUpdatePropertyView()
    {
        return 'app.work.adapters.road.update_property';
    }

    /**
     * {@inheritdoc}
     */
    public function getCreatePropertyHandler()
    {
        return App::make('\ParadoxOne\NDCounties\Database\Adapters\Property\Handlers\RoadPropertyCreateHandler');
    }

    /**
     * {@inheritdoc}
     */
    public function getUpdatePropertyHandler()
    {
        return App::make('\ParadoxOne\NDCounties\Database\Adapters\Property\Handlers\RoadPropertyUpdateHandler');
    }

    /**
     * {@inheritdoc}
     */
    public function getDeletePropertyHandler()
    {
        return App::make('\ParadoxOne\NDCounties\Database\Adapters\Property\Handlers\RoadPropertyDeleteHandler');
    }

    /**
     * {@inheritdoc}
     */
    public function getPropertyRelationships()
    {
        return array(
            $this->getAdapterNumericIdentifier() => function ($self) {
                return $self->hasOne('\Service\Properties\Road\RoadAttributes', 'road_id', 'id');
            }
        );
    }

    /**
     * {@inheritdoc}
     */
    public function requires()
    {
        return array(
            $this->getAdapterNumericIdentifier(),
            $this->getAdapterNumericIdentifier() . '.surfaceType',
        );
    }

    /**
     * {@inheritdoc}
     */
    public function getListTableHeaders()
    {
        return '<th class="text-right">Road Length</th><th class="text-right">Surface Type</th>';
    }

    /**
     * Returns an array of headers to be used when exporting data.
     *
     * @return mixed
     */
    public function getExportHeaders()
    {
        return ['Road Length', 'Road Surface Type'];
    }

    /**
     * Returns an array of data to be used when building export data.
     *
     * @param $data
     * @return mixed
     */
    public function formatExportArray($data)
    {
        $roadAttributes = $data->{'relationship_'.$this->getAdapterNumericIdentifier()};

        $exportData = [];
        $exportData[] = $roadAttributes->road_length;
        $exportData[] = $roadAttributes->surfaceType->name;

        return $exportData;
    }

    /**
     * {@inheritdoc}
     */
    public function formatListTableHeaders($data)
    {
        $roadAttributes = $data->{'relationship_' . $this->getAdapterNumericIdentifier()};

        $extraData = '';

        $extraData .= '<td class="text-right">' . e(nd_number_format($roadAttributes->road_length)) . '</td>';
        $extraData .= '<td class="text-right">' . e($roadAttributes->surfaceType->code) . ' - ' .
                      e($roadAttributes->surfaceType->name) . '</td>';

        return $extraData;
    }

    /**
     * Generates a HTML fragment to display in property views.
     *
     * @param $data
     * @return string
     */
    public function formatPropertyView($data)
    {
        $type = null;
        $unit = null;

        if ($data->surface_type_id !== 0) {
            $surfaceRepo = App::make('ParadoxOne\NDCounties\Contracts\Properties\Roads\SurfaceTypeRepositoryInterface');
            $type        = $surfaceRepo->getModelById($data->surface_type_id);

            $surfaceRepo = null;
            unset($surfaceRepo);
        } else {
            $type       = new \stdClass;
            $type->code = 'NA';
            $type->name = 'Not Available';
        }


        if (intval($data->unit_id) !== 9) {
            $unitRepo = App::make('ParadoxOne\NDCounties\Contracts\UnitRepositoryInterface');
            $unit     = $unitRepo->getModelById($data->unit_id);

            $unitRepo = null;
            unset($unitRepo);
        } else {
            $unit       = new \stdClass;
            $unit->code = '';
            $unit->name = 'None';
        }

        return View::make('app.work.adapters.road.widget')
                   ->with('data', $data)
                   ->with('type', $type)
                   ->with('unit', $unit)->render();
    }

    /**
     * Generates a HTML fragment to display on invoices.
     *
     * @param $data
     * @return mixed
     */
    public function formatInvoiceListing($data)
    {
        $type = null;
        $unit = null;

        if ($data[1]->surface_type_id !== 0) {
            $surfaceRepo = App::make('ParadoxOne\NDCounties\Contracts\Properties\Roads\SurfaceTypeRepositoryInterface');
            $type        = $surfaceRepo->getModelById($data[1]->surface_type_id);

            $surfaceRepo = null;
            unset($surfaceRepo);
        } else {
            $type       = new \stdClass;
            $type->code = 'NA';
            $type->name = 'Not Available';
        }


        if (intval($data[1]->unit_id) !== 9) {
            $unitRepo = App::make('ParadoxOne\NDCounties\Contracts\UnitRepositoryInterface');
            $unit     = $unitRepo->getModelById($data[1]->unit_id);

            $unitRepo = null;
            unset($unitRepo);
        } else {
            $unit       = new \stdClass;
            $unit->code = '';
            $unit->name = 'None';
        }

        return View::make('app.work.adapters.road.invoice')
                   ->with('data', $data[1])
                   ->with('record', $data[0])
                   ->with('type', $type)
                   ->with('unit', $unit)->render();
    }

    public function formatReportView($data)
    {
        $type = null;
        $unit = null;

        if (property_exists($data[0], 'surface_type_id') && $data[0]->surface_type_id !== 0) {
            $surfaceRepo = App::make('ParadoxOne\NDCounties\Contracts\Properties\Roads\SurfaceTypeRepositoryInterface');
            $type        = $surfaceRepo->getModelById($data[0]->surface_type_id);

            $surfaceRepo = null;
            unset($surfaceRepo);
        } else {
            $type       = new \stdClass;
            $type->code = 'NA';
            $type->name = 'Not Available';
        }


        if (property_exists($data[0], 'unit_id') && intval($data[0]->unit_id) !== 9) {
            $unitRepo = App::make('ParadoxOne\NDCounties\Contracts\UnitRepositoryInterface');
            $unit     = $unitRepo->getModelById($data[0]->unit_id);

            $unitRepo = null;
            unset($unitRepo);
        } else {
            $unit       = new \stdClass;
            $unit->code = '';
            $unit->name = 'None';
        }

        return View::make('app.reporting.adapters.road')
                   ->with('data', $data[0])
                   ->with('settings', $data[1])
                   ->with('type', $type)
                   ->with('unit', $unit)->render();
    }

    public function shouldShowOnReports($data)
    {
        if (property_exists($data, 'road_length') && strlen($data->road_length) > 0) {
            return true;
        }

        return false;
    }

}
