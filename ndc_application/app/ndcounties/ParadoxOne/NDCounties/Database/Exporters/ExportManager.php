<?php namespace ParadoxOne\NDCounties\Database\Exporters;

use Carbon\Carbon;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use ParadoxOne\NDCounties\Contracts\Data\DataExporterInterface;

class ExportManager
{
    protected $iocBindings = [
        'org_activity' => 'ParadoxOne\NDCounties\Database\Exporters\CSVExporters\ActivityExporter',
        'org_departments' => 'ParadoxOne\NDCounties\Database\Exporters\CSVExporters\DepartmentExporter',
        'org_districts' => 'ParadoxOne\NDCounties\Database\Exporters\CSVExporters\DistrictsExporter',
        'org_projects' => 'ParadoxOne\NDCounties\Database\Exporters\CSVExporters\ProjectsExporter',
        'res_employees' => 'ParadoxOne\NDCounties\Database\Exporters\CSVExporters\EmployeeExporter',
        'res_fuels' => 'ParadoxOne\NDCounties\Database\Exporters\CSVExporters\FuelsExporter',
        'res_materials' => 'ParadoxOne\NDCounties\Database\Exporters\CSVExporters\MaterialsExporter',
        'fs_accounts' => 'ParadoxOne\NDCounties\Database\Exporters\CSVExporters\FinanceAccountsExporter',
        'fs_tax_rates' => 'ParadoxOne\NDCounties\Database\Exporters\CSVExporters\TaxRatesExporter',
        'fs_invoices' => 'ParadoxOne\NDCounties\Database\Exporters\CSVExporters\InvoicesExporter',
        'work_records' => 'ParadoxOne\NDCounties\Database\Exporters\CSVExporters\WorkRecordsExporter',
        'cons_adjustments' => 'ParadoxOne\NDCounties\Database\Exporters\CSVExporters\InventoryAdjustmentsExporter',
        'fs_accounts_transactions' => 'ParadoxOne\NDCounties\Database\Exporters\CSVExporters\TransactionsExporter',
        'fs_invoice_payments' => 'ParadoxOne\NDCounties\Database\Exporters\CSVExporters\InvoicePaymentsExporter',
        'gen_properties' => 'ParadoxOne\NDCounties\Database\Exporters\CSVExporters\PropertiesExporter',
        'gen_properties_equipments' => 'ParadoxOne\NDCounties\Database\Exporters\CSVExporters\EquipmentUnitsExporter',
    ];

    /**
     * @var DataExporterInterface
     */
    protected $exporterInstance;

    /**
     * Applies all the settings that are needed.
     *
     * Usually comes from user input.
     *
     * @param $settingData
     */
    public function applySettings($settingData)
    {
        $settingData = new Collection($settingData);

        if (array_key_exists($settingData->get('export_data'), $this->iocBindings)) {
            $this->exporterInstance = App::make($this->iocBindings[$settingData->get('export_data')]);
        }

        $this->exporterInstance->setStartDate((new Carbon($settingData->get('start_date')))->toDateTimeString());
        $this->exporterInstance->setEndDate((new Carbon($settingData->get('end_date')))->toDateString());

        if ($settingData->get('export_type') == 'list') {
            $this->exporterInstance->setGetAllListData(true);
        } else {
            $this->exporterInstance->setGetAllListData(false);
        }

        $this->exporterInstance->setAggregateEmployeeRegularHours($settingData->has('employee_regular_hours'));
        $this->exporterInstance->setAggregateEmployeeRegularCost($settingData->has('employee_regular_cost'));
        $this->exporterInstance->setAggregateEmployeeRegularBenefits($settingData->has('employee_regular_benefits'));
        $this->exporterInstance->setAggregateEmployeeOvertimeHours($settingData->has('employee_overtime_hours'));
        $this->exporterInstance->setAggregateEmployeeOvertimeCost($settingData->has('employee_overtime_cost'));
        $this->exporterInstance->setAggregateEmployeeOvertimeBenefits($settingData->has('employee_overtime_benefits'));
        $this->exporterInstance->setAggregateEquipmentQuantity($settingData->has('equipment_quantity'));
        $this->exporterInstance->setAggregateEquipmentCost($settingData->has('equipment_cost'));
        $this->exporterInstance->setAggregateMaterialQuantity($settingData->has('material_quantity'));
        $this->exporterInstance->setAggregateMaterialCost($settingData->has('material_cost'));
        $this->exporterInstance->setAggregateFuelQuantity($settingData->has('fuel_quantity'));
        $this->exporterInstance->setAggregateFuelCost($settingData->has('fuel_cost'));
        $this->exporterInstance->setAggregatePropertyCost($settingData->has('property_cost'));
        $this->exporterInstance->setAggregateBillingQuantity($settingData->has('billing_quantity'));
        $this->exporterInstance->setAggregateBillingCost($settingData->has('billing_cost'));

        if ($settingData->has('restrictions')) {
            $restrictions = $settingData->get('restrictions');
            $this->exporterInstance->setRecordRestrictions($restrictions);
        }

        // We will now generate a file name.
        $fileName = $this->exporterInstance->getBaseName() . ' ' . (new Carbon());

        // Create the export_users dir.
        if (!file_exists(storage_path() . '/user_exports/')) {
            mkdir(storage_path() . '/user_exports/');
            // Create a `.gitignore` file.
            git_ignore(storage_path() . '/user_exports/.gitignore');
        }

        if (!file_exists(user_exports_path(Auth::user()->last_tenant) . strtolower($this->exporterInstance->getBaseName()) . '/')) {
            mkdir(user_exports_path(Auth::user()->last_tenant) . strtolower($this->exporterInstance->getBaseName()) . '/');
        }

        $fileName = str_replace(':', '-', $fileName);
        $exportFilePath = user_exports_path(Auth::user()->last_tenant) . strtolower($this->exporterInstance->getBaseName()) . '/' . $fileName . '.csv';
        touch($exportFilePath);

        $this->exporterInstance->setPath($exportFilePath);
        $this->exporterInstance->setFileName($fileName);
        $this->exporterInstance->setRawOptions($settingData);
    }

    public function export()
    {
        return $this->exporterInstance->export();
    }
}
