<?php namespace ParadoxOne\NDCounties\Database\Exporters\CSVExporters;

use League\Csv\Reader;
use ParadoxOne\NDCounties\Contracts\Data\DataExporterInterface;
use ParadoxOne\NDCounties\Contracts\Projects\ProjectRepositoryInterface;

class ProjectsExporter extends AbstractCSVExporter implements DataExporterInterface
{
    protected $projectsRepository;


    public function __construct(ProjectRepositoryInterface $projectsRepository)
    {
        $this->projectsRepository = $projectsRepository;
    }

    public function getBaseName()
    {
        return 'Projects';
    }


    private function getProjectsHeaders()
    {
        return array_merge(['Code', 'Name', 'Description', 'FEMA Project'], $this->getAggregateHeaders());
    }

    public function export()
    {
        $this->csvWriter->insertOne($this->getProjectsHeaders());

        $projects = $this->projectsRepository->getProjects()->select('code', 'name', 'description', 'fema_project', 'id');

        if (count($this->restrictedRecords) > 0) {
            $projects = $projects->whereIn('id', $this->restrictedRecords);
        }

        $projects = $projects->get();

        if (count($projects) == 0) {
            return $this->redirectOnEmpty('Projects');
        }

        $exportProjects = $this->formatDataForExport($projects, ['code', 'name', 'description', 'fema_project'], 'id', 'project_id');

        $this->csvWriter->insertAll($exportProjects);

        $reader = Reader::createFromPath($this->getPath());
        $this->sendHeaders($this->getFileName());
        $reader->output();
    }
}
