<?php namespace ParadoxOne\NDCounties\Database\Exporters\CSVExporters;

use Illuminate\Support\Str;
use League\Csv\Reader;
use ParadoxOne\NDCounties\Contracts\Data\DataExporterInterface;
use ParadoxOne\NDCounties\Contracts\InventoryManagement\AdjustmentRepositoryInterface;
use ParadoxOne\NDCounties\Contracts\Resources\MaterialRepositoryInterface;

class InventoryAdjustmentsExporter extends AbstractCSVExporter implements DataExporterInterface
{
    protected $adjustments;

    protected $consumables;

    protected $consumableID = null;

    protected $consumable;

    protected $adjustmentData;

    public function __construct(AdjustmentRepositoryInterface $adjustments, MaterialRepositoryInterface $consumables)
    {
        $this->adjustments = $adjustments;
        $this->consumables = $consumables;
    }

    private function applyExtraSettings()
    {
        if ($this->rawOptions->has('consumable')) {
            $this->consumableID = $this->rawOptions->get('consumable');
        }

        if ($this->consumableID != null) {
            $this->consumable = $this->consumables->getConsumableByID($this->consumableID);
            $this->adjustmentData = $this->adjustments->getAdjustments($this->consumableID);

            if (count($this->restrictedRecords) > 0) {
                $this->adjustmentData = $this->adjustmentData->whereIn('id', $this->restrictedRecords);
            }
            $this->adjustmentData = $this->adjustmentData->select('date_adjusted', 'adjustment_amount')->get();
        }
    }

    public function export()
    {
        $this->applyExtraSettings();

        if ($this->adjustmentData == null || count($this->adjustmentData) == 0) {
            return $this->redirectOnEmpty($this->consumable->name.' Inventory Adjustments');
        }


        $this->setGetAllListData(true);
        $csvHeaders = ['Adjustment Date', 'Adjustment Amount'];
        $this->csvWriter->insertOne($csvHeaders);

        $csvData = [];

        foreach ($this->adjustmentData as $data) {
            $csvData[] = [$data->date_adjusted, $data->adjustment_amount];
        }

        $this->csvWriter->insertAll($csvData);

        $reader = Reader::createFromPath($this->getPath());
        $this->sendHeaders(Str::ascii($this->consumable->name).' '.$this->getFileName());
        $reader->output();
    }

    public function getBaseName()
    {
        return 'Inventory Adjustments';
    }
}
