<?php namespace ParadoxOne\NDCounties\Database\Exporters\CSVExporters;

use Illuminate\Support\Str;
use League\Csv\Reader;
use ParadoxOne\NDCounties\Contracts\Data\DataExporterInterface;
use ParadoxOne\NDCounties\Contracts\Finance\Invoices\InvoiceReaderRepositoryInterface;
use ParadoxOne\NDCounties\Contracts\Finance\TransactionRepositoryInterface;
use ParadoxOne\NDCounties\Contracts\Finance\TransactionStatusRepositoryInterface;

class InvoicePaymentsExporter extends AbstractCSVExporter implements DataExporterInterface
{
    protected $transactions;
    protected $invoiceID;
    protected $transactionData = null;
    protected $invoice         = null;
    protected $invoices;
    protected $transactionStatuses;

    public function __construct(TransactionRepositoryInterface $transactions, InvoiceReaderRepositoryInterface $invoices, TransactionStatusRepositoryInterface $statuses)
    {
        $this->transactions        = $transactions;
        $this->invoices            = $invoices;
        $this->transactionStatuses = $statuses;
    }

    private function applyExtraSettings()
    {
        if ($this->rawOptions->has('invoice')) {
            $this->invoiceID       = $this->rawOptions->get('invoice');
            $this->invoice         = $this->invoices->getInvoiceByID($this->invoiceID);
            $this->transactionData =
                $this->transactions->getInvoiceTransactions($this->invoiceID, $this->invoice->paid_to_account);

            if (count($this->restrictedRecords) > 0) {
                $this->transactionData = $this->transactionData->whereIn('id', $this->restrictedRecords);
            }
            $this->transactionData = $this->transactionData->get();
        }
    }

    public function export()
    {
        $this->applyExtraSettings();

        if ($this->transactionData == null || count($this->transactionData) == 0) {
            $this->redirectOnEmpty($this->invoice->invoice_number . ' Invoice Payments');
        }

        $csvHeaders = ['Batch', 'Transaction Date', 'Description', 'From', 'Paid To', 'Status', 'Amount'];

        $this->csvWriter->insertOne($csvHeaders);

        $exportData = [];

        $statuses = $this->transactionStatuses->getStatuses();

        foreach ($this->transactionData as $data) {
            $exportData[] = [
                $data->batch,
                $data->created_at,
                $data->description,
                $data->beginAccount->code . ' - ' . $data->beginAccount->name,
                $data->destination->code . ' - ' . $data->destination->name,
                $statuses[$data->transaction_status],
                $data->amount
            ];
        }

        $this->csvWriter->insertAll($exportData);

        $reader = Reader::createFromPath($this->getPath());
        $this->sendHeaders(Str::ascii($this->invoice->invoice_number) . ' ' . $this->getFileName());
        $reader->output();
    }

    public function getBaseName()
    {
        return 'Invoice Payments';
    }
}
