<?php namespace ParadoxOne\NDCounties\Database\Exporters\CSVExporters;

use League\Csv\Reader;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use ParadoxOne\NDCounties\Contracts\Data\DataExporterInterface;
use ParadoxOne\NDCounties\Contracts\WorkEntries\WorkEntryReaderInterface;
use ParadoxOne\NDCounties\UserSessionManager;

class WorkRecordsExporter extends AbstractCSVExporter implements DataExporterInterface
{
    protected $workEntryReader;

    public function __construct(WorkEntryReaderInterface $workReader)
    {
        $this->workEntryReader    = $workReader;
    }

    public function getBaseName()
    {
        return 'Work Records';
    }

    private function buildConsumableHeaderArray($count, $text = 'Fuel')
    {
        $headers = [];

        for ($i = 1; $i <= $count; $i++) {
            $headers[] = $text.' '.$i;
            $headers[] = $text.' Quantity '.$i;
            $headers[] = $text.' Unit '.$i;
        }
        return $headers;
    }

    public function setRecordRestrictions($restrictions)
    {
        //lk('taco bell');
        //$this->restrictedRecords = Cache::get(Auth::user()->last_tenant.Auth::user()->id.'_work_record_search');
    }


    public function export()
    {
        $formatter              = App::make('formatter');


        $this->setGetAllListData(true);

        $this->workEntryReader->reader()->withDefaults();

        $this->restrictedRecords = Cache::get(Auth::user()->last_tenant.Auth::user()->id.'_work_record_search');

        if (count($this->restrictedRecords) > 0) {
            $this->workEntryReader->reader()->whereWorkEntriesID($this->restrictedRecords);
        } else {
            $this->workEntryReader->reader()->setDateRange($this->startDate, $this->endDate);
        }

        $workRecords = $this->workEntryReader->getRecords();

        if (count($workRecords) == 0) {
            return $this->redirectOnEmpty('Work Records');
        }

        $metaInformation = new \stdClass;
        $metaInformation->equipmentCount = 0;
        $metaInformation->materialCount = $this->workEntryReader->reader()->getHighestMaterialCount();
        $metaInformation->fuelCount = $this->workEntryReader->reader()->getHighestFuelCount();

        $csvHeaders = ['Record ID', 'Date', 'Employee', 'Hours', 'Cost', 'Project', 'Activity', 'Department', 'Road', 'Property', 'Equipment'];

        // Build the extra fuel columns.
        if (count($metaInformation->fuelCount) > 0) {
            $csvHeaders = array_merge($csvHeaders, $this->buildConsumableHeaderArray($metaInformation->fuelCount));
        }

        // Build the extra material columns.
        if (count($metaInformation->materialCount) > 0) {
            $csvHeaders = array_merge($csvHeaders, $this->buildConsumableHeaderArray($metaInformation->materialCount, 'Material'));
        }

        $csvHeaders = array_flatten($csvHeaders);

        $exportRecords = [];


        foreach ($workRecords as $record) {
            $batchedEquipmentRecords = [];

            $exportRecord = [];
            $exportRecord[] = $record->getWorkID();
            $exportRecord[] = $record->getWorkDate();
            $exportRecord[] = $formatter->employee($record->getEmployee());
            $exportRecord[] = $record->getRecordTotals()->employeeTotalHours;
            $exportRecord[] = $record->getRecordTotals()->employeeCombined;
            $exportRecord[] = $formatter->project($record->getProject());
            $exportRecord[] = $formatter->activity($record->getActivity());
            $exportRecord[] = $formatter->department($record->getDepartment());
            $exportRecord[] = $formatter->property($record->getOrganizationProperty());
            $exportRecord[] = $formatter->property($record->getProperty());


            if (count($record->getEquipmentUnits()) > 0) {
                $equipmentRecords = $record->getEquipmentUnits();

                // Let's take the first one now.
                $initialRecord = $equipmentRecords[0];
               // $exportRecord[] = $initialRecord->equipment->code.' - '.$initialRecord->equipment->name;
                $exportRecord[] = $formatter->property($initialRecord->equipment);
                $initialRecordFuelDifference = $metaInformation->fuelCount - count($initialRecord->fuels);

                foreach ($initialRecord->fuels as $fuel) {
                    // $exportRecord[] = $fuel->fuel->code.' - '.$fuel->fuel->name;
                    $exportRecord[] = $formatter->fuel($fuel->fuel);
                    $exportRecord[] = $fuel->total_quantity;
                    $exportRecord[] = $fuel->fuel->unit->name;
                }

                if ($initialRecordFuelDifference > 0) {
                    $exportRecord = array_merge($exportRecord, array_fill(0, $initialRecordFuelDifference * 3, ''));
                }

                unset($equipmentRecords[0]);

                // Now we need to build the "extra" equipment records.
                if (count($equipmentRecords) > 0) {
                    foreach ($equipmentRecords as $extraEquipmentRecord) {
                        $newEquipmentRecord = [];
                        $newEquipmentRecord[] = $record->getWorkID();
                        // $newEquipmentRecord = array_merge($newEquipmentRecord, array_fill(0, 9, ''));
                        $newEquipmentRecord[] = $record->getWorkDate();
                        $newEquipmentRecord[] = $record->getEmployee()->code.' - '.$record->getEmployee()->firstName.' '.$record->getEmployee()->lastName;
                        $newEquipmentRecord[] = 0;
                        $newEquipmentRecord[] = 0;
                        $newEquipmentRecord[] = $record->getProject()->code.' - '.$record->getProject()->name;
                        $newEquipmentRecord[] = $record->getActivity()->code.' - '.$record->getActivity()->name;
                        $newEquipmentRecord[] = $record->getDepartment()->code.' - '.$record->getDepartment()->name;
                        $newEquipmentRecord[] = $record->getOrganizationProperty()->code.' - '.$record->getOrganizationProperty()->name;
                        $newEquipmentRecord[] = $record->getProperty()->code.' - '.$record->getProperty()->name;
                        // $newEquipmentRecord[] = $extraEquipmentRecord->equipment->code.' - '.$extraEquipmentRecord->equipment->name;
                        $newEquipmentRecord[] = $formatter->property($extraEquipmentRecord->equipment);
                        $newEquipmentRecordFuelDifference = $metaInformation->fuelCount - count($extraEquipmentRecord->fuels);

                        foreach ($extraEquipmentRecord->fuels as $fuel) {
                           // $newEquipmentRecord[] = $fuel->fuel->code.' - '.$fuel->fuel->name;
                            $newEquipmentRecord[] = $formatter->fuel($fuel->fuel);
                            $newEquipmentRecord[] = $fuel->total_quantity;
                            $newEquipmentRecord[] = $fuel->fuel->unit->name;
                        }

                        if ($newEquipmentRecordFuelDifference > 0) {
                            $newEquipmentRecord = array_merge($newEquipmentRecord, array_fill(0, $newEquipmentRecordFuelDifference * 3, null));
                        }

                        $materialFillValue = $metaInformation->materialCount * 3;

                        if ($materialFillValue > 0) {
                            $newEquipmentRecord = array_merge($newEquipmentRecord, array_fill(0, $materialFillValue, null));
                        }

                        $batchedEquipmentRecords[] = $newEquipmentRecord;
                    }
                }
            } else {
                // Need the +1 because of the equipment field.
                $fuelFillValue = ($metaInformation->fuelCount * 3) + 1;

                if ($fuelFillValue > 0) {
                    $exportRecord = array_merge($exportRecord, array_fill(0, $fuelFillValue, null));
                }
            }

            // Now we need to handle the materials.
            if (count($record->getMaterials()) > 0) {
                $recordMaterials = $record->getMaterials();
                $materialRecordDifference = $metaInformation->materialCount - count($record->getMaterials());

                foreach ($recordMaterials as $material) {
                   // $exportRecord[] = $material->material->code.' - '.$material->material->name;
                    $exportRecord[] = $formatter->material($material->material);
                    $exportRecord[] = $material->total_quantity;
                    $exportRecord[] = $material->material->unit->name;
                }

                if ($materialRecordDifference > 0) {
                    $exportRecord = array_merge($exportRecord, array_fill(0, $materialRecordDifference * 3, ''));
                }
            } else {
                $materialFillValue = $metaInformation->materialCount * 3;

                if ($materialFillValue > 0) {
                    $exportRecord = array_merge($exportRecord, array_fill(0, $materialFillValue, ''));
                }
            }

            $exportRecords[] = $exportRecord;
            foreach ($batchedEquipmentRecords as $batchedRecord) {
                $exportRecords[] = $batchedRecord;
            }
        }

        $this->csvWriter->insertOne($csvHeaders);
        $this->csvWriter->insertAll($exportRecords);

        $reader = Reader::createFromPath($this->getPath());
        $this->sendHeaders($this->getFileName());
        $reader->output();
    }
}
