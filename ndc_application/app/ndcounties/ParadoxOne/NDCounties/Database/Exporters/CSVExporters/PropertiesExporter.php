<?php namespace ParadoxOne\NDCounties\Database\Exporters\CSVExporters;

use League\Csv\Reader;
use ParadoxOne\NDCounties\Contracts\Data\DataExporterInterface;
use ParadoxOne\NDCounties\Contracts\Properties\PropertiesRepositoryInterface;
use ParadoxOne\NDCounties\Database\Adapters\AdapterManager;

class PropertiesExporter extends AbstractCSVExporter implements DataExporterInterface
{
    /**
     * The AdapterManager implementation.
     *
     * @var AdapterManager
     */
    protected $adapterManager;

    /**
     * The PropertiesRepositoryInterface implementation.
     *
     * @var PropertiesRepositoryInterface
     */
    protected $properties;

    /**
     * A collection of 'found' properties.
     *
     * @var array
     */
    protected $foundProperties = [];

    /**
     * Adapter headers.
     *
     * @var array
     */
    protected $adapterHeaders = [];

    /**
     * A list of all adapters in use.
     *
     * @var array
     */
    protected $adaptersInUse = [];

    /**
     * The properties aggregate data.
     *
     * @var null|array
     */
    protected $aggregatedData = null;

    protected $aggregateColumnCount = 0;

    protected $aggregateEmptyArray = [];

    /**
     * The list of records to export.
     *
     * @var array
     */
    protected $exportRecords = [];

    public function __construct(AdapterManager $adapterManager, PropertiesRepositoryInterface $properties)
    {
        $this->adapterManager = $adapterManager;
        $this->properties     = $properties;
    }

    /**
     * Returns an array of CSV headings for export.
     *
     */
    protected function buildExportHeadings()
    {
        foreach ($this->adapterManager->getAdapters() as $adapter) {
            $this->adapterHeaders[$adapter->getAdapterNumericIdentifier()] = [
                count($adapter->getExportHeaders()),
                $adapter->getExportHeaders()
            ];
        }
    }

    protected function getExportHeadingArray()
    {
        $baseHeadings = ['Code', 'Name', 'Description'];

        foreach ($this->adapterHeaders as $adapterIdentifier => $heading) {
            if (in_array($adapterIdentifier, $this->adaptersInUse)) {
                $baseHeadings = array_merge($baseHeadings, $heading[1]);
            }
        }

        $baseHeadings = array_merge($baseHeadings, $this->getAggregateHeaders());

        return $baseHeadings;
    }

    protected function buildPropertyAdaptersInUseList()
    {
        foreach ($this->foundProperties as $properties) {
            foreach ($properties->type->adapters as $adapter) {
                $this->adaptersInUse[] = $adapter->adapter;
            }
        }

        // We just want the numeric identifier, nothing else.
        $this->adaptersInUse = array_values(array_unique($this->adaptersInUse));
    }

    /**
     * Determines if the given property is using the adapter, based on a numeric identifier.
     *
     * @param $property
     * @param $adapterNumericIdentifier
     * @return bool
     */
    protected function propertyUsingPropertyAdapter(&$property, $adapterNumericIdentifier)
    {
        return ($property->{'relationship_'.$adapterNumericIdentifier} !== null);
    }

    protected function buildAggregateData()
    {
        if (count($this->foundProperties) > 0) {
            if ($this->allListData == false) {
                if (count($this->selects) > 0) {
                    $this->selects[] = 'property_id';
                    $this->makeWorkEntryBuilder();
                    $this->aggregatedData = $this->workEntryBuilder->groupBy('property_id')->whereIn('work_entries.property_id', array_pluck($this->foundProperties, 'id'))->get();

                    $this->aggregateColumnCount = count($this->getAggregateHeaders());

                    if ($this->aggregateColumnCount > 0) {
                        $this->aggregateEmptyArray = array_fill(0, $this->aggregateColumnCount, '0.000000');
                    }
                }
            }
        }
    }

    /**
     * Builds the initial export list.
     */
    protected function buildInitialPropertyExport()
    {
        foreach ($this->foundProperties as $property) {
            $propertyToExport = [];
            $propertyToExport[] = $property->code;
            $propertyToExport[] = $property->name;
            $propertyToExport[] = $property->description;

            foreach ($this->adaptersInUse as $adapter) {
                if ($this->propertyUsingPropertyAdapter($property, $adapter)) {
                    // The property uses the adapter, so we should get the information from the adapter relationship
                    // and add it to the export.
                    $adapterData = $this->adapterManager->getExportData($adapter, $property);
                    $propertyToExport = array_merge($propertyToExport, $adapterData);
                } else {
                    // The property does not use the adapter, so we need to pad the export with empty values.
                    $propertyToExport = array_merge($propertyToExport, array_fill(0, $this->adapterHeaders[$adapter][0], null));
                }
            }

            // Now we need to build up the aggregate data.
            if ($this->aggregatedData != null) {
                $data = array_pluck_where($this->aggregatedData, 'property_id', $property->id);

                if (count($data) == 0 || $data == null) {
                    $data = $this->aggregateEmptyArray;
                } else {
                    $data = (array)$data[0];
                    unset($data['property_id']);
                    $data = array_values($data);
                }

                $propertyToExport = array_merge($propertyToExport, $data);
            }

            $this->exportRecords[] = $propertyToExport;
        }
    }

    public function export()
    {
        if (count($this->restrictedRecords) > 0) {
            $this->foundProperties = $this->properties->getPropertiesIn($this->restrictedRecords);
        } else {
            $this->foundProperties = $this->properties->getProperties()->get();
        }

        if (count($this->foundProperties) == 0) {
            return $this->redirectOnEmpty('Properties');
        }

        $this->buildExportHeadings();
        $this->buildPropertyAdaptersInUseList();
        $this->buildAggregateData();
        $this->buildInitialPropertyExport();

        $this->csvWriter->insertOne($this->getExportHeadingArray());

        $this->csvWriter->insertAll($this->exportRecords);

        $reader = Reader::createFromPath($this->getPath());
        $this->sendHeaders($this->getFileName());
        $reader->output();
    }

    public function getBaseName()
    {
        return 'Properties';
    }
}
