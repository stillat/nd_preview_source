<?php namespace ParadoxOne\NDCounties\Database\Exporters\CSVExporters;

use League\Csv\Reader;
use ParadoxOne\NDCounties\Contracts\Data\DataExporterInterface;
use ParadoxOne\NDCounties\Contracts\DepartmentsRepositoryInterface;

class DepartmentExporter extends AbstractCSVExporter implements DataExporterInterface
{
    protected $departmentRepository;


    public function __construct(DepartmentsRepositoryInterface $departmentRepository)
    {
        $this->departmentRepository = $departmentRepository;
    }

    public function getBaseName()
    {
        return 'Departments';
    }


    private function getDepartmentHeaders()
    {
        return array_merge(['Code', 'Name', 'Description'], $this->getAggregateHeaders());
    }

    public function export()
    {
        $this->csvWriter->insertOne($this->getDepartmentHeaders());

        $departments = $this->departmentRepository->getDepartments()->select('code', 'name', 'description', 'id');

        if (count($this->restrictedRecords) > 0) {
            $departments = $departments->whereIn('id', $this->restrictedRecords);
        }

        $departments = $departments->get();

        if (count($departments) == 0) {
            return $this->redirectOnEmpty('Departments');
        }

        $exportDepartments = $this->formatDataForExport($departments, ['code', 'name', 'description'], 'id', 'department_id');

        $this->csvWriter->insertAll($exportDepartments);

        $reader = Reader::createFromPath($this->getPath());
        $this->sendHeaders($this->getFileName());
        $reader->output();
    }
}
