<?php namespace ParadoxOne\NDCounties\Database\Exporters\CSVExporters;

use League\Csv\Reader;
use ParadoxOne\NDCounties\Contracts\Data\DataExporterInterface;
use ParadoxOne\NDCounties\Contracts\DistrictsRepositoryInterface;

class DistrictsExporter extends AbstractCSVExporter implements DataExporterInterface
{
    protected $districtsRepository;


    public function __construct(DistrictsRepositoryInterface $districtsRepository)
    {
        $this->districtsRepository = $districtsRepository;
    }

    public function getBaseName()
    {
        return 'Districts';
    }


    private function getDistrictsHeader()
    {
        return array_merge(['Code', 'Name', 'Description'], $this->getAggregateHeaders());
    }

    public function export()
    {
        $this->csvWriter->insertOne($this->getDistrictsHeader());

        $districts = $this->districtsRepository->getDistricts()->select('code', 'name', 'description', 'id');

        if (count($this->restrictedRecords) > 0) {
            $districts = $districts->whereIn('id', $this->restrictedRecords);
        }

        $districts = $districts->get();

        if (count($districts) == 0) {
            return $this->redirectOnEmpty('Districts');
        }

        $exportDistricts = $this->formatDataForExport($districts, ['code', 'name', 'description'], 'id', 'district_id');

        $this->csvWriter->insertAll($exportDistricts);

        $reader = Reader::createFromPath($this->getPath());
        $this->sendHeaders($this->getFileName());
        $reader->output();
    }
}
