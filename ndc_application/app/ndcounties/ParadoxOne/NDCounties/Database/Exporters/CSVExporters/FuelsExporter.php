<?php namespace ParadoxOne\NDCounties\Database\Exporters\CSVExporters;

use League\Csv\Reader;
use ParadoxOne\NDCounties\Contracts\Data\DataExporterInterface;
use ParadoxOne\NDCounties\Contracts\Resources\FuelRepositoryInterface;

class FuelsExporter extends AbstractCSVExporter implements DataExporterInterface
{
    protected $fuelRepository;


    public function __construct(FuelRepositoryInterface $fuelRepository)
    {
        $this->fuelRepository = $fuelRepository;
    }

    public function getBaseName()
    {
        return 'Fuels';
    }


    private function getFuelHeaders()
    {
        return ['Code', 'Name', 'Description', 'Unit Cost', 'Tracking Inventory', 'Desired Inventory Level', 'Critical Inventory Level', 'Current Inventory Level'];
    }

    public function export()
    {
        $this->csvWriter->insertOne($this->getFuelHeaders());

        $fuels = $this->fuelRepository->getFuels()->select('code', 'name', 'description', 'cost', 'tracking_inventory', 'desired_inventory_level', 'critical_inventory_level', 'current_inventory_level');

        if (count($this->restrictedRecords) > 0) {
            $fuels = $fuels->whereIn('id', $this->restrictedRecords);
        }

        $fuels = $fuels->get();

        if (count($fuels) == 0) {
            return $this->redirectOnEmpty('Fuels');
        }

        // Fuels cannot take advantage of aggregate data.
        $this->setGetAllListData(true);

        $exportFuels = $this->formatDataForExport($fuels, ['code', 'name', 'description', 'cost', 'tracking_inventory', 'desired_inventory_level', 'critical_inventory_level', 'current_inventory_level'], '', '');

        $this->csvWriter->insertAll($exportFuels);

        $reader = Reader::createFromPath($this->getPath());
        $this->sendHeaders($this->getFileName());
        $reader->output();
    }
}
