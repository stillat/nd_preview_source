<?php namespace ParadoxOne\NDCounties\Database\Exporters\CSVExporters;

use League\Csv\Reader;
use ParadoxOne\NDCounties\Contracts\Data\DataExporterInterface;
use ParadoxOne\NDCounties\Contracts\Finance\Taxes\TaxRateRepositoryInterface;

class TaxRatesExporter extends AbstractCSVExporter implements DataExporterInterface
{
    protected $taxRatesRepository;

    public function __construct(TaxRateRepositoryInterface $taxRateRepository)
    {
        $this->taxRatesRepository = $taxRateRepository;
    }

    public function getBaseName()
    {
        return 'Tax Rates';
    }

    public function getTaxRateHeaders()
    {
        return ['Name', 'Rate', 'Employees', 'Equipments', 'Fuels', 'Materials'];
    }

    public function export()
    {
        $this->csvWriter->insertOne($this->getTaxRateHeaders());

        $taxRates = $this->taxRatesRepository->getTaxRates();

        if (count($this->restrictedRecords) > 0) {
            $taxRates = $taxRates->whereIn('id', $this->restrictedRecords);
        }

        $taxRates = $taxRates->get();

        if (count($taxRates) == 0) {
            return $this->redirectOnEmpty('Tax Rates');
        }

        $this->setGetAllListData(true);
        $exportRates = $this->formatDataForExport($taxRates, ['tax_name', 'tax_rate', 'affects_employees', 'affects_equipments', 'affects_fuels', 'affects_materials'], '', '');

        $this->csvWriter->insertAll($exportRates);

        $reader = Reader::createFromPath($this->getPath());
        $this->sendHeaders($this->getFileName());
        $reader->output();
    }
}
