<?php namespace ParadoxOne\NDCounties\Database\Exporters\CSVExporters;

use UI;
use League\Csv\Reader;
use ParadoxOne\NDCounties\Contracts\ActivityRepositoryInterface;
use ParadoxOne\NDCounties\Contracts\Data\DataExporterInterface;

class ActivityExporter extends AbstractCSVExporter implements DataExporterInterface
{
    protected $activityRepository;

    public function __construct(ActivityRepositoryInterface $activityRepository)
    {
        $this->activityRepository = $activityRepository;
    }

    public function getBaseName()
    {
        return 'Activities';
    }

    private function getActivityHeaders()
    {
        return array_merge(['Code', 'Name', 'Description'], $this->getAggregateHeaders());
    }

    public function export()
    {
        $this->csvWriter->insertOne($this->getActivityHeaders());

        $activities = $this->activityRepository->getAll()->select('code', 'name', 'description', 'id');

        if (count($this->restrictedRecords) > 0) {
            $activities = $activities->whereIn('id', $this->restrictedRecords);
        }

        $activities = $activities->get();

        if (count($activities) == 0) {
            return $this->redirectOnEmpty('Activities');
        }

        $exportActivities = $this->formatDataForExport($activities, ['code', 'name', 'description'], 'id', 'activity_id');

        $this->csvWriter->insertAll($exportActivities);

        $reader = Reader::createFromPath($this->getPath());
        $this->sendHeaders($this->getFileName());
        $reader->output();
    }
}
