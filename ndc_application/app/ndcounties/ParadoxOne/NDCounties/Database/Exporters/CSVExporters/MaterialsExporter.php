<?php namespace ParadoxOne\NDCounties\Database\Exporters\CSVExporters;

use League\Csv\Reader;
use ParadoxOne\NDCounties\Contracts\Data\DataExporterInterface;
use ParadoxOne\NDCounties\Contracts\Resources\MaterialRepositoryInterface;

class MaterialsExporter extends AbstractCSVExporter implements DataExporterInterface
{
    protected $materialRepository;


    public function __construct(MaterialRepositoryInterface $materialRepository)
    {
        $this->materialRepository = $materialRepository;
    }

    public function getBaseName()
    {
        return 'Materials';
    }


    private function getMaterialsHeaders()
    {
        return ['Code', 'Name', 'Description', 'Unit Cost', 'Tracking Inventory', 'Desired Inventory Level', 'Critical Inventory Level', 'Current Inventory Level'];
    }

    public function export()
    {
        $this->csvWriter->insertOne($this->getMaterialsHeaders());

        $materials = $this->materialRepository->getMaterials()->select('code', 'name', 'description', 'cost', 'tracking_inventory', 'desired_inventory_level', 'critical_inventory_level', 'current_inventory_level');

        if (count($this->restrictedRecords) > 0) {
            $materials = $materials->whereIn('id', $this->restrictedRecords);
        }

        $materials = $materials->get();

        if (count($materials) == 0) {
            return $this->redirectOnEmpty('Materials');
        }

        $this->setGetAllListData(true);
        $exportMaterials = $this->formatDataForExport($materials, ['code', 'name', 'description', 'cost', 'tracking_inventory', 'desired_inventory_level', 'critical_inventory_level', 'current_inventory_level'], '', '');

        $this->csvWriter->insertAll($exportMaterials);

        $reader = Reader::createFromPath($this->getPath());
        $this->sendHeaders($this->getFileName());
        $reader->output();
    }
}
