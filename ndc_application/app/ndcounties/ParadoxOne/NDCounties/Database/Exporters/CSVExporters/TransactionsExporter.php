<?php namespace ParadoxOne\NDCounties\Database\Exporters\CSVExporters;

use Illuminate\Support\Str;
use League\Csv\Reader;
use ParadoxOne\NDCounties\Contracts\Data\DataExporterInterface;
use ParadoxOne\NDCounties\Contracts\Finance\AccountRepositoryInterface;
use ParadoxOne\NDCounties\Contracts\Finance\TransactionRepositoryInterface;
use ParadoxOne\NDCounties\Contracts\Finance\TransactionStatusRepositoryInterface;

class TransactionsExporter extends AbstractCSVExporter implements DataExporterInterface
{
    protected $transactions;
    protected $accounts;
    protected $transactionStatuses;
    protected $accountID = null;

    protected $transactionData = null;

    protected $account;

    public function __construct(TransactionRepositoryInterface $transactions, AccountRepositoryInterface $accounts, TransactionStatusRepositoryInterface $statuses)
    {
        $this->transactions = $transactions;
        $this->accounts = $accounts;
        $this->transactionStatuses = $statuses;
    }

    private function applyExtraSettings()
    {
        if ($this->rawOptions->has('account')) {
            $this->accountID = $this->rawOptions->get('account');
            $this->account = $this->accounts->getModelByID($this->accountID);
            $this->transactionData = $this->transactions->getAccountTransactions($this->accountID);

            if (count($this->restrictedRecords) > 0) {
                $this->transactionData = $this->transactionData->whereIn('id', $this->restrictedRecords);
            }
            $this->transactionData = $this->transactionData->get();
        }
    }

    public function export()
    {
        $this->applyExtraSettings();

        if ($this->transactionData == null || count($this->transactionData) == 0) {
            $this->redirectOnEmpty($this->account->name.' Account Transactions');
        }

        $csvHeaders = ['Batch', 'Transaction Date', 'Description', 'From', 'Paid To', 'Status', 'Amount'];

        $this->csvWriter->insertOne($csvHeaders);

        $exportData = [];

        $statuses = $this->transactionStatuses->getStatuses();

        foreach ($this->transactionData as $data) {
            $exportData[] = [
              $data->batch,
              $data->created_at,
              $data->description,
              $data->beginAccount->code.' - '.$data->beginAccount->name,
              $data->destination->code.' - '.$data->destination->name,
              $statuses[$data->transaction_status],
              $data->amount
            ];
        }

        $this->csvWriter->insertAll($exportData);

        $reader = Reader::createFromPath($this->getPath());
        $this->sendHeaders(Str::ascii($this->account->name).' '.$this->getFileName());
        $reader->output();
    }

    public function getBaseName()
    {
        return 'Account Transactions';
    }
}
