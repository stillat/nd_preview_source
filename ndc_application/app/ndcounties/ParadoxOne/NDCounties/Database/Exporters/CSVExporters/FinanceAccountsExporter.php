<?php namespace ParadoxOne\NDCounties\Database\Exporters\CSVExporters;

use Illuminate\Support\Facades\DB;
use League\Csv\Reader;
use ParadoxOne\NDCounties\Contracts\Data\DataExporterInterface;
use ParadoxOne\NDCounties\Contracts\Finance\AccountRepositoryInterface;

class FinanceAccountsExporter extends AbstractCSVExporter implements DataExporterInterface
{
    protected $financeAccountRepository;

    public function __construct(AccountRepositoryInterface $financeAccountRepository)
    {
        $this->financeAccountRepository = $financeAccountRepository;
    }

    public function getBaseName()
    {
        return 'Finance Accounts';
    }

    private function getFinanceAccountHeaders()
    {
        return ['Category', 'Code/Account#', 'Name', 'Description', 'Balance', 'Cash Account', 'Address Line 1', 'Address Line 2', 'City', 'State', 'Country', 'ZIP'];
    }

    public function export()
    {
        $this->csvWriter->insertOne($this->getFinanceAccountHeaders());
        $accounts = $this->financeAccountRepository->getAccounts()->select(DB::raw('fs_accounts.*, fs_accounts_categories.code as prefix'));

        if (count($this->restrictedRecords) > 0) {
            $accounts = $accounts->whereIn('fs_accounts.id', $this->restrictedRecords);
        }

        $accounts = $accounts->get();

        if (count($accounts) == 0) {
            return $this->redirectOnEmpty('Finance Accounts');
        }

        $this->setGetAllListData(true);
        $exportAccounts = $this->formatDataForExport($accounts, ['prefix', 'code', 'name', 'description', 'balance', 'is_cash_account', 'address_line_1', 'address_line_2', 'city', 'state', 'zip', 'country'], '', '');

        $this->csvWriter->insertAll($exportAccounts);

        $reader = Reader::createFromPath($this->getPath());
        $this->sendHeaders($this->getFileName());
        $reader->output();
    }
}
