<?php namespace ParadoxOne\NDCounties\Database\Exporters\CSVExporters;

use League\Csv\Reader;
use ParadoxOne\NDCounties\Contracts\Data\DataExporterInterface;
use ParadoxOne\NDCounties\Contracts\Finance\Invoices\InvoiceReaderRepositoryInterface;

class InvoicesExporter extends AbstractCSVExporter implements DataExporterInterface
{
    protected $invoiceRepository;

    public function __construct(InvoiceReaderRepositoryInterface $invoiceRepository)
    {
        $this->invoiceRepository = $invoiceRepository;
    }

    public function getBaseName()
    {
        return 'Invoices';
    }

    private function getInvoiceheaders()
    {
        return ['Invoice Number', 'Bill To', 'Pay To', 'Due Date', 'Total Discount', 'Total Tax', 'Invoice Total', 'Balance'];
    }

    public function export()
    {
        $this->csvWriter->insertOne($this->getInvoiceheaders());
        $invoices = $this->invoiceRepository->getInvoices();

        if (count($this->restrictedRecords) > 0) {
            $invoices = $invoices->whereIn('fs_invoices.id', $this->restrictedRecords);
        }

        $invoices = $invoices->get();

        if (count($invoices) == 0) {
            return $this->redirectOnEmpty('Invoices');
        }

        $this->setGetAllListData(true);
        $exportInvoices = $this->formatDataForExport($invoices, [
            'invoice_number', 'billedToAccount', 'paidToAccount', 'due_date', 'invoice_discount_total', 'invoice_tax_amount', 'invoice_total', 'cached_invoice_balance'
        ], '', '');

        $this->csvWriter->insertAll($exportInvoices);
        $reader = Reader::createFromPath($this->getPath());
        $this->sendHeaders($this->getFileName());
        $reader->output();
    }
}
