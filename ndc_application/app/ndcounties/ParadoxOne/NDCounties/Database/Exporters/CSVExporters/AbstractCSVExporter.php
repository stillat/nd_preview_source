<?php namespace ParadoxOne\NDCounties\Database\Exporters\CSVExporters;

use UI;
use Illuminate\Support\Facades\Redirect;
use League\Csv\Writer;
use Illuminate\Support\Facades\DB;
use Stillat\Database\Support\Facades\Tenant;
use ParadoxOne\NDCounties\Contracts\Data\DataExporterInterface;

abstract class AbstractCSVExporter implements DataExporterInterface
{
    protected $endDate;
    protected $startDate;
    protected $employeeRegularHours;
    protected $employeeRegularCost;
    protected $employeeRegularBenefits;
    protected $allListData;
    protected $employeeOvertimeHours;
    protected $employeeOvertimeCost;
    protected $employeeOvertimeBenefit;
    protected $equipmentQuantity;
    protected $equipmentCost;
    protected $materialQuantity;
    protected $materialCost;
    protected $fuelQuantity;
    protected $propertyCost;
    protected $fuelCost;
    protected $billingQuantity;
    protected $billingCost;
    protected $csvPath;
    protected $csvWriter;
    protected $fileName;

    protected $rawOptions;

    protected $workEntryBuilder = null;

    protected $selects = [];
    protected $restrictedRecords;

    protected function getTable($table)
    {
        return DB::connection(Tenant::getCurrentConnection())->table($table);
    }

    public function setRawOptions($options)
    {
        $this->rawOptions = $options;
    }

    public function getRawOptions()
    {
        return $this->rawOptions;
    }


    public function setEndDate($endDate)
    {
        $this->endDate = $endDate;
    }

    public function getEndDate()
    {
        return $this->endDate;
    }

    public function setStartDate($startDate)
    {
        $this->startDate = $startDate;
    }

    public function getStartDate()
    {
        return $this->startDate;
    }

    protected function makeWorkEntryBuilder()
    {
        if ($this->workEntryBuilder == null) {
            $this->workEntryBuilder = $this->getTable('work_entries')->select($this->selects)->whereBetween('work_date', [$this->startDate, $this->endDate]);
        }
    }

    public function setAggregateEmployeeRegularHours($value)
    {
        $this->employeeRegularHours = $value;

        if ($value) {
            $this->selects[] = DB::raw('COALESCE(SUM(employee_hours_regular_worked), 0) as `EmployeeRegularHours`');
        }
    }

    public function setAggregateEmployeeRegularCost($value)
    {
        $this->employeeRegularCost = $value;

        if ($value) {
            $this->selects[] = DB::raw('COALESCE(SUM(total_employee_regular_cost), 0) as `EmployeeRegularCost`');
        }
    }

    public function setAggregateEmployeeRegularBenefits($value)
    {
        $this->employeeRegularBenefits = $value;

        if ($value) {
            $this->selects[] = DB::raw('ROUND(COALESCE(SUM(employee_hours_regular_worked * employee_historic_fringe_regular), 0), 6) as `EmployeeRegularBenefitsCost`');
        }
    }

    public function setAggregateEmployeeOvertimeHours($value)
    {
        $this->employeeOvertimeHours = $value;

        if ($value) {
            $this->selects[] = DB::raw('COALESCE(SUM(employee_hours_overtime_worked), 0) as `EmployeeOvertimeHours`');
        }
    }

    public function setAggregateEmployeeOvertimeCost($value)
    {
        $this->employeeOvertimeCost = $value;

        if ($value) {
            $this->selects[] = DB::raw('COALESCE(SUM(total_employee_overtime_cost), 0) as `EmployeeOvertimeCost`');
        }
    }

    public function setRecordRestrictions($restrictions)
    {
        $this->restrictedRecords = $restrictions;
    }


    public function setAggregateEmployeeOvertimeBenefits($value)
    {
        $this->employeeOvertimeBenefit = $value;

        if ($value) {
            $this->selects[] = DB::raw('ROUND(COALESCE(SUM(employee_hours_overtime_worked * employee_historic_fringe_overtime), 0), 6) as `EmployeeOvertimeBenefitsCost`');
        }
    }

    public function setAggregateEquipmentQuantity($value)
    {
        $this->equipmentQuantity = $value;

        if ($value) {
            $this->selects[] = DB::raw('COALESCE(SUM(total_equipment_units_quantity), 0) as `EquipmentQuantity`');
        }
    }

    public function setAggregateEquipmentCost($value)
    {
        $this->equipmentCost = $value;

        if ($value) {
            $this->selects[] = DB::raw('COALESCE(SUM(total_equipment_units_cost), 0) as `EquipmentCost`');
        }
    }

    public function setAggregateMaterialQuantity($value)
    {
        $this->materialQuantity = $value;

        if ($value) {
            $this->selects[] = DB::raw('COALESCE(SUM(total_materials_quantity), 0) as `MaterialQuantity`');
        }
    }

    public function setAggregateMaterialCost($value)
    {
        $this->materialCost = $value;

        if ($value) {
            $this->selects[] = DB::raw('COALESCE(SUM(total_materials_cost), 0) as `MaterialCost`');
        }
    }

    public function setAggregateFuelQuantity($value)
    {
        $this->fuelQuantity = $value;

        if ($value) {
            $this->selects[] = DB::raw('COALESCE(SUM(total_fuels_quantity), 0) as `FuelsQuantity`');
        }
    }

    public function setAggregateFuelCost($value)
    {
        $this->fuelCost = $value;

        if ($value) {
            $this->selects[] = DB::raw('COALESCE(SUM(total_fuels_cost), 0) as `FuelsCost`');
        }
    }

    public function setAggregatePropertyCost($value)
    {
        $this->propertyCost = $value;

        if ($value) {
            $this->selects[] = DB::raw('COALESCE(SUM(total_property_cost), 0) as `PropertyCost`');
        }
    }

    public function setAggregateBillingQuantity($value)
    {
        $this->billingQuantity = $value;

        if ($value) {
            $this->selects[] = DB::raw('COALESCE(SUM(misc_billing_quantity), 0) as `BillingQuantity`');
        }
    }

    public function setAggregateBillingCost($value)
    {
        $this->billingCost = $value;

        if ($value) {
            $this->selects[] = DB::raw('COALESCE(SUM(total_property_cost), 0) as `BillingCost`');
        }
    }

    public function setGetAllListData($value)
    {
        $this->allListData = $value;

        $this->selects = [];
    }

    public function setPath($path)
    {
        if (!file_exists($path)) {
            touch($path);
        }

        $this->csvPath = $path;
        $this->makeWriter();
    }

    public function getPath()
    {
        return $this->csvPath;
    }

    public function setFileName($fileName)
    {
        $this->fileName = $fileName;
    }

    public function getFileName()
    {
        return $this->fileName;
    }


    protected function getAggregateHeaders()
    {
        if ($this->allListData) {
            return [];
        }

        $aggregateHeaders = [];

        if ($this->employeeRegularHours) {
            $aggregateHeaders[] = 'Employee Regular Hours';
        }

        if ($this->employeeRegularCost) {
            $aggregateHeaders[] = 'Employee Regular Cost';
        }

        if ($this->employeeRegularBenefits) {
            $aggregateHeaders[] = 'Employee Regular Benefits';
        }

        if ($this->employeeOvertimeHours) {
            $aggregateHeaders[] = 'Employee Overtime Hours';
        }

        if ($this->employeeOvertimeCost) {
            $aggregateHeaders[] = 'Employee Overtime Cost';
        }

        if ($this->employeeOvertimeBenefit) {
            $aggregateHeaders[] = 'Employee Overtime Benefits';
        }

        if ($this->equipmentQuantity) {
            $aggregateHeaders[] = 'Equipment Quantity';
        }

        if ($this->equipmentCost) {
            $aggregateHeaders[] = 'Equipment Cost';
        }

        if ($this->materialQuantity) {
            $aggregateHeaders[] = 'Material Quantity';
        }

        if ($this->materialCost) {
            $aggregateHeaders[] = 'Material Cost';
        }

        if ($this->fuelQuantity) {
            $aggregateHeaders[] = 'Fuel Quantity';
        }

        if ($this->fuelCost) {
            $aggregateHeaders[] = 'Fuel Cost';
        }

        if ($this->propertyCost) {
            $aggregateHeaders[] = 'Property Cost';
        }

        if ($this->billingQuantity) {
            $aggregateHeaders[] = 'Billing Quantity';
        }

        if ($this->billingCost) {
            $aggregateHeaders[] = 'Billing Cost';
        }

        return $aggregateHeaders;
    }

    protected function makeObjectArray(&$array)
    {
        foreach ($array as $key => $value) {
            if (is_object($value)) {
                $array[$key] = (array)$value;
            }
        }
    }

    protected function makeWriter()
    {
        $this->csvWriter = Writer::createFromPath($this->getPath(), 'w');
        $this->csvWriter->autodetectColumnsCount();
        $this->csvWriter->setNullHandlingMode(Writer::NULL_AS_EMPTY);
        $this->csvWriter->setDelimiter(',');
        $this->csvWriter->setEnclosure('"');
        $this->csvWriter->setEscape('\\');
    }

    protected function sendHeaders($fileName)
    {
        header('Content-Type: text/csv; charset=UTF-8');
        header('Content-Disposition: attachment; filename="'.$fileName.'.csv"');
    }

    protected function formatDataForExport($firstPassExportData, array $exportTo, $primaryKeyName, $associatedKey)
    {
        $aggregateData        = null;
        $aggregateColumnCount = count($this->getAggregateHeaders());

        $aggregateEmptyArray = [];

        if ($aggregateColumnCount > 0) {
            $aggregateEmptyArray = array_fill(0, $aggregateColumnCount, '0.000000');
        }

        if (count($firstPassExportData) > 0) {
            if ($this->allListData == false) {
                if (count($this->selects) > 0) {
                    $this->selects[] = $associatedKey;
                    $this->makeWorkEntryBuilder();
                    $aggregateData = $this->workEntryBuilder->groupBy($associatedKey)->whereIn($associatedKey, array_pluck($firstPassExportData, $primaryKeyName))->get();
                }
            }
        }

        $exportData = [];

        foreach ($firstPassExportData as $dataItem) {
            $dataToExport = [];

            foreach ($exportTo as $exportItem) {
                $dataToExport[] = $dataItem->{$exportItem};
            }

            if ($aggregateData != null) {
                $data = array_pluck_where($aggregateData, $associatedKey, $dataItem->{$primaryKeyName});

                if (count($data) == 0 || $data == null) {
                    $data = $aggregateEmptyArray;
                } else {
                    $data = (array)$data[0];
                    unset($data[$associatedKey]);
                    $data = array_values($data);
                }

                $dataToExport = array_merge($dataToExport, $data);
            }

            $exportData[] = $dataToExport;
        }

        return $exportData;
    }

    protected function redirectOnEmpty($title)
    {
        UI::info('There was no data to export.', $title);
        return Redirect::back();
    }
}
