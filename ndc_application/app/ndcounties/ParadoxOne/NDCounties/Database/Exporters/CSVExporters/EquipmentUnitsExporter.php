<?php

namespace ParadoxOne\NDCounties\Database\Exporters\CSVExporters;

use League\Csv\Reader;
use ParadoxOne\NDCounties\Contracts\Data\DataExporterInterface;
use ParadoxOne\NDCounties\Contracts\Properties\PropertiesRepositoryInterface;
use ParadoxOne\NDCounties\Database\Adapters\AdapterManager;

class EquipmentUnitsExporter extends PropertiesExporter
{
    public function export()
    {
        if (count($this->restrictedRecords) > 0) {
            $this->foundProperties = $this->properties->getPropertiesIn($this->restrictedRecords);
        } else {
            $this->foundProperties = $this->properties->getProperties()->where('properties.property_type', '=', 2)->get();
        }

        if (count($this->foundProperties) == 0) {
            return $this->redirectOnEmpty('Properties');
        }

        $this->buildExportHeadings();
        $this->buildPropertyAdaptersInUseList();
        $this->buildAggregateData();
        $this->buildInitialPropertyExport();

        $this->csvWriter->insertOne($this->getExportHeadingArray());

        $this->csvWriter->insertAll($this->exportRecords);

        $reader = Reader::createFromPath($this->getPath());
        $this->sendHeaders($this->getFileName());
        $reader->output();
    }

    public function getBaseName()
    {
        return 'Equipment Units';
    }
}
