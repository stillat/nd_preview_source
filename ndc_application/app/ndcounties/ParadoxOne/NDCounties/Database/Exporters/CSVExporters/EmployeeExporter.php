<?php namespace ParadoxOne\NDCounties\Database\Exporters\CSVExporters;

use League\Csv\Reader;
use ParadoxOne\NDCounties\Contracts\EmployeeRepositoryInterface;
use ParadoxOne\NDCounties\Contracts\Data\DataExporterInterface;

class EmployeeExporter extends AbstractCSVExporter implements DataExporterInterface
{
    protected $employeeRepository;


    public function __construct(EmployeeRepositoryInterface $employeeRepository)
    {
        $this->employeeRepository = $employeeRepository;
    }

    public function getBaseName()
    {
        return 'Employees';
    }


    private function getEmployeeHeaders()
    {
        return array_merge(['Code', 'First Name', 'Middle Name', 'Last Name', 'Hire Date', 'Termination Date', 'Hourly Wage', 'Overtime Wage', 'Hourly Benefits', 'Overtime Benefits'], $this->getAggregateHeaders());
    }

    public function export()
    {
        $this->csvWriter->insertOne($this->getEmployeeHeaders());

        $employees = $this->employeeRepository->getAll()->select('code', 'first_name', 'middle_name', 'last_name', 'hire_date', 'termination_date', 'wage', 'overtime', 'wage_benefit', 'overtime_benefit', 'id');

        if (count($this->restrictedRecords) > 0) {
            $employees = $employees->whereIn('id', $this->restrictedRecords);
        }

        $employees = $employees->get();

        if (count($employees) == 0) {
            return $this->redirectOnEmpty('Employees');
        }

        $exportEmployees = $this->formatDataForExport($employees, [
            'code', 'first_name', 'middle_name', 'last_name', 'hire_date', 'termination_date', 'wage', 'overtime', 'wage_benefit', 'overtime_benefit'
        ], 'id', 'employee_id');

        $this->csvWriter->insertAll($exportEmployees);

        $reader = Reader::createFromPath($this->getPath());
        $this->sendHeaders($this->getFileName());
        $reader->output();
    }
}
