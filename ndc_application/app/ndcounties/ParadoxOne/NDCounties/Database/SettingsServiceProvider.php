<?php namespace ParadoxOne\NDCounties\Database;

use Illuminate\Support\ServiceProvider;

class SettingsServiceProvider extends ServiceProvider
{
    protected $defer = false;

    public function register()
    {
        $this->app->singleton('sorting_work_entry', 'ParadoxOne\NDCounties\Settings\SortingSettingsManager');
        $this->app->singleton('formatter', 'ParadoxOne\NDCounties\Settings\CountyFormattingSettingsManager');
        $this->app->singleton('stateManager', 'ParadoxOne\NDCounties\Reporting\ReportStateManager');
    }
}
