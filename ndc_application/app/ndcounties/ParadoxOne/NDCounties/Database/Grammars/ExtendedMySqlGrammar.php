<?php namespace ParadoxOne\NDCounties\Database\Grammars;

use Illuminate\Database\Query\Builder;
use Illuminate\Database\Query\Grammars\MySqlGrammar;

class ExtendedMySqlGrammar extends MySqlGrammar
{
    protected $calcFoundRows = false;

    protected $useStraightJoin = false;

    protected $forcedIndexes = [];

    protected $forceIndex = false;

    public function setCalcFoundRows($calc = true)
    {
        $this->calcFoundRows = $calc;
        return $this;
    }

    public function straightJoin($join = true)
    {
        $this->useStraightJoin = $join;
        return $this;
    }

    public function forceIndexes($force)
    {
        if (is_array($force)) {
            if (count($force) > 0) {
                $this->forcedIndexes = $force;
                $this->forceIndex = true;
            } else {
                $this->forceIndex = false;
            }
        } elseif (is_bool($force)) {
            $this->forceIndex = $force;
        }

        return $this;
    }

    public function getCalcFoundRows()
    {
        return $this->calcFoundRows;
    }

    protected function compileExtensions()
    {
        $extended = '';
        if ($this->useStraightJoin) {
            $extended .= 'STRAIGHT_JOIN';
        }

        if ($this->calcFoundRows) {
            $extended .= ' SQL_CALC_FOUND_ROWS';
        }

        return $extended;
    }

    protected function compileColumns(Builder $query, $columns)
    {
        if (!is_null($query->aggregate)) {
            return;
        }

        $extended = $this->compileExtensions();

        $select = $query->distinct ? 'select '.$extended.' distinct ' : 'select '.$extended.' ';


        return $select . $this->columnize($columns);
    }

    protected function compileFrom(Builder $query, $table)
    {
        $extensions = '';
        if ($this->forceIndex) {
            $extensions = implode(',', $this->forcedIndexes);
            $extensions = ' FORCE INDEX('.$extensions.')';
        }

        return 'from '.$this->wrapTable($table).$extensions;
    }
}
