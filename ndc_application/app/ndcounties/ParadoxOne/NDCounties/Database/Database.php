<?php namespace ParadoxOne\NDCounties\Database;

use Carbon\Carbon;

final class Database
{
    /**
     * The ND Counties database system version.
     *
     * The database version is a 10 character long string.
     *
     */
    const VERSION = '0000000003';

    /**
     * Parses a version string.
     *
     * @param  string $versionString
     * @return int
     */
    public static function parseVersionString($versionString)
    {
        return $versionString;
    }

    /**
     * Returns the ND Counties database system version number.
     *
     * @param  string $parseString
     * @return int
     */
    public static function getVersion($parseString = null)
    {
        if ($parseString !== null) {
            return self::parseVersionString($parseString);
        }

        return self::parseVersionString(self::VERSION);
    }

    /**
     * Gets the now date formatted for a given DBMS.
     *
     * Supported DBMS Systems:
     *      * MySQL | Y/m/d H:i:s
     *
     * @param string $dbms
     * @return string
     * @throws NotImplementedException
     */
    public static function getNowDate($dbms = 'mysql')
    {
        switch ($dbms) {
            case 'mysql':
                return Carbon::now()->format('Y/m/d H:i:s');
                break;
            default:
                throw new NotImplementedException;
        }
    }

    /**
     * Returns the database version parsed from the given string.
     *
     * @param $string
     * @return string
     */
    public static function extractVersion($string)
    {
        if ($string == null) {
            return null;
        }
        $pattern = '/NDCV:\[(.*)\]/';

        $matches = [];
        preg_match_all($pattern, $string, $matches);

        if (count($matches) > 0) {
            return self::parseVersionString(end($matches)[0]);
        }

        return null;
    }

    public static function getSystemUser()
    {
        $user = new \stdClass;
        $user->id = 0;
        $user->admin = 1;
        return $user;
    }
}
