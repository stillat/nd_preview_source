<?php namespace ParadoxOne\NDCounties\Support\Facades;

use Illuminate\Support\Facades\Facade;

class FormPersist extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'formPersister';
    }
}
