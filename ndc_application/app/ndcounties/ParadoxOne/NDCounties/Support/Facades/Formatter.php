<?php namespace ParadoxOne\NDCounties\Support\Facades;

use Illuminate\Support\Facades\Facade;

class Formatter extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'formatter';
    }
}
