<?php namespace ParadoxOne\NDCounties;

use Carbon;
use Illuminate\Auth\AuthManager;
use Illuminate\Cache\Repository;
use Illuminate\Session\SessionManager;
use Illuminate\Support\Facades\Cache;
use ParadoxOne\NDCounties\Contracts\CountyMetaRepositoryInterface;

class UserSessionManager
{
    protected $sessionManager = null;

    protected $cacheRepository = null;

    protected $countyMetaRepository = null;

    protected $authManager = null;

    const WORK_DATE_KEY = 'user_work_date';

    const INVOICE_DUE_DATE_KEY = 'user_invoice_due_date';

    public function __construct(SessionManager $sessionManager, CountyMetaRepositoryInterface $countyMetaRepository,
                                Repository $cacheRepository, AuthManager $authManager)
    {
        $this->sessionManager       = $sessionManager;
        $this->cacheRepository      = $cacheRepository;
        $this->authManager          = $authManager;
        $this->countyMetaRepository = $countyMetaRepository;
    }

    /**
     * Sets the session work log date.
     *
     * @param $workDate
     * @return void
     */
    public function setWorkDate($workDate)
    {
        $this->sessionManager->put(self::WORK_DATE_KEY, $workDate);
    }

    /**
     * Sets the session invoice due date.
     *
     * @param $dueDate
     */
    public function setDueDate($dueDate)
    {
        $this->sessionManager->put(self::INVOICE_DUE_DATE_KEY, $dueDate);
    }

    /**
     * Returns a carbon date.
     *
     * @param $key
     * @return \Carbon
     */
    private function getCarbonDate($key)
    {
        if ($this->sessionManager->has($key)) {
            return new Carbon($this->sessionManager->get($key));
        }

        return Carbon::now();
    }

    /**
     * Gets the session work log date.
     *
     * @return Carbon
     */
    public function getWorkDate()
    {
        return $this->getCarbonDate(self::WORK_DATE_KEY);
    }

    /**
     * Gets the session invoice due date.
     *
     * @return \Carbon
     */
    public function getDueDate()
    {
        return $this->getCarbonDate(self::INVOICE_DUE_DATE_KEY);
    }

    /**
     * Removes the work date from the user session manager.
     *
     * @return void
     */
    public function clearWorkDate()
    {
        $this->sessionManager->forget(self::WORK_DATE_KEY);
    }

    /**
     * Removes the invoice due date from the user session manager.
     */
    public function clearDueDate()
    {
        $this->sessionManager->forget(self::INVOICE_DUE_DATE_KEY);
    }

    public function getMetaInformation()
    {
        return $this->cacheRepository->rememberForever('meta_county_cache' . $this->authManager->user()->last_tenant,
            function () {
                $databaseMetaInformation = $this->countyMetaRepository->getMetaInformation();

                $metaInformation = new \stdClass;

                if ($databaseMetaInformation !== null) {
                    $metaInformation->fuelCount      = $databaseMetaInformation->record_highest_fuel_count;
                    $metaInformation->materialCount  = $databaseMetaInformation->record_highest_material_count;
                    $metaInformation->equipmentCount = $databaseMetaInformation->record_highest_equipment_count;
                } else {
                    $metaInformation->fuelCount      = 0;
                    $metaInformation->materialCount  = 0;
                    $metaInformation->equipmentCount = 0;
                }

                return $metaInformation;
            });
    }

    /**
     * Sets the county meta information.
     *
     * Pass in `-1` for all values to do a hard reset.
     *
     * @param $highestFuelCount
     * @param $highestMaterialCount
     * @param $highestEquipmentCount
     * @return bool
     */
    public function setMetaInformation($highestFuelCount, $highestMaterialCount, $highestEquipmentCount)
    {
        // Let's blow away the current value stored in the cache.
        $this->cacheRepository->forget('meta_county_cache' . $this->authManager->user()->last_tenant);
        Cache::forget('meta_county_cache' . $this->authManager->user()->last_tenant);

        // This lets us do the super secret hard reset mode.
        if ($highestFuelCount == -1 && $highestMaterialCount == -1 && $highestEquipmentCount == -1) {
            $actualData = $this->countyMetaRepository->getActualMetaInformation();
            $this->countyMetaRepository->updateMetaInformation($actualData->highestFuel,
                                                               $actualData->highestMaterial,
                                                               $actualData->highestEquipment);

            return true;
        }

        // We are not doing a hard reset, so we just use the parameters.
        $this->countyMetaRepository->updateMetaInformation($highestFuelCount, $highestMaterialCount,
                                                           $highestEquipmentCount);
    }
}
