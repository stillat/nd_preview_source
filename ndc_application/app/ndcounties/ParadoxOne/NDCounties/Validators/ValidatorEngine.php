<?php namespace ParadoxOne\NDCounties\Validators;

class ValidatorEngine
{
    public function validateColor($attribute, $value, $parameters)
    {
        if (is_string($value) === false) {
            return false;
        }

        if (strtolower($value) == 'transparent') {
            return true;
        }

        return preg_match('/^#[a-f0-9]{6}$/i', $value);
    }
}
