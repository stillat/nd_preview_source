<?php namespace ParadoxOne\NDCounties;

use Illuminate\Filesystem\Filesystem;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Event;
use ParadoxOne\NDCounties\Application\Updater;

class NDCApplication
{
    /**
     * Determine if the application is currently installed.
     *
     * @return boolean
     */
    public static function isInstalled()
    {
        return file_exists(Config::get('app.manifest') . '/ndc_installed');
    }

    public static function needsUpdating()
    {
        return file_exists(Config::get('app.manifest').'/update_available');
    }

    public static function update()
    {
        /** @var Updater $updater */
        $updater = new Updater(new Filesystem, app_path());
        $updater->setWorkingPath(realpath(app_path().'../../'));
        $updater->migrateHub();
        //$updater->updateDependencies();
        @unlink(storage_path().'/meta/update_available');
    }

    public static function flagUpdated()
    {
        Event::fire('ndc.updated');
        @unlink(Config::get('app.manifest').'/update_available');
    }

    /**
     * Indicates that the system is installed.
     *
     * @return void
     */
    public static function flagInstalled()
    {
        Event::fire('ndc.installed');
        touch(Config::get('app.manifest') . '/ndc_installed');
    }

    /**
     * Indicates that the system is not installed.
     *
     * @return void
     */
    public static function flagUninstalled()
    {
        Event::fire('ndc.uninstalled');
        @unlink(Config::get('app.manifest') . '/ndc_installed');
    }
}
