<?php namespace ParadoxOne\NDCounties\Exporters;

use League\Csv\Writer;
use ParadoxOne\NDCounties\Contracts\ExportingUtilities\ExporterInterface;

class CSVExporter implements ExporterInterface
{
    protected $csvWriter = null;

    protected $headers = null;

    protected $data = null;

    public function __construct()
    {
        $this->csvWriter = new Writer(new \SplTempFileObject());
        $this->csvWriter->setDelimiter(',');
        $this->csvWriter->setEncoding('utf-8');
    }


    /**
     * Sets the headers for the row data.
     *
     * @param array $headers
     *
     * @return mixed
     */
    public function setHeaders(array $headers)
    {
        $this->headers = $headers;
    }

    /**
     * Adds a row of data to the exporter.
     *
     * @param array $row
     *
     * @return mixed
     */
    public function addRow(array $row)
    {
        $this->data = $row;
    }

    /**
     * Exports the data.
     *
     * @return mixed
     */
    public function export()
    {
        $this->csvWriter->insertOne($this->headers);
        $this->csvWriter->insertAll($this->data);
        // TODO: Implement export() method.
    }
}
