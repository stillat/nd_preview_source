<?php namespace ParadoxOne\NDCounties\Contracts;

interface AccountRepositoryInterface
{
    /**
     * Returns a list of accounts missing for a user.
     *
     * This function is primarily used as the exact opposite of
     * getAccounts(). Use this function to find all the service
     * level accounts that a user can potentially belong to in the
     * future.
     *
     * @param  int $user_id The user ID.
     * @return mixed
     */
    public function getMissingAccounts($user_id);

    /**
     * Returns a collection of all the accounts for a user.
     *
     * @param  int $userID The user ID.
     * @return mixed
     */
    public function getAccounts($userID);

    /**
     * Returns a collection of all the accounts in the system.
     *
     * @return [type] [description]
     */
    public function getAllAccounts();

    /**
     * Returns the logical primary account for a user.
     *
     * This function will return the first logical service account
     * discovered for a given user. This is typically used in conjunction
     * with the 'last_tenant' property from the user API.
     *
     * This function also returns a value 'total_service _accounts' which
     * will indicate how many service accounts the user belongs to, including
     * the returned logical account.
     *
     * @param  int $userID The user ID.
     * @return mixed
     */
    public function getLogicalPrimaryUserAccount($userID);

    /**
     * Returns an account based for the ID.
     *
     * @param  int $accountID The ID of the account.
     * @return mixed
     */
    public function getAccount($accountID);

    /**
     * Returns only the basic account information.
     *
     * @param $accountID
     * @return mixed
     */
    public function getAccountInformation($accountID);

    /**
     * Returns the account settings for the current active account.
     *
     * @param $id
     * @return mixed
     */
    public function getAccountSettings($id);

    /**
     * Automagically creates account settings for
     * an active account.
     */
    public function addSettingsToAccount();

    /**
     * Automagically creates contact settings for an account.
     *
     * @param  int $account
     * @return mixed
     */
    public function addContactSettingsToAccount($account);

    /**
     * Updates the accounts contact settings.
     *
     * @param       $accountID
     * @param array $newSettings
     * @return mixed
     */
    public function updateAccountContactSettings($accountID, array $newSettings);

    /**
     * Updates the accounts data settings.
     *
     * @param       $accountID
     * @param array $newSettings
     * @return mixed
     */
    public function updateAccountDataSettings($accountID, array $newSettings);

    /**
     * Updates the accounts display settings.
     *
     * @param array $input
     * @return mixed
     */
    public function updateAccountDisplaySettings(array $input);

    public function updateAccountHeadingDisplaySettings(array $input);

    /**
     * Sets an account current process.
     *
     * @param $accountID
     * @param $process
     * @return mixed
     */
    public function setAccountProcess($accountID, $process);

    /**
     * Releases all running processes.
     *
     * @param $accountID
     * @return mixed
     */
    public function releaseProcesses($accountID);

    /**
     * Gets all the users associated with an account.
     *
     * @param $accountID
     * @return mixed
     */
    public function getAccountUsers($accountID);

    public function setActiveStatus($account, $isActive);

    public function removeAccount($accountID);
}
