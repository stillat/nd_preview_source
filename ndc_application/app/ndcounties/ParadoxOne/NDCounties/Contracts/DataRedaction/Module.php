<?php

namespace ParadoxOne\NDCounties\Contracts\DataRedaction;

interface Module
{
    public function redact($data);
}
