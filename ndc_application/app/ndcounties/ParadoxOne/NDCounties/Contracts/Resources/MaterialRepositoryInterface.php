<?php namespace ParadoxOne\NDCounties\Contracts\Resources;

interface MaterialRepositoryInterface
{
    public function getMaterials();

    public function getAll();

    public function getAllWithTrashed();

    public function getModelById($id);

    public function errors();

    public function search($searchTerms);

    public function getConsumableByID($id);

    public function unTrash($materials);

}
