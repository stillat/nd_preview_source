<?php namespace ParadoxOne\NDCounties\Contracts\Data;

use ParadoxOne\NDCounties\Database\Management\BackupLogRepository;

interface BackupLogRepositoryInterface
{
    const STATUS_BACKUP_CREATED           = 1;
    const STATUS_BACKUP_CREATE_FAILED     = 2;
    const STATUS_BACKUP_RESTORED          = 3;
    const STATUS_BACKUP_RESTORED_FAILED   = 4;
    const STATUS_BACKUP_REMOVE            = 5;
    const STATUS_BACKUP_REMOVE_FAILED     = 6;
    const STATUS_BACKUP_DOWNLOADED        = 7;
    const STATUS_BACKUP_DOWNLOADED_FAILED = 8;
    const STATUS_BACKUP_UPLOADED          = 9;
    const STATUS_BACKUP_UPLOADED_FAILED   = 10;
    const STATUS_BACKUP_VERSION_UPDATED   = 11;
    const STATUS_BACKUP_VERSION_SYNCED    = 12;
    const STATUS_BACKUP_VERSION_SYNC_FAILED = 13;

    /**
     * Logs a backup operation.
     *
     * @param array $backupDetails
     * @return mixed
     */
    public function logBackup(array $backupDetails);

    /**
     * Gets the backup statistics for a given tenant.
     *
     * @param  int $tenantID
     * @return mixed
     */
    public function getBackupStatistics($tenantID);

    public function logCreate($user, $backupName, $account);

    public function logCreateFailed($user, $backupName, $account);

    public function logRestore($user, $backupName, $account);

    public function logRestoreFailed($user, $backupName, $account);

    public function logRemove($user, $backupName, $account);

    public function logRemoveFailed($user, $backupName, $account);

    public function logDownload($user, $backupName, $account);

    public function logDownloadFailed($user, $backupName, $account);

    public function logVersionUpdated($user, $backupName, $account);

    public function logUpload($user, $backupName, $account);

    public function logUploadFailed($user, $backupName, $account);

    public function logSystemSynced($user, $backupName, $account);

    public function logSystemSyncedFailed($user, $backupName, $account);
}
