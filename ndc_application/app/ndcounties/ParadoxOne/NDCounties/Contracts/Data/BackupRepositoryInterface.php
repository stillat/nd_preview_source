<?php namespace ParadoxOne\NDCounties\Contracts\Data;

use Symfony\Component\HttpFoundation\File\UploadedFile;

interface BackupRepositoryInterface
{
    public function setTenantAccount($tenantID);

    public function getTenantAccount();

    public function createBackup();

    public function getBackups();

    public function removeBackup($backupKey);

    public function restoreBackup($backupKey);

    public function prepareUploadedBackup(UploadedFile $backupFile);
}
