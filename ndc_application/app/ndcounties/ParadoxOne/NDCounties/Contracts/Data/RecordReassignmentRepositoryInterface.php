<?php namespace ParadoxOne\NDCounties\Contracts\Data;

interface RecordReassignmentRepositoryInterface
{
    /**
     * Reassigns the provided record pairs.
     *
     * @param array $startRecords
     * @param array $finishRecords
     * @return mixed
     */
    public function reassignRecordPairs(array $startRecords, array $finishRecords);
}
