<?php namespace ParadoxOne\NDCounties\Contracts\Data;

interface DataExporterInterface
{
    public function setEndDate($endDate);

    public function getEndDate();

    public function setStartDate($startDate);

    public function getStartDate();

    public function setAggregateEmployeeRegularHours($value);

    public function setAggregateEmployeeRegularCost($value);

    public function setAggregateEmployeeRegularBenefits($value);

    public function setAggregateEmployeeOvertimeHours($value);

    public function setAggregateEmployeeOvertimeCost($value);

    public function setAggregateEmployeeOvertimeBenefits($value);

    public function setAggregateEquipmentQuantity($value);

    public function setAggregateEquipmentCost($value);

    public function setAggregateMaterialQuantity($value);

    public function setAggregateMaterialCost($value);

    public function setAggregateFuelQuantity($value);

    public function setAggregateFuelCost($value);

    public function setAggregatePropertyCost($value);

    public function setAggregateBillingQuantity($value);

    public function setAggregateBillingCost($value);

    public function setGetAllListData($value);

    public function export();

    public function setPath($path);

    public function getPath();

    public function setFileName($fileName);

    public function getFileName();

    public function getBaseName();

    public function setRecordRestrictions($restrictions);

    public function setRawOptions($options);

    public function getRawOptions();
}
