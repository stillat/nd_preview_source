<?php namespace ParadoxOne\NDCounties\Contracts\Reporting;

interface ReportsRepositoryInterface
{
    /**
     * Creates a new report definition.
     *
     * @param array $reportData
     * @return mixed
     */
    public function create(array $reportData);

    /**
     * Creates a custom report.
     *
     * @param array $reportData
     * @return mixed
     */
    public function createCustomReport(array $reportData);

    /**
     * Updates a given definition entry.
     *
     * @param       $reportID
     * @param array $reportData
     * @return mixed
     */
    public function update($reportID, array $reportData);

    /**
     * Removes a definition entry.
     *
     * @param $reportID
     * @return mixed
     */
    public function remove(array $reportID);

    /**
     * Returns a list of reports without the SRL data.
     *
     * @return mixed
     */
    public function getReports();

    /**
     * Retrieves a report.
     *
     * @param $reportID
     * @return mixed
     */
    public function get($reportID);

    /**
     * Indicates if a report exists by name.
     *
     * @param $reportName
     * @return mixed
     */
    public function existsByName($reportName);

    /**
     * Installs the default reporting system values.
     *
     * @return mixed
     */
    public function installDefaultReportingSystem();
}
