<?php namespace ParadoxOne\NDCounties\Contracts\Reporting;

use ParadoxOne\NDCounties\Contracts\WorkEntries\WorkEntryReaderRepositoryInterface;

interface ReportConstructorInterface
{
    /**
     * Sets the meta definition entry record.
     *
     * @param $entryID
     * @return mixed
     */
    public function setMetaDefinitionEntry($entryID);

    /**
     * Sets the reports organization strategy.
     *
     * @param $mode
     * @param $value
     * @return mixed
     */
    public function setReportOrganizationStrategy($mode, $value);

    /**
     * Returns the reports organization strategy.
     *
     * @return mixed
     */
    public function getOrganizationStrategy();

    /**
     * Gets the meta definition entry used by the constructor.
     *
     * @return mixed
     */
    public function getMetaDefinitionEntry();

    /**
     * Returns a collection of formulas used by the constructor.
     *
     * @return mixed
     */
    public function getFormulas();

    /**
     * Returns the SRL Header.
     *
     * @return mixed
     */
    public function getSRLHeader();

    /**
     * Returns the raw SRL Body.
     *
     * @return mixed
     */
    public function getRawSRLBody();

    /**
     * Returns the SRL Footer.
     *
     * @return mixed
     */
    public function getSRLFooter();

    /**
     * Prepares a work reader for report retrieval.
     *
     * @param WorkEntryReaderRepositoryInterface $reader
     * @return mixed
     */
    public function prepare(WorkEntryReaderRepositoryInterface &$reader);

    /**
     * Sets the total block for the report.
     *
     * @param $blockID
     * @return mixed
     */
    public function setTotalBlock($blockID);

    /**
     * Gets the total block for the report.
     *
     * @return mixed
     */
    public function getTotalBlock();

    /**
     * Sets the report record total query.
     *
     * @param $equationID
     * @return mixed
     */
    public function setReportRecordTotalQuery($equationID);

    /**
     * Gets the report record total query.
     *
     * @return mixed
     */
    public function getReportRecordTotalQuery();

    /**
     * Gets the report name for the organization strategy.
     *
     * @param $mode
     * @return string
     */
    public function getOrganizationStrategyName($mode = null);

    /**
     * Gathers the organization groups.
     *
     * @param $organizationGroup
     * @return mixed
     */
    public function gatherOrganizationGroups($organizationGroup);

    /**
     * Groups the report by a given value.
     *
     * @param $value
     * @return mixed
     */
    public function groupReportBy($value);

    public function alsoGroupBy($value);

    public function sortReportBy($column, $sort);

    /**
     * Groups the report using a special grouping organization mode.
     *
     * @param $specialColumn
     * @param $firstGroupValues
     * @param $secondGroupValues
     * @param $firstGroupName
     * @param $secondGroupName
     * @return mixed
     */
    public function specialReportGrouping($specialColumn, $firstGroupValues, $secondGroupValues, $firstGroupName, $secondGroupName);

    /**
     * Gets information about the reports special grouping information.
     *
     * @return mixed
     */
    public function getSpecialReportGroupingInformation();

    /**
     * Gets the records for the report.
     *
     * @return mixed
     */
    public function getRecords();

    /**
     * Applies a filter to the report.
     *
     * @param       $filterName
     * @param array $filterValues
     * @param bool  $exclude
     *
     * @return mixed
     */
    public function applyFilter($filterName, array $filterValues, $exclude = false);

    /**
     * Gets the reporting filters.
     *
     * @return mixed
     */
    public function getReportFilters();

    /**
     * Sets the internal reporting engine mode.
     *
     * @param $mode
     * @return mixed
     */
    public function setInternalMode($mode);

    /**
     * Gets the internal reporting engine mode.
     *
     * @return mixed
     */
    public function getInternalMode();

    /**
     * Limits the report to the specified property types.
     *
     * @param $propertyTypes
     * @return mixed
     */
    public function limitPropertyTypes($propertyTypes);

    /**
     * Enables the consumable override.
     *
     * @return mixed
     */
    public function enableConsumableOverride();

    /**
     * Disables the consumable override.
     *
     * @return mixed
     */
    public function disableConsumableOverride();

    /**
     * Sets the consumable record grouping.
     *
     * @param $columns
     * @return mixed
     */
    public function groupConsumableRecord($columns);

    /**
     * Sets a consumable override restriction mode.
     *
     * @param       $type
     * @param array $restriction
     * @return mixed
     */
    public function setConsumableRestrictions($type, array $restriction);

    /**
     * Sets a constraint on the report.
     *
     * @param $constraintName
     * @param $constraintComparisonOperator
     * @param $constraintValue
     * @return mixed
     */
    public function constrainReport($constraintName, $constraintComparisonOperator, $constraintValue);
}
