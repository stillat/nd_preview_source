<?php

namespace ParadoxOne\NDCounties\Contracts\Reporting;

interface DirectReportInterface
{

    public function setReportConstructor(&$reportConstructor);

    public function setWorkRecordIDs($workRecords);

    public function getRecords();

    public function printHeaders();

    public function printBody();

    public function printFooters();

    public function setActivityFilters($filters);

    public function setDepartmentFilters($filters);

    public function setDistrictFilters($filters);

    public function setEmployeeFilters($filters);

    public function setEquipmentFilters($filters);

    public function setMaterialFilters($filters);

    public function setProjectFilters($filters);

    public function setRoadFilters($filters);

    public function setReportScopeFilters($filters);

    public function setPropertyFilters($filters);

}