<?php namespace ParadoxOne\NDCounties\Contracts\Reporting;

interface ReportDefinitionInterface
{
    public function getHeader();

    public function getBody();

    public function getReportID();

    public function getFooter();

    public function getSettings();

    public function getBaseReport();

    public function getAccountID();

    public function getIcon();

    public function getDescription();

    public function getName();

    public function isLimited();

    public function isExtension();

    public function getEncodedSettings();

    public function getPropertyTypeLimits();

    public function getNotificationMessage();
}
