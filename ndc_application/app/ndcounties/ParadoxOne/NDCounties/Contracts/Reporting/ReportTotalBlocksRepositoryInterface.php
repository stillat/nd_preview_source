<?php namespace ParadoxOne\NDCounties\Contracts\Reporting;

interface ReportTotalBlocksRepositoryInterface
{
    /**
     * Returns a builder for all total blocks.
     *
     * @return mixed
     */
    public function getTotalBlocks();

    /**
     * Gets a total block by ID.
     *
     * @param $totalBlockID
     * @return mixed
     */
    public function getTotalBlock($totalBlockID);

    /**
     * Gets the default total block for the given account.
     *
     * @param $accountID
     * @return mixed
     */
    public function getDefaultTotalBlockForAccount($accountID);

    /**
     * Sets the default total block for the given account.
     *
     * @param $accountID
     * @param $totalBlockID
     * @return mixed
     */
    public function setDefaultTotalBlockForAccount($accountID, $totalBlockID);
}
