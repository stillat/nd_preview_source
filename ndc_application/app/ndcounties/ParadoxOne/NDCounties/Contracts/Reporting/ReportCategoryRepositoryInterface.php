<?php namespace ParadoxOne\NDCounties\Contracts\Reporting;

interface ReportCategoryRepositoryInterface
{
    /**
     * Gets the report categories.
     *
     * @return mixed
     */
    public function getCategories();
}
