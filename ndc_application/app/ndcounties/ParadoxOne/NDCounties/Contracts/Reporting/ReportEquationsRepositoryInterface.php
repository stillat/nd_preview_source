<?php namespace ParadoxOne\NDCounties\Contracts\Reporting;

interface ReportEquationsRepositoryInterface
{
    /**
     * Creates an equation for the given account.
     *
     * @param       $accountID
     * @param array $equationInformation
     * @return mixed
     */
    public function createEquation($accountID, array $equationInformation);

    /**
     * Updates the given equation.
     *
     * @param       $equationID
     * @param array $equationInformation
     * @return mixed
     */
    public function updateEquation($equationID, array $equationInformation);

    /**
     * Validates a formula.
     *
     * @param $formula
     * @return mixed
     */
    public function validateFormula($formula);

    /**
     * Gets a list of equations.
     *
     * @return mixed
     */
    public function getEquations();

    /**
     * Gets information for the given equation ID.
     *
     * @param $equationID
     * @return mixed
     */
    public function getEquation($equationID);

    /**
     * Removes a given equation.
     *
     * @param $equationID
     * @return mixed
     */
    public function removeEquation($equationID);

    /**
     * Retrieve the default record equation for the given account.
     *
     * @param $accountID
     * @return mixed
     */
    public function getDefaultEquationForAccount($accountID);

    /**
     * Sets the default record equation for the given account.
     *
     * @param $accountID
     * @param $equationID
     * @return mixed
     */
    public function setDefaultEquationForAccount($accountID, $equationID);
}
