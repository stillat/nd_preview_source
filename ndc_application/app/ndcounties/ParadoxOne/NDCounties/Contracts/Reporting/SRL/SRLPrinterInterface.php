<?php namespace ParadoxOne\NDCounties\Contracts\Reporting\SRL;

interface SRLPrinterInterface
{
    /**
     * Sets the options for the printer.
     * f
     *
     * @param array $options
     * @return mixed
     */
    public function setOptions(array $options);

    /**
     * Gets the options set.
     *
     * @return array
     */
    public function getOptions();

    /**
     * Sets the source for the printer.
     *
     * @param $source
     * @return mixed
     */
    public function setSource($source);

    /**
     * Sets the raw data for the printer.
     *
     * @param $rawData
     * @return mixed
     */
    public function setRawData(&$rawData);

    /**
     * Returns the source with the raw data interlaced.
     *
     * @return mixed
     */
    public function srlPrint();
}
