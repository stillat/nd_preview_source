<?php namespace ParadoxOne\NDCounties\Contracts\Reporting;

interface ReportEngineModesRepositoryInterface
{
    /**
     * Returns a collection of reporting engine internal modes.
     *
     * @return mixed
     */
    public function getReportingModes();
}
