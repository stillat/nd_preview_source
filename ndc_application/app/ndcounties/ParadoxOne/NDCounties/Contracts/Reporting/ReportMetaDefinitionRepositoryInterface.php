<?php namespace ParadoxOne\NDCounties\Contracts\Reporting;

interface ReportMetaDefinitionRepositoryInterface
{
    /**
     * Creates a new report definition.
     *
     * @param array $definition
     * @return mixed
     */
    public function createDefinition(array $definition);

    /**
     * Creates a custom report.
     *
     * @param array $reportData
     * @return mixed
     */
    public function createCustomReport(array $reportData);

    /**
     * Removes an account's custom report.
     *
     * Implementations must ensure that accounts can only
     * remove reports registered to their own accounts.
     *
     * @param $account
     * @param $reportID
     * @return mixed
     */
    public function removeCustomReport($account, $reportID);

    /**
     * Updates a given definition entry.
     *
     * @param       $definitionID
     * @param array $definition
     * @return mixed
     */
    public function updateDefinition($definitionID, array $definition);

    /**
     * Removes a definition entry.
     *
     * @param $definitionID
     * @return mixed
     */
    public function removeDefinition($definitionID);

    /**
     * Returns a list of definitions without the SRL data.
     *
     * @return mixed
     */
    public function getDefinitions();

    /**
     * Retrieves a definition.
     *
     * @param $definitionID
     * @return mixed
     */
    public function getDefinition($definitionID);

    /**
     * Indicates if a report exists by name.
     *
     * @param $reportName
     * @return mixed
     */
    public function reportExistsByName($reportName, $exclude = []);

    /**
     * Installs the default reporting system values.
     *
     * @return mixed
     */
    public function installDefaultReportingSystem();

    public function exportReportsForDownload();
}
