<?php namespace ParadoxOne\NDCounties\Contracts\Extensibility;

interface GenericHandlerInterface
{
    public function handle($object, $validateOnly = false);

    public function handleLog($object, $validateOnly = false);

    public function logErrors();

    public function getRecordPropertyTotal();

    public function errors();
}
