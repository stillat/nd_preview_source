<?php namespace ParadoxOne\NDCounties\Contracts\Extensibility\PropertyTypes;

interface PropertyAdapterInterface
{
    /**
     * Called when the adapter is loaded.
     *
     * @return void
     */
    public function boot();

    /**
     * Gets the backing table name.
     *
     * @return mixed
     */
    public function getTableName();

    /**
     * Gets the backing work column name.
     *
     * @return mixed
     */
    public function getWorkColumnName();

    /**
     * Gets the name of the property adapater.
     *
     * @return string The name of the adapater.
     */
    public function getName();

    /**
     * Gets the description of the property adapter.
     *
     * @return string The description for the adapter.
     */
    public function getDescription();

    /**
     * Gets the adapter's numeric identifier.
     *
     * @return int The identifier.
     */
    public function getAdapterNumericIdentifier();

    /**
     * Gets the adapters insert view.
     *
     * @return string The view name.
     */
    public function getInsertView();

    /**
     * Gets the adapters update view.
     *
     * @return mixed
     */
    public function getUpdateView();

    /**
     * Gets the adapaters create property view.
     *
     * This is used to inject additional data into the
     * create property process.
     *
     * @return string The view name.
     */
    public function getCreatePropertyView();

    /**
     * Gets the adapters create property handler.
     *
     * @return string The fully-qualified class name of the handler.
     */
    public function getCreatePropertyHandler();

    /**
     * Gets the adapters update property view.
     *
     * @return string The view name.
     */
    public function getUpdatePropertyView();

    /**
     * Gets the adapters update property handler.
     *
     * @return string The fully-qualified class name of the handler.
     */
    public function getUpdatePropertyHandler();

    /**
     * Gets the adapters delete property handler.
     *
     * @return string The fully-qualified class name of the handler.
     */
    public function getDeletePropertyHandler();

    /**
     * Determines if the adatper should be hidden.
     *
     * @return bool
     */
    public function getIsHidden();

    /**
     * Returns the relationship that should be used for fetching data
     * using the property APIs.
     *
     * @return array
     */
    public function getPropertyRelationships();

    /**
     * Returns an array of the relationships the adapter provides.
     *
     * @return array
     */
    public function requires();

    /**
     * Returns a string of table headers to be used when listing the data.
     *
     * @return string
     */
    public function getListTableHeaders();

    /**
     * Returns an array of headers to be used when exporting data.
     *
     * @return mixed
     */
    public function getExportHeaders();

    /**
     * Returns an array of data to be used when building export data.
     *
     * @param $data
     * @return mixed
     */
    public function formatExportArray($data);

    /**
     * Generates a HTML fragment to display the data.
     *
     * @param  mixed $data
     * @return string
     */
    public function formatListTableHeaders($data);

    /**
     * Generates a HTML fragment to display in property views.
     *
     * @param $data
     * @return string
     */
    public function formatPropertyView($data);

    /**
     * Generates a HTML fragment to display on invoices.
     *
     * @param $data
     * @return mixed
     */
    public function formatInvoiceListing($data);

    public function formatReportView($data);

    public function shouldShowOnReports($data);
}
