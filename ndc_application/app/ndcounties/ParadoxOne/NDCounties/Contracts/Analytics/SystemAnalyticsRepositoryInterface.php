<?php namespace ParadoxOne\NDCounties\Contracts\Analytics;

interface SystemAnalyticsRepositoryInterface
{
    /**
     * Gets the system usage analytics.
     *
     * @return mixed
     */
    public function getSystemUsageAnalytics();
}
