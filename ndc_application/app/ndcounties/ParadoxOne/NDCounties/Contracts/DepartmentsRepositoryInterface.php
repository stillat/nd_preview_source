<?php namespace ParadoxOne\NDCounties\Contracts;

interface DepartmentsRepositoryInterface
{
    public function getDepartments();

    public function getAll();

    public function getAllWithTrashed();

    public function getModelById($id);

    public function errors();

    public function search($searchTerms);

    public function unTrash($departments);

}
