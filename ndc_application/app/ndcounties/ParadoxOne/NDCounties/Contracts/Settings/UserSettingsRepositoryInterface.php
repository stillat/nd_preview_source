<?php namespace ParadoxOne\NDCounties\Contracts\Settings;

interface UserSettingsRepositoryInterface
{
    /**
     * Gets a setting value.
     *
     * @param $userID
     * @param $subSystem
     * @param $settingName
     * @param $defaultValue
     * @return mixed
     */
    public function getSettingValue($userID, $subSystem, $settingName, $defaultValue = null);

    /**
     * Removes a setting from the user.
     *
     * @param $userID
     * @param $subSystem
     * @param $settingName
     * @return mixed
     */
    public function removeSetting($userID, $subSystem, $settingName);

    /**
     * Removes an entire setting sub-system.
     *
     * @param $userID
     * @param $subSystem
     * @return mixed
     */
    public function removeSubSystem($userID, $subSystem);

    /**
     * Creates a setting for the given user.
     *
     * @param $userID
     * @param $subSystem
     * @param $settingName
     * @param $initialValue
     * @return mixed
     */
    public function createSetting($userID, $subSystem, $settingName, $initialValue);

    /**
     * Sets the setting value.
     *
     * @param $userID
     * @param $subSystem
     * @param $settingName
     * @param $newValue
     * @return mixed
     */
    public function setSettingValue($userID, $subSystem, $settingName, $newValue);
}
