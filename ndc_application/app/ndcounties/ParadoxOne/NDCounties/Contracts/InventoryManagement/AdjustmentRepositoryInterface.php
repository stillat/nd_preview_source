<?php namespace ParadoxOne\NDCounties\Contracts\InventoryManagement;

interface AdjustmentRepositoryInterface
{
    /**
     * Adds a new adjustment to the inventory system.
     *
     * @param $adjustmentData
     * @return mixed
     */
    public function addAdjustment($adjustmentData);

    /**
     * Removes an adjustment from the system.
     *
     * @param $adjustmentID
     * @return mixed
     */
    public function removeAdjustment($adjustmentID);

    /**
     * Refreshes the DB cached inventory level for the given consumable.
     *
     * @param $consumableID
     * @return mixed
     */
    public function refreshInventoryLevel($consumableID);

    /**
     * Gets a builder instance returning adjustments for the given consumable.
     *
     * @param $consumableID
     * @return mixed
     */
    public function getAdjustments($consumableID);

    /**
     * Removes all the adjustments for a given consumable.
     *
     * @param $consumableID
     *
     * @return mixed
     */
    public function removeAdjustmentsForConsumable($consumableID);
}
