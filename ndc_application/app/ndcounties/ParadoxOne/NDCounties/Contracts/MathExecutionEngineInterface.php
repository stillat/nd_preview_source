<?php namespace ParadoxOne\NDCounties\Contracts;

interface MathExecutionEngineInterface
{
    /**
     * Add two arbitrary precision numbers
     *
     * @param string $a
     * @param string $b
     */
    public function add($a, $b);

    /**
     * Divide two arbitrary precision numbers
     *
     * @param string $a
     * @param string $b
     */
    public function div($a, $b);

    /**
     * Compare two arbitrary precision numbers
     *
     * @param string $a
     * @param string $b
     */
    public function comp($a, $b);

    /**
     * Multiply two arbitrary precision numbers
     *
     * @param string $a
     * @param string $b
     */
    public function mul($a, $b);

    /**
     * Raise an arbitrary precision number to another
     *
     * @param string $a
     * @param string $b
     */
    public function pow($a, $b);

    /**
     * Get the square root of an arbitrary precision number
     *
     * @param string $a
     */
    public function sqrt($a);

    /**
     * Subtract one arbitrary precision number from another
     *
     * @param string $a
     * @param string $b
     */
    public function sub($a, $b);
}
