<?php namespace ParadoxOne\NDCounties\Contracts\EquipmentUnits;

interface OdometerReadingRepositoryInterface
{
    /**
     * Removes an odometer reading based on the associated type, ID and the
     * work record ID for the odometer record ID.
     *
     * @param int $associatedType
     * @param int $associatedID
     * @return boolean
     */
    public function removeAssociated($associatedType, $associatedID);

    /**
     * Returns the odometer reading based on the associated type and the ID
     * for the associated type.
     *
     * @param int $associatedType The associated type.
     * @param int $associatedID   The associated type's ID.
     * @return mixed
     */
    public function getOdometerReadingByAssociated($associatedType, $associatedID);

    /**
     * Updates an odometer reading based on the associated type, and the ID for the associated
     * type.
     *
     * @param int $associatedType
     * @param int $associatedID
     * @param int $newOdometerReading
     * @return mixed
     */
    public function updateOdometerReadingByAssociated($associatedType, $associatedID, $newOdometerReading);
}
