<?php namespace ParadoxOne\NDCounties\Contracts\EquipmentUnits;

use ParadoxOne\NDCounties\Contracts\RecordableInterface;

interface WorkRecordRepositoryInterface extends RecordableInterface
{
    public function isValid($validateDat);
}
