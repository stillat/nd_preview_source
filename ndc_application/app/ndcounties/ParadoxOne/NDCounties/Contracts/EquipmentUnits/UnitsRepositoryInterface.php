<?php namespace ParadoxOne\NDCounties\Contracts\EquipmentUnits;

interface UnitsRepositoryInterface
{
    public function getModelById($id);

    public function getUnits();

    public function errors();

    public function setCreateProperty($id);

    public function isValid(array $data = array());

    public function getAccessoryUnits($id);

    public function addAccessoryUnitTo($id);
}
