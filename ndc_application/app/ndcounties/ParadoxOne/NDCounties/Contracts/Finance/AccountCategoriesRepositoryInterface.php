<?php namespace ParadoxOne\NDCounties\Contracts\Finance;

interface AccountCategoriesRepositoryInterface
{
    public function getCategories();

    public function getCategory($id);
}
