<?php namespace ParadoxOne\NDCounties\Contracts\Finance;

interface TransactionRepositoryInterface
{
    public function getAccountTransactions($accountID);

    public function getInvoiceTransactions($invoiceID, $paidToAccount);

    public function getModelByID($id);

    /**
     * Updates the status indicators for the given transactions.
     *
     * @param array $transactions
     * @param       $newStatus
     * @return mixed
     */
    public function updateTransactionStatuses(array $transactions, $newStatus);

    /**
     * Refreshes all account balances.
     *
     * @return mixed
     */
    public function refreshAccountBalances();

    /**
     * Creates a transaction.
     *
     * @param $beginAccount
     * @param $destination
     * @param $amount
     * @param $authorizedBy
     * @param $transactionMessage
     * @param $transactionStatus
     * @param $workRecordID
     * @return mixed
     */
    public function transferAmountToAccount($beginAccount, $destination, $amount,
                                            $authorizedBy, $transactionMessage, $transactionStatus, $workRecordID);
}
