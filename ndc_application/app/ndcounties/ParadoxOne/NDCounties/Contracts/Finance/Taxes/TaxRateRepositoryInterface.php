<?php namespace ParadoxOne\NDCounties\Contracts\Finance\Taxes;

interface TaxRateRepositoryInterface
{
    /**
     * Gets all tax rates.
     *
     * @return array The tax rates.
     */
    public function getTaxRates();

    public function getTaxRatesWithTrashed();

    public function unTrash($taxRates);

    /**
     * Returns a collection of tax rates that affect employees.
     *
     * @return array The tax rates.
     */
    public function getTaxRatesForEmployees();

    /**
     * Returns a collection of tax rates that affect equipment units.
     *
     * @return array The tax rates.
     */
    public function getTaxRatesForEquipmentUnits();

    /**
     * Returns a collection of tax rates that affect fuels.
     *
     * @return array The tax rates.
     */
    public function getTaxRatesForFuels();

    /**
     * Returns a collection of tax rates that affect materials.
     *
     * @return array The tax rates.
     */
    public function getTaxRatesForMaterials();

    /**
     * Returns a filtered result set of tax rates.
     *
     * @param  string $searchTerms The search terms.
     * @return array               The search results.
     */
    public function search($searchTerms);

    /**
     * Retrieves a tax rate by ID.
     *
     * @param  int $id The ID of the tax rate.
     * @return object
     */
    public function getTaxRateByID($id);

    /**
     * Returns an array of errors, if present.
     *
     * @return array
     */
    public function errors();
}
