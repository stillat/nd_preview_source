<?php namespace ParadoxOne\NDCounties\Contracts\Finance;

interface TransactionStatusRepositoryInterface
{
    public function getStatuses();
}
