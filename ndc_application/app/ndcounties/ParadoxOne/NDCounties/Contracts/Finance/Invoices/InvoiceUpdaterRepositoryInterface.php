<?php namespace ParadoxOne\NDCounties\Contracts\Finance\Invoices;

interface InvoiceUpdaterRepositoryInterface
{
    /**
     * Refreshes the invoice's information.
     *
     * @param int   $invoiceID
     * @param mixed $invoiceDataInformation
     * @return mixed
     */
    public function refreshInformation($invoiceID, $invoiceDataInformation);

    /**
     * Sets whether or not the invoice removes itself when it has no
     * work records.
     *
     * @param $doesSelfDestruct
     * @return mixed
     */
    public function setSelfDestructOnEmpty($doesSelfDestruct);

    /**
     * Gets the self destruct setting.
     *
     * @return mixed
     */
    public function getSelfDestructOnEmpty();

    /**
     * Removes the work record from any invoices.
     *
     * @param $workRecordID
     *
     * @return mixed
     */
    public function touchInvoicesForDestroy($workRecordID);

    /**
     * Removes a work record from an invoice.
     *
     * @param $invoiceID
     * @param $workRecordID
     * @return mixed
     */
    public function removeWorkRecordFromInvoice($invoiceID, $workRecordID);

    /**
     * Locks a given invoice.
     *
     * @param $invoiceID
     * @return mixed
     */
    public function lockInvoice($invoiceID);

    /**
     * Unlocks a given invoice.
     *
     * @param $invoiceID
     * @return mixed
     */
    public function unlockInvoice($invoiceID);

    /**
     * Changes an invoices accounts.
     *
     * @param $invoiceID
     * @param $newBillToAccount
     * @param $newPayToAccount
     * @return mixed
     */
    public function changeInvoiceAccounts($invoiceID, $newBillToAccount, $newPayToAccount);

    /**
     * Changes an invoices invoice number.
     *
     * @param $invoiceID
     * @param $newInvoiceNumber
     * @return mixed
     */
    public function changeInvoiceNumber($invoiceID, $newInvoiceNumber);

    /**
     * Syncs the invoice's balance automatically.
     *
     * @param $invoiceID
     * @return mixed
     */
    public function syncInvoiceBalance($invoiceID);
}
