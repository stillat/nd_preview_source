<?php namespace ParadoxOne\NDCounties\Contracts\Finance\Invoices;

/**
 * This writer will be responsible for the actual creation of new
 * invoice records. This interface should define no other functions
 * that do not help to accomplish the writing of a invoice record.
 */
interface InvoiceWriterRepositoryInterface
{
    /**
     * Sets the day invoice payment is due by.
     *
     * @param string $date The invoice due date.
     */
    public function setDueDate($date);

    /**
     * Gets the invoice due date.
     *
     * @return string
     */
    public function getDueDate();

    /**
     * Sets the finance account number.
     *
     * This is the account that the invoice is BILLED to.
     *
     * @param int $account The finance account.
     */
    public function setAccountNumber($account);

    /**
     * Sets the finance paid to account.
     *
     * This is the account that the invoice is PAID to.
     *
     * @param $account
     * @return mixed
     */
    public function setPaidToAccount($account);

    /**
     * Gets the finance account number that the invoice is paid to.
     *
     * @return mixed
     */
    public function getPaidToAccount();

    /**
     * Gets the finance account number.
     *
     * @return int
     */
    public function getAccountNumber();

    /**
     * Sets the invoice number.
     *
     * @param string $invoiceNumber The invoice number.
     */
    public function setInvoiceNumber($invoiceNumber);

    /**
     * Gets the invoice number.
     *
     * @return string
     */
    public function getInvoiceNumber();

    /**
     * Sets the invoice discount amount.
     *
     * @param float $discount The discount amount.
     */
    public function setDiscount($discount = 0.0);

    /**
     * Gets the invoice discount amount.
     *
     * @return float
     */
    public function getDiscount();

    /**
     * Sets the invoice client notes.
     *
     * @param string $clientNotes The client notes.
     */
    public function setClientNotes($clientNotes);

    /**
     * Gets the invoice client notes.
     *
     * @return string
     */
    public function getClientNotes();

    /**
     * Sets the invoice terms.
     *
     * @param string $invoiceTerms The invoice terms.
     */
    public function setInvoiceTerms($invoiceTerms);

    /**
     * Sets the misc billing quantity.
     *
     * @param $quantity
     * @return mixed
     */
    public function setMiscBillingQuantity($quantity);

    /**
     * Sets the misc billing rate.
     *
     * @param $rate
     * @return mixed
     */
    public function setMiscBillingRate($rate);

    /**
     * Gets the invoice terms.
     *
     * @return string
     */
    public function getInvoiceTerms();

    /**
     * Adds a tax rate to the invoice.
     *
     * @param int    $taxRateID         The tax rate ID.
     * @param float  $historicTaxRate   The historic tax rate.
     * @param bool   $affectsEmployees  Indicates whether the tax rate affects employees.
     * @param bool   $affectsEquipments Indicates whether the tax rate affects equipments.
     * @param bool   $affectsFuels      Indicates whether the tax rate affects fuels.
     * @param bool   $affectsMaterials  Indicates whether the tax rate affects materials.
     * @param string $originalID        Indicates if the tax rate has an original tax line record ID.
     */
    public function addTaxRate($taxRateID, $historicTaxRate, $affectsEmployees, $affectsEquipments, $affectsFuels, $affectsMaterials, $originalID = '');

    /**
     * Gets the tax rates for the invoice.
     *
     * @return array
     */
    public function getTaxRates();

    /**
     * Sets the invoice total.
     *
     * @param float $invoiceTotal The invoice total.
     */
    public function setInvoiceTotal($invoiceTotal);

    /**
     * Gets the invoice total.
     *
     * @return float
     */
    public function getInvoiceTotal();

    /**
     * Sets the invoice total materials.
     *
     * @param float $materialsTotal
     */
    public function setInvoiceTotalMaterials($materialsTotal);

    /**
     * Gets the invoice total materials.
     *
     * @return float
     */
    public function getInvoiceTotalMaterials();

    /**
     * Sets the invoice equipment units total.
     *
     * @param float $equipmentUnitsTotal The equipment unit total.
     */
    public function setInvoiceTotalEquipmentUnits($equipmentUnitsTotal);

    /**
     * Sets the invoice total employee cost.
     *
     * @param $employeeTotal
     * @return mixed
     */
    public function setInvoiceTotalEmployees($employeeTotal);

    /**
     * Gets the invoice total employee cost.
     *
     * @return mixed
     */
    public function getInvoiceTotalEmployees();

    /**
     * Gets the invoice equipment units total.
     *
     * @return float
     */
    public function getInvoiceTotalEquipmentUnits();

    /**
     * Sets the invoice total fuels.
     *
     * @param float $fuelsTotal The fuels total.
     */
    public function setInvoiceTotalFuels($fuelsTotal);

    /**
     * Gets the invoice fuels total.
     *
     * @return float
     */
    public function getInvoiceTotalFuels();

    /**
     * Increments the invoice total.
     *
     * @param  float $incrementAmount The amount to increment.
     * @return float                  The new total amount.
     */
    public function incrementInvoiceTotal($incrementAmount);

    /**
     * Increments the invoice total material amount.
     *
     * @param  float $incrementAmount The amount to increment.
     * @return float                  The new total amount.
     */
    public function incrementInvoiceTotalMaterials($incrementAmount);

    /**
     * Increments the invoice taxable material amount.
     *
     * @param  float $incrementAmount The amount to increment.
     * @return float                  The new taxable amount.
     */
    public function incrementInvoiceTotalMaterialsTaxable($incrementAmount);

    /**
     * Increments the equipment units total.
     *
     * @param  float $incrementAmount The amount to increment.
     * @return float                  The new total amount.
     */
    public function incrementInvoiceEquipmentUnits($incrementAmount);


    /**
     * Increments the invoice taxable material amount.
     *
     * @param  float $incrementAmount The amount to increment.
     * @return float                  The new taxable amount.
     */
    public function incrementInvoiceEquipmentUnitsTaxable($incrementAmount);

    /**
     * Increments the inovice total fuels amount.
     *
     * @param  float $incrementAmount The amount to increment.
     * @return float                  The new total amount.
     */
    public function incrementInvoiceFuel($incrementAmount);

    /**
     * Increments the invoice taxable material amount.
     *
     * @param  float $incrementAmount The amount to increment.
     * @return float                  The new taxable amount.
     */
    public function incrementInvoiceFuelTaxable($incrementAmount);

    /**
     * Adds a work record to the invoice.
     *
     * @param int $workRecordID The work record ID.
     */
    public function addWorkRecord($workRecordID);

    /**
     * Removes a work record from the invoice.
     *
     * @param int $workRecordID The work record ID.
     */
    public function removeWorkRecord($workRecordID);

    /**
     * Sets the invoice total override.
     *
     * @param $totalOverride
     * @return mixed
     */
    public function setInvoiceTotalOverride($totalOverride);

    /**
     * Gets the invoice total override.
     *
     * @return mixed
     */
    public function getInvoiceTotalOverride();

    /**
     * Commits the invoice record.
     *
     * @return bool
     */
    public function commit();

    /**
     * Appends the invoice record instead of committing.
     *
     * @return mixed
     */
    public function append();

    /**
     * Removes a collection of tax lines from an invoice.
     *
     * @param array $taxLines
     * @return mixed
     */
    public function removeTaxLinesFromInvoice(array $taxLines);
}
