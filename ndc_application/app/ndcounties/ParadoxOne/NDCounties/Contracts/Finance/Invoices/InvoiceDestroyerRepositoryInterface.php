<?php namespace ParadoxOne\NDCounties\Contracts\Finance\Invoices;

interface InvoiceDestroyerRepositoryInterface
{
    /**
     * Deducts an array of work entries from their given invoices.
     *
     * @param array $workEntries
     * @return mixed
     */
    public function deduct(array $workEntries);
}
