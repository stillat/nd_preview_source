<?php namespace ParadoxOne\NDCounties\Contracts\Finance\Invoices;

interface InvoiceReaderRepositoryInterface
{
    const INVOICE_READ_MODE_OPEN   = 0;
    const INVOICE_READ_MODE_CLOSED = 1;
    const INVOICE_READ_MODE_ALL    = 2;

    /**
     * Retrieves the invoices.
     *
     * @param int $invoiceMode
     * @return mixed
     */
    public function getInvoices($invoiceMode = self::INVOICE_READ_MODE_ALL);

    /**
     * Gets the neighboring invoice records for a given invoice ID.
     *
     * @param $invoiceID
     * @return mixed
     */
    public function getNeighbors($invoiceID);

    public function getInvoiceByID($id);

    /**
     * Returns a collection of an invoices tax rates.
     *
     * @param $id
     * @return mixed
     */
    public function getInvoiceTaxRatesByID($id);

    public function search($searchTerms);

    /**
     * Filters invoices based on invoice numbers, pay to accounts and bill to accounts.
     *
     * Start and dates are optional. Set both to null to make the reader to ignore those values.
     *
     * @param      $invoiceNumberFilters
     * @param      $payToFilters
     * @param      $billToFilters
     * @param null $startDate
     * @param null $endDate
     * @return mixed
     */
    public function filterInvoices($invoiceNumberFilters, $payToFilters, $billToFilters, $startDate = null, $endDate = null);

    /**
     * Gets the invoice for a given work record.
     *
     * @param $workRecordID
     * @return mixed
     */
    public function getInvoiceForWorkRecord($workRecordID);
}
