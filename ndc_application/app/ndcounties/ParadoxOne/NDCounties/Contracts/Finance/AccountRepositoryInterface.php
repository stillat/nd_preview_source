<?php namespace ParadoxOne\NDCounties\Contracts\Finance;

interface AccountRepositoryInterface
{
    public function getAccounts();

    public function getAccountsWithTrashed();

    public function getModelByID($id);

    public function search($searchTerms);

    public function unTrash($financeAccounts);

}
