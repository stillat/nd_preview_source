<?php namespace ParadoxOne\NDCounties\Contracts\Finance;

interface SupplierRepositoryInterface
{
    public function getSuppliers();

    public function getModelByID($id);

    public function getNotes($supplierID);

    public function getNote($noteID);

    public function createNote($supplierID, array $noteInformation);
}
