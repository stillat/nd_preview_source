<?php namespace ParadoxOne\NDCounties\Contracts\Projects;

interface ProjectRepositoryInterface
{
    public function getModelById($id);

    public function getProjects();

    public function getAll();

    public function getAllWithTrashed();

    public function errors();

    public function search($searchTerms);

    public function unTrash($projects);

}
