<?php namespace ParadoxOne\NDCounties\Contracts\ExportingUtilities;

interface ExporterInterface
{
    /**
     * Sets the headers for the row data.
     *
     * @param array $headers
     * @return mixed
     */
    public function setHeaders(array $headers);

    /**
     * Adds a row of data to the exporter.
     *
     * @param array $row
     * @return mixed
     */
    public function addRow(array $row);

    /**
     * Exports the data.
     *
     * @return mixed
     */
    public function export();
}
