<?php namespace ParadoxOne\NDCounties\Contracts;

interface ActivityStreamRepositoryInterface
{
    /**
     * Logs an event into the audit log.
     *
     * @param $userID
     * @param $accountID
     * @param $action
     * @param $actionContext
     * @param $actionClass
     * @param $actionMeta
     * @param $performedOn
     * @return mixed
     */
    public function logActivity($userID, $accountID = null, $action = null, $actionContext = null, $actionClass = null, $actionMeta = null, $performedOn = null);

    /**
     * Gets the recent activity for the account.
     *
     * @param $accountID
     * @return mixed
     */
    public function getRecentActivity($accountID);

    /**
     * Resets the audit logs for the given account.
     *
     * @param $accountID
     * @return mixed
     */
    public function resetAccountLogs($accountID);

    /**
     * Resets all the account longs.
     *
     * @return mixed
     */
    public function resetAllAccountLogs();

    public function postNotification($account, $type, $message);
}
