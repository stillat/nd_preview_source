<?php namespace ParadoxOne\NDCounties\Contracts\Unions;

use ParadoxOne\NDCounties\Contracts\Properties\EquipmentUnitInterface;
use ParadoxOne\NDCounties\Contracts\WorkEntries\ConsumablePropertyHandlerInterface;

interface UnionPropertyHandlerEquipmentUnitInterface extends EquipmentUnitInterface, ConsumablePropertyHandlerInterface
{
}
