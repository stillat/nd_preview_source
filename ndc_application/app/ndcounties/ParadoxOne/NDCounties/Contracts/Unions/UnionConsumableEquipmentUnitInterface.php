<?php namespace ParadoxOne\NDCounties\Contracts\Unions;

use ParadoxOne\NDCounties\Contracts\Properties\EquipmentUnitInterface;
use ParadoxOne\NDCounties\Contracts\WorkEntries\ConsumableRecordInterface;

interface UnionConsumableEquipmentUnitInterface extends EquipmentUnitInterface, ConsumableRecordInterface
{
}
