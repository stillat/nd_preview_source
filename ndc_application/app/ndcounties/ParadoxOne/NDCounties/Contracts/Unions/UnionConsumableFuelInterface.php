<?php namespace ParadoxOne\NDCounties\Contracts\Unions;

use ParadoxOne\NDCounties\Contracts\Resources\FuelTypeInterface;
use ParadoxOne\NDCounties\Contracts\WorkEntries\ConsumableRecordInterface;

/**
 * This is a union contract between FuelTypeInterface and ConsumableRecordInterface
 * This is done so that we type-hint and require an object that implements both contracts.
 */
interface UnionConsumableFuelInterface extends FuelTypeInterface, ConsumableRecordInterface
{
}
