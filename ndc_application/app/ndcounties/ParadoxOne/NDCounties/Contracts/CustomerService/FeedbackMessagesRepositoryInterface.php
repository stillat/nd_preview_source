<?php namespace ParadoxOne\NDCounties\Contracts\CustomerService;

interface FeedbackMessagesRepositoryInterface
{
    public function getMessages();
}
