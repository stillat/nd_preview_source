<?php namespace ParadoxOne\NDCounties\Contracts\WorkEntries;

use ParadoxOne\NDCounties\Contracts\Resolvers\ConsumableTypeResolverInterface;

/**
 * The writer will be responsible for the actual creation of new work
 * log entries. This interface should define no other functions that
 * do not help to accomplish the writing of a record.
 */
interface WorkEntryWriterRepositoryInterface
{
    /**
     * Returns a new instance of the WorkEntryWriterRepositoryInterface implementation.
     *
     * @param ConsumableTypeResolverInterface $resolver The consumable type resolver implementation.
     * @return  ParadoxOne\NDCounties\Contracts\WorkEntries\WorkEntryWriterRepositoryInterface
     */
    public function __construct(ConsumableTypeResolverInterface $resolver,
                                ConsumableWriterRepositoryInterface $consumableWriter);

    /**
     * Sets the consumable writer implementation
     *
     * @param ParadoxOne\NDCounties\Contracts\WorkEntries\ConsumableWriterRepositoryInterface $consumableWriter
     */
    public function setConsumableWriter(ConsumableWriterRepositoryInterface $consumableWriter);

    /**
     * Sets the day the work entry is logged for.
     *
     * @param string $date The date of the work log entry.
     */
    public function setWorkDate($date);

    /**
     * Gets the work date the entry is logged for.
     *
     * @return string
     */
    public function getWorkDate();

    /**
     * Sets the employee ID.
     *
     * @param int $employeeID Must match an employee, or be 0.
     */
    public function setEmployee($employeeID);

    /**
     * Gets the employee ID
     *
     * @return int
     */
    public function getEmployee();

    /**
     * Sets whether a record should be excluded from reports.
     *
     * @param $excluded
     * @return mixed
     */
    public function setExcludedFromReports($excluded);

    /**
     * Gets whether or not a record should be excluded from reports.
     *
     * @return mixed
     */
    public function getExcludedFromReports();

    /**
     * Sets the hours worked for the work entry record.
     *
     * @param int $hoursWorked   The regular hours worked (non-overtime).
     * @param int $overtimeHours The overtime hours worked, if any.
     */
    public function setHoursWorked($hoursWorked, $overtimeHours);

    /**
     * Gets the regular employee hours worked.
     *
     * @return decimal
     */
    public function getRegularHoursWorked();

    /**
     * Gets the overtime employee hours worked.
     *
     * @return decimal
     */
    public function getOvertimeHoursWorked();

    /**
     * Sets the historic employee wage data.
     *
     * @param float $regularWage  The employee's regular wage.
     * @param float $overtimeWage The employee's overtime wage.
     */
    public function setHistoricWageData($regularWage, $overtimeWage);

    /**
     * Gets the employees regular wage
     *
     * @return float
     */
    public function getHistoricRegularWage();

    /**
     * Gets the employees overtime wage
     *
     * @return float
     */
    public function getHistoricOvertimeWage();

    /**
     * Sets the historic employee benefit data.
     *
     * @param float $regularFringe  The employee's hourly benefits (non-overtime).
     * @param float $overtimeFringe The employee's overtime hourly benefits.
     */
    public function setHistoricFringeBenefits($regularFringe, $overtimeFringe);

    /**
     * Gets the employees historic regular fringe benefits
     *
     * @return float
     */
    public function getHistoricRegularFringeBenefits();

    /**
     * Gets the employees historic overtime fringe benefits
     *
     * @return float
     */
    public function getHistoricOvertimeFringeBenefits();

    /**
     * Sets the activity id.
     *
     * @param int $activityID Must match an activity, or be 0.
     */
    public function setActivity($activityID);

    /**
     * Gets the activity ID
     *
     * @return int
     */
    public function getActivity();

    /**
     * Sets the project id.
     *
     * @param int $projectID Must match an project, or be 0.
     */
    public function setProject($projectID);

    /**
     * Gets the project ID
     *
     * @return int
     */
    public function getProject();

    /**
     * Sets the department id.
     *
     * @param int $departmentID Must match an department, or be 0.
     */
    public function setDepartment($departmentID);

    /**
     * Gets the department ID
     *
     * @return int
     */
    public function getDepartment();

    /**
     * Sets the district id.
     *
     * @param int $districtID Must match an district, or be 0.
     */
    public function setDistrict($districtID);

    /**
     * Gets the district ID
     *
     * @return int
     */
    public function getDistrict();

    /**
     * Sets the property id.
     *
     * @param int $propertyID Must match an property, or be 0.
     */
    public function setProperty($propertyID);

    public function setOrganizationProperty($propertyID);

    /**
     * Gets the property ID
     *
     * @return int
     */
    public function getProperty();

    public function getOrganizationProperty();

    /**
     * Sets the property context value.
     *
     * @param  int $propertyContext
     * @return void
     */
    public function setPropertyContext($propertyContext);

    public function setOrganizationPropertyContext($propertyContext);

    /**
     * Gets the property context value.
     *
     * @return int
     */
    public function getPropertyContext();

    public function getOrganizationPropertyContext();

    /**
     * Sets the activity description.
     *
     * @param string $description The activity description, used to offer explanations.
     */
    public function setActivityDescription($description);

    /**
     * Gets the activity description
     *
     * @return string
     */
    public function getActivityDescription();

    /**
     * Sets the miscellaneous billing quantity.
     *
     * @param numeric $billingQuantity
     */
    public function setMiscBillingQuantity($billingQuantity);

    /**
     * Gets the miscellaneous billing quantity.
     *
     * @return numeric
     */
    public function getMiscBillingQuantity();

    /**
     * Sets the miscellaneous billing measurement unit.
     *
     * @param int $unitID
     */
    public function setMiscBillingUnit($unitID);

    /**
     * Gets the miscellaneous billing measurement unit.
     *
     * @return int
     */
    public function getMiscBillingUnit();

    /**
     * Sets the miscellaneous billing rate.
     *
     * @param numeric $rate
     */
    public function setMiscBillingRate($rate);

    /**
     * Gets the miscellaneous billing rate.
     *
     * @return numeric
     */
    public function getMiscBillingRate();

    /**
     * Sets the miscellaneous billing description.
     *
     * @param string $description
     */
    public function setMiscBillingDescription($description);

    /**
     * Gets the miscellaneous billing description.
     *
     * @return string
     */
    public function getMiscBillingDescription();

    /**
     * Sets the property total for the work record.
     *
     * @param $propertyTotal
     * @return mixed
     */
    public function setPropertyTotal($propertyTotal);

    /**
     * Gets the property total.
     *
     * @return mixed
     */
    public function getPropertyTotal();

    /**
     * Attaches a consumable record to the work entry.
     *
     * @param  ConsumableRecordInterface $consumable [description]
     * @return void
     */
    public function attachConsumable(ConsumableRecordInterface $consumable);

    /**
     * Sets the invoice override total.
     *
     * @param $total
     * @return mixed
     */
    public function setInvoiceOverrideTotal($total);

    /**
     * Gets the invoice override total.
     *
     * @return mixed
     */
    public function getInvoiceOverrideTotal();

    /**
     * Returns an array of consumable records.
     *
     * @return array
     */
    public function getConsumables();

    /**
     * Commits the daily work entry.
     *
     * @return bool Indicates if the save was successful.
     */
    public function commit();

    /**
     * Returns a collection of error messages to the user.
     *
     * @return \Illuminate\Support\MessageBag
     */
    public function errors();

    /**
     * Sets whether or not the employee is taxed.
     *
     * @param $taxed
     * @return mixed
     */
    public function setEmployeeIsTaxed($taxed);

    /**
     * Indicates whether or not the employee is taxed.
     *
     * @return mixed]
     */
    public function getEmployeeIsTaxed();

    public function setIsCreditInventoryMode($mode);
}
