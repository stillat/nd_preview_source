<?php

namespace ParadoxOne\NDCounties\Contracts\WorkEntries;

interface WorkEntryBatchUpdateRepositoryInterface
{

    /**
     * Updates  the given records with the new date.
     *
     * @param $recordIDs
     * @param $newDate
     * @return mixed
     */
    public function batchUpdateDates($recordIDs, $newDate);

    public function batchUpdateActivities($recordIDs, $newActivity);

    public function batchMergeAndRemoveActivities($targetActivity, $oldActivities);

    public function batchConvertRoadsToGenericProperties($roadsToConvert);

    public function batchMergeAndRemoveDepartments($target, $old);

    public function batchMergeAndRemoveDistricts($target, $old);

    public function batchMergeAndRemoveProjects($target, $old);

    public function batchMergeAndRemoveEmployees($target, $old);

    public function batchMergeAndRemoveFuels($target, $old);

    public function batchMergeAndRemoveMaterials($target, $old);

    public function batchMergeAndRemoveGenericProperties($target, $old);

    public function batchMergeAndRemoveEquipmentUnits($target, $old);

    public function batchMergeAndRemoveRoads($target, $old);

    public function batchUpdateRoads($recordIDs, $newRoad);

    public function batchUpdateProject($recordIDs, $newProject);

    public function batchUpdateDistrict($recordIDs, $newDistrict);

    public function batchUpdateDepartment($recordIDs, $newDepartment);

    public function batchRemoveEmptyConsumableRecords();

    /**
     * Removes all unused records such as materials, projects, etc.
     *
     * @return mixed
     */
    public function batchCleanRemovedRecords();

}