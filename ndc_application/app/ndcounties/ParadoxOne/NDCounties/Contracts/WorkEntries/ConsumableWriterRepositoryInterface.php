<?php namespace ParadoxOne\NDCounties\Contracts\WorkEntries;

interface ConsumableWriterRepositoryInterface
{
    /**
     * Sets the consumable for the current write session.
     *
     * @param ConsumableRecordInterface $consumable
     * @return mixed
     */
    public function setConsumable(ConsumableRecordInterface $consumable);

    /**
     * Updates or creates a consumable record.
     *
     * @param bool $validateOnly
     * @return mixed
     */
    public function updateOrCreate($validateOnly = false);

    /**
     * Commits a new consumable record.
     *
     * @param bool $validateOnly
     * @return mixed
     */
    public function commit($validateOnly = false);

    /**
     * Sets the tax percentages that should be used when creating the consumable record.
     *
     * @param $employeeTaxPercentage
     * @param $equipmentTaxPercentage
     * @param $materialTaxPercentage
     * @param $fuelTaxPercentage
     * @return mixed
     */
    public function setHistoricTaxRatePercentagesFromInvoice($employeeTaxPercentage, $equipmentTaxPercentage, $materialTaxPercentage, $fuelTaxPercentage);
}
