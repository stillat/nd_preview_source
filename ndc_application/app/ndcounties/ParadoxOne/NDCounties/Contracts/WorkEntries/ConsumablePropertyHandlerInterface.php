<?php namespace ParadoxOne\NDCounties\Contracts\WorkEntries;

interface ConsumablePropertyHandlerInterface
{
    public function handle(ConsumableRecordInterface $consumableRecord, $validateOnly = false);

    public function errors();
}
