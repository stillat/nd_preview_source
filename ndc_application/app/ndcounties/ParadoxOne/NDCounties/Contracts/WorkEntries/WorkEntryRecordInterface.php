<?php namespace ParadoxOne\NDCounties\Contracts\WorkEntries;

/**
 * This interface should be used by any implementation for a single work entry record.
 * NEVER return an ORM object directly to a view or any plugin code. This interface ensures
 * that every piece of user-land code can access work entry information consistently.
 *
 * Interface WorkEntryRecordInterface
 *
 * @package ParadoxOne\NDCounties\Contracts\WorkEntries
 */
interface WorkEntryRecordInterface
{
    /**
     * Gets the raw data.
     *
     * @return mixed
     */
    public function getRawData();

    /**
     * Returns the record employee.
     *
     * @return mixed
     */
    public function getEmployee();

    /**
     * Indicates if the employee information was taxed.
     *
     * @return mixed
     */
    public function getEmployeeIsTaxed();

    /**
     * Returns the record activity.
     *
     * @return mixed
     */
    public function getActivity();

    /**
     * Returns the record district.
     *
     * @return mixed
     */
    public function getDistrict();

    /**
     * Returns the record project.
     *
     * @return mixed
     */
    public function getProject();

    /**
     * Returns the record equipment units.
     *
     * @return mixed
     */
    public function getEquipmentUnits();

    /**
     * Returns the record property.
     *
     * @return mixed
     */
    public function getProperty();

    /**
     * Indicates if the record property is an equipment unit.
     *
     * @return mixed
     */
    public function isEquipmentProperty();

    /**
     * Returns the record department.
     *
     * @return mixed
     */
    public function getDepartment();

    /**
     * Returns the record materials.
     *
     * @return mixed
     */
    public function getMaterials();

    /**
     * Returns the record fuels.
     *
     * @return mixed
     */
    public function getFuels();

    /**
     * Gets the misc billing information.
     *
     * @return mixed
     */
    public function getMiscBilling();

    /**
     * Returns the employee's historic wage information.
     *
     * @return mixed
     */
    public function getHistoricWageInformation();

    /**
     * Returns the record's totals.
     *
     * @return mixed
     */
    public function getRecordTotals();

    /**
     * Gets the record work date.
     *
     * @return mixed
     */
    public function getWorkDate();

    /**
     * Returns the record ID.
     *
     * @return int
     */
    public function getWorkID();

    /**
     * Returns the activity description.
     *
     * @return mixed
     */
    public function getActivityDescription();

    /**
     * Indicates if the record is excluded from reporting.
     *
     * @return mixed
     */
    public function isExcludedFromReports();
}
