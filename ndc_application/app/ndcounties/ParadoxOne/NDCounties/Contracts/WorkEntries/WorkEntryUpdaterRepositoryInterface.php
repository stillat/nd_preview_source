<?php namespace ParadoxOne\NDCounties\Contracts\WorkEntries;

interface WorkEntryUpdaterRepositoryInterface extends WorkEntryWriterRepositoryInterface
{
    /**
     * Indicate that a fuel should be removed.
     *
     * @param $fuelID
     * @return mixed
     */
    public function notifyFuelRemoval($fuelID);

    /**
     * Indicate that a material should be removed.
     *
     * @param $materialID
     * @return mixed
     */
    public function notifyMaterialRemoval($materialID);

    /**
     * Indicate that an equipment unit and all its fuels should be removed.
     *
     * @param $equipmentUnitID
     * @return mixed
     */
    public function notifyEquipmentUnitRemoval($equipmentUnitID);

    /**
     * Sets the work entry ID.
     *
     * @param $workEntryID
     * @return mixed
     */
    public function setWorkEntryID($workEntryID);

    /**
     * Gets the work entry ID.
     *
     * @return mixed
     */
    public function getWorkEntryID();
}
