<?php namespace ParadoxOne\NDCounties\Contracts\WorkEntries;

interface WorkEntryVisibilityRepositoryInterface
{
    /**
     * Hides the given records from reports.
     *
     * @param array $recordIDs
     * @return mixed
     */
    public function hideRecordsFromReports(array $recordIDs);

    /**
     * Shows the given records to reports.
     *
     * @param array $recordIDs
     * @return mixed
     */
    public function showRecordsToReports(array $recordIDs);
}
