<?php namespace ParadoxOne\NDCounties\Contracts\WorkEntries;

interface WorkEntryReaderInterface
{
    /**
     * Sets the pagination details.
     *
     * @param int $perPage
     * @param int $currentPage
     * @return mixed
     */
    public function setPagination($perPage, $currentPage);

    /**
     * Gets the reader used internally.
     *
     * @return \ParadoxOne\NDCounties\Contracts\WorkEntries\WorkEntryReaderRepositoryInterface
     */
    public function reader();

    /**
     * Returns the collection of records.
     *
     * @return mixed
     */
    public function getRecords();

    /**
     * Returns a measurement unit from a central store.
     *
     * @param $unitID
     * @return mixed
     */
    public function getMeasurementUnit($unitID);

    /**
     * Returns the total count of records.
     *
     * @return mixed
     */
    public function count();

    /**
     * Returns a single record.
     *
     * @param $id
     * @return mixed
     */
    public function getSingleRecord($id);

    /**
     * Gets the last saved record ID.
     *
     * @return mixed
     */
    public function getLastSavedRecordID();
}
