<?php namespace ParadoxOne\NDCounties\Contracts\WorkEntries;

interface WorkEntryDestroyerInterface
{
    /**
     * Destroy a work record.
     *
     * @param $workRecordID
     * @param $softDelete
     * @return mixed
     */
    public function destroy($workRecordID, $softDelete = false);

    public function mergeValidationErrors($errors);

    public function errors();
}
