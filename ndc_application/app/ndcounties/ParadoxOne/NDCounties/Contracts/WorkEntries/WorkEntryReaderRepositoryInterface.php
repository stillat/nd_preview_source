<?php namespace ParadoxOne\NDCounties\Contracts\WorkEntries;

interface WorkEntryReaderRepositoryInterface
{
    /**
     * Returns a collection of work records for a given invoice.
     *
     * @param $invoiceID
     * @return mixed
     */
    public function forInvoice($invoiceID);


    /**
     * Adds a column to the selection queue.
     *
     * @param  string $column
     * @return mixed
     */
    public function select($column);

    /**
     * Adds an aggregated column to the selection queue.
     *
     * @param $column  The aggregated column query. i.e., SUM(employee_total) as totalEmployeeCost
     * @param $name    The end name of the column. i.e., totalEmployeeCost
     * @param $default The default value to assign to the $name when there is no results.
     * @return mixed
     */
    public function selectAggregate($column, $name, $default);

    /**
     * Forces the reader to only return the records with the given ID.
     *
     * @param $ids
     * @return mixed
     */
    public function whereWorkEntriesID($ids);

    /**
     * Instructs the builder to paginate the results.
     *
     * @param int $currentPage
     * @param int $perPage
     * @return mixed
     */
    public function paginate($currentPage = 1, $perPage = 10);

    /**
     * Adds a where condition to to work reader.
     *
     * @param $column
     * @param $operator
     * @param $condition
     * @return mixed
     */
    public function where($column, $operator, $condition);

    /**
     * Add a "where in" clause to the work reader.
     *
     * @param  string $column
     * @param  mixed  $values
     * @param  string $boolean
     * @param  bool   $not
     * @return $this
     */
    public function whereIn($column, $values, $boolean = 'and', $not = false);

    /**
     * Adds an "or where" condition to the work reader.
     *
     * @param $column
     * @param $operator
     * @param $condition
     * @return mixed
     */
    public function orWhere($column, $operator, $condition);

    /**
     * Adds a where between condition to the work reader.
     *
     * @param       $column
     * @param array $values
     * @return mixed
     */
    public function whereBetween($column, array $values);

    /**
     * Adds a "where year" condition to the work reader.
     *
     * @throws Exception
     * @param $year
     * @return mixed
     */
    public function whereYear($year);

    /**
     * Orders the results of the work reader.
     *
     * @param $column
     * @param $sortMethod
     * @return mixed
     */
    public function orderBy($column, $sortMethod);

    /**
     * Adds a "group by" constraint to the work reader.
     *
     * @param $column
     * @return mixed
     */
    public function groupBy($column);

    /**
     * Sets the date range for the work reader.
     *
     * @param $startDate
     * @param $endDate
     * @return mixed
     */
    public function setDateRange($startDate, $endDate);

    /**
     * Indicates that activity data should be collected.
     *
     * @return WorkEntryReaderRepositoryInterface
     */
    public function withActivities();

    /**
     * Indicates that department data should be collected.
     *
     * @return WorkEntryReaderRepositoryInterface
     */
    public function withDepartments();

    /**
     * Indicates that district data should be collected.
     *
     * @return WorkEntryReaderRepositoryInterface
     */
    public function withDistricts();

    /**
     * Indicates that project data should be collected.
     *
     * @return WorkEntryReaderRepositoryInterface
     */
    public function withProjects();

    /**
     * Indicates that property data should be collected.
     *
     * @return WorkEntryReaderRepositoryInterface
     */
    public function withProperties();

    /**
     * Indicates that employee data should be collected.
     *
     * @return WorkEntryReaderRepositoryInterface
     */
    public function withEmployees();

    /**
     * Indicates that all default data should be collected.
     *
     * @return WorkEntryReaderRepositoryInterface
     */
    public function withDefaults();

    /**
     * Returns the work records.
     *
     * @return mixed
     */
    public function getRecords();

    /**
     * Gets the highest record fuel count.
     *
     * @return int
     */
    public function getHighestFuelCount();

    /**
     * Gets the highest record material count.
     *
     * @return int
     */
    public function getHighestMaterialCount();

    /**
     * Gets the consumable records.
     *
     * @return mixed
     */
    public function getOrganizedConsumableRecords();

    /**
     * Returns a multi-dimensional array of odometer readings.
     *
     * @return mixed
     */
    public function getOdometerReadings();

    /**
     * Returns the total count of work records for pagination.
     *
     * @return mixed
     */
    public function getCount();

    /**
     * Returns any aggregated information.
     *
     * @return mixed
     */
    public function getAggregate();


    /**
     * Returns a list of all years there are work records for.
     *
     * @return mixed
     */
    public function getWorkYears();

    /**
     * Returns the work record IDs discovered when gathering records.
     *
     * @return mixed
     */
    public function getWorkRecordIDs();

    /**
     * Returns the unpacked work record IDs. Will return all the work record IDs
     * even if group by clauses were used.
     *
     * @return mixed
     */
    public function getUnpackedWorkRecordIDs();

    /**
     * Returns the next and previous ID's for a given record.
     *
     * @param $id
     * @return mixed
     */
    public function getNextAndPreviousForRecord($id);

    /**
     * Returns a measurement unit from the internal cache.
     *
     * @param $id
     * @return mixed
     */
    public function getMeasurementUnitFromCache($id);

    /**
     * Sets if the reader is in report mode.
     *
     * @param $isReportMode
     * @return mixed
     */
    public function setReportMode($isReportMode);

    /**
     * Sets the reporting sort settings.
     *
     * If no sorting is supplied, implementations should apply the users
     * default work sort settings.
     *
     * @param $reportSortSettings
     * @return mixed
     */
    public function setReportSortSettings($reportSortSettings);

    /**
     * Gets the report record totals.
     *
     * @return mixed
     */
    public function getReportTotals();

    /**
     * Sets the total query for the grand totals.
     *
     * @param $query
     * @return mixed
     */
    public function setReportRecordTotalQuery($query);

    /**
     * Gets the property IDs returned.
     *
     * @return mixed
     */
    public function getProperties();

    /**
     * Gets the activity IDs returned.
     *
     * @return mixed
     */
    public function getActivities();

    /**
     * Gets the employee IDs returned.
     *
     * @return mixed
     */
    public function getEmployees();

    /**
     * Gets the department IDs returned.
     *
     * @return mixed
     */
    public function getDepartments();

    /**
     * Gets the district IDs returned.
     *
     * @return mixed
     */
    public function getDistricts();

    /**
     * Gets the project IDs returned.
     *
     * @return mixed
     */
    public function getProjects();

    /**
     * Gets the equipment unit IDs returned.
     *
     * @return mixed
     */
    public function getEquipmentUnits();

    /**
     * Enables the consumable override.
     *
     * @return mixed
     */
    public function enableConsumableOverride();

    /**
     * Disables the consumable override.
     *
     * @return mixed
     */
    public function disableConsumableOverride();

    /**
     * Sets the consumable record grouping.
     *
     * @param $columns
     * @return mixed
     */
    public function groupConsumableRecord($columns);

    /**
     * Sets a consumable override restriction mode.
     *
     * @param       $type
     * @param array $restriction
     * @return mixed
     */
    public function setConsumableRestrictions($type, array $restriction);

    /**
     * Adds a having clause to the query.
     *
     * @param $column
     * @param $operator
     * @param $value
     * @return mixed
     */
    public function having($column, $operator, $value);

    /**
     * Intelligently returns the proper report header.
     *
     * @return mixed
     */
    public function getReportPropertyHeader();
}
