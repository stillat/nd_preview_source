<?php namespace ParadoxOne\NDCounties\Contracts\WorkEntries;

interface ConsumableRecordInterface
{
    /**
     * Sets the record ID.
     *
     * @param $id
     * @return mixed
     */
    public function setRecordID($id);

    /**
     * Gets the record ID.
     *
     * @return mixed
     */
    public function getRecordID();


    /**
     * Sets the consumable type.
     *
     * @param string $type
     */
    public function setConsumableType($type);

    /**
     * Returns the consumable type
     *
     * @return string
     */
    public function getConsumableType();

    /**
     * Sets the consumable record id.
     *
     * @param int $id
     */
    public function setConsumableID($id);

    /**
     * Returns the consumable id.
     *
     * @return int
     */
    public function getConsumableID();

    /**
     * Sets the associated type.
     *
     * @param string $type
     */
    public function setAssociatedType($type);

    /**
     * Returns the associated type.
     *
     * @return int
     */
    public function getAssociatedType();

    /**
     * Sets the associated record id.
     *
     * @param string $id
     */
    public function setAssociatedID($id);

    /**
     * Returns the associated ID.
     *
     * @return int
     */
    public function getAssociatedID();

    /**
     * Indicates that there is no associated records
     *
     * @return  void
     */
    public function nothingAssociated();

    /**
     * Sets the work entry ID.
     *
     * @param int $id The id of the work entry record.
     */
    public function setWorkEntryID($id);

    /**
     * Returns the work entry id.
     *
     * @return int
     */
    public function getWorkEntryID();

    /**
     * Sets the measurement unit ID.
     *
     * @param int $id The ID of the measurement unit record.
     */
    public function setMeasurementUnit($id);

    /**
     * Returns the measurement unit.
     *
     * @return int
     */
    public function getMeasurementUnit();

    /**
     * Sets the total cost for the record.
     *
     * This is calculated with: total_quantiy * historic_cost
     *
     * @param float $cost Must be pre-calculated.
     */
    public function setTotalCost($cost);

    /**
     * Returns the total consumable cost.
     *
     * @return int
     */
    public function getTotalCost();

    /**
     * Sets the historic cost for the consumable.
     *
     * @param float $cost
     */
    public function setHistoricCost($cost);

    /**
     * Returns the historic consumable cost.
     *
     * @return float
     */
    public function getHistoricCost();

    /**
     * Sets the quantiy of the consumable used.
     *
     * This will be expressed in quantity per measurement unit. For
     * example, "1 Gallon(s)" or "2 Each".
     *
     * @param float $quantity
     */
    public function setQuantity($quantity);

    /**
     * Returns the consumable quantity used.
     *
     * @return float
     */
    public function getQuantity();

    /**
     * Sets an additional property on the consumable record.
     *
     * @param mixed $property The property name
     * @param mixed $value    The property value
     */
    public function setProperty($property, $value);

    /**
     * Gets a property value on the consumable record.
     *
     * @param  mixed $property The name of the property
     * @param  mixed $default  The default property value
     * @return mixed           The value of the property
     */
    public function getProperty($property, $default = null);

    /**
     * Returns an array of the consumable's property values.
     *
     * @return array
     */
    public function getProperties();

    /**
     * Sets whether or not the consumable record is taxed.
     *
     * @param bool $taxed Whether or not the record is taxed.
     */
    public function setTaxed($taxed);

    /**
     * Indicates whether or not a record is taxed.
     *
     * @return boolean Tax status.
     */
    public function isTaxed();

    /**
     * Sets the binding record.
     *
     * @param $bindingRecord
     * @return mixed
     */
    public function setBindingRecord($bindingRecord);

    /**
     * Gets the binding record.
     *
     * @return mixed
     */
    public function getBindingRecord();
}
