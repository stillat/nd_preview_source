<?php namespace ParadoxOne\NDCounties\Contracts;

interface CountyMetaRepositoryInterface
{
    /**
     * Updates the county's meta information.
     *
     * @param $highestFuelCount
     * @param $highestMaterialCount
     * @param $highestEquipmentCount
     * @return mixed
     */
    public function updateMetaInformation($highestFuelCount, $highestMaterialCount, $highestEquipmentCount);

    /**
     * Get the actual meta information from the backing store.
     *
     * @return mixed
     */
    public function getActualMetaInformation();

    /**
     * Returns the county's meta information.
     *
     * @return mixed
     */
    public function getMetaInformation();
}
