<?php namespace ParadoxOne\NDCounties\Contracts;

interface UserRepositoryInterface
{
    /**
     * Returns an array of account and tenants.
     *
     * @return mixed
     */
    public function getAccounts($userID);

    public function getUsersIn($requestedUsers);

    public function getUsers();

    public function getLastCreatedUser();

    public function addSettingsToAccount($userID);

    public function resetPassword($user, $newPassword);

    public function updateUserSettings($userID, array $settingsData);
}
