<?php namespace ParadoxOne\NDCounties\Contracts;

interface GenderRepositoryInterface
{
    public function getGenders();
}
