<?php namespace ParadoxOne\NDCounties\Contracts;

interface UnitRepositoryInterface
{
    /**
     * Returns a collection of units.
     *
     * @return array $units
     */
    public function getUnits();

    public function getModelById($id);

    public function search($searchTerms);

    public function whereIn($measurementUnits);
}
