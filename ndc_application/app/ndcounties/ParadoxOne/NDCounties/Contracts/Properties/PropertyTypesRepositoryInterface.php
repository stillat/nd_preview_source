<?php namespace ParadoxOne\NDCounties\Contracts\Properties;

interface PropertyTypesRepositoryInterface
{
    public function getPropertyTypes();

    public function getPropertyTypesForReportScope();

    public function getModelById($id);
}
