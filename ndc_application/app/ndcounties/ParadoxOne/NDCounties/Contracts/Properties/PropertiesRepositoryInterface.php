<?php namespace ParadoxOne\NDCounties\Contracts\Properties;

interface PropertiesRepositoryInterface
{
    public function getModelById($id);

    public function getProperties();

    public function getAll();

    public function getAllWithTrashed();

    public function getPropertiesWithTrashed();

    public function getProperty($id);

    public function getPropertiesIn(array $properties);

    public function search($searchTerms, $limitType = 0);

    public function searchGeneric($searchTerms);
    public function searchRoads($searchTerms);
    public function searchEquipmentUnits($searchTerms);

    public function errors();

    public function attachChildProperty($parentPropertyId, $childPropertyId);

    public function detachChildProperty($parentPropertyId, $removeChildProperty);

    public function createChildProperty($parentPropertyId, array $childPropertyData);

    public function updateChildProperty($childPropertyId, array $updateInformation);

    public function removeChildProperty(array $removeContext);

    public function detachChildFromAll($childPropertyId);

    public function removeAllChildrenFromParent($parentPropertyId);

    /**
     * Un-nests a global property from it's parent property.
     *
     * @param $parentPropertyID
     * @param $nestedPropertyID
     * @return mixed
     */
    public function unNestGlobalProperty($parentPropertyID, $nestedPropertyID);

    /**
     * Un-nests an exclusive property and makes it into a global property.
     *
     * @param $nestedPropertyID
     * @return mixed
     */
    public function unNestExclusiveProperty($nestedPropertyID);

    public function unTrash($properties);

}
