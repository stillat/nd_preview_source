<?php namespace ParadoxOne\NDCounties\Contracts\Properties;

use Stillat\Database\Repositories\RepositoryInterface;

interface PropertyCategoriesRepositoryInterface extends RepositoryInterface
{
    public function getCategories();

    public function getCategory($id);
}
