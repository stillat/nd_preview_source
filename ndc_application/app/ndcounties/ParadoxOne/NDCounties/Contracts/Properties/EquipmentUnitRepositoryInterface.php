<?php namespace ParadoxOne\NDCounties\Contracts\Properties;

interface EquipmentUnitRepositoryInterface
{
    public function getEquipmentUnits();

    public function getModelById($id);

    public function errors();

    public function getAll();

    public function search($searchTerms);
}
