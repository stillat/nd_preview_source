<?php namespace ParadoxOne\NDCounties\Contracts\Properties\Roads;

interface SurfaceTypeRepositoryInterface
{
    public function search($searchTerms);

    public function getModelById($id);

    public function getSurfaceTypes();

    public function addSurfaceToRoad($surfaceID, $roadID);

    public function removeSurfaceFromRoad($road);

    public function getSurfaceTypesWhereIn(array $ids);
}
