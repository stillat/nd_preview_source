<?php namespace ParadoxOne\NDCounties\Contracts\Properties\Roads;

use ParadoxOne\NDCounties\Contracts\RecordableInterface;

interface WorkRecordRepositoryInterface extends RecordableInterface
{
    /**
     * Determines if the work record is valid.
     *
     * @param \ParadoxOne\NDCounties\Contracts\Properties\Roads\data $data
     * @return mixed
     */
    public function isValid($data);
}
