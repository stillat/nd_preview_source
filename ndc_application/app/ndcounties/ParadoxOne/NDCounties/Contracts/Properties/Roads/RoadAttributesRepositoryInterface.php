<?php namespace ParadoxOne\NDCounties\Contracts\Properties\Roads;

interface RoadAttributesRepositoryInterface
{
    public function setRoad($roadID);

    public function errors();
}
