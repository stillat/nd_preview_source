<?php namespace ParadoxOne\NDCounties\Contracts;

interface RecordableInterface
{
    public function getRecordByID($recordID);

    public function getRecordByWorkID($workID);
}
