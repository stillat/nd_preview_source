<?php namespace ParadoxOne\NDCounties\Contracts;

interface StreamBuilderInterface
{
    /**
     * Sets the streams data.
     *
     * @param $data
     * @return mixed
     */
    public function setStreamData($data);

    /**
     * Gets the streams data.
     *
     * @return mixed
     */
    public function getStreamData();

    /**
     * Gets the streams string representation.
     *
     * @return mixed
     */
    public function getStream();

    /**
     * Formats an item of the stream.
     *
     * @param $item
     * @return mixed
     */
    public function formatItem($item);

    /**
     * Alias of getStream();
     *
     * @return mixed
     */
    public function __toString();
}
