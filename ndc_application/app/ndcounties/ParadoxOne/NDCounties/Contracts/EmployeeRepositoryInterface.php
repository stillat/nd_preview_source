<?php namespace ParadoxOne\NDCounties\Contracts;

interface EmployeeRepositoryInterface
{
    public function errors();

    public function getAll();

    public function getAllWithTrashed();

    public function getModelByID($id);

    public function search($searchTerms);

    public function unTrash($employees);

}
