<?php namespace ParadoxOne\NDCounties\Contracts;

interface SystemInformationRepositoryInterface
{
    /**
     * Gets the database version for a given tenant account.
     *
     * @return mixed
     */
    public function getDatabaseVersion();

    /**
     * Updates the system information for a given tenant.
     *
     * @param array $systemInformation
     * @return mixed
     */
    public function updateSystemInformation(array $systemInformation);

    public function getSystemInformation();

    public function bringDatabaseVersionCurrent();
}
