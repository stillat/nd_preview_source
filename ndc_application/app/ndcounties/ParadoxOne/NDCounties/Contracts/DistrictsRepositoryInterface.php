<?php namespace ParadoxOne\NDCounties\Contracts;

interface DistrictsRepositoryInterface
{
    public function getDistricts();

    public function getAll();

    public function getAllWithTrashed();

    public function getModelById($id);

    public function errors();

    public function search($searchTerms);

    public function unTrash($districts);

}
