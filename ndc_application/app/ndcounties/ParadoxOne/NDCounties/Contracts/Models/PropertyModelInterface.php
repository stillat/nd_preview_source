<?php namespace ParadoxOne\NDCounties\Contracts\Models;

interface PropertyModelInterface
{
    /**
     * Returns a collection of children properties.
     *
     * @return array
     */
    public function children();
}
