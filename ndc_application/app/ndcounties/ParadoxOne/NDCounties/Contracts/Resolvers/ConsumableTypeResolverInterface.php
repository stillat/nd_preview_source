<?php namespace ParadoxOne\NDCounties\Contracts\Resolvers;

/**
 * The ConsumableTypeResolverInterface needs to be used in conjunction with the
 * Work Entry DTS (see the corresponding RFC: Work Log/Entry and DTS, located at
 * https://bitbucket.org/paradoxonellc/nd-counties/wiki/Developers/RFC/Work%20Logs.md)
 *
 * When there are updates to the core system (like the addition of recognized consumable types), this contract
 * needs to be updated with new `isXXX` functions.
 *
 * @since 2.0 Albacore
 */
interface ConsumableTypeResolverInterface
{
    /**
     * Resolves a consumable type
     *
     * @param  int $consumableType The consumable type
     * @return string                 The full qualified class name of the consumable type.
     */
    public function resolve($consumableType);

    /**
     * Resolves a consumable type's property handler.
     *
     * @param  int $consumableType The consumable type
     * @return string                  The fully qualified class name of the consumable type property handler.
     */
    public function resolvePropertyHandler($consumableType);

    /**
     * Returns a new instance of a consumable type, based on the types ID passed in
     *
     * @param int $consumableType
     */
    public function makeInstance($consumableType);

    /**
     * Returns a new instance of a consumable type property resolver, based on the types ID
     *
     * @param  int $consumableType
     */
    public function makePropertyHandlerInstance($consumableType);

    /**
     * Returns an array of all consumable type identifiers
     *
     * @return array
     */
    public function getTypeIdentifiers();

    /**
     * Determines if a $consumableType is a material
     *
     * @param  string|int $consumableType The class name or type ID of the consumable
     * @return boolean                     Returns true if the $consumableType is a material, false if not
     */
    public function isMaterial($consumableType);

    /**
     * Determines if a $consumableType is a fuel
     *
     * @param  string|int $consumableType The class name or type ID of the consumable
     * @return boolean                     Returns true if the $consumableType is a fuel, false if not
     */
    public function isFuel($consumableType);

    /**
     * Determines if a $consumableType is an equipment unit
     *
     * @param  string|int $consumableType The class name or type ID of the consumable
     * @return boolean                     Returns true if the $consumableType is an equipment unit, false if not
     */
    public function isEquipmentUnit($consumableType);
}
