<?php namespace ParadoxOne\NDCounties\Contracts\Resolvers;

interface MaterialHydrationInterface
{
    /**
     * Hydrates a collection of fuel consumables from input
     *
     * @param  array $input [description]
     * @return array
     */
    public function hydrateMaterialConsumables(array $input);
}
