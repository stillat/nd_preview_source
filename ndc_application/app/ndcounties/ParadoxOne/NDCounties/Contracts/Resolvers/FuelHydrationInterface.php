<?php namespace ParadoxOne\NDCounties\Contracts\Resolvers;

interface FuelHydrationInterface
{
    /**
     * Hydrates a collection of fuel consumables from input
     *
     * @param  array $input [description]
     * @return array
     */
    public function hydrateFuelConsumables(array $input);
}
