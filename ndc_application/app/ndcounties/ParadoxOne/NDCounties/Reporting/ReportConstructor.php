<?php namespace ParadoxOne\NDCounties\Reporting;

use Carbon\Carbon;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use ParadoxOne\NDCounties\Contracts\Reporting\DirectReportInterface;
use ParadoxOne\NDCounties\Contracts\Reporting\ReportConstructorInterface;
use ParadoxOne\NDCounties\Contracts\Reporting\ReportEquationsRepositoryInterface;
use ParadoxOne\NDCounties\Contracts\Reporting\ReportMetaDefinitionRepositoryInterface;
use ParadoxOne\NDCounties\Contracts\Reporting\SRL\SRLPrinterInterface;
use ParadoxOne\NDCounties\Contracts\WorkEntries\WorkEntryReaderRepositoryInterface;
use ParadoxOne\NDCounties\DataRedactionServices\StandardEngineFactory;
use ParadoxOne\NDCounties\Reporting\SRL\Commands\EvaluateCommand;

class ReportConstructor implements ReportConstructorInterface
{
    use \ParadoxOne\NDCounties\Reporting\Builders\SpecialOrgReflectionTrait;
    use \ParadoxOne\NDCounties\Reporting\Builders\RecordRestrictionTrait;
    use \ParadoxOne\NDCounties\Reporting\Builders\NormalModeBuilderTrait;
    use \ParadoxOne\NDCounties\Reporting\Builders\ConsumableModeBuilderTrait;
    use \ParadoxOne\NDCounties\Reporting\Builders\PropertyLimiterBuilderTrait;
    use \ParadoxOne\NDCounties\Reporting\Builders\ConstraintBuilderTrait;
    use \ParadoxOne\NDCounties\Reporting\Builders\SpecialModeBuilderTrait;
    use \ParadoxOne\NDCounties\Reporting\SRL\PreparationTools\PreparationHandlerTrait;

    /**
     * The token to indicate that the body extends the header.
     *
     * @var string
     */
    const SRL_BODY_EXTEND_HEADER = '<!--EXTENDS HEADER-->';

    const SRL_BODY_NO_ROW_WRAP = '<!--ROW WRAP OFF-->';
    const SRL_BODY_ROW_WRAP_ON = '<!--ROW WRAP ON-->';

    const SRL_INHERIT_HEADER = '<!--INHERIT HEADER-->';

    const SRL_EVALUATE_COMMAND_REGEX = '/EVALUATE\(.*?\)/';

    const SRL_FOOTER_REPLACE_TOTALS = '<!--GRAND TOTALS-->';

    const SRL_DEFAULT_REPORT_RECORD_QUERY = 'TOTAL(employee.cost + fuel.total + material.total + property.total + billing.total + equipment.total.cost)';

    /**
     * The RDM repository implementation.
     *
     * @var \ParadoxOne\NDCounties\Contracts\Reporting\ReportMetaDefinitionRepositoryInterface
     */
    protected $rdmRepository = null;

    /**
     * The RMD entry.
     *
     * @var null
     */
    protected $reportMetaDefinitionEntry = null;

    /**
     * The SRL Header.
     *
     * @var string
     */
    protected $srlHeader = '';

    /**
     * The SRL Body.
     *
     * @var string
     */
    protected $srlBody = '';

    /**
     * The formulas for the report.
     *
     * @var array
     */
    protected $formulas = [];

    /**
     * The SRL Printer implementation.
     *
     * @var \ParadoxOne\NDCounties\Contracts\Reporting\SRL\SRLPrinterInterface
     */
    protected $srlPrinter = null;

    /**
     * The SRL Printer implementation for headers.
     *
     * @var \ParadoxOne\NDCounties\Contracts\Reporting\SRL\SRLPrinterInterface
     */
    protected $srlHeaderPrinter = null;

    /**
     * The SRL Printer implementation for footers.
     *
     * @var \ParadoxOne\NDCounties\Contracts\Reporting\SRL\SRLPrinterInterface
     */
    protected $srlFooterPrinter = null;

    /**
     * Sets the report record total query.
     *
     * @var mixed
     */
    protected $reportRecordQuery = null;

    /**
     * The equation repository implementation.
     *
     * @var \ParadoxOne\NDCounties\Contracts\Reporting\ReportEquationsRepositoryInterface
     */
    protected $equationRepository = null;

    protected $workReaderReference = null;

    protected $organizationStrategy = null;

    protected $notAvailableRecord = null;

    protected $formatter = null;

    protected $redactor = null;

    /**
     * The evaluate command.
     *
     * @var \ParadoxOne\NDCounties\Reporting\SRL\Commands\EvaluateCommand
     */
    protected $evaluateCommand = null;

    /**
     * The report grouping column name.
     *
     * @var string
     */
    protected $reportGroupingColumn = '';

    /**
     * More grouping columns for advanced usage.
     *
     * @var array
     */
    protected $advancedGroupingColumns = [];

    /**
     * The values of the first group in a special grouping strategy.
     *
     * @var array
     */
    protected $specialGroupingFirstGroupValues = [];

    /**
     * The values of the second group in a special grouping strategy.
     *
     * @var array
     */
    protected $specialGroupingSecondGroupValues = [];

    /**
     * The name of the first group in a special grouping strategy.
     *
     * @var string
     */
    protected $specialGroupingFirstGroupName = '';

    /**
     * The name of the second group in a special grouping strategy.
     *
     * @var string
     */
    protected $specialGroupingSecondGroupName = '';

    /**
     * @var \ParadoxOne\NDCounties\Contracts\Reporting\DirectReportInterface
     */
    protected $directReportProvider = null;

    /**
     * The filters that are going to applied to the report.
     *
     * @var array
     */
    protected $reportingFilters = [];

    protected $specialGroupColumnName = '';

    protected $defaultTotalBlock = '';

    protected $internalEngineMode = 0;

    public function __construct(ReportMetaDefinitionRepositoryInterface $rdmRepository, ReportEquationsRepositoryInterface $equationRepository)
    {
        $this->rdmRepository = $rdmRepository;

        $this->evaluateCommand = new EvaluateCommand;

        $this->equationRepository = $equationRepository;

        $this->notAvailableRecord       = new \stdClass();
        $this->notAvailableRecord->code = 'NA';
        $this->notAvailableRecord->name = 'NA';
    }

    /**
     * Sets the internal reporting engine mode.
     *
     * @param $mode
     * @return mixed
     */
    public function setInternalMode($mode)
    {
        $this->internalEngineMode = $mode;
    }

    /**
     * Gets the internal reporting engine mode.
     *
     * @return mixed
     */
    public function getInternalMode()
    {
        return $this->internalEngineMode;
    }

    /**
     * Sets the SRL Printer implementation.
     *
     * @param SRLPrinterInterface $printer
     */
    public function setPrinter(SRLPrinterInterface $printer)
    {
        $this->srlPrinter = $printer;
    }

    /**
     * Sets the SRL Header Printer implementation.
     *
     * @param SRLPrinterInterface $printer
     */
    public function setHeaderPrinter(SRLPrinterInterface $printer)
    {
        $this->srlHeaderPrinter = $printer;
    }

    /**
     * Sets the SRL Footer Printer implementation.
     *
     * @param SRLPrinterInterface $printer
     */
    public function setFooterPrinter(SRLPrinterInterface $printer)
    {
        $this->srlFooterPrinter = $printer;
    }

    /**
     * Refreshes all SRL Printers.
     *
     * @return void
     */
    public function refreshPrinters()
    {
        $this->srlPrinter->setSource($this->getRawSRLBody());
        $this->srlHeaderPrinter->setSource($this->getSRLHeader());
        $this->srlFooterPrinter->setSource(str_replace(self::SRL_FOOTER_REPLACE_TOTALS, $this->defaultTotalBlock,
            $this->getSRLFooter()));
    }

    /**
     * Gets the SRL Printer implementation.
     *
     * @return SRLPrinterInterface
     */
    public function getPrinter()
    {
        return $this->srlPrinter;
    }

    /**
     * Gets the SRL Header Printer implementation.
     *
     * @return SRLPrinterInterface
     */
    public function getHeaderPrinter()
    {
        return $this->srlHeaderPrinter;
    }

    /**
     * Gets the SRL Footer Printer implementation.
     *
     * @return SRLPrinterInterface
     */
    public function getFooterPrinter()
    {
        return $this->srlFooterPrinter;
    }

    /**
     * Sets the meta definition entry record.
     *
     * @param $entryID
     *
     * @return mixed
     * @throws \Exception
     */
    public function setMetaDefinitionEntry($entryID)
    {
        if ($this->srlPrinter == null || $this->srlHeaderPrinter == null) {
            throw new \Exception('Set an SRL Printer before loading meta definitions');
        }

        $this->reportMetaDefinitionEntry = $this->rdmRepository->getDefinition($entryID);

        $this->srlHeader = $this->reportMetaDefinitionEntry->srl_header;

        if (strpos($this->reportMetaDefinitionEntry->srl_body, self::SRL_BODY_EXTEND_HEADER) !== false) {
            $this->srlBody =
                str_replace(self::SRL_BODY_EXTEND_HEADER, $this->srlHeader, $this->reportMetaDefinitionEntry->srl_body);
            $this->srlPrinter->setOptions(['bodyExtendsHeader' => true]);
        } else {
            $this->srlBody = $this->reportMetaDefinitionEntry->srl_body;

            if (strpos($this->srlBody, self::SRL_INHERIT_HEADER) !== false) {
                $this->srlBody = str_replace(self::SRL_INHERIT_HEADER, $this->srlHeader, $this->srlBody);
                $this->srlPrinter->setOptions($this->srlPrinter->getOptions() + ['bodyExtendsHeader' => true]);
            }
        }

        if (strpos($this->srlBody, self::SRL_BODY_NO_ROW_WRAP) !== false) {
            $this->srlPrinter->setOptions($this->srlPrinter->getOptions() + ['noRowWrap' => true]);
        }

        if (strpos($this->srlHeader, self::SRL_BODY_NO_ROW_WRAP) !== false) {
            $this->srlHeaderPrinter->setOptions($this->srlHeaderPrinter->getOptions() + ['noRowWrap' => true]);
        }

        if (strpos($this->reportMetaDefinitionEntry->srl_footer, self::SRL_BODY_NO_ROW_WRAP) !== false) {
            $this->srlFooterPrinter->setOptions($this->srlFooterPrinter->getOptions() + ['noRowWrap' => true]);
        }

        if (strpos($this->srlBody, self::SRL_BODY_ROW_WRAP_ON) !== false) {
            $options = $this->srlPrinter->getOptions();
            unset($options['noRowWrap']);
            $this->srlPrinter->setOptions($options);
            unset($options);
        }

        $settings = json_decode($this->reportMetaDefinitionEntry->getSettings());
        if (property_exists($settings, 'directProvider')) {
            $providerClass = $settings->directProvider;
            if (class_exists($providerClass)) {
                $this->directReportProvider = App::make($providerClass);
                $this->directReportProvider->setReportConstructor($this);
            }
        }

        $this->refreshPrinters();
    }

    /**
     * Gets the meta definition entry used by the constructor.
     *
     * @return mixed
     */
    public function getMetaDefinitionEntry()
    {
        return $this->reportMetaDefinitionEntry;
    }

    /**
     * Returns a collection of formulas used by the constructor.
     *
     * @return mixed
     */
    public function getFormulas()
    {
        // TODO: Implement getFormulas() method.
    }

    /**
     * Returns the SRL Header.
     *
     * @return mixed
     */
    public function getSRLHeader()
    {
        return $this->srlHeader;
    }

    /**
     * Returns the raw SRL Body.
     *
     * @return mixed
     */
    public function getRawSRLBody()
    {
        return $this->srlBody;
    }

    /**
     * Returns the SRL Footer.
     *
     * @return mixed
     */
    public function getSRLFooter()
    {
        return $this->reportMetaDefinitionEntry->srl_footer;
    }

    private function convertSRLExpressionsForReportingEquations($expression)
    {
        $srlDataMappings = [
            // Employee
            'employee.total.hours.overtime' => 'employee_hours_overtime_worked',
            'employee.total.hours.regular'  => 'employee_hours_regular_worked',
            'employee.wage.overtime'        => 'employee_historic_wage_overtime',
            'employee.wage.regular'         => 'employee_historic_wage_regular',
            'employee.cost.overtime'        => 'total_employee_overtime_cost',
            'employee.cost.regular'         => 'total_employee_regular_cost',
            'employee.cost'                 => 'total_employee_combined_cost',

            // Fuel
            'fuel.quantity'                 => 'total_fuels_quantity',
            'fuel.total'                    => 'total_fuels_cost',

            // Material
            'material.quantity'             => 'total_materials_quantity',
            'material.total'                => 'total_materials_cost',

            // Property
            'property.total'                => 'total_property_cost',

            // Billing
            'billing.rate'                  => 'misc_billing_rate',
            'billing.quantity'              => 'misc_billing_quantity',
            'billing.total'                 => 'total_misc_billing_cost',

            // Aggregate equipment units.
            'equipment.total.quantity'      => 'total_equipment_units_quantity',
            'equipment.total.costWithFuels' => 'total_equipment_and_fuels_cost',
            'equipment.total.cost'          => 'total_equipment_units_cost',

        ];

        foreach ($srlDataMappings as $mapping => $target) {
            $expression = str_replace($mapping, $target, $expression);
        }

        return $expression;
    }

    private function escapeDataMappingsForSRLInlineExpressions($expression)
    {
        $expression = str_replace('data-mapping="emp', 'data-mapping="E\\{ESC}MP', $expression);
        $expression = str_replace('data-mapping="equ', 'data-mapping="E\\{ESC}QU', $expression);
        $expression = str_replace('data-mapping="fue', 'data-mapping="F\\{ESC}EU', $expression);
        $expression = str_replace('data-mapping="mat', 'data-mapping="M\\{ESC}AT', $expression);
        $expression = str_replace('data-mapping="pro', 'data-mapping="P\\{ESC}RO', $expression);

        return $expression;
    }

    private function unEscapeDataMappingsForSRLInlineExpressions($expression)
    {
        $expression = str_replace('data-mapping="E\\{ESC}MP', 'data-mapping="emp', $expression);
        $expression = str_replace('data-mapping="E\\{ESC}QU', 'data-mapping="equ', $expression);
        $expression = str_replace('data-mapping="F\\{ESC}EU', 'data-mapping="fue', $expression);
        $expression = str_replace('data-mapping="M\\{ESC}AT', 'data-mapping="mat', $expression);
        $expression = str_replace('data-mapping="P\\{ESC}RO', 'data-mapping="pro', $expression);

        return $expression;
    }

    /**
     * Converts SRL style mappings in equations.
     *
     * This method applies to the body SRL syntax.
     *
     * @param $expression
     * @param $data
     * @return string
     */
    private function convertSRLExpressionsForEvaluateBody($expression, $data)
    {

        // Fix mappings here.
        $expression = $this->escapeDataMappingsForSRLInlineExpressions($expression);

        $srlDataMappings = [
            // Employee
            'employee.total.hours.overtime' => 'employee_hours_overtime_worked',
            'employee.total.hours.regular'  => 'employee_hours_regular_worked',
            'employee.wage.overtime'        => 'employee_historic_wage_overtime',
            'employee.wage.regular'         => 'employee_historic_wage_regular',
            'employee.fringe.overtime'        => 'employee_historic_fringe_overtime',
            'employee.fringe.regular'         => 'employee_historic_fringe_regular',
            'employee.cost.overtime'        => 'total_employee_overtime_cost',
            'employee.cost.regular'         => 'total_employee_regular_cost',
            'employee.cost'                 => 'total_employee_combined_cost',

            // Fuel
            'fuel.quantity'                 => 'total_fuels_quantity',
            'fuel.total'                    => 'total_fuels_cost',
            'fuel.cost'                    => 'total_fuels_cost',

            // Material
            'material.quantity'             => 'total_materials_quantity',
            'material.total'                => 'total_materials_cost',
            'material.cost'                => 'total_materials_cost',

            // Property
            'property.total'                => 'total_property_cost',

            // Billing
            'billing.rate'                  => 'misc_billing_rate',
            'billing.quantity'              => 'misc_billing_quantity',
            'billing.total'                 => 'total_misc_billing_cost',

            // Aggregate equipment units.
            'equipment.total.quantity'      => 'total_equipment_units_quantity',
            'equipment.total.costWithFuels' => 'total_equipment_and_fuels_cost',
            'equipment.total.cost'          => 'total_equipment_units_cost',

            // Record
            'record.total'                  => 'recordTotal',
        ];

        foreach ($srlDataMappings as $mapping => $target) {
            $expression = str_replace($mapping, e((object_get($data, $target))), $expression);
        }

        $expression = $this->unEscapeDataMappingsForSRLInlineExpressions($expression);

        return $expression;
    }

    /**
     * Converts SRL style mappings in equations.
     *
     * This method applies to the footer SRL syntax.
     *
     * @param $expression
     * @param $data
     * @return string
     */
    private function convertSRLExpressionsForEvaluateFooter($expression, $data)
    {
        $expression = $this->escapeDataMappingsForSRLInlineExpressions($expression);

        $srlDataMappings = [
            'report.grandEmployeeRegularHours'   => 'grand_employee_hours_regular_worked',
            'report.grandEmployeeOvertimeHours'  => 'grand_employee_hours_overtime_worked',
            'report.grandEmployeeTotalHours'     => 'grand_employee_hours_total_worked',
            'report.grandBillingQuantity'        => 'grand_misc_billing_quantity',
            'report.grandMaterialsQuantity'      => 'grand_total_materials_quantity',
            'report.grandEquipmentUnitsQuantity' => 'grand_total_equipment_units_quantity',
            'report.grandFuelsQuantity'          => 'grand_total_fuels_quantity',
            'report.grandMaterialsCost'          => 'grand_total_materials_cost',
            'report.grandEquipmentUnitsCost'     => 'grand_total_equipment_units_cost',
            'report.grandFuelsCost'              => 'grand_total_fuels_cost',
            'report.grandFuelsEquipmentCost'     => 'grand_total_equipment_and_fuels_cost',
            'report.grandBillingCost'            => 'grand_total_misc_billing_cost',
            'report.grandPropertyCost'           => 'grand_total_property_cost',
            'report.grandEmployeeRegularCost'    => 'grand_total_employee_regular_cost',
            'report.grandEmployeeOvertimeCost'   => 'grand_total_employee_overtime_cost',
            'report.grandEmployeeTotalCost'      => 'grand_total_employee_combined_cost',
            'report.grandEmployeeTotalHours'     => 'grand_employee_hours_total_worked',
            'report.grandRecordTotal'            => 'grand_record_totals',

        ];

        foreach ($srlDataMappings as $mapping => $target) {
            $expression = str_replace($mapping, e((object_get($data, $target))), $expression);
        }

        $expression = $this->unEscapeDataMappingsForSRLInlineExpressions($expression);

        return $expression;
    }

    protected $alreadyDidOptions = false;

    /**
     * Prints the SRL Body for the given raw data.
     *
     * @param $rawData
     *
     * @return string
     */
    public function srlPrintBody($rawData)
    {
        $this->srlPrinter->setRawData($rawData);


        if (!$this->alreadyDidOptions) { $this->srlPrinter->setOptions($this->srlPrinter->getOptions() +
            ['specialGroupName' => $this->specialGroupingSecondGroupName]); $this->alreadyDidOptions = true; }

        $printResult = $this->srlPrinter->srlPrint();

        $potentialEvaluateExpressions = [];

        preg_match_all(self::SRL_EVALUATE_COMMAND_REGEX, $printResult, $potentialEvaluateExpressions);

        if (count($potentialEvaluateExpressions) > 0) {
            if (is_array($potentialEvaluateExpressions[0])) {
                $potentialEvaluateExpressions = $potentialEvaluateExpressions[0];
            }

            // Convert SRL mappings for expressions.
            foreach ($potentialEvaluateExpressions as $key => $expression) {
                $potentialEvaluateExpressions[$key] =
                    $this->convertSRLExpressionsForEvaluateBody($expression, $rawData);
            }
            $this->evaluateCommand->reset();
            $this->evaluateCommand->setExpressions($potentialEvaluateExpressions);
            $this->evaluateCommand->evaluate();
            $solutions = $this->evaluateCommand->getSolutions();

            $printResult = strtr($printResult, $solutions);

            unset($solutions);
        }

        return $printResult;
    }

    /**
     * Prints the SRL form of the header.
     *
     * @return mixed
     */
    public function srlPrintHeader()
    {
        return $this->srlHeaderPrinter->srlPrint();
    }

    public function prepareGroupedFooter($data)
    {
        return $this->srlHeaderPrinter->srlPrintHeaderAsGroupedFooter($data);
    }

    /**
     * Prints the SRL form of the footer.
     *
     * @return string
     */
    public function srlPrintFooter()
    {
        $totals = $this->workReaderReference->getReportTotals();
        if ($this->internalEngineMode >= 1) {
            $totals->grand_consumable_equipment_total = 0;
            $totals->grand_consumable_fuel_total      = 0;
            $totals->grand_consumable_material_total  = 0;
            $totals->grand_consumable_total           = 0;

            $totals->grand_consumable_equipment_quantity = 0;
            $totals->grand_consumable_fuel_quantity      = 0;
            $totals->grand_consumable_material_quantity  = 0;
            $totals->grand_consumable_quantity           = 0;

            foreach ($this->consumableWorkRecords as $record) {
                if ($record->consumable_type == 2) {
                    $totals->grand_consumable_equipment_total += $record->total_cost;
                    $totals->grand_consumable_equipment_quantity += $record->total_quantity;
                } elseif ($record->consumable_type == 3) {
                    $totals->grand_consumable_fuel_total += $record->total_cost;
                    $totals->grand_consumable_fuel_quantity += $record->total_quantity;
                } elseif ($record->consumable_type == 4) {
                    $totals->grand_consumable_material_total += $record->total_cost;
                    $totals->grand_consumable_material_quantity += $record->total_quantity;
                }
            }

            $totals->grand_consumable_total    =
                ($totals->grand_consumable_equipment_total + $totals->grand_consumable_fuel_total +
                    $totals->grand_consumable_material_total);
            $totals->grand_consumable_quantity =
                ($totals->grand_consumable_equipment_quantity + $totals->grand_consumable_fuel_quantity +
                    $totals->grand_consumable_material_quantity);
        }

        $this->srlFooterPrinter->setRawData($totals);

        $printResult = $this->srlFooterPrinter->srlPrint();

        $potentialEvaluateExpressions = [];

        preg_match_all(self::SRL_EVALUATE_COMMAND_REGEX, $printResult, $potentialEvaluateExpressions);

        if (count($potentialEvaluateExpressions) > 0) {
            if (is_array($potentialEvaluateExpressions[0])) {
                $potentialEvaluateExpressions = $potentialEvaluateExpressions[0];
            }

            // Apply the SRL data mappings to the expressions.
            foreach ($potentialEvaluateExpressions as $key => $expression) {
                $potentialEvaluateExpressions[$key] =
                    $this->convertSRLExpressionsForEvaluateFooter($expression, $totals);
            }

            $this->evaluateCommand->reset();
            $this->evaluateCommand->setExpressions($potentialEvaluateExpressions);
            $this->evaluateCommand->evaluate();
            $solutions = $this->evaluateCommand->getSolutions();

            $printResult = strtr($printResult, $solutions);

            /**
            foreach ($solutions as $original => $solution)
            {
            $printResult = str_replace($original, e(nd_number_format($solution)), $printResult);
            }
             */

            unset($solutions);
        }

        return $printResult;
    }

    /**
     * Sets the report record total query.
     *
     * @param $equationID
     *
     * @return mixed
     */
    public function setReportRecordTotalQuery($equationID)
    {
        $this->reportRecordQuery = $equationID;
    }

    /**
     * Gets the report record total query.
     *
     * @return mixed
     */
    public function getReportRecordTotalQuery()
    {
        return $this->reportRecordQuery;
    }

    private function formatRecordTotals($recordTotal)
    {
        $formattedQuery = '';

        $formattedQuery = str_replace('TOTAL', '', $recordTotal);

        if (starts_with($formattedQuery, '(') == false) {
            $formattedQuery = '(' . $formattedQuery;
        } else {
            $formattedQuery = $formattedQuery;
        }

        $formattedQuery = str_finish($formattedQuery, ')');

        if (ends_with($formattedQuery, 'as recordTotal') == false) {
            $formattedQuery .= ' as recordTotal';
        }

        return $formattedQuery;
    }

    private function formatGrandTotalQuery($recordTotal)
    {
        return 'SUM' .
        str_replace('as recordTotal', 'as grand_record_totals', $this->formatRecordTotals($recordTotal));
    }

    protected $validGroups = ['activity_id', 'department_id', 'district_id', 'employee_id', 'project_id', 'property_id', 'work_date', 'consumable_id'];

    /**
     * Groups the report by a given value.
     *
     * @param $value
     *
     * @return mixed
     * @throws \Exception
     */
    public function groupReportBy($value)
    {
        if (!in_array($value, $this->validGroups)) {
            throw new ReportingException("[GUARD] Invalid Reporting Group");
        }
        $this->reportGroupingColumn = 'work_entries.' . $value;
    }

    public function alsoGroupBy($value)
    {
        if (strlen($value) == 0  || $value == 'none' || $value == null) {
            // Just bail early.
            return;
        }
        if (!in_array($value, $this->validGroups)) {
            throw new ReportingException("[GUARD] Invalid Reporting Group");
        }
        $this->advancedGroupingColumns[] = 'work_entries.'.$value;
    }

    /**
     * Reporting sorting settings.
     *
     * @var array
     */
    private $sortingSettings = [];

    public function sortReportBy($column, $sort)
    {
        $this->sortingSettings[$column] = $sort;
    }

    /**
     * Prepares a work reader for report retrieval.
     *
     * @param WorkEntryReaderRepositoryInterface $reader
     *
     * @return mixed
     */
    public function prepare(WorkEntryReaderRepositoryInterface &$reader)
    {
        $reader->setReportMode(true);
        $reader->where('exclude_from_report', '=', 0);

        if (strlen(trim($this->reportGroupingColumn)) > 0 || is_object($this->reportGroupingColumn)) {
            $reader->select(DB::raw('SUM(employee_hours_regular_worked) as employee_hours_regular_worked'));
            $reader->select(DB::raw('SUM(employee_hours_overtime_worked) as employee_hours_overtime_worked'));
            $reader->select(DB::raw('SUM(misc_billing_quantity) as misc_billing_quantity'));
            $reader->select(DB::raw('SUM(total_materials_quantity) as total_materials_quantity'));
            $reader->select(DB::raw('SUM(total_equipment_units_quantity) as total_equipment_units_quantity'));
            $reader->select(DB::raw('SUM(total_fuels_quantity) as total_fuels_quantity'));
            $reader->select(DB::raw('SUM(total_materials_cost) as total_materials_cost'));
            $reader->select(DB::raw('SUM(total_equipment_units_cost) as total_equipment_units_cost'));
            $reader->select(DB::raw('SUM(total_fuels_cost) as total_fuels_cost'));
            $reader->select(DB::raw('SUM(total_misc_billing_cost) as total_misc_billing_cost'));

            $reader->select(DB::raw('SUM(total_property_cost) as total_property_cost'));
            $reader->select(DB::raw('SUM(total_employee_regular_cost) as total_employee_regular_cost'));
            $reader->select(DB::raw('SUM(total_employee_overtime_cost) as total_employee_overtime_cost'));
            $reader->select(DB::raw('SUM(total_employee_combined_cost) as total_employee_combined_cost'));
        }

        $actualRecordQuery = '';

        if ($this->reportRecordQuery == null) {
            $actualRecordQuery = $this->formatRecordTotals(self::SRL_DEFAULT_REPORT_RECORD_QUERY);

            $grandTotalQuery = $this->formatGrandTotalQuery(self::SRL_DEFAULT_REPORT_RECORD_QUERY);
            $grandTotalQuery = $this->convertSRLExpressionsForReportingEquations($grandTotalQuery);
            $reader->setReportRecordTotalQuery($grandTotalQuery);
        } else {
            $actualRecordQuery = $this->formatRecordTotals($this->reportRecordQuery);

            if (strlen(trim($this->reportGroupingColumn)) > 0) {
                $reader->groupBy($this->reportGroupingColumn);
                if (count($this->advancedGroupingColumns) > 0) {
                    foreach ($this->advancedGroupingColumns as $column) {
                        $reader->groupBy($column);
                    }
                }
                $actualRecordQuery = 'SUM' . $actualRecordQuery;
            }


            $this->reportRecordQuery = $this->convertSRLExpressionsForReportingEquations($this->reportRecordQuery);
            $reader->setReportRecordTotalQuery($this->formatGrandTotalQuery($this->reportRecordQuery));
        }

        if (strlen(trim($this->reportGroupingColumn)) > 0 || is_object($this->reportGroupingColumn)) {
            // This check needs to be here to ensure the accuracy of the generated reports. This allows the
            // organization strategy to work "nicely" with custom report groupings.
            if ($this->organizationStrategy != null) {
                if (count($this->organizationStrategy) > 0) {
                    $additionalGroupingColumn = $this->organizationStrategy['mode'];

                    // This check is to avoid doubling up the query for special grouping modes.
                    if ($additionalGroupingColumn != 'special') {
                        $reader->groupBy(DB::raw('work_entries.' . $additionalGroupingColumn . ',' .
                            $this->reportGroupingColumn));
                    } else {
                        if (count($this->specialGroupingFirstGroupValues) > 0) {
                            $filters = $this->reportingFilters;
                            reset($filters);
                            $filters = key($filters);
                            $this->applyFilter($this->specialGroupColumnName,
                                array_merge($this->specialGroupingFirstGroupValues,
                                    $this->specialGroupingSecondGroupValues));
                        }

                        //$reader->groupBy($this->reportGroupingColumn);
                    }
                }
            } else {
                $reader->groupBy($this->reportGroupingColumn);
            }

            // $actualRecordQuery = 'SUM' . $actualRecordQuery;
        }

        if (count($this->reportingFilters) > 0) {

            foreach ($this->reportingFilters as $filterName => $filterValues) {
                // Another safety check.
                if (count($filterValues) > 0) {
                    if ($this->organizationStrategy['mode'] == 'special') {
                        $filterValues['exclude'] = true;
                    }

                    if ($filterValues['exclude'] == false) {
                        if ($filterName == 'property_id') {
                            $reader->setQuery($reader->getQuery()->where(function ($query) use ($filterName, $filterValues) {
                                $query->whereNotIn('work_entries.property_id', $filterValues['values']);
                                $query->whereNotIn('work_entries.property_organization_id', $filterValues['values']);
                            }));
                        } else {
                            if ($filterName == 'road_id') {
                                $reader->setQuery($reader->getQuery()->where(function ($query) use ($filterName, $filterValues) {
                                    $query->whereNotIn('work_entries.property_organization_id', $filterValues['values']);
                                }));
                            } else {
                                $reader->setQuery($reader->getQuery()->where(function ($query) use ($filterName, $filterValues) {
                                    $query->whereNotIn('work_entries.' . $filterName, $filterValues['values']);
                                }));
                            }
                        }
                    } else {
                        if ($filterName == 'property_id') {
                            $reader->setQuery($reader->getQuery()->where(function ($query) use ($filterName, $filterValues) {
                                $query->where(function ($q) use ($filterName, $filterValues) {
                                    $q->whereIn('work_entries.property_organization_id', $filterValues['values']);
                                })->orWhere(function ($q) use ($filterName, $filterValues) {
                                    $q->whereIn('work_entries.property_id', $filterValues['values']);
                                });
                            }));
                        } else {
                            if ($filterName == 'road_id') {
                                $reader->setQuery($reader->getQuery()->where(function ($query) use ($filterName, $filterValues) {
                                    $query->where(function ($q) use ($filterName, $filterValues) {
                                        $q->whereIn('work_entries.property_organization_id', $filterValues['values']);

                                    });
                                    //->orWhere(function ($q) use ($filterName, $filterValues) {
                                    //    $q->whereIn('work_entries.property_id', $filterValues['values']);
                                    // });
                                }));
                            } else {
                                $reader->setQuery($reader->getQuery()->where(function ($query) use ($filterName, $filterValues) {
                                    $query->whereIn('work_entries.' . $filterName, $filterValues['values']);
                                }));
                            }
                        }
                    }
                }
            }
        }

        $actualRecordQuery = $this->convertSRLExpressionsForReportingEquations($actualRecordQuery);
        $reader->select(DB::raw($actualRecordQuery));
        $this->workReaderReference = &$reader;
    }

    /**
     * Gathers the organization groups.
     *
     * @param $organizationGroup
     *
     * @return mixed
     */
    public function gatherOrganizationGroups($organizationGroup)
    {
        switch ($organizationGroup) {
            case 'property_id':
                return $this->workReaderReference->getProperties();
                break;
            case 'property_organization_id':
                return $this->workReaderReference->getProperties();
                break;
            case 'equipment_id':
                return $this->workReaderReference->getEquipmentUnits();
                break;
            case 'department_id':
                return $this->workReaderReference->getDepartments();
                break;
            case 'district_id':
                return $this->workReaderReference->getDistricts();
                break;
            case 'employee_id':
                return $this->workReaderReference->getEmployees();
                break;
            case 'activity_id':
                return $this->workReaderReference->getActivities();
                break;
            case 'project_id':
                return $this->workReaderReference->getProjects();
                break;
        }
    }

    /**
     * Sets the reports organization strategy.
     *
     * @param $mode
     * @param $value
     *
     * @return mixed
     */
    public function setReportOrganizationStrategy($mode, $value)
    {
        $this->organizationStrategy = ['mode' => $mode, 'value' => $value];

        if ($this->formatter == null) {
            $this->formatter = App::make('formatter');
        }

        if ($this->redactor == null) {
            $this->redactor = StandardEngineFactory::getEngine();
        }
    }

    /**
     * Returns the reports organization strategy.
     *
     * @return mixed
     */
    public function getOrganizationStrategy()
    {
        return $this->organizationStrategy;
    }

    /**
     * Gets the name for a particular organization category value based on the current mode.
     *
     * This method users the report formatter.
     *
     * @param $mode
     * @param $value
     *
     * @returns mixed
     */
    public function getOrganizationStrategyGroupingName($value, $mode = null)
    {
        if ($this->organizationStrategy == null) {
            return null;
        }

        $formattedObject = new \stdClass();

        if ($mode == null) {
            $mode = $this->organizationStrategy['mode'];
        }

        if ($this->organizationStrategy['mode'] == 'special') {
            return $this->specialReflectionGroupingName($value, $mode);
        }

        if ($this->formatter == null) {
            $this->formatter = App::make('formatter');
        }

        switch ($mode) {
            case 'property_organization_id':
            case 'equipment_id':
            case 'property_id':
                if ($value != 0 || $value != 1) {
                    $property =
                        array_pluck_first_where($this->workReaderReference->getRawWorkRecords(), 'property_id', $value);
                    if ($property == null) {
                        $property = array_pluck_first_where($this->workReaderReference->getRawWorkRecords(), 'property_organization_id', $value);
                        if ($property == null) {
                            return null;
                            break;
                        }
                        $formattedObject->code = $property->property_organization_code;
                        $formattedObject->name = $property->property_organization_name;
                    } else {
                        $formattedObject->code = $property->property_code;
                        $formattedObject->name = $property->property_name;
                    }

                } else {
                    $formattedObject->code = 'NA';
                    $formattedObject->name = 'NA';
                }

                return $this->formatter->propertyHeading($formattedObject);
                break;
            case 'department_id':
                if ($value != 0) {
                    $department =
                        array_pluck_first_where($this->workReaderReference->getRawWorkRecords(), 'department_id',
                            $value);
                    if ($department == null) {
                        return null;
                        break;
                    }
                    $formattedObject->code = $department->department_code;
                    $formattedObject->name = $department->department_name;
                } else {
                    $formattedObject->code = 'NA';
                    $formattedObject->name = 'NA';
                }

                return $this->formatter->departmentHeading($formattedObject);
                break;
            case 'district_id':
                if ($value != 0) {
                    $district =
                        array_pluck_first_where($this->workReaderReference->getRawWorkRecords(), 'district_id',
                            $value);

                    if ($district == null) {
                        return null;
                        break;
                    }
                    $formattedObject->code = $district->district_code;
                    $formattedObject->name = $district->department_name;
                } else {
                    $formattedObject->code = 'NA';
                    $formattedObject->name = 'NA';
                }

                return $this->formatter->districtHeading($formattedObject);
                break;
            case 'employee_id':
                if ($value != 0) {
                    $employee =
                        array_pluck_first_where($this->workReaderReference->getRawWorkRecords(), 'employee_id', $value);

                    if ($employee == null) {
                        return null;
                        break;
                    }

                    $formattedObject->code = $employee->employee_code;
                    $formattedObject->name = $employee->employee_last_name . ', ' . $employee->employee_first_name;
                } else {
                    $formattedObject->code = 'NA';
                    $formattedObject->name = 'NA';
                }

                $temp = $this->formatter->employeeHeading($formattedObject);
                return $this->redactor->redact($temp);
                break;
            case 'activity_id':
                if ($value != 0) {
                    $activity =
                        array_pluck_first_where($this->workReaderReference->getRawWorkRecords(), 'activity_id', $value);
                    if ($activity == null) {
                        return null;
                        break;
                    }

                    $formattedObject->code = $activity->activity_code;
                    $formattedObject->name = $activity->activity_name;
                } else {
                    $formattedObject->code = 'NA';
                    $formattedObject->name = 'NA';
                }

                return $this->formatter->activityHeading($formattedObject);
                break;
            case 'project_id':
                if ($value != 0) {
                    $project =
                        array_pluck_first_where($this->workReaderReference->getRawWorkRecords(), 'project_id', $value);

                    if ($project == null) {
                        return null;
                        break;
                    }

                    $formattedObject->code = $project->project_code;
                    $formattedObject->name = $project->project_name;
                } else {
                    $formattedObject->code = 'NA';
                    $formattedObject->name = 'NA';
                }

                return $this->formatter->projectHeading($formattedObject);
                break;
        }
    }

    /**
     * Gets the report name for the organization strategy.
     *
     * @param $mode
     *
     * @return string
     */
    public function getOrganizationStrategyName($mode = null)
    {
        if ($this->organizationStrategy == null) {
            return null;
        }

        if ($mode == null) {
            $mode = $this->organizationStrategy['mode'];
        }

        switch ($mode) {
            case 'property_organization_id':
            case 'property_id':
                return 'Road';
                break;
            case 'equipment_id':
                return 'Equipment Unit';
                break;
            case 'department_id':
                return 'Department';
                break;
            case 'district_id':
                return 'District';
                break;
            case 'employee_id':
                return 'Employee';
                break;
            case 'activity_id':
                return 'Activity';
                break;
            case 'project_id':
                return 'Project';
                break;
        }
    }

    /**
     * Groups the report using a special grouping organization mode.
     *
     * @param $specialColumn
     * @param $firstGroupValues
     * @param $secondGroupValues
     * @param $firstGroupName
     * @param $secondGroupName
     *
     * @return mixed
     */
    public function specialReportGrouping($specialColumn, $firstGroupValues, $secondGroupValues, $firstGroupName, $secondGroupName)
    {
        $this->specialGroupingFirstGroupValues  = $firstGroupValues;
        $this->specialGroupingSecondGroupValues = $secondGroupValues;
        $this->specialGroupingFirstGroupName    = $firstGroupName;
        $this->specialGroupingSecondGroupName   = $secondGroupName;

        $this->specialGroupColumnName           = $specialColumn;

        $filterColumnName = $this->reportingFilters;
        reset($filterColumnName);
        $filterColumnName = key($filterColumnName);

        // The special grouping mode will need to take advantage of multiple groupings.
        if ($filterColumnName == $specialColumn || $filterColumnName == null) {
            $this->reportGroupingColumn = DB::raw('work_entries.' . $specialColumn);
        } else {
            $this->reportGroupingColumn =
                DB::raw('work_entries.' . $filterColumnName . ',work_entries.' . $specialColumn);
        }

        $this->organizationStrategy = ['mode' => 'special'];
    }

    /**
     * Gets information about the reports special grouping information.
     *
     * @return mixed
     */
    public function getSpecialReportGroupingInformation()
    {
        return [
            'special_grouping_column' => $this->specialGroupColumnName,
            'group_one_values'        => $this->specialGroupingFirstGroupValues,
            'group_two_values'        => $this->specialGroupingSecondGroupValues,
            'group_one_name'          => $this->specialGroupingFirstGroupName,
            'group_two_name'          => $this->specialGroupingSecondGroupName
        ];
    }

    protected $tempRecords = null;

    protected $reportRecords = null;

    /**
     * Gets the records for the report.
     *
     * @return mixed
     * @throws \Exception
     */
    public function getRecords()
    {
        if ($this->workReaderReference == null) {
            throw new \Exception('Prepare constructor first.');
        }

        // If there are any property type limits, such as EQUIPMENT UNIT or ROAD
        // this piece of code will make sure that only records with properties of those types
        // are returned.
        if (count($this->propertyLimits) > 0) {
            $query  = $this->workReaderReference->getQuery();
            $limits = $this->propertyLimits;
            if ($this->workReaderReference->getLegacyMode()) {
                $query  = $query->where(function ($query) use ($limits) {
                    $query->where(function ($q) use ($limits) {
                        $q->whereIn('prop.property_type', $limits);
                    })->orWhere(function ($q) use ($limits) {
                        $q->whereIn('prop_o.property_type', $limits);
                    });
                    // $query->whereIn('prop.property_type', $limits);
                });
            } else {
                $query = $query->havingRaw(DB::raw('property_type IN ('.implode(',', $limits).')'))->orHavingRaw(DB::raw('property_organization_type IN ('.implode(',', $limits).')'));
            }
            $this->workReaderReference->setQuery($query);
        }

        $this->workReaderReference->setReportSortSettings($this->sortingSettings);

        // Sets the engines internal operation mode.
        $this->workReaderReference->setInternalMode($this->internalEngineMode);
        // Sets the printer options.
        $this->srlHeaderPrinter->setOptions($this->srlHeaderPrinter->getOptions() +
            ['consumableMode' => $this->internalEngineMode]);
        $this->srlPrinter->setOptions($this->srlHeaderPrinter->getOptions() +
            ['consumableMode' => $this->internalEngineMode]);
        $this->srlFooterPrinter->setOptions($this->srlHeaderPrinter->getOptions() +
            ['consumableMode' => $this->internalEngineMode]);

        // This piece of code will return gather the report records for modes 0 - 4.
        if ($this->internalEngineMode == 0) {
            // doGatherForNormalMode handles both NORMAL reporting and
            // the gathering of special report records.
            $this->reportRecords = $this->doGatherForNormalMode();
        } elseif (in_array($this->internalEngineMode, [1, 2, 3, 4])) {
            $this->reportRecords = $this->doGatherForConsumableMode();
        } else {
            throw new \Exception('An invalid reporting mode was specified.');
        }

        $srlPreparation = json_decode($this->reportMetaDefinitionEntry->srl_settings);
        $srlPreparation = object_get($srlPreparation, 'preparation', []);

        $this->handleSRLPrepareFunctions($srlPreparation);
        $this->buildSRLPreparationBank($this->workReaderReference->getWorkRecordIDs());

        if ($this->directReportProvider !== null) {

            $this->directReportProvider->setWorkRecordIDs($this->workReaderReference->getWorkRecordIDs());

            return $this->directReportProvider->getRecords();
        }

        return $this->reportRecords;
    }

    /**
     * Applies a filter to the report.
     *
     * @param       $filterName
     * @param array $filterValues
     * @param bool  $exclude
     *
     * @return mixed
     */
    public function applyFilter($filterName, array $filterValues, $exclude = false)
    {
        $validFilterNames = [
            'activity_id', 'department_id', 'project_id', 'district_id', 'employee_id', 'property_id', 'road_id'
        ];
        if (in_array($filterName, $validFilterNames)) {
            if (count($filterValues) > 0) {
                $this->reportingFilters[$filterName] = ['values' => $filterValues, 'exclude' => $exclude];
            }
        }
    }

    public function isDirectReport()
    {
        return !is_null($this->directReportProvider);
    }

    public function &getDirectProvider()
    {
        return $this->directReportProvider;
    }

    /**
     * Gets the reporting filters.
     *
     * @return mixed
     */
    public function getReportFilters()
    {
        return $this->reportingFilters;
    }

    /**
     * Sets the total block for the report.
     *
     * @param $blockID
     * @return mixed
     */
    public function setTotalBlock($blockID)
    {
        $this->defaultTotalBlock = $blockID;
    }

    /**
     * Gets the total block for the report.
     *
     * @return mixed
     */
    public function getTotalBlock()
    {
        return $this->defaultTotalBlock;
    }
}
