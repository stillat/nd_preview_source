<?php namespace ParadoxOne\NDCounties\Reporting\Builders;

use Illuminate\Support\Facades\DB;
use ParadoxOne\NDCounties\Reporting\InvalidReportOperatorException;
use ParadoxOne\NDCounties\Reporting\ReportingException;

trait ConstraintBuilderTrait
{
    /**
     * Holds the constraint mappings.
     *
     * This exists to hide the actual schema information from developers.
     *
     * @var array
     */
    private $constraintMappings = [
        'emp_regular_hours' => 'employee_hours_regular_worked',
        'emp_overtime_hours' => 'employee_hours_overtime_worked',
        'emp_regular_total' => 'total_employee_regular_cost',
        'emp_overtime_total' => 'total_employee_overtime_cost',
        'emp_combined_total' => 'total_employee_combined_cost',

        'material_total' => 'total_materials_cost',
        'equipment_cost' => 'total_equipment_units_cost',
        'fuel_cost' => 'total_fuels_cost',

        'material_quantity' => 'total_materials_quantity',
        'fuel_quantity' => 'total_fuels_quantity',
        'equipment_quantity' => 'total_equipment_units_quantity',

        'mbilling_total' => 'total_misc_billing_cost',
        'mbilling_quantity' => 'misc_billing_quantity'
    ];


    /**
     * Holds the valid comparison operators.
     * @var array
     */
    private $validComparisonOperators = [
        '=', '>', '<', '>=', '<='
    ];

    /**
     * Sets a constraint on the report.
     *
     * @param $constraintName
     * @param $constraintComparisonOperator
     * @param $constraintValue
     * @throws ReportingException
     * @throws InvalidReportOperatorException
     * @return mixed
     */
    public function constrainReport($constraintName, $constraintComparisonOperator, $constraintValue)
    {
        // Just some validation.

        if (array_key_exists($constraintName, $this->constraintMappings) == false) {
            throw new ReportingException("{$constraintName} is an invalid constraint.");
        }

        if (in_array($constraintComparisonOperator, $this->validComparisonOperators) == false) {
            throw new InvalidReportOperatorException("{$constraintComparisonOperator} is an invalid comparison operator.");
        }

        if (is_numeric($constraintValue) == false) {
            throw new ReportingException("{$constraintValue} is not a valid constraint value.");
        }

        // Now we can actually work on the constraining process.
        $this->workReaderReference->where(DB::raw($this->constraintMappings[$constraintName]), $constraintComparisonOperator, $constraintValue);
    }
}
