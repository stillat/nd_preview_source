<?php namespace ParadoxOne\NDCounties\Reporting\Builders;

use Illuminate\Support\Facades\App;

trait SpecialOrgReflectionTrait
{
    protected $cachedObjects = null;

    private function specialReflectionGroupingName($value, $mode)
    {
        if ($mode == null) {
            $mode = $this->organizationStrategy['mode'];
        }

        if ($this->formatter == null) {
            $this->formatter = App::make('formatter');
        }

        $filterColumnName = $this->reportingFilters;
        reset($filterColumnName);
        $filterColumnName = key($filterColumnName);

        $values = $this->reportingFilters[$filterColumnName]['values'];


        // Build the cache.
        if ($this->cachedObjects == null && count($values) > 0) {
            switch ($filterColumnName) {
                case 'employee_id':
                    $repository          = App::make('ParadoxOne\NDCounties\Contracts\EmployeeRepositoryInterface');
                    $this->cachedObjects = $repository->getAllWithTrashed()->whereIn('id', $values)->get();
                    break;
                case 'activity_id':
                    $repository          = App::make('ParadoxOne\NDCounties\Contracts\ActivityRepositoryInterface');
                    $this->cachedObjects = $repository->getAllWithTrashed()->whereIn('id', $values)->get();
                    break;
                case 'department_id':
                    $repository          = App::make('ParadoxOne\NDCounties\Contracts\DepartmentsRepositoryInterface');
                    $this->cachedObjects = $repository->getAllWithTrashed()->whereIn('id', $values)->get();
                    break;
                case 'district_id':
                    $repository          = App::make('ParadoxOne\NDCounties\Contracts\DistrictsRepositoryInterface');
                    $this->cachedObjects = $repository->getAllWithTrashed()->whereIn('id', $values)->get();
                    break;
                case 'project_id':
                    $repository          =
                        App::make('ParadoxOne\NDCounties\Contracts\Projects\ProjectRepositoryInterface');
                    $this->cachedObjects = $repository->getAllWithTrashed()->whereIn('id', $values)->get();
                    break;
                case 'equipment_id':
                case 'property_id':
                    $repository          =
                        App::make('ParadoxOne\NDCounties\Contracts\Properties\PropertiesRepositoryInterface');
                    $this->cachedObjects = $repository->getPropertiesIn($values);
                    break;

            }
        }

        $formattedObject = new \stdClass();

        switch ($filterColumnName) {
            case 'employee_id':
                if ($value != 0) {
                    $obj                   = array_pluck_first_where($this->cachedObjects, 'id', $value);
                    $formattedObject->code = $obj->code;
                    $formattedObject->name = $obj->last_name . ', ' . $obj->first_name;
                } else {
                    $formattedObject->code = 'NA';
                    $formattedObject->name = 'NA';
                }

                return $this->formatter->employee($formattedObject);
                break;
            case 'activity_id':
                if ($value != 0) {
                    $obj                   = array_pluck_first_where($this->cachedObjects, 'id', $value);
                    $formattedObject->code = $obj->code;
                    $formattedObject->name = $obj->name;
                } else {
                    $formattedObject->code = 'NA';
                    $formattedObject->name = 'NA';
                }

                return $this->formatter->activity($formattedObject);
                break;
            case 'department_id':
                if ($value != 0) {
                    $obj                   = array_pluck_first_where($this->cachedObjects, 'id', $value);
                    $formattedObject->code = $obj->code;
                    $formattedObject->name = $obj->name;
                } else {
                    $formattedObject->code = 'NA';
                    $formattedObject->name = 'NA';
                }

                return $this->formatter->department($formattedObject);
                break;
            case 'district_id':
                if ($value != 0) {
                    $obj                   = array_pluck_first_where($this->cachedObjects, 'id', $value);
                    $formattedObject->code = $obj->code;
                    $formattedObject->name = $obj->name;
                } else {
                    $formattedObject->code = 'NA';
                    $formattedObject->name = 'NA';
                }

                return $this->formatter->district($formattedObject);
                break;
            case 'project_id':
                if ($value != 0) {
                    $obj                   = array_pluck_first_where($this->cachedObjects, 'id', $value);
                    $formattedObject->code = $obj->code;
                    $formattedObject->name = $obj->name;
                } else {
                    $formattedObject->code = 'NA';
                    $formattedObject->name = 'NA';
                }

                return $this->formatter->project($formattedObject);
                break;
            case 'equipment_id':
            case 'property_id':
                if ($value != 0 || $value != 1) {
                    $obj                   = array_pluck_first_where($this->cachedObjects, 'id', $value);
                    $formattedObject->code = $obj->code;
                    $formattedObject->name = $obj->name;
                } else {
                    $formattedObject->code = 'NA';
                    $formattedObject->name = 'NA';
                }

                return $this->formatter->property($formattedObject);
                break;
        }
    }
}
