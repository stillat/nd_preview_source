<?php namespace ParadoxOne\NDCounties\Reporting\Builders;

trait ConsumableModeBuilderTrait
{
    /**
     * Enables the consumable override.
     *
     * @return mixed
     */
    public function enableConsumableOverride()
    {
        $this->workReaderReference->enableConsumableOverride();
    }


    /**
     * Disables the consumable override.
     *
     * @return mixed
     */
    public function disableConsumableOverride()
    {
        $this->workReaderReference->disableConsumableOverride();
    }

    /**
     * Sets the consumable record grouping.
     *
     * @param $columns
     * @return mixed
     */
    public function groupConsumableRecord($columns)
    {
        $this->workReaderReference->groupConsumableRecord($columns);
    }

    /**
     * Sets a consumable override restriction mode.
     *
     * @param       $type
     * @param array $restriction
     * @return mixed
     */
    public function setConsumableRestrictions($type, array $restriction)
    {
        $this->workReaderReference->setConsumableRestrictions($type, $restriction);
    }


    private function doGatherForConsumableMode()
    {
        if ($this->restrictingWorkRecords) {
            $this->workReaderReference->setRestrictionColumns($this->restrictingColumns);
        }

        if ($this->specialGroupingSecondGroupName == '') {
            $this->consumableWorkRecords = $this->workReaderReference->getOrganizedConsumableRecords();
        }

        if (in_array($this->internalEngineMode, [2, 3, 4])) {
            $this->consumableWorkRecords =
                array_pluck_where($this->consumableWorkRecords, 'consumable_type', $this->internalEngineMode);
        }

        return $this->consumableWorkRecords;
        // $this->workReaderReference->getOrganizedConsumableRecords();
    }
}
