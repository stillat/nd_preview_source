<?php namespace ParadoxOne\NDCounties\Reporting\Builders;

use UI;

trait RecordRestrictionTrait
{
    protected $validRestrictingColumns = ['equipment_id', 'material_id'];

    protected $restrictingColumns = [];

    protected $restrictingWorkRecords = false;

    public function restrictWorkRecords($filterName, array $values, $mode)
    {
        if (!in_array($filterName, $this->validRestrictingColumns)) {
            UI::info('An invalid filter was supplied and was ignored.');

            return;
        }

        $this->restrictingWorkRecords = true;
        $this->restrictingColumns[]   = [$filterName, $values, $mode];
    }
}
