<?php namespace ParadoxOne\NDCounties\Reporting\Builders;

use Carbon\Carbon;
use ParadoxOne\NDCounties\Database\Entities\WorkEntries\WorkEntry;

trait NormalModeBuilderTrait
{
    /**
     * Checks to see if the records need to be restricted.
     *
     * If so, this method will perform the actions necessary to limit the reports work records.
     */
    private function checkForRecordRestriction()
    {
        if ($this->restrictingWorkRecords) {
            $this->workReaderReference->setRestrictionColumns($this->restrictingColumns);
        }
    }

    /**
     * Indicates if the engine is generated a special mode report.
     *
     * @return bool
     */
    private function isSpecialModeReport()
    {
        return (!strlen(trim($this->specialGroupingSecondGroupName)) == 0);
    }

    private function doGatherForNormalMode()
    {
        $this->checkForRecordRestriction();

        if (!$this->isSpecialModeReport()) {
            $records = $this->workReaderReference->getRecords();
            return $records;
        } else {
            // The following work records will be combined by the special
            // report grouping column.
            $workRecords = $this->workReaderReference->getRecords();
            return $this->constructSpecialReportRecords($workRecords);
        }
    }
}
