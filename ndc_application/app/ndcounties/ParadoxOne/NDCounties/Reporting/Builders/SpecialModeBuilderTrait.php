<?php namespace ParadoxOne\NDCounties\Reporting\Builders;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use ParadoxOne\NDCounties\Reporting\Utilities\WorkRecordAdder;

trait SpecialModeBuilderTrait
{
    protected function constructSpecialReportRecords(&$records)
    {
        $firstGroup  = [];
        $secondGroup = [];

        foreach ($records as $workRecord) {
            if (in_array(intval(object_get($workRecord->rawData, $this->specialGroupColumnName)),
                         $this->specialGroupingFirstGroupValues)) {
                $firstGroup[] = $workRecord;
            } else {
                $secondGroup[] = $workRecord;
            }
        }

        $adder = new WorkRecordAdder;
        $adder->setGenericName($this->specialGroupingSecondGroupName);
        $secondGroupRecord = $adder->addWorkRecords($secondGroup);

        // Some simple cleanup.
        $secondGroup = null;
        unset($secondGroup);
        $adder = null;
        unset($adder);

        // Add the second group record to the END of the records array.
        $firstGroup[] = $secondGroupRecord;

        return $firstGroup;
    }
}
