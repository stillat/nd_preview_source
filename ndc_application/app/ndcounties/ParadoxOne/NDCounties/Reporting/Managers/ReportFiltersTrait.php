<?php namespace ParadoxOne\NDCounties\Reporting\Managers;

trait ReportFiltersTrait
{
    public $reportingFilters = [];

    public function applyFilters()
    {
        $this->applyReportingFilter('activity_id', 'activity_filters', 'activityFilerMode');
        $this->applyReportingFilter('department_id', 'department_filters', 'departmentFilterMode');
        $this->applyReportingFilter('district_id', 'district_filters', 'districtFilterMode');
        $this->applyReportingFilter('employee_id', 'employee_filters', 'employeeFilterMode');
        $this->applyReportingFilter('equipment_id', 'equipment_filters', 'equipFilterMode');
        $this->applyReportingFilter('material_id', 'material_filters', 'materialFilterMode');
        $this->applyReportingFilter('project_id', 'project_filters', 'projectFilterMode');
        $this->applyReportingFilter('property_id', 'property_filters', 'propertyFilterMode');

        // Limit the property types.
        if ($this->request->has('property_type_filters')) {
            $this->limitPropertyTypes($this->request->get('property_type_filters', []));
        }
    }

    public function applyReportingFilter($filterName, $filterValues, $exclude)
    {
        $validFilterNames = [
            'activity_id',
            'department_id',
            'project_id',
            'district_id',
            'employee_id',
            'property_id',
            'road_id'
        ];
        if (in_array($filterName, $validFilterNames)) {
            $filterValues = $this->request->get($filterValues);
            if (count($filterValues) > 0) {
                $exclude                             = ($this->request->get($exclude, 'inc') == 'inc') ? false : true;
                $this->reportingFilters[$filterName] = ['values' => $filterValues, 'exclude' => $exclude];
            }
        }
    }
}
