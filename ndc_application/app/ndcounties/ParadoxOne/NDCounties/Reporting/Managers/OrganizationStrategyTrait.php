<?php namespace ParadoxOne\NDCounties\Reporting\Managers;

use ParadoxOne\NDCounties\Reporting\ReportGenerationException;

trait OrganizationStrategyTrait
{
    public $organizationStrategy;

    public function setOrganizationStrategy($strategy)
    {
        $this->organizationStrategy = $strategy;

        if ($strategy != 3) {
            $this->applyFilters();
        }

        switch ($strategy) {
            case 2:
                $this->applyGenericStrategy();
                break;
            case 3:
                $this->breakDownVisible = false;
                $success                = $this->applySpecialStrategy();
                if (!$success) {
                    throw new ReportGenerationException('Failure in special strategy');
                }
                break;
        }
    }
}
