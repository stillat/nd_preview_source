<?php namespace ParadoxOne\NDCounties\Reporting\Managers;

trait NormalModeManagerTrait
{
    /**
     * Checks to see if the records need to be restricted.
     *
     * If so, this method will perform the actions necessary to limit the reports work records.
     */
    private function checkForRecordRestriction()
    {
        // Defined in constraint manager trait.
        if ($this->restrictingWorkRecords) {
            $this->reader->reader()->setRestrictionColumns($this->restrictingColumns);
        }
    }

    /**
     * Indicates if the engine is generated a special mode report.
     *
     * @return bool
     */
    private function isSpecialModeReport()
    {
        return $this->specialMode;
    }

    private function doGatherForNormalMode()
    {
        $this->checkForRecordRestriction();

        if (!$this->isSpecialModeReport()) {
            return $this->reader->reader()->getRecords();
        } else {
            // The following work records will be combined by the special
            // report grouping column.
            $workRecords = $this->reader->reader()->getRecords();

            return $this->constructSpecialReportRecords($workRecords);
        }
    }
}
