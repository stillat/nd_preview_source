<?php namespace ParadoxOne\NDCounties\Reporting\Managers;

trait GroupingManagerTrait
{
    protected $uiGroupColumn;

    public $genericGroupingColumn;

    public function setUIGroupColumn($column)
    {
        $this->uiGroupColumn = $column;

        if ($column != 'none' && $column != 'consumable_id') {
            // Disable break downs.
            $this->breakDownVisible = false;
            $this->groupReportBy($column);
        }
    }

    /**
     * Groups the report by a given value.
     *
     * @param $value
     *
     * @return mixed
     * @throws \Exception
     */
    public function groupReportBy($value)
    {
        $this->reportGroupingColumn = 'work_entries.' . $value;
    }
}
