<?php namespace ParadoxOne\NDCounties\Reporting\Managers;

trait PropertyLimiterManagerTrait
{
    protected $propertyLimits = [];

    /**
     * Limits the report to the specified property types.
     *
     * @param $propertyTypes
     * @return mixed
     */
    public function limitPropertyTypes($propertyTypes)
    {
        if (count($propertyTypes) > 0) {
            $this->propertyLimits = $propertyTypes;
        }
    }
}
