<?php namespace ParadoxOne\NDCounties\Reporting\Managers;

trait EquationManagerTrait
{
    public $equationID = 0;

    public $grandTotalID = 0;

    protected $equationInstance = null;

    protected $grandTotalInstance = null;

    public function setEquation($id)
    {
        $this->equationID = $id;
        // TODO: Get equation/grand total from the database
        // TODO: Set the grand total query from the equation.
    }

    public function setGrandTotalBlock($id)
    {
        $this->grandTotalID = $id;
        // TODO: Get quation/grand total from the database
    }

    /**
     * Returns a modified SQL query to get record totals.
     *
     * @param $recordTotal
     * @return mixed|string
     */
    private function formatRecordTotals($recordTotal)
    {
        $formattedQuery = str_replace('TOTAL', '', $recordTotal);

        if (starts_with($formattedQuery, '(') == false) {
            $formattedQuery = '(' . $formattedQuery;
        } else {
            $formattedQuery = $formattedQuery;
        }

        $formattedQuery = str_finish($formattedQuery, ')');

        if (ends_with($formattedQuery, 'as recordTotal') == false) {
            $formattedQuery .= ' as recordTotal';
        }

        return $formattedQuery;
    }

    /**
     * Creates a grand record totals SQL query.
     *
     * @param $recordTotal
     * @return string
     */
    private function formatGrandTotalQuery($recordTotal)
    {
        return 'SUM' .
               str_replace('as recordTotal', 'as grand_record_totals', $this->formatRecordTotals($recordTotal));
    }
}
