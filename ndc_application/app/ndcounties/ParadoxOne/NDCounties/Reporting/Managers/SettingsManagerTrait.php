<?php namespace ParadoxOne\NDCounties\Reporting\Managers;

trait SettingsManagerTrait
{
    /**
     * Gets the settings for the reporting engine.
     *
     * This function takes into account the settings the user has overridden.
     *
     * @return mixed
     */
    protected function getSettingsWithModifications()
    {
        $settings                                      = $this->settingsManager->getReportSettings();
        $settings->show_employee_wage                  = $this->request->has('show_employee_wage');
        $settings->show_material_breakdown             = $this->request->has('show_material_breakdown');
        $settings->show_material_breakdown_total       = $this->request->has('show_material_breakdown_total');
        $settings->show_equipment_unit_breakdown       = $this->request->has('show_equipment_unit_breakdown');
        $settings->show_equipment_unit_breakdown_total = $this->request->has('show_equipment_unit_breakdown_total');
        $settings->show_fuel_breakdown                 = $this->request->has('show_fuel_breakdown');
        $settings->show_fuel_breakdown_total           = $this->request->has('show_fuel_breakdown_total');
        $settings->show_property_breakdown             = $this->request->has('show_property_breakdown');
        $settings->show_property_breakdown_total       = $this->request->has('show_property_breakdown_total');
        $settings->show_equipment_unit_rate            = $this->request->has('show_equipment_unit_rate');

        return $settings;
    }
}
