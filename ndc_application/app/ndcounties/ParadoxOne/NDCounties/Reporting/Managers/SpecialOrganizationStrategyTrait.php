<?php namespace ParadoxOne\NDCounties\Reporting\Managers;

use ParadoxOne\NDCounties\Reporting\Utilities\WorkRecordAdder;

trait SpecialOrganizationStrategyTrait
{
    public $specialGroupFilters            = null;
    public $specialGroupSecondaryFilters   = null;
    public $specialGroupColumnName         = null;
    public $specialGroupingSecondGroupName = null;
    public $specialMode                    = false;

    private function getSpecialGroupName()
    {
        $groupName = Input::get('special_grouping_name');

        if (strlen($this->specialGroupColumnName) == 0) {
            return 'OTHER';
        }

        return $this->specialGroupColumnName;
    }

    protected function applySpecialStrategy()
    {
        $this->specialMode = true;
    }

    protected function constructSpecialReportRecords(&$records)
    {
        $firstGroup  = [];
        $secondGroup = [];

        foreach ($records as $workRecord) {
            if (in_array(intval(object_get($workRecord->rawData, $this->specialGroupColumnName)),
                $this->specialGroupFilters)) {
                $firstGroup[] = $workRecord;
            } else {
                $secondGroup[] = $workRecord;
            }
        }

        $adder = new WorkRecordAdder;
        $adder->setGenericName($this->specialGroupingSecondGroupName);
        $secondGroupRecord = $adder->addWorkRecords($secondGroup);

        // Some simple cleanup.
        $secondGroup = null;
        unset($secondGroup);
        $adder = null;
        unset($adder);

        // Add the second group record to the END of the records array.
        $firstGroup[] = $secondGroupRecord;

        return $firstGroup;
    }
}
