<?php namespace ParadoxOne\NDCounties\Reporting;

class ReportStateManager
{
    /**
     * Holds the reporting state objects.
     *
     * @var array
     */
    protected $stateObjects = [];

    protected $dataStorage = [];

    /**
     * Returns an item from storage.
     *
     * NOTE: Will return 0 if the object does not exist.
     *
     * @param $name
     * @return mixed
     */
    public function getFromStorage($name)
    {
        if (!array_key_exists($name, $this->dataStorage)) {
            return 0;
        }

        return $this->dataStorage[$name];
    }

    /**
     * Sets a storage items value, creating it if doesn't exist.
     *
     * @param $name
     * @param $newValue
     */
    public function setStorage($name, $newValue)
    {
        $this->dataStorage[$name] = $newValue;
    }

    /**
     * Removes an item from storage.
     *
     * @param $name
     */
    public function removeFromStorage($name)
    {
        unset($this->dataStorage[$name]);
    }

    /**
     * Notifies the state manager an object has been observed.
     *
     * @param $object
     */
    public function notify($object)
    {
        $this->stateObjects[$object] = true;
    }

    /**
     * Indicates if an object was observed.
     *
     * @param $object
     * @return bool
     */
    public function observed($object)
    {
        return array_key_exists($object, $this->stateObjects);
    }

    /**
     * Tears down the state manager objects.
     *
     * @return void
     */
    public function tearDown()
    {
        $this->stateObjects = [];
    }
}
