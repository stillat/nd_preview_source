<?php namespace ParadoxOne\NDCounties\Reporting\SRL;

use Carbon\Carbon;
use Illuminate\Support\Facades\App;
use ParadoxOne\NDCounties\DataRedactionServices\StandardEngineFactory;
use ParadoxOne\NDCounties\Contracts\Reporting\SRL\SRLPrinterInterface;

class BaseSRLPrinter implements SRLPrinterInterface
{
    use \ParadoxOne\NDCounties\Reporting\SRL\Directives\TrackDirectiveTrait;

    use \ParadoxOne\NDCounties\Reporting\SRL\Directives\GetTrackDirectiveTrait;

    use \ParadoxOne\NDCounties\Reporting\SRL\Directives\PreparedDirectiveTrait;

    use \ParadoxOne\NDCounties\Reporting\SRL\Directives\GetOdometerReadingDirectiveTrait;

    use \ParadoxOne\NDCounties\Reporting\SRL\Directives\GetSurfaceTypeDirectiveTrait;



    /**

     * The SRL source format.

     *

     * @var string

     */

    protected $source = '';



    /**

     * The state manager.

     *

     * @var null

     */

    protected $stateManager = null;



    protected $resetSource = '';



    /**

     * The printer's raw data.

     *

     * @var null

     */

    protected $rawData = null;



    protected $rawRecord = null;



    /**

     * Cached report formatting settings.

     *

     * @var null

     */

    protected $cachedFormattingSettings = null;



    /**

     * Cached report settings.

     *

     * @var null

     */

    protected $cachedReportingSettings = null;



    /**

     * Holds the regular expression for the INSPECT command.

     *

     * @var string

     */

    const SRL_PROPERTY_INSPECT_REGEX = '/INSPECT\(.*?\)/';



    /**

     * Holds the regular expression for the DIRECT command.

     *

     * @var string

     */

    const SRL_DATA_DIRECT_REGEX = '/DIRECT\(.*?\)/';



    // TODO: Implement TRACK later.

    const SRL_STATE_TRACK_REGEX    = '/TRACK\(.*?\)/';

    const SRL_STATE_TAKE_ALL_REGEX = '';

    const SRL_STATE_GET_TRACK      = '/GETTRACK\(.*?\)/';

    const SRL_STATE_PREPARED       = '/PREPARED\(.*?\)/';



    const SRL_GET_ODOMETER_COMMAND_REGEX     = '/GET_ODOMETER_READING\(.*?\)/';

    const SRL_GET_SURFACE_TYPE_COMMAND_REGEX = '/GET_SURFACE_TYPE\(.*?\)/';



    /**

     * The default SRL Data mappings.

     *

     * @var array

     */

    protected $srlDataMappings = [

        // Employee

        'employee.total.hours.overtime' => 'employee_hours_overtime_worked',

        'employee.total.hours.regular'  => 'employee_hours_regular_worked',

        'employee.wage.overtime'        => 'employee_historic_wage_overtime',

        'employee.wage.regular'         => 'employee_historic_wage_regular',

        'employee.fringe.overtime'        => 'employee_historic_fringe_overtime',

        'employee.fringe.regular'         => 'employee_historic_fringe_regular',

        'employee.cost.overtime'        => 'total_employee_overtime_cost',

        'employee.cost.regular'         => 'total_employee_regular_cost',

        'employee.cost'                 => 'total_employee_combined_cost',



        // Fuel

        'fuel.quantity'                 => 'total_fuels_quantity',

        'fuel.total'                    => 'total_fuels_cost',



        // Material

        'material.quantity'             => 'total_materials_quantity',

        'material.total'                => 'total_materials_cost',



        // Property

        'property.total'                => 'total_property_cost',



        // Billing

        'billing.rate'                  => 'misc_billing_rate',

        'billing.quantity'              => 'misc_billing_quantity',

        'billing.total'                 => 'total_misc_billing_cost',



        // Aggregate equipment units.

        'equipment.total.quantity'      => 'total_equipment_units_quantity',

        'equipment.total.costWithFuels' => 'total_equipment_and_fuels_cost',

        'equipment.total.cost'          => 'total_equipment_units_cost',



        // Record

        'record.total'                  => 'recordTotal',

    ];



    /**

     * The printer options.

     *

     * @var array

     */

    protected $options = [];



    /**

     * A generic not available record.

     *

     * @var null

     */

    protected $notAvailableRecordHolder = null;



    /**

     * The formatting settings manager.

     *

     * @var \ParadoxOne\NDCounties\Settings\CountyFormattingSettingsManager

     */

    protected $formatter = null;



    protected $redactor = null;



    /**

     * Cached generics.

     *

     * @var array

     */

    protected $cachedGenerics = [];



    public function __construct()
    {
        $this->formatter    = App::make('formatter');

        $this->redactor     = StandardEngineFactory::getEngine();

        $this->stateManager = App::make('stateManager');



        $this->notAvailableRecordHolder       = new \stdClass;

        $this->notAvailableRecordHolder->code = 'NA';

        $this->notAvailableRecordHolder->name = 'NA';
    }



    public function setReportingSettings($settings)
    {
        $this->cachedReportingSettings = $settings;

        if ($settings->show_employee_wage == false) {
            $this->srlDataMappings['employee.wage.overtime'] = '&nbsp;';

            $this->srlDataMappings['employee.wage.regular']  = '&nbsp;';

            $this->srlDataMappings['employee.fringe.overtime'] = '&nbsp;';

            $this->srlDataMappings['employee.fringe.regular']  = '&nbsp;';
        }

        if ($settings->show_employee_hours_worked == false) {
            $this->srlDataMappings['employee.total.hours.overtime'] = '&nbsp;';

            $this->srlDataMappings['employee.total.hours.regular'] = '&nbsp;';
        }
    }



    /**

     * Formats the body extension.

     *

     * @param $source

     * @return string

     */

    private function formatBodyExtension($source)
    {
        $tempSource = str_replace('<th', '<td', $source);

        $tempSource = str_replace('</th>', '</td>', $tempSource);



        return $tempSource;
    }



    /**

     * Sets the source for the printer.

     *

     * @param $source

     * @return mixed

     */

    public function setSource($source)
    {
        if (array_key_exists('bodyExtendsHeader', $this->options)) {
            $tempSource        = $this->formatBodyExtension($source);

            $this->source      = $tempSource;

            $this->resetSource = $tempSource;

            unset($tempSource);
        } else {
            $this->source      = $source;

            $this->resetSource = $source;
        }
    }



    /**

     * Sets the raw data for the printer.

     *

     * @param $rawData

     * @return mixed

     */

    public function setRawData(&$rawData)
    {
        $this->rawData   = $rawData[0];

        $this->rawRecord = $rawData[1];



        // Reset source.

        $this->source = $this->resetSource;
    }



    /**

     * Sets the options for the printer.

     * f

     *

     * @param array $options

     * @return mixed

     */

    public function setOptions(array $options)
    {
        $this->options = $options;
    }



    /**

     * Gets the options set.

     *

     * @return array

     */

    public function getOptions()
    {
        return $this->options;
    }



    private function buildEscapedMapping($mapping)
    {
        return substr($mapping, 0, 1) . '\\{ESC}' . substr($mapping, 1);
    }



    /**

     * Formats the SRL data mappings.

     *

     * @return void

     */

    private function formatSRLMappings()
    {



        // START: Creates some default data in case we are using prepared reports somewhere else.

        foreach (['activity_id', 'consumable_type', 'employee_id', 'project_id', 'department_id', 'district_id',

                  'property_id', 'property_context', 'id', 'property_organization_id'] as $property) {
            if (!property_exists($this->rawData, $property)) {
                $this->rawData->{$property} = 0;
            }
        }



        if (!property_exists($this->rawData, 'work_date')) {
            $this->rawData->work_date = Carbon::now();
        }



        foreach (['equipments'] as $property) {
            if (!property_exists($this->rawData, $property)) {
                $this->rawData->{$property} = [];
            }
        }



        \Debugbar::startMeasure('cmap', 'Creating mappings...');



        $openSpan = '" data-a\{ESC}ctivity="' . $this->rawData->activity_id . '"';

        $openSpan .= ' data-e\{ESC}mployee="' . $this->rawData->employee_id . '"';

        $openSpan .= ' data-p\{ESC}roject="' . $this->rawData->project_id . '"';

        $openSpan .= ' data-d\{ESC}epartment="' . $this->rawData->department_id . '"';

        $openSpan .= ' data-d\{ESC}istrict="' . $this->rawData->district_id . '"';

        $openSpan .= ' data-p\{ESC}roperty="' . $this->rawData->property_id . '"';

        $openSpan .= ' data-p\{ESC}roperty-organization="' . $this->rawData->property_organization_id . '"';

        $openSpan .= ' data-p\{ESC}roperty-context="' . $this->rawData->property_context . '"';

        $openSpan .= ' data-workid="' . $this->rawData->id . '"';

        $openSpan .= '>';



        $replacementMappings = [];



        foreach ($this->srlDataMappings as $mapping => $target) {
            $startSpan = '<span data-type="numeric" data-mapping="' . $this->buildEscapedMapping($mapping);

            $replacementMappings[$mapping] = $startSpan.$openSpan.e((object_get($this->rawData, $target))) . '</span>';
        }



        $this->source = strtr($this->source, $replacementMappings);

        //$this->source = str_replace(array_keys($replacementMappings), array_values($replacementMappings), $this->source);



        \Debugbar::stopMeasure('cmap');



        /**

        // Removed in favor of above implementation.

        // END: Prepared reports fixes.

        \Debugbar::startMeasure('com', 'GMAP');

        foreach ($this->srlDataMappings as $mapping => $target)

        {

            $openSpan = '<span data-type="numeric" data-mapping="' . $this->buildEscapedMapping($mapping) .

                        '" data-a\{ESC}ctivity="' . $this->rawData->activity_id . '"';

            $openSpan .= ' data-e\{ESC}mployee="' . $this->rawData->employee_id . '"';

            $openSpan .= ' data-p\{ESC}roject="' . $this->rawData->project_id . '"';

            $openSpan .= ' data-d\{ESC}epartment="' . $this->rawData->department_id . '"';

            $openSpan .= ' data-d\{ESC}istrict="' . $this->rawData->district_id . '"';

            $openSpan .= ' data-p\{ESC}roperty="' . $this->rawData->property_id . '"';

            $openSpan .= ' data-p\{ESC}roperty-organization="' . $this->rawData->property_organization_id . '"';

            $openSpan .= ' data-p\{ESC}roperty-context="' . $this->rawData->property_context . '"';

            $openSpan .= ' data-workid="' . $this->rawData->id . '"';

            $openSpan .= '>';

            // The mappings will always be numeric here.

            $this->source = str_replace($mapping,$openSpan . e(nd_number_format(search_object_graph($this->rawData, $target))) . '</span>',$this->source);

        }

        \Debugbar::stopMeasure('com');

        */
    }



    /**

     * Formats the SRL generics.

     *

     * @return void

     */

    private function formatGenerics()
    {

        // Employee

        $employeeReplace = '';



        if ($this->rawData->employee_id == -1) {
            $groupName = 'OTHER';

            if (array_key_exists('specialGroupName', $this->options)) {
                $groupName = $this->options['specialGroupName'];
            }



            $object          = new \stdClass();

            $object->code    = $groupName;

            $object->name    = $groupName;

            $employeeReplace = $this->formatter->other($object);
        } else {
            if (array_key_exists('employee_' . $this->rawData->employee_id, $this->cachedGenerics) == false) {

                // Employee

                if ($this->rawData->employee_id > 0) {
                    $employee        = new \stdClass();

                    $employee->code  = $this->rawData->employee_code;

                    $employee->name  = $this->rawData->employee_last_name . ', ' . $this->rawData->employee_first_name;

                    $employeeReplace = $this->formatter->employee($employee);

                    $employeeReplace = $this->redactor->redact($employeeReplace);

                    unset($employee);
                } else {
                    $employeeReplace = $this->formatter->employee($this->notAvailableRecordHolder);
                }



                $this->cachedGenerics['employee_' . $this->rawData->employee_id] = $employeeReplace;
            } else {
                $employeeReplace = $this->cachedGenerics['employee_' . $this->rawData->employee_id];
            }
        }



        $this->source = str_replace('employee', e($employeeReplace), $this->source);



        // Activity

        $activityReplace = '';



        if ($this->rawData->activity_id == -1) {
            $groupName = 'OTHER';

            if (array_key_exists('specialGroupName', $this->options)) {
                $groupName = $this->options['specialGroupName'];
            }



            $activity        = new \stdClass();

            $activity->code  = $groupName;

            $activity->name  = $groupName;

            $activityReplace = $this->formatter->other($activity);
        } else {
            if (array_key_exists('activity_' . $this->rawData->activity_id, $this->cachedGenerics) == false) {

                // Employee

                if ($this->rawData->activity_id > 0) {
                    $activity        = new \stdClass();

                    $activity->code  = $this->rawData->activity_code;

                    $activity->name  = $this->rawData->activity_name;

                    $activityReplace = $this->formatter->activity($activity);

                    unset($activity);
                } else {
                    $activityReplace = $this->formatter->activity($this->notAvailableRecordHolder);
                }



                $this->cachedGenerics['activity_' . $this->rawData->activity_id] = $activityReplace;
            } else {
                $activityReplace = $this->cachedGenerics['activity_' . $this->rawData->activity_id];
            }
        }



        $this->source = str_replace('activity', e($activityReplace), $this->source);



        // Property

        $propertyReplace = '';

        if ($this->rawData->property_id == -1) {
            $groupName = 'OTHER';

            if (array_key_exists('specialGroupName', $this->options)) {
                $groupName = $this->options['specialGroupName'];
            }



            $object          = new \stdClass();

            $object->code    = $groupName;

            $object->name    = $groupName;

            $propertyReplace = $this->formatter->other($object);
        } else {
            $orgIsSpecified = false;



            if ($this->rawData->property_organization_id > 1 && ($this->rawData->property_organization_id != $this->rawData->property_id)) {
                $property        = new \stdClass();

                $property->code  = $this->rawData->organization_property->code;

                $property->name  = $this->rawData->organization_property->name;

                $propertyReplace = e($this->formatter->property($property));

                $orgIsSpecified = true;



                unset($property);
            }



            if ($this->rawData->property_id > 1) {
                $property        = new \stdClass();

                $property->code  = $this->rawData->property_code;

                $property->name  = $this->rawData->property_name;



                if ($orgIsSpecified) {
                    $propertyReplace .= '<strong>&nbsp;/</strong><br>';
                }



                $propertyReplace .= e($this->formatter->property($property));

                unset($property);
            }



            if ($this->rawData->property_id <= 1 && $this->rawData->property_organization_id <= 1) {
                $propertyReplace = $this->formatter->property($this->notAvailableRecordHolder);
            }
        }



        $this->source = str_replace('property', $propertyReplace, $this->source);



        // Department

        $departmentReplace = '';



        if ($this->rawData->department_id == -1) {
            $groupName = 'OTHER';

            if (array_key_exists('specialGroupName', $this->options)) {
                $groupName = $this->options['specialGroupName'];
            }



            $object            = new \stdClass();

            $object->code      = $groupName;

            $object->name      = $groupName;

            $departmentReplace = $this->formatter->other($object);
        } else {
            if (array_key_exists('department_' . $this->rawData->department_id, $this->cachedGenerics) == false) {

                // Employee

                if ($this->rawData->department_id > 0) {
                    $dept              = new \stdClass();

                    $dept->code        = $this->rawData->department_code;

                    $dept->name        = $this->rawData->department_name;

                    $departmentReplace = $this->formatter->department($dept);

                    unset($dept);
                } else {
                    $departmentReplace = $this->formatter->department($this->notAvailableRecordHolder);
                }



                $this->cachedGenerics['department_' . $this->rawData->department_id] = $departmentReplace;
            } else {
                $departmentReplace = $this->cachedGenerics['department_' . $this->rawData->department_id];
            }
        }



        $this->source = str_replace('department', e($departmentReplace), $this->source);



        // District

        $districtReplace = '';



        if ($this->rawData->district_id == -1) {
            $groupName = 'OTHER';

            if (array_key_exists('specialGroupName', $this->options)) {
                $groupName = $this->options['specialGroupName'];
            }



            $object          = new \stdClass();

            $object->code    = $groupName;

            $object->name    = $groupName;

            $districtReplace = $this->formatter->other($object);
        } else {
            if (array_key_exists('district_' . $this->rawData->district_id, $this->cachedGenerics) == false) {

                // Employee

                if ($this->rawData->department_id > 0) {
                    $dist              = new \stdClass();

                    $dist->code        = $this->rawData->district_code;

                    $dist->name        = $this->rawData->district_name;

                    $departmentReplace = $this->formatter->district($dist);

                    unset($dist);
                } else {
                    $districtReplace = $this->formatter->district($this->notAvailableRecordHolder);
                }



                $this->cachedGenerics['district_' . $this->rawData->district_id] = $districtReplace;
            } else {
                $districtReplace = $this->cachedGenerics['district_' . $this->rawData->district_id];
            }
        }



        $this->source = str_replace('district', e($districtReplace), $this->source);



        // Project

        $projectReplace = '';



        if ($this->rawData->project_id == -1) {
            $groupName = 'OTHER';

            if (array_key_exists('specialGroupName', $this->options)) {
                $groupName = $this->options['specialGroupName'];
            }



            $object         = new \stdClass();

            $object->code   = $groupName;

            $object->name   = $groupName;

            $projectReplace = $this->formatter->other($object);
        } else {
            if (array_key_exists('project_' . $this->rawData->project_id, $this->cachedGenerics) == false) {

                // Employee

                if ($this->rawData->project_id > 0) {
                    $proj           = new \stdClass();

                    $proj->code     = $this->rawData->project_code;

                    $proj->name     = $this->rawData->project_name;

                    $projectReplace = $this->formatter->project($proj);

                    unset($proj);
                } else {
                    $projectReplace = $this->formatter->project($this->notAvailableRecordHolder);
                }



                $this->cachedGenerics['project_' . $this->rawData->project_id] = $projectReplace;
            } else {
                $projectReplace = $this->cachedGenerics['project_' . $this->rawData->project_id];
            }
        }



        $this->source = str_replace('project', e($projectReplace), $this->source);



        // Billing

        $this->source = str_replace('billing', '&nbsp;', $this->source);



        // Consumables.

        $this->source = str_replace('material', '&nbsp;', $this->source);

        $this->source = str_replace('fuel', '&nbsp;', $this->source);



        if (count($this->rawData->equipments) == 0) {
            $this->source = str_replace('equipment.odometer', '&nbsp;', $this->source);
        } elseif (count($this->rawData->equipments) == 1) {
            $replace = '';



            if ($this->rawData->equipments[0]->equipment->odometer != null) {
                $replace = e($this->rawData->equipments[0]->equipment->odometer->odometer_reading);
            }



            $this->source = str_replace('equipment.odometer', $replace, $this->source);
        } else {
            $this->source = str_replace('equipment.odometer',

                                        '<span data-indicator="see-breakdown" data-type="e\\{ESC}quipment">See break down</span>',

                                        $this->source);
        }



        if (count($this->rawData->equipments) == 0) {
            $this->source = str_replace('equipment.description', '&nbsp;', $this->source);



            $this->source = str_replace('equipment', '&nbsp;', $this->source);
        } elseif (count($this->rawData->equipments) == 1) {
            $replace = '';



            if ($this->rawData->equipments[0]->equipment->id == 0) {
                $replace = $this->formatter->property($this->notAvailableRecordHolder);
            } else {
                $replace = $this->formatter->property($this->rawData->equipments[0]->equipment);
            }



            $this->source = str_replace('equipment.description', $this->rawData->equipments[0]->equipment->description,

                                        $this->source);



            $this->source = str_replace('equipment', $replace, $this->source);
        } else {
            $this->source = str_replace('equipment.description', '&nbsp;', $this->source);

            $this->source = str_replace('equipment',

                                        '<span data-indicator="see-breakdown" data-type="e\\{ESC}quipment">See break down</span>',

                                        $this->source);
        }
    }



    private function formatMetaData()
    {
        $this->source =

            str_replace('record.date', e((new Carbon($this->rawData->work_date))->toDateString()), $this->source);



        if (str_contains($this->source, 'billing.description')) {
            if (strlen($this->rawData->misc_description) > 0) {
                $this->source = str_replace('billing.description', e($this->rawData->misc_description), $this->source);
            } else {
                $this->source = str_replace('billing.description', '&nbsp;', $this->source);
            }



            $this->stateManager->notify('record_' . $this->rawData->id . '_billing_desc');
        }



        if (str_contains($this->source, 'activity.description')) {
            if (strlen($this->rawData->activity_description) > 0) {
                $this->source =

                    str_replace('activity.description', e($this->rawData->activity_description), $this->source);
            } else {
                $this->source = str_replace('activity.description', '&nbsp;', $this->source);
            }



            $this->stateManager->notify('record_' . $this->rawData->id . '_activity_desc');
        }



        $this->source = str_replace('record.id', $this->rawData->id, $this->source);
    }



    private function prepareConsumables()
    {
        $consumableMode = 0;



        if (array_key_exists('consumableMode', $this->options)) {
            $consumableMode = $this->options['consumableMode'];
        }

        if (intval($consumableMode) > 0) {
            $this->srlDataMappings += ['consumable.quantity' => 'total_quantity'];

            $this->srlDataMappings += ['consumable.unit_cost' => 'historic_cost'];

            $this->srlDataMappings += ['consumable.cost' => 'total_cost'];
        }
    }



    private function printConsumables()
    {
        $consumableMode = 0;



        if (array_key_exists('consumableMode', $this->options)) {
            $consumableMode = $this->options['consumableMode'];
        }



        if (intval($consumableMode) > 0) {

            /**

             * $this->source = str_replace('consumable.quantity', nd_number_format($this->rawData->total_quantity), $this->source);

             * $this->source = str_replace('consumable.unit_cost', nd_number_format($this->rawData->historic_cost), $this->source);

             * $this->source = str_replace('consumable.cost', nd_number_format($this->rawData->total_cost), $this->source);

             */



            if ($this->rawData->consumable_type == 2) {
                $this->source =

                    str_replace('consumable', $this->formatter->property($this->rawData->equipment), $this->source);
            } elseif ($this->rawData->consumable_type == 3) {
               try {
                   if (property_exists($this->rawData, 'fuel')) {
                       $this->source = str_replace('consumable', $this->formatter->fuel($this->rawData->fuel), $this->source);
                   } elseif (property_exists($this->rawData, 'fuels') && is_array($this->rawData->fuels) && count($this->rawData->fuels) > 0) {
                       $this->source = str_replace('consumable', $this->formatter->fuel($this->rawData->fuels[0]), $this->source);
                   }
               } catch (\Exception $e) {
               }
            } elseif ($this->rawData->consumable_type == 4) {
                $this->source =

                    str_replace('consumable', $this->formatter->material($this->rawData->material), $this->source);
            }
        }
    }



    private function parsePropertyInformation()
    {
        $inspectionCommands = [];

        preg_match_all(self::SRL_PROPERTY_INSPECT_REGEX, $this->source, $inspectionCommands);

        $inspectionCommands = array_filter($inspectionCommands);



        if (count($inspectionCommands) > 0) {
            if (is_array($inspectionCommands[0])) {
                $inspectionCommands  = $inspectionCommands[0];

                $propertyInformation = $this->rawRecord->getProperty()->work_data;



                if ($propertyInformation != null) {
                    foreach ($inspectionCommands as $command) {
                        $parsedCommand = substr($command, 8);

                        $parsedCommand = substr($parsedCommand, 0, -1);

                        $parsedCommand = explode('|', $parsedCommand);



                        if (isset($propertyInformation[$parsedCommand[0]])) {
                            $actualData =

                                object_get($propertyInformation[$parsedCommand[0]], $parsedCommand[1]);



                            if ($actualData == null) {
                                $this->source = str_replace($command, '&nbsp;', $this->source);
                            } else {
                                if (is_numeric($actualData)) {

                                    // $this->source = str_replace($command, e(nd_number_format($actualData)), $this->source);

                                    $this->source = str_replace($command, e(($actualData)), $this->source);
                                } else {
                                    $this->source = str_replace($command, e($actualData), $this->source);
                                }
                            }
                        }
                    }
                }



                $this->source = str_replace($inspectionCommands, [], $this->source);
            }
        }
    }



    /**

     * Compiles DIRECT commands.

     */

    private function compileDirectCommands()
    {
        $directCommands = [];

        preg_match_all(self::SRL_DATA_DIRECT_REGEX, $this->source, $directCommands);



        if (count($directCommands) > 0) {
            if (is_array($directCommands[0])) {
                $directCommands = $directCommands[0];



                if ($this->rawData != null) {
                    foreach ($directCommands as $command) {
                        $parsedCommand = substr($command, 7);

                        $parsedCommand = substr($parsedCommand, 0, -1);

                        $parsedCommand = explode(',', $parsedCommand);



                        // Clean up the command parts.

                        foreach ($parsedCommand as $key => $piece) {
                            $parsedCommand[$key] = trim($piece);
                        }



                        $data = object_get($this->rawData, $parsedCommand[0]);



                        if ($data != null) {
                            if (is_numeric($data)) {

                                //$data = nd_number_format($data);

                                $data = $data;
                            } else {
                                $data = htmlentities($data, ENT_QUOTES, 'UTF-8');
                            }
                        } else {

                            // Now we need to do our fancy default stuff.

                            if (strtolower($parsedCommand[1]) == 'null') {
                                $data = '&nbsp;';
                            } else {
                                if (is_numeric($parsedCommand[1])) {
                                    $data = $data;
                                } else {
                                    $data = htmlentities($parsedCommand[1], ENT_QUOTES, 'UTF-8');
                                }
                            }
                        }



                        $this->source = str_replace($command, $data, $this->source);
                    }
                }
            }
        }
    }



    protected $escapeMappings = ['\\{ESC}' => ''];



    /**

     * Returns the source with the raw data interlaced.

     *

     * @return mixed

     */

    public function srlPrint()
    {

        // Compile the PREPARED statements. DUN DUN DUN.

        $this->compilePreparedCommands();

        // Direct commands have the first chance at the compiler's data functions.

        $this->compileDirectCommands();

        $this->prepareConsumables();

        $this->formatSRLMappings();

        $this->formatMetaData();

        $this->formatGenerics();

        $this->printConsumables();



        // Property information is handled by the INSPECT command. This command is generally used in 'safe' situations

        // and appears last.

        $this->parsePropertyInformation();

        // Now we need to process the TRACK commands.

        $this->processTracks();



        $this->compileOdometerReadings();

        $this->compileSurfaceTypes();



        // Remove escaped mappings.

       //$this->source = str_replace('\\{ESC}', '', $this->source);

        $this->source = strtr($this->source, $this->escapeMappings);



        if (array_key_exists('noRowWrap', $this->options)) {
            $this->source = str_replace('<tr', '<tr data-rowtype="record"', $this->source);



            return $this->source;
        }



        return '<tr data-rowtype="record">' . $this->source . '</tr>';
    }
}
