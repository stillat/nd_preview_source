<?php namespace ParadoxOne\NDCounties\Reporting\SRL;

use ParadoxOne\NDCounties\Contracts\Reporting\SRL\SRLPrinterInterface;

class FooterSRLPrinter extends BaseSRLPrinter implements SRLPrinterInterface
{
    use \ParadoxOne\NDCounties\Reporting\SRL\Directives\TrackDirectiveTrait;
    use \ParadoxOne\NDCounties\Reporting\SRL\Directives\GetTrackDirectiveTrait;

    /**
     * The SRL source format.
     *
     * @var string
     */
    protected $source = '';

    protected $resetSource = '';

    /**
     * The printer's raw data.
     *
     * @var null
     */
    protected $rawData = null;

    /**
     * The report totals data mappings.
     *
     * @var array
     */
    protected $srlDataMappings = [
        'report.grandEmployeeRegularHours'   => 'grand_employee_hours_regular_worked',
        'report.grandEmployeeOvertimeHours'  => 'grand_employee_hours_overtime_worked',
        'report.grandEmployeeTotalHours'     => 'grand_employee_hours_total_worked',
        'report.grandBillingQuantity'        => 'grand_misc_billing_quantity',
        'report.grandMaterialsQuantity'      => 'grand_total_materials_quantity',
        'report.grandEquipmentUnitsQuantity' => 'grand_total_equipment_units_quantity',
        'report.grandFuelsQuantity'          => 'grand_total_fuels_quantity',
        'report.grandMaterialsCost'          => 'grand_total_materials_cost',
        'report.grandEquipmentUnitsCost'     => 'grand_total_equipment_units_cost',
        'report.grandFuelsCost'              => 'grand_total_fuels_cost',
        'report.grandFuelsEquipmentCost'     => 'grand_total_equipment_and_fuels_cost',
        'report.grandBillingCost'            => 'grand_total_misc_billing_cost',
        'report.grandPropertyCost'           => 'grand_total_property_cost',
        'report.grandEmployeeRegularCost'    => 'grand_total_employee_regular_cost',
        'report.grandEmployeeOvertimeCost'   => 'grand_total_employee_overtime_cost',
        'report.grandEmployeeTotalCost'      => 'grand_total_employee_combined_cost',
        'report.grandEmployeeTotalHours'     => 'grand_employee_hours_total_worked',
        'report.grandRecordTotal'            => 'grand_record_totals',

    ];

    protected $options = [];

    /**
     * Sets the options for the printer.
     * f
     *
     * @param array $options
     * @return mixed
     */
    public function setOptions(array $options)
    {
        $this->options = $options;
    }

    /**
     * Gets the options set.
     *
     * @return array
     */
    public function getOptions()
    {
        return $this->options;
    }

    /**
     * Sets the source for the printer.
     *
     * @param $source
     * @return mixed
     */
    public function setSource($source)
    {
        $this->source      = $source;
        $this->resetSource = $source;
    }

    public function setReportingSettings($settings)
    {
        $this->cachedFormattingSettings = $settings;
    }


    /**
     * Sets the raw data for the printer.
     *
     * @param $rawData
     * @return mixed
     */
    public function setRawData(&$rawData)
    {
        $this->rawData = $rawData;

        // Reset source.
        $this->source = $this->resetSource;
    }

    private function formatConsumable()
    {
        if (array_key_exists('consumableMode', $this->options)) {
            $this->srlDataMappings =
                $this->srlDataMappings + ['report.grandConsumableEquipmentCost' => 'grand_consumable_equipment_total'];
            $this->srlDataMappings =
                $this->srlDataMappings + ['report.grandConsumableFuelCost' => 'grand_consumable_fuel_total'];
            $this->srlDataMappings =
                $this->srlDataMappings + ['report.grandConsumableMaterialCost' => 'grand_consumable_material_total'];
            $this->srlDataMappings =
                $this->srlDataMappings + ['report.grandConsumableCost' => 'grand_consumable_total'];
            $this->srlDataMappings = $this->srlDataMappings +
                                     ['report.grandConsumableEquipmentQuantity' => 'grand_consumable_equipment_quantity'];
            $this->srlDataMappings =
                $this->srlDataMappings + ['report.grandConsumableFuelQuantity' => 'grand_consumable_fuel_quantity'];
            $this->srlDataMappings = $this->srlDataMappings +
                                     ['report.grandConsumableMaterialQuantity' => 'grand_consumable_material_quantity'];
            $this->srlDataMappings =
                $this->srlDataMappings + ['report.grandConsumableQuantity' => 'grand_consumable_quantity'];
        }
    }

    /**
     * Formats the SRL data mappings.
     *
     * @return void
     */
    private function formatSRLMappings()
    {
        $replaceMappings = [];

        foreach ($this->srlDataMappings as $mapping => $target) {
            $replaceMappings[$mapping] = '<span data-type="numeric">'.e(object_get($this->rawData, $target)).'</span>';
            // The mappings will always be numeric here.
            // $this->source = str_replace($mapping, e(nd_number_format(search_object_graph($this->rawData, $target))), $this->source);
        }
        $this->source = strtr($this->source, $replaceMappings);
    }

    /**
     * Returns the source with the raw data interlaced.
     *
     * @return mixed
     */
    public function srlPrint()
    {
        $this->formatConsumable();
        \Debugbar::startMeasure('footer', 'Footer');

        $this->formatSRLMappings();
        \Debugbar::stopMeasure('footer');

        $this->processGetTracks();

        if (array_key_exists('noRowWrap', $this->options)) {
            return $this->source;
        }

        return '<tr>' . $this->source . '</tr>';
    }
}
