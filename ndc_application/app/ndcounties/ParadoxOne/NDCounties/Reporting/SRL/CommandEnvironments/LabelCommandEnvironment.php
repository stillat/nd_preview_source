<?php namespace ParadoxOne\NDCounties\Reporting\SRL\CommandEnvironments;

class LabelCommandEnvironment
{
    public $date                       = 'Work Date';
    public $employeeTotalHoursOvertime = 'Overtime Hours';
    public $employeeTotalHoursRegular  = 'Regular Hours';
    public $employeeWageOvertime       = 'Overtime Rate';
    public $employeeWageRegular        = 'Regular Rate';
    public $employeeFringeOvertime     = 'Overtime Fringe Benefits';
    public $employeeFringeRegular      = 'Regular Fringe Benefits';
    public $employeeCostOvertime       = 'Overtime Cost';
    public $employeeCostRegular        = 'Regular Cost';
    public $employeeCost               = 'Employee Total Cost';

    public $fuelQuantity = 'Fuel Quantity';
    public $fuelTotal    = 'Fuel Cost';

    public $materialQuantity = 'Material Quantity';
    public $materialTotal    = 'Material Cost';

    public $propertyTotal   = 'Property Cost';
    public $billingRate     = 'Billing Rate';
    public $billingQuantity = 'Billing Quantity';
    public $billingTotal    = 'Billing Cost';

    public $equipmentTotalQuantity      = 'Equipment HR/MI';
    public $equipmentTotalCostWithFuels = 'Equipment Cost (with Fuels)';
    public $equipmentTotalCost          = 'Equipment Cost';

    public $total = 'Record Total';

    public $activityDescription = 'Activity Description';
    public $billingDescription  = 'Billing Description';

    public $employee   = 'Employee';
    public $activity   = 'Activity';
    public $property   = 'Property';
    public $department = 'Department';
    public $district   = 'District';
    public $project    = 'Project';
    public $fuel       = 'Fuel';
    public $material   = 'Material';
    public $equipment  = 'Equipment Unit';
    public $billing    = 'Billing';
}
