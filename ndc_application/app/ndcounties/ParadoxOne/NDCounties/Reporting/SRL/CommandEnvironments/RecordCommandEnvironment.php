<?php namespace ParadoxOne\NDCounties\Reporting\SRL\CommandEnvironments;

use ParadoxOne\NDCounties\Reporting\ReportManager;

class RecordCommandEnvironment
{
    /**
     * @var ReportManager
     */
    public $manager;

    public function date()
    {
        return substr($this->manager->recordData->work_date, 0, 10);
    }
}
