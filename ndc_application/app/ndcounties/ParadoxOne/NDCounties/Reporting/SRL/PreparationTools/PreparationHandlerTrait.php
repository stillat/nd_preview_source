<?php namespace ParadoxOne\NDCounties\Reporting\SRL\PreparationTools;

use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;

trait PreparationHandlerTrait
{
    /**
     * Holds the SRL Preparation values.
     *
     * @var null|array
     */
    protected $srlPreparationBankValues = null;

    protected $srlPreparationBankHolder = [];

    /**
     * Handles the SRL Prepare instructions.
     *
     * @param $srlPreparationData
     */
    private function handleSRLPrepareFunctions($srlPreparationData)
    {
        if ($srlPreparationData != null) {
            if (is_array($srlPreparationData)) {
                $this->srlPreparationBankValues = $srlPreparationData;
            }
        }

        /*
        if ($srlPreparationData != null && strlen($srlPreparationData) != 0 &&
            strtolower($srlPreparationData) != '<!--SKIP-->'
        )
        {
            $this->srlPreparationBankValues = json_decode($srlPreparationData);
        }
        */
    }

    /**
     * Builds the actual SRL preparation banks.
     *
     * @param $workEntryIDs
     */
    private function buildSRLPreparationBank($workEntryIDs)
    {
        // First check to see if preparation bank values even exist. If they do, let's continue.
        if ($this->srlPreparationBankValues != null) {
            foreach ($this->srlPreparationBankValues as $srlPreparation) {
                $builderInstance = DB::connection(\Tenant::getCurrentConnection())->table($srlPreparation->baseTable);

                if ($srlPreparation->joinRecordsTable == true) {
                    $builderInstance =
                        $builderInstance->join('work_entries as record', $srlPreparation->joinRecordsTableColumn, '=',
                                               'record.id');
                    if (count($workEntryIDs) > 0) {
                        $builderInstance = $builderInstance->where(function ($query) use ($workEntryIDs) {
                           $query->whereIn('record.id', $workEntryIDs);
                        });
                    }
                }

                if (property_exists($srlPreparation, 'joins')) {
                    if (count($srlPreparation->joins) > 0) {
                        foreach ($srlPreparation->joins as $join) {
                            $builderInstance = $builderInstance->join($join->start, $join->columnOne, $join->operator,
                                                                      $join->columnTwo);
                        }
                    }
                }

                // Handles the group by things.
                if (property_exists($srlPreparation, 'groups')) {
                    if (count($srlPreparation->groups) > 0) {
                        foreach ($srlPreparation->groups as $group) {
                            $builderInstance = $builderInstance->groupBy($group);
                        }
                    }
                }

                if (property_exists($srlPreparation, 'conditions')) {
                    if (count($srlPreparation->conditions) > 0) {
                        foreach ($srlPreparation->conditions as $condition) {
                            $builderInstance = $builderInstance->where(function ($query) use ($condition) {
                                $query->where($condition->column, $condition->operator, $condition->value);
                            });
                        }
                    }
                }

                $builderInstance = $builderInstance->select(DB::raw($srlPreparation->value));

                try {
                    $this->srlPreparationBankHolder[$srlPreparation->name] = $builderInstance->get();
                } catch (\Exception $e) {
                    lk($e);
                }

                if (count($this->srlPreparationBankHolder) > 0) {
                    App::make('stateManager')->setStorage('preparation_bank', $this->srlPreparationBankHolder);
                }
            }
        }
    }
}
