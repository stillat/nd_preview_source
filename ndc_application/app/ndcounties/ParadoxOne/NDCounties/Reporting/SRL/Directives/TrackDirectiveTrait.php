<?php namespace ParadoxOne\NDCounties\Reporting\SRL\Directives;

trait TrackDirectiveTrait
{
    private function processTracks()
    {
        $trackCommands = [];
        preg_match_all(self::SRL_STATE_TRACK_REGEX, $this->source, $trackCommands);

        if (count($trackCommands) > 0) {
            if (is_array($trackCommands[0])) {
                $trackCommands = $trackCommands[0];
                foreach ($trackCommands as $command) {
                    $parsedCommand = substr($command, 6);
                    $parsedCommand = substr($parsedCommand, 0, -1);
                    $parsedCommand = explode(',', $parsedCommand);

                    foreach ($parsedCommand as $key => $piece) {
                        $parsedCommand[$key] = trim($piece);
                    }

                    // Process the command.
                    $currentValue = $this->stateManager->getFromStorage($parsedCommand[0]);

                    // Process the operator.
                    switch ($parsedCommand[1]) {
                        // Normal mode.
                        case '=':
                            $currentValue = $parsedCommand[2];
                            break;
                        case '+':
                            $currentValue = $currentValue + $parsedCommand[2];
                            break;
                        case '-':
                            $currentValue = $currentValue - $parsedCommand[2];
                            break;
                        case '*':
                            $currentValue = $currentValue * $parsedCommand[2];
                            break;
                        case '/':

                            if ($parsedCommand[2] == 0) {
                                $currentValue = 0;
                                break;
                            }

                            $currentValue = $currentValue / $parsedCommand[2];
                            break;
                        case '%':
                            $currentValue = $currentValue % $parsedCommand[2];
                            break;

                        // Backwards mode. A little treat ;)
                        case '--':
                            $currentValue = $parsedCommand[2] - $currentValue;
                            break;
                        case '//':
                            if ($currentValue == 0) {
                                break;
                            }

                            $currentValue = $parsedCommand[2] / $currentValue;
                            break;
                        case '%%':
                            $currentValue = $parsedCommand[2] % $currentValue;
                            break;
                    }

                    $this->stateManager->setStorage($parsedCommand[0], $currentValue);


                    // After processing, TRACK commands should just be hidden.
                    $this->source = str_replace($command, '', $this->source);
                }
            }
        }
    }
}
