<?php namespace ParadoxOne\NDCounties\Reporting\SRL\Directives;

trait PreparedDirectiveTrait
{
    private function compilePreparedCommands()
    {
        $preparationBank = $this->stateManager->getFromStorage('preparation_bank');
        $currentIndex    = $this->stateManager->getFromStorage('_c_internal_track');
        if (count($preparationBank) > 0) {
            $preparedCommands = [];
            preg_match_all(self::SRL_STATE_PREPARED, $this->source, $preparedCommands);

            if (count($preparedCommands) > 0) {
                if (is_array($preparedCommands[0])) {
                    $preparedCommands = $preparedCommands[0];

                    foreach ($preparedCommands as $command) {
                        $parsedCommand = substr($command, 9);
                        $parsedCommand = substr($parsedCommand, 0, -1);
                        $parsedCommand = explode(',', $parsedCommand);

                        foreach ($parsedCommand as $key => $piece) {
                            $parsedCommand[$key] = trim($piece);
                        }

                        if (isset($preparationBank[$parsedCommand[0]])) {
                            if (isset($preparationBank[$parsedCommand[0]][$currentIndex])) {
                                $preparedData = $preparationBank[$parsedCommand[0]][$currentIndex];
                                $preparedData = object_get($preparedData, $parsedCommand[1]);

                                if ($preparedData == null || strlen($preparedData) == 0) {
                                    if (strtolower($parsedCommand[2]) == 'null') {
                                        $preparedData = '&nbsp;';
                                    } else {
                                        $preparedData == (0);
                                    }
                                } else {
                                    if (is_numeric($preparedData)) {
                                        $preparedData = ($preparedData);
                                    } else {
                                        $preparedData = htmlentities($preparedData, ENT_QUOTES, 'UTF-8');
                                    }
                                }

                                $this->source = str_replace($command, $preparedData, $this->source);
                            } else {
                                $this->source = str_replace($command, '', $this->source);
                            }
                        } else {
                            $this->source = str_replace($command, '', $this->source);
                        }
                    }
                }
            }
        }
    }
}
