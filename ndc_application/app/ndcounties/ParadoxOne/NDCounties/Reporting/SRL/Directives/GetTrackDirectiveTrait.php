<?php namespace ParadoxOne\NDCounties\Reporting\SRL\Directives;

trait GetTrackDirectiveTrait
{
    private function processGetTracks()
    {
        $getTrackCommands = [];
        preg_match_all(self::SRL_STATE_GET_TRACK, $this->source, $getTrackCommands);
        if (count($getTrackCommands) > 0) {
            if (is_array($getTrackCommands[0])) {
                $getTrackCommands = $getTrackCommands[0];

                foreach ($getTrackCommands as $command) {
                    $parsedCommand = substr($command, 9);
                    $parsedCommand = substr($parsedCommand, 0, -1);
                    $parsedCommand = explode(',', $parsedCommand);

                    foreach ($parsedCommand as $key => $piece) {
                        $parsedCommand[$key] = trim($piece);
                    }

                    $stateObject = $this->stateManager->getFromStorage($parsedCommand[0]);

                    // This will handle default values (only if the value is 0, or less, or non-numeric. You get it).
                    if ($stateObject == 0) {
                        if (is_numeric($parsedCommand[1])) {
                            $stateObject = $parsedCommand[1];
                        }
                    }

                    $this->source = str_replace($command, nd_number_format($stateObject), $this->source);
                }
            }
        }
    }
}
