<?php namespace ParadoxOne\NDCounties\Reporting\SRL\Directives;

use Illuminate\Support\Facades\App;

trait GetOdometerReadingDirectiveTrait
{
    /**
     * Compiles the odometer readings.
     */
    private function compileOdometerReadings()
    {
        $odometerReadingCommands = [];
        preg_match_all(self::SRL_GET_ODOMETER_COMMAND_REGEX, $this->source, $odometerReadingCommands);

        if (count($odometerReadingCommands) > 0) {
            if (is_array($odometerReadingCommands[0])) {
                $odometerReadingCommands = $odometerReadingCommands[0];
                $odometerReadings        = $this->stateManager->getFromStorage('odometer_readings');

                foreach ($odometerReadingCommands as $command) {
                    $parsedCommand = substr($command, 21);
                    $parsedCommand = substr($parsedCommand, 0, -1);
                    $parsedCommand = explode(',', $parsedCommand);

                    foreach ($parsedCommand as $key => $piece) {
                        $parsedCommand[$key] = trim($piece);
                    }

                    $odometerReadings = $odometerReadings[$parsedCommand[0]];
                    $odometerReadings =
                        array_pluck_where($odometerReadings, 'associated_record', intval($parsedCommand[1]));
                    $odometerReadings = array_pluck_where($odometerReadings, 'equipment_id', intval($parsedCommand[2]));

                    if (count($odometerReadings) > 0) {
                        if ($odometerReadings[0]->odometer_reading != null) {
                            $this->source =
                                str_replace($command, e($odometerReadings[0]->odometer_reading), $this->source);
                            continue;
                        }
                    }

                    $this->source = str_replace($command, '&nbsp;', $this->source);
                }
            }
        }
    }
}
