<?php namespace ParadoxOne\NDCounties\Reporting\SRL;

class CommandEnvironment
{
    /**
     * @var BaseSRLPrinter
     */
    protected $printer;

    protected $repairOdometerReadings = null;

    /**
     * Holds old tracked values.
     *
     * Do not use the state manager in this environment.
     *      *
     *
     * @var array
     */
    protected $tracks = [];

    protected $cachedPropertyData = [];

    public function setPrinter(&$printer)
    {
        $this->printer = $printer;
    }

    public function getSurfaceType()
    {
    }

    public function getPropertyData($propertyType, $data, $default = '')
    {
        $cacheKey = $this->printer->rawData->id . '_' . $propertyType . '_' . $data . '_' . $default;

        if (isset($this->cachedPropertyData[$cacheKey])) {
            return $this->cachedPropertyData[$cacheKey];
        }

        $propertyData = $this->printer->rawRecord->getProperty()->work_data;
        if (isset($propertyData[$propertyType])) {
            $propertyData = $propertyData[$propertyType];

            $value = null;

            if (property_exists($propertyData, $data)) {
                $value = $propertyData->{$data};
            }

            if ($value == null || strlen($value) == 0) {
                $value = $default;
            }
            if (is_string($value)) {
                $value = e($value);
            }

            $this->cachedPropertyData[$cacheKey] = $value;

            return $value;
        }
    }

    public function getEquipmentData($data, $default = '')
    {
        return $this->getPropertyData(2, $data);
    }

    public function getRoadData($data, $default = '')
    {
        return $this->getPropertyData(1, $data);
    }

    /**
     * Gets a value from the preparation bank.
     *
     * @param $preparationBank
     * @param $value
     * @param $default
     * @return mixed
     */
    public function getPreparedData($preparationBank, $value, $default = '')
    {
        if (isset($this->printer->constructor->srlPreparationBankHolder[$preparationBank])) {
            // At this point, the preparation bank value actually exists. Now we need to check
            // if the current report index exists within the preparation bank.
            $currentIndex = $this->getPos();
            if (isset($this->printer->constructor->srlPreparationBankHolder[$preparationBank][$currentIndex])) {
                $preparedData = $this->printer->constructor->srlPreparationBankHolder[$preparationBank][$currentIndex];

                $processValue = null;

                if (property_exists($preparedData, $value)) {
                    $processValue = $preparedData->{$value};
                }

                if ($processValue == null || strlen($processValue) == 0) {
                    return $default;
                }

                if (is_numeric($processValue)) {
                    return $processValue;
                }

                return e($processValue);
            }
        }
    }

    /**
     * Gets an odometer reading.
     *
     * @param $recordType   The record type. 1 is general usage. 2 is equipment repair.
     * @param $recordID     The record ID.
     * @param $equipmentID  The equipment unit ID.
     */
    public function getOdometerReading($recordType, $recordID, $equipmentID)
    {
        if ($this->repairOdometerReadings == null) {
            $this->repairOdometerReadings = $this->printer->reader->getOdometerReadings();
        }

        if (count($this->repairOdometerReadings) == 0) {
            return;
        }

        $currentReadings =
            array_pluck_where($this->repairOdometerReadings[$recordType], 'associated_record', intval($recordID));

        if (count($currentReadings) == 0) {
            return;
        }

        $currentReadings = array_pluck_where($currentReadings, 'equipment_id', intval($equipmentID));

        if (count($currentReadings) == 0) {
            return;
        }

        if (count($currentReadings) > 0 && $currentReadings[0]->odometer_reading != null) {
            return $currentReadings[0]->odometer_reading;
        }
    }

    /**
     * Gets some data from the report record.
     *
     * @param        $data
     * @param string $default
     * @return mixed
     */
    public function getData($data, $default = '')
    {
        if (property_exists($this->printer->rawData, $data)) {
            $value = $this->printer->rawData->{$data};

            if (is_numeric($value)) {
                return nd_number_format($value);
            }

            return e($value);
        }

        if (is_numeric($default)) {
            return nd_number_format($default);
        }

        return e($default);
    }

    /**
     * Gets a tracked value.
     *
     * Use internally to prevent unset errors.
     *
     * @param $name
     * @return int
     */
    private function internalGetValue($name)
    {
        if (!isset($this->tracks[$name])) {
            $this->tracks[$name] = 0;

            return 0;
        }

        return $this->tracks[$name];
    }

    /**
     * Tracks the current position of the report.
     *
     * This is the new way to track position, previously it was TRACK(_c_internal_track, +, 1)
     */
    public function trackPos()
    {
        $this->increment('_c_internal_track', 1);
    }

    /**
     * Gets the current position of the report.
     *
     * @return number
     */
    public function getPos()
    {
        return $this->getValue('_c_internal_track', 0);
    }

    /**
     * Gets a value that has been previously tracked.
     *
     * @param     $name
     * @param int $default
     * @return number
     */
    public function getValue($name, $default = 0)
    {
        if (!isset($this->tracks[$name])) {
            return $default;
        }

        return $this->tracks[$name];
    }

    /**
     * Decrements a value.
     *
     * @param $name
     * @param $value
     */
    public function decrement($name, $value)
    {
        $this->tracks[$name] = $this->internalGetValue($name) - $value;
    }

    /**
     * Increments a value.
     *
     * @param $name
     * @param $value
     */
    public function increment($name, $value)
    {
        $this->tracks[$name] = $this->internalGetValue($name) + $value;
    }

    /**
     * Tracks a value with the given operator by the amount.
     *
     * Using the 'double operators' such as '--' will reverse
     * the operation order.
     *
     * @param $name
     * @param $operator
     * @param $amount
     */
    public function track($name, $operator, $amount)
    {
        $value = $this->internalGetValue($name);

        switch ($operator) {
            case '=':
                $this->tracks[$name] = $amount;
                break;
            case '+':
                $this->tracks[$name] = $amount + $value;
                break;
            case '-':
                $this->tracks[$name] = $value - $amount;
                break;
            case '*':
                $this->tracks[$name] = $amount * $value;
                break;
            case '/':
                if ($amount == 0) {
                    $this->tracks[$name] = 0;
                    break;
                }
                $this->tracks[$name] = $value / $amount;
                break;
            case '%':
                $this->tracks[$name] = $value % $amount;
                break;
            case '--':
                $this->tracks[$name] = $amount - $value;
                break;
            case '//':
                if ($value == 0) {
                    $this->tracks[$name] = 0;
                    break;
                }
                $this->tracks[$name] = $amount / $value;
                break;
            case '%%':
                $this->tracks[$name] = $amount % $value;
                break;
        }
    }

    public function evaluate($expression)
    {
    }
}
