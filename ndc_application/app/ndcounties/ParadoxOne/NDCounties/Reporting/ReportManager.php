<?php namespace ParadoxOne\NDCounties\Reporting;

use ParadoxOne\NDCounties\Reporting\SRL\BaseSRLPrinter;
use ParadoxOne\NDCounties\Reporting\SRL\CommandEnvironments\LabelCommandEnvironment;
use ParadoxOne\NDCounties\Reporting\SRL\CommandEnvironments\RecordCommandEnvironment;

/**
 * Class ReportManager
 *
 * The ReportManager class is a facade to handle the complex interactions
 * with the underlying report constructors and SRL language components.
 *
 * @package ParadoxOne\NDCounties\Reporting
 */
class ReportManager
{
    use \ParadoxOne\NDCounties\Reporting\Managers\EquationManagerTrait;
    use \ParadoxOne\NDCounties\Reporting\Managers\OrganizationStrategyTrait;
    use \ParadoxOne\NDCounties\Reporting\Managers\ReportFiltersTrait;
    use \ParadoxOne\NDCounties\Reporting\Managers\GroupingManagerTrait;
    use \ParadoxOne\NDCounties\Reporting\Managers\GenericOrganizationStrategyTrait;
    use \ParadoxOne\NDCounties\Reporting\Managers\SpecialOrganizationStrategyTrait;
    use \ParadoxOne\NDCounties\Reporting\Managers\PreparationManagerTrait;
    use \ParadoxOne\NDCounties\Reporting\Managers\ConsumableModeManagerTrait;
    use \ParadoxOne\NDCounties\Reporting\Managers\ConstraintManagerTrait;
    use \ParadoxOne\NDCounties\Reporting\Managers\PropertyLimiterManagerTrait;
    use \ParadoxOne\NDCounties\Reporting\Managers\NormalModeManagerTrait;

    const SRL_DEFAULT_REPORT_RECORD_QUERY = 'TOTAL(employee.cost + fuel.total + material.total + property.total + billing.total + equipment.total.cost)';

    /**
     * @var BaseSRLPrinter
     */
    public $basePrinter;

    /**
     * @var \Twig_Environment
     */
    public $environment;

    public $reportName;

    public $report;

    public $reportBlocks;

    public $hasFooter = false;

    public $hasBody = false;

    public $hasHeader = false;

    public $isOverridden = false;

    public $recordData = null;

    public $recordCommandEnvironment;

    public $labelCommandEnvironment;

    public $startDate;

    public $endDate;

    public $engineInternalMode = 0;

    public $reader;

    public $reportSettings;

    private $reportID;

    public $breakDownVisible = true;

    public $materialFilters = null;
    public $materialMode    = false;

    public $request;

    /**
     * The report grouping column name.
     *
     * @var string
     */
    public $reportGroupingColumn = '';

    public function __construct()
    {
        $fs                = new \Twig_Loader_Filesystem(storage_path() . '/reporting/');
        $this->environment =
            new \Twig_Environment(new \Twig_Loader_Chain([$fs]), ['autoescape' => false, 'debug' => true]);
        $this->environment->addExtension(new \Twig_Extension_Debug());
        $this->basePrinter                       = new BaseSRLPrinter;
        $this->basePrinter->twigCompiler         = $this->environment;
        $this->recordCommandEnvironment          = new RecordCommandEnvironment;
        $this->labelCommandEnvironment           = new LabelCommandEnvironment;
        $this->recordCommandEnvironment->manager = &$this;
    }

    public function setReader(&$reader)
    {
        $this->reader = $reader;
        $this->reader->reader()->withDefaults();
    }

    public function setReportID($id)
    {
        $this->reportID = $id;
        // TODO: Get report from database.
    }

    public function setReport($reportName)
    {
        $this->reportName;
        $this->report       = $this->environment->loadTemplate($reportName);
        $this->reportBlocks = array_flip($this->report->getBlockNames());
        $this->hasFooter    = isset($this->reportBlocks['footer']);
        $this->hasHeader    = isset($this->reportBlocks['header']);
        $this->hasBody      = isset($this->reportBlocks['body']);
        $this->isOverridden = isset($this->reportBlocks['report']);
    }

    public function setReportDates($startDate, $endDate)
    {
        $this->startDate = $startDate;
        $this->endDate   = $endDate;
        $this->reader->reader()->setDateRange($startDate, $endDate);
    }

    public function setRecordData(&$data)
    {
        $this->recordData = $data;
    }

    protected $isConsumableMode = false;

    public function setIsConsumableMode($mode)
    {
        $this->isConsumableMode = $mode;
    }

    public function setEngineInternalMode($mode)
    {
        $this->engineInternalMode = $mode;

        // Engine modes 1-4 are consumable engine modes.
        if (in_array($mode, [1, 2, 3, 4])) {
            if ($this->materialFilters != null) {
                $this->setConsumableRestrictions(4, [$this->materialMode, $this->materialFilters]);
                $this->enableConsumableOverride();
            }
        }

        //if ($this->uiGroupColumn == 'consumable_id') {
        if ($this->isConsumableMode) {
            if ($this->organizationStrategy == 2) {
                $this->groupConsumableRecord([
                    $this->genericGroupingColumn,
                    'consumable_type',
                    'consumable_id',
                    'associated_type',
                    'associated_id'
                ]);
            } else {
                $this->groupConsumableRecord([
                    'consumable_type',
                    'consumable_id',
                    'associated_type',
                    'associated_id'
                ]);
            }

            $this->enableConsumableOverride();
        }
    }

    public $records = [];

    public function buildRecords()
    {
        if (count($this->propertyLimits) > 0) {
            $query  = $this->reader->reader()->getQuery();
            $limits = $this->propertyLimits;
            $query  = $query->where(function ($query) use ($limits) {
                $query->whereIn('prop.property_type', $limits);
            });
            $this->reader->reader()->setQuery($query);
        }

        $this->reader->reader()->setInternalMode($this->engineInternalMode);
        $this->basePrinter->setOptions($this->basePrinter->getOptions() +
                                       ['consumableMode' => $this->engineInternalMode]);

        if ($this->engineInternalMode == 0) {
            // doGatherForNormalMode handles both NORMAL reporting and
            // the gathering of special report records.
            $this->records = $this->doGatherForNormalMode();
        } elseif (in_array($this->engineInternalMode, [1, 2, 3, 4])) {
            $this->records = $this->doGatherForConsumableMode();
        } else {
            throw new ReportingException('Invalid reporting mode');
        }

        // TODO: Develop the new SRL prep.

        return $this->records;
    }

    /**
     * Prints the report's header.
     *
     * @return string
     */
    public function srlPrintHeader()
    {
        if ($this->hasHeader) {
            return $this->report->renderBlock('header', ['label' => &$this->labelCommandEnvironment]);
        }

        return '';
    }

    /**
     * Prints the report's footer.
     *
     * @return string
     */
    public function srlPrintFooter()
    {
        if ($this->hasFooter) {
            return $this->footerTemplate->render(['label' => &$this->labelCommandEnvironment]);
        }

        return '';
    }

    /**
     * Prints the report's body.
     *
     * @param $data
     * @return string
     */
    public function srlPrintBody(&$data = null)
    {
        if ($data !== null) {
            $this->recordData = $data;
        }

        if ($this->hasBody) {
            return $this->report->renderBlock('body',
                ['record' => &$this->recordCommandEnvironment, 'label' => &$this->labelCommandEnvironment]);
        }

        if ($this->isOverridden) {
            return $this->report->renderBlock('report',
                ['record' => &$this->recordCommandEnvironment, 'label' => &$this->labelCommandEnvironment]);
        }

        return '';
    }
}
