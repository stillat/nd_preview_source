<?php

namespace ParadoxOne\NDCounties\Reporting\Direct;

use Illuminate\Support\Facades\DB;
use ParadoxOne\NDCounties\Contracts\Reporting\DirectReportInterface;

class EquipmentUseDirectProvider extends AbstractDirectProvider implements DirectReportInterface
{

    private $reportQuery = '
SELECT (SELECT code
        FROM   properties
        WHERE  id = consumable_id)                             AS code,
       (SELECT name
        FROM   properties
        WHERE  id = consumable_id)                             AS name,
       Coalesce((SELECT Sum(total_property_cost)
                 FROM   work_entries AS we
                 WHERE  we.property_context = 1
                        AND we.property_id = work_consumables.consumable_id
                        AND we.id IN ( <@direct=records> )), 0)            AS total_repair,
       Sum(total_quantity)                                     AS total_quantity
       ,
       Sum(total_cost)                                         AS
       total_rent,
       Coalesce((SELECT Sum(wc.total_cost)
                 FROM   work_consumables AS wc
                 WHERE  wc.associated_type = 3
                        AND wc.associated_id = work_consumables.consumable_id
                        AND wc.work_entry_id IN ( <@direct=records> )), 0) AS total_fuel,
       ( Coalesce( ( SELECT Sum(total_property_cost) FROM work_entries AS we
         WHERE
         we.property_context = 1 AND we.property_id =
         work_consumables.consumable_id
         AND we.id IN (<@direct=records>)
         ), 0)
         + Coalesce(( SELECT Sum(wc.total_cost) FROM work_consumables AS wc
         WHERE
         wc.associated_type = 3 AND wc.associated_id =
         work_consumables.consumable_id
         AND
         wc.work_entry_id IN (<@direct=records>) ), 0) )                   AS
       total_fuel_and_repair,
       ( Coalesce( ( SELECT Sum(total_property_cost) FROM work_entries AS we
         WHERE
         we.property_context = 1 AND we.property_id =
         work_consumables.consumable_id
         AND we.id IN (<@direct=records>)
         ), 0)
         + Sum(total_quantity) + Sum(total_cost)
         + Coalesce(( SELECT Sum(wc.total_cost) FROM work_consumables AS wc
         WHERE
         wc.associated_type = 3 AND wc.associated_id =
         work_consumables.consumable_id
         AND
         wc.work_entry_id IN (<@direct=records>) ), 0) )                   AS total_cost
FROM   work_consumables
       INNER JOIN work_entries
               ON work_entries.id = work_consumables.work_entry_id
WHERE  work_entry_id IN ( <@direct=records> )
       AND work_consumables.consumable_type = 2
       AND ( total_equipment_units_quantity > 0
              OR total_property_cost > 0 )
GROUP  BY consumable_id
HAVING total_cost > 0
ORDER  BY code ASC;
';

	protected $reportRecords = [];

    public function getRecords()
    {
        if (count($this->workRecords) == 0) {
            return [];
        } else {
            $this->reportRecords = $this->getReportConnection()->select($this->prepareQuery($this->reportQuery));
			return $this->reportRecords;
        }
    }

	public function printHeaders()
	{
		return '
		<tr><th>Code</th>
		<th>Description</th>
		<th class="text-right">Repair Costs</th>
		<th class="text-right">Fuel Costs</th>
		<th class="text-right">Total Costs</th>
		<th class="text-right">Hr/Mi.</th>
		<th class="text-right">Total Rent</th></tr>
		';
	}

	public function printBody()
	{
		$reportBody = '';

		foreach ($this->reportRecords as $record) {
			$reportBody .= $this->formatRow('<tr><td>code</td><td>desc</td><td class="text-right"><span data-type="numeric">repair</span></td><td class="text-right"><span data-type="numeric">fuel</span></span></td><td class="text-right"><span data-type="numeric">total_costs</span></td><td class="text-right"><span data-type="numeric">hr_mi</span></td><td class="text-right"><span data-type="numeric">total_rent</span></td>', [
				'code' => e($record->code),
				'desc' => e($record->name),
				'repair' => $record->total_repair,
				'hr_mi' => $record->total_quantity,
				'fuel' => $record->total_fuel,
				'total_costs' => $record->total_fuel_and_repair,
				'total_rent' => $record->total_rent
			]);
		}

		return $reportBody;
	}

	public function printFooters()
	{
		return $this->formatRow('<tr><th></th><th></th><th class="text-right"><span data-type="numeric">repair</span></th><th class="text-right"><span data-type="numeric">fuel</span></span></th><th class="text-right"><span data-type="numeric">total_costs</span></th><th class="text-right"><span data-type="numeric">hr_mi</span></th><th class="text-right"><span data-type="numeric">total_rent</span></th>', [
			'repair' => array_sum(array_pluck($this->reportRecords, 'total_repair')),
			'hr_mi' => array_sum(array_pluck($this->reportRecords, 'total_quantity')),
			'fuel' => array_sum(array_pluck($this->reportRecords, 'total_fuel')),
			'total_costs' => array_sum(array_pluck($this->reportRecords, 'total_fuel_and_repair')),
			'total_rent' => array_sum(array_pluck($this->reportRecords, 'total_rent'))
		]);
	}


}