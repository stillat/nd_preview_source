<?php

namespace ParadoxOne\NDCounties\Reporting\Direct;

use ParadoxOne\NDCounties\Contracts\Reporting\DirectReportInterface;
use ParadoxOne\NDCounties\Reporting\ReportConstructor;
use Stillat\Database\Support\Facades\Tenant;
use Illuminate\Support\Facades\DB;

abstract class AbstractDirectProvider implements DirectReportInterface
{

    /**
     * The work records.
     *
     * @var array
     */
    protected $workRecords = [];

    /**
     * The report constructor instance.
     *
     * @var ReportConstructor
     */
    protected $reportConstructor = null;

    protected $activityFilters = [];

    protected $departmentFilters = [];

    protected $districtFilters = [];

    protected $employeeFilters = [];

    protected $equipmentUnitFilters = [];

    protected $materialFilters = [];

    protected $projectFilters = [];

    protected $roadFilters = [];

    protected $reportScopeFilters = [];

    protected $propertyFilters = [];

    public function setWorkRecordIDs($workRecords)
    {
        $this->workRecords = $workRecords;
    }

    /**
     * Gets a string that is safe to use in an SQL whereIn query.
     *
     * @return string
     */
    protected function getSafeWorkRecordString()
    {
        $workRecords = [];

        foreach ($this->workRecords as $record) {
            try {
                if (is_int((int)$record)) {
                    $workRecords[] = $record;
                }
            } catch (\Exception $e) {
                continue;
            }
        }

        return implode(',',$workRecords);
    }

    /**
     * Prepares a direct query.
     *
     * @param $query
     * @return mixed
     */
    protected function prepareQuery($query)
    {
        $query = str_replace('<@direct=records>', $this->getSafeWorkRecordString(), $query);
        return $query;
    }

    public function setReportConstructor(&$reportConstructor)
    {
        $this->reportConstructor = $reportConstructor;
    }

    protected function getReportConnection()
    {
        return DB::connection(Tenant::getCurrentConnection());
    }

    protected function formatRow($format, $data)
    {
        return strtr($format, $data);
    }

    public function setActivityFilters($filters)
    {
        $this->activityFilters = $filters;
    }

    public function setDepartmentFilters($filters)
    {
        $this->departmentFilters = $filters;
    }

    public function setDistrictFilters($filters)
    {
        $this->districtFilters = $filters;
    }

    public function setEmployeeFilters($filters)
    {
        $this->employeeFilters = $filters;
    }

    public function setEquipmentFilters($filters)
    {
        $this->equipmentUnitFilters = $filters;
    }

    public function setMaterialFilters($filters)
    {
        $this->materialFilters = $filters;
    }

    public function setProjectFilters($filters)
    {
        $this->projectFilters = $filters;
    }

    public function setRoadFilters($filters)
    {
        $this->roadFilters = $filters;
    }

    public function setReportScopeFilters($filters)
    {
        $this->reportScopeFilters = $filters;
    }

    public function setPropertyFilters($filters)
    {
        $this->propertyFilters = $filters;
    }


}