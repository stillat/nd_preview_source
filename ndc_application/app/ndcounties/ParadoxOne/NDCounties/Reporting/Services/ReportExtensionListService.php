<?php namespace ParadoxOne\NDCounties\Reporting\Services;

use Reporting\MetaDefinition;

class ReportExtensionListService
{
    /**
     * Returns a collection of merged reports.
     *
     * @param  $collection
     * @return array
     */
    public static function getList(&$collection)
    {
        $newCollection = [];

        foreach ($collection as $report) {
            if (strlen($report->report_extends_uid) == 0) {
                $newCollection[] = MetaDefinition::mapFromStandardClass($report);
            } else {
                $secondaryReport = array_pluck_where($collection, 'report_uid', $report->report_extends_uid);

                if (is_array($secondaryReport) && count($secondaryReport) > 0) {
                    $extensionService = new ReportExtensionService;
                    $factoryModel = MetaDefinition::mapFromStandardClass($report);
                    $secondaryReport = MetaDefinition::mapFromStandardClass($secondaryReport[0]);
                    $newCollection[] = $extensionService->extend($factoryModel, $secondaryReport);
                }
            }
        }

        return $newCollection;
    }
}
