<?php namespace ParadoxOne\NDCounties\Reporting\Services;

class ReportExtensionService
{
    private $protectedSettings = ['internal_report_mode', 'consumable_grouping_ui', 'report_iterator', 'directProvider'];

    private function extendBasicSRLDefs(&$primary, &$secondary)
    {
        if (!str_contains($primary->srl_header, '<!--OVERRIDE-BASE-->')) {
            $primary->srl_header = $secondary->srl_header;
        }
        if (!str_contains($primary->srl_body, '<!--OVERRIDE-BASE-->')) {
            $primary->srl_body = $secondary->srl_body;
        }
        if (!str_contains($primary->srl_footer, '<!--OVERRIDE-BASE-->')) {
            $primary->srl_footer = $secondary->srl_footer;
        }


    }

    private function mergeSettings($firstSettingsArray, $secondSettingsArray)
    {
        $mergedSettingsArray = [];
        $firstValues = [];
        $secondValues = [];

        foreach ($firstSettingsArray as $setting) {
            $firstValues[] = $setting->name;
        }

        foreach ($secondSettingsArray as $setting) {
            $secondValues[] = $setting->name;
        }


        // Remove any "protected settings" from the first values array.
        foreach ($this->protectedSettings as $protected) {
            if (isset($firstValues[$protected])) {
                unset($firstValues[$protected]);
            }
           // unset($firstValues[array_search($protected, $firstValues)]);
        }

        // We now need to merge protected values from the extended report.
        foreach ($this->protectedSettings as $protected) {
            $value = array_pluck_where($secondSettingsArray, 'name', $protected);

            if (is_array($value) && count($value) > 0) {
                $mergedSettingsArray[] = $value[0];
            }
        }

        // Now we need to merge the rest of the settings.
        foreach ($firstValues as $value) {
            $potentialSetting = array_pluck_where($firstSettingsArray, 'name', $value);

            if (is_array($potentialSetting) && count($potentialSetting) > 0) {
                if (count($potentialSetting) > 0) {
                    $mergedSettingsArray[] = (object) $potentialSetting[0];
                } else {
                    continue;
                }
            }
        }


        return $mergedSettingsArray;
    }

    public function extend(&$primaryReport, &$secondaryReport)
    {
        $primaryReport->baseReport = $secondaryReport;

        $primarySettings   = json_decode($primaryReport->getSettings());
        $secondarySettings = json_decode($secondaryReport->getSettings());

        // Merge some settings.
        $primarySettings->preparation   = $secondarySettings->preparation;
        $primarySettings->alert_message = $secondarySettings->alert_message;

        if (property_exists($secondarySettings, 'directProvider')) {
            $primarySettings->directProvider = $secondarySettings->directProvider;
        }

        $primarySettings->settings      =
            $this->mergeSettings($primarySettings->settings, $secondarySettings->settings);

        $this->extendBasicSRLDefs($primaryReport, $secondaryReport);
        $primaryReport->srl_settings = json_encode($primarySettings);


        return $primaryReport;
    }
}
