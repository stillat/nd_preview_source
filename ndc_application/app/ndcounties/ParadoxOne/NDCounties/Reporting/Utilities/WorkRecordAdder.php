<?php namespace ParadoxOne\NDCounties\Reporting\Utilities;

use Carbon\Carbon;
use ParadoxOne\NDCounties\Database\Entities\WorkEntries\WorkEntry;

class WorkRecordAdder
{
    /**
     * The generic name that will be used for related data names, descriptions, etc.
     *
     * @var string
     */
    protected $genericName = 'OTHER';

    /**
     * The various properties that will be added together.
     *
     * @var array
     */
    protected $propertiesToAdd = [
      'employee_hours_regular_worked',
      'employee_hours_overtime_worked',
      'misc_milling_quantity',
      'total_materials_quantity',
      'total_equipment_units_quantity',
      'total_fuels_quantity',
      'total_materials_cost',
      'total_equipment_units_cost',
      'total_fuels_cost',
      'total_equipment_and_fuels_cost',
      'total_misc_billing_cost',
      'total_property_cost',
      'total_employee_regular_cost',
      'total_employee_overtime_cost',
      'total_employee_combined_cost'
    ];

    public function setGenericName($name)
    {
        $this->genericName = $name;
    }

    public function getGenericName()
    {
        return $this->genericName;
    }

    /**
     * Adds work records together and returns one, consolidated work record.
     *
     * @param $workRecords
     * @return mixed
     */
    public function addWorkRecords(&$workRecords)
    {
        if ($workRecords == null) {
            return $this->getEmptyRecord();
        }

        $startRecord = $this->getRecordBase();

        // The most genius adding machine ever seen.
        foreach ($workRecords as $record) {
            foreach ($this->propertiesToAdd as $property) {
                $startRecord->{$property} = object_get($startRecord, $property, 0) + object_get($record->rawData, $property, 0);
            }
        }

        return new WorkEntry($startRecord, [], [], $this->getPropertyBase());
    }

    /**
     * Returns a new record base.
     *
     * @return \stdClass
     */
    protected function getRecordBase()
    {
        $newRawRecord = new \stdClass();
        // Here we need to build a raw work record from scratch.
        $newRawRecord->work_date                         = Carbon::now()->toDateString();
        $newRawRecord->id                                = -1;
        $newRawRecord->activity_description              = '';
        $newRawRecord->employee_id                       = -1;
        $newRawRecord->employee_historic_wage_regular    = 0;
        $newRawRecord->employee_historic_wage_overtime   = 0;
        $newRawRecord->employee_historic_fringe_overtime = 0;
        $newRawRecord->employee_historic_fringe_regular  = 0;
        $newRawRecord->exclude_from_report               = 0;
        $newRawRecord->activity_id                       = -1;
        $newRawRecord->activity_code                     = $this->genericName;
        $newRawRecord->activity_name                     = $this->genericName;
        $newRawRecord->district_id                       = -1;
        $newRawRecord->district_name                     = $this->genericName;
        $newRawRecord->district_code                     = $this->genericName;
        $newRawRecord->department_id                     = -1;
        $newRawRecord->department_name                   = $this->genericName;
        $newRawRecord->department_code                   = $this->genericName;
        $newRawRecord->project_id                        = -1;
        $newRawRecord->project_name                      = $this->genericName;
        $newRawRecord->project_code                      = $this->genericName;
        $newRawRecord->employee_code                     = $this->genericName;
        $newRawRecord->employee_first_name               = $this->genericName;
        $newRawRecord->employee_last_name                = $this->genericName;
        $newRawRecord->employee_middle_name              = $this->genericName;
        $newRawRecord->property_context                  = 0;
        $newRawRecord->misc_billing_quantity             = 0;
        $newRawRecord->misc_billing_rate                 = 0;
        $newRawRecord->misc_unit_id                      = -1;
        $newRawRecord->misc_description                  = '';
        $newRawRecord->total_materials_quantity          = 0;
        $newRawRecord->total_materials_cost              = 0;
        $newRawRecord->total_fuels_quantity              = 0;
        $newRawRecord->total_fuels_cost                  = 0;
        $newRawRecord->total_equipment_and_fuels_cost    = 0;
        $newRawRecord->total_equipment_units_quantity    = 0;
        $newRawRecord->total_equipment_units_cost        = 0;
        $newRawRecord->total_misc_billing_cost           = 0;
        $newRawRecord->total_employee_regular_cost       = 0;
        $newRawRecord->total_employee_overtime_cost      = 0;
        $newRawRecord->total_employee_combined_cost      = 0;
        $newRawRecord->employee_hours_regular_worked     = 0;
        $newRawRecord->employee_hours_overtime_worked    = 0;
        $newRawRecord->property_id                       = -1;
        $newRawRecord->recordTotal                       = 0;


        return $newRawRecord;
    }

    private function getPropertyBase()
    {
        $newProperty                   = new \stdClass();
        $newProperty->id               = -1;
        $newProperty->property_type    = 0;
        $newProperty->code             = $this->genericName;
        $newProperty->description      = '';
        $newProperty->name             = $this->genericName;
        $newProperty->exlusively_owned = 0;
        $newProperty->work_data        = null;

        return $newProperty;
    }

    /**
     * Returns a new empty record.
     *
     * @return WorkEntry
     */
    public function getEmptyRecord()
    {
        return new WorkEntry($this->getRecordBase(), [], [], $this->getPropertyBase());
    }
}
