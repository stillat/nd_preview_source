<?php namespace ParadoxOne\NDCounties\Processes;

class ProcessManager
{
    const PROCESS_NONE                    = 0;
    const PROCESS_DATABASE_BACKUP_CREATE  = 01;
    const PROCESS_DATABASE_BACKUP_RESTORE = 01;

    /**
     * Determines if a process is a database related process.
     *
     * @param $process
     * @return bool
     */
    public static function isDatabaseProcess($process)
    {
        return in_array($process, [
            self::PROCESS_DATABASE_BACKUP_RESTORE,
            self::PROCESS_DATABASE_BACKUP_CREATE
        ]);
    }
}
