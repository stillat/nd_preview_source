<?php namespace ParadoxOne\NDCounties\Site;

use Stillat\Menu\Renderers\DefaultRenderer;

class MenuRenderer extends DefaultRenderer
{
    /**
     * Renders an item's opening block
     *
     * @param  string array $options
     * @return string
     */
    public function renderItemOpen(array $attributes)
    {
        $classString = '';

        if ($attributes['isActive']) {
            // If the current menu item is active, lets create
            // an opening list-item element and set its class to active.
            $classString .= 'active';
        }

        if ($attributes['text'] == 'Log In') {
            $classString .= ' block-button ml15';
        }

        // If the current menu item is not active,
        // let's just create an opening list-item element.
        return '<li class="' . $classString . '">';
    }

    /**
     * Renders an item's body
     *
     * @param  string array $options
     * @return string
     */
    public function renderItemBody(array $attributes)
    {
        return '<a href="' . $attributes['href'] . '">' . $attributes['text'] . '</a>';
    }
}
