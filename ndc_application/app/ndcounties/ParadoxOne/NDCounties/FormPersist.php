<?php namespace ParadoxOne\NDCounties;

use Exception;
use Illuminate\Support\Facades\Cache;

class FormPersist
{
    /**
     * The current active account.
     *
     * @var int
     */
    protected $activeAccount;

    public function __construct($activeAccount = null)
    {
        $this->activeAccount = $activeAccount;
    }

    public function setActiveAccount($activeAccount)
    {
        $this->activeAccount = $activeAccount;
    }

    protected function buildKey($keyName)
    {
        if ($this->activeAccount == null) {
            throw new Exception('The active account cannot be null');
        }

        return $this->activeAccount . '_persistedForm' . $keyName;
    }

    public function persist($keyName, $value, $name)
    {
        $cacheKey = $this->buildKey($keyName);

        $persistedValues = Cache::get($cacheKey, null);

        if ($persistedValues == null) {
            $persistedValues = array();
        }

        if (count($persistedValues) == 10) {
            array_shift($persistedValues);
        }

        $persistedValues[$value] = $name;

        Cache::forever($cacheKey, $persistedValues);

        return $persistedValues;
    }

    public function getData($keyName)
    {
        $cacheKey = $this->buildKey($keyName);

        $data = Cache::get($cacheKey, null);

        if ($data !== null) {
            $data = array_reverse($data, true);
        }

        return $data;
    }

    public function forget($keyName)
    {
        Cache::forget($this->buildKey($keyName));
    }
}
