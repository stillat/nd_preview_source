<?php namespace ParadoxOne\NDCounties;

class Resurrect
{
    protected $recentData = null;

    public function __construct(array $persistedData = array())
    {
        $this->recentData = reset($persistedData);
    }

    public function setData(array $persistedData)
    {
        $this->recentData = reset($persistedData);
    }

    public function get($key)
    {
        return 'data-resurrect="' . $this->recentData->{$key} . '"';
    }
}
