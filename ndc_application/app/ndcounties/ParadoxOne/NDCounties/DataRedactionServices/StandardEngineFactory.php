<?php

namespace ParadoxOne\NDCounties\DataRedactionServices;

use ParadoxOne\NDCounties\DataRedactionServices\Modules\SSNModule;

class StandardEngineFactory
{
    public static $engine = null;

    public static function getEngine()
    {
        if (self::$engine == null) {
            self::$engine = new Engine;
            self::$engine->addModule(new SSNModule);
        }

        return self::$engine;
    }
}
