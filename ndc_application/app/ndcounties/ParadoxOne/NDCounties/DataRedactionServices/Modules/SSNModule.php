<?php

namespace ParadoxOne\NDCounties\DataRedactionServices\Modules;

use ParadoxOne\NDCounties\Contracts\DataRedaction\Module;

class SSNModule implements Module
{
    public function redact($data)
    {
        $redacted = preg_replace('#\b[A-Za-z0-9]{3}-[A-Za-z0-9]{2}-[0-9]{4}\b#', '', $data);
        if ($redacted == null) {
            return $data;
        }
        return $redacted;
    }
}
