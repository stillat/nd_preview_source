<?php

namespace ParadoxOne\NDCounties\DataRedactionServices;

use ParadoxOne\NDCounties\Contracts\DataRedaction\Module;

class Engine
{
    protected $redactionModules = [];

    public function addModule(Module $module)
    {
        $this->redactionModules[] = $module;
    }

    public function redact($data)
    {
        $tempData = $data;

        /** @var Module $module */
        foreach ($this->redactionModules as $module) {
            $tempData = $module->redact($tempData);
        }

        return $tempData;
    }
}
