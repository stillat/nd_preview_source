<?php namespace ParadoxOne\NDCounties;

use Illuminate\Support\ServiceProvider;

class InteractionServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->bind('formPersister', function () {
            return new FormPersist;
        });
    }
}
