<?php namespace ParadoxOne\NDCounties\FlatFile;

use ParadoxOne\NDCounties\Contracts\Reporting\ReportEngineModesRepositoryInterface;

class ReportEngineModesRepository implements ReportEngineModesRepositoryInterface
{
    /**
     * Returns a collection of reporting engine internal modes.
     *
     * @return mixed
     */
    public function getReportingModes()
    {
        return [
            0 => 'Report Engine Default',
            1 => 'Consumable Engine'
        ];
    }
}
