<?php namespace ParadoxOne\NDCounties\FlatFile;

use ParadoxOne\NDCounties\Contracts\Finance\TransactionStatusRepositoryInterface;

class TransactionStatusRepository implements TransactionStatusRepositoryInterface
{
    public function getStatuses()
    {
        return array(
            0 => 'Pending',
            1 => 'Approved',
            2 => 'Canceled'
        );
    }
}
