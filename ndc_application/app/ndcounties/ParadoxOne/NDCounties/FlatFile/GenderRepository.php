<?php namespace ParadoxOne\NDCounties\FlatFile;

use ParadoxOne\NDCounties\Contracts\GenderRepositoryInterface;

class GenderRepository implements GenderRepositoryInterface
{
    public function getGenders()
    {
        return array(
            '0' => 'Not Specified',
            '1' => 'Male',
            '2' => 'Female'
        );
    }
}
