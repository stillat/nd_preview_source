<?php namespace ParadoxOne\NDCounties\FlatFile;

use ParadoxOne\NDCounties\Contracts\Reporting\ReportCategoryRepositoryInterface;

class ReportCategoryRepository implements ReportCategoryRepositoryInterface
{
    /**
     * Gets the report categories.
     *
     * @return mixed
     */
    public function getCategories()
    {
        return [
            0 => 'General Reports',
            1 => 'Summary Reports',
            2 => 'Road Reports',
            3 => 'Equipment Reports',
            4 => 'Fuel Reports',
            5 => 'Other Reports',
            6 => 'My Reports'
        ];
    }
}
