<?php namespace ParadoxOne\NDCounties\ErrorManagement;

use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Auth;
use ParadoxOne\NDCounties\Database\Repositories\AccountRepository;

class InvalidAccountManager
{
    public $accountsRepository;

    public function __construct(AccountRepository $accountsRepository)
    {
        $this->accountsRepository = $accountsRepository;
    }

    public static function isInvalidAccountManager(\Exception $exception)
    {
        if (Str::contains(strtolower($exception->getMessage()), ['Unknown database', 'unknown database'])) {
            Log::error($exception);
            return true;
        }

        return false;
    }

    public static function handle()
    {
        $instance = App::make('ParadoxOne\NDCounties\ErrorManagement\InvalidAccountManager');

        $serviceAccounts = array();

        if (Auth::user()->admin == false) {
            $serviceAccounts = $instance->accountsRepository->getAccounts(Auth::user()->id)->get();
        } else {
            $serviceAccounts = $instance->accountsRepository->getAllAccounts()->get();
        }

        return View::make('errors.invalid_account')->with('serviceAccounts', $serviceAccounts)->with('user', Auth::user());
    }
}
