<?php

namespace ParadoxOne\NDCounties\ErrorManagement;

use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;

class DatabaseConnectionLostManager
{
    public static function databaseConnectionLost(\Exception $exception)
    {
        if (Str::contains(strtolower($exception->getMessage()), ['sqlstate[hy000]', '[2002]', 'target machine actively refused it'])) {
            Log::error($exception);
            return true;
        }

        return false;
    }
}
