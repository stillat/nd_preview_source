<?php namespace ParadoxOne\NDCounties;

use Input;

class IdentificationCode
{
    /**
     * Generates a UUID4 string
     *
     * @return string
     */
    public static function UUID4()
    {
        $data = openssl_random_pseudo_bytes(16);

        $data[6] = chr(ord($data[6]) & 0x0f | 0x40); // set version to 0100
        $data[8] = chr(ord($data[8]) & 0x3f | 0x80); // set bits 6-7 to 10

        return vsprintf('%s%s-%s-%s-%s-%s%s%s', str_split(bin2hex($data), 4));
    }

    /**
     * Creates a new code for a record.
     *
     * @var    string $inputCode The name of the input field.
     * @return string
     */
    public static function prepare($inputCode = 'code')
    {
        if (Input::has($inputCode)) {
            if (strlen(Input::get($inputCode)) == 0) {
                return self::generateRandom();
            } else {
                return Input::get($inputCode);
            }
        } else {
            return self::generateRandom();
        }
    }

    /**
     * Creates a new code for a record.
     *
     * @var    string $inputCode The name of the input field.
     * @return string
     */
    public static function prepareFrom($inputCode, $length = 5)
    {
        if ($inputCode) {
            if (strlen($inputCode) == 0) {
                return self::generateRandom($length);
            } else {
                return $inputCode;
            }
        } else {
            return self::generateRandom($length);
        }
    }

    /**
     * Returns a randomly generated string.
     *
     * @return string
     */
    public static function generateRandom($length = 5)
    {
        return \strtoupper(\str_random($length));
    }
}
