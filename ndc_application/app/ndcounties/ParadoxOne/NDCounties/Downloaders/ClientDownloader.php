<?php namespace ParadoxOne\NDCounties\Downloaders;

class ClientDownloader
{
    public static function download($file, $returnBytes = false, $chunkSize = 1048576)
    {
        $buffer = '';
        $count = 0;

        $handle = \fopen($file, 'rb');

        if ($handle === false) {
            return false;
        }

        while (!\feof($handle)) {
            $buffer = \fread($handle, $chunkSize);
            echo $buffer;
            // Don't flush output buffer when it hasn't been started.
            // \ob_flush();
            \flush();
            if ($returnBytes) {
                $count += \strlen($buffer);
            }
        }

        $status = \fclose($handle);

        if ($returnBytes and $status) {
            return $count;
        }

        return $status;
    }
}
