<?php

/*
|--------------------------------------------------------------------------
| Security Routes
|--------------------------------------------------------------------------
|
| This keeps the routes file clean.
|
*/
Route::post('update-password/{token}', 'HomeController@postUpdatePassword');

Route::get('reset-password/{token}', 'HomeController@getUpdatePassword');
Route::get('reset-password', 'HomeController@getResetPassword');
Route::post('reset-password', 'HomeController@postResetPassword');
Route::get('authorize', array('as' => 'authorize', 'uses' => 'HomeController@getAuthorizationCode'));
Route::get('login', array('as' => 'login', 'uses' => 'HomeController@getLogin'));
Route::get('logout', array('as' => 'logout', 'uses' => 'App\DashboardController@getSignout'))->after('invalidate-browser-cache');
Route::post('login', 'HomeController@postLogin');
Route::post('authorize', 'HomeController@postAuthorizationCode');
