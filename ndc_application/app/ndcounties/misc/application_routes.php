<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

// This little if check will disable the sandbox on production servers.
use Illuminate\Support\Facades\Redirect;

if (App::environment() != 'production') {
    Route::controller('sandbox', '\SandboxController');
}

Route::group(array('before' => 'auth', 'namespace' => 'App'), function () {
    // This piece of code is just sharing some information with the views that the entire
    // application will use. This piece of code is essential for the correct working of the
    // system.
    View::composer('layouts.master', 'ParadoxOne\NDCounties\UI\Composers\AppComposer');
    View::composer('app.reporting.view', 'ParadoxOne\NDCounties\UI\Composers\AppComposer');
    View::composer('data_tools.master', 'ParadoxOne\NDCounties\UI\Composers\DataToolsMenuComposer');
    View::composer('app.settings.master', 'ParadoxOne\NDCounties\UI\Composers\SettingsMenuComposer');
    View::composer(['app.fuels.partials.table', 'app.materials.partials.table'],
        'ParadoxOne\NDCounties\UI\Composers\InventorySettingsComposer');
    View::composer('app.consumables.inventory_detail',
        'ParadoxOne\NDCounties\UI\Composers\InventoryManagementComposer');
    View::composer('app.dashboard_widgets.activity_stream',
        'ParadoxOne\NDCounties\UI\Composers\ActivityStreamComposer');
    $sharer = App::make('\ParadoxOne\NDCounties\UI\Composers\ViewSharer');
    $sharer->share();
    // End of the view sharing code.

    Route::cache(__FILE__, function () {

        Route::controller('service/accounts', 'ServiceAccountController');

        Route::group(array('prefix' => 'utility'), function () {
            Route::controller('inspector', 'Inspector\InspectorController');
        });

        Route::group(array('prefix' => 'consumables'), function () {
            Route::controller('utilities', 'Consumables\ConsumableUtilitiesController');
        });

        Route::group(array('prefix' => 'data-tools'), function () {
            Route::controller('backup-and-restore', 'DataTools\BackupController');
            Route::controller('reassign-records', 'DataTools\ReassignRecordsController');
            Route::controller('export', 'DataTools\DataExportController');
        });

        Route::group(array('prefix' => 'finances'), function () {
            Route::controller('transaction', 'Finances\Transactions\TransactionController');

            // Disabling the suppliers.
            // Route::resource('suppliers.notes', 'Finances\SupplierNotesController');
            // Route::resource('suppliers', 'Finances\SuppliersController');

            Route::resource('categories', 'Finances\AccountsCategoryController', array('except' => array('view')));
            Route::resource('accounts', 'Finances\AccountsController');

            Route::get('invoices/change-accounts/{id}', 'Finances\Invoices\InvoiceController@getUpdateAccounts');
            Route::post('invoices/change-accounts/{id}', 'Finances\Invoices\InvoiceController@postUpdateAccounts');

            Route::get('invoices/change-number/{id}', 'Finances\Invoices\InvoiceController@getUpdateInvoiceNumber');
            Route::post('invoices/change-number/{id}', 'Finances\Invoices\InvoiceController@postUpdateInvoiceNumber');

            Route::get('invoices/unlock/{id}', 'Finances\Invoices\InvoiceController@getUnlockInvoice');
            Route::get('invoices/lock/{id}', 'Finances\Invoices\InvoiceController@getLockInvoice');
            Route::post('invoices/unlock/{id}', 'Finances\Invoices\InvoiceController@postUnlockInvoice');
            Route::post('invoices/lock/{id}', 'Finances\Invoices\InvoiceController@postLockInvoice');

            Route::resource('invoices.payments', 'Finances\Invoices\InvoicePaymentsController');
            Route::resource('invoices.records', 'Finances\Invoices\InvoiceRecordsController', [
                'except' => ['edit', 'create', 'update', 'destroy', 'store']
            ]);
            Route::get('invoices/search', 'Finances\Invoices\InvoiceController@getSearch');
            Route::post('invoices/search', 'Finances\Invoices\InvoiceController@postSearch');
            Route::resource('invoices', 'Finances\Invoices\InvoiceController',
                array('except' => array('edit', 'create', 'update', 'destroy', 'store')));
            Route::resource('tax-rates', 'Finances\Taxes\TaxesController');
            Route::controller('/', 'Finances\DashboardController');
        });

        Route::group(array('prefix' => 'settings'), function () {
            Route::controller('account', 'Settings\AccountSettingsController');
            Route::resource('reports/total-blocks', 'Settings\ReportTotalBlocksController');
            Route::resource('reports/equations', 'Settings\ReportEquationsController');
            Route::controller('reports', 'Settings\ReportSettingsController');
            Route::controller('user', 'Settings\UserSettingsController');
        });

        Route::group(array('prefix' => 'search'), function () {
            Route::controller('provider.json', 'Search\InlineProviderController');
            Route::controller('existence', 'Search\ExistenceController');
            Route::controller('sort', 'Search\SortingController');
            Route::controller('r', 'Search\SearchController');
            Route::controller('/', 'Search\SuggestionController');
        });



        Route::resource('road-surface-types', 'Properties\Roads\SurfaceTypesController');
        Route::resource('property-categories', 'Properties\PropertyCategoriesController');
        Route::resource('property-types', 'Properties\PropertyTypesController');
        Route::resource('departments', 'DepartmentsController');
        Route::resource('activities', 'Employees\ActivityController');
        Route::resource('employees', 'Employees\EmployeeController');
        Route::resource('materials', 'Materials\MaterialController');
        Route::resource('properties.nested', 'Properties\NestedPropertiesController',
            array('except' => array('index', 'view', 'show', 'edit', 'update', 'destroy')));
        Route::resource('properties.children', 'Properties\ChildPropertiesController',
            array('except' => array('index', 'view', 'show', 'edit', 'update', 'destroy')));
        Route::resource('properties', 'Properties\PropertiesController');
        Route::controller('reporting', 'Reporting\ReportsController');
        Route::resource('work-logs', 'Work\WorkLogsController');
        Route::resource('districts', 'DistrictsController');
        Route::resource('projects', 'Projects\ProjectsController');
        Route::controller('utility', 'UtilityController');
        Route::controller('trashed', 'UnTrashController');
        Route::resource('fuels', 'Fuels\FuelController');

        Route::controller('/', 'DashboardController');
    });

    Route::get('data-tools/', function () {
        return Redirect::to(url('data-tools/export'));
    });

    Route::get('settings/', function () {
        return Redirect::to(url('settings/account'));
    });





});
