<?php

/*
|--------------------------------------------------------------------------
| Administration Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/


Route::group(array('prefix' => 'admin', 'namespace' => 'Admin', 'before' => 'auth|admin'), function () {
    View::composer('admin.dashboard_widgets.storage_costs',
        'ParadoxOne\NDCounties\UI\Composers\Admin\ExportDiskCostComposer');
    Route::controller('utilities/system-wide', 'Utilities\SystemWideUtilitiesController');
    Route::controller('utilities/reports', 'Utilities\ReportsController');
    Route::controller('utilities/tenants', 'Utilities\TenantController');
    Route::controller('utilities/users', 'Utilities\UserController');

    Route::resource('reports/equations', 'Reports\EquationsController');
    Route::controller('database/query', 'DatabaseManagement\QueryController');
    Route::resource('accounts', 'AccountsController');
    Route::resource('reports', 'Reports\ReportController');
    Route::resource('users', 'UsersController');
    Route::resource('units', 'UnitsController');
    Route::controller('update-terms', 'UpdateTermsController');
    Route::controller('mail', 'MailController');
    Route::controller('/', 'DashboardController');
});
