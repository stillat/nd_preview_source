<?php

/*
|--------------------------------------------------------------------------
| Tenant Fixes
|--------------------------------------------------------------------------
|
| This file contains some fail-safe logic to make sure that all users
| are not running with a tenant value of `0`.
|
*/

if (Auth::check()) {
    if (Auth::user()->last_tenant == 0) {
        if (Auth::user()->admin == 1) {
            UI::warning('Your active service level account has been changed to prevent system instability. Review the active service account before committing any changes', 'Service Account Changed');

            $user = Auth::user();
            $user->last_tenant = 1;
            $user->save();
        } else {
            $accountRepository = App::make('ParadoxOne\NDCounties\Contracts\AccountRepositoryInterface');
            $primaryServiceAccount = $accountRepository->getLogicalPrimaryUserAccount(Auth::user()->id);

            if ($primaryServiceAccount !== null) {
                $user = Auth::user();
                $user->last_tenant = $primaryServiceAccount->tenant_id;
                $user->save();

                if ($primaryServiceAccount->total_service_accounts > 1) {
                    UI::warning('Your active service level account has been changed to prevent system instability. Review the active service account before committing any changes', 'Service Account Changed');
                }
            } else {

                // If the return value is NULL, that means there are no active service accounts for the user. Since
                // a service account can be revoked mid-session, we must handle that here to prevent un-authorized
                // use and damage to the system:
                // If there are no service accounts and the user is not an administrator,
                Session::flash('serviceAccountsRevoked', 'yes');
                Auth::logout();
            }
        }
    }
}
