<?php

use Illuminate\Support\Facades\App;

\Artisan::add(new TruncateTenantCommand());
\Artisan::add(App::make('InstallDefaultReportsCommand'));
\Artisan::add(new ResetUserSettingsCommand());
\Artisan::add(App::make('ClearExportsCommand'));
\Artisan::add(new QuickUninstallCommand());
\Artisan::add(new RebuildMigrationsCommand());
