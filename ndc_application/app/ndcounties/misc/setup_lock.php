<?php

$NDC_INSTALLED = false;


/*
|--------------------------------------------------------------------------
| Setup Lock
|--------------------------------------------------------------------------
|
| This file will check to see if the system is actually installed. If it
| is not, it will redirect the user to an installation wizard that will
| prompt the administrator to create their account, and also create the
| first user account.
|
| This script expects that the database configuration files have been
| correctly set.
|
*/

use ParadoxOne\NDCounties\NDCApplication as NDCApp;

if (NDCApp::isInstalled() === false) {

    // Just a fail-safe to set the NDC_INSTALLED variable.
    $NDC_INSTALLED = false;

    // Since the application is not installed, let's register some new routes
    // that will allow administrators to perform an initial setup.
    Route::controller('setup', 'System\SetupController');

    Route::get('/', function () {
        return View::make('system.install.notification');
    });
} else {
    // Indicate that the application is installed and application routes
    // should be registered.
    $NDC_INSTALLED = true;
}
