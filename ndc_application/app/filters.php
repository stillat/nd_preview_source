<?php/*|--------------------------------------------------------------------------| Application & Route Filters|--------------------------------------------------------------------------|| Below you will find the "before" and "after" events for the application| which may be used to do any work before or after a request into your| application. Here you may also register your custom route filters.|*/use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\View;
use ParadoxOne\NDCounties\Processes\ProcessManager;

App::before(function ($request) {    //});App::after(function ($request) {    /**    if (Auth::user() !== null)    {        // Cache::forget('func_get_account_'.Auth::user()->last_tenant);    }    */});/*|--------------------------------------------------------------------------| Authentication Filters|--------------------------------------------------------------------------|| The following filters are used to verify that the user of the current| session is logged into this application. The "basic" filter easily| integrates HTTP Basic authentication for quick, simple checking.|*/Route::filter('auth', function () {    if (Auth::guest() || Auth::user() == null) {
        return View::make('public.signin');        // return Redirect::guest('login');
    }});Route::filter('accountRedirect', function () {    if (Auth::user()->last_tenant == 0) {
        return Redirect::to(action('App\ServiceAccountController@getChoose'));

    }});Route::filter('admin', function () {    if (Auth::guest()) {
        return View::make('public.signin');        // return Redirect::guest('login');
    }    if (Auth::user()->admin == false) {
        App::abort(404);

    }});Route::filter('auth.basic', function () {    return Auth::basic();});Route::filter('auth.redirect', function () {    if (Auth::check()) {
        return Redirect::to('/');

    }    return View::make('public.signin');    // return Redirect::to('login');});/*|--------------------------------------------------------------------------| Guest Filter|--------------------------------------------------------------------------|| The "guest" filter is the counterpart of the authentication filters as| it simply checks that the current user is not logged in. A redirect| response will be issued if they are, which you may freely change.|*/Route::filter('guest', function () {    if (Auth::check()) {
        return Redirect::to('/');

    }});/*|--------------------------------------------------------------------------| CSRF Protection Filter|--------------------------------------------------------------------------|| The CSRF filter is responsible for protecting your application against| cross-site request forgery attacks. If this special token in a user| session does not match the one given in this request, we'll bail.|*/Route::filter('csrf', function () {    if (Session::token() !== Input::get('_token')) {
        throw new Illuminate\Session\TokenMismatchException;

    }});/*|--------------------------------------------------------------------------| Invalidate Browser Cache Filter|--------------------------------------------------------------------------|| Causes the user's browser to invalidate the cache.|*/Route::filter('invalidate-browser-cache', function ($request, $response) {    $response->headers->set('Cache-Control', 'nocache, no-store, max-age=0, must-revalidate');    $response->headers->set('Pragma', 'no-cache');    $response->headers->set('Expires', 'Fri, 01 Jan 1990 00:00:00 GMT');});
