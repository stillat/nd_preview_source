@extends('layouts.master')

@section('pageTitle')
    <i class="ndc-chart-pie"></i> You are creating a new report
@stop

@section('buttonBar')
    <div class="btn-group pull-up-10">
        <a href="{{ action('Admin\Reports\ReportController@index') }}" class="btn btn-danger">Cancel</a>
    </div>
@stop

@section('additionalStyles')
    <link rel="stylesheet" href="{{{ app_url() }}}/assets/css/codemirror/codemirror.css"/>
    <style>.CodeMirror { height: auto; }</style>
@stop

@section('additionalScripts')
    <script src="{{{ app_url() }}}/assets/js/admin_reporting.js" defer></script>
@stop

@section('content')
    <div class="row">
        {{  Form::open(array('action' => 'Admin\Reports\ReportController@store', 'method' => 'post', 'class' => 'form-horizontal')) }}
        <div class="col-sm-12">
            <ul class="nav nav-tabs">
                <li class="active"><a data-toggle="tab" href="#general"><strong><i class="fa fa-file-o"></i> General Report Information</strong></a></li>
                <li><a data-toggle="tab" href="#body"><strong><i class="fa fa-code"></i> Report Definition</strong></a></li>
                <li><a data-toggle="tab" href="#srl"><strong><i class="fa fa-cogs"></i> Advanced</strong></a></li>
            </ul>
            <div class="tab-content nopadding">
                <div class="tab-pane active" style="padding: 15px;" id="general">
                    @include('admin.reports.management.pages.create_general')
                </div>
                <div class="tab-pane" id="body">
                    @include('admin.reports.management.pages.create_def')
                </div>
                <div class="tab-pane" id="srl">
                    <textarea name="report_srl" id="srldef">{{ File::get(app_path().'/ndcounties/stubs/report_srl_definition_default.json') }}</textarea>
                </div>
            </div>
        </div>
        <div class="col-sm-12">
            <br><button type="submit'" class="btn btn-primary col-sm-offset-2">Submit</button>
            <button type="reset" class="btn btn-default">Reset</button>
        </div>
        {{ Form::close() }}
    </div>
@stop