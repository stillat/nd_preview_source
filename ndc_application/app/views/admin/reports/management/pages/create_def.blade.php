<div class="tabbable">
    <ul class="nav nav-tabs nav-dark" >
        <li class="active"><a href="#srlHeaderTab" data-toggle="tab" style="border-radius: 0px;"><i class="fa fa-code"></i> Header</a></li>
        <li><a href="#srlBodyTab" data-toggle="tab" style="border-radius: 0px;"><i class="fa fa-code"></i> Body</a></li>
        <li><a href="#srlFooterTab" data-toggle="tab" style="border-radius: 0px;"><i class="fa fa-code"></i> Footer</a></li>
    </ul>
    <div class="tab-content nopadding">
        <div class="tab-pane active" id="srlHeaderTab">
            <textarea name="srl_header" id="srlHeader"><!--SRL HEADER--></textarea>
        </div>
        <div class="tab-pane" id="srlBodyTab">
            <textarea name="srl_body" id="srlBody"><!--SRL BODY--></textarea>
        </div>
        <div class="tab-pane" id="srlFooterTab">
            <textarea name="srl_footer" id="srlFooter"><!--SRL FOOTER--></textarea>
        </div>
    </div>

</div>