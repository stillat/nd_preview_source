<div class="form-group">
    <label class="col-sm-2 control-label" for="ruid">RUID:</label>
    <div class="col-sm-3">
        <input type="text" name="ruid" id="ruid" class="form-control" value="{{ Input::old('ruid', $definition->report_uid) }}">
    </div>
</div>
<div class="form-group">
    <label class="col-sm-2 control-label" for="eruid">Extends RUID:</label>
    <div class="col-sm-3">
        <input type="text" name="eruid" id="eruid" value="{{ Input::old('eruid', $definition->report_extends_uid) }}" placeholder="RUID of report to extend (if any)" class="form-control">
    </div>
</div>
<div class="form-group">
    <label class="col-sm-2 control-label" for="report_name">Name:</label>
    <div class="col-sm-8">
        <input type="text" id="report_name" name="report_name" value="{{ Input::old('report_name', $definition->report_name) }}" class="form-control">
    </div>
</div>
<div class="form-group is-required">
    <label class="col-sm-2 control-label" for="report_description">Description:</label>
    <div class="col-sm-8">
        <textarea id="report_description" name="report_description" class="form-control mousetrap autosize  {{{ user_form_size() }}}">{{{ Input::old('report_description', $definition->report_description) }}}</textarea>
        {{ Form::errorMsg('report_description') }}
    </div>
</div>
<div class="form-group">
    <label class="col-sm-2 control-label" for="report_icon">Icon:</label>
    <div class="col-sm-3">
        <input type="text" id="report_icon" name="report_icon" value="{{ Input::old('report_icon', $definition->report_icon) }}" class="form-control">
    </div>
</div>
<div class="form-group">
    <label class="col-sm-2 control-label" for="report_category">Category:</label>
    <div class="col-sm-5">
        {{ Form::select('report_category', $categories, Input::old('report_category', $definition->report_category), ['class' => 'form-control', 'id' => 'report_category']) }}
        {{ Form::errorMsg('report_category') }}
    </div>
</div>
<div class="form-group">
    <label class="col-sm-2 control-label" for="report_account">Account</label>
    <div class="col-sm-5">
        {{ Form::select('report_account', $accounts, Input::old('report_account', $definition->account_id), ['class' => 'form-control', 'id' => 'report_account']) }}
        {{ Form::errorMsg('report_account') }}
    </div>
</div>