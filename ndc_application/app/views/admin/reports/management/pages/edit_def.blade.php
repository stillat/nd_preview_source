<div class="tabbable taebs-below">
    <ul class="nav nav-tabs nav-dark" >
        <li class="active"><a href="#srl_tab_header" data-toggle="tab" style="border-radius: 0px;"><i class="fa fa-code"></i> Header</a></li>
        <li><a href="#srl_tab_body" data-toggle="tab" style="border-radius: 0px;"><i class="fa fa-code"></i> Body</a></li>
        <li><a href="#srl_tab_footer" data-toggle="tab" style="border-radius: 0px;"><i class="fa fa-code"></i> Footer</a></li>
    </ul>
    <div class="tab-content nopadding">
        <div class="tab-pane active" id="srl_tab_header">
            <textarea name="srl_header" id="srlHeader">{{ Input::old('srl_header', $definition->srl_header) }}</textarea>
        </div>
        <div class="tab-pane" id="srl_tab_body">
            <textarea name="srl_body" id="srlBody">{{ Input::old('srl_body', $definition->srl_body) }}</textarea>
        </div>
        <div class="tab-pane" id="srl_tab_footer">
            <textarea name="srl_footer" id="srlFooter">{{ Input::old('srl_footer', $definition->srl_footer) }}</textarea>
        </div>
    </div>

</div>