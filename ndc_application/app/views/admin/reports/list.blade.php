@extends('layouts.master')

@section('pageTitle')
<i class="ndc-chart-pie"></i> System Reports
@stop

@section('buttonBar')
<div class="btn-group pull-up-10">
    {{-- <a href="{{ action('Admin\Utilities\ReportsController@getInstallDefaultReports') }}" class="btn btn-white mr5"><i class="ndc-chart-pie"></i> Re-install Default Reports and Equations</a> --}}
    <a data-toggle="modal" data-target="#uploadMergeReports" class="btn btn-white"><i class="fa fa-upload inline-icon"></i> Upload and Merge Reports</a>
    <a href="{{ action('Admin\Utilities\ReportsController@getDownloadReports') }}" class="btn btn-white"><i class="fa fa-download inline-icon"></i> Download Reports</a>

    <a href="{{ action('Admin\Reports\ReportController@create') }}" class="btn btn-white"><i class="fa fa-plus inline-icon"></i> Create Report</a>
</div>
@stop

@section('content')
    @include('admin.reports.dialogs.upload')
<div class="row">

    <div class="col-md-12">
        {{ $definitions->links() }}
    </div>

    <div class="col-md-12">
       @include('admin.reports.partials.table')
   </div>

   <div class="col-md-12">
    {{ $definitions->links() }}
</div>

</div>

@include('layouts.dialogs.remove', array('title' => 'Remove Report', 'content' => 'Removing system reports may cause confusion and instability with user created reports. Are you sure you want to remove this report?', 'agreeText' => 'Remove Report', 'action' => 'Admin\Reports\ReportController@destroy'))

@stop