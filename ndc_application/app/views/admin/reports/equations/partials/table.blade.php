<div class="table-responsive mb30">
    <table class="table table-hover table-fixed">
        <thead>
            <tr>
                <th>Setting Name</th>
                <th>Formula</th>
                <th>Account ID</th>
                <th class="wd-100 text-right"></th>
            </tr>
        </thead>
        <tbody>
            @foreach($equations as $equation)
            <tr>
                <td><a href="{{ action('Admin\Reports\EquationsController@show', array($equation->id)) }}">{{{ $equation->total_setting_name }}}</a></td>
                <td><a href="{{ action('Admin\Reports\EquationsController@show', array($equation->id)) }}"><code>{{{ $equation->formula }}}</code></a></td>
                <td><a href="{{ action('Admin\Reports\EquationsController@show', array($equation->id)) }}">{{{ $equation->account_name }}}</a></td>
                <td class="table-action td-ignore">
                    <a href="{{ action('Admin\Reports\EquationsController@edit', array($equation->id)) }}"><i class="fa fa-pencil"></i></a>
                    <a class="cursor-pointer delete-row" data-rcontext="{{{ $equation->id }}}"><i class="fa fa-trash-o"></i></a>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>