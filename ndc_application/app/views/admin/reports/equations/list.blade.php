@extends('layouts.master')
@section('pageTitle')
Reporting System Equations
@stop
@section('buttonBar')
<div class="btn-group pull-up-10">
	<a href="{{ action('Admin\Reports\EquationsController@create') }}" class="btn btn-white">Create Equation</a>
</div>
@stop

@section('content')
<div class="row">
    <div class="col-sm-12">
        <div class="alert alert-warning">
            <strong>Warning!</strong> Reporting system equations are what control how totals on reports are calculated. Modifying any reporting system equation without exact knowledge of what reports it affects, how it affects internal systems, or whether or not it is a default setting is <strong>not recommended</strong>.</a>.
        </div>
    </div>
</div>

<div class="row">

    <div class="col-md-12">
        {{ $equations->links() }}
    </div>

    <div class="col-md-12">
       @include('admin.reports.equations.partials.table')
   </div>

   <div class="col-md-12">
    {{ $equations->links() }}
</div>

</div>

@include('layouts.dialogs.remove', array('title' => 'Remove Report Equation', 'action' => 'Admin\Reports\EquationsController@destroy'))

@stop