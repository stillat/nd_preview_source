@extends('layouts.master')

@section('pageTitle')
{{{ $equation->total_setting_name }}}
@stop
@section('buttonBar')
<div class="btn-group pull-up-10">
    <a href="{{ action('Admin\Reports\EquationsController@edit', $equation->id) }}" class="btn btn-white">Update this equation</a>
</div>
@stop

@section('content')

<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title">General Information for {{{ $equation->total_setting_name }}}</h3>
    </div>
    <table class="table table-hover">
        <thead>
            <tr>
                <th>Total Setting Name</th>
                <th>Formula</th>
                <th>Account ID</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>{{{ $equation->total_setting_name }}}</td>
                <td><code>{{{ $equation->formula }}}</code></td>
                <td>{{{ $equation->account_name }}}</td>
            </tr>
        </tbody>
    </table>
</div>
@stop