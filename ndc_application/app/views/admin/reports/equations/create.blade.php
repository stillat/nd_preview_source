@extends('layouts.master')
@section('pageTitle')
You are creating a new reporting equation total
@stop
@section('buttonBar')
<div class="btn-group pull-up-10">
    <a href="{{ action('Admin\Reports\EquationsController@index') }}" class="btn btn-danger">Cancel</a>
</div>
@stop
@section('content')
<div class="row">
    <div class="col-md-12">
        {{ Form::open(array('action' => 'Admin\Reports\EquationsController@store', 'method' => 'post', 'class' => 'form-horizontal')) }}
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">You are creating a reporting equation.</h4>
            </div>
            <div class="panel-body">
                <div class="form-group is-required">
                    <label class="col-sm-2 control-label" for="total_setting_name">Setting name:</label>
                    <div class="col-sm-8">
                        <input type="text" id="total_setting_name" name="total_setting_name" class="form-control mousetrap ndc_form_home_element {{{ user_form_size() }}}" value="{{{ Input::old('total_setting_name', '') }}}" autofocus>
                        {{ Form::errorMsg('total_setting_name') }}
                    </div>
                </div>
                <div class="form-group is-required">
                    <label class="col-sm-2 control-label" for="name">Account ID:</label>
                    <div class="col-sm-8">
                        <input type="text" id="account_id" name="account_id" class="form-control mousetrap {{{ user_form_size() }}}" value="{{{ Input::old('account_id', '') }}}">
                        {{ Form::errorMsg('account_id') }}
                    </div>
                </div>
                <div class="form-group is-required">
                    <label class="col-sm-2 control-label" for="description">Formula:</label>
                    <div class="col-sm-8">
                        <textarea id="formula" name="formula" class="form-control mousetrap ndc_form_trail_element {{{ user_form_size() }}}">{{{ Input::old('formula', '') }}}</textarea>
                        {{ Form::errorMsg('formula') }}
                    </div>
                </div>
            </div>
            <div class="panel-footer">
                <div class="btn-group m5g">
                    <button type="submit" id="ndc_form_defaultaction" value="addNew" name="submit" class="btn btn-primary">Save Account Equation</button>
                </div>
                <div class="btn-group mr5">
                    <button type="reset" id="ndc_form_reset" class="btn btn-default">Reset</button>
                </div>
            </div>
        </div>
        {{ Form::close() }}
    </div>
</div>
@stop