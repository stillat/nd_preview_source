<div class="table-responsive mb30">
    <table class="table table-hover table-striped table-fixed">
        <thead>
            <tr>
                <th>UID/EUID</th>
                <th>Name</th>
                <th>Description</th>
                <th>Account</th>
                <th>Category</th>
                <th class="wd-100 text-right"></th>
            </tr>
        </thead>
        <tbody>
            @foreach($definitions as $definition)
            <tr>
                <td><a href="{{ action('Admin\Reports\ReportController@show', array($definition->id)) }}"><small>{{{ $definition->report_uid }}}<br>{{{ $definition->report_extends_uid }}}</small></a></td>
                <td><a href="{{ action('Admin\Reports\ReportController@show', array($definition->id)) }}"><span class="{{{ $definition->report_icon }}}"></span> {{{ limit_name($definition->report_name) }}}</a></td>
                <td><a href="{{ action('Admin\Reports\ReportController@show', array($definition->id)) }}">{{{ limit_description($definition->report_description) }}}</a></td>
                <td>
                    <a href="{{ action('Admin\Reports\ReportController@show', array($definition->id)) }}">
                        @if($definition->account_id == 0)
                        <i class="fa fa-shield"></i>
                        @endif
                        {{{ limit_name($definition->account_name) }}}
                    </a>
                </td>
                <td><a href="{{ action('Admin\Reports\ReportController@show', array($definition->id)) }}">{{{ limit_name($categories[$definition->report_category]) }}}</a></td>
                <td class="table-action td-ignore"><a href="{{ action('Admin\Reports\ReportController@edit', array($definition->id)) }}"><i class="fa fa-edit"></i></a> <a class="cursor-pointer delete-row" data-rcontext="{{{ $definition->id }}}"><i class="fa fa-trash-o"></i></a></td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>