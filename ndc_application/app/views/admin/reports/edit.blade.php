@extends('layouts.master')
@section('pageTitle')
You are updating an existing report meta definition entry
@stop
@section('buttonBar')
<div class="btn-group pull-up-10">
    <a href="{{ action('Admin\Reports\ReportController@index') }}" class="btn btn-danger">Cancel</a>
</div>
@stop
@section('additionalStyles')
<link rel="stylesheet" href="{{{ app_url() }}}/assets/css/codemirror/codemirror.css"/>
@stop
@section('content')
<div class="row">
    <div class="col-md-12">
        {{  Form::open(array('action' => ['Admin\Reports\ReportController@update', $definition->id], 'method' => 'patch', 'class' => 'form-horizontal')) }}
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">You are updating an existing report meta definition entry.</h4>
                <p>Report meta definition (RMD) entries are what make reports available to service users. All fields are required. Simplified Reporting Language (SRL) fields have been marked as such.</p>
            </div>
            <div class="panel-body nopadding">
                <ul class="nav nav-tabs">
                    <li class="active"><a href="#general" data-toggle="tab"><strong>General Report Information</strong></a></li>
                    <li class=""><a href="#srlheaderTab" data-toggle="tab"><strong>SRL Header</strong></a></li>
                    <li class=""><a href="#srlBodyTab" data-toggle="tab"><strong>SRL Body</strong></a></li>
                    <li class=""><a href="#srlFooterTab" data-toggle="tab"><strong>SRL Footer</strong></a></li>
                    <li class=""><a href="#srlPrepareTab" data-toggle="tab"><strong>SRL Preparation</strong></a></li>
                    <li class=""><a href="#srlSettingsTab" data-toggle="tab"><strong><i class="fa fa-cogs"></i> Automatic Settings</strong></a></li>
                </ul>
                <div class="tab-content mb30">
                    <div class="tab-pane active" id="general">
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Report Name:</label>
                            <div class="col-sm-8">
                                <input type="text" name="report_name" class="form-control" value="{{ Input::old('report_name', $definition->report_name) }}" autofocus>
                                {{ Form::errorMsg('report_name') }}
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Report Icon:</label>
                            <div class="col-sm-8">
                                <input type="text" name="report_icon" class="form-control" value="{{ Input::old('report_icon', $definition->report_icon) }}">
                                {{ Form::errorMsg('report_icon') }}
                            </div>
                        </div>
                        <div class="form-group is-required">
                            <label class="col-sm-2 control-label" for="report_description">Report Description:</label>
                            <div class="col-sm-8">
                                <textarea id="report_description" name="report_description" class="form-control mousetrap  {{{ user_form_size() }}}">{{{ Input::old('report_description', $definition->report_description) }}}</textarea>
                                {{ Form::errorMsg('report_description') }}
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Property Type Limits:</label>
                            <div class="col-sm-8">
                                <input type="text" name="property_type_limits" class="form-control" value="{{ Input::old('property_type_limits', $definition->property_type_limits) }}">
                                {{ Form::errorMsg('property_type_limits') }}
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Report UID:</label>
                            <div class="col-sm-8">
                                <input type="text" name="report_uid" class="form-control" value="{{ Input::old('report_uid', $definition->report_uid) }}">
                                {{ Form::errorMsg('report_uid') }}
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Report Category:</label>
                            <div class="col-sm-8">
                                <input type="text" name="report_category" class="form-control" value="{{ Input::old('report_category', $definition->report_category) }}">
                                {{ Form::errorMsg('report_category') }}
                            </div>
                        </div>
                        <div class="form-group is-required">
                           <label class="col-sm-2 control-label" for="report_srl_message">SRL Message:</label>
                           <div class="col-sm-8">
                                <textarea id="report_srl_message" name="srl_message" class="form-control mousetrap  {{{ user_form_size() }}}">{{{ Input::old('srl_message', $definition->srl_notification) }}}</textarea>
                                {{ Form::errorMsg('srl_message') }}
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane nopadding" id="srlheaderTab">
                        {{ Form::errorMsg('srl_header') }}
                        <textarea id="srlHeader" name="srl_header">{{ Input::old('srl_header', $definition->srl_header) }}</textarea>
                    </div>
                    <div class="tab-pane nopadding" id="srlBodyTab">
                        {{ Form::errorMsg('srl_body') }}
                        <textarea id="srlBody" name="srl_body">{{ Input::old('srl_body', $definition->srl_body) }}</textarea>
                    </div>
                    <div class="tab-pane nopadding" id="srlPrepareTab">
                        {{ Form::errorMsg('srl_prepare') }}
                        <textarea id="srlPrepare" name="srl_prepare">{{ Input::old('srl_prepare', $definition->srl_prepare) }}</textarea>
                    </div>
                    <div class="tab-pane nopadding" id="srlFooterTab">
                        {{ Form::errorMsg('srl_footer') }}
                        <textarea id="srlFooter" name="srl_footer">{{ Input::old('srl_footer', $definition->srl_footer) }}</textarea>
                    </div>
                    <div class="tab-pane nopadding" id="srlSettingsTab">
                        {{ Form::errorMsg('srl_automatic_settings') }}
                        <textarea id="srlSettings" name="srl_automatic_settings">{{ Input::old('srl_automatic_settings', $definition->srl_automatic_settings) }}</textarea>
                    </div>
                </div>
            </div>
            <!-- panel-body -->
            <div class="panel-footer">
                <button type="submit'" class="btn btn-primary">Submit</button>
                <button type="reset" class="btn btn-default">Reset</button>
            </div>
            <!-- panel-footer -->
        </div>
        <!-- panel-default -->
        {{ Form::close() }}
    </div>
</div>
@stop
@section('additionalScripts')
<script src="{{{ app_url() }}}/assets/js/admin_reporting.js" defer></script>
@stop