@extends('layouts.master')

@section('additionalStyles')
    <link rel="stylesheet" href="{{ app_url() }}/assets/css/prism.css" rel="stylesheet" type="text/css">
@stop


@section('additionalScripts')
    <script src="{{ app_url() }}/assets/js/prism.js" ></script>
@stop

@section('pageTitle')
<i class="{{{ $definition->getIcon() }}}"></i> Report: {{{ $definition->getName() }}}
@stop

@section('buttonBar')
    <div class="btn-group n-mt-5">
        {{ HTML::modelNaviateToolbar(action('Admin\Reports\ReportController@show', [$definition->previous()]), $definition->previous(), action('Admin\Reports\ReportController@index'), action('Admin\Reports\ReportController@show', [$definition->next()]), $definition->next(), true) }}
    </div>
<div class="btn-group n-mt-5">
    <a href="{{ action('Admin\Reports\ReportController@edit', $definition->id) }}" class="btn btn-white"><i class="fa fa-edit inline-icon"></i> Update this definition</a>
</div>
@stop

@section('content')
    @if($definition->report_extends_uid != '')
        <div class="row">
            <div class="col-sm-12">
                <div class="alert alert-info">
                    <p>This report extends another report (<a href="{{ action('Admin\Reports\ReportController@show', $definition->getBaseReport()->getReportID()) }}">{{ $definition->getBaseReport()->getName() }}</a>). Data below will represent the extended, merged report.</p>
                </div>
            </div>
        </div>
        @endif
<div class="row">
    <div class="col-sm-12">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h3 class="panel-title">General Details for {{{ $definition->report_name }}}</h3>
            </div>
            <table class="table table-hover table-bordered">
                <thead>
                <tr>
                    <th>Report Name</th>
                    <th>Account ID</th>
                    <th>Report Icon</th>
                    <th>Report Description</th>
                    <th>Report UID</th>
                    @if($definition->report_extends_uid != '')
                    <th>Extends UID</th>
                    @endif
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>{{{ $definition->report_name }}}</td>
                    <td>{{{ $account->account_name }}}</td>
                    <td><i class="{{{ $definition->report_icon }}}"></i> {{{ $definition->report_icon }}}</td>
                    <td>{{{ $definition->report_description }}}</td>
                    <td>{{{ $definition->report_uid }}}</td>
                    @if($definition->report_extends_uid != '')
                    <td>{{{ $definition->report_extends_uid }}}</td>
                    @endif
                </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>
    <?php
    $cantDetermineLimit = false;
    try {
        $definition->isLimited();
    } catch (\Exception $e) {
        $cantDetermineLimit = true;
    }
    ?>
@if($definition->getSettings() == null || $cantDetermineLimit)
    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-danger">
                <div class="panel-heading">
                    <h3 class="panel-title"><i class="fa fa-warning"></i> This Road Appears to Be Damaged</h3>
                </div>
                <div class="panel-body">
                    <p>Here is the original settings for reference:</p>
                    <pre  class="line-numbers language-markup"><code class="language-javascript">{{ e($definition->getSettings()) }}</code></pre>
                </div>
            </div>
        </div>
    </div>
@endif
<div class="row">
    <div class="col-sm-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-code"></i> Report Definition</h3>
            </div>
            <div class="panel-body nopadding">
                <ul class="nav nav-tabs nav-dark" >
                    <li class="active"><a href="#srlHeader" data-toggle="tab" style="border-radius: 0px;"><i class="fa fa-code"></i> Header</a></li>
                    <li><a href="#srlBody" data-toggle="tab" style="border-radius: 0px;"><i class="fa fa-code"></i> Body</a></li>
                    <li><a href="#srlFooter" data-toggle="tab" style="border-radius: 0px;"><i class="fa fa-code"></i> Footer</a></li>
                    <li><a href="#srlSettings" data-toggle="tab" style="border-radius: 0px;"><i class="fa fa-code"></i> Settings</a></li>
                </ul>
                <div class="tab-content nopadding">
                    <div class="tab-pane active" id="srlHeader">
                        <pre  class="line-numbers language-markup"><code class="language-markup">{{ e($definition->getHeader()) }}</code></pre>
                    </div>
                    <div class="tab-pane" id="srlBody">
                        <pre  class="line-numbers language-markup"><code class="language-markup">{{ e($definition->getBody()) }}</code></pre>
                    </div>
                    <div class="tab-pane" id="srlFooter">
                        <pre  class="line-numbers language-markup"><code class="language-markup">{{ e($definition->getFooter()) }}</code></pre>
                    </div>
                    <div class="tab-pane" id="srlSettings">
                        <pre  class="line-numbers language-markup"><code class="language-javascript">{{ e(json_encode(json_decode($definition->getSettings()), JSON_PRETTY_PRINT)) }}</code></pre>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop