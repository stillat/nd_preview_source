<div class="modal fade" id="uploadMergeReports" tabindex="-1" role="dialog" aria-labelledby="uploadMergeReportsLabel"
     aria-hidden="true">
    {{ Form::open(['action' => 'Admin\Utilities\ReportsController@postMergeReports', 'files' => true]) }}
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"
                        style="margin-top: 10px;">&times;</button>
                <h4 id="uploadMergeReportsLabel"><i class="fa fa-upload"></i> Upload and Merge Reports</h4>
            </div>
            <div class="modal-body">
                <p>You should only upload and merge reports during non-peak traffic times. Merging reports will not
                    remove any custom reports or equations, but will remove all system reports! Make sure the merge file
                    contains all relevant system reports with the correct report UUIDs.</p>

                <p>It is recommended that you download the current report set before merging.</p>

                <fieldset>
                    <legend><i class="fa fa-key"></i> Authentication</legend>
                    <div class="form-group">
                        <label for="inputPassword" class="col-sm-4 control-label">Enter your password</label>

                        <div class="col-sm-8">
                            <input type="password" class="form-control" id="inputPassword" name="current_password"
                                   placeholder="Your password">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputSecurityCode" class="col-sm-4 control-label">Last four of security
                            code:</label>

                        <div class="col-sm-8">
                            <input type="password" class="form-control" id="inputSecurityCode" name="security_code"
                                   placeholder="The last four digits of the current installation's security code">
                        </div>
                    </div>
                </fieldset>

                <fieldset>
                    <legend><i class="fa ndc-chart-pie"></i> Report File</legend>
                    <div class="fileupload fileupload-new" data-provides="fileupload">
                        <div class="input-append">
                            <div class="uneditable-input" style="width: 71%;">
                                <i class="glyphicon glyphicon-file fileupload-exists"></i>
                                <span class="fileupload-preview"></span>
                            </div>
                    <span class="btn btn-default btn-file">
                      <span class="fileupload-new">Select report file</span>
                      <span class="fileupload-exists">Change</span>
                      <input type="file" name="report_pak"/>
                    </span>
                            <a href="#" class="btn btn-default fileupload-exists" data-dismiss="fileupload">Remove</a>
                        </div>
                    </div>
                </fieldset>
            </div>
            <div class="modal-footer">
                {{ Form::open(['action' => 'App\DataTools\BackupController@postCreate'], ['method' => 'post']) }}
                <button type="submit" class="btn btn-warning"><i class="fa fa-upload"></i> Upload and Merge Reports
                </button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                {{ Form::close() }}
            </div>
        </div>
    </div>
    {{ Form::close() }}
</div>