@extends('layouts.master')

@section('pageTitle')
    <i class="fa fa-compress"></i> Backup Manager
@stop

@section('content')
    <div class="row">
        <div class="col-sm-12">
            <div class="table-responsive">
                <table class="table table-hover table-striped">
                    <thead>
                    <tr>
                        <th>Account Name</th>
                        <th>Account Tenant</th>
                        <th class="wd-200"></th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($accounts as $account)
                    <tr>
                        <td>{{{ $account->account_name }}}</td>
                        <td>{{{ $account->tenant_name }}}</td>
                        <td><a class="btn btn-link btn-block" href="">Launch manager for this account</a></td>
                    </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@stop