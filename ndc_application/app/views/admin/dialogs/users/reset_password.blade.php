@extends('layouts.modal')
@section('header')
    Reset User Password
@stop

@section('formOpen')
    {{ Form::open(array('action' => 'Admin\Utilities\UserController@postResetPassword', 'method' => 'post', 'class' => 'form-horizontal form-bordered')) }}
@stop

@section('body')
    <p>You are resetting the password for the user account with the name:
        <strong>{{{ $user->first_name.' '.$user->last_name }}}</strong>. Password resets take affect the next time a
        user attempts to login, after they have logged out or their session has expired.</p>
    <div class="form-group">
        <label class="col-sm-3 control-label">Your password</label>

        <div class="col-sm-9">
            <input type="password" placeholder="Your account password" class="form-control" name="admin_password">
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-3 control-label">New Password</label>

        <div class="col-sm-9">
            <input type="password" placeholder="New password for {{{ $user->first_name.' '.$user->last_name }}}"
                   class="form-control" name="new_password">
        </div>
    </div>
    <input type="hidden" name="user" value="{{{ $user->id }}}">
@stop

@section('formClose')
    {{ Form::close() }}
@stop