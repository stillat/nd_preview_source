@extends('layouts.modal')

@section('header')
    Remove Account
@stop

@section('body')
    <p>You are about to remove: <strong>{{{ $account->account_name }}}</strong></p>
    <p>Are you sure you want to remove this account? This action cannot be undone and all data will be lost immediately. To confirm this action, enter your password.</p>
    {{ Form::open(array('action' => ['Admin\Utilities\TenantController@postRemoveAccount', $account->id], 'class' => 'form-horizontal form-bordered')) }}
    <div class="panel panel-default">
        <div class="panel-body panel-body-nopadding">
            <div class="form-group">
                <label class="col-sm-2 control-label">Confirm:</label>
                <div class="col-sm-9">
                    <input type="password" class="form-control" name="confirm_password"/>
                </div>
            </div>
        </div>
        <div class="panel-footer text-right">
            <button type="submit" class="btn btn-danger"><i class="fa fa-trash-o"></i> Remove Account</button>
        </div>
    </div>
    {{ Form::close() }}
@stop

@section('footer')
@stop