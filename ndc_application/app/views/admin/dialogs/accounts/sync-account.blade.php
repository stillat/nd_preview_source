@extends('layouts.modal')

@section('header')
Sync Service Account
@stop

@section('body')
@if($user->admin == true)
<p>Administrator accounts have access to all service accounts.</p>
@else
<p>This will sync a service account to the user: <strong>{{ $user->first_name.' '.$user->last_name }}</strong></p>
{{ Form::open(array('action' => 'Admin\Utilities\TenantController@postSyncAccount', 'class' => 'form-horizontal form-bordered')) }}
<div class="panel panel-default">
	<div class="panel-body panel-body-nopadding">
		<div class="form-group">
			<label class="col-sm-3 control-label">Service Account</label>
			<div class="col-sm-9">
				{{ Form::select('account', $accounts, null, array('class' => 'form-control', 'id' => 'serviceAccountChoose', 'data-placeholder' => 'Select an account', 'style' => 'width: 100%;')); }}
			</div>
		</div>
	</div>
	<div class="panel-footer">
		<button type="submit" class="btn btn-primary">Submit</button>
	</div>
</div>
<input type="hidden" name="employee_id" value="{{ $user->id }}">
{{ Form::close() }}
@endif
@stop

@section('footer')
@if($user->admin == false)
<script>
	$(document).ready(function(){
		$('#serviceAccountChoose').select2();
	});
</script>
@endif
@stop