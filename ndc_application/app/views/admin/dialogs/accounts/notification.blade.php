@extends('layouts.modal')

@section('header')
    <i class="fa fa-bell"></i> Post Notification
@stop

@section('body')
    <p>Posted messages will appear within account and user activity streams.</p>
    {{ Form::open(array('action' => 'Admin\Utilities\SystemWideUtilitiesController@postPostNotification', 'class' => 'form-horizontal form-bordered')) }}
    <div class="panel panel-default">
        <div class="panel-body panel-body-nopadding">
            <div class="form-group">
                <label class="col-sm-2 control-label">Type:</label>
                <div class="col-sm-9">
                    <select name="message_type" id="message_type" class="form-control" style="width: 100%;">
                        <option value="0">System Update</option>
                        <option value="1">Account Message</option>
                        <option value="2">Maintenance Notification</option>
                        <option value="4">Maintenance Complete Notification</option>
                        <option value="3">Error Response</option>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">Account:</label>
                <div class="col-sm-9">
                    {{ Form::select('account', $currentAccounts, 0, array('class' => 'form-control', 'id' => 'serviceAccountChoose', 'data-placeholder' => 'Select an account', 'style' => 'width: 100%;')); }}
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">Message:</label>
                <div class="col-sm-9">
                    <textarea name="message" id="messageWriter" class="form-control" style="min-height: 36px;"></textarea>
                </div>
            </div>
        </div>
        <div class="panel-footer text-right">
            <button type="submit" class="btn btn-primary"><i class="fa fa-send"></i> Send Notification</button>
        </div>
    </div>
    {{ Form::close() }}
@stop

@section('footer')
    <script>
        $(document).ready(function () {
            $('#message_type').select2();
            $('#serviceAccountChoose').select2();
            $('#messageWriter').autosize();
        });
    </script>
@stop