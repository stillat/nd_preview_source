@extends('layouts.master')

@section('pageTitle')
<i class="fa ndcf-measure"></i> Measurement Units
@stop

@section('buttonBar')
<div class="btn-group pull-up-10">
    <a href="{{ route('admin.units.create') }}" class="btn btn-white"><i class="fa fa-plus inline-icon"></i> Add New Unit</a>
</div>
@stop

@section('content')

<div class="row">

    <div class="col-sm-12">
        <p>Measurement units are shared across all of the <a href="{{ url('admin/accounts') }}">service accounts</a>. It is not advised to remove any measurement units, as any record that requires the removed measurement unit may become invalid and unavailable for use. Be sure to search the list before adding any new measurement units to avoid cluttering the list with the same measurement unit.</p>
    </div>

    <div class="col-sm-12">
        <div class="panel panel-default">
            <div class="panel-body">

                {{ $units->links() }}

                <div class="table-responsive mb30">
                    <table class="table table-hover table-fixed">
                        <thead>
                            <tr>
                                <th class="wd-100">Code</th>
                                <th>Name</th>
                                <th>Description</th>
                                <th class="wd-100"></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($units as $unit)
                            <tr>
                                <td>{{{ $unit->code }}}</td>
                                <td>{{{ $unit->name }}}</td>
                                <td>{{{ $unit->description }}}</td>
                                <td class="table-action">
                                    <a class="cursor-pointer delete-row" data-rcontext="{{{ $unit->id }}}"><i class="fa fa-trash-o"></i></a>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>

                    {{ $units->links() }}

                </div>
            </div>

        </div>
    </div>
</div>

@include('layouts.dialogs.remove', array('title' => 'Remove Service Unit', 'action' => 'Admin\UnitsController@destroy'))
@stop