@extends('layouts.master')

@section('pageTitle')
<i class="fa fa-flask"></i> Learning Center
@stop

@section('buttonBar')
<div class="btn-group">
    <a href="{{ route('admin.learn.create') }}" class="btn btn-white"><i class="fa fa-plus inline-icon"></i> New Article</a>
</div>
@stop