@extends('layouts.master')

@section('additionalStyles')
<link rel="stylesheet" href="{{ app_url() }}/assets/uik/css/uikit.min.css">
<link rel="stylesheet" href="{{ app_url() }}/assets/uik/css/uikit.addons.min.css">
<link rel="stylesheet" href="{{ app_url() }}/assets/uik/codemirror/lib/codemirror.css">
@stop

@section('additionalScripts')
<script src="{{ app_url() }}/assets/uik/js/uikit.min.js"></script>
<script src="{{ app_url() }}/assets/uik/codemirror/lib/codemirror.js"></script>
<script src="{{ app_url() }}/assets/uik/codemirror/mode/markdown/markdown.js"></script>
<script src="{{ app_url() }}/assets/uik/codemirror/addon/mode/overlay.js"></script>
<script src="{{ app_url() }}/assets/uik/codemirror/mode/xml/xml.js"></script>
<script src="{{ app_url() }}/assets/uik/codemirror/mode/gfm/gfm.js"></script>
<script src="{{ app_url() }}/assets/uik/js/marked.js"></script>
<script src="{{ app_url() }}/assets/uik/js/markdownarea.min.js"></script>

@stop

@section('pageTitle')
<i class="fa fa-flask"></i> New Learning Center Article
@stop

@section('content')
<div class="row">
    {{ Form::open(array('route' => 'admin.learn.store', 'class' => 'form-horizontal')) }}
    <div class="col-md-12">
        <div class="panel panel-dark">
            <div class="panel-heading">
              <div class="panel-btns">
                <a href="" class="minimize">−</a>
            </div><!-- panel-btns -->
            <h3 class="panel-title">Article Meta Information</h3>
        </div>
        <div class="panel-body">
            <div class="form-group">
                <label class="col-sm-2 control-label">Title:</label>
                <div class="col-sm-8">
                    <input type="text" name="title" class="form-control" value="{{ Input::old('title', '') }}" autofocus>
                    {{ Form::errorMsg('title') }}
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">Slug:</label>
                <div class="col-sm-8">
                    <input type="text" name="slug" class="form-control" value="{{ Input::old('slug', '') }}" autofocus>
                    {{ Form::errorMsg('slug') }}
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">Keywords:</label>
                <div class="col-sm-8">
                    <input type="text" name="keywords" class="form-control" value="{{ Input::old('keywords', '') }}" autofocus>
                    {{ Form::errorMsg('keywords') }}
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">Description:</label>
                <div class="col-sm-8">
                    <textarea class="form-control" name="description" rows="3"></textarea>
                    {{ Form::errorMsg('keywords') }}
                </div>
            </div>

        </div>
    </div>
</div>
<div class="col-md-12">
    <textarea name="content" data-uk-markdownarea="{maxsplitsize:600}"></textarea>
</div>
<div class="col-md-12">
    <hr>
    <button type="submit" class="btn btn-primary">Publish Article</button>
</div>
{{ Form::close() }}
</div>
@stop