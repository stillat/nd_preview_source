@extends('layouts.master')

@section('pageTitle')
<i class="fa fa-user"></i> User Accounts
@stop

@section('buttonBar')
<div class="btn-group">
	<a href="{{ route('admin.users.create') }}" class="btn btn-default"><i class="fa fa-plus inline-icon"></i> New User</a>
</div>
@stop

@section('content')

<div class="row">
	
	<div class="col-md-12">
		{{ $userAccounts->links() }}
	</div>

	<div class="col-md-12">
		<div class="table-responsive">
			<table class="table table-hidaction table-hover mb30">
				<thead>
					<tr>
						<th>#</th>
						<th>First Name</th>
						<th>Last Name</th>
						<th>Email</th>
						<th>Last Seen/Last Login</th>
						<th></th>
					</tr>
				</thead>
				<tbody>
					@foreach($userAccounts as $user)
					<tr>
						<td>{{{ $user->id }}}</td>
						<td><a href="{{ route('admin.users.show', array($user->id)) }}">{{{ $user->first_name }}}</a></td>
						<td><a href="{{ route('admin.users.show', array($user->id)) }}">{{{ $user->last_name }}}</a></td>
						<td><a href="mailto:{{{ $user->email }}}">{{{ $user->email }}}</a></td>
						@if ($user->last_seen !== null)
						<td>{{{ with(new Carbon($user->last_seen))->diffForHumans() }}} - {{{ with(new Carbon($user->last_login))->toDayDateTimeString() }}}</td>
						@else
						<td>Never</td>
						@endif
						<td class="text-center">
							@if($user->id !== Auth::user()->id)
								<a href="{{ action('Admin\Utilities\UserController@getAssumeIdentity', [$user->id])  }}" class="btn btn-link"><i class="fa fa-user"></i> Assume Identity</a>
							@else
								<strong>This is you</strong>
							@endif
						</td>
					</tr>
					@endforeach
				</tbody>
			</table>
		</div>
	</div>

	<div class="col-md-12">
		{{ $userAccounts->links() }}
	</div>

	<div class="col-md-12">
		<h2>Service Administrators</h2>

		<div class="row">

			@foreach($administrators as $admin)
			<div class="col-md-6">
				{{ HTML::userCard($admin) }}
			</div>
			@endforeach

		</div>

	</div>

</div>

@stop