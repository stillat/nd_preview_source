@extends('layouts.master')

@section('pageTitle')
<i class="fa fa-plus"></i> You are adding a new user account
@stop

@section('buttonBar')
<div class="btn-group">
	<a href="{{ route('admin.users.index') }}" class="btn btn-danger">Cancel</a>
</div>
@stop

@section('content')

<div class="row">

	<div class="col-md-12">
		{{  Form::open(array('route' => 'admin.users.store', 'method' => 'post', 'class' => 'form-horizontal')) }}
			<div class="panel panel-default">
				<div class="panel-heading">
					<h4 class="panel-title">You are creating a new user account.</h4>
					<p>User accounts can log into the system and make changes to their account. Only users with a <strong>service account</strong> associated with their user account may use the ND Counties software.</p>
				</div>
				<div class="panel-body">

					<div class="form-group">
						<label class="col-sm-2 control-label">First Name:</label>
						<div class="col-sm-8">
							<input type="text" name="first_name" class="form-control" value="{{ Input::old('first_name', '') }}" autofocus>
							{{ Form::errorMsg('first_name') }}
						</div>
					</div>

					<div class="form-group">
						<label class="col-sm-2 control-label">Last Name:</label>
						<div class="col-sm-8">
							<input type="text" name="last_name" class="form-control" value="{{ Input::old('last_name', '') }}">
							{{ Form::errorMsg('last_name') }}
						</div>
					</div>

					<div class="form-group">
						<label class="col-sm-2 control-label">Email:</label>
						<div class="col-sm-8">
							<input type="email" name="email" class="form-control" value="{{ Input::old('email', '') }}">
							{{ Form::errorMsg('email') }}
						</div>
					</div>

					<div class="form-group">
						<label class="col-sm-2 control-label">Password:</label>
						<div class="col-sm-8">
							<input type="text" name="password" class="form-control" value="{{ Input::old('password', '') }}">
							{{ Form::errorMsg('password') }}
						</div>
					</div>

					<div class="form-group">
						<div class="col-sm-offset-2 col-sm-10">
							<div class="checkbox">
								<label>
									<input type="checkbox" name="admin"> System Administrator Account
								</label>
							</div>
						</div>
					</div>

				</div><!-- panel-body -->
				<div class="panel-footer">
					<button type="submit'" class="btn btn-primary">Submit</button>
					<button type="reset" class="btn btn-default">Reset</button>
				</div><!-- panel-footer -->
			</div><!-- panel-default -->
		{{ Form::close() }}
	</div>

</div>

@stop