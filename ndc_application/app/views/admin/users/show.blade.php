@extends('layouts.master')

@section('pageTitle')
{{{ str_limit($user->first_name.' '.$user->last_name, 30, '...') }}} <span>{{{ $user->email }}}</span>
@stop

@section('buttonBar')
<div class="btn-group pull-up-10">
	<a class="btn btn-lightblue" href="{{ action('Admin\Utilities\TenantController@getSyncAccount', array('user' => $user->id)) }}" data-toggle="modal" data-target="#modalHousing">Sync Service Account</a>
</div>
<div class="btn-group pull-up-10">
	{{ HTML::modelNaviateToolbar('admin.users.show', $user->previous(), 'admin.users.index', 'admin.users.show', $user->next()) }}
</div>
@stop

@section('content')
<div class="row">
	<div class="col-sm-3">
		<img src="{{ $avatarPresenter->getAvatar($user->id, $user->email, 2, '_311') }}" style="width: 311px; height: 311px;" class="thumbnail img-responsive {{ HTML::userStatus($user) }}" alt="">
		<a href="{{ action('Admin\Utilities\UserController@getRemoveProfileImage', $user->id) }}"><i class="fa fa-trash-o"></i> Remove profile image</a>
		<div class="mb30"></div>

		<h5 class="subtitle">Connect</h5>
		<ul class="profile-social-list">
			<li><i class="fa fa-envelope-o"></i> <a href="mailto:{{{ $user->email }}}">{{{ $user->email }}}</a></li>
		</ul>

		<div class="mb30"></div>


	</div>
	<div class="col-sm-9">

		<div class="profile-header">
			<h2 class="profile-name">{{{ $user->first_name.' '.$user->last_name }}}</h2>
			<div class="profile-location"><i class="fa fa-envelope-o"></i> <a href="mailto:{{{ $user->email }}}">{{{ $user->email }}}</a></div>
			

			<div class="mb20"></div>

			<a href="mailto:{{{ $user->email }}}" class="btn btn-white"><i class="fa fa-envelope-o"></i> Email</a>
			<a href="{{ action('Admin\Utilities\TenantController@getSyncAccount', array('user' => $user->id)) }}" data-toggle="modal" data-target="#modalHousing" class="btn btn-lightblue">Sync Service Account</a>
			<a href="{{ action('Admin\Utilities\UserController@getResetPassword', array('user' => $user->id)) }}" data-toggle="modal" data-target="#modalHousing" class="btn btn-primary-alt">Change Password</a>
		</div>

		<ul class="nav nav-tabs nav-justified nav-profile">
			<li class="active"><a href="#services" data-toggle="tab"><strong>Service Accounts</strong></a></li>
            <li><a href="#activity" data-toggle="tab"><strong><i class="fa fa-clock-o"></i> Recent Activity</strong></a></li>
		</ul>

		<div class="tab-content">
			<div class="tab-pane active" id="services">
				<div class="activity-list">
					@if(count($serviceAccounts) == 0 and $user->admin == false)
					<p>There are no service accounts for this user. <a href="{{ action('Admin\Utilities\TenantController@getSyncAccount', array('user' => $user->id)) }}" data-toggle="modal" data-target="#modalHousing">Sync service account now.</a></p>
					@endif
					@if($user->admin == true)
					<div class="media act-media">
						<a class="pull-left" href="#">
							<img class="media-object act-thumb" src="{{ app_url() }}/assets/img/cloud.png" alt="">
						</a>
						<div class="media-body act-media-body">
							<strong>Administrator Access</strong>
							<br><small>Administrators can access all service accounts by default.</small>
						</div>
					</div>
					@endif
					@foreach($serviceAccounts as $account)
					<div class="media act-media">
						<a class="pull-left" href="#">
							<img class="media-object act-thumb" src="{{ app_url() }}/assets/img/cloud.png" alt="">
						</a>
						<div class="media-body act-media-body">
							<div class="row">
								<div class="col-md-8">
									<strong><a href="{{ action('Admin\AccountsController@show', [$account->id]) }}">{{{ $account->account_name }}}</a></strong>
									<br><small>Created on <strong>{{{ with(new Carbon($account->created_at))->toDateTimeString() }}}</strong>. Last updated: <strong>{{{ with(new Carbon($account->updated_at))->toDateTimeString() }}}</strong></small><br>
									<small class="text-muted">Tenant: {{{ $account->tenant_name }}}</small>
								</div>
								<div class="col-md-4">
									<div class="btn-group">
										{{ Form::open(array('method' => 'DELETE', 'action' => 'Admin\Utilities\TenantController@getSyncAccount')) }}
										<input type="hidden" name="account" value="{{{ $account->tenant_id }}}">
										<input type="hidden" name="user" value="{{{ $user->id }}}">
										<button type="submit" class="btn btn-lightblue"><i class="fa fa-trash-o"></i> Remove this Service Account</button>
										{{ Form::close() }}
									</div>
								</div>
							</div>
						</div>
					</div>
					@endforeach
				</div>
			</div>
            <div class="tab-pane" id="activity">
                <div class="row">
                    <div class="col-sm-12">
                        {{ $activityData->links() }}
                    </div>
                </div>
                <div class="activity-list">
                    {{ $activityStream }}
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        {{ $activityData->links() }}
                    </div>
                </div>
            </div>
		</div>

	</div>
</div>
@stop