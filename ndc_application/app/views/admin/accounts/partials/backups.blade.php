<div class="table-responsive">
    <table class="table table-hover table-striped">
        <thead>
        <tr>
            <th>Backup</th>
            <th>Size</th>
            <th>Generated On</th>
        </tr>
        </thead>
        <tbody>
        @foreach($backups as $backup)
            <tr>
                <td>{{ $backup->getName() }}</td>
                <td>{{ UI::formatSizeUnits($backup->getSize()) }}</td>
                <td>{{ $backup->getCreateDate()->diffForHumans() }}</td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>

