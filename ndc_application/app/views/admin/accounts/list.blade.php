@extends('layouts.master')

@section('pageTitle')
{{ branding_get_inline_logo() }} Service Accounts <small>Service Access Accounts</small>
@stop

@section('buttonBar')
<div class="btn-group pull-up-10">
	<button type="button" class="btn btn-lightblue dropdown-toggle" data-toggle="dropdown">
		<i class="glyphicon glyphicon-cog inline-icon"></i> <span class="caret"></span>
	</button>
	<ul class="dropdown-menu pull-left" role="menu">
		<li><a href="{{ action('Admin\Utilities\TenantController@getMigrateTenants') }}">Migrate All Tenants</a></li>
		<li><a href="{{ action('Admin\Utilities\TenantController@getRollbackTenants') }}">Rollback All Tenants</a></li>
		<li><a data-toggle="modal" data-backdrop="static" data-target="#refreshTenantModal" class="btn btn-danger-alt">Refresh All Tenants</a></li>
	</ul>
</div>
<div class="btn-group pull-up-10">
	<a href="{{ route('admin.accounts.create') }}" class="btn btn-white"><i class="fa fa-plus inline-icon"></i> New Service Account</a>
</div>

@stop

@section('content')

<div class="row">
	
	<div class="col-md-12">
		{{ $accounts->links() }}
	</div>

	<div class="col-md-12">
		<div class="table-responsive">
			<table class="table table-hidaction table-hover mb30">
				<thead>
					<tr>
						<th>#</th>
						<th>Account Name</th>
                        <th></th>
						<th>Tenant Name</th>
						<th>Created At</th>
						<th>Updated At</th>
					</tr>
				</thead>
				<tbody>
					@foreach($accounts as $account)
					<tr>
						<td><a href="{{{ action('Admin\AccountsController@show', [$account->id])  }}}">{{{ $account->id }}}</a></td>
						<td><a href="{{{ action('Admin\AccountsController@show', [$account->id])  }}}">{{{ $account->account_name }}}</a></td>
                        <td>
                            {{ Form::open(array('method' => 'POST', 'action' => 'App\ServiceAccountController@postChoose')) }}
                            <input type="hidden" name="account" value="{{{ $account->tenant_id }}}">
                            <input type="hidden" name="accountid" value="{{{ $account->id }}}">
                            <button type="submit" class="btn btn-link"><i class="fa fa-cloud"></i> Connect now</button>
                            {{ Form::close() }}
                        </td>
						<td><a href="{{{ action('Admin\AccountsController@show', [$account->id])  }}}">{{{ $account->tenant_name }}}</a></td>
						<td><a href="{{{ action('Admin\AccountsController@show', [$account->id])  }}}">{{{ $account->created_at }}}</a></td>
						<td><a href="{{{ action('Admin\AccountsController@show', [$account->id])  }}}">{{{ $account->updated_at }}}</a></td>
					</tr>
					@endforeach
				</tbody>
			</table>
		</div>
	</div>

	<div class="col-md-12">
		{{ $accounts->links() }}
	</div>


</div>

<div class="modal fade" id="refreshTenantModal" tabindex="-1" role="dialog" aria-labelledby="refreshTenantModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="refreshTenantModalLabel"><i class="glyphicon glyphicon-warning-sign"></i> You are about to refresh all tenant accounts.</h4>
      </div>
      <div class="modal-body">
        <p>You are requesting to <strong>refresh all tenant accounts</strong>. This will remove all account data, and may irreversibly damage the underlying data store. This action is only recommended for development servers.</p>
      </div>
      <div class="modal-footer">
        <a href="{{ action('Admin\Utilities\TenantController@getRefreshTenants') }}" class="btn btn-danger">Yes, refresh tenants</a>
        <button type="button" class="btn btn-white" data-dismiss="modal">Cancel</button>
      </div>
    </div>
  </div>
</div>
@stop