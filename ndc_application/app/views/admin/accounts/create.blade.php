@extends('layouts.master')

@section('pageTitle')
<i class="fa fa-plus"></i> You are adding a new service account
@stop

@section('buttonBar')
<div class="btn-group">
	<a href="{{ route('admin.accounts.index') }}" class="btn btn-danger">Cancel</a>
</div>
@stop

@section('content')

<div class="row">

	<div class="col-md-12">
		{{  Form::open(array('route' => 'admin.accounts.store', 'method' => 'post', 'class' => 'form-horizontal')) }}
			<div class="panel panel-default">
				<div class="panel-heading">
					<h4 class="panel-title">You are creating a new service account.</h4>
					<p>Service accounts allow user accounts to connect to a service database, or tenant.</p>
				</div>
				<div class="panel-body">

					<div class="form-group">
						<label class="col-sm-2 control-label">Account Name:</label>
						<div class="col-sm-8">
							<input type="text" name="account_name" class="form-control" value="{{ Input::old('account_name', '') }}" autofocus>
							{{ Form::errorMsg('account_name') }}
						</div>
					</div>

				</div><!-- panel-body -->
				<div class="panel-footer">
					<button type="submit'" class="btn btn-primary">Submit</button>
					<button type="reset" class="btn btn-default">Reset</button>
					<span><strong>Note</strong>: Creating accounts may take up to a few minutes, as account creation is building databases in the background.</span>
				</div><!-- panel-footer -->
			</div><!-- panel-default -->
		{{ Form::close() }}
	</div>

</div>

@stop