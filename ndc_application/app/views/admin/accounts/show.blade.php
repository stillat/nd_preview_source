@extends('layouts.master')

@section('pageTitle')
    {{ branding_get_inline_logo() }} Service Account: {{{ $account->account_name }}}
@stop

@section('buttonBar')
    <div class="btn-group pull-up-10">

        {{ Form::open(array('method' => 'POST', 'action' => 'App\ServiceAccountController@postChoose', 'style'=>'float:left;display:inline-block;')) }}
        <input type="hidden" name="account" value="{{{ $account->tenant_id }}}">
        <input type="hidden" name="accountid" value="{{{ $account->id }}}">
        <button type="submit" class="btn btn-white"><i class="fa fa-cloud inline-icon"></i> Connect now</button>
        {{ Form::close() }}

        <button type="button" class="btn btn-white dropdown-toggle" data-toggle="dropdown">
            <i class="fa fa-cogs inline-icon"></i>
            <span class="caret"></span>
            <span class="sr-only">Toggle Dropdown</span>
        </button>
        <ul class="dropdown-menu" role="menu">
            <li>
                @if($account->active)
                    {{ Form::open(array('method' => 'POST', 'action' => 'Admin\Utilities\TenantController@postDeactivateAccount', 'style'=>'display:inline-block;')) }}
                    <input type="hidden" name="account" value="{{{ $account->tenant_id }}}">
                    <input type="hidden" name="accountid" value="{{{ $account->id }}}">
                    <button type="submit" class="btn btn-link">Deactivate Account</button>
                    {{ Form::close() }}
                @else
                    {{ Form::open(array('method' => 'POST', 'action' => 'Admin\Utilities\TenantController@postActivateAccount', 'style'=>'display:inline-block;')) }}
                    <input type="hidden" name="account" value="{{{ $account->tenant_id }}}">
                    <input type="hidden" name="accountid" value="{{{ $account->id }}}">
                    <button type="submit" class="btn btn-link">Activate Account</button>
                    {{ Form::close() }}
                @endif
            </li>
            <li><a href="{{ action('Admin\Utilities\TenantController@getRemoveAccount', [$account->id]) }}" data-toggle="modal" data-target="#modalHousing" class="btn btn-link" style="color: #428bca;">Remove Account</a></li>
        </ul>
    </div>
@stop

@section('content')

    <div class="row">
        <div class="col-sm-12">
            <ul class="nav nav-tabs">
                <li class="active"><a href="#generalInformation" data-toggle="tab"><strong>
                            General Information</strong></a></li>
                <li><a href="#accountUsers" data-toggle="tab"><strong><i class="fa fa-users"></i>
                            Account Users</strong></a></li>
                <li><a href="#backups" data-toggle="tab"><strong><i class="fa fa-compress"></i>
                            Backups</strong></a></li>
            </ul>
            <div class="tab-content mb30">
                <div class="tab-pane active" id="generalInformation">
                    @include('admin.dashboard_widgets.account')
                </div>
                <div class="tab-pane" id="accountUsers">
                    @foreach($accountUsers as $user)
                        <div class="media act-media">
                            <a class="pull-left" href="#">
                                <img class="media-object act-thumb"
                                     src="{{ $avatarPresenter->getAvatar($user->id, $user->email, 2) }}" alt="">
                            </a>

                            <div class="media-body act-media-body">
                                <strong><a href="{{ action('Admin\UsersController@show', [$user->id]) }}">{{{ $user->first_name.' '.$user->last_name }}}</a></strong><br>
                                <small class="text-muted"><i class="fa fa-envelope"></i> <a
                                            href="mailto:{{{ $user->email }}}">{{{ $user->email }}}</a></small>
                            </div>
                        </div>
                    @endforeach
                </div>
                <div class="tab-pane" id="backups">
                    @include('admin.accounts.partials.backups')
                </div>
            </div>
        </div>
    </div>

@stop