@extends('layouts.master')

@section('pageTitle')
    {{ branding_get_inline_logo() }} Update Service Terms
@stop
@section('content')
    <div class="row">

        <div class="col-md-12">
            {{  Form::open(array('action' => 'Admin\UpdateTermsController@postIndex', 'method' => 'post', 'class' => 'form-horizontal')) }}
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="panel-title">You are updating the service terms and conditions</h4>
                    <p>Users will be required to use read the update notifications and accept them to continue using the service.</p>
                </div>
                <div class="panel-body">

                    <div class="form-group">
                        <label class="col-sm-2 control-label">Title:</label>
                        <div class="col-sm-8">
                            <input type="text" name="title" class="form-control" value="{{ Input::old('title', 'Important Service Updates') }}">
                            {{ Form::errorMsg('title') }}
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">Content:</label>
                        <div class="col-sm-8">
                            <textarea name="content" id="termsContent" class="form-control" cols="30" rows="10" autofocus></textarea>
                            {{ Form::errorMsg('title') }}
                        </div>
                    </div>

                </div><!-- panel-body -->
                <div class="panel-footer">
                    <button type="submit'" class="btn btn-primary">Submit</button>
                    <button type="reset" class="btn btn-default">Reset</button>
                </div><!-- panel-footer -->
            </div><!-- panel-default -->
            {{ Form::close() }}
        </div>

    </div>
@stop