@extends('layouts.master')

@section('pageTitle')
    <i class="fa fa-database"></i> Database Query Runner
@stop

@section('additionalStyles')
    <link rel="stylesheet" href="{{ app_url() }}/assets/css/codemirror/codemirror.css"/>
    <link rel="stylesheet" href="{{ app_url() }}/assets/css/codemirror/show-hint.css"/>
    <script src="{{ app_url() }}/assets/js/dep/codemirror/codemirror.js"></script>
    <script src="{{ app_url() }}/assets/js/dep/codemirror/sql.js"></script>
    <script src="{{ app_url() }}/assets/js/dep/codemirror/show-hint.js"></script>
    <script src="{{ app_url() }}/assets/js/dep/codemirror/sql-hint.js"></script>
    <style>.CodeMirror { height: auto; }</style>
@stop

@section('content')
{{ Form::open(['action' => 'Admin\DatabaseManagement\QueryController@postIndex', 'method' => 'post', 'class' => 'form-horizontal']) }}
<div class="row">
    <div class="col-sm-12">
        <fieldset>
            <legend><i class="fa fa-key"></i> Authentication</legend>
            <div class="form-group">
                <label for="inputPassword" class="col-sm-2 control-label">Enter your password</label>
                <div class="col-sm-6">
                    <input type="password" class="form-control" id="inputPassword" name="current_password" placeholder="Your password">
                </div>
            </div>
            <div class="form-group">
                <label for="inputSecurityCode" class="col-sm-2 control-label">Last four of security code:</label>
                <div class="col-sm-6">
                    <input type="password" class="form-control" id="inputSecurityCode" name="security_code" placeholder="The last four digits of the current installation's security code">
                </div>
            </div>
        </fieldset>
    </div>
</div>
<div class="row">
    <div class="col-sm-12" style="margin-top:20px;">
<fieldset class="mb30">
    <legend><i class="fa fa-database"></i> Query</legend>
            <textarea id="code" name="code">{{ Input::old('code', '-- Tenant Database Query Runner v1.0.0
-- Copyright (c) Paradox One LLC. All rights reserved.
-- The database query runner requires advanced authentication to run.
-- The query runner is not intended to display results. Use third-party software to display results.
-- The runner is simply a utility to run commands against all tenants at once, without needing to swap connections.') }}</textarea>
</fieldset>
    </div>
</div>
<div class="row">
    <div class="col-sm-12">
        <ul class="nav nav-tabs">
            <li class="active"><a href="#queryran" data-toggle="tab"><strong>Final Query Ran</strong></a></li>
        </ul>
        <div class="tab-content mb30">
            <div class="tab-pane active" id="queryran">
                <p>The final query that was ran against the database engine will differ from the query entered in the query box.</p>
                <pre><?php if (isset($finalQuery)) {
    echo $finalQuery;
} ?></pre>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-sm-12">
        <div class="checkbox block"><label><input type="checkbox" name="dry" checked="checked"> Do not run any query on the database engine</label></div>
        <button type="submit" class="btn btn-primary" style="margin-top:20px;"><i class="fa fa-play"></i> Run Query</button>
    </div>
</div>
{{ Form::close() }}
@stop

@section('additionalScripts')
    <script>
        window.onload = function() {
            var mime = 'text/x-mariadb';
            if (window.location.href.indexOf('mime=') > -1) {
                mime = window.location.href.substr(window.location.href.indexOf('mime=') + 5);
            }
            window.editor = CodeMirror.fromTextArea(document.getElementById('code'), {
                mode: mime,
                indentWithTabs: true,
                smartIndent: true,
                lineNumbers: true,
                matchBrackets : true,
                autofocus: true,
                viewportMargin: Infinity,
                extraKeys: {"Ctrl-Space": "autocomplete"},
                hintOptions: {tables: {
                    users: {name: null, score: null, birthDate: null},
                    countries: {name: null, population: null, size: null}
                }}
            });
        };
    </script>
@stop