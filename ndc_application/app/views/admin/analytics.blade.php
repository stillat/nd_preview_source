@extends('layouts.master')

@section('pageTitle')
    {{ branding_get_inline_logo() }} ND Counties Service Analytics
@stop
@section('content')

    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title"><i class="fa fa-bar-chart-o"></i> Analytics</h3>
                </div>
                <div class="panel-body" class="nopadding" style="padding: 0">
                    <ul class="nav nav-tabs nopadding">
                        <li class="active"><a href="#sysAnalytics" data-toggle="tab"><strong>System Database Usage Analytics</strong></a></li>
                    </ul>
                    <div class="tab-content mb30">
                        <div class="tab-pane active" id="sysAnalytics">
                            @include('admin.dashboard_widgets.database_size_analytics')
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@stop