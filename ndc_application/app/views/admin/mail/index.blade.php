@extends('layouts.master')

@section('pageTitle')
    <i class="fa fa-envelope"></i> Customer Message Center
@stop

@section('content')
    {{ Form::open(['action' => 'Admin\MailController@postIndex', 'method' => 'post']) }}
    <div class="panel panel-default">
        <div class="panel-body">

            <div class="form-group mb5">
                <select class="chosen-select" name="accounts[]" multiple data-placeholder="Recipients">
                    @foreach($accounts as $account)
                        <option value="{{$account->id}}">{{{ $account->account_name }}}</option>
                    @endforeach
                </select>
            </div>

            <div class="form-group">
                <input type="text" name="subject" placeholder="Subject" class="form-control"/>
            </div>

            <textarea id="wysiwyg" name="message" placeholder="Your message here..." class="form-control" rows="20">Dear @@recipient,<br><br>Message content here...<br><br>ND Counties Support</textarea>

        </div>
        <div class="panel-footer">
            <button type="submit" class="btn btn-primary">Send</button>
        </div>
    </div>
    {{ Form::close() }}
@stop

@section('additionalStyles')
    <link rel="stylesheet" href="{{{ app_url() }}}/assets/css/bootstrap-wysihtml5.css"/>
@stop

@section('additionalScripts')
    <script src="{{{ app_url() }}}/assets/js/dep/chosen.jquery.min.js" defer></script>
    <script src="{{{ app_url() }}}/assets/js/dep/wysihtml5-0.3.0.min.js" defer></script>
    <script src="{{{ app_url() }}}/assets/js/dep/bootstrap-wysihtml5.js" defer></script>

    <script src="{{{ app_url() }}}/assets/js/admin/mail.js" defer></script>
@stop