@extends('layouts.master')

@section('pageTitle')
    {{ branding_get_inline_logo() }} ND Counties Service Administration
@stop
@section('content')

	<div class="row">
        <div class="col-sm-3">
            <div class="row">
                @include('app.dashboard_widgets.user_card')
            </div>
            <div class="row">
                <ul class="list-unstyled">
                    <li><a href="{{ url('admin/analytics') }}"><i class="fa fa-bar-chart-o"></i> System Analytics</a></li>
<li>
<strong>Apache Modules</strong><br>
<pre><code>
<?php

if (function_exists('apache_get_modules')) {
    var_dump(apache_get_modules());
}

?>
</code>
</pre>
</li>
                </ul>
            </div>
        </div>
		<div class="col-sm-9">
			@include('admin.dashboard_widgets.systemwide_utilities')
		</div>

	</div>
@stop


@section('additionalScripts')
<script src="{{{ app_url() }}}/assets/js/a_dash.min.js" defer></script>
@stop