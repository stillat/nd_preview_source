<div class="table-responsive">
    {{ Form::open(['action' => ['Admin\AccountsController@update', $account->id], 'method' => 'put']) }}
    <div class="form-group">
        <label class="col-sm-3 control-label">Active Status:</label>

        <div class="col-sm-9">
            @if ($account->active)
                <p class="form-control-static text-success"><i class="fa fa-check-circle-o"></i> Active</p>
            @else
                <p class="form-control-static text-danger"><i class="fa fa-circle-o"></i> Inactive</p>
            @endif
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-3 control-label">Tenant Name:</label>

        <div class="col-sm-9">
            <p class="form-control-static">{{{ $account->tenant_name }}}</p>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-3 control-label" for="account_name">Account Name:</label>

        <div class="col-sm-9">
            <input type="text" id="account_name" name="account_name"
                   class="form-control mousetrap ndc_form_home_element {{{ user_form_size() }}}"
                   value="{{{ Input::old('account_name', $account->account_name) }}}" autofocus>
            {{ Form::errorMsg('account_name') }}
        </div>
    </div>
    <div class="form-group is-required">
        <label class="col-sm-3 control-label" for="address_line_1">Address Line 1:</label>

        <div class="col-sm-9">
            <input type="text" id="address_line_1" name="address_line_1"
                   class="form-control mousetrap {{{ user_form_size() }}}"
                   value="{{{ Input::old('address_line_1', $account->address_line_1) }}}">
            {{ Form::errorMsg('address_line_1') }}
        </div>
    </div>
    <div class="form-group is-required">
        <label class="col-sm-3 control-label" for="address_line_2">Address Line 2:</label>

        <div class="col-sm-9">
            <input type="text" id="address_line_2" name="address_line_2"
                   class="form-control mousetrap {{{ user_form_size() }}}"
                   value="{{{ Input::old('address_line_2', $account->address_line_2) }}}">
            {{ Form::errorMsg('address_line_2') }}
        </div>
    </div>
    <div class="form-group is-required">
        <label class="col-sm-3 control-label" for="city">City:</label>

        <div class="col-sm-4">
            <input type="text" id="city" name="city" class="form-control mousetrap {{{ user_form_size() }}}"
                   value="{{{ Input::old('city', $account->city) }}}">
            {{ Form::errorMsg('city') }}
        </div>
    </div>
    <div class="form-group is-required">
        <label class="col-sm-3 control-label" for="state">State:</label>

        <div class="col-sm-4">
            <input type="text" id="state" name="state" class="form-control mousetrap {{{ user_form_size() }}}"
                   value="{{{ Input::old('state', $account->state) }}}">
            {{ Form::errorMsg('state') }}
        </div>
    </div>
    <div class="form-group is-required">
        <label class="col-sm-3 control-label" for="country">Country:</label>

        <div class="col-sm-4">
            <input type="text" id="country" name="country" class="form-control mousetrap {{{ user_form_size() }}}"
                   value="{{{ Input::old('country', $account->country) }}}">
            {{ Form::errorMsg('country') }}
        </div>
    </div>
    <div class="form-group is-required">
        <label class="col-sm-3 control-label" for="zip">Zip:</label>

        <div class="col-sm-2">
            <input type="text" id="zip" name="zip" class="form-control mousetrap {{{ user_form_size() }}}"
                   value="{{{ Input::old('zip', $account->zip) }}}">
            {{ Form::errorMsg('zip') }}
        </div>
    </div>
    <div class="form-group">
        <button type="submit" class="col-sm-offset-3 btn btn-primary">Update Account Contact Information</button>
    </div>
    {{ Form::close() }}
</div>