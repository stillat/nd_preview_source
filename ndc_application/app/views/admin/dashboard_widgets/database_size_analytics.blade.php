<table class="table-responsive">
    <table class="table table-hover table-striped">
        <thead>
        <tr>
            <th>Database Name</th>
            <th>Database Size in MB</th>
            <th>Free Space in MB</th>
        </tr>
        </thead>
        <tbody>
        @foreach($systemAnalytics as $analyticsRecord)
        <tr>
            <td>{{{ $analyticsRecord->database_name }}}</td>
            <td>{{{ $analyticsRecord->size }}}</td>
            <td>{{{ $analyticsRecord->free_size }}}</td>
        </tr>
        @endforeach
        </tbody>
    </table>
</table>