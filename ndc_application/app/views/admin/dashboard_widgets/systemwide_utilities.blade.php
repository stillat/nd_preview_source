<?php UI::enqueueScript('admin/systemwideutil.js'); ?>
<div class="panel panel-primary">
    <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-wrench"></i> Systemwide Utilities</h3>
    </div>
    <div class="panel-body" style="max-height: 350px; overflow-y: auto; padding:10px;">
        <p>Systemwide utilities allow you to perform specific actions on all tenants automatically. Do <strong>not</strong> run any utilities you are not comfortable running.</p>

        <input class="form-control" type="text" id="utilitiesTableFilter" placeholder="Filter utilities table..." autofocus="autofocus" style="margin-bottom: 10px;"/>

        <table class="table table-bordered" id="utilitiesTable">
            <thead>
            <tr>
                <th class="wd-200">Utility</th>
                <th>Description</th>
                <th class="wd-100"></th>
            </tr>
            </thead>
            <tbody>
                <tr class="warning">
                    <td><i class="fa fa-truck"></i> Patch Orphaned Fuel Consumable Records</td>
                    <td>Adds back equipment records that may have been incorrectly removed due to [BUG3002]. This should be a one time action.</td>
                    <td><a class="btn btn-link" href="{{ action('Admin\Utilities\SystemWideUtilitiesController@getPatchOrphanedFuels') }}"><i class="fa fa-play"></i> Run now</a></td>
                </tr>
                <tr class="success">
                    <td><i class="fa fa-compress"></i> Backup <strong>Service</strong> Database</td>
                    <td>Creates a backup of the service database.<br>This operation can take a long time. Try to only run this operation during non-peak service hours. This operation is automatically run nightly.</td>
                    <td><a class="btn btn-link" href="{{ action('Admin\Utilities\SystemWideUtilitiesController@getClearUserExports') }}"><i class="fa fa-play"></i> Run now</a></td>
                </tr>
                <tr class="success">
                    <td><i class="fa fa-compress"></i> Backup <strong>Client</strong> Databases</td>
                    <td>Creates a backup of all client databases.<br>This operation can take a long time. Try to only run this operation during non-peak service hours. This operation is automatically run nightly.</td>
                    <td><a class="btn btn-link" href="{{ action('Admin\Utilities\SystemWideUtilitiesController@getClearUserExports') }}"><i class="fa fa-play"></i> Run now</a></td>
                </tr>
                <tr class="danger">
                    <td>Clear User Exports</td>
                    <td>Removes all user's exports from disk.</td>
                    <td><a class="btn btn-link" href="{{ action('Admin\Utilities\SystemWideUtilitiesController@getClearUserExports') }}"><i class="fa fa-play"></i> Run now</a></td>
                </tr>
                <tr class="danger">
                    <td>Clear Application Cache</td>
                    <td>Clears the applications cache store.</td>
                    <td><a class="btn btn-link" href="{{ action('Admin\Utilities\SystemWideUtilitiesController@getClearApplicationCache') }}"><i class="fa fa-play"></i> Run now</a></td>
                </tr>
                <tr class="info">
                    <td>Sync User and System Settings</td>
                    <td>Syncs all user settings and brings them current with any updates.</td>
                    <td><a class="btn btn-link" href="{{ action('Admin\Utilities\SystemWideUtilitiesController@getSyncUserSettings') }}"><i class="fa fa-play"></i> Run now</a></td>
                </tr>
                <tr class="warning">
                    <td>Re-install System Reports</td>
                    <td>Re-installs all of the default system reports; rolling back any changes made to match a new install.</td>
                    <td><a class="btn btn-link" href="{{ action('Admin\Utilities\ReportsController@getInstallDefaultReports') }}"><i class="fa fa-play"></i> Run now</a></td>
                </tr>
            </tbody>
        </table>
    </div>
</div>