<div class="panel panel-primary">
    <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-hdd-o"></i> User Exports Disk Cost</h3>
    </div>
    <div class="panel-body " style="padding: 10px;">
        <p>This widget estimates the monthly cost of the current disk usage, as used by user's data exports.</p>
        <div class="row">
            <div class="col-sm-6">
                <dl class="dl-horizontal">
                    <dt>Maximum Desired Disk Space</dt>
                    <dd>{{{ $maxDiskCostNice }}}</dd>
                    <dt>Current Desired Disk Space</dt>
                    <dd>{{{ $diskCostNice }}}</dd>
                </dl>
            </div>
            <div class="col-sm-6">
                <dl class="dl-horizontal">
                    <dt>Current Cost</dt>
                    <dd>{{{ $diskCostMonthly }}} <small><strong>USD</strong></small></dd>
                    <dt>Cost / GB</dt>
                    <dd>{{{ $costPerGB }}} <small><strong>USD</strong></small></dd>
                </dl>
            </div>
        </div>
        <div class="progress">
            <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="{{{ $currentDiskCost }}}" aria-valuemin="0" aria-valuemax="{{{ $maxDiskCost  }}}" style="width: {{ $diskPercentage }}%">
            </div>
        </div>
        <hr/>
        <a class="btn btn-block btn-white" href="{{ action('Admin\Utilities\SystemWideUtilitiesController@getClearUserExports') }}"><i class="fa fa-trash-o"></i> Clear User Exports</a>
    </div>
 </div>