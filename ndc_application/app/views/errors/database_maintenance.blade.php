<!DOCTYPE html>
<html>
<head>
    <link href='https://fonts.googleapis.com/css?family=Raleway:400,200,300,600' rel='stylesheet'>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>ND Counties | We're updating things</title>
    <link rel="stylesheet" href="{{ app_url() }}/micros/maint/css/bootstrap.min.css">
    <link rel="stylesheet" href="{{ app_url() }}/micros/maint/css/font-awesome.min.css">
    <link rel="stylesheet" href="{{ app_url() }}/micros/maint/css/slick.css">
    <link rel="stylesheet" href="{{ app_url() }}/micros/maint/css/main.css">
    <!--[if gte IE 9]>
    <link rel="stylesheet" href="{{ app_url() }}/micros/maint/css/ie.css">
    <![endif]-->
</head>
<body data-spy="scroll" data-target=".main-nav">
<header class="main-header" id="home">
    <div class="overlay"></div>
    <div class="container">
        <div class="row">
            <div class="col-md-11 col-md-offset-1">
                <div class="logo"><a href="{{ app_url() }}" style="color: white; font-size: 40px; letter-spacing: -2px;"><i class="fa fa-cloud"></i> ND Counties</a></div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-5 col-md-offset-1">
                <div class="head-hero">
                    <h1>We are <span class="highlight">Updating</span> some things</h1>
                    <p style="color: white;"><i class="fa fa-arrow-down"></i> Scroll down for more details </p>
                </div>
            </div>
            <div class="col-md-5 col-md-offset-1 visible-md visible-lg over-hide"> <img src="{{ app_url() }}/micros/maint/img/maint.png" alt="App Image" class="hero-img"> </div>
        </div>
    </div>
</header>
<nav role="navigation" class="main-nav visible-sm visible-md visible-lg">
    <ul class="nav">
        <li><a href="#home" class="text-hide" title="Home" data-placement="left">HOME</a></li>
        <li><a href="#changes" class="text-hide" title="Changes" data-placement="left">CHANGES</a></li>
    </ul>
</nav>
<section class="features section-spacing" id="changes">
    <div class="container">
        @include('errors.maint.'.Config::get('crms.maintenanceMessage', 'general'))
    </div>
</section>

<footer class="site-footer section-spacing">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
                <p class="scroll-top"><a href="#"><i class="fa fa-chevron-up"></i></a></p>
                <h3>Say Hi, Get In Touch</h3>
                <ul class="social">
                    <li><a href="mailto:support@ndcounties.com"><i class="fa fa-envelope"></i></a></li>
                </ul>
                <small>&copy; Paradox One, LLC. All Rights Reserved.</small> </div>
        </div>
    </div>
</footer>

<script src="{{ app_url() }}/micros/maint/js/jquery-1.11.1.min.js"></script>
<script src="{{ app_url() }}/micros/maint/js/bootstrap.min.js"></script>
<script src="{{ app_url() }}/micros/maint/js/slick.min.js"></script>
<script src="{{ app_url() }}/micros/maint/js/main.js"></script>
</body>
</html>
