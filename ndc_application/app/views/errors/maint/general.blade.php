<h2 class="text-center">About the Updates</h2>
      <div>
        <div class="row">
          <div class="col-md-7">
            <div class="features-container">
              <div class="app-features row">
                <div class="col-md-3 col-md-offset-1 col-sm-3 col-xs-3 text-center"><img src="{{ app_url() }}/micros/maint/img/icon-2.png"></div>
                <div class="col-md-8 col-sm-9 col-xs-9">
                  <h4>User Experience</h4>
                  <p>We've made a few subtle changes to the user interface to improve the user experience. These updates improve workflow, layout and the accessibility of the many features that are accessible through the user interface.</p>
                </div>
              </div>
              <div class="app-features row">
                <div class="col-md-3 col-md-offset-1 col-sm-3 col-xs-3 text-center"><img src="{{ app_url() }}/micros/maint/img/icon-4.png"></div>
                <div class="col-md-8 col-sm-9 col-xs-9">
                  <h4>User Feedback</h4>
                  <p>We receive feedback from industry experts daily, from people who use our products, and from people who are just interested in improving the industry as a whole. We evaluate each piece of feedback we receive, and if we like it, we find a way to incorporate it into the product.</p>
                </div>
              </div>
              <div class="app-features row">
                <div class="col-md-3 col-md-offset-1 col-sm-3 col-xs-3 text-center"><img src="{{ app_url() }}/micros/maint/img/Shield.png"></div>
                <div class="col-md-8 col-sm-9 col-xs-9">
                  <h4>Account Security and Recovery</h4>
                  <p>We are dedicated to ensuring the security and recoverability of our user's accounts and data. Each update we release contains security updates and safeguards to improve the overall stability of our product. This lets our users sleep well at night, as well as us.</p>
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-5 text-center"> <img src="{{ app_url() }}/micros/maint/img/maint.png" alt="App Feature img"> </div>
        </div>
      </div>
    </div>