<h2 class="text-center">About the Updates</h2>
      <div>
        <div class="row">
          <div class="col-md-7">
            <div class="features-container">
              <div class="app-features row">
                <div class="col-md-3 col-md-offset-1 col-sm-3 col-xs-3 text-center"><img src="{{ app_url() }}/micros/maint/img/compose.png"></div>
                <div class="col-md-8 col-sm-9 col-xs-9">
                  <h4>Work Record Updater</h4>
                  <p>We've completed the core work record updater. This means you can update existing work records and change the employee, activity, work record settings and even any associated property information.</p>
                </div>
              </div>
              <div class="app-features row">
                <div class="col-md-3 col-md-offset-1 col-sm-3 col-xs-3 text-center"><img src="{{ app_url() }}/micros/maint/img/browser.png"></div>
                <div class="col-md-8 col-sm-9 col-xs-9">
                  <h4>Improved Usability and Compatibility</h4>
                  <p>We've improved the usability and compatibility of our online service. We've made over 300 small updates that improve various aspects of the user interface and how our software works with different web browsers.</p>
                </div>
              </div>
              <div class="app-features row">
                <div class="col-md-3 col-md-offset-1 col-sm-3 col-xs-3 text-center"><img src="{{ app_url() }}/micros/maint/img/Shield.png"></div>
                <div class="col-md-8 col-sm-9 col-xs-9">
                  <h4>Account Security and Recovery</h4>
                  <p>We are dedicated to ensuring the security and recoverability of our user's accounts and data. Each update we release contains security updates and safeguards to improve the overall stability of our product. This lets our users sleep well at night, as well as us.</p>
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-5 text-center"> <img src="{{ app_url() }}/micros/maint/img/maint.png" alt="App Feature img"> </div>
        </div>
      </div>
    </div>