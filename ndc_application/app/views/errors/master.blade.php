<!DOCTYPE html>
<html lang="en" ng-app="app">
<head>
  <meta charset="utf-8">
  <meta http-equiv=”X-UA-Compatible” content=”IE=edge”>
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
  <meta name="description" content="">
  <link rel="shortcut icon" href="{{ Config::get('app.url') }}/favicon.ico" type="image/ico">

  <title>@yield('title', 'ND Counties')</title>
  <?php  echo $__env->make('integration.universal_analytics'); ?>
</head>
<body>

</body>
</html>