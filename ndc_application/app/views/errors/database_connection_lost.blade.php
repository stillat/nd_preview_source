<!DOCTYPE html>
<html>
<head>
    <title>ND Counties</title>
    {{ UI::getCss() }}
    <?php  echo $__env->make('integration.universal_analytics'); ?>
</head>
<body class="signin">
<div class="container text-center">
    <form class="form-signin">
        <h2 class="form-signin-heading"><img src="{{ app_url() }}/assets/img/cloud.png">Be right back</h2>
        <p>We are performing scheduled maintenance and should be back in no time.</p>
    </form>
    <hr>
    <a href="https://ndcounties.com/app">ND Counties</a>
</div>
</body>
</html>