@extends('layouts.master')

@section('pageTitle')
    <small><i class="fa fa-warning"></i> Error 2001: Missing Reports</small>
@stop

@section('content')
    <div class="notfoundpanel">
        <h2>Missing Reports</h2>
        <h4>The system reports are missing and need to be installed before continuing.</h4>
    </div>
@stop