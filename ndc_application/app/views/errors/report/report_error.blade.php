<!DOCTYPE html>
<html>
<head>
    <title>ND Counties</title>
    {{ UI::getCss() }}
    <?php  echo $__env->make('integration.universal_analytics'); ?>
</head>
<body class="signin">
    <div class="container text-center">
      <form class="form-signin" style="max-width: 60%;">
        <h2 class="form-signin-heading"><img src="{{ app_url() }}/assets/img/cloud.png">An unexpected error occurred</h2>
        <div style="text-align: left;">
          <p>We apologize for the inconvenience, but it seems like an unexpected error has occurred. There are many things that can contribute to these kinds of errors, and right now we suggest that you go back and try your action again.</p>
          <p>We are automatically notified when these errors occur, but any extra information we can receive from you definitely helps. If you'd like to help us resolve this issue faster, please explain what you were trying to do and what you were doing right before and leading up to this error message. Don't worry about being brief: let us know everything you can think of. Did you resize your browser, or have apps that interact with our service open? Include those too!</p>
        </div>
        <textarea name="" id="" cols="30" rows="10" class="form-control" style="-webkit-box-shadow: 4px 3px 5px 0px rgba(122,122,122,1);-moz-box-shadow: 4px 3px 5px 0px rgba(122,122,122,1);box-shadow: 4px 3px 5px 0px rgba(122,122,122,1);"></textarea>
        <br><br>
        <button class="btn btn-primary"><i class="glyphicon glyphicon-comment"></i> Send feedback about this error</button>
     </form>
  <hr>
  <a href="#">ND Counties</a>
</div>
</body>
</html>