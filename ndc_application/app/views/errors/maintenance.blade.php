<!DOCTYPE html>
<html>
<head>
  <title>ND Counties</title>
  {{ UI::getCss() }}
  <?php  echo $__env->make('integration.universal_analytics'); ?>
</head>
<body class="signin">
<div class="container text-center">
  <form class="form-signin" style="max-width: 440px;">
    <h2 class="form-signin-heading"><i class="fa fa-gift"></i> We're updating some things</h2>
    <br>
    <p>We are updating things and will be right back!</p>
  </form>
  <hr>
  <a href="https://ndcounties.com/app">ND Counties</a>
</div>
</body>
</html>