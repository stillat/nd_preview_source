<!DOCTYPE html>
<html>
<head>
    <title>ND Counties</title>
    {{ UI::getCss() }}
    <?php  echo $__env->make('integration.universal_analytics'); ?>
</head>
<body class="signin">
<div class="container text-center" style="width: 60%;">
    <form class="form-signin">
        <h2 class="form-signin-heading"><img src="{{ app_url() }}/assets/img/cloud.png">Invalid Account</h2>
        <div class="alert alert-warning">
            The account you are trying to log into is invalid. Please contact support to have this issue resolved. You can also try to connect to a different account below.
        </div>
    </form>
    <div class="text-left">
        @include('app.accounts.list_choose')
    </div>
    <hr>
    <a href="https://ndcounties.com/app">ND Counties</a>
</div>
<script>var NDC_USER_SETTING_SHOW_ANIMATION_ON_CHANGE  = true;</script>
{{ UI::getJs() }}
</body>
</html>