<!DOCTYPE html>
<html>
<head>
    <title>ND Counties</title>
    {{ UI::getCss() }}
    <?php  echo $__env->make('integration.universal_analytics'); ?>
</head>
<body class="signin">
    <div class="container text-center">
      <form class="form-signin">
        <h2 class="form-signin-heading"><img src="{{ app_url() }}/assets/img/cloud.png">Unable to log in</h2>
        <div class="alert alert-warning">
          We were unable to log you in because there are no active service accounts associated with your user log in.
        </div>
     </form>
  <hr><a href="https://ndcounties.com/app">ND Counties</a>
</div>
</body>
</html>