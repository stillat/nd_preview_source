@extends('layouts.master')

@section('pageTitle')
<small><i class="fa fa-warning"></i> Error 404: Record Not Found</small>
@stop

@section('content')
    <div class="notfoundpanel">
        <h2>Page or Record Not Found</h2>
        <h4>The page or record you are looking for may have been removed, had its name changed or might be currently unavailable.</h4>
    </div>
@stop