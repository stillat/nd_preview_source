<!DOCTYPE html>
<html lang="en-US">
	<head>
		<meta charset="utf-8">
	</head>
	<body>
		<h2>Password Reset Successfully</h2>

		<div>
		    <p>Your password has been successfully reset. If you did not request this password change, contact Paradox One LLC immediately to protect your account.</p>
		</div>
	</body>
</html>