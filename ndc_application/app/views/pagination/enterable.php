<?php
	$presenter = new Illuminate\Pagination\BootstrapPresenter($paginator);
?>

<?php if ($paginator->getLastPage() > 1): ?>
	<ul class="pagination display-inline">
			<?php echo $presenter->render(); ?>
			<li>
				<?php
					echo Form::open(array('url' => $paginator->getUrl(0), 'method' => 'GET'));

					// Get the query data.
					$requestData = Input::instance();
					$queryData  = $requestData->query->all();

					// Remove the page value.
					unset($queryData['page']);

					// Loop over the query data items remaining and create hidden form fields.
					foreach($queryData as $dataField => $dataValue)
					{
						if (is_array($dataValue))
						{
							if (count($dataValue) > 0)
							{
								foreach($dataValue as $urlComponent)
								{
									echo Form::hidden($dataField.'[]', $urlComponent);
								}
							}
						}
						else
						{							
							echo Form::hidden($dataField, $dataValue);
						}
					}
				?>
				<input type="number" name="page" id="paginator_lbs_pages" min="0" max="<?php echo $paginator->getLastPage(); ?>" value="<?php echo $paginator->getCurrentPage(); ?>" placeholder="Page #" class="form-control paginator-box">
				<?php echo Form::close(); ?>
			</li>
	</ul>
<?php endif; ?>