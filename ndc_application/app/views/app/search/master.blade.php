@extends('layouts.master')

@section('pageTitle')
    <i class="fa fa-search"></i> {{{ $title }}}
@stop

@section('buttonBar')
    <div class="btn-group pull-up-10">
        <a href="{{ $listURL }}" class="btn btn-white"><i class="fa fa-th-list inline-icon"></i> Return to List</a>
    </div>
@stop

@section('content')
    <div class="row">
        {{ Form::open(['action' => 'App\Search\SearchController@'.$searchURL, 'method' => 'GET']) }}
            <div class="col-sm-12">
                <div class="input-group">
                    {{ Form::text('q', Input::get('q', ''), ['placeholder' => 'Enter your search terms...', 'autofocus' => 'autofocus', 'class' => 'form-control']) }}
                    <span class="input-group-btn">
                        <button class="btn btn-white" type="submit" style="height: 40px;"><i class="fa fa-search"></i> Search</button>
                    </span>
                </div>
            </div>
        {{ Form::close() }}
    </div>

    <div class="row">
        <div class="col-sm-12">
            Search results for: <strong>{{{ Input::get('q', 'enter a search term to get started...') }}}</strong>
        </div>

        @if (count($searchResults) == 0)
            @include('partials.no_search_results')
        @else
            
            <div class="col-sm-12" style="margin-top:10px;">
                <ul class="media-list">
                    @foreach($searchResults as $result)
                        <li class="media" style="border-bottom:1px solid rgba(6, 6, 6, 0.23);">
                            <div class="media-body">
                                <h4 class="media-heading"><a href="{{ action($itemURL, $result->id) }}"><i class="{{{ $icon }}}"></i> {{{ object_get($result, 'name', 'No name available') }}}</a></h4>

                                @if (object_get($result, 'description', null) == null || strlen($result->description) == 0)
                                    No description available.
                                    @else
                                    {{{ $result->description }}}
                                    @endif
                                <br/><br/>
                                <a href="{{ action($itemURL, $result->id) }}"><i class="fa fa-arrow-right"></i> View this search result</a>
                            </div>
                        </li>
                    @endforeach
                </ul>
            </div>
        @endif

    </div>
@stop