@extends('layouts.master')

@section('pageTitle')
    <i class="fa fa-cubes"></i> Materials
    @if(Input::get('search') == 1 && $foundResults)
        <span>Search Results</span>
    @endif
@stop

@section('buttonBar')
    @if(Input::get('search') == 1 && $foundResults)
        <div class="btn-group pull-up-10">
            <a href="{{ action('App\Materials\MaterialController@index') }}" class="btn btn-white"><i
                        class="fa fa-th-list inline-icon"></i></a>
        </div>
    @endif
    <div class="btn-group pull-up-10">
        <a href="{{ action('App\Search\SearchController@getMaterialsSearchDialog') }}" data-toggle="modal"
           data-target="#modalHousing" class="btn btn-white"><i class="fa fa-search inline-icon"></i> Search
            Materials</a>
        @include('partials.generic_export', ['export_data' => 'res_materials', 'title' => 'materials', 'margin' => '77px'])
        <a href="{{ action('App\Materials\MaterialController@create') }}" class="btn btn-white nd-add"><i
                    class="fa fa-plus inline-icon"></i> New Material</a>
    </div>
@stop

@section('content')

    <div class="row hidden-print">
        <div class="col-sm-12">
            {{ $materials->appends(array_except(Input::query(), Paginator::getPageName()))->links() }}
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
            @include('app.materials.partials.table')
        </div>
    </div>

    <div class="row hidden-print">
        <div class="col-sm-12">
            {{ $materials->appends(array_except(Input::query(), Paginator::getPageName()))->links() }}
        </div>
        @include('layouts.list_management')
    </div>

    @include('layouts.dialogs.remove', array('title' => 'Remove Material', 'action' => 'App\Materials\MaterialController@destroy'))
@stop

@section('additionalScripts')
    <script>var NDC_LM = 'material';</script>
    <script src="{{{ app_url() }}}/assets/js/list_management.min.js{{{ UI::getBuster() }}}" defer></script>
@stop