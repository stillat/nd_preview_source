@extends('app.consumables.update_base')

<?php $updateOKText = 'Update this material'; ?>

@section('formOpen')
	{{ Form::open(array('route' => array('materials.update', $consumable->id), 'method' => 'put', 'class' => 'form-horizontal')) }}
@stop

@section('buttonBar')
	<div class="btn-group n-mt-5">
		{{ HTML::modelNaviateToolbar('materials.edit', $consumable->previousMaterial(), 'materials.index', 'materials.edit', $consumable->nextMaterial()) }}
	</div>
	<div class="btn-group n-mt-5">
		<a href="{{ Redirector::getRoute(route('materials.index')) }}" class="btn btn-danger ndc_form_leave">Cancel</a>
	</div>
@stop