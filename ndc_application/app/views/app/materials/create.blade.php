@extends('app.consumables.create_base')

<?php $consumableOKText = 'Save and add another material'; ?>

@section('pageTitle')
<i class="fa fa-plus"></i> You are adding a new material resource
@stop

@section('buttonBar')
<div class="btn-group n-mt-5">
	<a href="{{ Redirector::getRoute(route('materials.index')) }}" class="btn btn-danger ndc_form_leave">Cancel</a>
</div>
@stop

@section('formOpen')
	{{ Form::open(array('route' => 'materials.store', 'method' => 'post', 'class' => 'form-horizontal')) }}
@stop