<div class="table-responsive">
    <table class="table table-hover table-striped mb30 table-fixed">
        <thead>
            <tr>
                <th class="wd-200">Code</th>
                <th class="wd-200">Name</th>
                <th class="text-right wd-100">Cost</th>
                <th class="text-right wd-100">Unit</th>
                <th>Description</th>
                @if($material->tracking_inventory)
                    <th class="text-right hidden-xs hidden-sm wd-200">Tracking Inventory</th>
                    <th class="text-right wd-200">Inventory Level</th>
                    <th><small>/</small> Status</th>
                @endif
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>
                    <strong>{{{ limit_code($material->code) }}}</strong>
                </td>
                <td>
                   {{{ limit_name($material->name) }}}
                </td>
                <td class="text-right">
                    {{{ nd_number_format($material->cost) }}}
                </td>
                <td class="text-right">
                    {{{ limit_name($unit->name) }}}
                </td>
                <td>
                    <p class="read-more">{{{ limit_description(if_null_then_na($material->description)) }}}</p>
                </td>
                @if($material->tracking_inventory)
                    <td class="text-right hidden-xs hidden-sm">{{ bool_to_icon($material->tracking_inventory) }}</td>
                    <td class="inventory_row text-right">{{{ nd_number_format($material->current_inventory_level) }}}</td>
                    <td class="inventory_row">{{ inventory_status($material->current_inventory_level, $material->desired_inventory_level, $material->critical_inventory_level) }}</td>
                @endif
            </tr>
        </tbody>
    </table>
</div>