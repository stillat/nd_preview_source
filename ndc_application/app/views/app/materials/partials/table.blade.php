<div class="table-responsive">
    <table class="table table-hover table-striped mb30 table-fixed">
        <thead>
        <tr>
            @if(!isset($hideFeatures))
                <th class="hidden-print" style="width:46px;">
                    <div class="ckbox ckbox-primary">
                        <input type="checkbox" id="batchToggleRecords">
                        <label for="batchToggleRecords"></label>
                    </div>
                </th>
            @endif
            <th class="wd-200">Code</th>
            <th class="wd-200">Name</th>
            <th class="text-right wd-100">Cost</th>
            <th class="text-right wd-100">Unit</th>
            <th>Description</th>
            @if($inventorySettings->show_inventory_management_in_consumables)
                <th class="text-right hidden-xs hidden-sm wd-200">Tracking Inventory</th>
                <th class="text-right wd-200">Inventory Level</th>
                <th>
                    <small>/</small>
                    Status
                </th>
            @endif
            <th class="wd-100 hidden-print text-right">
                <a href="{{{ action('App\Search\SortingController@getMaterials') }}}" data-toggle="modal"
                   data-target="#modalHousing"><i class="fa fa-sort"></i>
                    <small>Sorting</small>
                </a>
            </th>
        </tr>
        </thead>
        <tbody>
        @foreach($materials as $material)
            @if ($material->deleted_at == null)
                <tr>
                    @if(!isset($hideFeatures))
                        <td class="hidden-print">
                            <div class="ckbox ckbox-primary">
                                <input type="checkbox" class="rm_multi" id="remCheck{{{ $material->id }}}"
                                       data-record="{{{ $material->id }}}">
                                <label for="remCheck{{{ $material->id }}}"></label>
                            </div>
                        </td>
                    @endif
                    <td>
                        <a href="{{ route('materials.show', array($material->id)) }}"><strong>{{{ limit_code($material->code) }}}</strong></a>
                    </td>
                    <td>
                        <a href="{{ route('materials.show', array($material->id)) }}">{{{ limit_name($material->name) }}}</a>
                    </td>
                    <td class="text-right">
                        <a href="{{ route('materials.show', array($material->id)) }}">{{{ nd_number_format($material->cost) }}}</a>
                    </td>
                    <td class="text-right">
                        <a href="{{ route('materials.show', array($material->id)) }}">{{{ $units[$material->unit_id] }}}</a>
                    </td>
                    <td>
                        <a href="{{ route('materials.show', array($material->id)) }}">{{{ if_null_then_na(limit_description($material->description)) }}}</a>
                    </td>
                    @if($inventorySettings->show_inventory_management_in_consumables)
                        <td class="text-right hidden-xs hidden-sm"><a href="{{ route('materials.show', array($material->id)) }}">{{ bool_to_icon($material->tracking_inventory) }}</a></td>
                        @if($material->tracking_inventory)
                            <td class="inventory_row"><a href="{{ route('materials.show', array($material->id)) }}" class="text-right">{{{ nd_number_format($material->current_inventory_level) }}}</a></td>
                            <td class="inventory_row"><a href="{{ route('materials.show', array($material->id)) }}">{{ inventory_status($material->current_inventory_level, $material->desired_inventory_level, $material->critical_inventory_level) }}</a></td>
                        @else
                            <td colspan="2"></td>
                        @endif
                    @endif
                    <td class="table-action td-ignore">
                        <a href="{{ route('materials.edit', array($material->id)) }}"><i class="fa fa-pencil"></i></a>
                        <a class="cursor-pointer delete-row" data-rcontext="{{{ $material->id }}}"><i class="fa fa-trash-o"></i></a>
                    </td>
                </tr>
            @else
                <tr>
                    @if(!isset($hideFeatures))
                        <td class="hidden-print">
                            <div class="ckbox ckbox-primary">
                                <input type="checkbox" class="rm_multi" id="remCheck{{{ $material->id }}}"
                                       data-record="{{{ $material->id }}}">
                                <label for="remCheck{{{ $material->id }}}"></label>
                            </div>
                        </td>
                    @endif
                    <td>
                        <strong>{{{ limit_code($material->code) }}}</strong>
                    </td>
                    <td>
                        {{{ limit_name($material->name) }}}
                    </td>
                    <td class="text-right">
                        {{{ nd_number_format($material->cost) }}}
                    </td>
                    <td class="text-right">
                        {{{ $units[$material->unit_id] }}}
                    </td>
                    <td>
                        {{{ if_null_then_na(limit_description($material->description)) }}}
                    </td>
                    @if($inventorySettings->show_inventory_management_in_consumables)
                        <td class="text-right hidden-xs hidden-sm">{{ bool_to_icon($material->tracking_inventory) }}</td>
                        @if($material->tracking_inventory)
                            <td class="inventory_row"><a class="text-right">{{{ nd_number_format($material->current_inventory_level) }}}</a></td>
                            <td class="inventory_row">{{ inventory_status($material->current_inventory_level, $material->desired_inventory_level, $material->critical_inventory_level) }}</td>
                        @else
                            <td colspan="2"></td>
                        @endif
                    @endif
                    <td class="table-action td-ignore">
                        <a href="{{ action('App\UnTrashController@getMaterials') }}?d={{ $material->id }}" class="tooltips" data-toggle="tooltip" data-placement="left" title="Restoring will un-delete the material">Restore</a>
                    </td>
                </tr>
            @endif
        @endforeach
        </tbody>
    </table>
</div>