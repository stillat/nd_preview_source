@extends('layouts.master')

@section('pageTitle')
<i class="fa fa-cubes"></i> {{{ limit_code($material->code) }}} - <small>{{{ limit_name($material->name)  }}}</small>
@stop

@section('buttonBar')
<div class="btn-group n-mt-5">
    {{ HTML::modelNaviateToolbar('materials.show', $material->previousMaterial(), 'materials.index', 'materials.show', $material->nextMaterial()) }}
</div>
<div class="btn-group n-mt-5">
    <a href="{{ route('materials.edit', array($material->id)) }}" class="btn btn-white"><i class="fa fa-edit inline-icon"></i> Update material</a>
</div>
@stop

@section('content')
<div class="row">
    <div class="col-md-12">
    @include('app.materials.partials.detail_table')
    </div>
</div>
<ul class="nav nav-tabs">
        @if($material->tracking_inventory)
        <li class="active"><a href="#inventoryManager" data-toggle="tab"><i class="fa ndcf-weight-scale"></i> <strong>Inventory Management</strong></a></li>
        @endif
    </ul>
    <div class="tab-content mb30">
        @if($material->tracking_inventory)
            <div class="tab-pane active" id="inventoryManager">
                @include('app.consumables.inventory_detail', ['consumable' => &$material])
            </div>
        @endif
    </div>
@stop