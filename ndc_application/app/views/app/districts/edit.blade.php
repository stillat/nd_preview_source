@extends('layouts.master')

@section('pageTitle')
<i class="fa fa-pencil"></i> <strong>{{{ limit_code($district->code)  }}}</strong> - {{{ limit_name($district->name) }}}
@stop

@section('buttonBar')
<div class="btn-group n-mt-5">
	{{ HTML::modelNaviateToolbar('districts.edit', $district->previous(), 'districts.index', 'districts.edit', $district->next()) }}
</div>
<div class="btn-group n-mt-5">
	<a href="{{ Redirector::getRoute(route('districts.index')) }}" class="btn btn-danger ndc_form_leave">Cancel</a>
</div>
@stop

@section('content')

<div class="row">
	<div class="col-md-12">
		{{ Form::open(array('route' => array('districts.update', $district->id), 'method' => 'put', 'class' => 'form-horizontal')) }}
		<div class="panel-primary panel-alt">
			<div class="panel-body">
				<div class="form-group">
					<label class="col-sm-2 control-label" for="code">Code:</label>
					<div class="col-sm-2">
						<div class="input-group">
							<span class="input-group-addon"><i class="glyphicon glyphicon-barcode"></i></span>
							<input type="text" id="code" name="code" class="form-control text-right mousetrap ndc_form_home_element {{{ user_form_size() }}}" value="{{{ Input::old('code', $district->code) }}}" autofocus>
						</div>
						{{ Form::errorMsg('code') }}
					</div>
				</div>

				<div class="form-group is-required">
					<label class="col-sm-2 control-label" for="name">Name:</label>
					<div class="col-sm-8">
						<input type="text" id="name" name="name" class="form-control mousetrap {{{ user_form_size() }}}" value="{{{ Input::old('name', $district->name) }}}">
						{{ Form::errorMsg('name') }}
					</div>
				</div>

				<div class="form-group is-required">
					<label class="col-sm-2 control-label" for="description">Description:</label>
					<div class="col-sm-8">
						<textarea id="description" name="description" class="form-control mousetrap ndc_form_trail_element {{{ user_form_size() }}}">{{{ Input::old('description', $district->description) }}}</textarea>
						{{ Form::errorMsg('description') }}
					</div>
				</div>


			</div>
			<div class="panel-footer col-sm-offset-2">
				<div class="btn-group mr5">
					<button type="submit" id="ndc_form_defaultaction" value="return" name="submit" class="btn btn-info">Update this district</button>
				</div>
				<div class="btn-group mr5">
					<button type="reset" id="ndc_form_reset" class="btn btn-default">Reset</button>
				</div>
			</div>
		</div>
		{{ Form::close() }}
	</div>
</div>

@stop