<div class="table-responsive">
    <table class="table table-hover table-striped mb30 table-fixed">
        <thead>
        <tr>
            @if(!isset($hideFeatures))
                <th class="hidden-print" style="width:46px;">
                    <div class="ckbox ckbox-primary">
                        <input type="checkbox" id="batchToggleRecords">
                        <label for="batchToggleRecords"></label>
                    </div>
                </th>
            @endif
            <th class="wd-200">Code</th>
            <th class="wd-200">Name</th>
            <th>Description</th>
            <th class="wd-100 hidden-print text-right">
                <a href="{{{ action('App\Search\SortingController@getDistricts') }}}" data-toggle="modal"
                   data-target="#modalHousing"><i class="fa fa-sort"></i>
                    <small>Sorting</small>
                </a>
            </th>
        </tr>
        </thead>
        <tbody>
        @foreach($districts as $district)
            @if ($district->deleted_at == null)
                <tr>
                    @if(!isset($hideFeatures))
                        <td class="hidden-print">
                            <div class="ckbox ckbox-primary">
                                <input type="checkbox" class="rm_multi" id="remCheck{{{ $district->id }}}"
                                       data-record="{{{ $district->id }}}">
                                <label for="remCheck{{{ $district->id }}}"></label>
                            </div>
                        </td>
                    @endif
                    <td>
                        <a href="{{ route('districts.show', array($district->id)) }}"><strong>{{{ limit_code($district->code) }}}</strong></a>
                    </td>
                    <td>
                        <a href="{{ route('districts.show', array($district->id)) }}">{{{ limit_name($district->name) }}}</a>
                    </td>
                    <td>
                        <a href="{{ route('districts.show', array($district->id)) }}">{{{ if_null_then_na(limit_description($district->description)) }}}</a>
                    </td>
                    <td class="table-action td-ignore">
                        <a href="{{ route('districts.edit', array($district->id)) }}"><i class="fa fa-pencil"></i></a>
                        <a class="cursor-pointer delete-row" data-rcontext="{{{ $district->id }}}"><i class="fa fa-trash-o"></i></a>
                    </td>
                </tr>
            @else
                <tr>
                    @if(!isset($hideFeatures))
                        <td class="hidden-print">
                            <div class="ckbox ckbox-primary">
                                <input type="checkbox" class="rm_multi" id="remCheck{{{ $district->id }}}"
                                       data-record="{{{ $district->id }}}">
                                <label for="remCheck{{{ $district->id }}}"></label>
                            </div>
                        </td>
                    @endif
                    <td>
                        <strong>{{{ limit_code($district->code) }}}</strong>
                    </td>
                    <td>
                        {{{ limit_name($district->name) }}}
                    </td>
                    <td>
                        {{{ if_null_then_na(limit_description($district->description)) }}}
                    </td>
                    <td class="table-action td-ignore">
                        <a href="{{ action('App\UnTrashController@getDistricts') }}?d={{ $district->id }}" class="tooltips" data-toggle="tooltip" data-placement="left" title="Restoring will un-delete the district">Restore</a>
                    </td>
                </tr>
            @endif
        @endforeach
        </tbody>
    </table>
</div>