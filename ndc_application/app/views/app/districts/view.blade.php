@extends('layouts.master')

@section('pageTitle')
<i class="fa fa-map-marker"></i> {{{ limit_code($district->code) }}} - <small>{{{ limit_name($district->name) }}}</small>
@stop

@section('buttonBar')
<div class="btn-group n-mt-5">
    {{ HTML::modelNaviateToolbar('districts.show', $district->previous(), 'districts.index', 'districts.show', $district->next()) }}
</div>
<div class="btn-group n-mt-5">
    <a href="{{ route('districts.edit', array($district->id)) }}" class="btn btn-white"><i class="fa fa-edit inline-icon"></i> Update district</a>
</div>
@stop

@section('content')
<div class="row">
    <div class="col-sm-12">
    @include('app.districts.partials.detail_table')
    </div>
</div>
@stop