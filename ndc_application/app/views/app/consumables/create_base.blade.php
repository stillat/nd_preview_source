@extends('layouts.master')


@section('content')

   <div class="row">

        <div class="col-md-12">
            @yield('formOpen', '<form>')
            <div class="panel-primary panel-alt">
                <div class="panel-body">
                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="code">Code:</label>
                        <div class="col-sm-2">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="glyphicon glyphicon-barcode"></i></span>
                                <input type="text" id="code" name="code" class="form-control text-right mousetrap ndc_form_home_element {{{ user_form_size() }}}" value="{{{ Input::old('code', '') }}}" autofocus>
                            </div>
                            {{ Form::errorMsg('code') }}
                        </div>
                        <label class="col-sm-1 control-label" for="name">Name:</label>
                        <div class="col-sm-5 is-required">
                            <input type="text" id="name" name="name" class="form-control mousetrap {{{ user_form_size() }}}" value="{{{ Input::old('name', '') }}}">
                            {{ Form::errorMsg('name') }}
                        </div>
                    </div>

                    <div class="form-group is-required">
                        <label class="col-sm-2 control-label" for="description">Description:</label>
                        <div class="col-sm-8">
                            <textarea id="description" name="description" class="form-control mousetrap autosize {{{ user_form_size() }}}">{{{ Input::old('description', '') }}}</textarea>
                            {{ Form::errorMsg('description') }}
                        </div>
                    </div>

                    <fieldset class="form-group">
                        <div class="is-required">
                            <label class="col-sm-2 control-label" for="cost">Cost:</label>
                            <div class="col-sm-3">
                                <div class="input-group">
                                    <span class="input-group-addon">$</span>
                                    <input type="number" min="0" step="any" id="cost" name="cost" class="form-control text-right mousetrap {{{ user_form_size() }}}" value="{{{ nd_number_format(Input::old('cost', '0.00')) }}}">
                                </div>
                                {{ Form::errorMsg('cost') }}
                            </div>

                            <label class="col-sm-1 control-label" for="unit_id">Unit:</label>
                            <div class="col-sm-3">
                                {{ Form::select('unit_id', $units, Input::old('unit_id', 0), array('style' => 'width: 100%;', 'id' => 'unit_id', 'class' => 'form-control mousetrap ndc_form_trail_element'.user_form_size())) }}
                                {{ Form::errorMsg('unit_id') }}
                            </div>
                        </div>
                    </fieldset>

                    <fieldset class="form-group">
                        <div class="is-required">
                            <div class="col-sm-offset-2 col-sm-10">
                                <div class="ckbox ckbox-primary">
                                    <input type="checkbox" value="1" id="chkManageInventory" name="manage_inventory">
                                    <label for="chkManageInventory">Manage inventory for this consumable resource</label>
                                </div>
                            </div>
                        </div>
                    </fieldset>

                    <div id="inventoryManagementLevelsContainer" style="display: none;">
                        <div class="form-group">
                            <label class="col-sm-2 control-label" for="initial_inventory_level">Initial Inventory Level:</label>
                            <div class="col-sm-3">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa ndcf-weight-scale"></i></span>
                                    <input type="number" min="0" step="any" id="initial_inventory_level" name="initial_inventory_level" class="form-control text-right mousetrap {{{ user_form_size() }}}" value="{{{ nd_number_format(Input::old('initial_inventory_level', '0.00')) }}}">
                                </div>
                                {{ Form::errorMsg('initial_inventory_level') }}
                            </div>
                            <label class="col-sm-2 control-label" for="desired_inventory_level">Desired Inventory Level:</label>
                            <div class="col-sm-3">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa ndcf-weight-scale"></i></span>
                                    <input type="number" min="0" step="any" id="desired_inventory_level" name="desired_inventory_level" class="form-control text-right mousetrap {{{ user_form_size() }}}" value="{{{ nd_number_format(Input::old('desired_inventory_level', '0.00')) }}}">
                                </div>
                                {{ Form::errorMsg('desired_inventory_level') }}
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label" for="critical_inventory_level">Critical Inventory Level:</label>
                            <div class="col-sm-3">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa ndcf-weight-scale"></i></span>
                                    <input type="number" min="0" step="any" id="critical_inventory_level" name="critical_inventory_level" class="form-control text-right mousetrap {{{ user_form_size() }}}" value="{{{ nd_number_format(Input::old('critical_inventory_level', '0.00')) }}}">
                                </div>
                                {{ Form::errorMsg('critical_inventory_level') }}
                            </div>
                        </div>
                    </div>

                </div>
                <div class="panel-footer col-sm-offset-2">
                    <div class="btn-group mr5">
                        <button type="submit" id="ndc_form_defaultaction" value="addNew" name="submit" class="btn btn-primary"><?php if (isset($consumableOKText)) {
    echo $consumableOKText;
} else {
    echo 'Save and add another';
} ?></button>
                        <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
                            <span class="caret"></span>
                            <span class="sr-only">Toggle Dropdown</span>
                        </button>
                        <ul class="dropdown-menu" role="menu">
                            <li><button type="submit" id="ndc_form_secondaryaction" value="return" name="submit" class="btn btn-white btn-md">Save and return to previous action</button></li>
                        </ul>
                    </div>

                    <div class="btn-group mr5">
                        <button type="reset" id="ndc_form_reset" class="btn btn-default">Reset</button>
                    </div>
                </div>
            </div>
            {{ Form::close() }}
        </div>

    </div>

@stop

@section('additionalScripts')
    <script src="{{ app_url() }}/assets/js/consumables/make.js" defer></script>
@stop