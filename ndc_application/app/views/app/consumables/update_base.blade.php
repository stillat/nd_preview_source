@extends('layouts.master')

@section('pageTitle')
    <i class="fa fa-pencil"></i> {{{ limit_code($consumable->code) }}} -
    <small>{{{ limit_name($consumable->name) }}}</small>
@stop


@section('content')

    <div class="row">
        <div class="col-md-12">
            @yield('formOpen', '<form>')
            <div class="panel-primary panel-alt">
                <div class="panel-body">
                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="code">Code:</label>

                        <div class="col-sm-2">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="glyphicon glyphicon-barcode"></i></span>
                                <input type="text" id="code" name="code"
                                       class="form-control text-right mousetrap ndc_form_home_element {{{ user_form_size() }}}"
                                       value="{{{ Input::old('code', $consumable->code) }}}" autofocus>
                            </div>
                            {{ Form::errorMsg('code') }}
                        </div>
                        <label class="col-sm-1 control-label" for="name">Name:</label>

                        <div class="col-sm-5 is-required">
                            <input type="text" id="name" name="name"
                                   class="form-control mousetrap {{{ user_form_size() }}}"
                                   value="{{{ Input::old('name', $consumable->name) }}}">
                            {{ Form::errorMsg('name') }}
                        </div>
                    </div>

                    <div class="form-group is-required">
                        <label class="col-sm-2 control-label" for="description">Description:</label>

                        <div class="col-sm-8">
                            <textarea id="description" name="description"
                                      class="form-control mousetrap autosize {{{ user_form_size() }}}">{{{ Input::old('description', $consumable->description) }}}</textarea>
                            {{ Form::errorMsg('description') }}
                        </div>
                    </div>

                    <fieldset class="form-group">
                        <div class="is-required">
                            <label class="col-sm-2 control-label" for="cost">Cost:</label>

                            <div class="col-sm-3">
                                <div class="input-group">
                                    <span class="input-group-addon">$</span>
                                    <input type="number" min="0" step="any" id="cost" name="cost"
                                           class="form-control text-right mousetrap {{{ user_form_size() }}}"
                                           value="{{{ nd_number_format(Input::old('cost', nd_number_format($consumable->cost))) }}}">
                                </div>
                                {{ Form::errorMsg('cost') }}
                            </div>

                            <label class="col-sm-1 control-label" for="unit_id">Unit:</label>

                            <div class="col-sm-3">
                                {{ Form::select('unit_id', $units, Input::old('unit_id', $consumable->unit_id), array('style' => 'width: 100%;', 'id' => 'unit_id','class' => 'form-control mousetrap ndc_form_trail_element'.user_form_size())) }}
                                {{ Form::errorMsg('unit_id') }}
                            </div>
                        </div>
                    </fieldset>

                    <fieldset class="form-group">
                        <div class="is-required">
                            <div class="col-sm-offset-2 col-sm-10">
                                <div class="ckbox ckbox-primary">
                                    {{ Form::checkbox('manage_inventory', '0', $consumable->tracking_inventory, ['id' => 'chkManageInventory']) }}
                                    <label for="chkManageInventory">Manage inventory for this consumable
                                        resource</label>
                                </div>
                            </div>
                        </div>
                    </fieldset>

                    <div id="inventoryManagementLevelsContainer" <?php if (!$consumable->tracking_inventory) {
    echo 'style="display: none;"';
} ?>>
                        <div class="form-group">
                            <label class="col-sm-2 control-label" for="desired_inventory_level">Desired Inventory Level:</label>
                            <div class="col-sm-3">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa ndcf-weight-scale"></i></span>
                                    <input type="number" min="0" step="any" id="desired_inventory_level" name="desired_inventory_level" class="form-control text-right mousetrap {{{ user_form_size() }}}" value="{{{ nd_number_format(Input::old('desired_inventory_level', $consumable->desired_inventory_level)) }}}">
                                </div>
                                {{ Form::errorMsg('desired_inventory_level') }}
                            </div>
                            <label class="col-sm-2 control-label" for="critical_inventory_level">Critical Inventory Level:</label>
                            <div class="col-sm-3">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa ndcf-weight-scale"></i></span>
                                    <input type="number" min="0" step="any" id="critical_inventory_level" name="critical_inventory_level" class="form-control text-right mousetrap {{{ user_form_size() }}}" value="{{{ nd_number_format(Input::old('critical_inventory_level', $consumable->critical_inventory_level)) }}}">
                                </div>
                                {{ Form::errorMsg('critical_inventory_level') }}
                            </div>
                        </div>
                    </div>


                </div>
                <div class="panel-footer col-sm-offset-2">
                    <div class="btn-group mr5">
                        <button type="submit" id="ndc_form_defaultaction" value="return" name="submit"
                                class="btn btn-primary"><?php if (isset($updateOKText)) {
    echo $updateOKText;
} else {
                                echo 'Update this record';
                            } ?></button>
                    </div>

                    <div class="btn-group mr5">
                        <button type="reset" id="ndc_form_reset" class="btn btn-default">Reset</button>
                    </div>
                </div>
            </div>
            {{ Form::close() }}
        </div>

    </div>

@stop

@section('additionalScripts')
    <script src="{{ app_url() }}/assets/js/consumables/make.js" defer></script>
@stop