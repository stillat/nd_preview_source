<div class="row mb20">
    <div class="col-sm-8">
        @if($consumable->current_inventory_level >= $consumable->desired_inventory_level)
            <strong>{{{ $consumable->name }}}</strong> is above the <span class="label label-success">desirable</span> level.
            <br><strong>Desirable Level</strong>: {{{ nd_number_format($consumable->desired_inventory_level).' '.Str::plural($unit->name) }}}
        @elseif($consumable->current_inventory_level < $consumable->desired_inventory_level && $consumable->current_inventory_level > $consumable->critical_inventory_level)
            <strong>{{{ $consumable->name }}}</strong> is <span class="label label-warning" style="color: black;">below the desirable</span> inventory level.
            <br><strong>Desirable Level</strong>: {{{ nd_number_format($consumable->desired_inventory_level).' '.Str::plural($unit->name) }}}
        @else
            <strong>{{{ $consumable->name }}}</strong> has is at or <span class="label label-danger">below the critical</span> level.
            <br><strong>Critical Level</strong>: {{{ nd_number_format($consumable->critical_inventory_level).' '.Str::plural($unit->name) }}}
        @endif
            <br><strong>Current Level</strong>: {{{ nd_number_format($consumable->current_inventory_level).' '.Str::plural($unit->name) }}}
    </div>
    <div class="col-sm-4 text-right">
        <div class="btn-group">
            <a href="#" data-toggle="dropdown" class="dropdown-toggle btn btn-white">
                <i class="fa fa-download inline-icon"></i> Export <span class="caret"></span>
            </a>
            <ul role="menu" class="dropdown-menu">
                <li><a href="{{ export_csv('cons_adjustments', 'list', ['consumable' => $consumable->id]).$inventoryRestrictionString }}">Export this page</a></li>
                <li><a href="{{ export_csv('cons_adjustments', 'list', ['consumable' => $consumable->id]) }}">Export all inventory adjustments</a></li>
            </ul>
            <a class="btn btn-white" href="{{ action('App\Consumables\ConsumableUtilitiesController@getAdjustInventoryLevel', [$consumable->id]) }}" data-toggle="modal" data-target="#modalHousing">Manually Adjust Inventory Level</a>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-sm-12">
        {{ $inventoryAdjustments->links() }}
    </div>
</div>
<div class="row">
    @if(count($inventoryAdjustments) == 0)
        <p style="margin-top: 20px;">There are no inventory adjustments to display right now.</p>
    @else
        <div class="table-responsive">
            <table class="table table-hover table-striped">
                <thead>
                <tr>
                    <th class="wd-200">Date</th>
                    <th class="hidden-print">User</th>
                    <th class="hidden-print">Details</th>
                    <th class="text-right">Amount</th>
                </tr>
                </thead>
                <tbody>
                {{ $adjustmentStream }}
                </tbody>
            </table>
        </div>
    @endif
</div>
<div class="row">
    <div class="col-sm-12">
        {{ $inventoryAdjustments->links() }}
    </div>
</div>