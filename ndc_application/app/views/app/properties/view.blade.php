@extends('layouts.master')

@section('pageTitle')
    <i class="fa fa-chevron-right"></i> {{{ limit_code($property->code) }}} -
    <small>{{{ limit_name($property->name) }}}</small>
@stop

@section('buttonBar')
    <div class="btn-group n-mt-5">
        {{ HTML::modelNaviateToolbar('properties.show', $property->previous(), 'properties.index', 'properties.show', $property->next()) }}
    </div>
    <div class="btn-group n-mt-5">
        <a href="{{ route('properties.edit', array($property->id)) }}" class="btn btn-white"><i
                    class="fa fa-edit inline-icon"></i> Update property</a>
    </div>
@stop

@section('content')

    <div class="row">
        <div class="col-sm-12">
            <dl class="dl-horizontal">
                <dt>Property</dt>
                <dd><strong>{{{ limit_code($property->name) }}}</strong> - {{{ limit_name($property->name) }}}</dd>
                <dt>Property Type</dt>
                <dd><strong>{{{ limit_code($property->type->code) }}}</strong>
                    - {{{ limit_name($property->type->name) }}}
                </dd>
            </dl>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12 col-sm-12">
            @include('app.properties.partials.detail_table')
        </div>
    </div>

    <div class="row <?php if (count($childProperties) == 0) {
        echo 'hidden-print';
    } ?>">
        <div class="col-md-12 col-sm-12">
            <ul class="nav nav-tabs nav-dark">
                <li class="active"><a data-toggle="tab"><strong><i class="fa fa-home"></i> Nested
                            Properties</strong></a></li>
                @if($property->type->id != 2)
                    @if($property->exclusively_owned == 0)
                        <li class="dropdown">
                            <a href="#" data-toggle="dropdown" class="dropdown-toggle tooltips" data-toggle="tooltip"
                               data-html="true" data-placement="right"
                               title="<i class='fa fa-level-down'></i> Property Nesting Options">
                                <i class="fa fa-level-down"></i> <span class="caret"></span>
                            </a>
                            <ul role="menu" class="dropdown-menu pull-right" style="margin-right: -146px">
                                <li><strong><i class="fa fa-level-down"></i> Property Nesting Options</strong></li>
                                <li class="divider"></li>
                                <li><a href="{{ route('properties.nested.create', array($property->id)) }}">Nest an
                                        existing property</a></li>
                                <li><a href="{{ route('properties.children.create', array($property->id)) }}">Create a
                                        new nested property</a></li>
                            </ul>
                        </li>
                    @endif
                @endif
            </ul>
            <div class="tab-content mb30">
                <div class="tab-pane active">
                    @if($property->type->id !== 2)
                        @include('app.properties.partials.child_properties_table')
                    @else
                        @include('app.properties.partials.equipment_notice_child')
                    @endif
                </div>
            </div>
        </div>
    </div>
@stop
