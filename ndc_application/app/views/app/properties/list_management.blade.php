<div class="row">
    <div class="col-sm-3">
        <select class="form-control" name="bulk_action" id="bulkAction">
            <option value="remove">Remove selected items</option>
            <option value="record_merge" id="mergeRemoveListManagement">Merge and remove the selected items</option>
            <option value="convert_road_to_generic" id="convertRoadToGenericListManagement" disabled="disabled">Convert selected roads to generic properties</option>
        </select>
    </div>
    <div class="col-sm-2">
        <a class="btn btn-block btn-white" id="applyBulkAction">Apply batch action</a>
    </div>
</div>
@include('layouts.list_merge')
@include('app.properties.dialogs.convert_road_to_generic')