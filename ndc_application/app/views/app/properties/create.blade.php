@extends('layouts.master')

@section('pageTitle')
    @if ($mode == 'normal')
        <i class="fa fa-plus"></i> You are adding a new <span id="propertyTypeHelper">property</span>
    @elseif ($mode == 'child')
        <i class="fa fa-plus"></i> You are adding a nested property to
        <strong>{{{ $parentProperty->code }}}</strong> - {{{ $parentProperty->name }}}
    @endif
@stop

@section('buttonBar')
    <div class="btn-group n-mt-5">
        <a href="{{ Redirector::getRoute(route('properties.index')) }}" class="btn btn-danger ndc_form_leave">Cancel</a>
    </div>
@stop


@section('content')

    @if($mode == 'child')
    <div class="row">
        <div class="col-sm-12">
            <div class="alert alert-info">
                <strong><i class="fa fa-help"></i> Note</strong>: You are adding a new nested property to <strong>{{{ $parentProperty->code }}}</strong> - {{{ $parentProperty->name }}}. This property will only be available as a nested property, and will not appear in the main properties list and cannot be nested under other properties. To create a property that can be nested by multiple properties
                <a href="{{ action('App\Properties\PropertiesController@create') }}">click here instead</a>.
            </div>
        </div>
    </div>
    @endif

    <div class="row">
        <div class="col-md-12">
            @if ($mode == 'normal')
                {{ Form::open(array('route' => 'properties.store', 'method' => 'post', 'class' => 'form-horizontal')) }}
            @elseif ($mode == 'child')
                {{ Form::open(array('action' => array('App\Properties\ChildPropertiesController@store', $parentProperty->id), 'method' => 'post', 'class' => 'form-horizontal')) }}
            @endif
            <div class="panel-primary panel-alt">
                <div class="panel-body">

                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="code">Code:</label>

                        <div class="col-sm-4">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="glyphicon glyphicon-barcode"></i></span>
                                <input type="text" id="code" name="code"
                                       class="form-control text-right mousetrap ndc_form_home_element {{{ user_form_size() }}}"
                                       value="{{{ Input::old('code', '') }}}" autofocus>
                            </div>
                            {{ Form::errorMsg('code') }}
                        </div>
                    </div>

                    <div class="form-group is-required">
                        <label class="col-sm-2 control-label" for="name">Name:</label>

                        <div class="col-sm-8">
                            <input type="text" id="name" name="name"
                                   class="form-control mousetrap {{{ user_form_size() }}}"
                                   value="{{{ Input::old('name', '') }}}">
                            {{ Form::errorMsg('name') }}
                        </div>
                    </div>

                    <div class="form-group is-required">
                        <label class="col-sm-2 control-label" for="description">Description:</label>

                        <div class="col-sm-8">
                            <input type="text" id="description" name="description"
                                   class="form-control mousetrap {{{ user_form_size() }}}"
                                   value="{{{ Input::old('description', '') }}}">
                            {{ Form::errorMsg('description') }}
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="category_id">Property Category</label>

                        <div class="col-sm-5">
                            {{ Form::select('category_id', $categories, Input::old('category_id', 0), array('class' => 'form-control mousetrap', 'id' => 'category_id','data-placeholder' => 'Choose a category')) }}
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="property_type">Property Type</label>

                        <div class="col-sm-5">
                            {{ Form::select('property_type', $types->lists('code', 'id'), Input::old('property_type', 0), array('class' => 'form-control mousetrap ndc_form_trail_element', 'id' => 'property_type','data-placeholder' => 'Choose a property type')) }}
                        </div>
                    </div>

                    @foreach($propertyAdapters as $adapter)
                        @if($adapter->getCreatePropertyView() !== null)
                            @include($adapter->getCreatePropertyView(), array('adapter' => $adapter))
                        @endif
                    @endforeach

                </div>
                <div class="panel-footer col-sm-offset-2">
                    <div class="btn-group mr5">
                        <button type="submit" id="ndc_form_defaultaction" value="addNew" name="submit"
                                class="btn btn-primary">Save and add another property
                        </button>
                        <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
                            <span class="caret"></span>
                            <span class="sr-only">Toggle Dropdown</span>
                        </button>
                        <ul class="dropdown-menu" role="menu">
                            <li>
                                <button type="submit" id="ndc_form_secondaryaction" value="return" name="submit"
                                        class="btn btn-white btn-md">Save and return to previous action
                                </button>
                            </li>
                        </ul>
                    </div>

                    <div class="btn-group mr5">
                        <button type="reset" id="ndc_form_reset" class="btn btn-default">Reset</button>
                    </div>
                </div>
            </div>
            {{ Form::close() }}
        </div>

    </div>
@stop

@section('additionalScripts')
    <script>
        var propertyTypes = {{ $types->toJson() }};
        var inititalPropertyType = {{{ Input::get('type', -1) }}};
    </script>
    <script src="{{{ app_url() }}}/assets/js/properties.js" defer></script>
@stop