@extends('layouts.master')

@section('pageTitle')
    <i class="fa fa-home"></i> Properties
    @if(Input::get('search') == 1 && $foundResults)
        <small>Search Results</small>
    @else
        @if (isset($propertyTypes[$currentType]))
            <small>{{{ $propertyTypes[$currentType] }}}</small>
        @else
            <small>All Categories</small>
        @endif
    @endif
@stop

@section('buttonBar')
    @if ((Input::get('search') != 1 && !$foundResults) && count($propertyTypes) > 2)
        <label for="categorychooser" class="control-label hidden-sm hidden-print"
               style="display: inline; position: absolute;left: -131px;top: 3px;">Filter category:</label>
        <div class="btn-group n-mt-5">
            <div style="width: 200px; display: inline-block; margin-left: 5px;">
                {{ Form::select('property_type', $categories, Input::get('sort', -1), array('class' => 'form-control nd-filter chosen', 'id' => 'typechooser','data-placeholder' => 'Choose a property type')) }}
            </div>
        </div>
    @endif
    <div class="btn-group" style="margin-top: -14px;">
        @if (Input::get('search') == 1 && $foundResults)
            <a href="{{ action('App\Properties\PropertiesController@index') }}" class="btn btn-white"><i
                        class="fa fa-th-list inline-icon"></i></a>
        @endif
        <a href="{{ action('App\Search\SearchController@getPropertySearchDialog') }}" data-toggle="modal"
           data-target="#modalHousing" class="btn btn-white"><i class="fa fa-search inline-icon"></i> Search Properties</a>
        @include('partials.generic_export', ['export_data' => 'gen_properties', 'title' => 'properties', 'margin' => '85px'])
    </div>
    <div class="btn-group" style="margin-top: -14px;">
        <a href="{{ action('App\Properties\PropertiesController@create', ['type' => Input::get('sort', -1)]) }}"
           class="btn btn-white nd-add"><i class="fa fa-plus inline-icon"></i> New Property</a>

        <button type="button" class="btn btn-white dropdown-toggle" data-toggle="dropdown">
            <span class="caret"></span>
            <span class="sr-only">Toggle Dropdown</span>
        </button>
        <ul class="dropdown-menu" role="menu" style="margin-left: -50px;">
            <li><a href="{{ action('App\Properties\PropertyCategoriesController@create') }}">New Property Category</a>
            </li>
            <li><a href="{{ action('App\Properties\PropertyCategoriesController@index') }}">Manage Property
                    Categories</a></li>
            @if(Auth::user()->admin)
                <li class="divider"></li>
                <li><a href="{{ action('App\Properties\PropertyTypesController@create') }}">New Property Type</a></li>
                <li><a href="{{ action('App\Properties\PropertyTypesController@index') }}">Manage Property Types</a>
                </li>
            @endif
        </ul>
    </div>
@stop

@section('content')

    <div class="row hidden-print">
        <div class="col-md-12">
            {{ $properties->appends(Input::all())->links() }}
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            @include('app.properties.partials.table')
        </div>
    </div>

    <div class="row hidden-print">
        <div class="col-md-12">
            {{ $properties->appends(Input::all())->links() }}
        </div>
        @include('app.properties.list_management')
    </div>

    @include('layouts.dialogs.remove', array('title' => 'Remove Property', 'action' => 'App\Properties\PropertiesController@destroy'))
@stop

@section('additionalScripts')
    <script>
        var NDC_LM = 'property';
    </script>
    <script src="{{{ app_url() }}}/assets/js/list_management.min.js{{{ UI::getBuster() }}}" defer></script>
@stop