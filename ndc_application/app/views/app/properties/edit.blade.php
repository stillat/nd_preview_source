@extends('layouts.master')

@section('pageTitle')
<i class="fa fa-pencil"></i> <strong>{{{ limit_code($property->code) }}}</strong> - {{{ limit_name($property->name) }}}
@stop

@section('buttonBar')
<div class="btn-group n-mt-5">
    <a href="{{ Redirector::getRoute(route('properties.index')) }}" class="btn btn-danger ndc_form_leave">Cancel</a>
</div>
@stop

@section('content')

<div class="row">

    <div class="col-md-12">
        {{ Form::open(array('route' => array('properties.update', $property->id), 'method' => 'put', 'class' => 'form-horizontal')) }}
        <div class="panel-primary panel-alt">
            <div class="panel-body">
                <div class="form-group">
                    <label class="col-sm-2 control-label" for="code">Code:</label>
                    <div class="col-sm-4">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="glyphicon glyphicon-barcode"></i></span>
                            <input type="text" id="code" name="code" class="form-control text-right mousetrap ndc_form_home_element {{{ user_form_size() }}}" value="{{{ Input::old('code', $property->code) }}}" autofocus>
                        </div>
                        {{ Form::errorMsg('code') }}
                    </div>
                </div>

                <div class="form-group is-required">
                    <label class="col-sm-2 control-label" for="name">Name:</label>
                    <div class="col-sm-8">
                        <input type="text" id="name" name="name" class="form-control mousetrap {{{ user_form_size() }}}" value="{{{ Input::old('name', $property->name) }}}">
                        {{ Form::errorMsg('name') }}
                    </div>
                </div>

                <div class="form-group is-required">
                    <label class="col-sm-2 control-label" for="description">Description:</label>
                    <div class="col-sm-8">
                        <input type="text" id="description" name="description" class="form-control mousetrap {{{ user_form_size() }}}" value="{{{ Input::old('description', $property->description) }}}">
                        {{ Form::errorMsg('description') }}
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-2 control-label" for="category_id">Property Category</label>

                    <div class="col-sm-5">
                        {{ Form::select('category_id', $categories, Input::old('category_id', $property->category_id), array('class' => 'form-control mousetrap', 'id' => 'category_id','data-placeholder' => 'Choose a category')) }}
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-2 control-label" for="property_type">Property Type</label>
                    <div class="col-sm-5">
                        {{ Form::select('property_type', $types, Input::old('property_type', $property->property_type), array('class' => 'form-control mousetrap ndc_form_trail_element chosen-select', 'id' => 'property_type','data-placeholder' => 'Choose a property type', 'disabled' => 'disabled')) }}
                    </div>
                </div>

                @foreach($propertyAdapters as $adapter)
                @if(in_array($adapter->getAdapterNumericIdentifier(), $property->type->adapters->lists('id')))
                @include($adapter->getUpdatePropertyView(), array('adapter' => $adapter))
                @endif
                @endforeach

            </div>
            <div class="panel-footer col-sm-offset-2">
                <div class="btn-group mr5">
                    <button type="submit" id="ndc_form_defaultaction" value="update" name="submit" class="btn btn-primary">Update property</button>
                </div>

                <div class="btn-group mr5">
                    <button type="reset" id="ndc_form_reset" class="btn btn-default">Reset</button>
                </div>
            </div>
        </div>
        {{ Form::close() }}
    </div>

</div>
@stop

@section('additionalScripts')
<script>var propertyMode = 'update';</script>
<script src="{{{ app_url() }}}/assets/js/properties.js" defer></script>
@stop