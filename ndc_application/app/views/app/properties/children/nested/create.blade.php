@extends('layouts.master')

@section('pageTitle')
<i class="fa fa-plus"></i> You nesting existing properties to {{{ limit_code($parentProperty->code) }}} - <small>{{{ limit_name($parentProperty->name) }}}</small>
@stop

@section('buttonBar')
<div class="btn-group pull-up-10">
	<a href="{{ Redirector::getRoute(route('properties.show', $parentProperty->id)) }}" class="btn btn-danger ndc_form_leave">Cancel</a>
</div>
@stop

@section('content')

    <div class="row">
        <div class="col-sm-12">
            <div class="alert alert-info">
                <strong><i class="fa fa-lightbulb-o"></i> Note</strong>: It is recommended that you only nest properties where it makes sense to do so. Nesting the system property <strong><i class="fa fa-home"></i> (NA) Not Available</strong> and the system property <strong><i class="fa fa-truck"></i> (NA - EQUIPMENT) Not Available - Equipment</strong> are not allowed; and the system will not nest this property type if selected.
            </div>
        </div>
    </div>

<div class="row">

	{{ Form::open(array('route' => array('properties.nested.store', $parentProperty->id), 'method' => 'post', 'class' => 'form-horizontal')) }}
	<div class="col-sm-12">
		<div class="panel-primary panel-alt">
			<div class="panel-body">
				<div class="table-responsive">
					<table class="table table-hover table-striped table-fixed">
						<thead>
							<tr>
								<th><span class="mr10">Existing Property</span></th>
								<th class="wd-100"><a data-bind="click: addProperty" class="cursor-pointer"><i class="fa fa-plus"></i> Property</a></th>
							</tr>
						</thead>
						<tbody id="properties_holder"></tbody>
					</table>
				</div>
			</div>
			<div class="panel-footer">
				<div class="btn-group mr5">
					<input type="hidden" id="parentPropertyID" value="{{{ $parentProperty->id }}}">
					<button type="submit" id="ndc_form_defaultaction" value="return" name="submit" class="btn btn-primary">Nest These Properties</button>
				</div>

				<div class="btn-group mr5">
					<a id="ndc_form_reset" class="btn btn-default" data-bind="click: removeAllProperties">Reset</a>
				</div>
			</div>
		</div>
	</div>
	{{ Form::close() }}
</div>

<script type="text/html" id="property_tmpl">
	<%* createdPropertiesCount++ %>
	<tr data-property="<%:~getPropertyCount()%>">
		<td><input data-select="property" data-property="<%:~getPropertyCount()%>" id="propertiesSelector_<%:~getPropertyCount()%>" name="nestedProperty[]" type="text" class="form-control"></td>
		<td><a data-property="<%:~getPropertyCount()%>" tabindex="-1" href="#" class="btn btn-link" data-bind="click: removeProperty">Remove</a></td>
	</tr>
</script>

@stop

@section('additionalScripts')
<script src="{{{ app_url() }}}/assets/js/jsrender.min.js" defer></script>
<script src="{{{ app_url() }}}/assets/js/nested_properties.js" defer></script>

@stop