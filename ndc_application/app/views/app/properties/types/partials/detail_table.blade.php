<div class="table-responsive mb30">
    <table class="table table-hover table-striped mb30 table-fixed">
        <thead>
            <tr>
                <th class="wd-200">Code</th>
                <th class="wd-200">Name</th>
                <th>Description</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>
                    <strong>{{{ limit_code($propertyType->code) }}}</strong>
                </td>
                <td>
                    {{{ limit_name($propertyType->name) }}}
                </td>
                <td>
                    {{{ limit_description(if_null_then_na($propertyType->description)) }}}
                </td>
            </tr>
        </tbody>
    </table>
</div>

<p class="text-info"><i class="fa fa-warning"></i> <strong>Note</strong>: Data adapters cannot be modified once they have been set. They are listed here as a reference only.</p>

<div class="table-responsive">
    <table class="table table-hover table-striped table-fixed">
        <thead>
            <tr>
                <th>Data Adapter</th>
                <th>Description</th>
            </tr>
        </thead>
        <tbody>
        @if(count($adapters) > 0)
        @foreach($adapters as $adapter)
        <tr>
            <td>{{{ $propertyAdapters[$adapter->adapter]->getName() }}}</td>
            <td>{{{ $propertyAdapters[$adapter->adapter]->getDescription() }}}</td>
        </tr>
        @endforeach
        @else
        <tr>
            <td colspan="2">There are no data adapters available for this property type.</td>
        </tr>
        @endif
        </tbody>
    </table>
</div>