@extends('layouts.master')

@section('pageTitle')
<i class="fa fa-map-marker"></i> {{{ limit_code($propertyType->code) }}} - <small>{{{ limit_name($propertyType->name) }}}</small>
@stop

@section('buttonBar')
<div class="btn-group pull-up-10">
    {{ HTML::modelNaviateToolbar('property-types.show', $propertyType->previous(), 'property-types.index', 'property-types.show', $propertyType->next()) }}
</div>
<div class="btn-group pull-up-10">
    <a href="{{ route('property-types.edit', array($propertyType->id)) }}" class="btn btn-white"><i class="fa fa-edit inline-icon"></i> Update property type</a>
</div>
@stop

@section('content')
<div class="row">
    <div class="col-md-12">
    @include('app.properties.types.partials.detail_table')
    </div>
</div>
@stop