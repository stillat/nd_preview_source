@extends('layouts.master')

@section('pageTitle')
<i class="fa fa-map-marker"></i> Property Types
@stop

@section('buttonBar')
<div class="btn-group n-mt-5">
	<a href="{{ action('App\Properties\PropertyTypesController@create') }}" class="btn btn-white nd-add"><i class="fa fa-plus inline-icon"></i> New Property Type</a>
</div>
@stop

@section('content')

<div class="row">

	<div class="col-sm-12">
		{{ $propertyTypes->links() }}
	</div>

	<div class="col-sm-12">
		@include('app.properties.types.partials.table')
	</div>

	<div class="col-sm-12">
		{{ $propertyTypes->links() }}
	</div>

</div>

@include('layouts.dialogs.remove', array('title' => 'Remove Property Type', 'action' => 'App\Properties\PropertyTypesController@destroy'))
@stop