@if($property->exclusively_owned == 1)
    <h4 class="dark">Nested properties are not available for this property.</h4>
    <p>Because this property is exclusively owned by another property type, additional nested properties cannot be
        added.</p>
@else
    @if(count($childProperties) == 0)
        <h4 class="dark">There are no nested properties available for this property.</h4>
        <p>Nested properties can help to organize your data even further. <a
                    href="{{ route('properties.nested.create', array($property->id)) }}">Add a nested property now</a>
        </p>
    @else
        <div class="table-responsive">
            <table class="table table-hover table-striped mb30 table-fixed">
                <thead>
                <tr class="long_table_heading">
                    <th class="wd-200">Code</th>
                    <th class="wd-200">Name</th>
                    <th class="wd-200">Property Type</th>
                    <th>Description</th>
                    <th class="wd-100"></th>
                </tr>
                </thead>
                <tbody>
                @foreach($childProperties as $childProperty)
                    <tr>
                        <td>
                            <a href="{{ route('properties.show', array($childProperty->id)) }}"><strong>{{{ limit_code($childProperty->code) }}}</strong></a>
                        </td>
                        <td>
                            <a href="{{ route('properties.show', array($childProperty->id)) }}">{{{ limit_name($childProperty->name) }}}</a>
                        </td>
                        <td>
                            <a href="{{ route('properties.show', array($childProperty->id)) }}"><strong>{{{ limit_code($childProperty->type->code) }}}</strong>
                                - {{{ limit_name($childProperty->type->name) }}}</a>
                        </td>
                        <td>
                            <a href="{{ route('properties.show', array($childProperty->id)) }}">{{{ limit_description(if_null_then_na($childProperty->description)) }}}</a>
                        </td>
                        <td class="table-action td-ignore">
                            <a href="{{ route('properties.edit', array($childProperty->id)) }}" class="tooltips"
                               data-toggle="tooltip" data-html="true" data-placement="left"
                               title="<i class='fa fa-pencil'></i> Update this property"><i
                                        class="fa fa-pencil"></i></a>
                            <a class="cursor-pointer delete-row tooltips" data-rcontext="{{{ $childProperty->id }}}"
                               data-toggle="tooltip" data-html="true" data-placement="left"
                               title="<i class='fa fa-trash-o'></i> Remove this property permanently"><i
                                        class="fa fa-trash-o"></i></a>
                            @if($childProperty->exclusively_owned)
                                <a class="cursor-pointer tooltips" data-toggle="modal" data-target="#modalHousing" href="{{ action('App\UtilityController@getUnNestExclusiveProperty', $childProperty->id) }}" data-toggle="tooltip" data-html="true" data-placement="left" title="<i class='fa fa-level-up'></i> Un-nest this property"><i class="fa fa-level-up"></i>&nbsp;</a>
                            @else
                                <a class="cursor-pointer tooltips" data-toggle="modal" data-target="#modalHousing" href="{{ action('App\UtilityController@getUnNestGlobalProperty', [$property->id, $childProperty->id]) }}" data-toggle="tooltip" data-html="true" data-placement="left" title="<i class='fa fa-level-up'></i> Un-nest this property"><i class="fa fa-level-up"></i>&nbsp;</a>
                            @endif
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
        @include('layouts.dialogs.remove', array('title' => 'Remove Nested Property', 'agreeText' => 'Remove this property permanently', 'content' => 'Are you sure you want to remove this property? This will remove the property permanently. If you just want to remove the property from this list, consider "un-nesting" this property instead by clicking/tapping the <i class="fa fa-level-up"></i> button next to the property.', 'action' => 'App\UtilityController@deleteDestroyNestedProperty', 'extraFields' => ['parentProperty' => $property->id]))
    @endif
@endif