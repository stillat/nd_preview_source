<div class="table-responsive">
    <table class="table table-hover table-striped mb30 table-fixed">
        <thead>
        <tr class="long_table_heading">
            @if(!isset($hideFeatures))
                <th class="hidden-print" style="width:46px;">
                    <div class="ckbox ckbox-primary">
                        <input type="checkbox" id="batchToggleRecords">
                        <label for="batchToggleRecords"></label>
                    </div>
                </th>
            @endif
            <th class="wd-200">Code</th>
            <th class="wd-200">Name</th>
            <th class="wd-200">Category</th>
            @if(Input::get('sort', -1) < 0)
                <th class="wd-200">Property Type</th>
            @endif
            <th>Description</th>
            <th class="wd-100 hidden-print text-right">
                <a href="{{{ action('App\Search\SortingController@getProperties') }}}" data-toggle="modal"
                   data-target="#modalHousing"><i class="fa fa-sort"></i>
                    <small>Sorting</small>
                </a>
            </th>
        </tr>
        </thead>
        <tbody>
        @foreach($properties as $property)
            @if ($property->deleted_at == null)
                <tr>
                    @if(!isset($hideFeatures))
                        <td class="hidden-print">
                            <div class="ckbox ckbox-primary">
                                <input type="checkbox" class="rm_multi" id="remCheck{{{ $property->id }}}"
                                       data-record="{{{ $property->id }}}" data-ptype="{{{ $property->property_type }}}">
                                <label for="remCheck{{{ $property->id }}}"></label>
                            </div>
                        </td>
                    @endif
                    <td>
                        <a href="{{ route('properties.show', array($property->id)) }}"><strong>{{{ limit_code($property->code) }}}</strong></a>
                    </td>
                    <td>
                        <a href="{{ route('properties.show', array($property->id)) }}">{{{ limit_name($property->name) }}}</a>
                    </td>
                    <td><a href="{{ url('properties?sort='.$property->category->id) }}">{{{ limit_name($property->category->category_name) }}}</a></td>
                    @if(Input::get('sort', -1) < 0)
                        <td>
                            <a href="{{ route('properties.show', array($property->id)) }}"><strong>{{{ $property->type->code }}}</strong></a>
                        </td>
                    @endif
                    <td>
                        <a href="{{ route('properties.show', array($property->id)) }}">{{{ if_null_then_na(limit_description($property->description)) }}}</a>
                    </td>
                    <td class="table-action td-ignore">
                        <a href="{{ route('properties.edit', array($property->id)) }}"><i class="fa fa-pencil"></i></a>
                        <a class="cursor-pointer delete-row" data-rcontext="{{{ $property->id }}}"><i class="fa fa-trash-o"></i></a>
                    </td>
                </tr>
            @else
                <tr>
                    @if(!isset($hideFeatures))
                        <td class="hidden-print">
                            <div class="ckbox ckbox-primary">
                                <input type="checkbox" class="rm_multi" id="remCheck{{{ $property->id }}}"
                                       data-record="{{{ $property->id }}}" data-ptype="{{{ $property->property_type }}}">
                                <label for="remCheck{{{ $property->id }}}"></label>
                            </div>
                        </td>
                    @endif
                    <td>
                        <strong>{{{ limit_code($property->code) }}}</strong>
                    </td>
                    <td>
                        {{{ limit_name($property->name) }}}
                    </td>
                    <td>{{{ limit_name($property->category->category_name) }}}</td>
                    @if(Input::get('sort', -1) < 0)
                        <td>
                            <strong>{{{ $property->type->code }}}</strong>
                        </td>
                    @endif
                    <td>
                        {{{ if_null_then_na(limit_description($property->description)) }}}
                    </td>
                    <td class="table-action td-ignore">
                        <a href="{{ action('App\UnTrashController@getProperties') }}?d={{ $property->id }}" class="tooltips" data-toggle="tooltip" data-placement="left" title="Restoring will un-delete the property">Restore</a>
                    </td>
                </tr>
            @endif
        @endforeach
        </tbody>
    </table>
</div>