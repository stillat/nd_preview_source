<h4 class="dark">Equipment units cannot have nested properties.</h4>
<p>Nested properties are not available for equipment units.</p>