<div class="table-responsive">
    <table class="table table-hover table-striped mb30 table-fixed">
        <thead>
            <tr class="long_table_heading">
                <th class="wd-200">Code</th>
                <th class="wd-200">Name</th>
                <th class="wd-200">Category</th>
                <th>Description</th>
                {{ $adapterHeaders }}
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>
                    <strong>{{{ limit_code($property->code) }}}</strong>
                </td>
                <td>
                    {{{ limit_name($property->name) }}}
                </td>
                <td>{{{ limit_name($property->category->category_name) }}}</td>
             <td>
                 {{{ limit_description(if_null_then_na($property->description)) }}}
             </td>
             {{ $adapterData }}
         </tr>
     </tbody>
 </table>
</div>