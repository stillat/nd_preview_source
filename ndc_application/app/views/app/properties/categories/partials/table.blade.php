<div class="table-responsive">
    <table class="table table-striped table-hover">
        <thead>
        <tr>
            <th>Category Name</th>
            <th>Properties in this Category</th>
            <th class="wd-100"></th>
        </tr>
        </thead>
        <tbody>
        @foreach($categories as $category)
            <tr>
                <td>{{{ $category->category_name }}}</td>
                <td>{{{ $category->property_count }}}</td>
                <td class="table-action td-ignore">
                    <a href="{{ route('property-categories.edit', array($category->id)) }}"><i class="fa fa-pencil"></i></a>
                    <a class="cursor-pointer delete-row" data-rcontext="{{{ $category->id }}}"><i class="fa fa-trash-o"></i></a>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>