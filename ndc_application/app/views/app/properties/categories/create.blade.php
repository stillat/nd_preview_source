@extends('layouts.master')

@section('pageTitle')
    <i class="fa fa-plus"></i> You are adding a new property category
@stop

@section('buttonBar')
    <div class="btn-group n-mt-5">
        <a href="{{ Redirector::getRoute(route('property-categories.index')) }}" class="btn btn-danger ndc_form_leave">Cancel</a>
    </div>
@stop

@section('content')

    <div class="row">

        <div class="col-md-12">
            {{ Form::open(array('route' => 'property-categories.store', 'method' => 'post', 'class' => 'form-horizontal')) }}
            <div class="panel-primary panel-alt">
                <div class="panel-body">
                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="category_name">Category name:</label>
                        <div class="col-sm-8">
                            <input type="text" id="category_name" name="category_name" class="form-control mousetrap ndc_form_home_element {{{ user_form_size() }}}" value="{{{ Input::old('category_name', '') }}}" autofocus>
                            {{ Form::errorMsg('category_name') }}
                        </div>
                    </div>
                </div>
                <div class="panel-footer col-sm-offset-2">
                    <div class="btn-group mr5">
                        <button type="submit" id="ndc_form_defaultaction" value="addNew" name="submit" class="btn btn-primary">Save and add another property category</button>
                        <button type="button" id="ndc_form_secondaryaction" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
                            <span class="caret"></span>
                            <span class="sr-only">Toggle Dropdown</span>
                        </button>
                        <ul class="dropdown-menu" role="menu">
                            <li><button type="submit" value="return" name="submit" class="btn btn-white btn-md">Save and return to previous action</button></li>
                        </ul>
                    </div>

                    <div class="btn-group mr5">
                        <button type="reset" id="ndc_form_reset" class="btn btn-default">Reset</button>
                    </div>
                </div>
            </div>
            {{ Form::close() }}
        </div>

    </div>

@stop