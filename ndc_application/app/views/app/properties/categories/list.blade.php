@extends('layouts.master')

@section('pageTitle')
    <i class="fa fa-tag"></i> Property Categories
@stop

@section('buttonBar')
    <div class="btn-group n-mt-5">
        <a href="{{ action('App\Properties\PropertyCategoriesController@create') }}" class="btn btn-white nd-add"><i
                    class="fa fa-plus inline-icon"></i> Create Property Category</a>
    </div>
@stop

@section('content')
    <div class="row">
        <div class="col-sm-12">
            {{ $categories->links() }}
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
            @include('app.properties.categories.partials.table')
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
            {{ $categories->links() }}
        </div>
    </div>

    @include('layouts.dialogs.remove', array('title' => 'Remove Property Category', 'content' => 'Are you sure you want to remove this category? Any property that currently uses this category will be listed as "Uncategorized".', 'agreeText' => 'Remove Category', 'action' => 'App\Properties\PropertyCategoriesController@destroy'))
@stop