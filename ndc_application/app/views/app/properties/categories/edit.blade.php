@extends('layouts.master')

@section('pageTitle')
    <i class="fa fa-pencil"></i> {{{ $category->category_name }}}
@stop

@section('buttonBar')
    <div class="btn-group n-mt-5">
        <a href="{{ Redirector::getRoute(route('property-categories.index')) }}" class="btn btn-danger ndc_form_leave">Cancel</a>
    </div>
@stop

@section('content')

    <div class="row">

        <div class="col-md-12">
            {{ Form::open(array('route' => ['property-categories.update',$category->id], 'method' => 'put', 'class' => 'form-horizontal')) }}
            <div class="panel-primary panel-alt">
                <div class="panel-body">
                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="category_name">Category name:</label>
                        <div class="col-sm-8">
                            <input type="text" id="category_name" name="category_name" class="form-control mousetrap ndc_form_home_element {{{ user_form_size() }}}" value="{{{ Input::old('category_name', $category->category_name) }}}" autofocus>
                            {{ Form::errorMsg('category_name') }}
                        </div>
                    </div>
                </div>
                <div class="panel-footer col-sm-offset-2">
                    <div class="btn-group mr5">
                        <button type="submit" id="ndc_form_defaultaction" value="return" name="submit" class="btn btn-info">Update this property category</button>
                    </div>
                    <div class="btn-group mr5">
                        <button type="reset" id="ndc_form_reset" class="btn btn-default">Reset</button>
                    </div>
                </div>
            </div>
            {{ Form::close() }}
        </div>

    </div>

@stop