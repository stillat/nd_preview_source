<div class="table-responsive">
	<table class="table table-hover table-striped mb30 table-fixed">
		<thead>
			<tr>
				<th class="wd-200">Code</th>
				<th class="wd-200">Name</th>
				<th>Description</th>
				<th class="wd-100"></th>
			</tr>
		</thead>
		<tbody>
			@foreach($surfaceTypes as $type)
			<tr>
				<td><a href="{{ action('App\Properties\Roads\SurfaceTypesController@show', array($type->id)) }}"><strong>{{{ limit_code($type->code) }}}</strong></a></td>
				<td><a href="{{ action('App\Properties\Roads\SurfaceTypesController@show', array($type->id)) }}">{{{ limit_name($type->name) }}}</a></td>
				<td><a href="{{ action('App\Properties\Roads\SurfaceTypesController@show', array($type->id)) }}">{{{ if_null_then_na(limit_description($type->description)) }}}</a></td>
				<td class="table-action td-ignore">
					<a href="{{ action('App\Properties\Roads\SurfaceTypesController@edit', array($type->id)) }}"><i class="fa fa-pencil"></i></a>
					<a class="cursor-pointer delete-row" data-rcontext="{{{ $type->id }}}"><i class="fa fa-trash-o"></i></a>
				</td>
			</tr>
			@endforeach
		</tbody>
	</table>
</div>