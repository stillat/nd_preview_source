<div class="table-responsive">
	<table class="table table-hover table-striped mb30 table-fixed">
		<thead>
			<tr>
				<th class="wd-200">Code</th>
				<th class="wd-200">Name</th>
				<th>Description</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td><strong>{{{ $surfaceType->code }}}</strong></td>
				<td>{{{ $surfaceType->name }}}</td>
				<td>{{{ if_null_then_na($surfaceType->description) }}}</td>
			</tr>
		</tbody>
	</table>
</div>