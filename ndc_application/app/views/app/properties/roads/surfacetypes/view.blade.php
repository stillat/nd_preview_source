@extends('layouts.master')

@section('pageTitle')
<i class="fa fa-chevron-right"></i> {{{ limit_code($surfaceType->code) }}} - <small>{{{ limit_name($surfaceType->name) }}}</small>
@stop

@section('buttonBar')
<div class="btn-group pull-up-10">
    {{ HTML::modelNaviateToolbar('road-surface-types.show', $surfaceType->previous(), 'road-surface-types.index', 'road-surface-types.show', $surfaceType->next()) }}
</div>
<div class="btn-group pull-up-10">
    <a href="{{ route('road-surface-types.edit', array($surfaceType->id)) }}" class="btn btn-white"><i class="fa fa-edit inline-icon"></i> Update Surface Type</a>
</div>
@stop

@section('content')
<div class="row">
    <div class="col-md-12">
    @include('app.properties.roads.surfacetypes.partials.detail_table')
    </div>
</div>
@stop