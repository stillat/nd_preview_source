@extends('layouts.master')

@section('pageTitle')
<i class="fa fa-chevron-right"></i> Road Surface Types
@stop

@section('buttonBar')
<div class="btn-group">
	<a href="{{ action('App\Properties\Roads\SurfaceTypesController@create') }}" class="btn btn-white nd-add n-mt-5"><i class="fa fa-plus inline-icon"></i> New Surface Type</a>
</div>
@stop

@section('content')

<div class="row">

	<div class="col-sm-12">
		{{ $surfaceTypes->links() }}
	</div>

	<div class="col-sm-12">
		@include('app.properties.roads.surfacetypes.partials.table')
	</div>

	<div class="col-sm-12">
		{{ $surfaceTypes->links() }}
	</div>

</div>

@include('layouts.dialogs.remove', array('title' => 'Remove Surface Type', 'action' => 'App\Properties\Roads\SurfaceTypesController@destroy'))
@stop