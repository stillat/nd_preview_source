<div class="modal fade" id="convertEquipmentToRoadPropertyDialog" tabindex="-1" role="dialog" aria-labelledby="convertEquipmentToRoadPropertyDialogLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 id="convertEquipmentToRoadPropertyDialogLabel">Convert Equipment Unit to Road Record</h4>
            </div>
            <div class="modal-body">
                <p><strong>Important</strong>: Converting an equipment unit to a road property type is an irreversible action. </p>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-warning" id="confirmConvertEquipmentUnit">Convert Equipment Unit to Road</button>
                <button type="button" class="btn btn-default" id="cancelConvertEquipmentUnit" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>
</div>