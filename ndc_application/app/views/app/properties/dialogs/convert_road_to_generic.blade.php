<div class="modal fade" id="convertRoadToGenericDialog" tabindex="-1" role="dialog" aria-labelledby="convertRoadToGenericDialogLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            {{ Form::open(['action' => 'App\UtilityController@postConvertRoadsToGenerics', 'method' => 'post']) }}
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 id="convertRoadToGenericDialogLabel">Convert Roads to Generic Properties</h4>
            </div>
            <div class="modal-body">
                <p><strong>Important</strong>: Converting roads to generic properties is an irreversible action. Generic properties can still be used as roads when adding work records and when running reports. You will loose all road length data that has been entered on previous records.</p>
                <p>Convert roads to generic properties if the following is true for your use:</p>
                <ul>
                    <li>You <strong>do not</strong> track road length data;</li>
                    <li>You will not be tracking road length data;</li>
                    <li>You do not want to be prompted for road length data when using the "Property" field on work records</li>
                </ul>
            </div>
            <div class="modal-footer">
                <input type="hidden" name="convertRoadToGenerics" id="convertRoadToGenerics">
                <button type="submit" class="btn btn-warning" id="confirmConvertEquipmentUnit">Convert Selected Roads to Generic Properties</button>
                <button type="button" class="btn btn-default" id="cancelConvertEquipmentUnit" data-dismiss="modal">Cancel</button>
            </div>
            {{ Form::close() }}
        </div>
    </div>
</div>