@extends('layouts.master')

@section('pageTitle')
    <i class="fa fa-cog"></i> Settings
@stop

@section('additionalStyles')
    <style>.contentpanel {padding-top: 0px;} .inner-content-panel{padding-top:20px;}li.active>a{color:black;font-weight: bold;}fieldset>legend {padding-left: 15px;}fieldset>p{padding-left: 30px;}</style>
@stop

@section('content')

<div class="row">
    <div class="col-sm-3 inner-content-panel" style="border-right: 1px solid #d3d7db; min-height: 30px;">
        <h4><strong>Account Wide Settings</strong></h4>
        <?php
            $routePathToRestore =  Menu::getRoutePath();

        if (Str::startsWith(Request::url(), url('settings/reports/equations'))) {
            Menu::setRoutePath(url('settings/reports/equations'));
        } else {
            Menu::setRoutePath(Request::url());
        }

        ?>
        {{ Menu::render('settings_account_wide') }}
        <h4><strong>Report Settings</strong></h4>
        {{ Menu::render('settings_report_settings') }}
        <h4><strong>My Settings</strong></h4>
        {{ Menu::render('settings_user_settings') }}
        <?php
            Menu::setRoutePath($routePathToRestore);
        ?>
    </div>
    @yield('formOpen')
    <div class="<?php if (isset($extraWide)) {
    echo 'col-sm-9';
} else {
    echo 'col-sm-6';
} ?> inner-content-panel" style="padding-right:10px;padding-top:8px;">
        <h3><strong>@yield('settingTitle')</strong></h3>
        @yield('settingContent')
    </div>
    @yield('extraFields')
    @yield('formClose')

</div>
@stop