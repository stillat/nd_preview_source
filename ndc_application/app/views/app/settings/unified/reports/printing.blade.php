@extends('app.settings.master')

@section('settingTitle')
    Report Print Settings
@stop

@section('formOpen')
    {{ Form::open(['action' => 'App\Settings\ReportSettingsController@postPrint', 'method' => 'POST', 'class' => 'form-horizontal']) }}
@stop

@section('formClose')
    {{ Form::close() }}
@stop

@section('settingContent')
    <p>The report print settings allow further customization in how reports are printed. These settings control how reports appear both on screen and on physical paper. Adjust these settings to your company or organizations print standards.</p><hr/>

    <div class="form-group">
        <label class="col-sm-3 control-label" for="report_print_orientation">Default Orientation</label>
        <div class="col-sm-6">
            {{ Form::select('report_print_orientation', $orientations, Input::old('report_print_orientation', $reportSettings->report_print_orientation), ['class' => 'form-control']) }}
        </div>
    </div>
        <div class="ckbox ckbox-default col-sm-offset-3">
            {{ Form::checkbox('report_print_address', 1, Input::old('report_print_address', $reportSettings->report_print_address), ['id' => 'report_print_address']) }}
            <label for="report_print_address">Print Organization Address on Reports</label>
        </div>
        <div class="ckbox ckbox-default col-sm-offset-3">
            {{ Form::checkbox('report_print_filters', 1, Input::old('report_print_filters', $reportSettings->report_print_filters), ['id' => 'report_print_filters']) }}
            <label for="report_print_filters">Print Report Filters</label>
        </div>
        <div class="ckbox ckbox-default col-sm-offset-3">
            {{ Form::checkbox('report_print_report_engine_version', 1, Input::old('report_print_report_engine_version', $reportSettings->report_print_report_engine_version), ['id' => 'report_print_report_engine_version']) }}
            <label for="report_print_report_engine_version">Print the report engine version number on reports</label>
        </div>
        <div class="ckbox ckbox-default col-sm-offset-3">
            {{ Form::checkbox('report_print_generator', 1, Input::old('report_print_generator', $reportSettings->report_print_generator), ['id' => 'report_print_generator']) }}
            <label for="report_print_generator">Print the name of the person who generated the report ({{{ Auth::user()->first_name.' '.Auth::user()->last_name }}})</label>
        </div>

    <div class="form-group">
        <button type="submit" class="col-sm-offset-3 btn btn-primary">Update Report Print Settings</button>
    </div>
@stop