@extends('app.settings.master')

@section('settingTitle')
    Report Descriptions
@stop

@section('formOpen')
    {{ Form::open(['action' => 'App\Settings\ReportSettingsController@postDescription', 'method' => 'POST', 'class' => 'form-horizontal']) }}
@stop

@section('formClose')
    {{ Form::close() }}
@stop

@section('settingContent')
    <p>The report description settings control how, and if description fields are printed on reports. For example,
        activity, billing and work descriptions can all be adjusted independently. Regardless of settings, descriptions
        will only appear once per record on a report.</p><hr/>

    <div class="ckbox ckbox-default col-sm-offset-3">
        {{ Form::checkbox('only_show_descriptions_if_entered', 1, Input::old('only_show_descriptions_if_entered', $reportSettings->only_show_descriptions_if_entered), ['id' => 'only_show_descriptions_if_entered']) }}
        <label for="only_show_descriptions_if_entered">
            Only show descriptions listed below if they are available for the work record<br>
        </label>

        <p class="text-info"><strong>Note</strong>: If a report displays one of the descriptions listed below and there
            is no description available, a placeholder value will be used instead (regardless of this setting).</p>
    </div>
    <div class="ckbox ckbox-default col-sm-offset-3">
        {{ Form::checkbox('always_show_billing_description', 1, Input::old('always_show_billing_description', $reportSettings->always_show_billing_description), ['id' => 'always_show_billing_description']) }}
        <label for="always_show_billing_description">
            Always show billing descriptions on reports<br>
        </label>

        <p class="text-info"><strong>Note</strong>: If a report already displays the billing description, the billing
            description will not be added to the end of the record.</p>
    </div>
    <div class="ckbox ckbox-default col-sm-offset-3">
        {{ Form::checkbox('always_show_activity_description', 1, Input::old('always_show_activity_description', $reportSettings->always_show_activity_description), ['id' => 'always_show_activity_description']) }}
        <label for="always_show_activity_description">
            Always show activity descriptions on reports<br>
        </label>

        <p class="text-info"><strong>Note</strong>: If a report already displays the activity description, the activity
            description will not be added to the end of the record.</p>
    </div>

    <div class="form-group">
        <button type="submit" class="col-sm-offset-3 btn btn-primary">Update Report Descriptions</button>
    </div>
@stop