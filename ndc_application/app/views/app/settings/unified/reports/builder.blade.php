@extends('app.settings.master')

@section('settingTitle')
    Report Builder Settings
@stop

@section('formOpen')
    {{ Form::open(['action' => 'App\Settings\ReportSettingsController@postBuilder', 'method' => 'POST', 'class' => 'form-horizontal']) }}
@stop

@section('formClose')
    {{ Form::close() }}
@stop

@section('settingContent')
    <p>The report data builder settings allow the customization of the "Report Builder" page, which is used to generate reports. These settings are account-wide and will affect all users on the account.</p>
    <hr/>

    <div class="ckbox ckbox-default col-sm-offset-3">
        {{ Form::checkbox('show_removed_filters', 1, Input::old('show_removed_filters', $reportSettings->show_removed_filters), ['id' => 'chkShowRemovedFilters']) }}
        <label for="chkShowRemovedFilters">
            Show removed records in the filters list<br>
        </label>
        <p class="text-info">Enabling this option will display older, removed records in the filters list. Use this option to run reports with far-reaching historical data. When mainly running reports on current, up-to-date records, it is recommended to leave this option disabled.</p>
    </div>

    <div class="form-group">
        <button type="submit" class="col-sm-offset-3 btn btn-primary">Update Builder Settings</button>
    </div>
@stop