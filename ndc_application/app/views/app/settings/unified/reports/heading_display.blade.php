@extends('app.settings.master')

@section('settingTitle')
    Report Heading Display Settings
@stop

@section('formOpen')
    {{ Form::open(['action' => 'App\Settings\ReportSettingsController@postHeadingDisplay', 'method' => 'POST', 'class' => 'form-horizontal']) }}
@stop

@section('formClose')
    {{ Form::close() }}
@stop

@section('settingContent')
    <p><i class="fa fa-lightbulb-o"></i> These settings only apply to report group headings. To control how data in a report body is displayed, change the settings on the <a href="{{ url('settings/reports/display')  }}">report display settings</a> page.</p>
    <p>Report display settings control how various pieces of data are printed on reports. For example, employees can be displayed by their code, their name or their code <em>and</em> name. In addition, all data name's casing can be transformed on reports.</p><hr/>

    <div class="form-group">
        <label class="col-sm-3 control-label" for="text_transformation">Text Transformation</label>
        <div class="col-sm-3">
            {{ Form::select('text_transformation', $validTransformations, Input::old('text_transformation', $formattingSettings->transform->transform), ['class' => 'form-control']) }}
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-3 control-label" for="employee_format">Employee Display Format</label>
        <div class="col-sm-3">
            {{ Form::select('employee_format', $validDisplaySettings, Input::old('employee_format', $formattingSettings->employee->format), ['class' => 'form-control']) }}
        </div>
        <label class="col-sm-3 control-label" for="fuel_format">Fuel Display Format</label>
        <div class="col-sm-3">
            {{ Form::select('fuel_format', $validDisplaySettings, Input::old('fuel_format', $formattingSettings->fuel->format), ['class' => 'form-control']) }}
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-3 control-label" for="material_format">Material Display Format</label>
        <div class="col-sm-3">
            {{ Form::select('material_format', $validDisplaySettings, Input::old('material_format', $formattingSettings->material->format), ['class' => 'form-control']) }}
        </div>
        <label class="col-sm-3 control-label" for="activity_format">Activity Display Format</label>
        <div class="col-sm-3">
            {{ Form::select('activity_format', $validDisplaySettings, Input::old('activity_format', $formattingSettings->activity->format), ['class' => 'form-control']) }}
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-3 control-label" for="department_format">Department Display Format</label>
        <div class="col-sm-3">
            {{ Form::select('department_format', $validDisplaySettings, Input::old('department_format', $formattingSettings->department->format), ['class' => 'form-control']) }}
        </div>
        <label class="col-sm-3 control-label" for="district_format">District Display Format</label>
        <div class="col-sm-3">
            {{ Form::select('district_format', $validDisplaySettings, Input::old('district_format', $formattingSettings->district->format), ['class' => 'form-control']) }}
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-3 control-label" for="project_format">Project Display Format</label>
        <div class="col-sm-3">
            {{ Form::select('project_format', $validDisplaySettings, Input::old('project_format', $formattingSettings->project->format), ['class' => 'form-control']) }}
        </div>
        <label class="col-sm-3 control-label" for="property_format">Property Display Format</label>
        <div class="col-sm-3">
            {{ Form::select('property_format', $validDisplaySettings, Input::old('property_format', $formattingSettings->property->format), ['class' => 'form-control']) }}
        </div>
    </div>

    <div class="form-group">
        <button type="submit" class="col-sm-offset-3 btn btn-primary">Update Report Heading Display Settings</button>
    </div>

@stop