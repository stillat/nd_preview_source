@extends('app.settings.master')

@section('settingTitle')
    Report Data Breakdowns
@stop

@section('formOpen')
    {{ Form::open(['action' => 'App\Settings\ReportSettingsController@postBreakdown', 'method' => 'POST', 'class' => 'form-horizontal']) }}
@stop

@section('formClose')
    {{ Form::close() }}
@stop

@section('settingContent')
    <p>The report data breakdown settings control the how the reporting engine prints breakdown tables by default.
        Breakdown tables display additional information, such as equipment and fuel usage. Breakdowns are not available
        on all reports.</p><hr/>

    <div class="ckbox ckbox-default col-sm-offset-3">
        {{ Form::checkbox('show_borders_on_breakdowns', 1, Input::old('show_borders_on_breakdowns', $reportSettings->show_borders_on_breakdowns), ['id' => 'show_borders_on_breakdowns']) }}
        <label for="show_borders_on_breakdowns">Show Borders on Breakdown Tables</label>
    </div>
    <div class="form-group" style="margin-top: 10px;">
        <label class="col-sm-3 control-label" for="record_breakdown_style">Breakdown Table Display Style</label>

        <div class="col-sm-6">
            {{ Form::select('record_breakdown_style', $displayModes, Input::old('record_breakdown_style', $reportSettings->record_breakdown_style), ['class' => 'form-control']) }}
        </div>
    </div>

    <div class="ckbox ckbox-default col-sm-offset-3">
        {{ Form::checkbox('show_employee_wage', 1, Input::old('show_employee_wage', $reportSettings->show_employee_wage), ['id' => 'chkShowEmployeeWage']) }}
        <label for="chkShowEmployeeWage">
            Show Employee Wage<br>
        </label>
        <p class="text-info">This option overrides internal reporting settings.</p>
    </div>

    <div class="ckbox ckbox-default col-sm-offset-3">
        {{ Form::checkbox('show_employee_hours_worked', 1, Input::old('show_employee_hours_worked', $reportSettings->show_employee_hours_worked), ['id' => 'chkShowEmployeeHoursWorked']) }}
        <label for="chkShowEmployeeHoursWorked">
            Show Employee Hours Worked<br>
        </label>
        <p class="text-info">This option overrides internal reporting settings.</p>
    </div>

    <div class="ckbox ckbox-default col-sm-offset-3">
        {{ Form::checkbox('show_material_breakdown', 1, Input::old('show_material_breakdown', $reportSettings->show_material_breakdown), ['id' => 'show_material_breakdown']) }}
        <label for="show_material_breakdown">Show Material Breakdown Table</label>
    </div>
    <div class="ckbox ckbox-default col-sm-offset-3">
        {{ Form::checkbox('show_material_breakdown_total', 1, Input::old('show_material_breakdown_total', $reportSettings->show_material_breakdown_total), ['id' => 'show_material_breakdown_total']) }}
        <label for="show_material_breakdown_total">Show Totals on Material Breakdown Table</label>
    </div>
    <div class="ckbox ckbox-default col-sm-offset-3">
        {{ Form::checkbox('show_equipment_unit_breakdown', 1, Input::old('show_equipment_unit_breakdown', $reportSettings->show_material_breakdown), ['id' => 'show_equipment_unit_breakdown']) }}
        <label for="show_equipment_unit_breakdown">Show Equipment Unit Breakdown Table</label>
    </div>
    <div class="ckbox ckbox-default col-sm-offset-3">
        {{ Form::checkbox('show_equipment_unit_breakdown_total', 1, Input::old('show_equipment_unit_breakdown_total', $reportSettings->show_equipment_unit_breakdown_total), ['id' => 'show_equipment_unit_breakdown_total']) }}
        <label for="show_equipment_unit_breakdown_total">Show Totals on Equipment Unit Breakdown Table</label>
    </div>
    <div class="ckbox ckbox-default col-sm-offset-3">
        {{ Form::checkbox('show_equipment_unit_rate', 1, Input::old('show_equipment_unit_rate', $reportSettings->show_equipment_unit_rate), ['id' => 'show_equipment_unit_rate']) }}
        <label for="show_equipment_unit_rate">Show Equipment Unit Rate on Reports</label>
    </div>
    <div class="ckbox ckbox-default col-sm-offset-3">
        {{ Form::checkbox('show_fuel_breakdown', 1, Input::old('show_fuel_breakdown', $reportSettings->show_fuel_breakdown), ['id' => 'show_fuel_breakdown']) }}
        <label for="show_fuel_breakdown">
            Show Fuel Breakdown Table<br>
        </label>
        <p class="text-info"><strong>Note</strong>: Fuel breakdowns are only available when equipment unit breakdown
            tables are enabled.</p>
    </div>
    <div class="ckbox ckbox-default col-sm-offset-3">
        {{ Form::checkbox('show_fuel_breakdown_total', 1, Input::old('show_fuel_breakdown_total', $reportSettings->show_fuel_breakdown_total), ['id' => 'show_fuel_breakdown_total']) }}
        <label for="show_fuel_breakdown_total">
            Show Totals on Fuel Breakdown Table<br>
        </label>
        <p class="text-info"><strong>Note</strong>: Fuel breakdown totals are only available when equipment unit
            breakdown tables are enabled.</p>
    </div>
    <div class="ckbox ckbox-default col-sm-offset-3">
        {{ Form::checkbox('show_property_breakdown', 1, Input::old('show_property_breakdown', $reportSettings->show_property_breakdown), ['id' => 'show_property_breakdown']) }}
        <label for="show_property_breakdown">Show Property Breakdown Table</label>
    </div>
    <div class="ckbox ckbox-default col-sm-offset-3">
        {{ Form::checkbox('show_property_breakdown_total', 1, Input::old('show_property_breakdown_total', $reportSettings->show_property_breakdown_total), ['id' => 'show_property_breakdown_total']) }}
        <label for="show_property_breakdown_total">Show Totals on Property Breakdown Table</label>
    </div>
    <div class="form-group">
        <button type="submit" class="col-sm-offset-3 btn btn-primary">Update Breakdown Settings</button>
    </div>
@stop