@extends('app.settings.master')

@section('settingTitle')
    Inventory Management
@stop

@section('formOpen')
    {{ Form::open(['action' => 'App\Settings\AccountSettingsController@postInventory', 'method' => 'POST', 'class' => 'form-horizontal']) }}
@stop

@section('formClose')
    {{ Form::close() }}
@stop

@section('settingContent')
    <p>These settings allow you to configure how the system handles inventory management; and even if it should be used
        at all. These settings affect all users of the account.</p>
    <hr/>

    <div class="form-group is-required">
        <div class="col-sm-10 col-sm-offset-3">
            <div class="ckbox ckbox-default">
                {{ Form::checkbox('show_inventory_management_in_consumables', 1, Input::old('show_inventory_management_in_consumables', $inventorySettings->show_inventory_management_in_consumables), ['id' => 'show_inventory_management_in_consumables']) }}
                <label for="show_inventory_management_in_consumables">Show inventory management data in fuel and material listings</label>
            </div>
        </div>
        <div class="col-sm-10 col-sm-offset-3">
            <div class="ckbox ckbox-default">
                {{ Form::checkbox('auto_deduct_on_record_create', 1, Input::old('auto_deduct_on_record_create', $inventorySettings->auto_deduct_on_record_create), ['id' => 'auto_deduct_on_record_create']) }}
                <label for="auto_deduct_on_record_create">Automatically adjust inventory levels when creating or updating work records</label>
            </div>
        </div>
        <div class="col-sm-10 col-sm-offset-3">
            <div class="ckbox ckbox-default">
                {{ Form::checkbox('convert_removed_work_inventory_items_to_system',1, Input::old('convert_removed_work_inventory_items_to_system', $inventorySettings->convert_removed_work_inventory_items_to_system), ['id' => 'convert_removed_work_inventory_items_to_system']) }}
                <label for="convert_removed_work_inventory_items_to_system">When work records are removed, retain any inventory management data associated<br>with the work record.</label>
            </div>
        </div>
    </div>

    <div class="form-group">
        <button type="submit" class="col-sm-offset-3 btn btn-primary">Update Inventory Management</button>
    </div>
@stop