@extends('app.settings.master')

@section('settingTitle')
    Contact Information
@stop

@section('formOpen')
    {{ Form::open(['action' => 'App\Settings\AccountSettingsController@postIndex', 'method' => 'POST', 'class' => 'form-horizontal']) }}
@stop

@section('formClose')
    {{ Form::close() }}
@stop

@section('settingContent')
    <p>Make sure to keep your account's contact information up to date. The contact information set below will be our primary way of contacting account holders. Some features and tools rely on these settings.</p><hr/>

    <div class="form-group">
        <label class="col-sm-3 control-label" for="account_name">Account Name:</label>
        <div class="col-sm-9">
            <input type="text" id="account_name" name="account_name"
                   class="form-control mousetrap ndc_form_home_element {{{ user_form_size() }}}"
                   value="{{{ Input::old('account_name', $currentAccount->account_name) }}}" autofocus>
            {{ Form::errorMsg('account_name') }}
        </div>
    </div>
    <div class="form-group is-required">
        <label class="col-sm-3 control-label" for="address_line_1">Address Line 1:</label>
        <div class="col-sm-9">
            <input type="text" id="address_line_1" name="address_line_1" class="form-control mousetrap {{{ user_form_size() }}}" value="{{{ Input::old('address_line_1', $currentAccount->address_line_1) }}}">
            {{ Form::errorMsg('address_line_1') }}
        </div>
    </div>
    <div class="form-group is-required">
        <label class="col-sm-3 control-label" for="address_line_2">Address Line 2:</label>
        <div class="col-sm-9">
            <input type="text" id="address_line_2" name="address_line_2" class="form-control mousetrap {{{ user_form_size() }}}" value="{{{ Input::old('address_line_2', $currentAccount->address_line_2) }}}">
            {{ Form::errorMsg('address_line_2') }}
        </div>
    </div>
    <div class="form-group is-required">
        <label class="col-sm-3 control-label" for="city">City:</label>
        <div class="col-sm-4">
            <input type="text" id="city" name="city" class="form-control mousetrap {{{ user_form_size() }}}" value="{{{ Input::old('city', $currentAccount->city) }}}">
            {{ Form::errorMsg('city') }}
        </div>
    </div>
    <div class="form-group is-required">
        <label class="col-sm-3 control-label" for="state">State:</label>
        <div class="col-sm-4">
            <input type="text" id="state" name="state" class="form-control mousetrap {{{ user_form_size() }}}" value="{{{ Input::old('state', $currentAccount->state) }}}">
            {{ Form::errorMsg('state') }}
        </div>
    </div>
    <div class="form-group is-required">
        <label class="col-sm-3 control-label" for="country">Country:</label>
        <div class="col-sm-4">
            <input type="text" id="country" name="country" class="form-control mousetrap {{{ user_form_size() }}}" value="{{{ Input::old('country', $currentAccount->country) }}}">
            {{ Form::errorMsg('country') }}
        </div>
    </div>
    <div class="form-group is-required">
        <label class="col-sm-3 control-label" for="zip">Zip:</label>
        <div class="col-sm-2">
            <input type="text" id="zip" name="zip" class="form-control mousetrap {{{ user_form_size() }}}" value="{{{ Input::old('zip', $currentAccount->zip) }}}">
            {{ Form::errorMsg('zip') }}
        </div>
    </div>
    <div class="form-group">
        <button type="submit" class="col-sm-offset-3 btn btn-primary">Update Contact Information</button>
    </div>
@stop