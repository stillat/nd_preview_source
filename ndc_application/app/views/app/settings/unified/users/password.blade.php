@extends('app.settings.master')

@section('settingTitle')
    Change My Password
@stop

@section('formOpen')
    {{ Form::open(['action' => 'App\Settings\UserSettingsController@postChangePassword', 'method' => 'POST', 'class' => 'form-horizontal']) }}
@stop

@section('formClose')
    {{ Form::close() }}
@stop

@section('settingContent')
    <p>Keep your account secure by periodically changing your password. Also remember that easy passwords are easy to hack and guess. Consider using both upper and lowercase characters, special characters (such as <code>!@#$%^&amp;*(){}[]</code>) and making the password at least 8 characters in length. Never share your password.</p><hr/>

    <div class="form-group is-required">
        <label class="col-sm-3 control-label" for="current_password">Current Password:</label>
        <div class="col-sm-9">
            <input type="password" id="current_password" name="current_password" class="form-control mousetrap {{{ user_form_size() }}}" value="{{{ Input::old('current_password', '') }}}" autofocus="autofocus">
            {{ Form::errorMsg('current_password') }}
        </div>
    </div>
    <div class="form-group is-required">
        <label class="col-sm-3 control-label" for="new_password">New Password:</label>
        <div class="col-sm-9">
            <input type="password" id="new_password" name="new_password" class="form-control mousetrap {{{ user_form_size() }}}" value="{{{ Input::old('new_password', '') }}}">
            {{ Form::errorMsg('new_password') }}
        </div>
    </div>
    <div class="form-group is-required">
        <label class="col-sm-3 control-label" for="new_password_confirmation">Confirm New Password:</label>
        <div class="col-sm-9">
            <input type="password" id="address_line_1" name="new_password_confirmation" class="form-control mousetrap {{{ user_form_size() }}}" value="{{{ Input::old('new_password_confirmation', '') }}}">
            {{ Form::errorMsg('new_password_confirmation') }}
        </div>
    </div>
    <div class="form-group">
        <button type="submit" class="col-sm-offset-3 btn btn-primary">Change My Password</button>
    </div>
@stop