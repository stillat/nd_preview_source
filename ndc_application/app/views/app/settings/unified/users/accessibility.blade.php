@extends('app.settings.master')

@section('settingTitle')
    Accessibility &amp; Ease of Use Settings
@stop

@section('formOpen')
    {{ Form::open(['action' => 'App\Settings\UserSettingsController@postAccessibility', 'method' => 'POST', 'class' => 'form-horizontal']) }}
@stop

@section('formClose')
    {{ Form::close() }}
@stop

@section('settingContent')
    <p>Accessibility settings are available to allow users to customize how they interact with the service. Options
        include how many items to show on a page, when the values in the forms are reset and if we should show an
        animation when navigating forms.</p>
    <hr/>
    <fieldset>
        <legend>General Accessibility Settings</legend>
        <div class="ckbox ckbox-default col-sm-offset-2">
            {{ Form::checkbox('auto_expand_more_options', 1, Input::old('auto_expand_more_options', $userSettings->auto_expand_more_options), ['id' => 'auto_expand_more_options']) }}
            <label for="auto_expand_more_options">
                Auto expand hidden options on larger forms
            </label>
        </div>
        <div class="ckbox ckbox-default col-sm-offset-2">
            {{ Form::checkbox('auto_clear_general_forms', 1, Input::old('auto_clear_general_forms', $userSettings->auto_clear_forms), ['id' => 'auto_clear_general_forms']) }}
            <label for="auto_clear_general_forms">
                Auto clear form values when a record is successfully created
            </label>
        </div>
        <div class="ckbox ckbox-default col-sm-offset-2 mb10">
            {{ Form::checkbox('show_animation_on_form_change', 1, Input::old('show_animation_on_form_change', $userSettings->show_animation_on_form_change), ['id' => 'show_animation_on_form_change']) }}
            <label for="show_animation_on_form_change">
                Play an animation when tabbing between form elements
            </label>
        </div>
        <div class="form-group">
            <label class="col-sm-5 control-label" for="number_of_items_per_page">Number of items to show on a page:</label>

            <div class="col-sm-3"><input type="number" id="number_of_items_per_page" name="number_of_items_per_page"
                                         class="form-control mousetrap text-right {{{ user_form_size() }}}" min="0" max="200" value="{{{ Input::old('number_of_items_per_page', $userSettings->items_per_page) }}}">
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-3 control-label" for="">Form size:</label>
            <div class="col-sm-6 mb30">
                {{ Form::select('form_size', $formSizes, Input::old('form_size', $userSettings->form_size), array('class' => 'form-control mousetrap', 'data-placeholder' => 'Choose a form size')) }}
            </div>
        </div>
    </fieldset>

    <fieldset>
        <legend>Work Record Form Workflow</legend>
        <div class="col-sm-offset-2">
            <p class="static-control"><strong>General Behavior</strong></p>
        </div>
        <div class="ckbox ckbox-default col-sm-offset-2">
            {{ Form::checkbox('wl_return_to_general', 1, Input::old('wl_return_to_general', $workResetSettings->wl_return_to_general), ['id' => 'wl_return_to_general']) }}
            <label for="wl_return_to_general" style="display:inline">
                Return to the "General Information" tab after a work record has been saved
            </label>
        </div>
        <div class="ckbox ckbox-default col-sm-offset-2">
            {{ Form::checkbox('wl_refresh_employee', 1, Input::old('wl_refresh_employee', $workResetSettings->wl_refresh_employee), ['id' => 'wl_refresh_employee']) }}
            <label for="wl_refresh_employee">
                When updating a record, always update an employees wage information when<br>
                the record's employee is changed. By default, we will keep the wage<br>
                information that was entered when the record was initially created.
            </label>
        </div>
        <div class="col-sm-offset-2">
            <p class="static-control"><strong>Optional Data Entry</strong><br>These settings can be used to make certain fields on the "Add Work Entry" page optional. When a field is made optional, it will still appear on the page but will be ignored by the tab key. Optional fields will have a blue border.</p>
        </div>
        <div class="ckbox ckbox-default col-sm-offset-2">
            {{ Form::checkbox('wl_optional_employee_overtime', 1, Input::old('wl_optional_employee_overtime', $workResetSettings->wl_optional_employee_overtime), ['id' => 'wl_optional_employee_overtime']) }}
            <label for="wl_optional_employee_overtime">
                <i class="fa fa-user"></i> Entering employee overtime should be optional
            </label>
        </div>
        <div class="ckbox ckbox-default col-sm-offset-2">
            {{ Form::checkbox('wl_optional_district', 1, Input::old('wl_optional_district', $workResetSettings->wl_optional_district), ['id' => 'wl_optional_district']) }}
            <label for="wl_optional_district">
                <i class="fa fa-map-marker"></i> Entering a district should be optional
            </label>
        </div>
        <div class="col-sm-offset-2">
            <p class="static-control"><strong>Automatically Clear Work Record Fields</strong><br>Be sure to enable the "Auto clear form values" setting above for these settings to take affect.</p>
        </div>
        <div class="ckbox ckbox-default col-sm-offset-2">
            {{ Form::checkbox('wl_reset_employee', 1, Input::old('wl_reset_employee', $workResetSettings->wl_reset_employee), ['id' => 'wl_reset_employee']) }}
            <label for="wl_reset_employee">
                <i class="fa fa-user"></i> Clear employee information after a record has been saved
            </label>
        </div>
        <div class="ckbox ckbox-default col-sm-offset-2">
            {{ Form::checkbox('wl_reset_work_date', 1, Input::old('wl_reset_work_date', $workResetSettings->wl_reset_work_date), ['id' => 'wl_reset_work_date']) }}
            <label for="wl_reset_work_date">
                <i class="fa fa-calendar-o"></i> Clear work date after a record has been saved
            </label>
        </div>
        <div class="ckbox ckbox-default col-sm-offset-2">
            {{ Form::checkbox('wl_reset_activity', 1, Input::old('wl_reset_activity', $workResetSettings->wl_reset_activity), ['id' => 'wl_reset_activity']) }}
            <label for="wl_reset_activity">
                <i class="fa ndc-bicycle"></i> Clear activity after a record has been saved
            </label>
        </div>
        <div class="ckbox ckbox-default col-sm-offset-2">
            {{ Form::checkbox('wl_reset_department', 1, Input::old('wl_reset_department', $workResetSettings->wl_reset_department), ['id' => 'wl_reset_department']) }}
            <label for="wl_reset_department">
                <i class="fa fa-users"></i> Clear department after a record has been saved
            </label>
        </div>
        <div class="ckbox ckbox-default col-sm-offset-2">
            {{ Form::checkbox('wl_reset_district', 1, Input::old('wl_reset_district', $workResetSettings->wl_reset_district), ['id' => 'wl_reset_district']) }}
            <label for="wl_reset_district">
                <i class="fa fa-map-marker"></i> Clear district after a record has been saved
            </label>
        </div>
        <div class="ckbox ckbox-default col-sm-offset-2">
            {{ Form::checkbox('wl_reset_project', 1, Input::old('wl_reset_project', $workResetSettings->wl_reset_project), ['id' => 'wl_reset_project']) }}
            <label for="wl_reset_project">
                <i class="fa fa-folder-open"></i> Clear project after a record has been saved
            </label>
        </div>
        <div class="ckbox ckbox-default col-sm-offset-2">
            {{ Form::checkbox('wl_reset_property', 1, Input::old('wl_reset_property', $workResetSettings->wl_reset_property), ['id' => 'wl_reset_property']) }}
            <label for="wl_reset_property">
                <i class="fa fa-home"></i> Clear property after a record has been saved
            </label>
        </div>
        <div class="ckbox ckbox-default col-sm-offset-2">
            {{ Form::checkbox('wl_reset_materials_and_items', 1, Input::old('wl_reset_materials_and_items', $workResetSettings->wl_reset_materials_and_items), ['id' => 'wl_reset_materials_and_items']) }}
            <label for="wl_reset_materials_and_items">
                <i class="fa fa-cubes"></i> Clear materials &amp; items after a record has been saved
            </label>
        </div>
        <div class="ckbox ckbox-default col-sm-offset-2">
            {{ Form::checkbox('wl_reset_equipment_and_fuels', 1, Input::old('wl_reset_equipment_and_fuels', $workResetSettings->wl_reset_equipment_and_fuels), ['id' => 'wl_reset_equipment_and_fuels']) }}
            <label for="wl_reset_equipment_and_fuels">
                <i class="fa fa-truck"></i> Clear equipment &amp; fuels after a record has been saved
            </label>
        </div>
        <div class="ckbox ckbox-default col-sm-offset-3">
            {{ Form::checkbox('wl_reset_only_fuels', 1, Input::old('wl_reset_only_fuels', $workResetSettings->wl_reset_only_fuels), ['id' => 'wl_reset_only_fuels']) }}
            <label for="wl_reset_only_fuels">
                <i class="ndc-fuel"></i> Clear only fuels after a record has been saved<br>Requires "Clear equipment &amp; fuels after a record has been saved" to be checked.
            </label>
        </div>
        <div class="ckbox ckbox-default col-sm-offset-3">
            {{ Form::checkbox('wl_keep_only_first_equip', 1, Input::old('wl_keep_only_first_equip', $workResetSettings->wl_keep_only_first_equip), ['id' => 'wl_keep_only_first_equip']) }}
            <label for="wl_keep_only_first_equip">
                <i class="fa fa-truck"></i> When clearing only the fuels, keep the first equipment record<br>Requires "Clear only fuels after a record has been saved" to be checked.
            </label>
        </div>
        <div class="ckbox ckbox-default col-sm-offset-2">
            {{ Form::checkbox('wl_reset_settings', 1, Input::old('wl_reset_settings', $workResetSettings->wl_reset_settings), ['id' => 'wl_reset_settings']) }}
            <label for="wl_reset_settings">
                <i class="fa fa-wrench"></i> Clear work record settings after a record has been saved
            </label>
        </div>
        </fieldset>
        <fieldset style="margin-top: 15px;">
        <legend>Work Record Form Legacy &amp; Compatibility Settings</legend>
        <div class="col-sm-offset-2">
            <p class="static-control">Legacy &amp; compatibility settings exist to help ease the transition to more advanced, modern features. Enabling these settings might restrict the use of advanced features, and are generally <strong>not</strong> recommended for new users.</p>
        </div>
        <div class="ckbox ckbox-default col-sm-offset-2">
            {{ Form::checkbox('wl_enable_equipment_compatibility', 1, Input::old('wl_enable_equipment_compatibility', $workResetSettings->wl_enable_equipment_compatibility), ['id' => 'wl_enable_equipment_compatibility']) }}
            <label for="wl_enable_equipment_compatibility" style="display:inline">
                Enable legacy equipment unit compatibility<br>
                <p>Legacy equipment unit compatibility allows the first equipment unit to be set from the "General Information" tab. This <strong>does not affect</strong> the behavior of equipment service repairs.</p>
            </label>
        </div>
        <div class="ckbox ckbox-default col-sm-offset-2">
            {{ Form::checkbox('wl_enable_invoice_compatibility', 1, Input::old('wl_enable_invoice_compatibility', $workResetSettings->wl_enable_invoice_compatibility), ['id' => 'wl_enable_invoice_compatibility']) }}
            <label for="wl_enable_invoice_compatibility" style="display:inline">
                Hide integrated invoice system<br>
                <p>Enabling this option will hide the integrated invoicing system that appears when creating and updating work records. When this option is enabled, only the billing information will appear on the "Invoices &amp; Billing" tab. This setting does <strong>not</strong> disable the invoice system.</p>
            </label>
        </div>
    </fieldset>

    <div class="form-group" style="margin-top: 10px;">
        <button type="submit" class="col-sm-offset-2 btn btn-primary">Update Accessibility Settings</button>
    </div>

@stop