@extends('app.settings.master')

@section('settingTitle')
    User Contact Information
@stop

@section('formOpen')
    {{ Form::open(['action' => 'App\Settings\UserSettingsController@postContact', 'files' => true, 'method' => 'POST', 'class' => 'form-horizontal']) }}
@stop

@section('formClose')
    {{ Form::close() }}
@stop

@section('extraFields')
    <div class="col-sm-3" style="border-left: 1px solid #B8BEC4; margin-top: 9%;min-height: 30px;">
        <img id="imagePreviewHolder" src="{{ $avatarPresenter->getAvatar($currentUser->id, $currentUser->email, 2, '_311') }}"
             style="width: 112px;height:112px;" class="img-responsive"><br>
        <input type="file" id="changeUserPhoto" name="profile_image" style="display: none;">
        <a id="changerUserPhotoActivation" style="text-decoration: none;cursor: pointer;"><i class="fa fa-photo"></i> Change profile image</a>
    </div>
@stop

@section('settingContent')
    <p>Make sure your contact information is up to date and that you can receive emails at the address that is listed.
        If you ever need to reset your password, you will need to be able to receive emails from us.</p><hr/>

    <div class="form-group is-required">
        <label class="col-sm-3 control-label" for="first_name">First Name:</label>

        <div class="col-sm-9">
            <input type="text" id="first_name" name="first_name" class="form-control mousetrap {{{ user_form_size() }}}"
                   value="{{{ Input::old('first_name', $currentUser->first_name) }}}">
            {{ Form::errorMsg('first_name') }}
        </div>
    </div>
    <div class="form-group is-required">
        <label class="col-sm-3 control-label" for="last_name">Last Name:</label>

        <div class="col-sm-9">
            <input type="text" id="last_name" name="last_name" class="form-control mousetrap {{{ user_form_size() }}}"
                   value="{{{ Input::old('last_name', $currentUser->last_name) }}}">
            {{ Form::errorMsg('last_name') }}
        </div>
    </div>
    <div class="form-group is-required">
        <label class="col-sm-3 control-label" for="email">Email Address:</label>

        <div class="col-sm-9">
            <input type="email" id="email" name="email" class="form-control mousetrap {{{ user_form_size() }}}"
                   value="{{{ Input::old('email', $currentUser->email) }}}">
            {{ Form::errorMsg('email') }}
        </div>
    </div>

    <div class="form-group">
        <button type="submit" class="col-sm-offset-3 btn btn-primary">Update User Contact Information</button>
    </div>
@stop

@section('additionalScripts')
    <script src="{{ app_url() }}/assets/js/settings/settings.js" defer></script>
@stop