@extends('app.settings.master')

@section('settingTitle')
    Reset All Sorting Settings
@stop

@section('formOpen')
    {{ Form::open(['action' => 'App\Settings\UserSettingsController@postSorting', 'method' => 'POST', 'class' => 'form-horizontal']) }}
@stop

@section('formClose')
    {{ Form::close() }}
@stop

@section('settingContent')
    <p>You can reset all your sorting settings from this page. Additionally, you can reset each sort setting individually by clicking the "Reset Sorting Settings" option in the lower left corner of each sorting window.</p>
    <hr/>
    <div class="form-group">
        <button type="submit" class="col-sm-offset-3 btn btn-danger-alt">Reset All Sorting Settings</button>
    </div>
@stop