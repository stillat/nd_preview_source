@extends('app.settings.master')

@section('settingTitle')
    System Behavior
@stop

@section('formOpen')
    {{ Form::open(['action' => 'App\Settings\AccountSettingsController@postBehavior', 'method' => 'POST', 'class' => 'form-horizontal']) }}
@stop

@section('formClose')
    {{ Form::close() }}
@stop

@section('settingContent')
    <p>System behavior settings control how the service responds to users, or how things work internally. For example,
        the decimal places (or precision) used in mathematical expressions can be adjusted as well how the system should
        react to invoices.</p><hr/>

    <div class="form-group is-required">
        <div class="form-group">
            <label class="col-sm-3 control-label" for="data_decimal_places">Decimal Places</label>

            <div class="col-sm-4">
                <input type="number" min="0" max="6" class="form-control text-right" name="data_decimal_places"
                       id="data_decimal_places"
                       value="{{{ Input::old('data_decimal_places', $accountSettings->data_decimal_places) }}}"/>
            </div>
        </div>

        <fieldset>
            <legend>Invoicing</legend>
            <p>Any changes to the invoicing behavior settings will be applied on all future invoices (unless the settings are changed again).</p>
            <div class="col-sm-10 col-sm-offset-3">
                <div class="ckbox ckbox-default">
                    {{ Form::checkbox('auto_process_invoice', 1, Input::old('auto_process_invoice', $accountSettings->auto_process_invoice), ['id' => 'chkAutoTransaction']) }}
                    <label for="chkAutoTransaction">Automatically Process Invoice Transactions<br><p>Check this option to have account balancces automatically updated when processing invoices.<br><strong>Do not</strong> check this option if account balances are managed manually.</p></label>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label" for="default_invoice_status">Default Transaction Status</label>
                <div class="col-sm-4">
                    {{ Form::select('default_invoice_status', $transactionStatuses, Input::old('default_invoice_status', $accountSettings->invoice_default_status), ['class' => 'form-control']) }}
                </div>
            </div>
        </fieldset>
    </div>

    <div class="form-group">
        <button type="submit" class="col-sm-offset-3 btn btn-primary">Update System Behavior</button>
    </div>
@stop