@extends('layouts.master')
@section('pageTitle')
<i class="fa fa-rocket"></i> {{{ $currentAccount->account_name }}} Account Settings
@stop
@section('buttonBar')
<div class="btn-group pull-up-10">
    <a href="{{ Redirector::getRoute(url('/')) }}" class="btn btn-danger ndc_form_leave">Cancel</a>
</div>
@stop
@section('content')
<div class="row">
    <div class="col-md-12">
        {{ Form::open(array('url' => '/settings/account', 'method' => 'post', 'class' => 'form-horizontal')) }}
        <div class="panel panel-default">
            <div class="panel-body">
                <div class="form-group">
                    <label class="col-sm-2 control-label" for="account_name">Account Name:</label>
                    <div class="col-sm-8">
                        <input type="text" id="account_name" name="account_name" class="form-control mousetrap ndc_form_home_element {{{ user_form_size() }}}" value="{{{ Input::old('account_name', $currentAccount->account_name) }}}" autofocus>
                        {{ Form::errorMsg('account_name') }}
                    </div>
                </div>
                <ul class="nav nav-tabs">
                    <li class="active"><a href="#accountContactInformation" data-toggle="tab"><strong>Contact Information</strong></a></li>
                    <li class=""><a href="#accountDataSettings" data-toggle="tab"><strong>Account Data Settings</strong></a></li>
                    <li class=""><a href="#accountDisplaySettings" data-toggle="tab"><strong>Report Display Settings</strong></a></li>
                </ul>
                <div class="tab-content">
                @include('app.settings.account_parts.contact')
                @include('app.settings.account_parts.data')
                @include('app.settings.account_parts.display')
                </div>
            </div>
            <!-- panel-body -->
            <div class="panel-footer">
                <div class="btn-group mr5">
                    <button type="submit" class="btn btn-primary">Update Account Settings</button> <button type="reset" id="ndc_form_reset" class="btn btn-default">Reset</button>
                </div>
            </div>
        </div>
        {{ Form::close() }}
    </div>
</div>
@stop