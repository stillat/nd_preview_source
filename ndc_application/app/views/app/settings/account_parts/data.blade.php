<div class="tab-pane" id="accountDataSettings">
    <div class="form-group is-required">
        <div class="col-sm-10 col-sm-offset-2">
            <div class="ckbox ckbox-default">
                {{ Form::checkbox('use_districts', 1, Input::old('use_districts', $accountSettings->use_districts), ['id' => 'chkUseDistricts']) }}
                <label for="chkUseDistricts">Use Districts</label>
            </div>
        </div>
        <div class="col-sm-10 col-sm-offset-2">
            <div class="ckbox ckbox-default">
                {{ Form::checkbox('show_employee_overtime', 1, Input::old('show_employee_overtime', $accountSettings->show_employee_overtime), ['id' => 'chkShowOvertime']) }}
                <label for="chkShowOvertime">Show Employee Overtime</label>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-3 control-label" for="data_decimal_places">Decimal Places</label>
            <div class="col-sm-2">
                <input type="number" min="0" max="6" class="form-control text-right" name="data_decimal_places" id="data_decimal_places" value="{{{ Input::old('data_decimal_places', $accountSettings->data_decimal_places) }}}"/>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-3 control-label" for="default_invoice_status">Default Invoice Status</label>
            <div class="col-sm-2">
                {{ Form::select('default_invoice_status', $transactionStatuses, Input::old('default_invoice_status', $accountSettings->invoice_default_status), ['class' => 'form-control']) }}
            </div>
        </div>
    </div>
</div>
