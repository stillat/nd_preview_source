<div class="tab-pane" id="accountDisplaySettings">
    <div class="form-group">
        <label class="col-sm-3 control-label" for="text_transformation">Text Transformation</label>
        <div class="col-sm-6">
            {{ Form::select('text_transformation', $validTransformations, Input::old('text_transformation', $formattingSettings->transform->transform), ['class' => 'form-control']) }}
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-3 control-label" for="employee_format">Employee Display Format</label>
        <div class="col-sm-2">
            {{ Form::select('employee_format', $validDisplaySettings, Input::old('employee_format', $formattingSettings->employee->format), ['class' => 'form-control']) }}
        </div>
        <label class="col-sm-3 control-label" for="fuel_format">Fuel Display Format</label>
        <div class="col-sm-2">
            {{ Form::select('fuel_format', $validDisplaySettings, Input::old('fuel_format', $formattingSettings->fuel->format), ['class' => 'form-control']) }}
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-3 control-label" for="material_format">Material Display Format</label>
        <div class="col-sm-2">
            {{ Form::select('material_format', $validDisplaySettings, Input::old('material_format', $formattingSettings->material->format), ['class' => 'form-control']) }}
        </div>
        <label class="col-sm-3 control-label" for="activity_format">Activity Display Format</label>
        <div class="col-sm-2">
            {{ Form::select('activity_format', $validDisplaySettings, Input::old('activity_format', $formattingSettings->activity->format), ['class' => 'form-control']) }}
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-3 control-label" for="department_format">Department Display Format</label>
        <div class="col-sm-2">
            {{ Form::select('department_format', $validDisplaySettings, Input::old('department_format', $formattingSettings->department->format), ['class' => 'form-control']) }}
        </div>
        <label class="col-sm-3 control-label" for="district_format">District Display Format</label>
        <div class="col-sm-2">
            {{ Form::select('district_format', $validDisplaySettings, Input::old('district_format', $formattingSettings->district->format), ['class' => 'form-control']) }}
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-3 control-label" for="project_format">Project Display Format</label>
        <div class="col-sm-2">
            {{ Form::select('project_format', $validDisplaySettings, Input::old('project_format', $formattingSettings->project->format), ['class' => 'form-control']) }}
        </div>
        <label class="col-sm-3 control-label" for="property_format">Property Display Format</label>
        <div class="col-sm-2">
            {{ Form::select('property_format', $validDisplaySettings, Input::old('property_format', $formattingSettings->property->format), ['class' => 'form-control']) }}
        </div>
    </div>
</div>