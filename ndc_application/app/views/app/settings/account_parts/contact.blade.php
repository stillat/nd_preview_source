<div class="tab-pane active" id="accountContactInformation">
    <div class="form-group">
        <div class="col-sm-10 col-sm-offset-2">
            <p class="form-control-static"><i class="fa fa-info-circle"></i> Make sure to keep your account settings and contact information up to date.</p>
        </div>
    </div>
    <div class="form-group is-required">
        <label class="col-sm-2 control-label" for="address_line_1">Address Line 1:</label>
        <div class="col-sm-6">
            <input type="text" id="address_line_1" name="address_line_1" class="form-control mousetrap {{{ user_form_size() }}}" value="{{{ Input::old('address_line_1', $currentAccount->address_line_1) }}}">
            {{ Form::errorMsg('address_line_1') }}
        </div>
    </div>
    <div class="form-group is-required">
        <label class="col-sm-2 control-label" for="address_line_2">Address Line 2:</label>
        <div class="col-sm-6">
            <input type="text" id="address_line_2" name="address_line_2" class="form-control mousetrap {{{ user_form_size() }}}" value="{{{ Input::old('address_line_2', $currentAccount->address_line_2) }}}">
            {{ Form::errorMsg('address_line_2') }}
        </div>
    </div>
    <div class="form-group is-required">
        <label class="col-sm-2 control-label" for="city">City:</label>
        <div class="col-sm-4">
            <input type="text" id="city" name="city" class="form-control mousetrap {{{ user_form_size() }}}" value="{{{ Input::old('city', $currentAccount->city) }}}">
            {{ Form::errorMsg('city') }}
        </div>
    </div>
    <div class="form-group is-required">
        <label class="col-sm-2 control-label" for="state">State:</label>
        <div class="col-sm-4">
            <input type="text" id="state" name="state" class="form-control mousetrap {{{ user_form_size() }}}" value="{{{ Input::old('state', $currentAccount->state) }}}">
            {{ Form::errorMsg('state') }}
        </div>
    </div>
    <div class="form-group is-required">
        <label class="col-sm-2 control-label" for="country">Country:</label>
        <div class="col-sm-4">
            <input type="text" id="country" name="country" class="form-control mousetrap {{{ user_form_size() }}}" value="{{{ Input::old('country', $currentAccount->country) }}}">
            {{ Form::errorMsg('country') }}
        </div>
    </div>
    <div class="form-group is-required">
        <label class="col-sm-2 control-label" for="zip">Zip:</label>
        <div class="col-sm-2">
            <input type="text" id="zip" name="zip" class="form-control mousetrap {{{ user_form_size() }}}" value="{{{ Input::old('zip', $currentAccount->zip) }}}">
            {{ Form::errorMsg('zip') }}
        </div>
    </div>
</div>