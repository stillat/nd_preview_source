@extends('app.consumables.update_base')

<?php $updateOKText = 'Update this fuel'; ?>

@section('formOpen')
    {{ Form::open(array('route' => array('fuels.update', $consumable->id), 'method' => 'put', 'class' => 'form-horizontal')) }}
@stop

@section('buttonBar')
    <div class="btn-group n-mt-5">
        {{ HTML::modelNaviateToolbar('fuels.edit', $consumable->previousFuel(), 'fuels.index', 'fuels.edit', $consumable->nextFuel()) }}
    </div>
    <div class="btn-group n-mt-5">
        <a href="{{ Redirector::getRoute(route('fuels.index')) }}" class="btn btn-danger ndc_form_leave">Cancel</a>
    </div>
@stop