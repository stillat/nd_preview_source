<div class="table-responsive">
    <table class="table table-hover table-striped mb30 table-fixed">
        <thead>
            <tr>
                <th class="wd-200">Code</th>
                <th class="wd-200">Name</th>
                <th class="text-right wd-100">Cost</th>
                <th class="text-right wd-100">Unit</th>
                <th>Description</th>
                @if($fuel->tracking_inventory)
                    <th class="text-right hidden-xs hidden-sm wd-200">Tracking Inventory</th>
                    <th class="text-right wd-200">Inventory Level</th>
                    <th><small>/</small> Status</th>
                @endif
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>
                    <strong>{{{ limit_code($fuel->code)  }}}</strong>
                </td>
                <td>
                    {{{ limit_name($fuel->name) }}}
                </td>
                <td class="text-right">
                    {{{ nd_number_format($fuel->cost) }}}
                </td>
                <td class="text-right">
                    {{{ limit_name($unit->name) }}}
                </td>
                <td>
                    <p class="read-more">{{{ limit_description(if_null_then_na($fuel->description)) }}}</p>
                </td>
                @if($fuel->tracking_inventory)
                    <td class="text-right hidden-xs hidden-sm">{{ bool_to_icon($fuel->tracking_inventory) }}</td>
                    <td class="inventory_row text-right">{{{ nd_number_format($fuel->current_inventory_level) }}}</td>
                    <td class="inventory_row">{{ inventory_status($fuel->current_inventory_level, $fuel->desired_inventory_level, $fuel->critical_inventory_level) }}</td>
                @endif
            </tr>
        </tbody>
    </table>
</div>