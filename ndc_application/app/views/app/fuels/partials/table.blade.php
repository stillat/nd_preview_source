<div class="table-responsive">
    <table class="table table-hover table-striped mb30 table-fixed">
        <thead>
        <tr>
            @if(!isset($hideFeatures))
                <th class="hidden-print" style="width:46px;">
                    <div class="ckbox ckbox-primary">
                        <input type="checkbox" id="batchToggleRecords">
                        <label for="batchToggleRecords"></label>
                    </div>
                </th>
            @endif
            <th class="wd-200">Code</th>
            <th class="wd-200">Name</th>
            <th class="text-right wd-100">Cost</th>
            <th class="text-right wd-100">Unit</th>
            <th class="hidden-xs hidden-sm">Description</th>
            @if($inventorySettings->show_inventory_management_in_consumables)
                <th class="text-right hidden-xs hidden-sm wd-200">Tracking Inventory</th>
                <th class="text-right wd-200">Inventory Level</th>
                <th>
                    <small>/</small>
                    Status
                </th>
            @endif
            <th class="wd-100 hidden-print text-right">
                <a href="{{{ action('App\Search\SortingController@getFuels') }}}" data-toggle="modal"
                   data-target="#modalHousing"><i class="fa fa-sort"></i>
                    <small>Sorting</small>
                </a>
            </th>
        </tr>
        </thead>
        <tbody>
        @foreach($fuels as $fuel)
            @if ($fuel->deleted_at == null)
                <tr>
                    @if(!isset($hideFeatures))
                        <td class="hidden-print">
                            <div class="ckbox ckbox-primary">
                                <input type="checkbox" class="rm_multi" id="remCheck{{{ $fuel->id }}}"
                                       data-record="{{{ $fuel->id }}}">
                                <label for="remCheck{{{ $fuel->id }}}"></label>
                            </div>
                        </td>
                    @endif
                    <td>
                        <a href="{{ route('fuels.show', array($fuel->id)) }}"><strong>{{{ limit_code($fuel->code) }}}</strong></a>
                    </td>
                    <td>
                        <a href="{{ route('fuels.show', array($fuel->id)) }}">{{{ limit_name($fuel->name) }}}</a>
                    </td>
                    <td class="text-right">
                        <a href="{{ route('fuels.show', array($fuel->id)) }}">{{{ nd_number_format($fuel->cost) }}}</a>
                    </td>
                    <td class="text-right">
                        <a href="{{ route('fuels.show', array($fuel->id)) }}">{{{ $units[$fuel->unit_id] }}}</a>
                    </td>
                    <td class="hidden-xs hidden-sm">
                        <a href="{{ route('fuels.show', array($fuel->id)) }}">{{{ if_null_then_na(limit_description($fuel->description)) }}}</a>
                    </td>
                    @if($inventorySettings->show_inventory_management_in_consumables)
                        <td class="text-right hidden-xs hidden-sm"><a href="{{ route('fuels.show', array($fuel->id)) }}">{{ bool_to_icon($fuel->tracking_inventory) }}</a></td>
                        @if($fuel->tracking_inventory)
                            <td class="inventory_row"><a href="{{ route('fuels.show', array($fuel->id)) }}" class="text-right">{{{ nd_number_format($fuel->current_inventory_level) }}}</a></td>
                            <td class="inventory_row"><a href="{{ route('fuels.show', array($fuel->id)) }}">{{ inventory_status($fuel->current_inventory_level, $fuel->desired_inventory_level, $fuel->critical_inventory_level) }}</a></td>
                        @else
                            <td colspan="2"></td>
                        @endif
                    @endif
                    <td class="table-action td-ignore">
                        <a href="{{ route('fuels.edit', array($fuel->id)) }}"><i class="fa fa-pencil"></i></a>
                        <a class="cursor-pointer delete-row" data-rcontext="{{{ $fuel->id }}}"><i class="fa fa-trash-o"></i></a>
                    </td>
                </tr>
            @else
                <tr>
                    @if(!isset($hideFeatures))
                        <td class="hidden-print">
                            <div class="ckbox ckbox-primary">
                                <input type="checkbox" class="rm_multi" id="remCheck{{{ $fuel->id }}}"
                                       data-record="{{{ $fuel->id }}}">
                                <label for="remCheck{{{ $fuel->id }}}"></label>
                            </div>
                        </td>
                    @endif
                    <td>
                        <strong>{{{ limit_code($fuel->code) }}}</strong>
                    </td>
                    <td>
                        {{{ limit_name($fuel->name) }}}
                    </td>
                    <td class="text-right">
                        {{{ nd_number_format($fuel->cost) }}}
                    </td>
                    <td class="text-right">
                        {{{ $units[$fuel->unit_id] }}}
                    </td>
                    <td class="hidden-xs hidden-sm">
                        {{{ if_null_then_na(limit_description($fuel->description)) }}}
                    </td>
                    @if($inventorySettings->show_inventory_management_in_consumables)
                        <td class="text-right hidden-xs hidden-sm">{{ bool_to_icon($fuel->tracking_inventory) }}</td>
                        @if($fuel->tracking_inventory)
                            <td class="inventory_row"><a class="text-right">{{{ nd_number_format($fuel->current_inventory_level) }}}</a></td>
                            <td class="inventory_row">{{ inventory_status($fuel->current_inventory_level, $fuel->desired_inventory_level, $fuel->critical_inventory_level) }}</td>
                        @else
                            <td colspan="2"></td>
                        @endif
                    @endif
                    <td class="table-action td-ignore">
                        <a href="{{ action('App\UnTrashController@getFuels') }}?d={{ $fuel->id }}" class="tooltips" data-toggle="tooltip" data-placement="left" title="Restoring will un-delete the fuel">Restore</a>
                    </td>
                </tr>
            @endif
        @endforeach
        </tbody>
    </table>
</div>