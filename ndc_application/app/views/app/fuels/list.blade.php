@extends('layouts.master')

@section('pageTitle')
<i class="ndc-fuel"></i> Fuels
@if(Input::get('search') == 1 && $foundResults)
	<span>Search Results</span>
@endif
@stop

@section('buttonBar')
	@if(Input::get('search') == 1 && $foundResults)
		<div class="btn-group pull-up-10">
			<a href="{{ action('App\Fuels\FuelController@index') }}" class="btn btn-white"><i class="fa fa-th-list inline-icon"></i></a>
		</div>
	@endif
	<div class="btn-group pull-up-10">
	<a href="{{ action('App\Search\SearchController@getFuelsSearchDialog') }}" data-toggle="modal" data-target="#modalHousing" class="btn btn-white"><i class="fa fa-search inline-icon"></i> Search Fuels</a>
    @include('partials.generic_export', ['export_data' => 'res_fuels', 'title' => 'fuels', 'margin' => '50px'])
    <a href="{{ action('App\Fuels\FuelController@create') }}" class="btn btn-white nd-add"><i class="fa fa-plus inline-icon"></i> New Fuel</a>
</div>
@stop

@section('content')

	<div class="row hidden-print">
		<div class="col-sm-12">
			{{ $fuels->appends(array_except(Input::query(), Paginator::getPageName()))->links() }}
		</div>
	</div>

	<div class="row">
		<div class="col-sm-12">
			@include('app.fuels.partials.table')
		</div>
	</div>

	<div class="row hidden-print">
		<div class="col-sm-12">
			{{ $fuels->appends(array_except(Input::query(), Paginator::getPageName()))->links() }}
		</div>
		@include('layouts.list_management')
	</div>


@include('layouts.dialogs.remove', array('title' => 'Remove Fuel', 'action' => 'App\Fuels\FuelController@destroy'))
@stop

@section('additionalScripts')
	<script>var NDC_LM = 'fuel';</script>
	<script src="{{{ app_url() }}}/assets/js/list_management.min.js{{{ UI::getBuster() }}}" defer></script>
@stop