@extends('layouts.master')

@section('pageTitle')
    <i class="ndc-fuel"></i> {{{ limit_code($fuel->code) }}} -
    <small>{{{ limit_name($fuel->name) }}}</small>
@stop

@section('buttonBar')
    <div class="btn-group n-mt-5">
        {{ HTML::modelNaviateToolbar('fuels.show', $fuel->previousFuel(), 'fuels.index', 'fuels.show', $fuel->nextFuel()) }}
    </div>
    <div class="btn-group n-mt-5">
        <a href="{{ route('fuels.edit', array($fuel->id)) }}" class="btn btn-white"><i
                    class="fa fa-edit inline-icon"></i> Update fuel</a>
    </div>
@stop

@section('content')
    <div class="row">
        <div class="col-md-12">
            @include('app.fuels.partials.detail_table')
        </div>
    </div>
    <ul class="nav nav-tabs">
        @if($fuel->tracking_inventory)
            <li class="active"><a href="#inventoryManager" data-toggle="tab"><i class="fa ndcf-weight-scale"></i>
                    <strong>Inventory Management</strong></a></li>
        @endif
    </ul>
    <div class="tab-content mb30">
        @if($fuel->tracking_inventory)
            <div class="tab-pane active" id="inventoryManager">
                @include('app.consumables.inventory_detail', ['consumable' => &$fuel])
            </div>
        @endif
    </div>
@stop