<?php
    $equipmentBreakdownTotalCost = 0;
    $equipmentBreakdownTotalQuantity = 0;
    ?>
<div class="{{{ $breakdownClass }}} width-adjustable">
    <table class="table table-hover breakdown-table">
        <thead>
            <tr class="header-row">
                <th>Equipment Unit</th>
                <th>Odometer Reading</th>
                <th class="text-right">Quantity</th>
                @if($settings->show_equipment_unit_rate)
                <th class="text-right">Unit Cost</th>
                <th class="wd-100">Unit</th>
                @endif
                <th class="text-right">Cost</th>
            </tr>
        </thead>
        <tbody>
            @foreach($equipmentUnits as $equipmentUnit)
            <?php
                $equipmentBreakdownTotalCost += $equipmentUnit->total_cost;
                $equipmentBreakdownTotalQuantity += $equipmentUnit->total_quantity;
                ?>
            <tr class="breakdown-row">
                <td>{{{ Formatter::equipment($equipmentUnit->equipment) }}}</td>
                @if($equipmentUnit->equipment->odometer != null)
                <td>{{{ $equipmentUnit->equipment->odometer->odometer_reading }}}</td>
                @else
                <td></td>
                @endif
                <td class="text-right"><span data-type="numeric">{{{ $equipmentUnit->total_quantity }}}</span></td>

                @if($settings->show_equipment_unit_rate)
                <td class="text-right"><span data-type="numeric">{{{ $equipmentUnit->historic_cost }}}</span></td>
                <td>{{{ $reader->getMeasurementUnit($equipmentUnit->equipment->unit_id)->name }}}</td>
                @endif
                <td class="text-right"><span data-type="numeric">{{{ $equipmentUnit->total_cost }}}</span></td>
            </tr>
            @if(count($equipmentUnit->fuels) > 0)
            @if($settings->show_fuel_breakdown)
            <tr data-type="breakdown" class="breakdown-row" data-breakdown="fuel">
            @else
            <tr data-type="breakdown" class="breakdown-row" data-breakdown="fuel" style="display: none;">
            @endif
                <td colspan="{{{ $equipmentColumnCount }}}">
                    @include('app.reporting.breakdown.fuels', ['fuels' => &$equipmentUnit->fuels])
                </td>
            </tr>
            @endif
            @endforeach
            @if($settings->show_equipment_unit_breakdown_total)
            <tr class="breakdown-row">
                <th>Equipment Breakdown Totals</th>
                <th class="text-right" colspan="2"><span data-type="numeric">{{{ $equipmentBreakdownTotalQuantity }}}</span></th>
                @if($settings->show_equipment_unit_rate)
                <th colspan="1"></th>
                @endif
                <th class="text-right"><span data-type="numeric">{{{ $equipmentBreakdownTotalCost }}}</span></th>
            </tr>
            @endif
        </tbody>
    </table>
</div>
<?php
    $equipmentBreakdownTotalCost = 0;
    $equipmentBreakdownTotalQuantity = 0;

    unset($equipmentBreakdownTotalCost);
    unset($equipmentBreakdownTotalQuantity);
    ?>