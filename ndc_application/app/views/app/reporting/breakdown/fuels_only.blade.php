<?php
$fuelBreakdownTotalCost = 0;
$fuelBreakdownTotalQuantity = 0;
?>
<div class="{{{ $breakdownClass }}} width-adjustable">
    <table class="table table-hover breakdown-table">
        <thead>
        <tr class="header-row">
            <th>Fuels</th>
            <th class="text-right">Quantity</th>
            <th class="text-right">Unit Cost</th>
            <th>Unit</th>
            <th class="text-right">Cost</th>
        </tr>
        </thead>
        <tbody>
        @foreach($record->getGroupedFuels() as $fuel)
            <?php
            $fuelBreakdownTotalCost += $fuel->total_cost;
            $fuelBreakdownTotalQuantity += $fuel->total_quantity;
            ?>
            <tr class="breakdown-row">
                <td>{{{ Formatter::fuel($fuel->fuel) }}}</td>
                <td class="text-right"><span data-type="numeric">{{{ $fuel->total_quantity }}}</span></td>
                <td class="text-right"><span data-type="numeric">{{{ $fuel->historic_cost }}}</span></td>
                <td>{{{ $reader->getMeasurementUnit($fuel->fuel->unit_id)->name }}}</td>
                <td class="text-right"><span data-type="numeric">{{{ $fuel->total_cost }}}</span></td>
            </tr>
        @endforeach
        @if($settings->show_fuel_breakdown_total)
            <tr class="breakdown-row"  data-breakdown="fuel">
                <th>Fuel Breakdown Totals</th>
                <th class="text-right"><span data-type="numeric">{{{ $fuelBreakdownTotalQuantity }}}</span></th>
                <th colspan="2"></th>
                <th class="text-right"><span data-type="numeric">{{{ $fuelBreakdownTotalCost }}}</span></th>
            </tr>
        @endif
        </tbody>
    </table>
</div>
<?php
$fuelBreakdownTotalCost = 0;
$fuelBreakdownTotalQuantity = 0;

unset($fuelBreakdownTotalCost);
unset($fuelBreakdownTotalQuantity);
?>