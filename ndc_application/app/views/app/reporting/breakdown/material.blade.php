<?php
    $breakdownTotalCost = 0;
    $breakdownTotalQuantity = 0;
?>
<div class="{{{ $breakdownClass }}} width-adjustable">
<table class="table table-hover breakdown-table">
    <thead>
        <tr class="header-row">
            <th>Material</th>
            <th class="text-right">Quantity</th>
            <th class="text-right">Unit Cost</th>
            <th class="wd-100">Unit</th>
            <th class="text-right">Cost</th>
        </tr>
    </thead>
    <tbody>
        @foreach($materials as $mat)
        <?php
            $breakdownTotalCost += $mat->total_cost;
            $breakdownTotalQuantity += $mat->total_quantity;
        ?>
        <tr class="breakdown-row">
            <td>{{{ Formatter::material($mat->material) }}}</td>
            <td class="text-right"><span data-type="numeric">{{{ $mat->total_quantity }}}</span></td>
            <td class="text-right"><span data-type="numeric">{{{ $mat->historic_cost }}}</span></td>
            <td>{{{ $reader->getMeasurementUnit($mat->material->unit_id)->name }}}</td>
            <td class="text-right"><span data-type="numeric">{{{ $mat->total_cost }}}</span></td>
        </tr>
        @endforeach
        @if ($settings->show_material_breakdown_total)
        <tr class="breakdown-row">
            <th>Breakdown Totals</th>
            <th class="text-right"><span data-type="numeric">{{{ $breakdownTotalQuantity }}}</span></th>
            <th colspan="2"></th>
            <th class="text-right"><span data-type="numeric">{{{ $breakdownTotalCost }}}</span></th>
        </tr>
        @endif
    </tbody>
</table>
</div>
<?php
    $breakdownTotalCost = 0;
    $breakdownTotalQuantity = 0;
    unset($breakdownTotalCost);
    unset($breakdownTotalQuantity);
?>