@extends('layouts.master')
@section('pageTitle')
<i class="ndc-chart-pie"></i> <strong>{{{ $currentAccount->account_name }}}</strong> - <span id="reportNotice" style="font-size: inherit;"></span>
@stop
@section('buttonBar')
<div class="reportRunnerHolder" style="display: none;">
    <div class="pull-right">
        <a href="#" style="margin-left: 8px;" data-content="Every report needs to have a date range entered. Select a date range from the various options available by activiating the drop down to the right." data-title="Report Date Range" data-placement="bottom" data-toggle="popover" data-container="body" class="popovers cursor-pointer" data-original-title="" title=""><i class="fa fa-question-circle inline-icon"></i></a>
        <div id="daterange" class="btn btn-white n-mt-5"><i class="fa fa-calendar-o inline-icon" style="margin-right:10px;"></i> <span> September 10, 2014 - September 24, 2014</span> <b class="caret"></b></div>
    </div>
</div>
@stop
@section('additionalStyles')
<style>
    td {vertical-align: middle !important;}
</style>
@stop
@section('content')
<div id="createFormLoader">
    <div class="spinner"></div>
    <p style="margin-right:auto;margin-left: auto; width: 300px; text-align: center;">Getting the reporting system ready. Please wait.</p>
</div>
<div class="reportRunnerHolder" style="display:none;">
    {{ Form::open(['action' => 'App\Reporting\ReportsController@postRun', 'method' => 'POST', 'id' => 'reportRunnerForm']) }}
    <input type="hidden" name="report_mdef" id="report_mdef" value="0" />
    <input type="hidden" name="report_start_date" id="report_start_date" value="{{ $startDate }}" />
    <input type="hidden" name="report_end_date" id="report_end_date" value="{{ $endDate }}" />
    <input type="hidden" id="internalMode" value="0" name="internal_reporting_mode">
    <input type="hidden" id="c_iterate_on" value="0" name="c_iterate_on">

    <!--
    <div class="row" id="srlReportDescriptionHolder">
        <div class="col-sm-12">
            <div class="panel panel-default">
                <div class="panel-body nopadding" style="padding: 10px !important;">
                    <p id="srlReportDescriptionText"></p>
                    <div class="alert alert-warning" id="srlNotificationPermanent">
                        <p id="srlNotificationMessage"></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    -->

    <div class="row">
        <div class="col-sm-4" id="reportSelector">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title"><i class="glyphicon glyphicon-stats"></i> Available Reports</h3>
                </div>
                <div class="panel-body nopadding" style="overflow-y: auto;overflow-x: hidden;max-height: 500px;">
                    <div id="tree"></div>
                </div>
            </div>
        </div>
        <div class="col-sm-8" id="reportInstructions">
            <div class="" style="max-height: 500px;min-height: 400px;padding-top: 75px;vertical-align: middle;">
                <h2><i class="glyphicon glyphicon-stats"></i> Let's run a report</h2>
                <ol style="font-size: 21px; margin: 20px;">
                    <li>First, select a date range from the date ranger picker. This is located in the top right corner of the screen</li>
                    <li>Then, select a report from the "Available Reports" list. This is located on the left side of the page.</li>
                </ol>
                <p class="lead">After selecting a report, this message will disappear and the reporting options will take its place.</p>
                <p>Here are some helpful hints for getting the most out of the reporting engine:</p>
                <ul>
                    <li>Select a report before applying any filters or settings. Settings are reset when a different report is selected</li>
                    <li>Some reports change the default reporting settings. A message will appear above the date picker when this happens</li>
                    <li>Have incorrect totals? The equations used can be changed from the "Override Settings" tab when the report settings appear</li>
                    <li>All settings can be applied to almost any report. Any setting that is not disabled on a report can be used</li>
                    <li>Reports are ordered according to the work records sorting methods (when it is possible to do so)</li>
                </ul>
            </div>
        </div>
        <div class="col-sm-8" id="reportOptions">
            <div class="panel panel-default">
                <ul class="nav nav-tabs">
                    <li class="active" id="primaryFilterTab"><a href="#rFilter" data-toggle="tab"><strong><i class="fa fa-filter"></i> Report Filters</strong></a></li>
                    <li id="groupingTab"><a href="#rOrg" data-toggle="tab" data-mode="advanced"><strong><i class="fa fa-th-list"></i> Grouping Method</strong></a></li>
                    <li id="overrideTab"><a href="#override" data-toggle="tab" data-mode="advanced"><strong><i class="fa fa-cogs"></i> Override Settings</strong></a></li>
                    <li><a href="#notes" data-toggle="tab" data-mode="advanced"><strong><i class="fa fa-quote-right"></i> Report Notes</strong></a></li>
                </ul>
                <div class="tab-content mb30 nopadding">
                    <div class="tab-pane active nopadding" id="rFilter">
                        <table class="table table-hover table-bordered">
                            <thead>
                            <tr>
                                <th class="wd-50"><a href="#" style="margin-left: 8px;" data-content="Report filter modes allow you to control how the filter is included in the report. Check the box to have only the specified filters included. Un-check the box to have everything but the specified filters included." data-title="Report Filter Modes" data-placement="right" data-toggle="popover" data-container="body" class="popovers cursor-pointer" data-original-title="" title=""><i class="fa fa-question-circle"></i></a></th>
                                <th class="wd-100">Filter</th>
                                <th>Value</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td>
                                    <div class="ckbox ckbox-default">
                                        <input type="checkbox" value="1" id="activityFilterMode" name="activityMode" checked="checked">
                                        <label for="activityFilterMode"></label>
                                    </div>
                                </td>
                                <td><a href="#" id="activityAll"><strong>Activity</strong></a></td>
                                <td>{{ Form::select('activity_filters[]', $activities, null, ['class' => 'chosen-select', 'id' => 'activityFilter', 'multiple' => 'multiple', 'data-placeholder' => 'Select your activity filters']) }}</td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="ckbox ckbox-default">
                                        <input type="checkbox" value="1" id="departmentFilterMode" name="departmentMode" checked="checked">
                                        <label for="departmentFilterMode"></label>
                                    </div>
                                </td>
                                <td><a href="#" id="departmentAll"><strong>Department</strong></a></td>
                                <td>{{ Form::select('department_filters[]', $departments, null, ['class' => 'chosen-select', 'id' => 'departmentFilter', 'multiple' => 'multiple', 'data-placeholder' => 'Select your department filters']) }}</td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="ckbox ckbox-default">
                                        <input type="checkbox" value="1" id="districtFilterMode" name="districtMode" checked="checked">
                                        <label for="districtFilterMode"></label>
                                    </div>
                                </td>
                                <td><a href="#" id="districtAll"><strong>District</strong></a></td>
                                <td>{{ Form::select('district_fiilters[]', $districts, null, ['class' => 'chosen-select', 'id' => 'districtFilter', 'multiple' => 'multiple', 'data-placeholder' => 'Select your district filters']) }}</td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="ckbox ckbox-default">
                                        <input type="checkbox" value="1" id="employeeFilterMode" name="employeeMode" checked="checked">
                                        <label for="employeeFilterMode"></label>
                                    </div>
                                </td>
                                <td><a href="#" id="employeeAll"><strong>Employee</strong></a></td>
                                <td>{{ Form::select('employee_filters[]', $employees, null, ['class' => 'chosen-select', 'id' => 'employeeFilter', 'multiple' => 'multiple', 'data-placeholder' => 'Select your employee filters']) }}</td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="ckbox ckbox-default">
                                        <input type="checkbox" value="1" id="equipmentUnitFilterMode" name="equipmentMode" checked="checked">
                                        <label for="equipmentUnitFilterMode"></label>
                                    </div>
                                </td>
                                <td><a href="#" id="equipAll"><strong>Equipment Units</strong></a></td>
                                <td>{{ Form::select('equipment_filters[]', $equipments, null, ['class' => 'chosen-select', 'id' => 'equipmentFilter', 'multiple' => 'multiple', 'data-placeholder' => 'Select your equipment unit filters']) }}</td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="ckbox ckbox-default">
                                        <input type="checkbox" value="1" id="materialFilterMode" name="materialMode" checked="checked">
                                        <label for="materialFilterMode"></label>
                                    </div>
                                </td>
                                <td><a href="#" id="materialAll"><strong>Materials</strong></a></td>
                                <td>{{ Form::select('material_filters[]', $materials, null, ['class' => 'chosen-select', 'id' => 'materialFilter', 'multiple' => 'multiple', 'data-placeholder' => 'Select your material filters']) }}</td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="ckbox ckbox-default">
                                        <input type="checkbox" value="1" id="projectFilterMode" name="projectMode" checked="checked">
                                        <label for="projectFilterMode"></label>
                                    </div>
                                </td>
                                <td><a href="#" id="projectAll"><strong>Project</strong></a></td>
                                <td>{{ Form::select('project_filters[]', $projects, null, ['class' => 'chosen-select', 'id' => 'projectFilter', 'multiple' => 'multiple', 'data-placeholder' => 'Select your project filters']) }}</td>
                            </tr>
                            <tr>
                                <td colspan="2"><a href="#" id="propertyTypeAll"><strong>Limit Property Type</strong></a></td>
                                <td>{{ Form::select('property_type_filters[]', $propertyTypes, null, ['class' => 'chosen-select', 'id' => 'propertyTypeFilter', 'multiple' => 'multiple', 'data-placeholder' => 'Limit the property types']) }}</td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="ckbox ckbox-default">
                                        <input type="checkbox" value="1" id="propertyFilterMode" name="propertyMode" checked="checked">
                                        <label for="propertyFilterMode"></label>
                                    </div>
                                </td>
                                <td><a href="#" id="propertyAll"><strong>Property</strong></a></td>
                                <td>
                                    <select name="property_filters[]" id="propertyFilter" class="chosen-select" data-placeholder="Select your property filters" multiple="mulitple">
                                        @foreach($properties as $propertyID => $property)
                                            <option value="{{{ $propertyID }}}" data-type="{{{ $property[1] }}}">{{{ $property[0] }}}</option>
                                        @endforeach
                                    </select>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="tab-pane" id="rOrg">
                        <div class="row" style="padding: 10px;">
                            <div class="col-sm-12">
                                <fieldset style="margin-top: 10px;">
                                    <legend>Dynamic Grouping Methods</legend>
                                    <p style="padding: 10px;">Grouping methods are the primary way to change how the reports are generated and laid out when printed.</p>
                                    <div class="rdio rdio-default">
                                        <input type="radio" name="org_strategy" id="orgReportDefault" value="1" checked="checked">
                                        <label for="orgReportDefault">Report Default Grouping Method</label>
                                    </div>
                                    <div class="rdio rdio-primary">
                                        <input type="radio" name="org_strategy" id="orgGenericGroup" value="2">
                                        <label for="orgGenericGroup">Generic Grouping Grouping Method</label>
                                    </div>
                                    <div id="genericGroupingOptions" style="display: none; margin-bottom: 20px;">
                                        <div class="form-group" style="margin-top: 15px;">
                                            <label class="col-sm-3 control-label">Generic Grouping Filter</label>
                                            <div class="col-sm-5">
                                                <select name="generic_grouping_column" class="form-control n-mt-5" id="genericGroupingColumn">
                                                    <option value="activity_id">Activity</option>
                                                    <option value="department_id">Department</option>
                                                    <option value="district_id">District</option>
                                                    <option value="employee_id">Employee</option>
                                                    <option value="project_id">Project</option>
                                                    <option value="employee_id">Employee</option>
                                                    <option value="property_id">Property</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="rdio rdio-success mb20">
                                        <input type="radio" name="org_strategy" id="orgSpecialGroup" value="3">
                                        <label for="orgSpecialGroup">Special Grouping Strategy</label>
                                    </div>
                                    <div id="specialGroupingOptions" style="display: none;" class="mb10">
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label">Special Grouping Column</label>
                                            <div class="col-sm-5">
                                                <select name="special_grouping_column" class="form-control n-mt-5" id="specialGroupingColumn">
                                                    <option value="activity_id">Activity</option>
                                                    <option value="department_id">Department</option>
                                                    <option value="district_id">District</option>
                                                    <option value="employee_id">Employee</option>
                                                    <option value="project_id">Project</option>
                                                    <option value="property_id">Property</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label">Special Group Name</label>
                                            <div class="col-sm-5">
                                                <input type="text" name="special_grouping_name" id="specialGroupingName" class="form-control"/>
                                            </div>
                                        </div>
                                        <div class="form-group" style="margin-bottom:20px;">
                                            <label class="col-sm-3 control-label">Special Group Filters</label>
                                            <div class="col-sm-9">
                                                <select name="special_group_filters[]" class="form-control chosen-select" multiple="multiple" data-placeholder="Select your special grouping filters" id="specialSelectFilters"></select>
                                            </div>
                                        </div>
                                    </div>
                                </fieldset>
                                <fieldset>
                                    <legend>Record Combination Settings</legend>
                                    <p class="text-info" style="padding: 10px;">Combining records collapses multiple records into one report entry. For example, if report records are combined by activity, then any row containing "vacation" will be combined, and their totals added together.</p>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">Combine the report records by</label>
                                        <div class="col-sm-5">
                                            <select name="group_report_column" class="form-control n-mt-5" id="groupingReportColumn">
                                                <option value="none" selected="selected">Do not group this report</option>
                                                <option value="activity_id">Activity</option>
                                                <option value="department_id">Department</option>
                                                <option value="district_id">District</option>
                                                <option value="employee_id">Employee</option>
                                                <option value="project_id">Project</option>
                                                <option value="property_id">Property</option>
                                                <option value="consumable_id" id="consumableGroupingOption" disabled="disabled">Equipment, Fuel or Material</option>
                                            </select>
                                        </div>
                                    </div>
                                </fieldset>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="override">
                        <div class="row" style="padding: 10px;">
                            <div class="col-sm-12">
                                <p style="padding: 10px;">A select amount of settings can be overridden before the report is generated.</p>
                            </div>
                            <div class="col-sm-12">
                                <fieldset>
                                    <legend>Equations and Totals</legend>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">Report Total Equation</label>
                                        <div class="col-sm-5">
                                            {{ Form::select('equation', $equations, $defaultEquation, ['class' => 'form-control', 'id' => 'reportEquation']) }}
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">Report Total Block</label>
                                        <div class="col-sm-5">
                                            {{ Form::select('block', $blocks, $defaultBlock, ['class' => 'form-control', 'id' => 'reportBlock']) }}
                                        </div>
                                    </div>
                                </fieldset>
                                    <div id="moreOptions" style="margin-top: 20px;">
                                        <div class="panel-heading">
                                            <h3 class="panel-title">
                                                <a href="#moreOptionsHolder" data-toggle="collapse" data-parent="#moreOptions"><i class="fa fa-cogs"></i> Click to toggle more reporting options to override</a>
                                            </h3>
                                        </div>
                                        <div id="moreOptionsHolder" class="panel-collapse collapse">
                                            <div class="panel-body">
                                                @include('app.reporting.runner_parts.settings_override')
                                            </div>
                                        </div>
                                    </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="notes">
                        <div class="row" style="padding: 10px;">
                            <p style="padding: 10px;">Reporting notes will appear at the top of reports when they are available.</p>
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <textarea class="form-control autogrow" name="reportNotes" cols="30" rows="5"></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-8">
                </div>
                <div class="col-sm-4">
                    <button type="submit" class="btn btn-primary btn-block"><i class="ndc-chart-pie"></i> Run Report</button>
                </div>
            </div>
        </div>
    </div>
    {{ Form::close() }}
</div>
@stop
@section('additionalScripts')
<script>
    var reportListData = {{ $reportList }};
    var startDate = '{{ $startDate }}';
    var endDate = '{{ $endDate }}';
</script>
<script src="{{{ app_url() }}}/assets/js/dep/chosen.jquery.min.js" defer></script>
<script src="{{{ app_url() }}}/assets/js/reporting.min.js" defer></script>
@stop