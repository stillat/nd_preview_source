<?php
$disableAdditionalData = true;
?>
<div class="panel">
    <div class="table table-responsive">
        <table class="table table-hover">
            <thead>
            {{ $constructor->srlPrintHeader() }}
            </thead>
            <tbody>
            @foreach($records as $record)
                @include('app.reporting.features.individual_record')
            @endforeach
            </tbody>
        </table>
    </div>
</div>