@foreach($constructor->gatherOrganizationGroups($constructor->getOrganizationStrategy()['mode']) as $grouping)
<?php
    $organizationGroupRecords = [];
    foreach ($records as $record) {
        if ($record->objectGraphSearch($constructor->getOrganizationStrategy()['mode'], $grouping)) {

            if ($stateManager->observed('record_'.$record->getWorkID()) == false) {
                $organizationGroupRecords[] = $record;
                $stateManager->notify('record_'.$record->getWorkID());
            }
        }
    }
    ?>
@if (count($organizationGroupRecords) > 0)
<?php
    $groupingName = $constructor->getOrganizationStrategyGroupingName($grouping);
?>
@if ($groupingName !== null)
<div class="panel">
    <div class="panel-heading">
        <h3 class="panel-title">{{{ $constructor->getOrganizationStrategyName() }}}: {{{ $groupingName }}}</h3>
    </div>
    <div class="table table-responsive">
        <table class="table table-hover">
            <thead>
                {{ $constructor->srlPrintHeader() }}
            </thead>
            <tbody>
                @foreach($organizationGroupRecords as $record)
                    @include('app.reporting.features.individual_record')
                @endforeach
                {{ $constructor->prepareGroupedFooter($organizationGroupRecords[0]->rawData) }}
            </tbody>
        </table>
    </div>
</div>
@endif
@endif
<?php unset($organizationGroupRecords); ?>
@endforeach
<div class="panel panel-dark panel-alt">
    <div class="panel-heading" style="background-color: #4A4A4A;padding: 13px;border-radius: 0px;">
        <h3 class="panel-title">Grand Totals</h3>
    </div>
    <div class="table table-responsive">
        <table class="table table-hover" id="organizationGrandTotalsTable">
            <thead>
                {{ $constructor->srlPrintHeader() }}
            </thead>
            <tbody>
               {{ $constructor->srlPrintFooter() }}
            </tbody>
        </table>
    </div>
</div>