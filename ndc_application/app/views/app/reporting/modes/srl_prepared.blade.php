<?php

/*
 * Build the iterator data collection.
 */
$iteratorData = $stateManager->getFromStorage('preparation_bank')[$srlIterator];

?>

@if (count($iteratorData) == 0)
<?php ParadoxOne\NDCounties\UI\UIBuilder::info('The reporting engine did not return any records. Consider widening the search criteria or filters. You will be redirect back automatically in 5 seconds.', 'Report Not Generated'); ?>
<meta http-equiv="refresh" content="5;url="{{ $_SERVER['HTTP_REFERER'] }}"/>
@endif

<table class="table table-hover">
    <thead>
        {{ $constructor->srlPrintHeader() }}
    </thead>
    <tbody>
        @foreach ($iteratorData as $record)
        {{ $constructor->srlPrintBody([$record, $record]) }}
        @endforeach
        <tr class="table-footer">
            {{ $constructor->srlPrintFooter() }}
        </tr>
    </tbody>
</table>