<table class="table table-hover">
    <thead>
        {{ $constructor->srlPrintHeader() }}
    </thead>
    <tbody>
        @foreach ($records as $record)
            {{ $constructor->srlPrintBody([$record, null]) }}
        @endforeach
        <tr class="table-footer">
            {{ $constructor->srlPrintFooter() }}
        </tr>
    </tbody>
</table>