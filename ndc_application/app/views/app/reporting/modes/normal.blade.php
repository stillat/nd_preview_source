<table class="table table-hover">
    <thead>
        {{ $constructor->srlPrintHeader() }}
    </thead>
    <tbody>
        @foreach ($records as $record)
        <div class="avoid">
            @include('app.reporting.features.individual_record')
        </div>
        @endforeach
        <tr class="table-footer">
            {{ $constructor->srlPrintFooter() }}
        </tr>
    </tbody>
</table>