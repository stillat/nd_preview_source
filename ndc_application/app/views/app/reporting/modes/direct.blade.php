<table class="table table-hover">
    <thead>
    {{ $constructor->getDirectProvider()->printHeaders() }}
    </thead>
    <tbody>
    <div class="avoid">
        {{ $constructor->getDirectProvider()->printBody() }}
    </div>
    <tr class="table-footer">
        {{ $constructor->getDirectProvider()->printFooters() }}
    </tr>
    </tbody>
</table>