@extends('layouts.master')

@section('additionalStyles')
    <style>
        .contentpanel {padding: 0px;}td { vertical-align: middle !important;}.option-legend { width: 60%; border-bottom: 1px solid rgba(128, 128, 128, 0.32);padding-bottom: 10px;} #reportDescriptionHolder:empty {display:none;}
        .nav-tabs li.active > a, .nav-tabs > li.active {border: 0;background-color: rgba(199, 203, 226, 0.23) !important;}
        div:not(.chosen-container-active) > ul > li.search-field > input.default { width: 281px !important; }
    </style>
@stop

@section('roleSelector')
    @overwrite

@section('pageTitle')
    <div id="reportTitleHolder">
        @if($searchOnlyMode)
            <i class="fa fa-search"></i>
        @else
            <i class="ndc-chart-pie"></i>
        @endif
        <div class="text-muted" style="display: inline-block;">{{{ str_limit($currentAccount->account_name, 15)  }}}</div>
        @if($searchOnlyMode)
            - Search
        @else
            - Reporting <small><strong>&amp;</strong></small> Search
        @endif
    </div>
@stop

@section('headerExtra')
    @if(!$searchOnlyMode)
        <p class="text-muted" id="reportDescriptionHolder" style="margin: 10px 0px 0px 0px; padding: 0px 0px 0px;"></p>
    @endif
@stop

@section('buttonBar')
    <div id="daterange" class="btn btn-white n-mt-5"><i class="fa fa-calendar-o inline-icon"
                                                        style="margin-right:10px;"></i> <span> September 10, 2014 - September 24, 2014</span>
        <b class="caret"></b></div>
@stop

@section('leftMenu')
    @if (!$searchOnlyMode)
        {{ Menu::render('reportingMenu') }}
    @else
        @parent
    @endif
@overwrite

@section('content')

    <div id="createFormLoader">
        <div class="spinner"></div>
        <p style="margin-right:auto;margin-left: auto; width: 300px; text-align: center;">Getting things ready. Please
            wait.</p>
    </div>

    @include('app.reporting.runner_parts.selection_container')
    @include('app.reporting.runner_parts.dialogs.save')
    @include('app.reporting.runner_parts.dialogs.remove')
    @include('app.reporting.runner_parts.dialogs.error')
@stop

@section('additionalScripts')
    <script>
        var reportListData = {{ $reportList }};
        var startDate = '{{ $startDate }}';
        var endDate = '{{ $endDate }}';
        var customReportURL = '{{ action('App\Reporting\ReportsController@postSaveCustomReport') }}';
        var customUpdateURL = '{{ action('App\Reporting\ReportsController@postUpdateCustomReport') }}';
        var reportGenUrl = '{{ url('reporting/run') }}';
        var existenceURL = '{{ str_finish(action('App\Search\ExistenceController@getReport'), '/') }}';
    </script>

    <script src="{{{ app_url() }}}/assets/js/jsrender.min.js{{ UI::getBuster() }}" defer></script>
    <script src="{{{ app_url() }}}/assets/js/dep/chosen.jquery.min.js{{ UI::getBuster() }}" defer></script>
    <script src="{{{ app_url() }}}/assets/js/reporting.min.js{{ UI::getBuster() }}" defer></script>
@stop

@section('prependBody')
    {{ Form::open(['action' => 'App\Reporting\ReportsController@postRun', 'method' => 'POST', 'id' => 'reportRunnerForm']) }}
    <input type="hidden" name="report_mdef" id="report_mdef" value="0" />
    <input type="hidden" name="report_start_date" id="report_start_date" value="{{ $startDate }}" />
    <input type="hidden" name="report_end_date" id="report_end_date" value="{{ $endDate }}" />
    <input type="hidden" id="internalMode" value="0" name="internal_reporting_mode">
    <input type="hidden" id="c_iterate_on" value="0" name="c_iterate_on">
    <input type="hidden" value="1" id="equations_hidden_value" name="equations_hidden_value">
    <input type="hidden" value="1" id="grouping_hidden_value" name="grouping_hidden_value">
    <input type="hidden" value="0" id="report_new_window" name="report_new_window">
    <input type="hidden" value="" name="activity_filter_text" id="activityFilterText">
    <input type="hidden" value="" name="department_filter_text" id="departmentFilterText">
    <input type="hidden" value="" name="district_filter_text" id="districtFilterText">
    <input type="hidden" value="" name="employee_filter_text" id="employeeFilterText">
    <input type="hidden" value="" name="equipment_filter_text" id="equipmentFilterText">
    <input type="hidden" value="" name="material_filter_text" id="materialFilterText">
    <input type="hidden" value="" name="project_filter_text" id="projectFilterText">
    <input type="hidden" value="" name="propertytype_filter_text" id="propertytypeFilterText">
    <input type="hidden" value="" name="property_filter_text" id="propertyFilterText">
    <input type="hidden" value="" name="road_filter_text" id="roadFilterText">
    <input type="hidden" value="" name="combine_columns" id="combineColumns">
@stop

@section('appendBody')
    {{ Form::close() }}
@stop