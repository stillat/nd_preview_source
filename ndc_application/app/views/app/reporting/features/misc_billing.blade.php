@if($settings->always_show_billing_description || ($settings->only_show_descriptions_if_entered && strlen($record->getMiscBilling()->description) > 0))
@if($stateManager->observed('record_'.$record->getWorkID().'_billing_desc') == false)
<tr>
    <td colspan="{{ $columnCount }}">
        <div class="{{{ $breakdownClass }}} width-adjustable">
            <table class="table table-hover breakdown-table">
                <tbody>
                    <tr>
                        <td>Billing Description</td>
                        <td>
                        @if(strlen($record->getMiscBilling()->description) > 0)
                            {{{ $record->getMiscBilling()->description }}}
                        @else
                            &nbsp;
                        @endif
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </td>
</tr>
@endif
@endif