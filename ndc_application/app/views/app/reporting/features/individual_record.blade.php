{{ $constructor->srlPrintBody([$record->getRawData(), $record]) }}

@if($breakdownEnabled)
    @if($record->getProperty()->work_data != null)
        <?php
        $showPropertyBreakdown = show_property_breakdown($manager, $record);
        ?>
        @if(!isset($disableAdditionalData) && $showPropertyBreakdown)
            @if($settings->show_property_breakdown)
                <tr data-type="breakdown" data-breakdown="property">
            @else
                <tr data-type="breakdown" data-breakdown="property" style="display: none;">
                    @endif

                    @foreach($record->getProperty()->work_data as $adapter => $data)
                        <td colspan="{{ $columnCount }}">
                            <div class="{{{ $breakdownClass }}} width-adjustable">
                                <table class="table table-hover breakdown-table">
                                    {{ $manager->formatReportView($adapter, [$data, $record->getRawData(), $settings]) }}
                                </table>
                            </div>
                        </td>
                </tr>
                @endforeach
            @endif
        @endif
        @if (count($record->rawData->rawFuels) > 0)
            @if($settings->show_equipment_unit_breakdown == false && $settings->show_fuel_breakdown)
                <tr data-type="breakdown" data-breakdown="fuel-only">
            @else
                <tr data-type="breakdown" data-breakdown="fuel-only" style="display: none;">
                    @endif
                    <td colspan="{{ $columnCount }}">
                        <?php
                        $fuelBreakdownTotalCost = 0;
                        $fuelBreakdownTotalQuantity = 0;
                        ?>
                        <div class="{{{ $breakdownClass }}} width-adjustable">
                            <table class="table table-hover breakdown-table">
                                <thead>
                                <tr class="header-row">
                                    <th>Fuels</th>
                                    <th class="text-right">Quantity</th>
                                    <th class="text-right">Unit Cost</th>
                                    <th>Unit</th>
                                    <th class="text-right">Cost</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($record->getGroupedFuels() as $fuel)
                                    <?php
                                    $fuelBreakdownTotalCost += $fuel->total_cost;
                                    $fuelBreakdownTotalQuantity += $fuel->total_quantity;
                                    ?>
                                    <tr class="breakdown-row">
                                        <td>{{{ Formatter::fuel($fuel->fuel) }}}</td>
                                        <td class="text-right"><span
                                                    data-type="numeric">{{{ $fuel->total_quantity }}}</span></td>
                                        <td class="text-right"><span
                                                    data-type="numeric">{{{ $fuel->historic_cost }}}</span></td>
                                        <td>{{{ $reader->getMeasurementUnit($fuel->fuel->unit_id)->name }}}</td>
                                        <td class="text-right"><span
                                                    data-type="numeric">{{{ $fuel->total_cost }}}</span></td>
                                    </tr>
                                @endforeach
                                @if($settings->show_fuel_breakdown_total)
                                    <tr class="breakdown-row" data-breakdown="fuel">
                                        <th>Fuel Breakdown Totals</th>
                                        <th class="text-right"><span
                                                    data-type="numeric">{{{ $fuelBreakdownTotalQuantity }}}</span></th>
                                        <th colspan="2"></th>
                                        <th class="text-right"><span
                                                    data-type="numeric">{{{ $fuelBreakdownTotalCost }}}</span></th>
                                    </tr>
                                @endif
                                </tbody>
                            </table>
                        </div>
                        <?php
                        $fuelBreakdownTotalCost = 0;
                        $fuelBreakdownTotalQuantity = 0;

                        unset($fuelBreakdownTotalCost);
                        unset($fuelBreakdownTotalQuantity);
                        ?>
                    </td>
                </tr>
            @endif
            @if ($record->rawData->total_materials_quantity > 0 && count($record->getMaterials()) > 0)
                @if(!isset($disableAdditionalData))
                    @if($settings->show_material_breakdown)
                        <tr data-type="breakdown" data-breakdown="material">
                    @else
                        <tr data-type="breakdown" data-breakdown="material" style="display: none;">
                            @endif
                            <td colspan="{{ $columnCount }}">
                                <?php
                                $breakdownTotalCost = 0;
                                $breakdownTotalQuantity = 0;
                                ?>
                                <div class="{{{ $breakdownClass }}} width-adjustable">
                                    <table class="table table-hover breakdown-table">
                                        <thead>
                                        <tr class="header-row">
                                            <th>Material</th>
                                            <th class="text-right">Quantity</th>
                                            <th class="text-right">Unit Cost</th>
                                            <th class="wd-100">Unit</th>
                                            <th class="text-right">Cost</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($record->getMaterials() as $mat)
                                            <?php
                                            $breakdownTotalCost += $mat->total_cost;
                                            $breakdownTotalQuantity += $mat->total_quantity;
                                            ?>
                                            <tr class="breakdown-row">
                                                <td>{{{ Formatter::material($mat->material) }}}</td>
                                                <td class="text-right"><span
                                                            data-type="numeric">{{{ $mat->total_quantity }}}</span></td>
                                                <td class="text-right"><span
                                                            data-type="numeric">{{{ $mat->historic_cost }}}</span></td>
                                                <td>{{{ $reader->getMeasurementUnit($mat->material->unit_id)->name }}}</td>
                                                <td class="text-right"><span
                                                            data-type="numeric">{{{ $mat->total_cost }}}</span></td>
                                            </tr>
                                        @endforeach
                                        @if ($settings->show_material_breakdown_total)
                                            <tr class="breakdown-row">
                                                <th>Breakdown Totals</th>
                                                <th class="text-right"><span
                                                            data-type="numeric">{{{ $breakdownTotalQuantity }}}</span>
                                                </th>
                                                <th colspan="2"></th>
                                                <th class="text-right"><span
                                                            data-type="numeric">{{{ $breakdownTotalCost }}}</span></th>
                                            </tr>
                                        @endif
                                        </tbody>
                                    </table>
                                </div>
                                <?php
                                $breakdownTotalCost = 0;
                                $breakdownTotalQuantity = 0;
                                unset($breakdownTotalCost);
                                unset($breakdownTotalQuantity);
                                ?>
                            </td>
                            @endif
                        </tr>
                    @endif
                    @if(count($record->getEquipmentUnits()) > 0)
                        @if(!isset($disableAdditionalData))
                            @if($settings->show_equipment_unit_breakdown)
                                <tr data-type="breakdown" data-breakdown="equipment">
                            @else
                                <tr data-type="breakdown" data-breakdown="equipment" style="display: none;">
                                    @endif
                                    <td colspan="{{ $columnCount }}">
                                        <?php
                                        $equipmentBreakdownTotalCost = 0;
                                        $equipmentBreakdownTotalQuantity = 0;
                                        ?>
                                        <div class="{{{ $breakdownClass }}} width-adjustable">
                                            <table class="table table-hover breakdown-table">
                                                <thead>
                                                <tr class="header-row">
                                                    <th>Equipment Unit</th>
                                                    <th>Odometer Reading</th>
                                                    <th class="text-right">Quantity</th>
                                                    @if($settings->show_equipment_unit_rate)
                                                        <th class="text-right">Unit Cost</th>
                                                        <th class="wd-100">Unit</th>
                                                    @endif
                                                    <th class="text-right">Cost</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                @foreach($record->getEquipmentUnits() as $equipmentUnit)
                                                    <?php
                                                    $equipmentBreakdownTotalCost += $equipmentUnit->total_cost;
                                                    $equipmentBreakdownTotalQuantity += $equipmentUnit->total_quantity;
                                                    ?>
                                                    <tr class="breakdown-row">
                                                        <td>{{{ Formatter::equipment($equipmentUnit->equipment) }}}</td>
                                                        @if($equipmentUnit->equipment->odometer != null)
                                                            <td>{{{ $equipmentUnit->equipment->odometer->odometer_reading }}}</td>
                                                        @else
                                                            <td></td>
                                                        @endif
                                                        <td class="text-right"><span
                                                                    data-type="numeric">{{{ $equipmentUnit->total_quantity }}}</span>
                                                        </td>
                                                        @if($settings->show_equipment_unit_rate)
                                                            <td class="text-right"><span
                                                                        data-type="numeric">{{{ $equipmentUnit->historic_cost }}}</span>
                                                            </td>
                                                            <td>{{{ $reader->getMeasurementUnit($equipmentUnit->equipment->unit_id)->name }}}</td>
                                                        @endif
                                                        <td class="text-right"><span
                                                                    data-type="numeric">{{{ $equipmentUnit->total_cost }}}</span>
                                                        </td>
                                                    </tr>
                                                    @if(count($equipmentUnit->fuels) > 0)
                                                        @if($settings->show_fuel_breakdown)
                                                            <tr data-type="breakdown" class="breakdown-row"
                                                                data-breakdown="fuel">
                                                        @else
                                                            <tr data-type="breakdown" class="breakdown-row"
                                                                data-breakdown="fuel" style="display: none;">
                                                                @endif
                                                                <td colspan="{{{ $equipmentColumnCount }}}">
                                                                    <?php
                                                                    $fuelBreakdownTotalCost = 0;
                                                                    $fuelBreakdownTotalQuantity = 0;
                                                                    ?>
                                                                    <div class="{{{ $breakdownClass }}} width-adjustable">
                                                                        <table class="table table-hover breakdown-table">
                                                                            <thead>
                                                                            <tr class="header-row">
                                                                                <th>Fuels
                                                                                    - {{{ Formatter::property($equipmentUnit->equipment) }}}</th>
                                                                                <th class="text-right">Quantity</th>
                                                                                <th class="text-right">Unit Cost</th>
                                                                                <th>Unit</th>
                                                                                <th class="text-right">Cost</th>
                                                                            </tr>
                                                                            </thead>
                                                                            <tbody>
                                                                            @foreach($equipmentUnit->fuels as &$fuel)
                                                                                <?php
                                                                                $fuelBreakdownTotalCost += $fuel->total_cost;
                                                                                $fuelBreakdownTotalQuantity += $fuel->total_quantity;
                                                                                ?>
                                                                                <tr class="breakdown-row">
                                                                                    <td>{{{ Formatter::fuel($fuel->fuel) }}}</td>
                                                                                    <td class="text-right"><span
                                                                                                data-type="numeric">{{{ $fuel->total_quantity }}}</span>
                                                                                    </td>
                                                                                    <td class="text-right"><span
                                                                                                data-type="numeric">{{{ $fuel->historic_cost }}}</span>
                                                                                    </td>
                                                                                    <td>{{{ $reader->getMeasurementUnit($fuel->fuel->unit_id)->name }}}</td>
                                                                                    <td class="text-right"><span
                                                                                                data-type="numeric">{{{ $fuel->total_cost }}}</span>
                                                                                    </td>
                                                                                </tr>
                                                                            @endforeach
                                                                            @if($settings->show_fuel_breakdown_total)
                                                                                <tr class="breakdown-row"
                                                                                    data-breakdown="fuel">
                                                                                    <th>Fuel Breakdown Totals</th>
                                                                                    <th class="text-right"><span
                                                                                                data-type="numeric">{{{ $fuelBreakdownTotalQuantity }}}</span>
                                                                                    </th>
                                                                                    <th colspan="2"></th>
                                                                                    <th class="text-right"><span
                                                                                                data-type="numeric">{{{ $fuelBreakdownTotalCost }}}</span>
                                                                                    </th>
                                                                                </tr>
                                                                            @endif
                                                                            </tbody>
                                                                        </table>
                                                                    </div>
                                                                    <?php
                                                                    $fuelBreakdownTotalCost = 0;
                                                                    $fuelBreakdownTotalQuantity = 0;

                                                                    unset($fuelBreakdownTotalCost);
                                                                    unset($fuelBreakdownTotalQuantity);
                                                                    ?>
                                                                </td>
                                                            </tr>
                                                        @endif
                                                        @endforeach
                                                        @if($settings->show_equipment_unit_breakdown_total)
                                                            <tr class="breakdown-row">
                                                                <th>Equipment Breakdown Totals</th>
                                                                <th class="text-right" colspan="2"><span
                                                                            data-type="numeric">{{{ $equipmentBreakdownTotalQuantity }}}</span>
                                                                </th>
                                                                @if($settings->show_equipment_unit_rate)
                                                                    <th colspan="2"></th>
                                                                @endif
                                                                <th class="text-right"><span
                                                                            data-type="numeric">{{{ $equipmentBreakdownTotalCost }}}</span>
                                                                </th>
                                                            </tr>
                                                        @endif
                                                </tbody>
                                            </table>
                                        </div>
                                        <?php
                                        $equipmentBreakdownTotalCost = 0;
                                        $equipmentBreakdownTotalQuantity = 0;

                                        unset($equipmentBreakdownTotalCost);
                                        unset($equipmentBreakdownTotalQuantity);
                                        ?>
                                    </td>
                                    @endif
                                </tr>
                            @endif
                        @endif

                        @if(!isset($disableAdditionalData))
                            @if(($settings->only_show_descriptions_if_entered && strlen($record->getMiscBilling()->description) > 0) || $settings->always_show_billing_description)
                                @if($stateManager->observed('record_'.$record->getWorkID().'_billing_desc') == false)
                                    <tr>
                                        <td colspan="{{ $columnCount }}">
                                            <div class="{{{ $breakdownClass }}} width-adjustable">
                                                <table class="table table-hover breakdown-table">
                                                    <tbody>
                                                    <tr>
                                                        <td>Billing Description</td>
                                                        <td>
                                                            @if(strlen($record->getMiscBilling()->description) > 0)
                                                                {{{ $record->getMiscBilling()->description }}}
                                                            @else
                                                                &nbsp;
                                                            @endif
                                                        </td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </td>
                                    </tr>
                                @endif
                            @endif
                            @if(($settings->only_show_descriptions_if_entered && strlen($record->getActivityDescription()) > 0) || $settings->always_show_activity_description)
                                @if($stateManager->observed('record_'.$record->getWorkID().'_activity_desc') == false)
                                    <tr>
                                        <td colspan="{{ $columnCount }}">
                                            <div class="{{{ $breakdownClass }}} width-adjustable">
                                                <table class="table table-hover breakdown-table">
                                                    <tbody>
                                                    <tr>
                                                        <td>Activity Description</td>
                                                        <td>
                                                            @if(strlen($record->getActivityDescription()) > 0)
                                                                {{{ $record->getActivityDescription() }}}
                                                            @else
                                                                &nbsp;
                                                            @endif
                                                        </td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </td>
                                    </tr>
                                @endif
                            @endif
                        @endif
