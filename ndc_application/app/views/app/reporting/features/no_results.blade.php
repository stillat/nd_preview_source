<!DOCTYPE html>
<html>
<head>
    <title>ND Counties</title>
    {{ UI::getCss() }}
    <?php  echo $__env->make('integration.universal_analytics'); ?>
</head>
<body class="signin">
<div class="container text-center">
    <div style="max-width: 370px;padding: 15px;margin: 0 auto;">
        <h2 class="form-signin-heading"><i class="ndc-chart-pie"></i> No Report Generated</h2>
        <p>The reporting engine did not return any records. Consider widening the search criteria or filters.</p>
        <br>
        <a class="btn btn-white" id="closeWindow">Close this window</a>
    </div>
    <div class="text-left">
    </div>
    <hr>
    <a href="https://ndcounties.com/app">ND Counties</a>
</div>
<script>
    function init() {
        document.getElementById('closeWindow').onclick=function(){window.close();};
    }
    window.onload=init;
</script>
</body>
</html>