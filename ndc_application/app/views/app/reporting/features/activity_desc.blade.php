@if($settings->always_show_activity_description && ($settings->only_show_descriptions_if_entered && strlen($record->getActivityDescription()) > 0))
@if($stateManager->observed('record_'.$record->getWorkID().'_activity_desc') == false)
<tr>
    <td colspan="{{ $columnCount }}">
        <div class="{{{ $breakdownClass }}} width-adjustable">
            <table class="table table-hover breakdown-table">
                <tbody>
                    <tr>
                        <td>Activity Description</td>
                        <td>
                        @if(strlen($record->getActivityDescription()) > 0)
                            {{{ $record->getActivityDescription() }}}
                        @else
                            &nbsp;
                        @endif
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </td>
</tr>
@endif
@endif