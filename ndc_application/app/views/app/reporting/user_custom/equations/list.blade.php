@extends('app.settings.master')
<?php $extraWide = true; ?>
@section('settingTitle')
    <i class="ndc-math"></i> Report Total Formulas
@stop

@section('buttonBar')
    <div class="btn-group pull-up-10">
        <a href="{{ action('App\Settings\ReportEquationsController@create') }}" class="btn btn-white"><i class="fa fa-plus inline-icon"></i> New Formula</a>
    </div>
@stop

@section('settingContent')
    <p>Report formulas allow you to fine-tune the how the reporting engine calculates the grand total on your reports.</p>
    <hr/>

    <div class="panel">
        <div class="panel-heading">
            <h3 class="panel-title"><i class="ndc-math"></i> Default Equation</h3>
        </div>
        <div class="panel-body nopadding">
            <p style="padding: 10px;">This is the default formula that is being used on this account. This is the formula that will be used when an equation is not selected from the "Options" tab, or when a report does not provide it's own formula.</p>

            <div class="table-responsive mb30">
                <table class="table table-hover table-fixed">
                    <thead>
                    <tr>
                        <th>Formula Name</th>
                        <th>Equation</th>
                        <th class="wd-100 text-right"></th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>
                            {{{ $defaultEquation->total_setting_name }}}
                        </td>
                        <td>
                           <code>{{{ $defaultEquation->formula }}}</code>
                        </td>
                        @if($defaultEquation->account_id > 0)
                            <td class="table-action td-ignore">
                                <a href="{{ action('App\Settings\ReportEquationsController@edit', array($defaultEquation->id)) }}"><i
                                            class="fa fa-pencil"></i></a>
                                <a class="cursor-pointer delete-row" data-rcontext="{{{ $defaultEquation->id }}}"><i
                                            class="fa fa-trash-o"></i></a>
                            </td>
                        @else
                            <td class="table-action td-ignore"></td>
                        @endif
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <div class="row">

        <div class="col-md-12">
            {{ $equations->links() }}
        </div>

        <div class="col-md-12">
            @include('app.reporting.user_custom.equations.partials.table')
        </div>

        <div class="col-md-12">
            {{ $equations->links() }}
        </div>

    </div>

    @include('layouts.dialogs.remove', array('title' => 'Remove Report Equation', 'action' => 'App\Settings\ReportEquationsController@destroy'))

@stop