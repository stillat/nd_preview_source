@extends('app.settings.master')
<?php $extraWide = true; ?>
@section('settingTitle')
    <i class="ndc-math"></i> Report Formulas
@stop

@section('buttonBar')
    <div class="btn-group pull-up-10">
        <a href="{{ action('App\Settings\ReportEquationsController@index') }}" class="btn btn-danger">Cancel</a>
    </div>
@stop

@section('settingContent')
    <p>Report formulas allow you to fine-tune the how the reporting engine calculates the grand total on your reports.</p>
    <hr/>

    {{ Form::open(array('action' => 'App\Settings\ReportEquationsController@store', 'method' => 'post', 'class' => 'form-horizontal')) }}
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4 class="panel-title"><i class="fa fa-plus"></i> You are creating a new reporting formula.</h4>
        </div>
        <div class="panel-body">
            <div class="form-group is-required">
                <label class="col-sm-2 control-label" for="total_setting_name">Formula name:</label>
                <div class="col-sm-8">
                    <input type="text" id="total_setting_name" name="total_setting_name" class="form-control mousetrap ndc_form_home_element {{{ user_form_size() }}}" value="{{{ Input::old('total_setting_name', '') }}}" autofocus>
                    {{ Form::errorMsg('total_setting_name') }}
                </div>
            </div>
            <div class="form-group is-required">
                <label class="col-sm-2 control-label" for="description">Equation:</label>
                <div class="col-sm-8">
                    <textarea id="formula" name="formula" class="form-control mousetrap ndc_form_trail_element {{{ user_form_size() }}}">{{{ Input::old('formula', '') }}}</textarea>
                    {{ Form::errorMsg('formula') }}
                </div>
            </div>
        </div>
        <div class="panel-footer">
            <div class="btn-group m5g">
                <button type="submit" id="ndc_form_defaultaction" value="addNew" name="submit" class="btn btn-primary">Save Report Formula</button>
            </div>
            <div class="btn-group mr5">
                <button type="reset" id="ndc_form_reset" class="btn btn-default">Reset</button>
            </div>
        </div>
    </div>
    {{ Form::close() }}

@stop