<div class="table-responsive mb30">
    <table class="table table-hover table-fixed">
        <thead>
            <tr>
                <th>Formula Name</th>
                <th>Equation</th>
                <th></th>
                <th class="wd-100 text-right"></th>
            </tr>
        </thead>
        <tbody>
            @foreach($equations as $equation)
            <tr>
                <td><a href="{{ action('App\Settings\ReportEquationsController@show', array($equation->id)) }}">{{{ $equation->total_setting_name }}}</a></td>
                <td><a href="{{ action('App\Settings\ReportEquationsController@show', array($equation->id)) }}"><code>{{{ $equation->formula }}}</code></a></td>
                @if($equation->id == $defaultEquation->id)
                <td><span class="label label-primary">Default Equation</span></td>
                @else
                <td><a href="{{{ action('App\UtilityController@getChangeDefaultEquation', [$equation->id]) }}}" data-toggle="modal" data-target="#modalHousing">Make this the default</a></td>
                @endif
                @if($equation->account_id > 0)
                <td class="table-action td-ignore">
                    <a href="{{ action('App\Settings\ReportEquationsController@edit', array($equation->id)) }}"><i class="fa fa-pencil"></i></a>
                    <a class="cursor-pointer delete-row" data-rcontext="{{{ $equation->id }}}"><i class="fa fa-trash-o"></i></a>
                </td>
                @else
                <td class="table-action td-ignore"></td>
                @endif
            </tr>
            @endforeach
        </tbody>
    </table>
</div>