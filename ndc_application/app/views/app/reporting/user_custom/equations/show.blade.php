@extends('layouts.master')

@section('pageTitle')
<i class="ndc-math"></i> {{{ $equation->total_setting_name }}}
@stop
@section('buttonBar')
<div class="btn-group pull-up-10">
    <a href="{{ action('App\Settings\ReportEquationsController@edit', $equation->id) }}" class="btn btn-white"><i class="fa fa-pencil inline-icon"></i> Update this equation</a>
</div>
@stop

@section('content')

<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title">General Information for {{{ $equation->total_setting_name }}}</h3>
    </div>
    <table class="table table-hover">
        <thead>
            <tr>
                <th>Formula Name</th>
                <th>Equation</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>{{{ $equation->total_setting_name }}}</td>
                <td><code>{{{ $equation->formula }}}</code></td>
            </tr>
        </tbody>
    </table>
</div>
@stop