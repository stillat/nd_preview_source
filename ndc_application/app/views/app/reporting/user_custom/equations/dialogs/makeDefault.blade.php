@extends('layouts.modal')
@section('header')
    Change the Default Equation
@stop
@section('body')
    <p>You are about to change the default reporting equation. This new equation will be the default equation unless the
        default is changed again, or a different equation is selected from the "Override Settings" tab.</p>
    <div class="table-responsive mb30">
        <table class="table table-hover table-fixed">
            <thead>
            <tr>
                <th class="wd-200">Setting Name</th>
                <th>Formula</th>
                <th>Affected Accounts</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td>{{{ $newEquation->total_setting_name }}}</td>
                <td><code>{{{ $newEquation->formula }}}</code></td>
                <td>{{{ $newEquation->account_name }}}</td>
            </tr>
            </tbody>
        </table>
    </div>
    <input type="hidden" name="equation_id" value="{{{ $newEquation->id }}}">
@stop

@section('footer')
    <div class="modal-footer">
        <button type="submit" id="default_submit" class="btn btn-primary">Change Default Equation</button>
        <a class="btn btn-default" data-dismiss="modal">Close</a>
    </div>
@overwrite

@section('formOpen')
    {{ Form::open(array('action' => 'App\UtilityController@postChangeDefaultEquation', 'method' => 'post', 'class' => 'form-horizontal form-bordered')) }}
@stop

@section('formClose')
    {{ Form::close() }}
@stop