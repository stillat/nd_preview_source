@extends('layouts.master')
@section('pageTitle')
You are creating a new reporting total blcok
@stop
@section('buttonBar')
<div class="btn-group pull-up-10">
    <a href="{{ action('App\Settings\ReportTotalBlocksController@index') }}" class="btn btn-danger">Cancel</a>
</div>
@stop
@section('content')
<div class="row">
    <div class="col-md-12">
        {{ Form::open(array('action' => 'App\Settings\ReportTotalBlocksController@store', 'method' => 'post', 'class' => 'form-horizontal')) }}
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">You are creating a reporting total block.</h4>
            </div>
            <div class="panel-body">
                <div class="form-group is-required">
                    <label class="col-sm-2 control-label" for="name">Block name:</label>
                    <div class="col-sm-8">
                        <input type="text" id="name" name="name" class="form-control mousetrap ndc_form_home_element {{{ user_form_size() }}}" value="{{{ Input::old('name', '') }}}" autofocus>
                        {{ Form::errorMsg('name') }}
                    </div>
                </div>
                <div class="form-group is-required">
                    <label class="col-sm-2 control-label" for="description">Description:</label>
                    <div class="col-sm-8">
                        <textarea id="description" name="description" class="form-control mousetrap {{{ user_form_size() }}}">{{{ Input::old('description', '') }}}</textarea>
                        {{ Form::errorMsg('description') }}
                    </div>
                </div>
                <div class="form-group is-required">
                    <label class="col-sm-2 control-label" for="value">Value:</label>
                    <div class="col-sm-8">
                        <textarea id="value" name="value" class="form-control mousetrap ndc_form_trail_element {{{ user_form_size() }}}">{{{ Input::old('value', '') }}}</textarea>
                        {{ Form::errorMsg('value') }}
                    </div>
                </div>
            </div>
            <div class="panel-footer">
                <div class="btn-group m5g">
                    <button type="submit" id="ndc_form_defaultaction" value="addNew" name="submit" class="btn btn-primary">Save Account Total Block</button>
                </div>
                <div class="btn-group mr5">
                    <button type="reset" id="ndc_form_reset" class="btn btn-default">Reset</button>
                </div>
            </div>
        </div>
        {{ Form::close() }}
    </div>
</div>
@stop