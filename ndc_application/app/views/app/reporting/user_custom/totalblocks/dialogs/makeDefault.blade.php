@extends('layouts.modal')
@section('header')
Change the Default Report Total Block
@stop
@section('body')
{{ Form::open(array('action' => 'App\UtilityController@postChangeDefaultTotalBlock', 'method' => 'post', 'class' => 'form-horizontal form-bordered')) }}
<p>You are about to change the default reporting total block. This new total block will be the default total block unless the default is changed again, or a different report total block is selected from the "Override Settings" tab.</p>
<div class="table-responsive mb30">
    <table class="table table-hover table-fixed">
        <thead>
            <tr>
                <th>Block Name</th>
                <th>Description</th>
                <th>Value</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>{{{ $newBlock->name }}}</td>
                <td>{{{ $newBlock->description }}}</td>
                <td><code>{{{ $newBlock->value }}}</code></td>
            </tr>
        </tbody>
    </table>
</div>
<input type="hidden" name="block_id" value="{{{ $newBlock->id }}}">
{{ Form::close() }}
@stop