<div class="table-responsive mb30">
    <table class="table table-hover table-fixed">
        <thead>
            <tr>
                <th>Block Name</th>
                <th>Description</th>
                <th>Value</th>
                <th></th>
                <th class="wd-100 text-right"></th>
            </tr>
        </thead>
        <tbody>
            @foreach($totalBlocks as $block)
            <tr>
                <td><a href="{{{ action('App\Settings\ReportTotalBlocksController@show', [$block->id]) }}}">{{{ $block->name }}}</a></td>
                <td><a href="{{{ action('App\Settings\ReportTotalBlocksController@show', [$block->id]) }}}">{{{ $block->description }}}</a></td>
                <td><a href="{{{ action('App\Settings\ReportTotalBlocksController@show', [$block->id]) }}}"><code>{{{ $block->value }}}</code></a></td>
                @if($block->id == $defaultTotalBlock->id)
                <td><span class="label label-primary">Default Total Block</span></td>
                @else
                <td><a href="{{{ action('App\UtilityController@getChangeDefaultTotalBlock', [$block->id]) }}}" data-toggle="modal" data-target="#modalHousing">Make this the default</a></td>
                @endif
                @if($block->account_id > 0)
                <td class="table-action td-ignore">
                    <a href="{{ action('App\Settings\ReportTotalBlocksController@edit', array($block->id)) }}"><i class="fa fa-pencil"></i></a>
                    <a class="cursor-pointer delete-row" data-rcontext="{{{ $block->id }}}"><i class="fa fa-trash-o"></i></a>
                </td>
                @else
                <td class="table-action td-ignore"></td>
                @endif
            </tr>
            @endforeach
        </tbody>
    </table>
</div>