@extends('layouts.master')

@section('pageTitle')
{{{ $currentAccount->account_name }}} Report Total Blocks
@stop

@section('buttonBar')
<div class="btn-group pull-up-10">
	<a href="{{ action('App\Settings\ReportTotalBlocksController@create') }}" class="btn btn-white">Create Report Total Block</a>
</div>
@stop

@section('content')

<div class="row">
    <div class="col-sm-12">
        <div class="panel">
            <div class="panel-heading">
                <h3 class="panel-title">Default Report Total Block</h3>
            </div>
            <div class="panel-body nopadding">
                <p style="padding: 10px;">This is the default total block set for this systema ccount. This is the total block that will be used when a total block is not selected from the "Override Settings" tab when generating a report.</p>
                  <div class="table-responsive mb30">
                      <table class="table table-hover table-fixed">
                          <thead>
                              <tr>
                                  <th>Block Name</th>
                                  <th>Description</th>
                                  <th>Value</th>
                              </tr>
                          </thead>
                          <tbody>
                              <tr>
                                  <td>{{{ $defaultTotalBlock->name }}}</td>
                                  <td>{{{ $defaultTotalBlock->description }}}</td>
                                  <td><code>{{{ $defaultTotalBlock->value }}}</code></td>
                              </tr>
                          </tbody>
                      </table>
                  </div>
                </div>
        </div>
    </div>
</div>

<div class="row">

    <div class="col-md-12">
        {{ $totalBlocks->links() }}
    </div>

    <div class="col-md-12">
       @include('app.reporting.user_custom.totalblocks.partials.table')
   </div>

   <div class="col-md-12">
    {{ $totalBlocks->links() }}
</div>

</div>

@include('layouts.dialogs.remove', array('title' => 'Remove Report Total Block', 'action' => 'App\Settings\ReportTotalBlocksController@destroy'))

@stop
