@extends($reportView)

@section('additionalStyles')
    <link rel="stylesheet" href="{{{ app_url() }}}/assets/css/reporting.css{{ UI::getBuster() }}"/>
    @if($settings->report_print_orientation == 2)
        <style>
            @media print {
                @page {
                    size: landscape
                }
            }
        </style>
    @endif
@stop

@section('title')
    {{{ $constructor->getMetaDefinitionEntry()->report_name }}} - {{{ $reportDatePeriod }}}
@stop

@section('pageTitle')
    <i class="{{{ $constructor->getMetaDefinitionEntry()->report_icon }}}"></i>{{{ $constructor->getMetaDefinitionEntry()->report_name }}}
@stop

@section('buttonBar')
    <div class="btn-group n-mt-5">
        <button type="button" class="btn btn-white dropdown-toggle" data-toggle="dropdown"><i
                    class="fa fa-download inline-icon"></i> Export Options <span class="caret"></span></button>
        <ul class="dropdown-menu" role="menu">
            <li><a href="{{ url('work-logs?search=true') }}"><i class="fa fa-search inline-icon"></i> View the work
                    records in the work list</a></li>
            <li><a href="{{{ export_csv('work_records') }}}"><i class="fa  fa-file-excel-o inline-icon"></i> Export work
                    records used in this report</a></li>
        </ul>
    </div>
    <div class="btn-group n-mt-5">
        <button type="button" class="btn btn-white" data-toggle="print-report"><i class="fa fa-print inline-icon"></i>
            Print
        </button>
        <button type="button" class="btn btn-white dropdown-toggle"
                data-toggle="dropdown" <?php if (!$breakdownEnabled) {
            echo 'disabled="disabled"';
        } ?>>
            <i class="fa fa-th-list inline-icon"></i> Breakdown Visibility <span class="caret"></span>
        </button>
        @if($breakdownEnabled)
            <ul class="dropdown-menu breakdown-visibility" role="menu" style="margin-left:55px;">
                <li>
                    <div class="ckbox ckbox-default">
                        {{ Form::checkbox('equipment_units', 1, $settings->show_equipment_unit_breakdown, ['id' => 'chkEquipmentVisibility']) }}
                        <label for="chkEquipmentVisibility">Equipment Units</label>
                    </div>
                </li>
                <li>
                    <div class="ckbox ckbox-default">
                        {{ Form::checkbox('fuels', 1, $settings->show_fuel_breakdown, ['id' => 'chkFuelVisibility']) }}
                        <label for="chkFuelVisibility">Fuels</label>
                    </div>
                </li>
                <li>
                    <div class="ckbox ckbox-default">
                        {{ Form::checkbox('materials', 1, $settings->show_material_breakdown, ['id' => 'chkMaterialVisibility']) }}
                        <label for="chkMaterialVisibility">Materials</label>
                    </div>
                </li>
                <li>
                    <div class="ckbox ckbox-default">
                        {{ Form::checkbox('properties', 1, $settings->show_property_breakdown, ['id' => 'chkPropertiesVisibility']) }}
                        <label for="chkPropertiesVisibility">Properties</label>
                    </div>
                </li>
                <li class="divider"></li>
                <li><a id="toggleCenterFullWidth" class="cursor-pointer">Toggle centered/full-width</a></li>
            </ul>
        @endif
    </div>
@stop
@section('content')
    @include('app.reporting.features.printing')
    <div class="row">
        <div class="col-sm-12">
            <div>
                @if ($constructor->isDirectReport())
                    @include('app.reporting.modes.direct')
                @else
                    @if($srlIterator != '0')
                        @include('app.reporting.modes.srl_prepared')
                    @else
                        @if($constructor->getInternalMode() == 0)

                            {{-- This is the include block for the normal internal engine mode. --}}
                            @if($constructor->getOrganizationStrategy() == null)
                                @include('app.reporting.modes.normal')
                            @else
                                @if($constructor->getOrganizationStrategy()['mode'] !== 'special')
                                    @include('app.reporting.modes.organization_strategy')
                                @else
                                    @include('app.reporting.modes.special_organization_strategy')
                                @endif
                            @endif
                        @elseif(in_array($constructor->getInternalMode(), [1,2,3,4]))
                            {{-- This is the include block for the consumable internal engine mode. --}}
                            @if($constructor->getOrganizationStrategy() == null)
                                @include('app.reporting.modes.consumable_general')
                            @else
                                @if($constructor->getOrganizationStrategy()['mode'] !== 'special')
                                    @include('app.reporting.modes.consumable_organization_strategy')
                                @else

                                @endif
                            @endif
                        @endif
                    @endif
                @endif
            </div>
        </div>
    </div>
@stop
@section('additionalScripts')
    <script>
        var reportingMode = {{{ $constructor->getInternalMode() }}};
        var BREAKDOWN_WIDTH = {{{ $settings->record_breakdown_style }}};
                @if($constructor->getOrganizationStrategy() !== null)
                var groupingMethod = '{{{ $constructor->getOrganizationStrategy()['mode'] }}}';
                @else
                var groupingMethod = null;
        @endif
    </script>
    <script src="{{{ app_url() }}}/assets/js/report_viewer.min.js{{ UI::getBuster() }}" defer></script>

@stop