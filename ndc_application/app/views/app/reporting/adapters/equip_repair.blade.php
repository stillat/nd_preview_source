<tbody>
    @if(strlen($data->work_order_number) > 0)
    <tr>
        <td><strong>Work Order Number</strong></td>
        <td colspan="5">{{{ $data->work_order_number }}}</td>
    </tr>
    @endif
    @if(property_exists($data, 'odometer') && $data->odometer !== null)
    <tr>
        <td><strong>Odometer Reading</strong></td>
        <td colspan="5">{{{ $data->odometer->odometer_reading }}}</td>
    </tr>
    @endif
    @if(strlen($data->work_description) > 0)
    <tr>
        <td class="bottom_border"><strong>Work Description</strong></td>
        <td colspan="5" class="bottom_border">{{{ $data->work_description }}}</td>
    </tr>
    @endif
    @if (($data->commercial_costs + $data->commercial_labor_costs + $data->shop_costs + $data->purchased_costs) > 0)
        <tr>
            <td><strong>Shop Parts</strong></td>
            <td><span data-type="numeric">{{{ $data->shop_costs }}}</span></td>
            <td><strong>Invoice Parts</strong></td>
            <td><span data-type="numeric">{{{ $data->purchased_costs }}}</span></td>
            <td><strong>Total</strong></td>
            <td class="text-right"><span data-type="numeric">{{{ $data->shop_costs + $data->purchased_costs }}}</span></td>
        </tr>
        <tr>
            <td><strong>Contractor Costs</strong></td>
            <td><span data-type="numeric">{{{ $data->commercial_costs }}}</span></td>
            <td><strong>Contractor Labor</strong></td>
            <td><span data-type="numeric">{{{ $data->commercial_labor_costs }}}</span></td>
            <td><strong>Total</strong></td>
            <td class="text-right"><span data-type="numeric">{{{ $data->commercial_costs + $data->commercial_labor_costs }}}</span></td>
        </tr>
        @if($settings->show_property_breakdown_total)
            <tr>
                <th colspan="5"><strong>Equipment Unit Repair Breakdown Totals</strong></th>
                <th class="text-right"><span data-type="numeric">{{{ $data->commercial_costs + $data->commercial_labor_costs + $data->shop_costs + $data->purchased_costs }}}</span></th>
            </tr>
        @endif
    @endif
</tbody>