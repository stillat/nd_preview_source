<thead>
    <tr>
        <th colspan="2">{{{ $settings->property_code.' - '.$settings->property_name }}}</th>
    </tr>
    <tr>
        <th>Road Length</th>
        <th>Surface Type</th>
    </tr>
</thead>
<tbody>
    <tr>
        <td><span data-type="numeric">{{{ $data->road_length.' '.$unit->code.' ('.$unit->name.')' }}}</span></td>
        <td>{{{ $type->code.' '.$type->name  }}}</td>
    </tr>
</tbody>