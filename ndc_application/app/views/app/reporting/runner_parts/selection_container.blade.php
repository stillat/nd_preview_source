<div class="panel panel-white" id="reportBuilderHolder" style="display: none;">
    <div class="panel-body nopadding" style="padding-left: 10px !important; padding-right: 10px !important;">
        <ul id="mainTab" class="nav nav-tabs nopadding" style="background-color: white; border-bottom-width: 2px;border-bottom-style: solid;border-bottom-color: rgb(66, 139, 202);">
            <li class="active bb_property interaction_trigger" style="margin-bottom: -2px;"><a href="#lwFilters" data-toggle="tab"><strong><i
                                class="fa fa-filter"></i> <span
                                class="hidden-sm hidden-xs hidden-md">Filters</span></strong></a></li>
            <li><a class="cursor-pointer" id="showMoreOptions"><i class="fa fa-chevron-right"></i></a></li>
            <li class="bb_invoice interaction_trigger more-option" style="margin-bottom: -2px; display:none;"><a href="#lwConstraints" data-toggle="tab"><strong><i
                                class="fa fa-compress"></i> <span
                                class="hidden-sm hidden-xs hidden-md">Constraints</span></strong></a></li>
            <li class="bb_equip interaction_trigger more-option" style="margin-bottom: -2px; display:none;"><a href="#lwSorting" data-toggle="tab"><strong><i
                                class="fa fa-sort"></i> <span
                                class="hidden-sm hidden-xs hidden-md">Sorting</span></strong></a></li>
            @if(!$searchOnlyMode)
                <li class="bb_settings interaction_trigger more-option" style="margin-bottom: -2px; display:none;"><a href="#lwOptions" id="lwOptionsActivator" data-toggle="tab"><strong><i
                                    class="fa fa-cogs"></i> <span
                                    class="hidden-sm hidden-xs hidden-md">Options</span></strong></a></li>
            @endif
            <li style="display: none;" class="more-option"><a class="cursor-pointer" id="hideMoreOptions"><i class="fa fa-chevron-left"></i></a></li>

            @if(!$searchOnlyMode)
                <li class="pull-right live-help-step-four-selector" data-toggle="popover" data-trigger="hover" data-placement="bottom" data-content="Run the report with the provided filters and constraints."><a class="btn btn-white" id="generateReportButton" style="color: #2980b9;"><i
                                class="fa fa-play"></i> Run Report</a></li>
                <li class="pull-right live-help-step-four-selector" data-toggle="popover" data-trigger="hover" data-placement="bottom" data-content="Run the report with the provided filters and constrains and open the results in a new window."><a class="btn btn-white" id="generateReportInNewWindowButton" style="color: #2980b9;"><i
                                class="fa fa-external-link-square"></i></a></li>
                <li class="pull-right live-help-step-four-selector" data-toggle="popover" data-trigger="hover" data-placement="bottom" data-content="Search for records with the provided filters and constraints."><a class="btn btn-white" id="generateReportAsSearchButton" style="color: #2980b9;"><i
                                class="fa fa-search"></i></a></li>
            @else
                <li class="pull-right live-help-step-four-selector" data-toggle="popover" data-trigger="hover" data-placement="bottom" data-content="Search for records with the provided filters and constraints."><a class="btn btn-white" id="generateReportAsSearchButton" style="color: #2980b9;"><i
                                class="fa fa-search"></i> Perform Search</a></li>
            @endif

            @if(!$searchOnlyMode)
                <li class="pull-right custom-report-utility" data-toggle="popover" data-trigger="hover" data-placement="bottom" data-content="Save the current filters, constraints and options as a custom report."><a class="btn btn-white" id="saveReportButton" data-toggle="modal" data-backdrop="static" data-target="#reportSaveCustomReportModal"><i
                                class="fa fa-save"></i></a></li>
                <li class="pull-right custom-report-utility" data-toggle="popover" data-trigger="hover" data-placement="bottom" data-content="Updates the current custom report."><a class="btn btn-white" id="updateReportButton" data-toggle="modal" data-backdrop="static" data-target="#reportSaveCustomReportModal"><i
                                class="fa fa-save"></i> Update Report</a></li>
                <li class="pull-right custom-report-utility" data-toggle="popover" data-trigger="hover" data-placement="bottom" data-content="Removes the current custom report."><a class="btn btn-white" id="removeReportButton" data-toggle="modal" data-backdrop="static" data-target="#reportRemoveCustomReportModal"><i
                                class="fa fa-trash"></i> Remove Report</a></li>
            @endif
        </ul>
        <div class="tab-content nopadding">
            <div class="tab-pane active" id="lwFilters">
                @include('app.reporting.runner_parts.selection.filters')
            </div>
            <div class="tab-pane" id="lwConstraints" style="padding: 10px;">
                @include('app.reporting.runner_parts.selection.constraints')
            </div>
            <div class="tab-pane" id="lwSorting" style="padding: 10px;">
                @include('app.reporting.runner_parts.selection.sorting')
            </div>
            <div class="tab-pane" id="lwOptions" style="padding: 10px;">
                @include('app.reporting.runner_parts.selection.options')
            </div>
        </div>
    </div>
</div>