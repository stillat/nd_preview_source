<div class="tabbable tabs-left">
    <div class="row">
        <div class="col-sm-3">
            <ul class="nav nav-tabs" style="width: 100%; background: #FFFFFF;">
                <li id="groupingMethodTab" class="active"><a href="#grouping" data-toggle="tab"><i class="fa fa-th-list"></i> Grouping Method</a></li>
                <li id="equationsTab"><a href="#equations" data-toggle="tab"><i class="ndc-math"></i> Report Formulas</a></li>
                <li><a href="#display" data-toggle="tab" id="displaySettingsActivator"><i class="fa fa-laptop"></i> Display &amp; Print Settings</a></li>
                <li><a href="#notes" data-toggle="tab"><i class="fa fa-quote-right"></i> Report Notes</a></li>
                <li><a href="#searchMisc" data-toggle="tab"><i class="fa fa-search"></i> Search &amp; Miscellaneous</a></li>
            </ul>
        </div>
        <div class="col-sm-9">

            <div class="tab-content">
                <div class="tab-pane active" id="grouping">
                    @include('app.reporting.runner_parts.dialogs.grouping')
                </div>
                <div class="tab-pane" id="equations">
                    @include('app.reporting.runner_parts.dialogs.equations')
                </div>
                <div class="tab-pane" id="display">
                    @include('app.reporting.runner_parts.settings_override')
                </div>
                <div class="tab-pane" id="notes">
                    @include('app.reporting.runner_parts.dialogs.notes')
                </div>
                <div class="tab-pane" id="searchMisc">
                    @include('app.reporting.runner_parts.dialogs.search')
                </div>
            </div>
        </div>
    </div>
</div>