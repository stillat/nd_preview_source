    <table class="table table-hover table-bordered">
        <thead>
        <tr>
            <th class="wd-100">Filter</th>
            <th style="width: 150px;">Mode</th>
            <th>Value</th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td><a href="#" id="activityAll"><strong>Activity</strong></a></td>
            <td>
                {{ Form::select('activityFilerMode', $filterModes, null, ['class' => 'form-control', 'id' => 'activityFilterMode']) }}
            </td>
            <td>{{ Form::select('activity_filters[]', $activities, null, ['class' => 'chosen-select', 'id' => 'activityFilter', 'multiple' => 'multiple', 'data-placeholder' => 'Select your activity filters']) }}</td>
        </tr>
        <tr>
            <td><a href="#" id="departmentAll"><strong>Department</strong></a></td>
            <td>
                {{ Form::select('departmentFilterMode', $filterModes, null, ['class' => 'form-control', 'id' => 'departmentFilterMode']) }}
            </td>
            <td>{{ Form::select('department_filters[]', $departments, null, ['class' => 'chosen-select', 'id' => 'departmentFilter', 'multiple' => 'multiple', 'data-placeholder' => 'Select your department filters']) }}</td>
        </tr>
        <tr>
            <td><a href="#" id="districtAll"><strong>District</strong></a></td>
            <td>
                {{ Form::select('districtFilterMode', $filterModes, null, ['class' => 'form-control', 'id' => 'districtFilterMode']) }}
            </td>
            <td>{{ Form::select('district_filters[]', $districts, null, ['class' => 'chosen-select', 'id' => 'districtFilter', 'multiple' => 'multiple', 'data-placeholder' => 'Select your district filters']) }}</td>
        </tr>
        <tr>
            <td><a href="#" id="employeeAll"><strong>Employee</strong></a></td>
            <td>
                {{ Form::select('employeeFilterMode', $filterModes, null, ['class' => 'form-control', 'id' => 'employeeFilterMode']) }}
            </td>
            <td>{{ Form::select('employee_filters[]', $employees, null, ['class' => 'chosen-select', 'id' => 'employeeFilter', 'multiple' => 'multiple', 'data-placeholder' => 'Select your employee filters']) }}</td>
        </tr>
        <tr>
            <td><a href="#" id="equipAll"><strong>Equipment Units</strong></a></td>
            <td>
                {{ Form::select('equipFilterMode', $filterModes, null, ['class' => 'form-control', 'id' => 'equipmentFilterMode']) }}
            </td>
            <td>{{ Form::select('equipment_filters[]', $equipments, null, ['class' => 'chosen-select', 'id' => 'equipmentFilter', 'multiple' => 'multiple', 'data-placeholder' => 'Select your equipment unit filters']) }}</td>
        </tr>
        <tr>
            <td><a href="#" id="materialAll"><strong>Materials</strong></a></td>
            <td>
                {{ Form::select('materialFilterMode', $filterModes, null, ['class' => 'form-control', 'id' => 'materialFilterMode']) }}
            </td>
            <td>{{ Form::select('material_filters[]', $materials, null, ['class' => 'chosen-select', 'id' => 'materialFilter', 'multiple' => 'multiple', 'data-placeholder' => 'Select your material filters']) }}</td>
        </tr>
        <tr>
            <td><a href="#" id="projectAll"><strong>Project</strong></a></td>
            <td>
                {{ Form::select('projectFilterMode', $filterModes, null, ['class' => 'form-control', 'id' => 'projectFilterMode']) }}
            </td>
            <td>{{ Form::select('project_filters[]', $projects, null, ['class' => 'chosen-select', 'id' => 'projectFilter', 'multiple' => 'multiple', 'data-placeholder' => 'Select your project filters']) }}</td>
        </tr>
        <tr>
            <td><a href="#" id="roadAll"><strong>Road</strong></a></td>
            <td>
                {{ Form::select('roadFilterMode', $filterModes, null, ['class' => 'form-control', 'id' => 'roadFilterMode']) }}
            </td>
            <td>
                <select name="road_filters[]" id="roadFilter" class="chosen-select" data-placeholder="Select your road filters" multiple="mulitple">
                    @foreach($roads as $roadID => $road)
                        <option value="{{{ $roadID }}}" data-type="{{{ $road[1] }}}">{{{ $road[0] }}}</option>
                    @endforeach
                </select>
            </td>
        </tr>
        <tr>
            <td colspan="3" style="text-align: center"><a class="cursor-pointer" id="advancedReportToggle">Show report scope and property options</a></td>
        </tr>
        <tr style="border-top: 2px solid rgba(66, 139, 202, 0.21);" data-advanced="true">
            <td colspan="2"><a href="#" id="propertyTypeAll"><strong>Report Scope</strong></a></td>
            <td>{{ Form::select('property_type_filters[]', $propertyTypes, null, ['class' => 'chosen-select', 'id' => 'propertyTypeFilter', 'multiple' => 'multiple', 'data-placeholder' => 'Limit your reports scope']) }}</td>
        </tr>
        <tr data-advanced="true">
            <td><a href="#" id="propertyAll"><strong>Property</strong></a></td>
            <td>
                {{ Form::select('propertyFilterMode', $filterModes, null, ['class' => 'form-control', 'id' => 'propertyFilterMode']) }}
            </td>
            <td>
                <select name="property_filters[]" id="propertyFilter" class="chosen-select" data-placeholder="Select your property filters" multiple="mulitple">
                    @foreach($properties as $propertyID => $property)
                        <option value="{{{ $propertyID }}}" data-type="{{{ $property[1] }}}">{{{ $property[0] }}}</option>
                    @endforeach
                </select>
            </td>
        </tr>
        </tbody>
    </table>
