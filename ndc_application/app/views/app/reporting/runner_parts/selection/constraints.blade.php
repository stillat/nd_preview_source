<p>Use constraints to limit limit the data that is returned.</p>

<div class="table-responsive">
    <table class="table table-bordered table-striped table-hover">
        <thead>
            <tr>
                <th style="width: 300px;">Constraint</th>
                <th style="width: 300px;">Mode</th>
                <th>Value</th>
                <th class="wd-100"><a class="btn btn-white" id="addConstraintActivator"><i class="fa fa-plus"></i> Add Constraint</a></th>
            </tr>
        </thead>
        <tbody id="constraintHolder">

        </tbody>
    </table>
</div>

<script type="text/html" id="reportConstraint_tmpl">
    <%* constraintsCount++ %>
    <tr data-constraint="<%:~getConstraintCount()%>" id="constraintSelector<%:~getConstraintCount()%>">
        <td>
            <select name="constraint_names[]" id="constraintName<%:~getConstraintCount()%>" class="form-control" data-constraint="<%:~getConstraintCount()%>">
                @foreach($constraintNames as $value => $text)
                    <option value="{{{ $value }}}" <%:~isSelected('{{{ $value }}}', constraint_name)%>>{{{ $text }}}</option>
                @endforeach
            </select>
        </td>
        <td>
            <select name="constraint_modes[]" id="constraintMode<%:~getConstraintCount()%>" class="form-control" data-constraint="<%:~getConstraintCount()%>">
                @foreach($constraintModes as $value => $text)
                    <option value="{{{ $value }}}" <%:~isSelected('{{ $value }}', constraint_mode)%>>{{{ $text }}}</option>
                @endforeach
            </select>

        </td>
        <td><input type="number" name="constraint_values[]" step="any" value="<%>constraint_quantity%>" class="text-right form-control" /></td>
        <td><a class="btn btn-link remove_constraint" data-constraint="<%:~getConstraintCount()%>"><i class="fa fa-delete-o"></i> Remove this constraint</a></td>
    </tr>
</script>