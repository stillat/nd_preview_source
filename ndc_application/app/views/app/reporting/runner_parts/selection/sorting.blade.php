<p>Use sorting settings to control the order of records on the report.</p>

<div class="table-responsive">
    <table class="table table-bordered table-striped table-hover">
        <thead>
        <tr>
            <th style="width: 300px;">Sorting Column</th>
            <th style="width: 300px;">Sort Order</th>
            <th class="pull-right"><a class="btn btn-white" id="addSortColumnActivator"><i class="fa fa-plus"></i> Sort Another Column</a></th>
        </tr>
        </thead>
        <tbody id="sortColumnHolder">

        </tbody>
    </table>
</div>

<script type="text/html" id="sortingColumn_tmpl">
    <%* sortingCount++ %>
    <tr data-sorting="<%:~getSortingCount()%>" id="sortingSelector<%:~getSortingCount()%>">
        <td>
            <select name="sorting_names[]" id="sortingName<%:~getSortingCount()%>" class="form-control" data-sorting="<%:~getSortingCount()%>">
                @foreach($sortingColumns as $value => $text)
                    <option value="{{{ $value }}}" <%:~isSelected('{{{ $value }}}', column)%>>{{{ $text }}}</option>
                @endforeach
            </select>
        </td>
        <td>
            <select name="sorting_modes[]" id="sortingMode<%:~getSortingCount()%>" class="form-control" data-sorting="<%:~getSortingCount()%>">
                @foreach($sortingModes as $value => $text)
                    <option value="{{{ $value }}}" <%:~isSelected('{{ $value }}', mode)%>>{{{ $text }}}</option>
                @endforeach
            </select>

        </td>
        <td><a class="btn btn-link remove_sorting" data-sorting="<%:~getSortingCount()%>" style="float:right;text-align:right;"><i class="fa fa-delete-o"></i> Remove this sorting column</a></td>
    </tr>
</script>