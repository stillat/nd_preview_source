<div class="modal fade" id="reportCustomErrorMessage" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content panel-danger">
            <div class="modal-header panel-heading">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true" style="margin-top: 12px;color: white;opacity: 0.85;">&times;</button>
                <h4 style="color: white;"><i class="fa fa-warning"></i> Before we continue...</h4>
            </div>
            <div class="modal-body">
                <p id="customErrorMessage"></p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="reportGeneralMessage" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content panel-danger">
            <div class="modal-body">
                <p id="customGeneralMessage"></p>
            </div>
        </div>
    </div>
</div>