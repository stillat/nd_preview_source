<div id="groupingHolder">
    <fieldset style="margin-top: 10px;">
        <legend class="option-legend">Dynamic Grouping Methods</legend>
        <p style="padding: 10px;">Grouping methods are the primary way to change how the reports are generated and laid out when printed.</p>
        <div class="rdio rdio-default">
            <input type="radio" name="org_strategy" id="orgReportDefault" value="1" checked="checked">
            <label for="orgReportDefault">Report Default Grouping Method</label>
        </div>
        <div class="rdio rdio-primary">
            <input type="radio" name="org_strategy" id="orgGenericGroup" value="2">
            <label for="orgGenericGroup">Simple Grouping Method</label>
        </div>
        <div id="genericGroupingOptions" style="display: none; margin-bottom: 20px;">
            <div class="form-group" style="margin-top: 15px;">
                <label class="col-sm-3 control-label">Group report records by</label>
                <div class="col-sm-5">
                    <select name="generic_grouping_column" class="form-control n-mt-5" id="genericGroupingColumn">
                        <option value="activity_id">Activity</option>
                        <option value="department_id">Department</option>
                        <option value="district_id">District</option>
                        <option value="project_id">Project</option>
                        <option value="employee_id">Employee</option>
                        <option value="property_organization_id">Road</option>
                    </select>
                </div>
            </div>
        </div>
        <div class="rdio rdio-success mb20">
            <input type="radio" name="org_strategy" id="orgSpecialGroup" value="3">
            <label for="orgSpecialGroup">Special Grouping Strategy</label>
        </div>
        <div id="specialGroupingOptions" style="display: none;" class="mb10">
            <div class="form-group">
                <label class="col-sm-3 control-label">Special Grouping Column</label>
                <div class="col-sm-5">
                    <select name="special_grouping_column" class="form-control n-mt-5" id="specialGroupingColumn">
                        <option value="activity_id">Activity</option>
                        <option value="department_id">Department</option>
                        <option value="district_id">District</option>
                        <option value="employee_id">Employee</option>
                        <option value="project_id">Project</option>
                        <option value="property_id">Property</option>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label">Special Group Name</label>
                <div class="col-sm-5">
                    <input type="text" name="special_grouping_name" id="specialGroupingName" class="form-control"/>
                </div>
            </div>
            <div class="form-group" style="margin-bottom:20px;">
                <label class="col-sm-3 control-label">Special Group Filters</label>
                <div class="col-sm-9">
                    <select name="special_group_filters[]" class="form-control chosen-select" multiple="multiple" data-placeholder="Select your special grouping filters" id="specialSelectFilters"></select>
                </div>
            </div>
        </div>
    </fieldset>
    <fieldset>
        <legend class="option-legend">Combine Report Records</legend>
        <p class="text-info" style="padding: 10px;">Combining records collapses multiple records into one report entry. For example, if report records are combined by activity, then any row containing "vacation" will be combined, and their totals added together. You can combine up to three report columns.</p>
        <div class="form-group">
            <label class="col-sm-3 control-label">Combine the report records by</label>
            <div class="col-sm-5">
                <select name="group_report_columns[]" class="form-control n-mt-5 chosen-select" multiple="multiple" id="groupingReportColumn" data-placeholder="Select the columns to combine in your report">
                    <option value="activity_id">Activity</option>
                    <option value="department_id">Department</option>
                    <option value="district_id">District</option>
                    <option value="employee_id">Employee</option>
                    <option value="project_id">Project</option>
                    <option value="property_id">Road</option>
                    <option value="work_date">Work Date</option>
                    <option value="consumable_id" id="consumableGroupingOption" disabled="disabled">Equipment, Fuel or Material</option>
                </select>
            </div>
        </div>
    </fieldset>
</div>
<div id="groupingHolderDisabled">
    <fieldset style="margin-top: 10px;">
        <legend class="option-legend">Dynamic Grouping Methods</legend>
        <p>These options are not available for this report.</p>
        </fieldset>
</div>