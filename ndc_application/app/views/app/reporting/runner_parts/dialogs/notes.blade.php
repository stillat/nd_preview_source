<fieldset>
    <legend class="option-legend"><i class="fa fa-quote-right"></i> Report Notes</legend>
    <p style="padding: 10px;">Reporting notes will appear at the top of reports when they are available.</p>
    <div class="form-group">
        <div class="col-sm-12">
            <textarea class="form-control autogrow" name="reportNotes" cols="30" rows="5" placeholder="Type your report notes here to have them appear at the top of the report"></textarea>
        </div>
    </div>
</fieldset>