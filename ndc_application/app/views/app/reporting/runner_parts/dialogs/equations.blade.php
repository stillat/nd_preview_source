<div id="equationsHolder">
    <fieldset>
        <legend class="option-legend"><i class="ndc-math"></i> Report Formulas</legend>
        <div class="form-group">
            <label class="col-sm-3 control-label">Report Total Formula</label>

            <div class="col-sm-5">
                {{ Form::select('equation', $equations, $defaultEquation, ['class' => 'form-control', 'id' => 'reportEquation']) }}
                <br>
                <p class="text-muted">This option changes how the individual records are totaled and how the reports grand total is calculated. You can add custom reporting equations using the <a href="{{ url('settings/reports/equations')  }}">report formulas settings</a> page.</p>
            </div>
        </div>
        <div class="form-group" style="display:none;">
            <label class="col-sm-3 control-label">Report Total Block</label>

            <div class="col-sm-5">
                {{ Form::select('block', $blocks, $defaultBlock, ['class' => 'form-control', 'id' => 'reportBlock']) }}
            </div>
        </div>
    </fieldset>
</div>
<div id="equationsHolderDisabled">
    <fieldset>
        <legend class="option-legend"><i class="ndc-math"></i> Report Formulas</legend>
        <p>These options are not available for this report.</p>
    </fieldset>
</div>