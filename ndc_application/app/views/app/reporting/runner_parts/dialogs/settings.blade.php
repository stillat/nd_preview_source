<div class="modal fade" id="reportSettingsModal" role="dialog" aria-hidden="true">
    <div class="modal-dialog" style="width: 70%;">
        <div class="modal-content panel-primary">
            <div class="modal-header panel-heading">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true" style="margin-top: 12px;color: white;opacity: 0.85;">&times;</button>
                <h4><i class="fa fa-cogs"></i> Reporting Settings</h4>
            </div>
            <div class="modal-body nopadding">
                <div class="tabbable tabs-left">
                    <div class="row">
                        <div class="col-sm-2">
                            <ul class="nav nav-tabs" style="width: 100%; background: #FFFFFF;">
                                <li class="active" id="groupingMethodTab"><a href="#grouping" data-toggle="tab"><i class="fa fa-th-list"></i> Grouping Method</a></li>
                                <li id="equationsTab"><a href="#equations" data-toggle="tab"><i class="ndc-math"></i> Equations and Totals</a></li>
                                <li><a href="#display" data-toggle="tab" id="displaySettingsActivator"><i class="fa fa-laptop"></i> Display Settings</a></li>
                                <li><a href="#notes" data-toggle="tab"><i class="fa fa-quote-right"></i> Report Notes</a></li>
                            </ul>
                        </div>
                        <div class="col-sm-10">

                            <div class="tab-content">
                                <div class="tab-pane active" id="grouping">
                                    @include('app.reporting.runner_parts.dialogs.grouping')
                                </div>
                                <div class="tab-pane" id="equations">
                                    @include('app.reporting.runner_parts.dialogs.equations')
                                </div>
                                <div class="tab-pane" id="display">
                                    @include('app.reporting.runner_parts.settings_override')
                                </div>
                                <div class="tab-pane" id="notes">
                                    @include('app.reporting.runner_parts.dialogs.notes')
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>