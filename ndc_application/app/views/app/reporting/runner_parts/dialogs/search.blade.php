<fieldset class="row">
    <legend class="option-legend"><i class="fa fa-search"></i> Search</legend>
    <div class="col-sm-12">
        <div class="ckbox ckbox-default" style="margin-top: 12px;">
            <input type="checkbox" value="1" id="searchRecordsInstead" name="search_records_instead">
            <label for="searchRecordsInstead">
                <i class="fa fa-search"></i> Display report records <strong>instead</strong>
                <br/><p>Any records that would appear on the report will be displayed in a list instead.</p>
            </label>
        </div>
    </div>
</fieldset>
<fieldset class="row">
    <legend class="option-legend">Miscellaneous</legend>
    <div class="col-sm-12">
        <div class="ckbox ckbox-default" style="margin-top: 12px;">
            <input type="checkbox" value="1" id="compareLastSearch" name="compare_last_report">
            <label for="compareLastSearch">
                Compare with previous report/search
                <br/><p>Compares the records in the current report or search with the records in the last report or search.</p>
            </label>
        </div>
    </div>
</fieldset>