<div class="modal fade" id="reportRemoveCustomReportModal" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content panel-danger">
            <div class="modal-header panel-heading">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true" style="margin-top: 12px;color: white;opacity: 0.85;">&times;</button>
                <h4 style="color: white;"><i class="fa fa-trash"></i> Remove Custom Report</h4>
            </div>
            <div class="modal-body">
                {{ Form::open(['action' => 'App\Reporting\ReportsController@postRemoveCustomReport', 'id' => 'removeCustomReportForm', 'method' => 'post']) }}
                <input type="hidden" name="report" value="" id="customReportRemoveID">
                {{ Form::close() }}
                <p>Are you sure you want to permanently remove the currently selected report?</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" id="removeReportActivator">Remove Report</button>
                <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>