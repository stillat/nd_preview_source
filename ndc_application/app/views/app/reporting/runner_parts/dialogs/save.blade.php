<div class="modal fade" id="reportSaveCustomReportModal" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content panel-primary">
            <div class="modal-header panel-heading">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true" style="margin-top: 12px;color: white;opacity: 0.85;">&times;</button>
                <h4><i class="fa fa-save"></i> <span id="saveTitle">Save Custom Report</span></h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal">
                    <div class="form-group">
                        <label class="control-label col-sm-3" for="newReportName">Name</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" name="new_report_name" id="newReportName" placeholder="Give your new report a good name">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-3" for="newReportDescription">Description</label>
                        <div class="col-sm-9">
                            <textarea name="new_report_description" style="min-height: 35px; max-height: 150px;" id="newReportDescription" class="form-control autosize" placeholder="Give your new report a meaningful description"></textarea>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" id="saveReportActivator">Save Report</button>
                <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
