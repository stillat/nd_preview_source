<p>These settings are available to override on a per-report basis. Their initial values are the same as those set on the "Report General Settings" page. Changing these settings will not modify the default settings.</p>

<div class="row">
    <fielset class="row">
        <legend class="option-legend"><i class="fa fa-user"></i> Employee Reporting Settings</legend>
        <div class="col-sm-12">
            <div class="ckbox ckbox-default">
                {{ Form::checkbox('show_employee_wage', 1, true, ['id' => 'chkShowEmployeeWage']) }}
                <label for="chkShowEmployeeWage">
                    Show Employee Wage<br>
                    <p class="text-info">This option overrides internal reporting settings.</p>
                </label>
            </div>
        </div>
        <div class="col-sm-12">
            <div class="ckbox ckbox-default">
                {{ Form::checkbox('show_employee_hours_worked', 1, true, ['id' => 'chkShowEmployeeHoursWorked']) }}
                <label for="chkShowEmployeeHoursWorked">
                    Show Employee Hours Worked<br>
                    <p class="text-info">This option overrides internal reporting settings.</p>
                </label>
            </div>
        </div>
    </fielset>
</div>
<div class="row">
    <fieldset class="row mb20">
        <legend class="option-legend"><i class="fa fa-table"></i> Breakdown Table Reporting Settings</legend>
        <div class="col-sm-12">
            <div class="ckbox ckbox-default">
                {{ Form::checkbox('show_material_breakdown', 1, true, ['id' => 'show_material_breakdown', 'data-checked-setting' => 'monitor']) }}
                <label for="show_material_breakdown">Show Material Breakdown Table</label>
            </div>
        </div>
        <div class="col-sm-12">
            <div class="ckbox ckbox-default">
                {{ Form::checkbox('show_material_breakdown_total', 1, true, ['id' => 'show_material_breakdown_total', 'data-checked-setting' => 'monitor']) }}
                <label for="show_material_breakdown_total">Show Totals on Material Breakdown Table</label>
            </div>
        </div>
        <div class="col-sm-12">
            <div class="ckbox ckbox-default">
                {{ Form::checkbox('show_equipment_unit_breakdown', 1, true, ['id' => 'show_equipment_unit_breakdown', 'data-checked-setting' => 'monitor']) }}
                <label for="show_equipment_unit_breakdown">Show Equipment Unit Breakdown Table</label>
            </div>
        </div>
        <div class="col-sm-12">
            <div class="ckbox ckbox-default">
                {{ Form::checkbox('show_equipment_unit_breakdown_total', 1, true, ['id' => 'show_equipment_unit_breakdown_total', 'data-checked-setting' => 'monitor']) }}
                <label for="show_equipment_unit_breakdown_total">Show Totals on Equipment Unit Breakdown Table</label>
            </div>
        </div>
        <div class="col-sm-12">
            <div class="ckbox ckbox-default">
                {{ Form::checkbox('show_equipment_unit_rate', 1, true, ['id' => 'show_equipment_unit_rate', 'data-checked-setting' => 'monitor']) }}
                <label for="show_equipment_unit_rate">Show Equipment Unit Rate on Reports</label>
            </div>
        </div>
        <div class="col-sm-12">
            <div class="ckbox ckbox-default">
                {{ Form::checkbox('show_fuel_breakdown',1, true, ['id' => 'show_fuel_breakdown', 'data-checked-setting' => 'monitor']) }}
                <label for="show_fuel_breakdown">
                    Show Fuel Breakdown Table<br>
                    <p class="text-info"><strong>Note</strong> Fuel breakdowns are only available when equipment unit breakdown tables are enabled.</p>
                </label>
            </div>
        </div>
        <div class="col-sm-12">
            <div class="ckbox ckbox-default">
                {{ Form::checkbox('show_fuel_breakdown_total', 1, true, ['id' => 'show_fuel_breakdown_total', 'data-checked-setting' => 'monitor']) }}
                <label for="show_fuel_breakdown_total">
                    Show Totals on Fuel Breakdown Table<br>
                    <p class="text-info"><strong>Note</strong> Fuel breakdown totals are only available when equipment unit breakdown tables are enabled.</p>
                </label>
            </div>
        </div>
        <div class="col-sm-12">
            <div class="ckbox ckbox-default">
                {{ Form::checkbox('show_property_breakdown', 1, true, ['id' => 'show_property_breakdown', 'data-checked-setting' => 'monitor']) }}
                <label for="show_property_breakdown">Show Property Breakdown Table</label>
            </div>
        </div>
        <div class="col-sm-12">
            <div class="ckbox ckbox-default">
                {{ Form::checkbox('show_property_breakdown_total', 1, true, ['id' => 'show_property_breakdown_total', 'data-checked-setting' => 'monitor']) }}
                <label for="show_property_breakdown_total">Show Totals on Property Breakdown Table</label>
            </div>
        </div>
    </fieldset>
</div>