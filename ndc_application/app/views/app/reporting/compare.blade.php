@extends('layouts.master')

@section('pageTitle')
    <i class="fa fa-search"></i> Search Comparison
@stop
<?php $hideFeatures = true; ?>
@section('content')

    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title"><i class="fa fa-search"></i> Overview</h3>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-sm-12 col-md-12">
                            <ul>
                                <li>There are
                                    <strong>{{ (count($oldRecordIDs) + count($newWorkRecordIDs)) - count($intersection) }}</strong>
                                    records between both report/search queries (taking differences into account).
                                </li>
                                <li>The <strong>first report/search</strong> contains
                                    <strong>{{ count($oldRecordIDs) }}</strong> records.
                                </li>
                                <li>The <strong>second report/search</strong> contains
                                    <strong>{{ count($newWorkRecordIDs) }}</strong> records.
                                </li>
                                <li>The <strong>first report/search</strong> contains
                                    <strong>{{ count(array_diff($oldRecordIDs, $newWorkRecordIDs)) }}</strong> records
                                    that are not in the second report/search.
                                </li>
                                <li>The <strong>second report/search</strong> contains
                                    <strong>{{ count(array_diff($newWorkRecordIDs, $oldRecordIDs)) }}</strong> records
                                    that are not in the first report/search.
                                </li>
                                <li>There are <strong>{{ count($intersection) }}</strong> record(s) that appear in both
                                    report/search queries.
                                </li>
                            </ul>
                        </div>
                    </div>
                    <p>The filters used in both report/search queries (if available) were:</p>

                    <div class="row">
                        <div class="col-sm-6">
                            <h4>First Report/Search</h4>
                            <dl class="dl-horizontal">
                                {{ $oldFilterText }}
                            </dl>
                        </div>
                        <div class="col-sm-6">
                            <h4>Second Report/Search</h4>
                            <dl class="dl-horizontal">
                                {{ HTML::filterBlock('activity_filter_text', 'activityFilerMode', 'Activity') }}
                                {{ HTML::filterBlock('department_filter_text', 'departmentFilterMode', 'Department') }}
                                {{ HTML::filterBlock('district_filter_text', 'districtFilterMode', 'District') }}
                                {{ HTML::filterBlock('employee_filter_text', 'employeeFilterMode', 'Employee') }}
                                {{ HTML::filterBlock('equipment_filter_text', 'equipFilterMode', 'Equipment Unit') }}
                                {{ HTML::filterBlock('material_filter_text', 'materialFilterMode', 'Material') }}
                                {{ HTML::filterBlock('project_filter_text', 'projectFilterMode', 'Project') }}
                                {{ HTML::filterBlock('road_filter_text', 'roadFilterMode', 'Road') }}
                                {{ HTML::filterBlock('property_filter_text', 'propertyFilterMode', $propertyHeader) }}
                            </dl>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
            <ul class="nav nav-tabs">
                <li class="active bb_property interaction_trigger"><a href="#lwSame" data-toggle="tab"><strong><i
                                    class="fa fa-table"></i> Similar Records</strong></a></li>
                <li class="bb_material interaction_trigger"><a href="#lwNotInFirst" data-toggle="tab"><strong><i
                                    class="fa fa-table"></i> Records Not in First Report/Search Query</strong></a></li>
                <li class="bb_equip interaction_trigger"><a href="#lwNotInSecond" data-toggle="tab"><strong><i
                                    class="fa fa-table"></i> Records Not in Second Report/Search Query</strong></a></li>
            </ul>
            <div class="tab-content mb30 nopadding" id="interactionContainer"
                 style="margin-top: -1px;border-top-width: 2px;border-top-style: solid;border-top-color: rgb(66, 139, 202);">
                <div class="tab-pane active nopadding" id="lwSame">
                    @if (count($sameRecords) == 0)
                        <div class="panel-body">
                            <p style="padding: 10px 8px 0px 0px;">There are no records that exist in both report/search queries.</p>
                        </div>
                    @else
                        <p style="padding: 10px 8px 0px 0px;">The following records appear in both the first and second report/search query results.</p>
                        @include('app.work.partials.table', ['workRecords' => $sameRecords])
                    @endif
                </div>
                <div class="tab-pane nopadding" id="lwNotInFirst">
                    @if (count($notInOldRecords) == 0)
                        <div class="panel-body">
                            <p style="padding: 10px 8px 0px 0px;">There are no records in the second report/search query that do not appear in the first report/search query results.</p>
                        </div>
                    @else
                        <p style="padding: 10px 8px 0px 0px;">The following records appear in the second (most recent) report/search query, but not in the first report/search query results.</p>
                        @include('app.work.partials.table', ['workRecords' => $notInOldRecords])
                    @endif
                </div>
                <div class="tab-pane nopadding" id="lwNotInSecond">
                    @if (count($notInNewRecords) == 0)
                        <div class="panel-body">
                            <p style="padding: 10px 8px 0px 0px;">There are no records in the first report/search query that do not appear in the second report/search query results.</p>
                        </div>
                    @else
                        <p style="padding: 10px 8px 0px 0px;">The following records appear in the first report/search query, but in the second (most recent) report/search query results.</p>
                        @include('app.work.partials.table', ['workRecords' => $notInNewRecords])
                    @endif
                </div>
            </div>
        </div>
    </div>

@stop