<h4><i class="fa fa-clock-o"></i> Recent Account Activity</h4>
<div class="activity-list">{{ $activityStream }}</div>
<div class="row">
    <div class="col-sm-12 text-center">
        <a class="btn btn-block btn-primary" href="{{ url('/recent-activity') }}"><i class="fa fa-clock-o"></i> See more activity...</a>
    </div>
</div>