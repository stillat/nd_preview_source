<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-life-ring"></i> User Manual</h3>
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col-sm-12">
                <img src="{{ app_url()  }}/man/pdf.png" class="pull-left clear-fix" style="width: 50px; margin-right: 2px;">
                <p>Download the latest version of the ND Counties manual here.</p>
            </div>
            <div class="col-sm-12">
                <p><strong>Last Updated: {{ Carbon::createFromTimeStamp(filectime(public_path('man/ND Counties.pdf')))->diffForHumans() }}</strong></p>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">

                <p><a href="{{ app_url() }}/man/ND Counties.pdf" target="_blank" class="btn btn-primary btn-block" download>Download Latest Manual</a></p>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="downloadManual" tabindex="-1" role="dialog" aria-labelledby="downloadManualLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 id="downloadManualLabel"><i class="fa fa-life-raft"></i> Download your copy of the manual</h4>
            </div>
            <div class="modal-body">
                <p>Be sure to download your copy of the user manual. It contains <strong>important</strong> information as well answers to <strong>common questions</strong>. It also contains complete coverage of the reporting system.</p>
                <p>The manual is updated often, so be sure to get a new copy each time it is updated. Download your copy of the manual from the dashboard by clicking the "Download Latest Manual" button.</p>
                <p>Close this window by clicking anywhere outside the window, by clicking the "X" in the corner of this window, or by pressing the escape key.</p>
            </div>

        </div>
    </div>
</div>