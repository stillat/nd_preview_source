<ul class="list-unstyled">
    <li><strong><i class="fa fa-calendar"></i> Today:</strong> {{{ Carbon::now()->toFormattedDateString() }}}</li>
    <li><strong><i class="fa fa-calendar"></i> Week: </strong> {{{ Carbon::now()->weekOfYear }}}</li>
    <li><strong><i class="fa fa-calendar"></i> Quarter: </strong> {{{ Carbon::now()->quarter }}}</li>
</ul>