<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title">Quick Actions</h3>
    </div>
    <table class="table table-condensed">
        <tbody>
            <tr>
                <td><a href="{{{ action('App\Work\WorkLogsController@create') }}}"><i class="fa fa-coffee"></i> Add Work Log</a></td>
                <td><a href="{{{ action('App\Work\WorkLogsController@index') }}}">View Work Logs</a></td>
            </tr>
            <tr>
                <td><a href="{{{ action('App\Reporting\ReportsController@getRun') }}}"><i class="glyphicon glyphicon-stats"></i> Run a report</a></td>
            </tr>
            <tr>
                <td><a href="{{{ url('/settings/account') }}}"><i class="fa fa-cog"></i> Account Settings</a></td>
            </tr>
        </tbody>
    </table>
</div>