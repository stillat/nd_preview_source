<ul id="supportTab" class="nav nav-tabs nopadding" role="tablist">
    <li role="presentation" class="active bb_support_center"><a href="#support" id="support-tab" role="tab" data-toggle="tab"
                                              aria-controls="home" aria-expanded="true"><i class="fa fa-life-ring"></i>
            Support Center</a></li>
    <li role="presentation" class="bb_forums"><a href="#forums" role="tab" id="forums-tab" data-toggle="tab"
                                        aria-controls="profile" aria-expanded="false"><i class="fa fa-comments-o"></i>
            Community Forums</a></li>
</ul>

<div id="supportTabContent" class="tab-content nopadding">
    <div role="tabpanel" class="tab-pane fade active in" id="support" aria-labelledby="home-tab">
        <div>
            <form action="https://ndcounties.com/support" method="GET" class="form-horizontal" target="_blank">
                <input type="hidden" name="origin" value="ndc_application">
                <input type="hidden" name="ht-kb-search" value="1">

                <div class="input-group mb15">
                    <input type="text" name="s" placeholder="Search the knowledge base" class="form-control"
                           style="height: 36px;">
                  <span class="input-group-btn">
                    <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                  </span>
                </div>
            </form>
        </div>
        <style>#supportFeed h1, h2 ,h3,h4,h5,h6 { font-size: 15px;font-weight: bolder;}</style>
        <div id="supportFeed"></div>
    </div>
    <div role="tabpanel" class="tab-pane fade" id="forums" aria-labelledby="profile-tab">
        <div id="forumFeed"></div>
    </div>
</div>