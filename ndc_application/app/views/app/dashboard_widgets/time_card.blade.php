<div class="panel panel-primary panel-alt widget-today">
    <div class="panel-heading text-center">
        <i class="fa fa-calendar-o"></i>
    </div>
    <div class="panel-body text-center">
        <h3 class="today">{{{ Carbon::now()->toFormattedDateString() }}} <small>Week {{{ Carbon::now()->weekOfYear }}}</small></h3>
    </div>
</div>