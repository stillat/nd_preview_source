<div class="panel panel-default">
    <div class="panel-body nopadding">
        @if(strlen($currentAccount->state) >= 2 && strlen($currentAccount->zip) > 0)
        <div id="weatherSystem" style="background: #3498DB;color: white;padding: 20px;"></div>
        @else
        <div style="min-height: 350px;background: #3498DB;padding:20px;padding-top:10%" class="text-center">
            <h2 style="color:white;"><span class="wi wi800"></span> ND Counties Weather</h2>
            <p class="lead" style="color:white;">We cannot display weather information because we don't have enough address data.</p>
            <p><a href="{{ url('settings/account') }}" style="color:white;">Set your account's location details here</a></p>
        </div>
        @endif
    </div>
</div>