<div class="card hovercard">
    <div class="cardheader" style="background: url('{{ app_url() }}/images/ndc_bg_def.png');">

    </div>
    <div class="avatar">
        <img style="background-color: white" src="{{{ $userProfileImage }}}">
    </div>
    <div class="info">
        <div class="title" style="color: #428bca;">
            {{{ str_limit($authUser->first_name.' '.$authUser->last_name, 15, '...') }}}
        </div>
        <div class="desc">{{{ $currentAccount->account_name }}}</div>
        <div class="desc">
            @if(strlen($currentAccount->state) > 0)
                {{{ $currentAccount->state }}},
            @endif
            {{{ $currentAccount->country }}}
        </div>
    </div>
    <div class="bottom">
        <a class="btn btn-white btn-sm" href="{{{ url('work-logs/create') }}}" data-toggle="popover" data-trigger="hover" data-placement="top" data-container="body" data-content="Add a new work record">
            <i class="fa fa-coffee"></i>
        </a>
        <a class="btn btn-white btn-sm" href="{{{ url('reporting/run') }}}" data-toggle="popover" data-trigger="hover" data-placement="top" data-container="body" data-content="Run a report or search">
            <i class="fa ndc-chart-pie"></i>
        </a>
        <a class="btn btn-white btn-sm" href="{{{ url('settings/account') }}}" data-toggle="popover" data-trigger="hover" data-placement="top" data-container="body" data-content="Update your account's settings">
            <i class="fa fa-cog"></i>
        </a>
    </div>
</div>