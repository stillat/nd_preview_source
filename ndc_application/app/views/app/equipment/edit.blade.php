@extends('layouts.master')

@section('pageTitle')
<i class="fa fa-plus"></i> <strong>{{{ limit_code($equipmentUnit->code) }}}</strong> - {{{ limit_name($equipmentUnit->name) }}}
@stop

@section('buttonBar')
<div class="btn-group">
    <a href="{{ action('App\EquipmentUnits\UnitsController@index') }}" class="btn btn-danger ndc_form_leave">Cancel</a>
</div>
@stop

@section('content')
<div class="row">

    <div class="col-md-12">
        {{ Form::open(array('route' => array('equipment-units.update', $equipmentUnit->id), 'method' => 'put', 'class' => 'form-horizontal')) }}
        <div class="panel-primary panel-alt">
            <div class="panel-body">
                <div class="form-group">
                    <label class="col-sm-2 control-label" for="code">Code:</label>
                    <div class="col-sm-2">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="glyphicon glyphicon-barcode"></i></span>
                            <input type="text" id="code" name="code" class="form-control text-right mousetrap ndc_form_home_element {{{ user_form_size() }}}" value="{{{ Input::old('code', $equipmentUnit->code) }}}" autofocus autocomplete="off">
                        </div>
                        {{ Form::errorMsg('code') }}
                    </div>
                </div>

                <div class="form-group is-required">
                    <label class="col-sm-2 control-label" for="name">Name:</label>
                    <div class="col-sm-8">
                        <input type="text" id="name" name="name" class="form-control mousetrap {{{ user_form_size() }}}" value="{{{ Input::old('name', $equipmentUnit->name) }}}">
                        {{ Form::errorMsg('name') }}
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-2 control-label" for="description">Description:</label>
                    <div class="col-sm-8">
                        <textarea id="description" name="description" class="form-control mousetrap {{{ user_form_size() }}}">{{{ Input::old('description', $equipmentUnit->description) }}}</textarea>
                        {{ Form::errorMsg('description') }}
                    </div>
                </div>

                <div class="form-group is-required">
                    <label class="col-sm-2 control-label" for="serial_number">Serial Number:</label>
                    <div class="col-sm-5">
                        <input type="text" id="serial_number" name="serial_number" class="form-control mousetrap {{{ user_form_size() }}}" value="{{{ Input::old('serial_number', $equipmentUnit->serial_number) }}}">
                        {{ Form::errorMsg('serial_number') }}
                    </div>
                </div>

                <div class="form-group is-required">
                    <label class="col-sm-2 control-label" for="rental_rate">Rental Rate:</label>
                    <div class="col-sm-3">
                        <div class="input-group">
                            <span class="input-group-addon">$</span>
                            <input type="number" min="0" step="any" id="rental_rate" name="rental_rate" class="form-control text-right mousetrap {{{ user_form_size() }}}" value="{{{ nd_number_format(Input::old('rental_rate', nd_number_format($equipmentUnit->rental_rate))) }}}">
                        </div>
                        {{ Form::errorMsg('rental_rate') }}
                    </div>

                    <label class="col-sm-2 control-label" for="unit_id">Rental Rate Unit:</label>
                    <div class="col-sm-3">
                        {{ Form::select('unit_id', $measurementUnits, Input::old('unit_id', $equipmentUnit->unit_id), array('style' => 'width: 100%;', 'class' => 'form-control mousetrap chosen-select'.user_form_size(), 'id' => 'unit_id')) }}
                        {{ Form::errorMsg('unit_id') }}
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-2 control-label" for="make">Make:</label>
                    <div class="col-sm-2">
                        <input type="text" id="make" name="make" class="form-control text-right mousetrap {{{ user_form_size() }}}" placeholder="Make" value="{{{ Input::old('make', $equipmentUnit->make) }}}">
                        {{ Form::errorMsg('make') }}
                    </div>

                    <label class="col-sm-1 control-label" for="model">Model:</label>
                    <div class="col-sm-2">
                        <input type="text" id="model" name="model" class="form-control text-right mousetrap {{{ user_form_size() }}}" placeholder="Model" value="{{{ Input::old('model', $equipmentUnit->model) }}}">
                        {{ Form::errorMsg('model') }}
                    </div>

                    <label class="col-sm-1 control-label" for="year">Year:</label>
                    <div class="col-sm-2">
                        <input type="number" step="0" id="year" name="year" class="form-control text-right mousetrap {{{ user_form_size() }}}" value="{{{ Input::old('year', $equipmentUnit->year) }}}" placeholder="1990">
                        {{ Form::errorMsg('year') }}
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-2 control-label" for="purchase_cost">Purchase Cost:</label>
                    <div class="col-sm-2">
                        <div class="input-group">
                            <span class="input-group-addon">$</span>
                            <input type="number" min="0" step="any" id="purchase_cost" name="purchase_cost" class="form-control text-right mousetrap {{{ user_form_size() }}}" value="{{{ nd_number_format(Input::old('purchase_cost', nd_number_format($equipmentUnit->purchase_cost))) }}}">
                        </div>
                        {{ Form::errorMsg('purchase_cost') }}
                    </div>

                    <label class="col-sm-1 control-label" for="salvage_value">Salvage Value:</label>
                    <div class="col-sm-2">
                        <div class="input-group">
                            <span class="input-group-addon">$</span>
                            <input type="number" min="0" step="any" id="salvage_value" name="salvage_value" class="form-control text-right mousetrap {{{ user_form_size() }}}" value="{{{ nd_number_format(Input::old('salvage_value', nd_number_format($equipmentUnit->salvage_value))) }}}">
                        </div>
                        {{ Form::errorMsg('salvage_value') }}
                    </div>

                    <label class="col-sm-1 control-label" for="lifetime">Lifetime:</label>
                    <div class="col-sm-2">
                        <input type="number" min="0" step="0" id="lifetime" name="lifetime" class="form-control text-right mousetrap ndc_form_trail_element {{{ user_form_size() }}}" value="{{{ Input::old('lifetime', $equipmentUnit->lifetime) }}}" placeholder="Lifetime in years">
                        {{ Form::errorMsg('lifetime') }}
                    </div>
                </div>

            </div>
            <div class="panel-footer col-sm-offset-2">
                <div class="btn-group mr5">
                    <button type="submit" id="ndc_form_defaultaction" name="submit" class="btn btn-primary">Update this equipment unit</button>
                </div>

                <div class="btn-group mr5">
                    <button type="reset" id="ndc_form_reset" class="btn btn-default">Reset</button>
                </div>
            </div>
        </div>
        {{ Form::close() }}
    </div>
</div>
@stop