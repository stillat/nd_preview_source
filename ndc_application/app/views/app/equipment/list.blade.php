@extends('layouts.master')

@section('pageTitle')
<i class="fa fa-truck"></i> Equipment Unit Properties
@stop

@section('buttonBar')
<div class="btn-group">
   <a href="{{ action('App\EquipmentUnits\UnitsController@create') }}" class="btn btn-white nd-add"><i class="fa fa-plus inline-icon"></i> New Equipment Unit</a>
   <button type="button" class="btn btn-white dropdown-toggle" data-toggle="dropdown">
    <span class="caret"></span>
    <span class="sr-only">Toggle Dropdown</span>
</button>
<ul class="dropdown-menu" role="menu">
    <li><a href="{{ action('App\Properties\PropertyTypesController@create') }}">New Property Type</a></li>
</ul>
</div>
@stop

@section('content')

<div class="row">

    <div class="col-md-12">
        {{ $equipmentUnits->links() }}
    </div>

    <div class="col-md-12">
        @include('app.equipment.partials.table')
    </div>

    <div class="col-md-12">
        {{ $equipmentUnits->links() }}
    </div>

</div>

@include('layouts.dialogs.remove', array('title' => 'Remove Equipment Unit', 'action' => 'App\EquipmentUnits\UnitsController@destroy'))
@stop