@extends('layouts.master')

@section('pageTitle')
<i class="fa fa-plus"></i> You are adding a new Equipment Unit
@stop

@section('buttonBar')
<div class="btn-group">
    <a href="{{ action('App\EquipmentUnits\UnitsController@index') }}" class="btn btn-danger ndc_form_leave">Cancel</a>
</div>
@stop

@section('content')
<div class="row">

    <div class="col-md-12">
        {{ Form::open(array('route' => 'equipment-units.store', 'method' => 'post', 'class' => 'form-horizontal')) }}
        <div class="panel-primary panel-alt">
            <div class="panel-body">
                <div class="form-group">
                    <label class="col-sm-2 control-label" for="code">Code:</label>
                    <div class="col-sm-2">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="glyphicon glyphicon-barcode"></i></span>
                            <input type="text" id="code" name="code" class="form-control text-right mousetrap ndc_form_home_element {{{ user_form_size() }}}" value="{{{ Input::old('code', '') }}}" autofocus autocomplete="off">
                        </div>
                        {{ Form::errorMsg('code') }}
                    </div>
                </div>

                <div class="form-group is-required">
                    <label class="col-sm-2 control-label" for="name">Name:</label>
                    <div class="col-sm-8">
                        <input type="text" id="name" name="name" class="form-control mousetrap {{{ user_form_size() }}}" value="{{{ Input::old('name', '') }}}">
                        {{ Form::errorMsg('name') }}
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-2 control-label" for="description">Description:</label>
                    <div class="col-sm-8">
                        <textarea id="description" name="description" class="form-control mousetrap {{{ user_form_size() }}}">{{{ Input::old('description', '') }}}</textarea>
                        {{ Form::errorMsg('description') }}
                    </div>
                </div>

                <div class="form-group is-required">
                    <label class="col-sm-2 control-label" for="serial_number">Serial Number:</label>
                    <div class="col-sm-5">
                        <input type="text" id="serial_number" name="serial_number" class="form-control mousetrap {{{ user_form_size() }}}" value="{{{ Input::old('serial_number', '') }}}">
                        {{ Form::errorMsg('serial_number') }}
                    </div>
                </div>

                <div class="form-group is-required">
                    <label class="col-sm-2 control-label" for="rental_rate">Rental Rate:</label>
                    <div class="col-sm-3">
                        <div class="input-group">
                            <span class="input-group-addon">$</span>
                            <input type="number" min="0" step="any" id="rental_rate" name="rental_rate" class="form-control text-right mousetrap {{{ user_form_size() }}}" value="{{{ nd_number_format(Input::old('rental_rate', '0.00')) }}}">
                        </div>
                        {{ Form::errorMsg('rental_rate') }}
                    </div>

                    <label class="col-sm-2 control-label" for="unit_id">Rental Rate Unit:</label>
                    <div class="col-sm-3">
                        {{ Form::select('unit_id', $measurementUnits, Input::old('unit_id', 0), array('style' => 'width: 100%;', 'id' => 'unit_id', 'class' => 'form-control mousetrap chosen-select'.user_form_size())) }}
                        {{ Form::errorMsg('unit_id') }}
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-2 control-label" for="make">Make:</label>
                    <div class="col-sm-2">
                        <input type="text" id="make" name="make" class="form-control text-right mousetrap {{{ user_form_size() }}}" placeholder="Make" value="{{{ Input::old('make', '') }}}">
                        {{ Form::errorMsg('make') }}
                    </div>

                    <label class="col-sm-1 control-label" for="model">Model:</label>
                    <div class="col-sm-2">
                        <input type="text" id="model" name="model" class="form-control text-right mousetrap {{{ user_form_size() }}}" placeholder="Model" value="{{{ Input::old('model', '') }}}">
                        {{ Form::errorMsg('model') }}
                    </div>

                    <label class="col-sm-1 control-label" for="year">Year:</label>
                    <div class="col-sm-2">
                        <input type="number" step="0" id="year" name="year" class="form-control text-right mousetrap {{{ user_form_size() }}}" value="{{{ Input::old('year', '') }}}" placeholder="1990">
                        {{ Form::errorMsg('year') }}
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-2 control-label" for="purchase_cost">Purchase Cost:</label>
                    <div class="col-sm-2">
                        <div class="input-group">
                            <span class="input-group-addon">$</span>
                            <input type="number" min="0" step="any" id="purchase_cost" name="purchase_cost" class="form-control text-right mousetrap {{{ user_form_size() }}}" value="{{{ nd_number_format(Input::old('purchase_cost', '0.00')) }}}">
                        </div>
                        {{ Form::errorMsg('purchase_cost') }}
                    </div>

                    <label class="col-sm-1 control-label" for="salvage_value">Salvage Value:</label>
                    <div class="col-sm-2">
                        <div class="input-group">
                            <span class="input-group-addon">$</span>
                            <input type="number" min="0" step="any" id="salvage_value" name="salvage_value" class="form-control text-right mousetrap {{{ user_form_size() }}}" value="{{{ nd_number_format(Input::old('salvage_value', '0.00')) }}}">
                        </div>
                        {{ Form::errorMsg('salvage_value') }}
                    </div>

                    <label class="col-sm-1 control-label" for="lifetime">Lifetime:</label>
                    <div class="col-sm-2">
                        <input type="number" min="0" step="0" id="lifetime" name="lifetime" class="form-control text-right mousetrap ndc_form_trail_element {{{ user_form_size() }}}" value="{{{ Input::old('lifetime', '') }}}" placeholder="Lifetime in years">
                        {{ Form::errorMsg('lifetime') }}
                    </div>
                </div>

            </div>
            <div class="panel-footer col-sm-offset-2">
                <div class="btn-group mr5">
                    <button type="submit" id="ndc_form_defaultaction" value="addNew" name="submit" class="btn btn-primary">Save and add another equipment unit</button>
                    <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
                        <span class="caret"></span>
                        <span class="sr-only">Toggle Dropdown</span>
                    </button>
                    <ul class="dropdown-menu" role="menu">
                        <li><button type="submit" id="ndc_form_secondaryaction" value="return" name="submit" class="btn btn-white btn-md">Save and return to previous action</button></li>
                    </ul>
                </div>

                <div class="btn-group mr5">
                    <button type="reset" id="ndc_form_reset" class="btn btn-default">Reset</button>
                </div>
            </div>
        </div>
        {{ Form::close() }}
    </div>
</div>
@stop