<div class="profile-header">
    <div class="table-responsive">
        <table class="table table-hover table-striped table-fixed">
            <thead>
                <tr>
                    <th class="wd-100">Code</th>
                    <th>Name</th>
                    <th>Serial Number</th>
                    <th class="text-right">Rental Rate</th>
                    <th>Rental Unit</th>
                    <th>Description</th>
                    <th>Year</th>
                    <th>Make</th>
                    <th>Model</th>
                    <th>Purchase Cost</th>
                    <th>Salvage Value</th>
                    <th>Lifetime</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td><strong>{{{ $equipmentUnit->code }}}</strong></td>
                    <td>{{{ $equipmentUnit->name }}}</td>
                    <td>{{{ $equipmentUnit->serial_number }}}</td>
                    <td class="text-right">{{{ nd_number_format($equipmentUnit->rental_rate) }}}</td>
                    <td>{{{ $measurementUnit->name }}}</td>
                    <td>{{{ if_null_then_na($equipmentUnit->description) }}}</td>
                    <td>{{{ if_null_then_na($equipmentUnit->year) }}}</td>
                    <td>{{{ if_null_then_na($equipmentUnit->make) }}}</td>
                    <td>{{{ if_null_then_na($equipmentUnit->model) }}}</td>
                    <td>{{{ if_null_then_na(nd_number_format($equipmentUnit->purchase_cost)) }}}</td>
                    <td>{{{ if_null_then_na(nd_number_format($equipmentUnit->salvage_value)) }}}</td>
                    <td>{{{ if_null_then_na($equipmentUnit->lifetime) }}}</td>
                </tr>
            </tbody>
        </table>
    </div>
    <div class="mb20"></div>
    <a href="{{ route('equipment-units.edit', array($equipmentUnit->id)) }}" class="btn btn-white"><i class="fa fa-edit"></i> Update equipment unit</a>
</div>