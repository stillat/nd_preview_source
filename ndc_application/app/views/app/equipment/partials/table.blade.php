<div class="table-responsive">
    <table class="table table-hover table-striped mb30 table-fixed">
        <thead>
            <tr>
                <th class="wd-200">Code</th>
                <th class="wd-200">Name</th>
                <th class="wd-200">Serial Number</th>
                <th>Description</th>
                <th class="text-right wd-100">Rental Rate</th>
                <th class="text-right wd-100">Unit</th>
                <th class="wd-100"></th>
            </tr>
        </thead>
        <tbody>
            @foreach($equipmentUnits as $equipmentUnit)
            <tr>
                <td>
                    <a href="{{ route('equipment-units.show', array($equipmentUnit->id)) }}"><strong>{{{ limit_code($equipmentUnit->code) }}}</strong></a>
                </td>
                <td>
                    <a href="{{ route('equipment-units.show', array($equipmentUnit->id)) }}">{{{ limit_name($equipmentUnit->name) }}}</a>
                </td>
                <td>
                    <a href="{{ route('equipment-units.show', array($equipmentUnit->id)) }}">{{{ $equipmentUnit->serial_number }}}</a>
                </td>
                <td>
                    <a href="{{ route('equipment-units.show', array($equipmentUnit->id)) }}">{{{ limit_description($equipmentUnit->description) }}}</a>
                </td>
                <td class="text-right">
                    <a href="{{ route('equipment-units.show', array($equipmentUnit->id)) }}">{{{ nd_number_format($equipmentUnit->rental_rate) }}}</a>
                </td>
                <td class="text-right">
                    <a href="{{ route('equipment-units.show', array($equipmentUnit->id)) }}">{{{ $measurementUnits[$equipmentUnit->unit_id] }}}</a>
                </td>
                <td class="table-action td-ignore">
                <a href="{{ route('equipment-units.edit', array($equipmentUnit->id)) }}"><i class="fa fa-pencil"></i></a>
                    <a class="cursor-pointer delete-row" data-rcontext="{{{ $equipmentUnit->id }}}"><i class="fa fa-trash-o"></i></a>
                </td>
                
            </tr>
            @endforeach
        </tbody>
    </table>
</div>