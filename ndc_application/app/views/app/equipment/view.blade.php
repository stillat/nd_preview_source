@extends('layouts.master')

@section('pageTitle')
<i class="fa fa-truck"></i> {{{ limit_code($equipmentUnit->code) }}} - <small>{{{ limit_name($equipmentUnit->name) }}}</small>
@stop

@section('buttonBar')
<div class="btn-group pull-up-10">
    {{ HTML::modelNaviateToolbar('equipment-units.show', $equipmentUnit->previous(), 'equipment-units.index', 'equipment-units.show', $equipmentUnit->next()) }}
</div>
<div class="btn-group pull-up-10">
    <a href="{{ route('equipment-units.edit', array($equipmentUnit->id)) }}" class="btn btn-white"><i class="fa fa-edit inline-icon"></i> Update equipment unit</a>
</div>
@stop

@section('content')
<div class="row">
    <div class="col-md-12">
    @include('app.equipment.partials.detail_table')
    </div>
</div>
@stop