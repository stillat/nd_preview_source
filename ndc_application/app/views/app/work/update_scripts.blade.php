<script>

    var is_wlc = true;

    var WL_IS_EQUIPMENT_COMPATIBILITY_MODE = Boolean({{ $workResetSettings->wl_enable_equipment_compatibility }});
    var WL_COMPATIBILITY_HIDE_INVOICE = Boolean({{ $workResetSettings->wl_enable_invoice_compatibility }});
    var propertyTypes = {{ $types->toJson() }};

    var originalInvoice = {{{ get_property($invoice, 'id', 'null') }}};

    var autoClearForms = Boolean({{ user_settings()['auto_clear_forms'] }});


            @foreach($workResetSettings as $setting => $settingValue)

            @if($setting !== '__COMMENT')

            var NDC_{{{ strtoupper($setting)  }}} = Boolean({{{ $settingValue }}});

            @endif


            @endforeach

        @if($createLike)

            var WL_CREATOR_UPDATE_MODE = false;

            @else

            var WL_CREATOR_UPDATE_MODE = true;

            @endif


            @if($createLike)

            var WL_IS_CREATE_LIKE = true;

            @else

            var WL_IS_CREATE_LIKE = false;

            @endif


            var WL_RECORD = {{ json_encode($record->rawData) }};

    var WL_INVOICE = {{ json_encode($invoice) }};

    var WL_TOTAL_INVOICE = {{{ $record->rawData->total_invoice }}};

</script>

<script src="{{{ app_url() }}}/assets/js/jsrender.min.js" defer></script>

<script src="{{{ app_url() }}}/assets/js/workentry.min.js{{ UI::getBuster() }}" defer></script>