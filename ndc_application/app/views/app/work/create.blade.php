@extends('layouts.master')

@section('pageTitle')
    <i class="fa fa-coffee"></i> Add Work Record
@stop

@section('buttonBar')
    <div class="btn-group n-mt-5">
        <p id="recordStatusLabel" class="btn" disabled="disabled"><strong>There have been no changes to this record</strong></p>
        <a href="{{ action('App\Work\WorkLogsController@show', [$lastRecordID]) }}" id="lastRecordLink" class="btn btn-white"><i class="fa fa-chevron-left inline-icon"></i></a>
        <a href="{{ Redirector::getRoute(route('work-logs.index')) }}" class="btn btn-danger ndc_form_leave">Cancel</a>
    </div>
@stop

@section('content')
    @include('app.work.dialogs.reset_work')
    <div class="row visible-xs">
        <div class="col-sm-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="dark"><i class="fa fa-frown-o"></i> Screen Size Too Small</h4>
                </div>
                <div class="panel-body">
                    <p class="lead">Hello there! Your screen is very small. This feature requires a large screen area
                        because of its vast layout. We have simply hidden things for now - trust us, everything is still
                        here.</p>

                    <p>You can continue working once you make your screen a little bit larger.</p>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
            <div class="alert alert-danger" id="createWorkFormErrorHandler" style="display: none;">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <strong>Almost there!</strong> There are some errors that need to be corrected before we can save this
                record.
            </div>
        </div>
    </div>

    <div id="createFormLoader">
        <div class="spinner"></div>
        <p style="margin-right:auto;margin-left: auto; width: 300px; text-align: center;">Getting things ready. Please
            wait.</p>
    </div>

    <div id="invoiceOverrideWarningHolder" style="display: none;">
        <div class="row">
            <div class="col-sm-12">
                <div class="alert alert-warning">
                    <p><strong>Invoice Override</strong> The balance due on the invoice was overridden. We will not
                        automatically recalculate the balance due for this record. You can re-enable automatic invoice
                        calculation, but this may change the balance due. <a href="#" id="reenableAutomaticInvoiceCalc">Re-enable
                            automatic invoice calculation.</a></p>
                </div>
            </div>
        </div>
    </div>

    <div class="panel hidden-xs" id="createFormHolder" style="display: none">
        <div id="replayFormHolder"></div>
        {{ Form::open(array('action' => 'App\Work\WorkLogsController@store', 'method' => 'POST', 'class' => 'form-horizontal', 'enctype' => 'application/x-www-form-urlencoded', 'id' => 'createWorkForm')) }}
        <div class="panel-body nopadding">
            <div class="row">
                <div class="col-md-12">
                    <ul class="nav nav-tabs">
                        <li class="active bb_property interaction_trigger"><a href="#lwGeneral"
                                                                              data-toggle="tab"><strong><i
                                            class="fa fa-user"></i> <span class="hidden-sm hidden-xs hidden-md">Employee Information</span></strong></a></li>
                        <li class="bb_material interaction_trigger"><a href="#lwMaterials" data-toggle="tab"><strong><i
                                            class="fa fa-cubes"></i> <span class="hidden-sm hidden-xs hidden-md">Materials</span></strong></a></li>
                        <li class="bb_equip interaction_trigger"><a href="#lwFuels" data-toggle="tab"><strong><i
                                            class="fa fa-truck"></i> <span class="hidden-sm hidden-xs hidden-md">Equipment
                                    <small>and</small>
                                    Fuels</span></strong></a></li>
                        <li class="bb_invoice interaction_trigger"><a href="#lwBilling" data-toggle="tab"><strong><i
                                            class="fa ndcf-bill"></i> <span class="hidden-sm hidden-xs hidden-md">Invoice
                                    <small>and</small>
                                    Billing</span></strong></a></li>
                        <li><a class="cursor-pointer" id="showMoreOptions"><i class="fa fa-chevron-right"></i></a></li>
                        <li class="bb_settings interaction_trigger more-option" style="display: none;"><a href="#lwSettings" data-toggle="tab"><strong><i
                                            class="fa fa-cogs"></i> <span class="hidden-sm hidden-xs hidden-md">Record Settings</span></strong></a></li>
                        <li style="display: none;" class="more-option"><a class="cursor-pointer" id="hideMoreOptions"><i class="fa fa-chevron-left"></i></a></li>
                    </ul>
                    <div class="tab-content mb30" id="interactionContainer"
                         style="margin-top: -1px;border-top-width: 2px;border-top-style: solid;border-top-color: rgb(66, 139, 202);">
                        <div class="tab-pane active" id="lwGeneral">
                            @include('app.work.tabs.general')
                        </div>
                        <div class="tab-pane" id="lwMaterials">
                            @include('app.work.tabs.materials')
                        </div>
                        <div class="tab-pane" id="lwFuels">
                            @include('app.work.tabs.fuels')
                        </div>
                        <div class="tab-pane" id="lwBilling">
                            @include('app.work.tabs.billing')
                        </div>
                        <div class="tab-pane" id="lwSettings">
                            @include('app.work.tabs.settings')
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="panel-footer">
            <div class="btn-group mr5">
                <button type="submit" id="ndc_form_defaultaction" value="addNew" name="submit" class="btn btn-primary">
                    Save and add another work entry
                </button>
            </div>
            <input type="hidden" name="invoice_override" value="0" id="invoiceOverrideNotifier">

            <div class="btn-group mr5">
                <button type="reset" id="ndc_form_reset" class="btn btn-default" tabindex="-1">Reset</button>
            </div>
        </div>
        {{ Form::close() }}
    </div>
@stop
@section('additionalScripts')
    <script>
        var is_wlc = true;
        var WL_IS_CREATE_LIKE = false;
        var WL_COMPATIBILITY_HIDE_INVOICE = Boolean({{ $workResetSettings->wl_enable_invoice_compatibility }});
        var WL_IS_EQUIPMENT_COMPATIBILITY_MODE = Boolean({{ $workResetSettings->wl_enable_equipment_compatibility }});
        var propertyTypes = {{ $types->toJson() }};
        var autoClearForms = Boolean({{ user_settings()['auto_clear_forms'] }});
        var autoClearDate = '{{{ $workDate->format('m/j/Y') }}}';
        @foreach($workResetSettings as $setting => $settingValue)
        @if($setting !== '__COMMENT')
        var NDC_{{{ strtoupper($setting)  }}} = Boolean({{{ $settingValue }}});
        @endif
        @endforeach
    </script>
    <script src="{{{ app_url() }}}/assets/js/jsrender.min.js" defer></script>
    <script src="{{{ app_url() }}}/assets/js/workentry.min.js{{ UI::getBuster() }}" defer></script>
@stop