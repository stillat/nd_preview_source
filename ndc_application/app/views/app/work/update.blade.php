@extends('layouts.master')

@section('pageTitle')
    @if($createLike)
        <i class="fa fa-coffee"></i> Add Work Record <span>Like <a
                    href="{{{ action('App\Work\WorkLogsController@show', array($record->getWorkID())) }}}">#{{{ $record->getWorkID() }}}</a></span>
    @else
        <i class="fa fa-pencil"></i> Work Record Update
    @endif
@stop

@section('buttonBar')
    <div class="btn-group n-mt-5">
        <p id="recordStatusLabel" class="btn" disabled="disabled"><strong>There have been no changes to this
                record</strong></p>
        {{ HTML::modelNaviateToolbar('work-logs.edit', $record->previous, 'work-logs.index', 'work-logs.edit', $record->next) }}
    </div>
    <div class="btn-group n-mt-5">
        <a href="{{ Redirector::getRoute(route('work-logs.index')) }}" class="btn btn-danger">Cancel</a>
    </div>
@stop


@section('content')
    @include('app.work.dialogs.reset_work')
    <div class="row visible-xs">

        <div class="col-sm-12">

            <div class="panel panel-default">

                <div class="panel-heading">

                    <h4 class="dark"><i class="fa fa-frown-o"></i> Screen Size Too Small</h4>

                </div>

                <div class="panel-body">

                    <p class="lead">Hello there! Your screen is very small. This feature requires a large screen area

                        because of its vast layout. We have simply hidden things for now - trust us, everything is still

                        here.</p>


                    <p>You can continue working once you make your screen a little bit larger.</p>

                </div>

            </div>

        </div>

    </div>


    <div id="invoiceOverrideWarningHolder" style="display: none;">

        <div class="row">

            <div class="col-sm-12">

                <div class="alert alert-warning">

                    <p><strong>Invoice Override</strong> The balance due on the invoice was overridden. We will not

                        automatically recalculate the balance due for this record. You can re-enable automatic invoice

                        calculation, but this may change the balance due. <a href="#" id="reenableAutomaticInvoiceCalc">Re-enable

                            automatic invoice calculation.</a></p>

                </div>

            </div>

        </div>

    </div>


    <div class="row">

        <div class="col-sm-12">

            <div class="alert alert-danger" id="createWorkFormErrorHandler" style="display: none;">

                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>

                <strong>Almost there!</strong> There are some errors that need to be corrected before we can save this

                record.

            </div>

        </div>

    </div>


    <div id="updateFormHolder">

        <div class="spinner"></div>

        <p style="margin-right:auto;margin-left: auto; width: 300px; text-align: center;">Getting things ready. Please

            wait.</p>

    </div>


    <div class="panel hidden-xs" id="updateFormHolder" style="display: none;">

        @if($createLike)

            {{ Form::open(array('action' => 'App\Work\WorkLogsController@store', 'method' => 'POST', 'class' => 'form-horizontal', 'enctype' => 'application/x-www-form-urlencoded', 'id' => 'createWorkForm')) }}

        @else

            {{ Form::open(array('action' => ['App\Work\WorkLogsController@update', $record->rawData->id], 'method' => 'PUT', 'class' => 'form-horizontal', 'enctype' => 'application/x-www-form-urlencoded', 'id' => 'updateWorkForm')) }}

        @endif

        <div class="panel-body nopadding">

            <div class="row">

                <div class="col-md-12">

                    <ul class="nav nav-tabs">
                        <li class="active bb_property interaction_trigger"><a href="#lwGeneral"
                                                                              data-toggle="tab"><strong><i
                                            class="fa fa-user"></i> <span class="hidden-sm hidden-xs hidden-md">Employee Information</span></strong></a>
                        </li>
                        <li class="bb_material interaction_trigger"><a href="#lwMaterials" data-toggle="tab"><strong><i
                                            class="fa fa-cubes"></i> <span class="hidden-sm hidden-xs hidden-md">Materials</strong></a></li>
                        <li class="bb_equip interaction_trigger"><a href="#lwFuels" data-toggle="tab"><strong><i
                                            class="fa fa-truck"></i> <span class="hidden-sm hidden-xs hidden-md">Equipment
                                    <small>and</small>
                                    Fuels</span></strong></a></li>
                        <li class="bb_invoice interaction_trigger"><a href="#lwBilling" data-toggle="tab"><strong><i
                                            class="fa ndcf-bill"></i> <span class="hidden-sm hidden-xs hidden-md">Invoice
                                    <small>and</small>
                                    Billing</span></strong></a></li>
                        <li><a class="cursor-pointer" id="showMoreOptions"><i class="fa fa-chevron-right"></i></a></li>
                        <li class="bb_settings interaction_trigger more-option" style="display: none;"><a href="#lwSettings" data-toggle="tab"><strong><i
                                            class="fa fa-cogs"></i> <span class="hidden-sm hidden-xs hidden-md">Record Settings</span></strong></a></li>
                        <li style="display: none;" class="more-option"><a class="cursor-pointer" id="hideMoreOptions"><i class="fa fa-chevron-left"></i></a></li>
                    </ul>

                    <div class="tab-content mb30" id="interactionContainer"

                         style="margin-top: -1px;border-top-width: 2px;border-top-style: solid;border-top-color: rgb(66, 139, 202);">

                        <div class="tab-pane active" id="lwGeneral">

                            @include('app.work.tabs.general_update')

                        </div>

                        <div class="tab-pane" id="lwMaterials">

                            @include('app.work.tabs.materials_update')

                        </div>

                        <div class="tab-pane" id="lwFuels">

                            @include('app.work.tabs.fuels_update')

                        </div>

                        <div class="tab-pane" id="lwBilling">

                            @include('app.work.tabs.billing_update')

                        </div>

                        <div class="tab-pane" id="lwSettings">

                            @include('app.work.tabs.settings_update')

                        </div>

                    </div>

                </div>

            </div>

        </div>

        <div class="panel-footer">

            <div class="btn-group">

                <input type="hidden" name="property_was_changed" id="propertyChangedHolder" value="0">

                <input type="hidden" name="filing_property_was_changed" id="filingPropertyChangedHolder" value="0">

                <input type="hidden" name="property_was_updated" id="propertyUpdatedHolder" value="0">

                <input type="hidden" name="invoice_was_removed" id="invoiceRemovedHolder" value="0">

                <input type="hidden" name="original_invoice_id" id="originalInvoiceIDHolder"

                       value="{{{ get_property($invoice, 'id', '') }}}">

                @if($createLike)

                    <div class="btn-group mr5">

                        <button type="submit" id="ndc_form_defaultaction" value="addNew" name="submit"

                                class="btn btn-primary">

                            Save and add another work entry

                        </button>

                        <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">

                            <span class="caret"></span>

                            <span class="sr-only">Toggle Dropdown</span>

                        </button>

                        <ul class="dropdown-menu" role="menu">

                            <li>

                                <button type="submit" id="ndc_form_secondaryaction" value="return" name="submit"

                                        class="btn btn-white btn-md">Save and return to previous action

                                </button>

                            </li>

                        </ul>

                    </div>

                @else

                    <button type="submit" id="ndc_form_defaultaction" class="btn btn-primary mr5">Update This Work

                        Record

                    </button>

                    <button type="reset" id="ndc_form_reset" class="btn btn-default" tabindex="-1">Reset</button>

                @endif

            </div>

        </div>

        {{ Form::close() }}

    </div>

@stop


@section('additionalScripts')

    @include('app.work.update_scripts')


@stop
