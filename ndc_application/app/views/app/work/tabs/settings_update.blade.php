<div class="row">
    <div class="col-sm-12">
        <p class="lead">Work log settings</p>

        <p>Work log settings are available to help customize the behavior of work records on an individual basis.</p>
    </div>
    <div class="col-sm-12">
        <div class="ckbox ckbox-primary">
            {{ Form::checkbox('exclude_from_reports', 1, Input::old('exclude_from_reports', $record->rawData->exclude_from_report), ['id' => 'chkExcludeFromReporting']) }}
            <label for="chkExcludeFromReporting"><strong>Exclude this work record from reports</strong><br>

                <p>Select this option to save progress on incomplete work entries, or for an experimental record.
                    Records that are excluded will <strong>not</strong> appear in reports.</p></label>
        </div>
    </div>
    <div class="col-sm-12">
        <div class="ckbox ckbox-primary">
            {{ Form::checkbox('credit_inventory', 1, Input::old('exclude_from_reports', $record->rawData->credit_inventory), ['id' => 'chkCreditInventory']) }}
            <label for="chkCreditInventory"><strong>Credit Inventory</strong><br><p>Select this option to credit inventory levels instead of deducting them. This option has no effect if inventory levels are not set to automatically adjust.</p></label>
        </div>
    </div>
</div>