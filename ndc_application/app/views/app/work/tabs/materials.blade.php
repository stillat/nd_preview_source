<div class="row">
    <div class="col-sm-8">
    </div>
    <div class="col-sm-4 text-right">
        <div class="btn-group">
            <button type="button" tabindex="-1" class="btn btn-white" data-bind="click: addMaterial"><i class="fa fa-plus"></i> Add Material</button>
            <button type="button" tabindex="-1" class="btn btn-white dropdown-toggle" data-toggle="dropdown">
                <span class="caret"></span>
                <span class="sr-only">Toggle Dropdown</span>
            </button>
            <ul class="dropdown-menu" role="menu" style="margin-left: -20px;">
                <li><a href="#" data-bind="click: removeMaterialAll">Remove all materials</a></li>
            </ul>
        </div>
    </div>
</div>

<div class="table-responsive">
    <table class="table table-hover table-striped">
        <thead>
            <th class="col-sm-4">Material</th>
            <th class="text-right col-sm-2">Quantity</th>
            <th class="text-right col-sm-2">Unit Cost</th>
            <th class="text-right col-sm-2">Total Cost</th>
            <th class="col-sm-2"></th>
        </thead>
        <tbody id="materials_holder">

        </tbody>
        <tfoot>
            <tr>
                <td colspan="3" class="text-right" style="vertical-align: middle;"><label style="font-weight: bold;">Total Material Cost</label></td>
                <td>
                    <div class="input-grouping">
                    <div class="input-group has-warning">
                        <span class="input-group-addon">$</span>
                        <input type="number" step="any" data-adjust="invoice_amount" tabindex="-1" name="total_materials_cost" id="total_materials_cost" class="form-control mousetrap text-right">
                    </div>
                    </div>
                </td>
                <td></td>
            </tr>
        </tfoot>
    </table>
</div>

<script type="text/html" id="material_tmpl">
    <%* createdMaterialsCount++ %>
    <tr data-material="<%:~getMaterialCount()%>" id="materialSelector<%:~getMaterialCount()%>_holder">
        <td>
           <div class="input-grouping">
           <input type="hidden" id="materialTrackingInventory_<%:~getMaterialCount()%>" name="material_tracking_inventory[]" data-related-selector="materialSelector_<%:~getMaterialCount()%>" value="<%>tracking_inventory%>">
           <input class="required form-control mousetrap highlight_ignore select_mat" id="materialSelector_<%:~getMaterialCount()%>" name="material[]" value="<%>id%>" data-rule-number="true">
           </div>
        </td>
        <td>
            <div class="input-grouping">
            <input class="required form-control mousetrap text-right" id="materialQuantity<%:~getMaterialCount()%>" name="materialQuantity[]" data-material="quantity" data-calculation="material_total" value="<%>quantity%>" data-rule-number="true">
            </div>
        </td>
        <td>
            <div class="input-grouping">
            <div class="has-success">
                <input class="required form-control mousetrap text-right" id="materialSelector<%:~getMaterialCount()%>_cost" name="materialUnitCost[]" data-material="unit_cost" data-calculation="material_total" value="<%>~ndc_number(unit_cost)%>" data-rule-number="true">
            </div>
            </div>
        </td>
        <td>
            <div class="input-grouping">
            <div class="has-warning">
                <input class="required form-control mousetrap text-right readonly" name="materialTotalCost[]" data-material="total_cost" data-result="material_total_cost" tabindex="-1" value="<%>~ndc_number(total_cost)%>" data-rule-number="true" data-msg-number="There is an error somewhere else. Fix that error to auto-calculate this field.">
            </div>
            </div>
        </td>
        <td><a class="btn btn-link" data-material="<%:~getMaterialCount()%>" data-bind="click: removeMaterial" id="remove_material_record_<%:~getMaterialCount()%>">Remove</a><input type="hidden" name="materialTaxable[]" value="<%>taxable%>" data-calc="taxable" data-material="<%:~getMaterialCount()%>"></td>
    </tr>
</script>