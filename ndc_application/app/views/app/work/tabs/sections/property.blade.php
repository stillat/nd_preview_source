<div class="form-group row accessibility-row">
    <input type="hidden" name="record_organization_actual_property" id="record_organization_actual_property_holder" value="0">
    <input type="hidden" name="record_organization_property_context" id="record_organization_property_context_holder" value="0">
    <input type="hidden" name="record_organization_property_type" id="record_organization_property_type" value="0">
    <div class="input-grouping">
        <label for="" class="col-sm-1 control-label"><i class="fa fa-road"></i> Road</label>
        <div class="col-sm-5">
            <div class="input-group"><span class="input-group-addon"><i class="fa fa-home" id="organizationPropertyIconHolder"></i></span>
                <input type="text" id="record_property_organization_selector" name="record_organization_property" class="form-control mousetrap" value="0">
            </div>
        </div>
		<div class="col-sm-6"><p class="form-control-static"><a id="trackPropertySelector" class="cursor-pointer">Add equipment repair <strong>or</strong> road length record</a></p></div>
    </div>
</div>

<div id="trackPropertyHolder" class="form-group row accessibility-row" <?php if (!user_settings()['auto_expand_more_options']) {
?>style="display: none;"<?php
		} ?>>
	<input type="hidden" name="record_actual_property" id="record_actual_property_holder" value="0">
	<input type="hidden" name="record_property_context" id="record_property_context_holder" value="0">
	<input type="hidden" name="record_property_type" id="record_property_type" value="0">
	<div class="input-grouping" >
	<label for="" class="col-sm-1 control-label">Property</label>
    	<div class="col-sm-5">
    		<div class="input-group"><span class="input-group-addon"><i class="fa fa-home" id="propertyIconHolder"></i></span>
    			<input type="text" id="record_property_selector" name="record_property" class="form-control mousetrap" value="0">
    		</div>
    	</div>
    	<div class="col-sm-6"><p class="form-control-static">Need to log <strong>road length</strong> or an <strong>equipment service repair</strong>? Choose your property from this drop-down.</p></div>
	</div>
</div>

<div class="form-group row accessibility-row">
@foreach($propertyAdapters as $adapter)
@if($adapter->getInsertView() !== null)
@include($adapter->getInsertView(), array('adapter' => $adapter))
@endif
@endforeach
</div>

<div class="row accessibility-row" id="propertyHelpReveal">
</div>