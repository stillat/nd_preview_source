<div class="form-group row accessibility-row">
   <div class="input-grouping">
   <label class="col-sm-1 control-label">Date</label>
        <div class="col-sm-2">
             <div class="input-group">
                  <span class="input-group-addon"><i class="fa fa-calendar-o"></i></span>
                  <input type="text" name="record_date" autocomplete="off" class="form-control mousetrap ndc_form_home_element" placeholder="mm/dd/yyyy" id="record_date" value="{{{ Input::old('record_date', $record->getManagerWorkDate()) }}}" data-rule-required="true" data-msg-required="A record date is required." data-rule-dateformat>
            </div>
      </div>
   </div>

   <div class="input-grouping">
   <label class="col-sm-1 control-label">Employee</label>
      <div class="col-sm-2">
       <div class="input-group">
            <span class="input-group-addon"><i class="fa fa-user"></i></span>
            <input type="hidden" name="employee_is_taxed" id="employeeIsTaxed" value="0">
            <input type="text" name="record_employee" id="record_employee" class="form-control mousetrap s2" data-calculation="employeeTotal" value="{{{ Input::old('record_employee', $record->getEmployee()->id) }}}">
      </div>
   </div>
   </div>

<div class="input-grouping">
<label class="col-sm-1 control-label">Regular Hours</label>
<div class="col-sm-2">
    <div class="input-grouping">
    <div class="input-group">
             <span class="input-group-addon"><i class="fa fa-clock-o"></i></span>
             <input type="number" step="any" id="regular_hours_worked" name="regular_hours_worked" class="form-control mousetrap text-right" data-calculation="employeeTotal" value="{{{ nd_number_format(Input::old('regular_hours_worked', $record->getRecordTotals()->employeeRegularHours)) }}}" data-rule-number="true" data-msg-number="Please enter a valid number" data-rule-required="true">
    </div>
    </div>
</div>
</div>

<div class="input-grouping {{ skip_class(1, $workResetSettings->wl_optional_employee_overtime) }}">
<label class="col-sm-1 control-label">Overtime Hours</label>
<div class="col-sm-2">
    <div class="input-grouping">
    <div class="input-group">
             <span class="input-group-addon"><i class="fa fa-clock-o"></i></span>
             <input type="number" step="any" id="overtime_hours_worked" name="overtime_hours_worked" class="form-control mousetrap text-right" data-calculation="employeeTotal" value="{{{ nd_number_format(Input::old('overtime_hours_worked', $record->getRecordTotals()->employeeOvertimeHours)) }}}" data-rule-number="true" data-rule-required="true" {{ skip_class(2, $workResetSettings->wl_optional_employee_overtime) }}>
       </div>
    </div>
</div>
</div>
</div>

<div class="form-group row accessibility-row has-success">
     <div class="input-grouping">
     <label class="col-sm-1 control-label">Hourly Rate</label>
          <div class="col-sm-2">
               <div class="input-grouping">
               <div class="input-group">
                    <span class="input-group-addon">$</span>
                    <input type="number" step="any" id="hourly_rate" name="hourly_rate" class="form-control mousetrap text-right" value="{{{ nd_number_format(Input::old('hourly_rate', $record->getHistoricWageInformation()->regular)) }}}" tabindex="-1" data-calculation="employeeTotal" value="0" data-rule-number="true" data-rule-required="true">
               </div>
               </div>
        </div>
     </div>

   <div class="input-grouping">
   <label class="col-sm-1 control-label">Overtime Rate</label>
      <div class="col-sm-2">
       <div class="input-grouping">
       <div class="input-group">
                   <span class="input-group-addon">$</span>
                   <input type="number" step="any" id="overtime_rate" name="overtime_rate" class="form-control mousetrap text-right" value="{{{ nd_number_format(Input::old('overtime_rate', $record->getHistoricWageInformation()->overtime)) }}}" tabindex="-1" data-calculation="employeeTotal" value="0" data-rule-number="true" data-rule-required="true">
             </div>
       </div>
   </div>
</div>

<div class="input-grouping">
<label class="col-sm-1 control-label">Hourly Benefits</label>
<div class="col-sm-2">
    <div class="input-grouping">
    <div class="input-group">
             <span class="input-group-addon">$</span>
             <input type="number" step="any" id="hourly_benefits" name="hourly_benefits" class="form-control mousetrap text-right" value="{{{ nd_number_format(Input::old('hourly_benefits', $record->getHistoricWageInformation()->regularBenefits)) }}}" tabindex="-1" data-calculation="employeeTotal" value="0" data-rule-number="true" data-rule-required="true">
       </div>
    </div>
</div>
</div>

<div class="input-grouping">
<label class="col-sm-1 control-label">Overtime Benefits</label>
<div class="col-sm-2">
    <div class="input-grouping">
    <div class="input-group">
             <span class="input-group-addon">$</span>
             <input type="number" step="any" id="overtime_benefits" name="overtime_benefits" class="form-control mousetrap text-right" value="{{{ nd_number_format(Input::old('overtime_benefits', $record->getHistoricWageInformation()->overtimeBenefits)) }}}" tabindex="-1" data-calculation="employeeTotal" value="0" data-rule-number="true" data-rule-required="true">
       </div>
    </div>
</div>
</div>
</div>
<div class="form-group row accessibility-row has-warning mb30">
     <div class="input-grouping">
     <label class="col-sm-offset-6 col-sm-4 control-label">Employee Cost</label>
          <div class="col-sm-2">
               <div class="input-grouping">
               <div class="input-group">
                    <span class="input-group-addon">$</span>
                    <input type="number" id="total_employee_cost" step="any" data-adjust="invoice_amount" name="total_employee_cost" class="form-control mousetrap text-right" tabindex="-1" data-result="employeeTotal" value="{{{ nd_number_format($record->getRecordTotals()->employeeCombined) }}}" data-rule-number="number" data-msg-number="This field cannot be calculated right now.">
               </div>
               </div>
        </div>
     </div>
</div>