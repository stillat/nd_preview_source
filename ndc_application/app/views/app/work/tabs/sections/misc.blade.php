<div class="form-group row accessibility-row">
	<div class="input-grouping">
	<label class="col-sm-1 control-label">Activity</label>
    	<div class="col-sm-2">
    		<div class="input-group">
    			<span class="input-group-addon"><i class="fa ndc-bicycle"></i></span>
    			<input type="text" name="record_activity" id="record_activity" class="form-control mousetrap s2" value="{{{ Input::old('record_activity', 0) }}}">
    		</div>
    		<a id="activityDescriptionActivator" <?php if (user_settings()['auto_expand_more_options']) {
    ?>style="display: none;"<?php 
} ?> class="btn btn-link"><i class="fa fa-quote-right"></i> Enter an activity description</a>
    	</div>
	</div>
	<div class="input-grouping">
		<label class="col-sm-1 control-label">Project</label>
		<div class="col-sm-2">
			<div class="input-group">
				<span class="input-group-addon"><i class="fa fa-folder-open"></i></span>
				<input type="text" name="record_project" id="record_project" class="form-control mousetrap s2" value="{{{ Input::old('record_project', 0) }}}">
			</div>
		</div>
	</div>
	<div class="input-grouping">
	<label class="col-sm-1 control-label">Department</label>
    	<div class="col-sm-2">
    		<div class="input-group">
    			<span class="input-group-addon"><i class="fa fa-users"></i></span>
    			<input type="text" name="record_department" id="record_department" class="form-control mousetrap s2" value="{{{ Input::old('record_department', 0) }}}">
    		</div>
    	</div>
	</div>
	<div class="input-grouping {{ skip_class(1, $workResetSettings->wl_optional_district) }}">
	<label class="col-sm-1 control-label">District</label>
    	<div class="col-sm-2">
    		<div class="input-group">
    			<span class="input-group-addon"><i class="fa fa-map-marker"></i></span>
    			<input type="text" name="record_district" id="record_district" class="form-control mousetrap s2" value="{{{ Input::old('record_district', 0) }}}" {{ skip_class(2, $workResetSettings->wl_optional_district) }}>
    		</div>
    	</div>
	</div>
</div>
<div class="form-group row accessibility-row" id="activityDescriptionHolder" <?php if (user_settings()['auto_expand_more_options'] == false) {
    ?>style="display: none;"<?php 
} ?>>
	<div class="input-grouping">
	    <label for="" class="col-sm-1 control-label">Activity Description</label>
    	<div class="col-sm-11">
    	    <textarea id="activity_description" name="activity_description" maxlength="6553" cols="30" rows="1" class="form-control autosize" style="min-height: 36px;" data-rule-maxlength="6553">{{{ Input::old('activity_description', '') }}}</textarea>
    	</div>
	</div>
</div>
