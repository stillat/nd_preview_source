@include('app.work.tabs.sections.employee')
@if ($workResetSettings->wl_enable_equipment_compatibility)
    @include('app.work.compatibility_features.create_compat_equip')
@endif
@include('app.work.tabs.sections.property')
@include('app.work.tabs.sections.misc')
