@if ($workResetSettings->wl_enable_invoice_compatibility)
    <div style="display: none">
        @include('app.work.tabs.billing_sections.general_update')
    </div>
@else
    @include('app.work.tabs.billing_sections.general_update')
@endif
@include('app.work.tabs.billing_sections.items_update')
@if ($workResetSettings->wl_enable_invoice_compatibility)
    <div style="display: none">
        @include('app.work.tabs.billing_sections.notes_update')
        @include('app.work.tabs.billing_sections.tax_features_update')
    </div>
@else
    @include('app.work.tabs.billing_sections.notes_update')
    @include('app.work.tabs.billing_sections.tax_features_update')
@endif
