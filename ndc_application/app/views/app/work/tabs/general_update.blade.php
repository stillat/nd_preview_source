@include('app.work.tabs.sections.employee_update')
@if ($workResetSettings->wl_enable_equipment_compatibility)
    @include('app.work.compatibility_features.update_compat_equip')
@endif
@include('app.work.tabs.sections.property_update')
@include('app.work.tabs.sections.misc_update')

<div id="more_options_holder">

</div>