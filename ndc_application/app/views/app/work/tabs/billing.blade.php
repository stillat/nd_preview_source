@if ($workResetSettings->wl_enable_invoice_compatibility)
    <div style="display: none">
        @include('app.work.tabs.billing_sections.general')
    </div>
@else
    @include('app.work.tabs.billing_sections.general')
@endif
@include('app.work.tabs.billing_sections.items')
@if ($workResetSettings->wl_enable_invoice_compatibility)
    <div style="display: none">
        @include('app.work.tabs.billing_sections.notes')
        @include('app.work.tabs.billing_sections.tax_features')
    </div>
@else
    @include('app.work.tabs.billing_sections.notes')
    @include('app.work.tabs.billing_sections.tax_features')
@endif
