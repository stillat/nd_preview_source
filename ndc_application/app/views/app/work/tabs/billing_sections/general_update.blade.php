<div class="form-group row accessibility-row">
    <label class="col-sm-1 control-label">Invoice #</label>
    <div class="input-grouping">
        <div class="col-sm-2">
            <input type="text" name="billing_invoice_number" id="billing_invoice_number" value="{{{ get_property($invoice, 'id', '') }}}" class="form-control mousetrap text-right">
            <input type="hidden" name="billing_invoice_number_created" id="billing_invoice_number_created" value="0" data-rule-number="true">
        </div>
    </div>
    <label for="" class="col-sm-1 control-label">Invoice Date</label>
    <div class="col-sm-2">
        <div class="input-group">
            <span class="input-group-addon"><i class="fa fa-calendar-o"></i></span>
            <input type="text" name="billing_invoice_date" value="{{{ Input::old('billing_invoice_date', $invoiceDate->format('m/j/Y')) }}}" class="form-control mousetrap" placeholder="mm/dd/yyyy" id="datePickerInvoiceDate" disabled="disabled">
        </div>
    </div>
    <label for="" class="col-sm-1 control-label">Due Date</label>
    <div class="col-sm-2">
        <div class="input-grouping">
            <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-calendar-o"></i></span>
                <input type="text" name="billing_invoice_due_date" data-adjust="invoice_amount" class="form-control mousetrap" placeholder="mm/dd/yyyy" id="datePickerInvoiceDueDate" data-rule-dateformat data-rule-required="false" value="{{{ $dueDate->format('m/j/Y') }}}">
            </div>
        </div>
    </div>
    <label class="col-sm-1 control-label">Discount</label>
    <div class="col-sm-2">
        <div class="input-grouping">
            <div class="input-group">
                <input type="text" name="billing_invoice_discount" id="billing_invoice_discount" class="form-control mousetrap text-right" value="{{{ nd_number_format(get_property($invoice, 'discount', 0)) }}}" data-rule-number="true">
                <span class="input-group-addon">%</span>
            </div>
        </div>
    </div>
</div>
<div class="form-group row accessibility-row">
    <label for="" class="col-sm-1 control-label">Bill to:</label>
    <div class="col-sm-5">
        <div class="input-grouping">
            <div class="input-group">
                <span class="input-group-addon"><i class="fa ndcf-account-payable" style="font-size: 17px;"></i></span>
                <input type="text" name="billing_account_number" id="billing_account_number" value="{{{ get_property($invoice, 'account_number', 0) }}}" class="form-control mousetrap" placeholder="Account being billed" data-rule-number="true">
            </div>
        </div>
    </div>
    <label for="" class="col-sm-1 control-label">Pay to:</label>
    <div class="col-sm-5">
        <div class="input-grouping">
            <div class="input-group">
                <span class="input-group-addon"><i class="fa ndcf-accounts-receivable" style="font-size: 17px;"></i></span>
                <input type="text" name="billing_paid_to_account_number" id="billing_paid_to_account_number" value="{{{ get_property($invoice, 'paid_to_account', 0) }}}" class="form-control mousetrap" placeholder="Account to be paid to" data-rule-number="true">
            </div>
        </div>
    </div>
</div>