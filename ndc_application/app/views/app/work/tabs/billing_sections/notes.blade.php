<div class="panel panel-note">
    <div class="panel-heading">
        <div class="panel-btns">
            <a href="" class="minimize maximize">+</a>
        </div>
        <!-- panel-btns -->
        <h3 class="panel-title"><i class="fa fa-quote-right"></i> Invoice Client Notes &amp; Terms</h3>
    </div>
    <div class="panel-body" style="display: none">
        <div class="form-group row accessibility-row">
            <div class="input-grouping">
                <label for="" class="col-sm-1 control-label">Notes to Client</label>
                <div class="col-sm-11">
                    <textarea name="invoice_notes_to_client" id="invoiceNotesClient" maxlength="65553" style="height: 74px;" cols="30" rows="3" class="form-control autogrow"></textarea>
                </div>
            </div>
        </div>
        <div class="form-group row accessibility-row">
            <div class="input-grouping">
                <label for="" class="col-sm-1 control-label">Invoice Terms</label>
                <div class="col-sm-11">
                    <textarea name="invoice_terms" id="invoiceTerms" maxlength="65553" style="height: 74px;" cols="30" rows="3" class="form-control autogrow"></textarea>
                </div>
            </div>
        </div>
    </div>
</div>