<div class="table-responsive">
    <table class="table table-hover table-striped table-fixed">
        <tbody>
        <tr>
            <td class="text-right"><strong>Subtotal</strong>:</td>
            <!--<td class="text-right wd-200"><input type="text" id="invoiceTotalSubtotal" class="text-right" value="0.00"/></td>-->
            <td class="text-right wd-200" id="invoiceTotalSubtotal">0.00</td>
        </tr>
        <tr>
            <td colspan="2" class="text-right">
                <div class="table-responsive col-sm-4 pull-right">
                    <table class="table table-hover table-bordered table-fixed">
                        <thead>
                        <tr>
                            <th class="text-right wd-200">Tax Name</th>
                            <th class="text-right">Tax Rate</th>
                            <th class="wd-50 text-right"><a class="pull-right cursor-pointer"
                                                            data-bind="click: addTaxRate"><i class="fa fa-plus"></i></a>
                            </th>
                        </tr>
                        </thead>
                        <tbody id="invoiceTaxRatesHolder"></tbody>
                    </table>
                </div>
            </td>
        </tr>
        <tr>
            <td class="text-right"><strong>Tax Total</strong>:</td>
            <td class="text-right wd-200" id="invoiceTaxAmountHolder">0.00</td>
        </tr>
        <tr>
            <td class="text-right"><strong>Discount</strong>:</td>
            <td class="text-right wd-200" id="invoiceDiscountAmountHolder">0.00</td>
        </tr>
        <tr style="display: none;" id="invoicePreviousInvoiceDiscountHolder">
            <td class="text-right"><strong>Previous Discount</strong></td>
            <td class="text-right wd-200" id="invoicePreviousDiscountAmount">0.00</td>
        </tr>
        <tr style="display: none;" id="invoicePreviousBalanceHolder">
            <td class="text-right"><strong>Previous Balance</strong></td>
            <td class="text-right wd-200" id="invoicePreviousBalance">0.00</td>
        </tr>
        <tr>
            <td class="text-right" style="vertical-align: middle"><strong>Total (this record)</strong></td>
            <td class="text-right wd-200"><input type="number" class="text-right form-control" step="any"
                                                 id="invoiceTotalBalance" name="invoice_override_total"
                                                 value="{{{ nd_number_format(Input::old('invoice_override_total', 0)) }}}">
            </td>
        </tr>
        <tr style="display: none;" id="invoiceNewBalanceHolder">
            <td class="text-right"><strong>New Balance</strong></td>
            <td class="text-right wd-200" id="invoiceNewBalance">0.00</td>
        </tr>
        </tbody>
    </table>
</div>

<div id="removedTaxRatesHolder"></div>

<script type="text/html" id="taxLine_tmpl">
    <%* createdTaxRates++ %>
    <tr data-taxrate="<%:~getTaxRatesCount()%>" id="taxRateHeader<%:~getTaxRatesCount()%>">
        <td class="text-right">
            <input type="hidden" id="taxRateRecordID_<%:~getTaxRatesCount()%>" name="tax_record_id[]"
                   data-related-tax-record="taxLineSelector_<%:~getTaxRatesCount()%>" value="<%>linked_id%>">
            <input type="hidden" data-related-tax="<%:~getTaxRatesCount()%>" data-taxvalue="employee" value="0"><input
                    type="hidden" name="affectsEmployees[]" data-related-tax="<%:~getTaxRatesCount()%>"
                    data-affects="employee" value="<%>affects_employees%>">
            <input type="hidden" data-related-tax="<%:~getTaxRatesCount()%>" data-taxvalue="material" value="0"><input
                    type="hidden" name="affectsMaterials[]" data-related-tax="<%:~getTaxRatesCount()%>"
                    data-affects="materials" value="<%>affects_materials%>">
            <input type="hidden" data-related-tax="<%:~getTaxRatesCount()%>" data-taxvalue="equipment" value="0"><input
                    type="hidden" name="affectsEquipments[]" data-related-tax="<%:~getTaxRatesCount()%>"
                    data-affects="equipments" value="<%>affects_equipments%>">
            <input type="hidden" data-related-tax="<%:~getTaxRatesCount()%>" data-taxvalue="fuel" value="0"><input
                    type="hidden" name="affectsFuels[]" data-related-tax="<%:~getTaxRatesCount()%>" data-affects="fuels"
                    value="<%>affects_fuels%>">
            <input type="text" class="required form-control highlight_ignore s2"
                   id="taxRateSelector_<%:~getTaxRatesCount()%>" name="invoiceTaxRates[]" value="<%>id%>"
                   data-taxrate="<%:~getTaxRatesCount()%>">
        </td>
        <td valign="middle" class="text-right wd-200" data-tax="tax_rate" data-taxrate="<%:~getTaxRatesCount()%>"
            style="padding-top: 19px;"><input type="number" style="margin-top: -11px;" class="form-control text-right"
                                              data-tax="tax_rate" step="any" data-taxrate="<%:~getTaxRatesCount()%>"
                                              data-taxline="historic_rate" name="invoiceTaxHistoricRates[]"
                                              value="<%>tax_rate%>"></td>
        <td class="wd-20" style="padding-top: 19px;"><a href="#" data-taxrate="<%:~getTaxRatesCount()%>"
                                                        data-bind="click: removeTaxRate"><i
                        class="fa fa-minus-circle cursor-pointer"></i></a></td>
    </tr>
</script>