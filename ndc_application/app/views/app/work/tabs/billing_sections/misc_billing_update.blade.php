<div class="form-group row accessibility-row">
	<div class="input-grouping">
	<label for="" class="col-sm-1 control-label">Quantity</label>
    <div class="col-sm-3">
        <input type="number" step="any" name="misc_billing_quantity" id="misc_billing_quantity" class="form-control mousetrap text-right {{{ user_form_size() }}}" value="{{{ Input::old('misc_billing_quantity', $record->getMiscBilling()->quantity) }}}" data-rule-number="true">
    </div>
	</div>
	<div class="input-grouping">
	<label for="" class="col-sm-1 control-label">Unit</label>
    <div class="col-sm-3">
        <input type="text" class="form-control mousetrap s2" id="misc_billing_unit" name="misc_billing_unit" value="{{{ Input::old('misc_billing_unit', $record->getMiscBilling()->unit_id) }}}" data-msg-number="true">
    	{{ Form::errorMsg('misc_billing_unit') }}
    </div>
	</div>
	<div class="input-grouping">
	<label for="" class="col-sm-1 control-label">Rate</label>
    <div class="col-sm-3">
        <input type="number" step="any" name="misc_billing_rate" id="misc_billing_rate" class="form-control mousetrap text-right {{{ user_form_size() }}}" value="{{{ nd_number_format(Input::old('misc_billing_rate', $record->getMiscBilling()->rate)) }}}" data-msg-number="true">
    </div>
	</div>
</div>
<div class="form-group row accessibility-row">
	<div class="input-grouping">
	<label for="" class="col-sm-1 control-label">Billing Description</label>
    <div class="col-sm-11">
      	<textarea name="misc_billing_description" id="invoiceBillingDescription" maxlength="65553" style="height: 74px;" cols="30" rows="3" class="form-control mousetrap autogrow {{{ user_form_size() }}}">{{{ Input::old('misc_billing_description', $record->getMiscBilling()->description) }}}</textarea>
    </div>
	</div>
</div>