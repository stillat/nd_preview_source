@if (!$workResetSettings->wl_enable_invoice_compatibility)
<ul class="nav nav-tabs">
	<li class="active"><a href="#invMiscBilling" data-toggle="tab">Miscellaneous Billing</a></li>
	<li><a href="#invGeneral" data-toggle="tab">General <small>and</small> Materials</a></li>
	<li><a href="#invEquipment" data-toggle="tab"><i class="fa fa-truck"></i> Equipment <small>and</small> Fuels</a></li>
</ul>
@endif
<div class="tab-content mb30">
	<div class="tab-pane active" id="invMiscBilling">
	@include('app.work.tabs.billing_sections.misc_billing_update')
	</div>
	<div class="tab-pane" id="invGeneral">
		<div class="table-responsive">
			<table class="table table-hover table-striped">
				<thead>
					<tr>
						<th>Invoice Item</th>
						<th class="text-right">Quantity</th>
						<th>Taxable</th>
						<th class="text-right">Tax Total</th>
						<th class="text-right">Line Total</th>
					</tr>
				</thead>
				<tbody id="invoiceItemsHolder">
					<tr data-invoice="employee">
						<td data-linecalc="line_name" data-invoice="employee">Employee Hours</td>
						<td data-linecalc="line_quantity" data-invoice="employee" class="text-right">0</td>
						<td>
							<div class="checkbox n-mt-5">
								<label>
									<input type="checkbox" name="employee_taxable" data-linecalc="taxable" data-invoice="employee"> Taxable
								</label>
							</div>
						</td>
						<td class="text-right" data-linecalc="line_tax" data-invoice="employee">0</td>
						<td class="text-right" data-linecalc="line_total" data-invoice="employee">0</td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
	<div class="tab-pane" id="invEquipment">
		<div id="invoiceEquipmentItemsHolder">

		</div>
		<div class="table-responsive">

		</div>
	</div>
</div>

<script type="text/html" id="invoiceLineEquipment_tmpl">
	<%* createdInvoiceLinesCount++ %>
	<table class="table table-hover table-striped" data-invoice="equip" data-related="<%>line_related_item%>">
		<thead>
			<tr>
				<th style="background-color: rgba(<%:~getEquipmentColorInvoice()%>, 0.4);"><i class="fa fa-truck"></i> Invoice Equipment Unit</th>
				<th class="text-right" style="background-color: rgba(<%:~getEquipmentColorInvoice()%>, 0.4);">Quantity</th>
				<th style="background-color: rgba(<%:~getEquipmentColorInvoice()%>, 0.4);">Taxable</th>
				<th class="text-right" style="background-color: rgba(<%:~getEquipmentColorInvoice()%>, 0.4);">Tax Total</th>
				<th class="text-right" style="background-color: rgba(<%:~getEquipmentColorInvoice()%>, 0.4);">Line Total</th>
			</tr>
		</thead>
		<tbody>
			<tr data-invoice="<%>line_type%>" data-icount="<%:~getInvoiceLineCount()%>" data-related="<%>line_related_item%>">
				<td data-linecalc="line_name" data-invoice="<%>line_type%>" data-related="<%>line_related_item%>"><%>line_name%></td>
				<td data-linecalc="line_quantity" data-invoice="<%>line_type%>" data-related="<%>line_related_item%>" class="text-right"><%>line_quantity%></td>
				<td>
					<div class="checkbox n-mt-5">
						<label>
							<input type="checkbox" data-invoice="<%>line_type%>" data-related="<%>line_related_item%>" data-linecalc="taxable"

                            <%if taxable%>
                            checked="checked"
                            <%/if%>

							> Taxable
						</label>
					</div>
				</td>
				<td class="text-right" data-linecalc="line_tax" data-invoice="<%>line_type%>" data-related="<%>line_related_item%>"><%>~ndc_number(line_tax_total)%></td>
				<td class="text-right" data-linecalc="line_total" data-invoice="<%>line_type%>" data-related="<%>line_related_item%>"><%>~ndc_number(line_total)%></td>
			</tr>
			<tr id="invoiceEquipmentFuels<%>line_related_item%>" data-invoice="equip" data-related="<%>line_related_item%>">
				<td colspan="6">
					<div class="table-responsive">
						<table class="table table-hover table-striped table-bordered" style="border-left: 2px solid rgb(<%:~getEquipmentColorInvoice()%>);">
							<thead>
								<tr>
									<th style="background-color: rgba(<%:~getEquipmentColorInvoice()%>, 0.2);">Invoice Fuel</th>
									<th class="text-right" style="background-color: rgba(<%:~getEquipmentColorInvoice()%>, 0.2);">Quantity</th>
									<th style="background-color: rgba(<%:~getEquipmentColorInvoice()%>, 0.2);">Taxable</th>
									<th class="text-right" style="background-color: rgba(<%:~getEquipmentColorInvoice()%>, 0.2);">Tax Total</th>
									<th class="text-right" style="background-color: rgba(<%:~getEquipmentColorInvoice()%>, 0.2);">Line Total</th>
								</tr>
							</thead>
							<tbody id="invoiceEquipmentFuels<%>line_related_item%>_holder">

							</tbody>
						</table>
					</div>
				</td>
			</tr>
		</tbody>
	</table>
</script>

<script type="text/html" id="invoiceLine_tmpl">
	<%* createdInvoiceLinesCount++ %>
	<tr data-invoice="<%>line_type%>" data-icount="<%:~getInvoiceLineCount()%>" data-related="<%>line_related_item%>">
		<td data-linecalc="line_name" data-invoice="<%>line_type%>" data-related="<%>line_related_item%>"><%>line_name%></td>
		<td data-linecalc="line_quantity" data-invoice="<%>line_type%>" data-related="<%>line_related_item%>" class="text-right"><%>line_quantity%></td>
		<td>
			<div class="checkbox n-mt-5">
				<label>
					<input type="checkbox" data-invoice="<%>line_type%>" data-linecalc="taxable" data-related="<%>line_related_item%>"

					<%if taxable%>
                    checked="checked"
                    <%/if%>

					> Taxable
				</label>
			</div>
		</td>
		<td class="text-right" data-linecalc="line_tax" data-invoice="<%>line_type%>" data-related="<%>line_related_item%>"><%>~ndc_number(line_tax_total)%></td>
		<td class="text-right" data-linecalc="line_total" data-invoice="<%>line_type%>" data-related="<%>line_related_item%>"><%>~ndc_number(line_total)%></td>
	</tr>
</script>