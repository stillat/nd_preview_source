<div class="row">
	<div class="col-sm-8">
	</div>
	<div class="col-sm-4 text-right">
		<div class="btn-group">
			<button type="button" tabindex="-1" class="btn btn-white" data-bind="click: addEquipmentRow"><i class="fa fa-plus"></i> Add Equipment Unit</button>
			<button type="button" tabindex="-1" class="btn btn-white dropdown-toggle" data-toggle="dropdown">
				<span class="caret"></span>	
				<span class="sr-only">Toggle Dropdown</span>
			</button>
			<ul class="dropdown-menu" role="menu" style="margin-left: -76px;">
				<li><a href="#" data-bind="click: removeEquipmentUnits">Remove all equipment units and fuels</a></li>
			</ul>
		</div>
	</div>
</div>

<div class="table-responsive">
	<table class="table table-hover table-striped">
		<tfoot class="equipment-hide-first">
			<tr>
				<td class="text-right" colspan="2" style="vertical-align: middle;"><label><strong>Total Fuels Cost</strong></label></td>
				<td>
					<div class="input-grouping">
					<div class="input-group has-warning">
                    	<span class="input-group-addon">$</span>
                        <input type="number" step="any" name="total_fuels_cost" id="total_fuels_cost" tabindex="-1" class="form-control mousetrap text-right" data-rule-number="true" data-msg-number="This field cannot be calculated right now.">
                    </div>
					</div>
				</td>
				<td class="text-right" style="vertical-align: middle;"><label><strong>Equipment Unit Cost</strong></label></td>
				<td>
					<div class="input-grouping">
					<div class="input-group has-warning">
                    	<span class="input-group-addon">$</span>
                        <input type="number" step="any" name="total_equipment_cost" tabindex="-1" id="total_equipment_cost" class="form-control mousetrap text-right" data-rule-number="true" data-msg-number="This field cannot be calculated right now.">
                    </div>
					</div>
				</td>
			</tr>
			<tr>
				<td colspan="4" class="text-right" style="vertical-align: middle;"><label><strong>Combined Cost</strong></label></td>
				<td>
					<div class="input-grouping">
					<div class="input-group has-warning">
                    	<span class="input-group-addon">$</span>
                        <input type="number" step="any" data-adjust="invoice_amount" tabindex="-1" name="total_equip_fuels_cost" id="total_equip_fuels_cost" class="form-control mousetrap text-right" data-rule-number="true" data-msg-number="This field cannot be calculated right now.">
                    </div>
					</div>
				</td>
			</tr>
		</tfoot>
		<tbody id="equipment_units_holder">

		</tbody>
	</table>
</div>

<script type="text/html" id="equipmentUnitFuel_tmpl">
	<%* createdFuelsCount++ %>
	<tr data-fuel="<%:~getFuelCount()%>" id="fuelSelector<%:~getFuelCount()%>_holder">
		<td>
			<input type="hidden" id="fuelTrackingInventory_<%:~getFuelCount()%>" name="fuel_tracking_inventory[]" data-related-selector="fuelSelector_<%:~getFuelCount()%>" value="<%>tracking_inventory%>">
			<input type="hidden" name="fuelAssociatedType[]" id="fuelSelector_<%:~getFuelCount()%>_associated" data-equipment="<%:~getFuelCount()%>" data-apply="associated" value="<%>associated_type%>">
			<input type="hidden" name="boundFuelEquipmentRecords[]" value="<%>bindingRecord%>">
			<input type="text" class="required form-control mousetrap highlight_ignore select_fuel" id="fuelSelector_<%:~getFuelCount()%>" name="fuels[]" value="<%>id%>">
		</td>
		<td>
            <div class="input-grouping">
                <input type="text" class="required form-control mousetrap text-right" id="fuelQuantity<%:~getFuelCount()%>" name="fuelQuantity[]" data-fuel="quantity" data-calculation="fuel_total" value="<%>quantity%>" data-rule-number="true">
            </div>
		</td>
		<td>
			<div class="input-grouping">
			    <div class="has-success">
            	    <input type="text" class="required form-control mousetrap text-right" id="fuelSelector<%:~getFuelCount()%>_cost" name="fuelUnitCosts[]" data-fuel="unit_cost" data-calculation="fuel_total" value="<%>~ndc_number(unit_cost)%>" data-rule-number="true">
            	</div>
			</div>
		</td>
		<td>
			<div class="input-grouping">
			    <div class="has-warning">
                    <input type="text" class="required form-control mousetrap text-right readonly" tabindex="-1" name="fuelTotalCost[]" data-fuel="total_cost" data-result="fuel_total_cost" value="<%>~ndc_number(total_cost)%>" data-rule-number="true" data-msg-number="This field cannot be calculated right now.">
                </div>
			</div>
		</td>
		<td><a class="btn btn-link" data-fuel="<%:~getFuelCount()%>" data-bind="click: removeFuel" id="remove_fuel_record_<%:~getFuelCount()%>">Remove Fuel</a><input type="hidden" name="fuelTaxed[]" data-fuel="<%:~getFuelCount()%>" value="<%>taxable%>" data-calc="taxable"></td>
	</tr>
</script>

<script type="text/html" id="equipmentUnit_tmpl">
	<%* createdEquipmentUnitsCount++ %>
	<tr data-equipment="<%:~getEquipmentCount()%>" id="equipmentHeader<%:~getEquipmentCount()%>" class="equipment-hide-first">
		<th class="col-sm-4" style="background-color: rgba(<%:~getEquipmentColor()%>, 0.4);">Equipment Unit</th>
		<th class="text-right col-sm-2" style="background-color: rgba(<%:~getEquipmentColor()%>, 0.4);">Quantity <a style="margin-left: 5px;" data-content="Quantity refers to the total hours or miles (or any other unit) that the equipment unit was used." data-title="Quantity Help" data-placement="right" data-toggle="popover" data-container="body" class="popovers cursor-pointer" data-original-title="" title=""> <i class="fa fa-question-circle"></i></a></th>
		<th class="text-right col-sm-2" style="background-color: rgba(<%:~getEquipmentColor()%>, 0.4);">Rate</th>
		<th class="text-right col-sm-2" style="background-color: rgba(<%:~getEquipmentColor()%>, 0.4);">Total Cost</th>
		<th class="text-right col-sm-2" style="background-color: rgba(<%:~getEquipmentColor()%>, 0.4);"></th>
	</tr>
	<tr data-for="values" data-equipment="<%:~getEquipmentCount()%>" id="equipmentSelector<%:~getEquipmentCount()%>" class="equipment-hide-first">
		<td>
			<div class="input-group">
				<span class="input-group-addon" style="background-color: rgba(<%:~getEquipmentColor()%>, 0.3); color: black;"><i class="fa fa-truck"></i></span>
				<input type="hidden" name="equipmentBindingRecords[]" value="<%:~getEquipmentCount() %>">
				<input type="text" id="equipmentUnitSelector_<%:~getEquipmentCount()%>" name="equipmentUnits[]" class="form-control mousetrap highlight_ignore select_equipment_unit" value="<%>id%>">
			</div>
		</td>
		<td>
		    <div class="input-grouping">
		        <input type="text" class="form-control mousetrap text-right" id="equipmentQuantity<%:~getEquipmentCount()%>" name="equipmentQuantities[]" data-equipment="quantity" value="0" data-rule-number="true">
		    </div>
		</td>
		<td>
		    <div class="input-grouping">
		        <input type="text" class="form-control mousetrap text-right" id="equipmentRate<%:~getEquipmentCount()%>" name="equipmentRates[]" data-equipment="rate" value="0" data-rule-number="true">
		    </div>
		</td>
		<td>
		    <div class="input-grouping">
		        <input type="text" class="form-control mousetrap text-right" tabindex="-1" id="equipmentTotalCost<%:~getEquipmentCount()%>" name="equipmentTotalCosts[]" data-equipment="total_cost" value="0" data-rule-number="true" data-msg-number="This field cannot be calculated right now.">
		    </div>
		</td>
		<td><a class="btn btn-link remove_equipment_link" data-equipment="<%:~getEquipmentCount()%>" data-bind="click: removeEquipment" id="remove_equipment_record_<%:~getEquipmentCount()%>">Remove Equipment</a><input type="hidden" name="equipmentTaxed[]" data-equipment="<%:~getEquipmentCount()%>" data-calc="taxable" value="<%>taxable%>"></td>
	</tr>
	<tr data-equipment="<%:~getEquipmentCount()%>" id="equipmentOdometer<%:~getEquipmentCount()%>" class="equipment-hide-first">
		<td colspan="2" class="text-right" style="vertical-align: middle;">
			<strong>Odometer Reading:</strong>
		</td>
		<td colspan="2">
			<div class="input-grouping">
			    <input type="text" class="form-control mousetrap" id="equipmentOdometerReadingField<%:~getEquipmentCount()%>" name="equipmentOdometerReadings[]" data-equipment="odometer_reading" value="<%>odometer_reading%>" data-rule-number="true">
			</div>
		</td>
		<td></td>
	</tr>
	<tr data-equipment="<%:~getEquipmentCount()%>" id="equipmentSelector<%:~getEquipmentCount()%>_fuels">
		<td colspan="5">
			<div class="table-responsive">
				<table class="table table-hover table-striped table-bordered"  style="border-left: 2px solid rgb(<%:~getEquipmentColor()%>);">
					<thead>
						<tr>
							<th class="col-sm-4" style="background-color: rgba(<%:~getEquipmentColor()%>, 0.2);">Fuel</th>
							<th class="text-right col-sm-2" style="background-color: rgba(<%:~getEquipmentColor()%>, 0.2);">Quantity</th>
							<th class="text-right col-sm-2" style="background-color: rgba(<%:~getEquipmentColor()%>, 0.2);">Unit Cost</th>
							<th class="text-right col-sm-2" style="background-color: rgba(<%:~getEquipmentColor()%>, 0.2);">Total Cost</th>
							<th class="text-right col-sm-2" style="background-color: rgba(<%:~getEquipmentColor()%>, 0.2);">
								<div class="btn-group mb-0">
									<button type="button" class="btn btn-white" tabindex="-1" data-equipment="<%:~getEquipmentCount()%>" data-bind="click: addFuel"><i class="fa fa-plus"></i> Add Fuel</button>
									<button type="button" class="btn btn-white dropdown-toggle" tabindex="-1" data-toggle="dropdown">
										<span class="caret"></span>
										<span class="sr-only">Toggle Dropdown</span>
									</button>
									<ul class="dropdown-menu" role="menu">
										<li><a href="#" data-equipment="<%:~getEquipmentCount()%>" data-bind="click: removeFuelAll">Remove all fuels</a></li>
									</ul>
								</div>
							</th>
						</tr>
					</thead>
					<tfoot>
						<tr>
							<td colspan="3" class="text-right" style="vertical-align: middle;"><label style="font-weight: bold;">Fuels Cost</label></td>
							<td colspan="1">
								<div class="input-grouping">
                                    <div class="input-group has-warning col-sm-12">
                                        <span class="input-group-addon">$</span>
                                        <input type="number" step="any" name="record_fuels_cost[]" tabindex="-1" data-fuel="record_cost" id="record_fuels_cost_<%:~getEquipmentCount()%>" class="form-control mousetrap text-right" data-rule-number="true" data-msg-number="This field cannot be calculated right now.">
                                    </div>
								</div>
							</td>
							<td></td>
						</tr>
					</tfoot>
					<tbody id="equipmentSelector<%:~getEquipmentCount()%>_fuels_holder" data-equipment="<%:~getEquipmentCount()%>">
						<%for fuels tmpl="fuelRecordTemplate" /%>
					</tbody>					
				</table>
			</div>
		</td>
	</tr>
</script>