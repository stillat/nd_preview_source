<div class="form-group row accessibility-row">
    <div class="input-grouping">
        <label class="col-sm-1 control-label">Equipment Unit</label>
        <div class="col-sm-5">
            <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-truck"></i></span>
                <input type="text" name="compat_record_equipment" id="compat_record_equipment" class="form-control mousetrap highlight_ignore"  value="0" >
            </div>
        </div>
    </div>

    <div class="input-grouping">
        <label class="col-sm-1 control-label">Quantity</label>
        <div class="col-sm-2">
            <input type="number" step="any" id="compat_equip_quantity" name="compat_equip_quantity" class="form-control mousetrap text-right" value="{{{ nd_number_format(0) }}}" data-rule-number="true" data-msg-number="Please enter a valid number" data-rule-required="true">
        </div>
    </div>

    <div class="input-grouping has-success">
        <label class="col-sm-1 control-label">Rate</label>
        <div class="col-sm-2">
            <input type="number" step="any" id="compat_equip_rate" name="compat_equip_rate" class="form-control mousetrap text-right" value="{{{ nd_number_format(0) }}}" tabindex="-1" data-rule-number="true" data-msg-number="Please enter a valid number" data-rule-required="true">
        </div>
    </div>
</div>


<div class="form-group row accessibility-row has-warning mb30">
    <div class="input-grouping">
        <label class="col-sm-offset-6 col-sm-4 control-label">Equipment Cost</label>
        <div class="col-sm-2">
            <div class="input-grouping">
                <div class="input-group">
                    <span class="input-group-addon">$</span>
                    <input type="number" id="compat_equipment_cost" step="any" data-adjust="invoice_amount" name="total_employee_cost" class="form-control mousetrap text-right" tabindex="-1" value="{{{ nd_number_format(0) }}}" data-rule-number="number" data-msg-number="This field cannot be calculated right now.">
                </div>
            </div>
        </div>
    </div>
</div>