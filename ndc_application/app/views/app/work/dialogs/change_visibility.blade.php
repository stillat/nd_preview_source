<div class="modal fade" id="recordVisibility" tabindex="-1" role="dialog" aria-labelledby="recordVisibilityLabel"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close modal-close" data-dismiss="modal"
                        aria-hidden="true">&times;</button>
                <h4 id="recordVisibilityLabel"><i class="fa fa-eye"></i> Work Record Report Visibility</h4>
            </div>
            <div class="modal-body">
                <p id="visibilityMessage"></p>
            </div>
            <div class="modal-footer">
                {{ Form::open(['action' => 'App\UtilityController@postChangeRecordVisibility', 'method' => 'post']) }}
                <input type="hidden" name="visibilityContext" id="visibilityContext">
                <input type="hidden" name="visibilityAction" id="visibilityAction" value="0">
                <button type="submit" class="btn btn-primary">Adjust record visibility</button>
                <button type="button" class="btn btn-default modal-close" data-dismiss="modal">Cancel</button>
                {{ Form::close() }}
            </div>
        </div>
    </div>
</div>