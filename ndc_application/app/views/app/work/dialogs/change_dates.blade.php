<div class="modal fade" id="changeRecordDates" tabindex="-1" role="dialog" aria-labelledby="changeRecordDatesLabel"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            {{ Form::open(['action' => 'App\UtilityController@postChangeRecordDates', 'method' => 'post']) }}
            <div class="modal-header">
                <button type="button" class="close modal-close" data-dismiss="modal"
                        aria-hidden="true">&times;</button>
                <h4 id="changeRecordDatesLabel"><i class="fa fa-calendar"></i> Change Record Dates</h4>
            </div>
            <div class="modal-body">
                <p>Select a date and click "Update record dates" to update multiple records at one time</p>
                <fieldset>
                    <div class="input-grouping">
                        <label class="col-sm-3 control-label" style="margin-top: 10px; text-align: right;">New Work Date:</label>
                        <div class="col-sm-9">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-calendar-o"></i></span>
                                <input type="text" id="record_date" autocomplete="off" name="record_date" class="form-control mousetrap ndc_form_home_element" placeholder="mm/dd/yyyy" data-rule-required="true" data-msg-required="A record date is required." data-rule-dateformat>
                            </div>
                        </div>
                    </div>
                </fieldset>
            </div>
            <div class="modal-footer">
                <input type="hidden" name="changeDatesContext" id="changeDatesContext">
                <button type="submit" class="btn btn-primary">Update record dates</button>
                <button type="button" class="btn btn-default modal-close" data-dismiss="modal">Cancel</button>
            </div>
            {{ Form::close() }}
        </div>
    </div>
</div>