<div class="modal fade" id="resetFormDialog" tabindex="-1" role="dialog" aria-labelledby="resetFormDialogLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 id="resetFormDialogLabel">Reset Work Form</h4>
            </div>
            <div class="modal-body">
                Are you sure you want to reset the work form?
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-warning" id="confirmResetForm">Yes, reset the form</button>
                <button type="button" class="btn btn-default" id="cancelResetForm" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>
</div>