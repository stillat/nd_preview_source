@include('layouts.dialogs.remove', array('title' => 'Remove Work Record', 'content' => 'Are you sure want to <strong>permanently</strong> remove the selected records?', 'action' => 'App\Work\WorkLogsController@destroy', 'agreeText' => 'Remove work record'))
@include('app.work.dialogs.change_visibility')
@include('app.work.dialogs.change_dates')
@include('app.work.dialogs.change_org_data')