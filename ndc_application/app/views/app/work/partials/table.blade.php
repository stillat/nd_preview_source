<div style="width: 100%;
margin-bottom: 15px;
overflow-x: auto;
overflow-y: hidden;">
    <table class="table table-hover table-striped work_table" id="worktable">
        <thead>
        <tr class="long_table_heading">
            @if(!isset($hideFeatures))
            <th class="hidden-print" style="width:46px;">
                <div class="ckbox ckbox-primary">
                    <input type="checkbox" id="batchToggleRecords">
                    <label for="batchToggleRecords"></label>
                </div>
            </th>
            @endif
            <th style="min-width: 90px;">Date</th>
            <th>Employee</th>
            <th class="text-right">Total Hours</th>
            <th class="text-right">Cost</th>
            <th>Project</th>
            <th>Activity</th>
            <th>Department</th>
            <th>{{ $reportHeader }}</th>
            <th>Equipment</th>
            @for($i = 1; $i < $workMeta->fuelCount + 1; $i++)
                <th></i> Fuel {{{ $i }}}</th>
            @endfor
            @for($i = 1; $i < $workMeta->materialCount + 1; $i++)
                <th></i> Material {{{ $i }}}</th>
            @endfor
            <th class="hidden-print text-right">
                @if(!isset($hideFeatures))
                <a href="{{{ action('App\Search\SortingController@getWork') }}}" data-toggle="modal"
                   data-target="#modalHousing"><i class="fa fa-sort"></i>
                    <small>Sorting</small>
                </a>
                @endif
            </th>
        </tr>
        </thead>
        <tbody>
        @foreach($workRecords as $record)
            <?php
            $recordEquipmentUnits = $record->getEquipmentUnits();
            $recordEquipmentUnitCount = count($recordEquipmentUnits);

            $materialDifference = $workMeta->materialCount - count($record->getMaterials());
            ?>
            <tr>
                @if(!isset($hideFeatures))
                <td class="hidden-print">
                    <div class="ckbox ckbox-primary">
                        <input type="checkbox" class="rm_multi" id="remCheck{{{ $record->getWorkID() }}}"
                               data-workrecord="{{{ $record->getWorkID() }}}">
                        <label for="remCheck{{{ $record->getWorkID() }}}"></label>
                    </div>
                </td>
                @endif
                <td><a class="d_block" href="{{{ action('App\Work\WorkLogsController@show', array($record->getWorkID())) }}}">{{{ $record->getWorkDate() }}}</a></td>
                <td class="employee_row"><a class="d_block"
                                            href="{{{ action('App\Work\WorkLogsController@show', array($record->getWorkID())) }}}">{{{ $formatter->employee($record->getEmployee(), $recordOverrideSettings) }}}</a>
                </td>
                <td class="employee_row text-right"><a class="d_block"
                                                       href="{{{ action('App\Work\WorkLogsController@show', array($record->getWorkID())) }}}">{{{ $record->getRecordTotals()->employeeTotalHours }}}</a>
                </td>
                <td class="employee_row text-right"><a class="d_block"
                                                       href="{{{ action('App\Work\WorkLogsController@show', array($record->getWorkID())) }}}"><span data-type="numeric">{{{ $record->getRecordTotals()->employeeCombined }}}</span></a>
                </td>
                <td><a class="d_block"
                       href="{{{ action('App\Work\WorkLogsController@show', array($record->getWorkID())) }}}">{{{ $formatter->project($record->getProject(), $recordOverrideSettings) }}}</a>
                </td>
                <td><a class="d_block"
                       href="{{{ action('App\Work\WorkLogsController@show', array($record->getWorkID())) }}}">{{{ $formatter->activity($record->getActivity(), $recordOverrideSettings) }}}</a>
                </td>
                <td><a class="d_block"
                       href="{{{ action('App\Work\WorkLogsController@show', array($record->getWorkID())) }}}">{{{ $formatter->department($record->getDepartment(), $recordOverrideSettings) }}}</a>
                </td>
                <td class="property_row"><a class="d_block"
                       href="{{{ action('App\Work\WorkLogsController@show', array($record->getWorkID())) }}}">{{{ $formatter->property($record->getPreferredProperty(), $recordOverrideSettings) }}}</a>
                </td>

                @if ($recordEquipmentUnitCount > 0)
                    <?php
                    $fuelDifference = $workMeta->fuelCount - count($recordEquipmentUnits[0]->fuels);
                    ?>
                    <td class="equipment_row"><a class="d_block"
                                                 href="{{{ action('App\Work\WorkLogsController@show', array($record->getWorkID())) }}}">{{{ $formatter->property($recordEquipmentUnits[0]->equipment, $recordOverrideSettings) }}}</a>
                    </td>
                    @foreach($recordEquipmentUnits[0]->fuels as $fuel)
                        <td class="fuel_row"><a class="d_block"
                                                href="{{{ action('App\Work\WorkLogsController@show', array($record->getWorkID())) }}}">{{{ $formatter->fuel($fuel->fuel, $recordOverrideSettings) }}}</a>
                        </td>
                    @endforeach

                    @if($fuelDifference > 0)
                        {{ str_repeat('<td><a class="d_block" href="'.action('App\Work\WorkLogsController@show', array($record->getWorkID())).'">&nbsp;</a></td>', $fuelDifference) }}
                    @endif

                @else
                    {{ str_repeat('<td><a class="d_block" href="'.action('App\Work\WorkLogsController@show', array($record->getWorkID())).'">&nbsp;</a></td>', $workMeta->fuelCount + 1) }}
                @endif

                @foreach($record->getMaterials() as $material)
                    <td class="material_row"><a class="d_block"
                                                href="{{{ action('App\Work\WorkLogsController@show', array($record->getWorkID())) }}}">{{{ $formatter->material($material->material, $recordOverrideSettings) }}}</a>
                    </td>
                @endforeach
                {{-- Print out the remaining material lines. --}}
                @if($materialDifference > 0)
                    {{ str_repeat('<td><a class="d_block" href="'.action('App\Work\WorkLogsController@show', array($record->getWorkID())).'">&nbsp;</a></td>', $materialDifference) }}
                @endif

                <td class="table-action td-ignore hidden-print text-right">
                    @if(!isset($hideFeatures))
                    @if($record->isExcludedFromReports())
                    <i class="fa fa-eye-slash tooltips" data-toggle="tooltip" data-placement="left" title="Excluded from reports"></i>
                    @else
                    <i class="fa fa-eye tooltips" data-toggle="tooltip" data-placement="left" title="Included in reports"></i>
                    @endif
                    @endif
                    <a href="{{{ action('App\Work\WorkLogsController@edit', array($record->getWorkID())).'?mode=create-like' }}}" style="margin-left: 5px;" class="tooltips" data-toggle="tooltip" data-placement="left" title="Create new record like this one"><i class="fa fa-plus-square-o"></i></a>
                    @if($record->rawData->record_status == 5)
                            @if(!isset($hideFeatures))
                            <a><i class="fa fa-lock tooltips" data-toggle="tooltip" data-placement="left" title="This record is locked"></i></a>
                            <a><i class="fa fa-lock tooltips" data-toggle="tooltip" data-placement="left" title="This record is locked"></i></a>
                                @endif
                    @else
                            <a href="{{{ action('App\Work\WorkLogsController@edit', array($record->getWorkID())) }}}"><i
                                        class="fa fa-pencil tooltips" data-toggle="tooltip" data-placement="left" title="Update this record"></i></a>
                            @if(!isset($hideFeatures))
                            <a class="cursor-pointer delete-row tooltips" data-rcontext="{{{ $record->getWorkID() }}}" data-toggle="tooltip" data-placement="left" title="Remove this record"><i
                                        class="fa fa-trash-o"></i></a>
                            @endif
                    @endif

            </tr>
            @if ($recordEquipmentUnitCount > 1)
                @for ($i = 1; $i <= $recordEquipmentUnitCount - 1; $i++)
                    <tr>
                        <td class="hidden-print"><i class="fa fa-arrow-up"></i></td>
                        <td colspan="8"><a class="d_block"
                                           href="{{{ action('App\Work\WorkLogsController@show', array($record->getWorkID())) }}}">
                                &nbsp;</a></td>
                        <td class="equipment_row"><a class="d_block"
                                                     href="{{{ action('App\Work\WorkLogsController@show', array($record->getWorkID())) }}}">{{{ $formatter->property( $recordEquipmentUnits[$i]->equipment, $recordOverrideSettings)  }}}</a>
                        </td>
                        <?php
                        $nestedFuelDifference = $workMeta->fuelCount - count($recordEquipmentUnits[$i]->fuels);
                        ?>
                        @foreach($recordEquipmentUnits[$i]->fuels as $fuel)
                            <td class="fuel_row"><a class="d_block"
                                                    href="{{{ action('App\Work\WorkLogsController@show', array($record->getWorkID())) }}}">{{{ $formatter->fuel($fuel->fuel, $recordOverrideSettings) }}}</a>
                            </td>
                        @endforeach

                        @if($nestedFuelDifference > 0)
                            {{ str_repeat('<td><a class="d_block" href="'.action('App\Work\WorkLogsController@show', array($record->getWorkID())).'">&nbsp;</a></td>', $nestedFuelDifference) }}
                        @endif

                        @if ($workMeta->materialCount > 0)
                            {{ str_repeat('<td><a class="d_block" href="'.action('App\Work\WorkLogsController@show', array($record->getWorkID())).'">&nbsp;</a></td>', $workMeta->materialCount) }}
                        @endif
                        <td class="text-right hidden-print"><i class="fa fa-arrow-up" style="margin-right: 10px;"></i>
                        </td>
                    </tr>
                    <?php
                    // Memory clean up.
                    $nestedFuelDifference = null;
                    unset($nestedFuelDifference);
                    ?>
                @endfor
            @endif
            <?php

            // Memory clean up.
            $recordEquipmentUnitCount = null;
            $recordEquipmentUnits = null;
            unset($recordEquipmentUnits);
            unset($recordEquipmentUnitCount);
            ?>
        @endforeach
        </tbody>
    </table>
</div>
@include('app.work.partials.record_management')