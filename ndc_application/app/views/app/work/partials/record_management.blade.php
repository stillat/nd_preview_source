<div class="modal fade" id="recordManagementModal" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4><i class="fa fa-cogs"></i> Record Management Options</h4>
            </div>
            <div class="modal-body">
                <p>What would you like to do with this record right now?</p>
                <table class="table table-bordered">
                    <tbody>
                    <tr>
                        <td>Remove this work record</td>
                    </tr>
                    <tr>
                        <td>Update this work record</td>
                    </tr>
                    <tr>
                        <td>Lock this work record</td>
                    </tr>
                    <tr>
                        <td>Create a new record like this one</td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>