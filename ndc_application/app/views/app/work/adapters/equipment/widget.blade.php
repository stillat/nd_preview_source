<div class="table-responsive">
<table class="table table-hover">
    <tbody>
        <tr>
            <td><strong>Work Order Number</strong></td>
            <td colspan="5">{{{ $data->work_order_number }}}</td>
        </tr>
        <tr>
            <td><strong>Odometer Reading</strong></td>
            <td colspan="5"><?php
            if ($data->odometer !== null) {
                echo e($data->odometer->odometer_reading);
            } else {
                echo '&nbsp;';
            }

            ?></td>
        </tr>
        <tr>
            <td class="bottom_border"><strong>Description</strong></td>
            <td colspan="5" class="bottom_border">{{{ $data->work_description }}}</td>
        </tr>
        <tr>
            <td><strong>Shop Costs</strong></td>
            <td>{{{ nd_number_format($data->shop_costs) }}}</td>
            <td><strong>Purchased Costs</strong></td>
            <td>{{{ nd_number_format($data->purchased_costs) }}}</td>
            <td><strong>Total</strong></td>
            <td class="text-right">{{{ nd_number_format($data->shop_costs + $data->purchased_costs) }}}</td>
        </tr>
        <tr>
            <td><strong>Commercial Parts</strong></td>
            <td>{{{ nd_number_format($data->commercial_costs) }}}</td>
            <td><strong>Commercial Labor</strong></td>
            <td>{{{ nd_number_format($data->commercial_labor_costs) }}}</td>
            <td><strong>Total</strong></td>
            <td class="text-right">{{{ nd_number_format($data->commercial_costs + $data->commercial_labor_costs) }}}</td>
        </tr>
    </tbody>
    <tfoot>
        <tr class="success">
            <th colspan="5">&nbsp;</th>
            <th class="text-right">{{{ nd_number_format($data->commercial_costs + $data->commercial_labor_costs + $data->shop_costs + $data->purchased_costs) }}}</th>
        </tr>
    </tfoot>
</table>
</div>