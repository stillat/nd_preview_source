<div class="adapter_row" data-adapter="{{{ $adapter->getAdapterNumericIdentifier() }}}"  style="padding-left: 47px; padding-right: 47px;">
    <div class="form-group">
        <label class="col-sm-1 control-label" for="equipmentRepairWorkOrderNumber">Work Order Number</label>
        <div class="col-sm-3">
            <input type="text" id="equipmentRepairWorkOrderNumber" class="form-control text-right" name="equipment_repair_work_order_number" value="{{{ Input::old('equipment_repair_work_order_number', '')  }}}">
        </div>
        <label for="equipmentRepairOdometerReading" class="col-sm-1 control-label">Odometer Reading</label>
        <div class="col-sm-3">
            <input type="text" id="equipmentRepairOdometerReading" class="form-control text-right" name="equipment_repair_odometer_reading" value="{{{ Input::old('equipment_repair_odometer_reading', '') }}}">
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-1 control-label" for=equipmentRepairShopParts">Shop Parts</label>
        <div class="col-sm-2">
            <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-dollar"></i></span>
                <input type="text" id="equipmentRepairShopParts" class="form-control text-right" name="equipment_repair_shop_parts" value="{{{ nd_number_format(Input::old('equipment_repair_shop_parts', 0))  }}}">
            </div>
        </div>
        <label class="col-sm-1 control-label" for="equipmentRepairInvoiceParts">Invoice Parts</label>
        <div class="col-sm-2">
            <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-dollar"></i></span>
                <input type="text" id="equipmentRepairInvoiceParts" class="form-control text-right" name="equipment_repair_invoice_parts" value="{{{ nd_number_format(Input::old('equipment_repair_invoice_parts', 0)) }}}">
            </div>
        </div>
        <label for="equipmentRepairContractorCosts" class="col-sm-1 control-label">Contractor Costs</label>
        <div class="col-sm-2">
            <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-dollar"></i></span>
                <input type="text" id="equipmentRepairContractorCosts" class="form-control text-right" name="equipment_repair_contractor_costs" value="{{{ nd_number_format(Input::old('equipment_repair_contractor_costs', 0))  }}}">
            </div>
        </div>
        <label for="equipmentRepairContractorLabor" class="col-sm-1 control-label">Contractor Labor</label>
        <div class="col-sm-2">
            <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-dollar"></i></span>
                <input type="text" id="equipmentRepairContractorLabor" class="form-control text-right" name="equipment_repair_contractor_labor" value="{{{ nd_number_format(Input::old('equipment_repair_contractor_labor', 0)) }}}">
            </div>
        </div>
    </div>
    <div class="form-group row accessibility-row">
        <label for="equipmentRepairWorkDescription" class="col-sm-1 control-label">Work Description</label>
        <div class="col-sm-11">
            <textarea name="equipment_repair_work_description" id="equipmentRepairWorkDescription" cols="30" rows="1" maxlength="6553" class="form-control autogrow" style="min-height: 36px;">{{{ Input::old('equipment_repair_work_description', '') }}}</textarea>
        </div>
    </div>
</div>
