<div class="adapter_row" data-adapter="{{{ $adapter->getAdapterNumericIdentifier() }}}">
    <div class="form-group is-required">
        <label class="col-sm-2 control-label" for="serial_number">Serial Number:</label>
        <div class="col-sm-5">
            <input type="text" id="serial_number" name="equipment_serial_number" class="form-control mousetrap " value="">
            {{ Form::errorMsg('equipment_serial_number') }}
        </div>
    </div>
    <div class="form-group is-required">
        <label class="col-sm-2 control-label" for="rental_rate">Rental Rate:</label>
        <div class="col-sm-3">
            <div class="input-group">
                <span class="input-group-addon">$</span>
                <input type="number" min="0" step="any" id="rental_rate" name="equipment_rental_rate" class="form-control text-right mousetrap " value="0.0000">
            </div>
            {{ Form::errorMsg('equipment_rental_rate') }}
        </div>
        <label class="col-sm-2 control-label" for="unit_id">Rental Rate Unit:</label>
        <div class="col-sm-3">
            <input type="text" class="form-control unit_selector s2" name="equipment_rental_rate_unit" value="{{{ Input::old('road_property_length_unit', 9) }}}">
        	{{ Form::errorMsg('equipment_rental_rate_unit') }}
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2 control-label" for="make">Make:</label>
        <div class="col-sm-2">
            <input type="text" id="make" name="equipment_make" class="form-control text-right mousetrap " placeholder="Make" value="">
            {{ Form::errorMsg('equipment_make') }}
        </div>
        <label class="col-sm-1 control-label" for="model">Model:</label>
        <div class="col-sm-2">
            <input type="text" id="model" name="equipment_model" class="form-control text-right mousetrap " placeholder="Model" value="">
            {{ Form::errorMsg('equipment_model') }}
        </div>
        <label class="col-sm-1 control-label" for="year">Year:</label>
        <div class="col-sm-2">
            <input type="number" step="0" id="year" name="equipment_year" class="form-control text-right mousetrap " value="" placeholder="1990">
            {{ Form::errorMsg('equipment_year') }}
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2 control-label" for="purchase_cost">Purchase Cost:</label>
        <div class="col-sm-2">
            <div class="input-group">
                <span class="input-group-addon">$</span>
                <input type="number" min="0" step="any" id="purchase_cost" name="equipment_purchase_cost" class="form-control text-right mousetrap " value="0.0000">
            </div>
            {{ Form::errorMsg('equipment_purchase_cost') }}
        </div>
        <label class="col-sm-1 control-label" for="salvage_value">Salvage Value:</label>
        <div class="col-sm-2">
            <div class="input-group">
                <span class="input-group-addon">$</span>
                <input type="number" min="0" step="any" id="salvage_value" name="equipment_salvage_value" class="form-control text-right mousetrap " value="0.0000">
            </div>
            {{ Form::errorMsg('equipment_salvage_value') }}
        </div>
        <label class="col-sm-1 control-label" for="lifetime">Lifetime:</label>
        <div class="col-sm-2">
            <input type="number" min="0" step="0" id="lifetime" name="equipment_lifetime" class="form-control text-right mousetrap ndc_form_trail_element " value="" placeholder="Lifetime in years">
            {{ Form::errorMsg('equipment_lifetime') }}
        </div>
    </div>
</div>