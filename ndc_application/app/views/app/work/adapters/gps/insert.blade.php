<div class="form-group row accessibility-row adapter_row" data-adapterholder="true" data-adapter="{{{ $adapter->getAdapterNumericIdentifier() }}}">
	<div class="panel panel-default" data-adapter="{{{ $adapter->getAdapterNumericIdentifier() }}}">
		<div class="panel-heading">
			<h3 class="panel-title">{{{ $adapter->getName() }}}</h3>
		</div>
		<div class="panel-body">
			<label class="col-sm-1 control-label">Latitude</label>
			<div class="col-sm-3">
				<input type="text" name="gps_latitude" class="form-control mousetrap">
			</div>
			<label class="col-sm-1 control-label">Longitude</label>
			<div class="col-sm-3">
				<input type="text" name="gps_longitude" class="form-control mousetrap">
			</div>
		</div>
	</div>
</div>