<div class="adapter_row" data-adapter="{{{ $adapter->getAdapterNumericIdentifier() }}}">
	<div class="form-group">
		<label class="col-sm-2 control-label" for="roadLength">Road Length</label>
		<div class="col-sm-3">
			<input type="text" id="roadLength" class="form-control text-right" step="any" name="road_property_road_length" value="{{{ Input::old('road_property_road_length', get_update_data($adapter->getAdapterNumericIdentifier(),$record->getProperty()->work_data,'road_length',0)) }}}">
			{{ Form::errorMsg('road_length') }}
		</div>
		<label class="col-sm-1 control-label" for="roadLengthUnit">Unit</label>
		<div class="col-sm-2">
			<input type="text" id="roadLengthUnit" class="form-control unit_selector s2" name="road_property_length_unit" value="{{{ Input::old('road_property_length_unit', get_update_data($adapter->getAdapterNumericIdentifier(),$record->getProperty()->work_data,'unit_id',9)) }}}">
			{{ Form::errorMsg('road_property_length_unit') }}
		</div>
	</div>
	<div class="form-group">
		<label class="col-sm-2 control-label" for="roadSurfaceType">Surface Type</label>
		<div class="col-sm-5">
			<input type="text" id="roadSurfaceType" class="form-control s2" name="road_property_surface_type" value="{{{ Input::old('road_property_surface_type', get_update_data($adapter->getAdapterNumericIdentifier(),$record->getProperty()->work_data,'surface_type_id',0)) }}}">
			{{ Form::errorMsg('road_property_surface_type') }}
		</div>
	</div>
</div>