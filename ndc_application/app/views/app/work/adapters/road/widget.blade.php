<div class="table-responsive">
<table class="table table-hover">
    <tbody>
        <tr>
            <td><strong>Road Length</strong></td>
            <td>{{{ nd_number_format($data->road_length).' '.$unit->code }}}</td>
            <td><strong>Surface Type</strong></td>
            <td>{{{ $type->code }}}</td>
        </tr>
    </tbody>
</table>
</div>