@extends('layouts.master')
@section('pageTitle')
<i class="fa fa-file-text-o"></i> {{{ $record->getEmployee()->name }}} <span>{{{ $record->getWorkDate() }}}</span>
@stop
@section('buttonBar')
<div class="btn-group n-mt-5">
    {{ HTML::modelNaviateToolbar('work-logs.show', $record->previous, 'work-logs.index', 'work-logs.show', $record->next) }}
</div>
<div class="btn-group mr5 n-mt-5">
    <button type="button" class="btn btn-white dropdown-toggle" data-toggle="dropdown"><i class="fa fa-wrench inline-icon"></i> <span class="caret"></span></button>
    <ul class="dropdown-menu" role="menu" style="margin-left: -180px;">
        @if($record->rawData->record_status !== 5)
            <li><a href="{{{ action('App\Work\WorkLogsController@edit', array($record->getWorkID())) }}}"><i class="fa fa-pencil inline-icon"></i> Update this record</a></li>
            <li><a href="{{{ action('App\Work\WorkLogsController@edit', array($record->getWorkID())).'?mode=create-like' }}}"><i class="fa fa-plus-square-o inline-icon"></i> Create new record like this one</a></li>
        <li class="divider"></li>
        <li><a href="#" data-rcontext="{{{ $record->getWorkID() }}}" class="delete-row"><i class="fa fa-trash-o inline-icon"></i> Delete this record</a></li>
        @else
            <li class="disabled"><a disabled="disabled" class="disabled"><i class="fa fa-pencil-square-o inline-icon"></i> Update this record</a></li>
            <li><a href="{{{ action('App\Work\WorkLogsController@edit', array($record->getWorkID())).'?mode=create-like' }}}"><i class="fa fa-plus-square-o inline-icon"></i> Create new record like this one</a></li>
            <li class="divider"></li>
            <li class="disabled"><a disabled="disabled" class="delete-row"><i class="fa fa-trash-o inline-icon"></i> Delete this record</a></li>
        @endif
    </ul>
</div>
@stop
@section('content')
@if($record->rawData->record_status == 5)
<div class="row">
    <div class="col-sm-12">
        <div class="alert alert-info">
            <strong>Invoice Notice</strong> The invoice this record belongs to is locked. This means the record cannot be updated or removed.
        </div>
    </div>
</div>
@endif
@if($record->isExcludedFromReports())
<div class="row">
    <div class="col-sm-12">
        <div class="alert alert-warning">
            <strong>Reporting Notice</strong> Because of the settings set for this work record, it will not be included when generating reports.</a>.
        </div>
    </div>
</div>
@endif
<div class="row">
    @include('app.work.view_widgets.chart')
    <div class="col-sm-8">
        <div class="row">
            @include('app.work.view_widgets.employee')
            @include('app.work.view_widgets.billing')
        </div>
    </div>
</div>
<div class="row">
    <div class="col-sm-12">
        <ul class="nav nav-tabs">
            <li class="active bb_settings"><a href="#general" data-toggle="tab"><strong><i class="fa fa-file-text-o"></i></strong> <span class="hidden-xs hidden-sm hidden-md">General Information</span></a></li>
            <li class="bb_property"><a href="#property" data-toggle="tab"><strong><i class="fa fa-home"></i> <span class="hidden-xs hidden-sm hidden-md">Property: {{{ $record->getProperty()->code }}}</span></strong></a></li>
            <li class="bb_material"><a href="#materials" data-toggle="tab"><strong><i class="fa fa-cubes"></i> <span class="hidden-xs hidden-sm hidden-md">Materials</span></strong></a></li>
            <li class="bb_equip"><a href="#equip" data-toggle="tab"><strong><i class="fa fa-truck"></i> <span class="hidden-xs hidden-sm hidden-md">Equipment &amp; Fuels</span></strong></a></li>
            <li class="bb_fuel"><a href="#fuels" data-toggle="tab"><strong><i class="ndc-fuel"></i> <span class="hidden-xs hidden-sm hidden-md">Fuels Only</span></strong></a></li>
            <li class="bb_forums"><a href="#reportValues" data-toggle="tab"><strong><i class="ndc-chart-pie"></i> <span class="hidden-xs hidden-sm hidden-md">Report Values</span></strong></a></li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane active" id="general">
                <h4 class="hidden-lg"><i class="fa fa-coffee"></i> General Information</h4>
                @include('app.work.view_widgets.general')
            </div>
            <div class="tab-pane" id="property">
                <h4 class="hidden-lg"><i class="fa fa-home"></i> Property: {{{ $record->getProperty()->code }}}</h4>
                @include('app.work.view_widgets.property')
            </div>
            <div class="tab-pane" id="materials">
                <h4 class="hidden-lg"><i class="fa fa-cubes"></i> Materials</h4>
                @if(count($record->getMaterials()) > 0)
                @include('app.work.view_widgets.materials')
                @else
                <p class="mt5">There is no data to display here.</p>
                @endif
            </div>
            <div class="tab-pane" id="equip">
                <h4 class="hidden-lg"><i class="fa fa-truck"></i> Equipment &amp; Fuels</h4>
                @if(count($record->getEquipmentUnits()) > 0)
                @include('app.work.view_widgets.equip_fuels')
                @else
                <p class="mt5">There is no data to display here.</p>
                @endif
            </div>
            <div class="tab-pane" id="fuels">
                <h4 class="hidden-lg"><i class="ndc-fuel"></i> Fuels Only</h4>
                @if(count($record->getEquipmentUnits()) > 0)
                @include('app.work.view_widgets.fuels')
                @else
                <p class="mt5">There is no data to display here.</p>
                @endif
            </div>
            <div class="tab-pane" id="reportValues">
                <h4 class="hidden-lg"><i class="ndc-chart-pie"></i> Report Values</h4>
                @include('app.work.view_widgets.report_values')
            </div>
        </div>
    </div>
</div>
@include('layouts.dialogs.remove', array('title' => 'Remove Work Record', 'agreeText' => 'Remove work record', 'content' => 'Are you sure you want to remove this work record?', 'redirectToList' => true, 'action' => 'App\Work\WorkLogsController@destroy'))
@stop
@section('additionalScripts')
<script>
    var recordTotals = [
            { label: "Employee", data: [[1,{{{ $record->getRecordTotals()->employeeCombined }}}]], color: '#1abc9c'},
            { label: "Material", data: [[1,{{{ $record->getRecordTotals()->materialCost }}}]], color: '#3498db'},
            { label: "Equipment", data: [[1,{{{ $record->getRecordTotals()->equipmentUnitCost }}}]], color: '#d35400'},
            { label: "Fuel", data: [[1,{{{ $record->getRecordTotals()->fuelsCost }}}]], color: '#C4A5D1'},
            { label: "Billing", data: [[1,{{{ $record->getRecordTotals()->miscBilling }}}]], color: '#D6D6D6'}
    	 ];
</script>
<script src="{{{ app_url() }}}/assets/js/work_view.js" defer></script>
@stop