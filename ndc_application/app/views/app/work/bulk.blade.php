@if(count($workRecords) > 0)
    <div class="row">
        <div class="col-sm-3">
            <select class="form-control" name="bulk_action" id="bulkAction">
                <option value="remove">Remove selected work records</option>
                <option value="report_available">Make records visible to reports</option>
                <option value="report_hide">Hide records from reports</option>
                <option value="update_dates">Change the work date for the selected records</option>
                <option value="update_activity">Change the activity for the selected records</option>
                <option value="update_road">Change the road for the selected records</option>
                <option value="update_project">Change the project for the selected records</option>
                <option value="update_department">Change the department for the selected records</option>
                <option value="update_district">Change the district for the selected records</option>
            </select>
        </div>
        <div class="col-sm-2">
            <a class="btn btn-block btn-white" id="applyBulkAction">Apply batch action</a>
        </div>
    </div>
@endif