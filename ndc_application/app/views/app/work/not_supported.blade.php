@extends('layouts.master')

@section('pageTitle')
<i class="fa fa-coffee"></i> Log work
@stop

@section('buttonBar')
<div class="btn-group n-mt-5">
  <a href="{{ Redirector::getRoute(route('work-logs.index')) }}" class="btn btn-danger ndc_form_leave">Cancel</a>
</div>
@stop


@section('content')
<div class="row">
	<div class="col-sm-12">
		<div class="panel panel-default">
			<div class="panel-heading">
				<h4 class="dark"><i class="fa fa-frown-o"></i> Device Not Supported</h4>
			</div>
			<div class="panel-body">
				<p class="lead">Hello there! It's nice to see our application being used on all devices, but unfortunately we cannot offer this feature on mobile devices at this time.</p>
				<p>You can continue working once you get back to your desk with a PC, Mac or assorted tablet devices.</p>
			</div>
		</div>
	</div>
</div>
@stop