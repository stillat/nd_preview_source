<div class="table-responsive">
    <table class="table table-hover table-condensed">
        <thead>
            <tr>
                <th>Fuel</th>
                <th class="text-right">Quantity</th>
                <th>Unit</th>
                <th class="text-right">Cost</th>
                <th class="text-right">Total</th>
            </tr>
        </thead>
        <tbody>
            <?php $totalFuelsCost = 0; ?>
            @foreach($record->getEquipmentUnits() as $equipment)
            @foreach($equipment->fuels as $fuel)
            <?php $totalFuelsCost += $fuel->total_cost; ?>
            <tr>
                <td><strong>{{{ $fuel->fuel->code }}}</strong> {{{ $fuel->fuel->name }}}</td>
                <td class="text-right"><span data-type="numeric">{{{ $fuel->total_quantity }}}</span></td>
                <td>{{{ $fuel->fuel->unit->name }}}</td>
                <td class="text-right"><span data-type="numeric">{{{ $fuel->historic_cost }}}</span></td>
                <td class="text-right"><span data-type="numeric">{{{ $fuel->total_cost }}}</span></td>
            </tr>
            @endforeach
            @endforeach
        </tbody>
        <tfoot>
            <tr class="success">
                <th>Total</th>
                <th class="text-right" colspan="4"><span data-type="numeric">{{{ $totalFuelsCost }}}</span></th>
            </tr>
        </tfoot>
        <?php unset($totalFuelsCost); ?>
    </table>
</div>