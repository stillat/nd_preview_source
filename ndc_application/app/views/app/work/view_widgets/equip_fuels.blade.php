<div class="table-responsive">
    <table class="table table-hover table-condensed">
        <thead>
            <tr>
                <th>Equipment Unit</th>
                <th>Odometer</th>
                <th class="text-right">Quantity</th>
                <th class="text-right">Rate</th>
                <th class="text-right">Total</th>
            </tr>
        </thead>
        <?php $equipmentTotalCost = 0; $equipmentTotalFuel = 0; ?>
        @foreach($record->getEquipmentUnits() as $equipment)
        <?php $equipmentFuelTotal = 0; $equipmentTotalCost += $equipment->total_cost; ?>
        <tbody>
            <tr class="equipment_row">
                <td><strong>{{{ $equipment->equipment->code }}}</strong> {{{ $equipment->equipment->name }}}</td>
                <td>
                    @if($equipment->equipment->odometer == null)
                    &nbsp;
                    @else
                    {{{ $equipment->equipment->odometer->odometer_reading }}}
                    @endif
                </td>
                <td class="text-right"><span data-type="numeric">{{{ $equipment->total_quantity }}}</span> ({{{ $equipment->equipment->unit->name }}})</td>
                <td class="text-right"><span data-type="numeric">{{{ $equipment->historic_cost }}}</span></td>
                <td class="text-right"><span data-type="numeric">{{{ $equipment->total_cost }}}</span></td>
            </tr>
            <tr>
                <td colspan="5">
                    <table class="table table-hover table-condensed">
                        <thead>
                            <tr>
                                <th>Fuel</th>
                                <th class="text-right">Quantity</th>
                                <th>Unit</th>
                                <th class="text-right">Cost</th>
                                <th class="text-right">Total</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                                $recordFuelTotal = 0;
                                ?>
                            @foreach($equipment->fuels as $fuel)
                            <?php $recordFuelTotal += $fuel->total_cost; $equipmentTotalFuel += $fuel->total_cost; ?>
                            <tr>
                                <td><strong>{{{ $fuel->fuel->code }}}</strong> {{{ $fuel->fuel->name }}}</td>
                                <td class="text-right"><span data-type="numeric">{{{ $fuel->total_quantity }}}</span></td>
                                <td>{{{ $fuel->fuel->unit->name }}}</td>
                                <td class="text-right"><span data-type="numeric">{{{ $fuel->historic_cost }}}</span></td>
                                <td class="text-right"><span data-type="numeric">{{{ $fuel->total_cost }}}</span></td>
                            </tr>
                            @endforeach
                        </tbody>
                        <tfoot>
                            <tr class="fuel_row">
                                <th>Total</th>
                                <th colspan="4" class="text-right"><span data-type="numeric">{{{ $recordFuelTotal }}}</span></th>
                            </tr>
                        </tfoot>
                        <?php $equipmentFuelTotal += $recordFuelTotal; unset($recordFuelTotal); ?>
                    </table>
                </td>
            </tr>
        </tbody>
        <?php unset($equipmentFuelTotal); ?>
        @endforeach
        <tfoot>
            <tr>
                <th>Grand Equipment Total</th>
                <th colspan="4" class="text-right"><span data-type="numeric">{{{ $equipmentTotalCost }}}</span></th>
            </tr>
            <tr>
                <th>Grand Equipment Fuel Total</th>
                <th colspan="4" class="text-right"><span data-type="numeric">{{{ $equipmentTotalFuel }}}</span></th>
            </tr>
            <tr class="success">
                <th>Total</th>
                <th colspan="4" class="text-right"><span data-type="numeric">{{{ $equipmentTotalCost + $equipmentTotalFuel }}}</span></th>
            </tr>
        </tfoot>
        <?php unset($equipmentTotalCost); unset($equipmentTotalFuel); ?>
    </table>
</div>