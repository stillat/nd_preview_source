<div class="col-sm-12">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title"><i class="fa fa-file-o"></i> Miscellaneous Billing</h3>
        </div>
        @if($record->getRecordTotals()->miscBilling > 0)
        <table class="table table-hover">
            <tbody>
                <tr>
                    <td class="wd-200"><strong>Billing Quantity</strong></td>
                    <td class="text-right"><span data-type="numeric">{{{ $record->getMiscBilling()->quantity }}}</span> <strong>@</strong> <span data-type="numeric">{{{ $record->getMiscBilling()->rate }}}</span> ({{{ $reader->getMeasurementUnit($record->getMiscBilling()->unit_id)->name }}})</td>
                </tr>
                <tr>
                    <td><strong>Billing Description</strong></td>
                    <td>{{{ $record->getMiscBilling()->description }}}</td>
                </tr>
            </tbody>
            <tfoot>
                <tr class="success">
                    <th>Total</th>
                    <th class="text-right"><span data-type="numeric">{{{ $record->getRecordTotals()->miscBilling }}}</span></th>
                </tr>
            </tfoot>
        </table>
        @else
        <div class="panel-body">
            <p class="text-muted">There is no data to display here.</p>
        </div>
        @endif
    </div>
</div>