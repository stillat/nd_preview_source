<p>The following table will explain how this record is interpreted by the reporting engine. The following values can be
    used during the creation of new <a href="{{ url('settings/reports/equations') }}">reporting equations</a>.</p>

<table class="table table-striped table-hover">
    <thead>
    <tr>
        <th>Report Name</th>
        <th>Description</th>
        <th class="text-right">Value</th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td colspan="3" style="background-color: #faf2cc"><strong><i class="fa fa-user"></i> Employee Reporting Values</strong></td>
    </tr>
    <tr>
        <td><code>employee.total.hours.regular</code></td>
        <td>The number of regular (non-overtime) employee hours worked logged on the record.</td>
        <td class="text-right"><code><span data-type="numeric">{{{ $record->rawData->employee_hours_regular_worked }}}</span></code></td>
    </tr>
    <tr>
        <td><code>employee.total.hours.overtime</code></td>
        <td>The number of overtime employee hours logged on the record.</td>
        <td class="text-right"><code><span data-type="numeric">{{{ $record->rawData->employee_hours_overtime_worked }}}</span></code></td>
    </tr>
    <tr>
        <td><code>employee.wage.regular</code></td>
        <td>The employee's historic regular wage.</td>
        <td class="text-right"><code><span data-type="numeric">{{{ $record->rawData->employee_historic_wage_regular }}}</span></code></td>
    </tr>
    <tr>
        <td><code>employee.fringe.regular</code></td>
        <td>The employee's historic regular hour fringe benefits.</td>
        <td class="text-right"><code><span data-type="numeric">{{{ $record->rawData->employee_historic_fringe_regular }}}</span></code></td>
    </tr>
    <tr>
        <td><code>employee.wage.overtime</code></td>
        <td>The employee's historic overtime wage.</td>
        <td class="text-right"><code><span data-type="numeric">{{{ $record->rawData->employee_historic_wage_overtime  }}}</span></code></td>
    </tr>
    <tr>
        <td><code>employee.fringe.overtime</code></td>
        <td>The employee's historic overtime hour fringe benefits.</td>
        <td class="text-right"><code><span data-type="numeric">{{{ $record->rawData->employee_historic_fringe_overtime }}}</span></code></td>
    </tr>
    <tr>
        <td><code>employee.cost.overtime</code></td>
        <td>The employee overtime hours cost for the record.</td>
        <td class="text-right"><code><span data-type="numeric">{{{ $record->rawData->total_employee_overtime_cost }}}</span></code></td>
    </tr>
    <tr>
        <td><code>employee.cost.regular</code></td>
        <td>The employee regular hours cost for the record.</td>
        <td class="text-right"><code><span data-type="numeric">{{{ $record->rawData->total_employee_regular_cost }}}</span></code></td>
    </tr>
    <tr>
        <td><code>employee.cost</code></td>
        <td>The employee hours total cost (including regular and overtime) for the record.</td>
        <td class="text-right"><code><span data-type="numeric">{{{ $record->rawData->total_employee_combined_cost }}}</span></code></td>
    </tr>
    <tr>
        <td colspan="3" style="background-color: #faf2cc"><strong><i class="ndc-fuel"></i> Fuel Reporting Values</strong></td>
    </tr>
    <tr>
        <td><code>fuel.quantity</code></td>
        <td>The total quantity of all fuels on the record.</td>
        <td class="text-right"><code><span data-type="numeric">{{{ $record->rawData->total_fuels_quantity }}}</span></code></td>
    </tr>
    <tr>
        <td><code>fuel.total</code></td>
        <td>The total cost of all fuels on the record.</td>
        <td class="text-right"><code><span data-type="numeric">{{{ $record->rawData->total_fuels_cost }}}</span></code></td>
    </tr>
    <tr>
        <td colspan="3" style="background-color: #faf2cc"><strong><i class="fa fa-cubes"></i> Material Reporting Values</strong></td>
    </tr>
    <tr>
        <td><code>material.quantity</code></td>
        <td>The total quantity of all materials on the record.</td>
        <td class="text-right"><code><span data-type="numeric">{{{ $record->rawData->total_materials_quantity }}}</span></code></td>
    </tr>
    <tr>
        <td><code>material.total</code></td>
        <td>The total cost of all materials on the record.</td>
        <td class="text-right"><code><span data-type="numeric">{{{ $record->rawData->total_materials_cost }}}</span></code></td>
    </tr>
    <tr>
        <td colspan="3" style="background-color: #faf2cc"><strong><i class="fa fa-truck"></i> Equipment Unit Reporting Values</strong></td>
    </tr>
    <tr>
        <td><code>equipment.total.quantity</code></td>
        <td>The total quantity of all equipment unit hours or miles on the record.</td>
        <td class="text-right"><code><span data-type="numeric">{{{ $record->rawData->total_equipment_units_quantity }}}</span></code></td>
    </tr>
    <tr>
        <td><code>equipment.total.cost</code></td>
        <td>The total cost of all equipment unit on the record.</td>
        <td class="text-right"><code><span data-type="numeric">{{{ $record->rawData->total_equipment_units_cost }}}</span></code></td>
    </tr>
    <tr>
        <td><code>equipment.total.costWithFuels</code></td>
        <td>The total cost of equipment units and fuels combined.</td>
        <td class="text-right"><code><span data-type="numeric">{{{ $record->rawData->total_equipment_and_fuels_cost }}}</span></code></td>
    </tr>
    <tr>
        <td colspan="3" style="background-color: #faf2cc"><strong><i class="fa fa-home"></i> Property Reporting Values</strong></td>
    </tr>
    <tr>
        <td><code>property.total</code></td>
        <td>The total property cost (such as equipment repair costs) for the record.</td>
        <td class="text-right"><code><span data-type="numeric">{{{ $record->rawData->total_property_cost }}}</span></code></td>
    </tr>
    <tr>
        <td colspan="3" style="background-color: #faf2cc"><strong><i class="fa ndcf-bill"></i> Miscellaneous Billing Reporting Values</strong></td>
    </tr>
    <tr>
        <td><code>billing.rate</code></td>
        <td>The miscellaneous billing rate for the record.</td>
        <td class="text-right"><code><span data-type="numeric">{{{ $record->rawData->misc_billing_rate }}}</span></code></td>
    </tr>
    <tr>
        <td><code>billing.quantity</code></td>
        <td>The miscellaneous billing quantity for the record.</td>
        <td class="text-right"><code><span data-type="numeric">{{{ $record->rawData->misc_billing_quantity }}}</span></code></td>
    </tr>
    <tr>
        <td><code>billing.total</code></td>
        <td>The total miscellaneous billing cost for the record.</td>
        <td class="text-right"><code><span data-type="numeric">{{{ $record->rawData->total_misc_billing_cost }}}</span></code></td>
    </tr>
    </tbody>
</table>