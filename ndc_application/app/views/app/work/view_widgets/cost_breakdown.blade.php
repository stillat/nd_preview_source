<div class="col-sm-4">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title"><i class="fa fa-dollar"></i> Cost Breakdown</h3>
        </div>
        <table class="table table-hover">
            <tbody>
                <tr>
                    <td><strong>Employee</strong></td>
                    <td class="text-right"><span data-type="numeric">{{{ $record->getRecordTotals()->employeeCombined }}}</span></td>
                </tr>
                <tr>
                    <td><strong>Equipment</strong></td>
                    <td class="text-right"><span data-type="numeric">{{{ $record->getRecordTotals()->equipmentUnitCost }}}</span></td>
                </tr>
                <tr>
                    <td><strong>Fuel</strong></td>
                    <td class="text-right"><span data-type="numeric">{{{ $record->getRecordTotals()->fuelsCost }}}</span></td>
                </tr>
                <tr>
                    <td><strong>Material</strong></td>
                    <td class="text-right"><span data-type="numeric">{{{ $record->getRecordTotals()->materialCost }}}</span></td>
                </tr>
                @if($record->getRecordTotals()->miscBilling > 0)
                <tr>
                    <td><strong>Miscellaneous Billing</strong></td>
                    <td class="text-right"><span data-type="numeric">{{{ $record->getRecordTotals()->miscBilling }}}</span></td>
                </tr>
                @endif
            </tbody>
            <tfoot>
                <tr class="success">
                    <th>Total</th>
                    <th class="text-right"><span data-type="numeric">{{{ $record->getRecordTotals()->miscBilling + $record->getRecordTotals()->materialCost + $record->getRecordTotals()->fuelsCost + $record->getRecordTotals()->equipmentUnitCost + $record->getRecordTotals()->employeeCombined }}}</span></th>
                </tr>
            </tfoot>
        </table>
    </div>
</div>