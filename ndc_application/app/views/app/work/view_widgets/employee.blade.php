<div class="col-sm-12">
    <div class="panel panel-employee">
        <div class="panel-heading">
            <h3 class="panel-title"><i class="fa fa-user"></i> {{{ $record->getEmployee()->name }}}</h3>
        </div>
        <table class="table table-hover">
            <tbody>
                <tr>
                    <td class="bottom_border wd-200"><strong>Date</strong></td>
                    <td class="bottom_border">{{{ $record->getWorkDate() }}}</td>
                </tr>
                <tr>
                    <td><strong>Employee</strong></td>
                    <td>{{{ $record->getEmployee()->name }}}</td>
                </tr>
                <tr>
                    <td><strong>Regular Hours</strong></td>
                    <td class="text-right"><span data-type="numeric">{{{ $record->getRecordTotals()->employeeRegularHours }}}</span></td>
                </tr>
                <tr>
                    <td><strong>Overtime Hours</strong></td>
                    <td class="text-right"><span data-type="numeric">{{{ $record->getRecordTotals()->employeeOvertimeHours }}}</span></td>
                </tr>
            </tbody>
            <tfoot>
                <tr class="success">
                    <th>Total Hours</th>

                    <th class="text-right"><span data-type="numeric">{{{ $record->getRecordTotals()->employeeTotalHours }}}</span></th>
                </tr>
            </tfoot>
        </table>
    </div>
</div>