<div class="table-responsive">
    <table class="table table-hover">
        <thead>
            <tr>
                <th>Material</th>
                <th class="text-right">Quantity</th>
                <th class="text-right">Unit Cost</th>
                <th class="text-right">Total Cost</th>
            </tr>
        </thead>
        <tbody>
            @foreach($record->getMaterials() as $material)
            <tr>
                <td>{{{ $material->material->code }}}</td>
                <td class="text-right"><span data-type="numeric">{{{ $material->total_quantity }}}</span> ({{{ $material->material->unit->name }}})</td>
                <td class="text-right"><span data-type="numeric">{{{ $material->historic_cost }}}</span></td>
                <td class="text-right"><span data-type="numeric">{{{ $material->total_cost }}}</span></td>
            </tr>
            @endforeach
            @if(count($record->getMaterials()) == 0)
            <tr>
                <td colspan="4">
                    <p class="text-muted">There are no material records to display.</p>
                </td>
            </tr>
            @endif
        </tbody>
        <tfoot>
            <tr class="success">
                <th>Grand Totals</th>
                <th class="text-right"><span data-type="numeric">{{{ $record->getRecordTotals()->materialQuantity }}}</span></th>
                <th>&nbsp;</th>
                <th class="text-right"><span data-type="numeric">{{{ $record->getRecordTotals()->materialCost }}}</span></th>
            </tr>
        </tfoot>
    </table>
</div>