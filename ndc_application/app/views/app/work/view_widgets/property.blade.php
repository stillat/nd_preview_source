@if($record->getProperty()->work_data != null)
@foreach($record->getProperty()->work_data as $adapter => $data)
{{ $manager->formatPropertyWidget($adapter, $data) }}
@endforeach
@else
<p class="mt5">There is no data to display here.</p>
@endif