<table class="table table-hover table-fixed">
    <tbody>
    <tr>
        <td colspan="1"><strong><i class="fa fa-road"></i> Road</strong></td>
        <td colspan="1" class="text-left"><a href="{{{ url('properties', $record->getOrganizationProperty()->id) }}}">{{{ $record->getOrganizationProperty()->code.' - '.$record->getOrganizationProperty()->name }}}</a></td>
        <td rowspan="2" colspan="2" style="border: 1px solid #ddd;">
            <p><strong>This is how this record's properties will appear on reports</strong>:</p>
            {{ $record->getReportPropertyDisplay() }}
        </td>
    </tr>
    <tr>
        <td><strong>Property</strong></td>
        <td><a href="{{{ url('properties', $record->getProperty()->id) }}}">{{{ $record->getProperty()->code.' - '.$record->getProperty()->name }}}</a></td>

    </tr>
    <tr>
        <td><strong>Department</strong></td>
        <td><a href="{{{ url('departments', $record->getDepartment()->id) }}}">{{{ $record->getDepartment()->code }}}</a></td>
        <td><strong>Project</strong></td>
        <td><a href="{{{ url('projects', $record->getProject()->id) }}}">{{{ $record->getProject()->code }}}</a></td>
    </tr>
    <tr>
        <td><strong>Employee</strong></td>
        <td><a href="{{{ url('employees', $record->getEmployee()->id) }}}">{{{ $record->getEmployee()->name }}}</a></td>
        <td><strong>District</strong></td>
        <td><a href="{{{ url('districts', $record->getDistrict()->id) }}}">{{{ $record->getDistrict()->code }}}</a></td>
    </tr>
    <tr>
        <td><strong>Activity</strong></td>
        <td><a href="{{{ url('activities', $record->getActivity()->id) }}}">{{{ $record->getActivity()->code }}}</a></td>
        @if(strlen($record->getActivityDescription()) > 0)
            <td colspan="2">{{{ $record->getActivityDescription() }}}</td>
        @else
            <td></td>
            <td></td>
        @endif
    </tr>
    </tbody>
</table>