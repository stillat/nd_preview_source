<div class="col-sm-4 hidden-print">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">Record Cost Breakdown</h3>
        </div>
        <?php
            $recordTotal = $record->getRecordTotals()->miscBilling + $record->getRecordTotals()->materialCost + $record->getRecordTotals()->fuelsCost + $record->getRecordTotals()->equipmentUnitCost + $record->getRecordTotals()->employeeCombined;

            ?>
        <script>var RECORD_TOTAL_COST_CALC = <?php echo $recordTotal; ?>;</script>
        @if($recordTotal > 0)
        <div class="panel-body">
            <div id="recordCostSummary" style="width: 100%; height: 150px;"></div>
        </div>
        @else
        <div class="panel-body" style="height: 150px;">
            <p style="text-align: center; margin-top: 45px;">No data available for this chart.</p>
        </div>
        @endif
        <table class="table table-hover table-condensed">
            <tbody>
                <tr>
                    <td><strong>Employee</strong></td>
                    <td class="text-right"><span data-type="numeric">{{{ $record->getRecordTotals()->employeeCombined }}}</span></td>
                </tr>
                <tr>
                    <td><strong>Equipment</strong></td>
                    <td class="text-right"><span data-type="numeric">{{{ $record->getRecordTotals()->equipmentUnitCost }}}</span></td>
                </tr>
                <tr>
                    <td><strong>Fuel</strong></td>
                    <td class="text-right"><span data-type="numeric">{{{ $record->getRecordTotals()->fuelsCost }}}</span></td>
                </tr>
                <tr>
                    <td><strong>Material</strong></td>
                    <td class="text-right"><span data-type="numeric">{{{ $record->getRecordTotals()->materialCost }}}</span></td>
                </tr>
                @if($record->getRecordTotals()->miscBilling > 0)
                <tr>
                    <td><strong>Miscellaneous Billing</strong></td>
                    <td class="text-right"><span data-type="numeric">{{{ $record->getRecordTotals()->miscBilling }}}</span></td>
                </tr>
                @endif
            </tbody>
            <tfoot>
                <tr class="success">
                    <th>Total</th>
                    <th class="text-right"><span data-type="numeric">{{{ $recordTotal }}}</span></th>
                </tr>
            </tfoot>
        </table>
    </div>
</div>