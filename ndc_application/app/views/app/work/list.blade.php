@extends('layouts.master')

@section('pageTitle')
    <i class="fa fa-coffee"></i> Work Records

    @if($searchMode)
        <small>Search Results</small>
    @endif
@stop

@section('buttonBar')
    @if(count($workYears) > 0 && !$searchMode)
        <div class="btn-group n-mt-5">
            <button type="button" class="btn btn-white dropdown-toggle mr10" data-toggle="dropdown"><i
                        class="fa fa-clock-o inline-icon"></i>
                @if (user_settings()['active_year'] == -1)
                    All Years
                @elseif (user_settings()['active_year'] == -2)
                    Last Month
                @elseif (user_settings()['active_year'] == -3)
                    This Month
                @elseif (user_settings()['active_year'] == -4)
                    Today
                @else
                    {{{ user_settings()['active_year'] }}}
                @endif
                <span class="caret"/></button>
            <ul class="dropdown-menu" role="menu">
                <li><a href="{{{ action('App\UtilityController@getChangeActiveYear', array('today')) }}}">Today</a></li>
                <li><a href="{{{ action('App\UtilityController@getChangeActiveYear', array('tm')) }}}">This Month</a>
                </li>
                <li><a href="{{{ action('App\UtilityController@getChangeActiveYear', array('lm')) }}}">Last Month</a>
                </li>
                <li><a href="{{{ action('App\UtilityController@getChangeActiveYear', array('all')) }}}">All Years</a>
                </li>
                <li class="divider"></li>
                @foreach($workYears as $year)
                    <li><a href="{{{ action('App\UtilityController@getChangeActiveYear', array($year)) }}}"><?php

                            if ($year == user_settings()['active_year']) {
                                echo '<i class="fa fa-clock-o inline-icon"></i> ';
                            }

                            ?>{{{ $year }}}</a></li>
                @endforeach
            </ul>
        </div>
    @endif
    <div class="btn-group n-mt-5">
        <button type="button" class="btn btn-white dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
            <i class="fa fa-download inline-icon"></i> Export <span class="caret"></span>
        </button>
        <ul class="dropdown-menu" role="menu" style="margin-left: -58px;">
            <li><a href="{{ export_csv('work_records').$workRecordIDs }}">Export this page</a></li>
            <li><a href="{{ action('App\DataTools\DataExportController@getExportRecordRange') }}" data-toggle="modal"
                   data-target="#modalHousing">Export a date range</a></li>
        </ul>
        <a href="{{ action('App\Work\WorkLogsController@create') }}" class="btn btn-white nd-add"><i
                    class="fa fa-plus inline-icon"></i> Add Work Record</a>
    </div>
    <div class="btn-group n-mt-5">
        <button type="button" class="btn btn-white dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
            <i class="fa fa-wrench inline-icon"></i> <span class="caret"></span>
        </button>
        <ul class="dropdown-menu" role="menu" style="margin-left: -203px;">
            <li><a href="{{ action('App\UtilityController@getClearEmptyFuelMaterialRecords') }}">Clear empty fuel and material records</a></li>
        </ul>
    </div>
@stop

@section('content')

    @if($isSortingDisabled)
        <div class="row hidden-print">
            <div class="col-sm-12">
                <div class="alert alert-info">
                    <strong>Heads Up!</strong> Custom sorting is disabled when using the "All Years" filter. So for now,
                    we are sorting your records by the date they were created, starting with the newest ones first.
                </div>
            </div>
        </div>
    @endif

    @if ($searchMode)
        <div class="row">
            <div class="col-sm-12">
                <h4><i class="fa fa-filter"></i> Search Filters</h4>
                <dl class="dl-horizontal">
                    {{ Cache::get(Auth::user()->last_tenant . Auth::user()->id . '_work_record_search_filter') }}
                </dl>
            </div>
        </div>
    @endif

    <div class="row hidden-print">
        <div class="col-sm-12">
            {{ $paginator->appends(array_except(Input::query(), Paginator::getPageName()))->links() }}
        </div>
    </div>

    <div class="row">

        <div class="col-sm-12">
{{ \Debugbar::startMeasure('tab', 'Rendering WL Table') }}
            @if(count($workRecords) > 0)
                @include('app.work.partials.table')
            @else
                @if($searchMode)
                    @include('partials.no_search_results', ['icon' => 'fa fa-coffee'])
                @else
                    @include('partials.no_results')
                @endif
            @endif
{{ \Debugbar::stopMeasure('tab') }}
        </div>

    </div>

    <div class="row hidden-print">
        <div class="col-sm-12">
            {{ $paginator->appends(array_except(Input::query(), Paginator::getPageName()))->links() }}
        </div>
        @include('app.work.bulk')
    </div>
    @include('app.work.options')
@stop

@section('additionalScripts')
    <script src="{{{ app_url() }}}/assets/js/work_table.min.js{{ UI::getBuster() }}" defer></script>
    <script>
        var startDate = '{{ $startDate }}';
        var endDate = '{{ $endDate }}';
    </script>
    <script src="{{{ app_url() }}}/assets/js/dep/chosen.jquery.min.js" defer></script>
    <script src="{{{ app_url() }}}/assets/js/data/export.min.js" defer></script>
@stop