@extends('layouts.master')
@section('pageTitle')
    <i class="fa fa-dashboard"></i> {{{ Str::limit($currentAccount->account_name, 30) }}}
@stop
@section('extraBodyClass')
    application
@stop
@section('buttonBar')
    <div class="btn-group">
        <a href="{{ action('App\Work\WorkLogsController@create') }}" class="btn btn-white nd-add n-mt-5 mr5"><i
                    class="fa fa-coffee inline-icon"></i> Log Work</a>
        @if($showChooseAccount)
            <a href="{{ action('App\ServiceAccountController@getDestroy') }}" class="btn btn-white n-mt-5"><i
                        class="fa fa-cloud inline-icon"></i> Choose another account</a>
        @endif
    </div>
@stop
@section('content')
    <div class="row">
        <div class="col-lg-3 col-sm-3">
            <div class="row">
                <div class="col-sm-12">
                    @include('app.dashboard_widgets.user_card')
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    @include('app.dashboard_widgets.info')
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-sm-4">
            @include('app.dashboard_widgets.activity_stream')
        </div>
        <div class="col-lg-3 col-sm-3">
            @include('app.dashboard_widgets.manual')
        </div>
    </div>
@stop

@section('additionalScripts')
    <script src="{{{ app_url() }}}/assets/js/dashboards/min/main.min.js" defer></script>
@stop