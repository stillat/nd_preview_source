@extends('layouts.master')
@section('pageTitle')
    <i class="fa fa-clock-o"></i> {{{ Str::limit($currentAccount->account_name, 30) }}} - Recent Activity Stream
@stop
@section('buttonBar')
    <div class="btn-group">
        <a href="{{ action('App\Work\WorkLogsController@create') }}" class="btn btn-white nd-add n-mt-5 mr5"><i
                    class="fa fa-coffee inline-icon"></i> Log Work</a>
        @if($showChooseAccount)
            <a href="{{ action('App\ServiceAccountController@getDestroy') }}" class="btn btn-white n-mt-5"><i
                        class="fa fa-cloud inline-icon"></i> Choose another account</a>
        @endif
    </div>
@stop
@section('content')
    <div class="row">
        <div class="col-sm-12">
            {{ $streamedData->links() }}
        </div>
    </div>
    <div class="row">
        @if($firstStream != null)
            <div class="col-sm-4 activity-list">
                <h3>Newest</h3>
                @if($firstStream != null)
                    {{ $firstStream }}
                @endif
            </div>
            <div class="col-sm-4 activity-list">
                <h3>Older</h3>
                @if($secondStream != null)
                    {{ $secondStream }}
                @else
                    <p class="lead">There is nothing to show here</p>
                @endif
            </div>
            <div class="col-sm-4 activity-list">
                <h3>Oldest</h3>
                @if($thirdStream != null)
                    {{ $thirdStream }}
                @else
                    <p class="lead">There is nothing to show here</p>
                @endif
            </div>
        @else
            @include('partials.no_activity')
        @endif
    </div>
    <div class="row">
        <div class="col-sm-12">
            {{ $streamedData->links() }}
        </div>
    </div>



@stop