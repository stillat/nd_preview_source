<div class="panel panel-default">
    <div class="panel-body">
        <div class="row">
            <div class="col-sm-12">
                <input type="text" class="form-control" id="slaFilter" onkeyup="return filterServiceAccounts();" placeholder="Filter service accounts">
            </div>
        </div>
        <div class="activity-list">
            @if($user->admin == true)
                <div class="media act-media">
                    <a class="pull-left" href="#"><i class="fa fa-cloud" style="color: darkslategray;"></i></a>
                    <div class="media-body act-media-body">
                        <strong>Administrator Access</strong>
                        <br><small>Administrators can access all service accounts by default.</small>
                    </div>
                </div>
            @endif
            @if(count($serviceAccounts) == 0 and $user->admin == false)
                <p>You do not have any service accounts associated with your account.</p>
            @else
                @foreach($serviceAccounts as $account)
                    <div class="media act-media sla">
                        <a class="pull-left" href="#"><i class="fa fa-cloud"></i></a>
                        <div class="media-body act-media-body">
                            <div class="row">
                                <div class="col-md-8">
                                    <h4 style="display: inline-block; margin-top:0px;">{{{ $account->account_name }}}</h4>
                                    <br>
                                    <br><small>Created on <strong>{{{ with(new Carbon($account->created_at))->toDateTimeString() }}}</strong>. Last updated: <strong>{{{ with(new Carbon($account->updated_at))->toDateTimeString() }}}</strong></small><br>
                                    @if ($account->active == 0 and Auth::user()->admin == 1)
                                        <div class="alert alert-warning"><p>This account has been marked as disabled. As an administrator, you may still connect to it. However, standard accounts cannot connect to this account.</p>
                                        </div>
                                    @elseif($account->active == 0 and Auth::user()->admin == 0)
                                        <div class="alert alert-warning">
                                            <p>This account has been marked as disabled. You cannot connect to this account while it is disabled.</p>
                                        </div>
                                    @endif
                                </div>
                                <div class="col-md-4">
                                    <div class="btn-gdroup">
                                        @if($account->active == 1)
                                            {{ Form::open(array('method' => 'POST', 'action' => 'App\ServiceAccountController@postChoose')) }}
                                            <input type="hidden" name="account" value="{{{ $account->tenant_id }}}">
                                            <input type="hidden" name="accountid" value="{{{ $account->id }}}">
                                            <button type="submit" class="btn btn-primary btn-block"><i class="fa fa-cloud"></i> Connect to '{{{ str_limit($account->account_name, 50) }}}'</button>
                                            {{ Form::close() }}
                                        @else
                                            @if(Auth::user()->admin == 1)
                                                {{ Form::open(array('method' => 'POST', 'action' => 'App\ServiceAccountController@postChoose')) }}
                                                <input type="hidden" name="account" value="{{{ $account->tenant_id }}}">
                                                <input type="hidden" name="accountid" value="{{{ $account->id }}}">
                                                <button type="submit" class="btn btn-danger-alt btn-block"><i class="fa fa-cloud"></i> Connect to this '{{{ str_limit($account->account_name, 50) }}}'</button>
                                                {{ Form::close() }}
                                            @else
                                                <button type="submit" class="btn btn-danger-alt btn-block" disabled="disabled"><i class="fa fa-cloud"></i> Account Currently Disabled</button>
                                            @endif
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            @endif
        </div>
    </div>
</div>