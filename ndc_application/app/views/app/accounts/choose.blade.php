@extends('layouts.master')

@section('pageTitle')
<i class="fa fa-cloud"></i> Choose a Service Account to connect to
@stop

@section('content')

<div class="row">
	
	<div class="col-md-12">
		<p class="mb20">If you have more than once service account associated with your user account, you must select the service account to use. To do this, select a service account from the list below.</p>

		@include('app.accounts.list_choose')
	</div>

</div>

@stop