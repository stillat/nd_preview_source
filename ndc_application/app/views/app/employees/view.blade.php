@extends('layouts.master')

@section('pageTitle')
{{ gender_icon($employee->gender) }} <strong>{{{ limit_code($employee->code) }}}</strong>: {{{ limit_name($employee->first_name.' '.$employee->last_name) }}}
@stop

@section('buttonBar')
<div class="btn-group pull-up-10">
	{{ HTML::modelNaviateToolbar('employees.show', $employee->previous(), 'employees.index', 'employees.show', $employee->next()) }}
	<a href="#" class="btn btn-white"><i class="fa fa-coffee inline-icon"></i> New Work Entry</a>
</div>
@stop

@section('content')
<div class="row">
	<div class="col-sm-3">
		<img src="{{ app_url().'/'.$employee->large_image }}" class="thumbnail img-responsive" alt="">
	</div>
	<div class="col-sm-9">
		<div class="profile-header">
			<h2 class="profile-name"><strong>{{{ limit_code($employee->code) }}}</strong>: {{{ limit_name($employee->first_name.' '.$employee->last_name) }}}</h2>
			<div class="profile-location">
				<strong><i class="fa fa-barcode"></i> Code:</strong> {{{ limit_code($employee->code) }}}
			</div>
			<div class="profile-location">
				<strong>{{ gender_icon($employee->gender) }} Name:</strong> {{{ limit_description($employee->middle_name.' '.$employee->last_name.', '.$employee->first_name) }}}
			</div>
			<div class="profile-location">
				<strong><i class="fa fa-calendar"></i> Hire Date:</strong> {{{ if_null_then(mysql_date_to_client($employee->hire_date), 'Not available') }}} <strong>- <i class="fa fa-calendar"></i> Termination Date:</strong> {{{ if_null_then(mysql_date_to_client($employee->termination_date), 'Not available') }}}
			</div>
			<div class="mb20"></div>
			<a href="{{{ action('App\Work\WorkLogsController@create', ['loadEmployee' => $employee->id]) }}}" class="btn btn-success mr5"><i class="fa fa-coffee"></i> Add Work Record</a>
			<a href="{{ route('employees.edit', array($employee->id)) }}" class="btn btn-white"><i class="fa fa-edit"></i> Update employee</a>
		</div>

		<div class="panel panel-success">
			<div class="panel-heading">
				<h3 class="panel-title"><i class="fa fa-dollar"></i> Financial Details</h3>
			</div>
			<div class="table-responsive">
				<table class="table table-hover">
					<tr>
						<td>Hourly Wage</td>
						<td>{{{ nd_number_format($employee->wage) }}}</td>
					</tr>
					<tr>
						<td>Hourly Fringe Benefits</td>
						<td>{{{ nd_number_format($employee->wage_benefit) }}}</td>
					</tr>
					@if(show_overtime())
					<tr>
						<td>Overtime Wage</td>
						<td>{{{ nd_number_format($employee->overtime) }}}</td>
					</tr>
					<tr>
						<td>Hourly Fringe Benefits</td>
						<td>{{{ nd_number_format($employee->overtime_benefit) }}}</td>
					</tr>
					@endif
				</table>
			</div>
		</div>
	</div>
</div>
<!--
<div class="row">
	<div class="col-sm-12">
		<ul class="nav nav-tabs">
			<li class="active"><a href="#home" data-toggle="tab"><strong><i class="fa fa-coffee"></i> Recent Work Logs</strong></a></li>
			<li class=""><a href="#about" data-toggle="tab"><strong>Recent Activities</strong></a></li>	
		</ul>
		<div class="tab-content mb30">
			<div class="tab-pane active" id="home">
				<div class="table-responsive">
					<table class="table mb30">
						<thead>
							<tr>
								<th>#</th>
								<th>First Name</th>
								<th>Last Name</th>
								<th>Username</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>1</td>
								<td>Mark</td>
								<td>Otto</td>
								<td>@mdo</td>
							</tr>
							<tr>
								<td>2</td>
								<td>Jacob</td>
								<td>Thornton</td>
								<td>@fat</td>
							</tr>
							<tr>
								<td>3</td>
								<td>Larry</td>
								<td>the Bird</td>
								<td>@twitter</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
			<div class="tab-pane" id="about">
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitat</p>
				<p>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias </p>
			</div>
		</div>
	</div>
</div>
-->
@stop