<div class="table-responsive mb30">
    <table class="table table-hover table-striped mb30 table-fixed">
        <thead>
        <tr>
            @if(!isset($hideFeatures))
                <th class="hidden-print" style="width:46px;">
                    <div class="ckbox ckbox-primary">
                        <input type="checkbox" id="batchToggleRecords">
                        <label for="batchToggleRecords"></label>
                    </div>
                </th>
            @endif
            <th style="width:50px;border-bottom: 0px;"></th>
            <th class="wd-100">Code</th>
            <th>Employee</th>
            <th class="wd-150 text-right">Hourly Wage</th>
            <th class="wd-150 text-right">Hourly Fringe</th>
            @if(show_overtime())
                <th class="wd-150 text-right">Overtime Wage</th>
                <th class="wd-150 text-right">Overtime Fringe</th>
            @endif
            <th class="wd-100 hidden-print text-right">
                <a href="{{{ action('App\Search\SortingController@getEmployees') }}}" data-toggle="modal"
                   data-target="#modalHousing"><i class="fa fa-sort"></i>
                    <small>Sorting</small>
                </a>
            </th>
        </tr>
        </thead>
        <tbody>
        @foreach($employees as $employee)
            @if ($employee->deleted_at == null)
                <tr>
                    @if(!isset($hideFeatures))
                        <td class="hidden-print">
                            <div class="ckbox ckbox-primary">
                                <input type="checkbox" class="rm_multi" id="remCheck{{{ $employee->id }}}"
                                       data-record="{{{ $employee->id }}}">
                                <label for="remCheck{{{ $employee->id }}}"></label>
                            </div>
                        </td>
                    @endif
                    <td class="employee-photo-holder"><img alt="" src="{{ app_url().'/'.$employee->profile_image }}"></td>
                    <td>
                        <a href="{{ route('employees.show', array($employee->id)) }}"><strong>{{{ limit_code($employee->code) }}}</strong></a>
                    </td>
                    <td>
                        <a href="{{ route('employees.show', array($employee->id)) }}">
                            {{{ limit_description($employee->first_name.' '.$employee->last_name) }}}
                        </a>
                    </td>
                    <td class="text-right"><a href="{{ route('employees.show', array($employee->id)) }}">{{{ nd_number_format($employee->wage) }}}</a></td>
                    <td class="text-right"><a href="{{ route('employees.show', array($employee->id)) }}">{{{ nd_number_format($employee->wage_benefit) }}}</a></td>
                    @if(show_overtime())
                        <td class="text-right"><a href="{{ route('employees.show', array($employee->id)) }}">{{{ nd_number_format($employee->overtime) }}}</a></td>
                        <td class="text-right"><a href="{{ route('employees.show', array($employee->id)) }}">{{{ nd_number_format($employee->overtime_benefit) }}}</a></td>
                    @endif
                    <td class="table-action td-ignore">
                        <a href="{{ route('employees.edit', array($employee->id)) }}"><i class="fa fa-pencil"></i></a>
                        <a class="cursor-pointer delete-row" data-rcontext="{{{ $employee->id }}}"><i class="fa fa-trash-o"></i></a>
                    </td>
                </tr>
            @else
                <tr>
                    @if(!isset($hideFeatures))
                        <td class="hidden-print">
                            <div class="ckbox ckbox-primary">
                                <input type="checkbox" class="rm_multi" id="remCheck{{{ $employee->id }}}"
                                       data-record="{{{ $employee->id }}}">
                                <label for="remCheck{{{ $employee->id }}}"></label>
                            </div>
                        </td>
                    @endif
                    <td class="employee-photo-holder"><img alt="" src="{{ app_url().'/'.$employee->profile_image }}"></td>
                    <td>
                        <strong>{{{ limit_code($employee->code) }}}</strong>
                    </td>
                    <td>{{{ limit_description($employee->first_name.' '.$employee->last_name) }}}</td>
                    <td class="text-right">{{{ nd_number_format($employee->wage) }}}</td>
                    <td class="text-right">{{{ nd_number_format($employee->wage_benefit) }}}</td>
                    @if(show_overtime())
                        <td class="text-right">{{{ nd_number_format($employee->overtime) }}}</td>
                        <td class="text-right">{{{ nd_number_format($employee->overtime_benefit) }}}</td>
                    @endif
                    <td class="table-action td-ignore">
                        <a href="{{ action('App\UnTrashController@getEmployees') }}?d={{ $employee->id }}" class="tooltips" data-toggle="tooltip" data-placement="left" title="Restoring will un-delete the employee">Restore</a>
                    </td>
                </tr>
            @endif
        @endforeach
        </tbody>
    </table>
</div>
