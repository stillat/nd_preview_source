<div class="table-responsive">
	<table class="table table-hover inspector-holder">
		<thead>
			<tr>
				<th>Code</th>
				<th>Name</th>
				<th class="text-right">Regular Wage</th>
				<th class="text-right">Regular Fringe Benefits</th>
				@if(show_overtime())
				<th class="text-right">Overtime Wage</th>
				<th class="text-right">Overtime Fringe Benefits</th>
				@endif
			</tr>
		</thead>
		<tbody>
			@foreach($employees as $employeeID => $employee)
			<tr>
				<td><strong>{{{ $employee->code }}}</strong></td>
				<td>{{{ $employee->first_name.' '.$employee->last_name }}}</td>
				<td class="text-right">{{{ nd_number_format($employee->wage) }}} <a class="cursor-pointer" data-toggle="replaceable" data-replacefrom="{{{ nd_number_format($employee->wage) }}}" data-copyto="[name='wage']"><i class="fa fa-copy"></i> <span class="sr-only">Copy value</span></a></td>
				<td class="text-right">{{{ nd_number_format($employee->wage_benefit) }}} <a class="cursor-pointer" data-toggle="replaceable" data-replacefrom="{{{ nd_number_format($employee->wage_benefit) }}}" data-copyto="[name='wage_benefit']"><i class="fa fa-copy"></i> <span class="sr-only">Copy value</span></a></td></td>
				@if(show_overtime())
				<td class="text-right">{{{ nd_number_format($employee->overtime) }}} <a class="cursor-pointer" data-toggle="replaceable" data-replacefrom="{{{ nd_number_format($employee->overtime) }}}" data-copyto="[name='overtime']"><i class="fa fa-copy"></i> <span class="sr-only">Copy value</span></a></td>
				<td class="text-right">{{{ nd_number_format($employee->overtime_benefit) }}} <a class="cursor-pointer" data-toggle="replaceable" data-replacefrom="{{{ nd_number_format($employee->overtime_benefit) }}}" data-copyto="[name='overtime_benefit']"><i class="fa fa-copy"></i> <span class="sr-only">Copy value</span></a></td>
				@endif
			</tr>
			@endforeach
		</tbody>
	</table>
</div>

<a href="{{ action('App\Inspector\InspectorController@getClear', array('employees')) }}" data-rhistory="hide" data-target="#ndLoadExternal" data-rcontainer="#ajaxContentPanel" data-toggle="remote">Clear this list</a>