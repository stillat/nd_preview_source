@extends('layouts.master')

@section('pageTitle')
<i class="fa fa-pencil"></i> <strong>{{{ limit_code($employee->code) }}}</strong> - {{{ limit_name($employee->first_name.' '.$employee->last_name) }}}
@stop

@section('buttonBar')
<div class="btn-group pull-up-10">
	{{ HTML::modelNaviateToolbar('employees.edit', $employee->previous(), 'employees.index', 'employees.edit', $employee->next()) }}
</div>
<div class="btn-group pull-up-10">
	<a href="{{ Redirector::getRoute(route('employees.index')) }}" class="btn btn-danger ndc_form_leave">Cancel</a>
</div>
@stop

@section('content')

<div class="row">
	<div class="col-md-12">
		{{ Form::open(array('route' => array('employees.update', $employee->id), 'files' => true, 'method' => 'put', 'class' => 'form-horizontal')) }}
		<div class="panel-primary panel-alt">
			<div class="panel-body">
				<fieldset class="mb20">
					<legend>Personal Information</legend>

					<div class="form-group">
						<label class="col-sm-2 control-label" for="code">Code:</label>
						<div class="col-sm-2">
							<div class="input-group">
								<span class="input-group-addon"><i class="glyphicon glyphicon-barcode"></i></span>
								<input type="text" id="code" name="code" class="form-control mousetrap text-right ndc_form_home_element {{{ user_form_size() }}}" value="{{{ Input::old('code', $employee->code) }}}" autofocus autocomplete="off">
							</div>
							{{ Form::errorMsg('code') }}
						</div>
					</div>

					<div class="form-group is-required">
						<label class="col-sm-2 control-label" for="first_name">First Name:</label>
						<div class="col-sm-8">
							<input type="text" id="first_name" name="first_name" class="form-control mousetrap {{{ user_form_size() }}}" value="{{{ Input::old('first_name', $employee->first_name) }}}">
							{{ Form::errorMsg('first_name') }}
						</div>
					</div>

					<div class="form-group">
						<label class="col-sm-2 control-label" for="middle_name">Middle Name:</label>
						<div class="col-sm-8">
							<input type="text" id="middle_name" name="middle_name" class="form-control mousetrap {{{ user_form_size() }}}" value="{{{ Input::old('middle_name', $employee->middle_name) }}}">
							{{ Form::errorMsg('middle_name') }}
						</div>
					</div>

					<div class="form-group is-required">
						<label class="col-sm-2 control-label" for="last_name">Last Name:</label>
						<div class="col-sm-8">
							<input type="text" id="last_name" name="last_name" class="form-control mousetrap {{{ user_form_size() }}}" value="{{{ Input::old('last_name', $employee->last_name) }}}">
							{{ Form::errorMsg('last_name') }}
						</div>
					</div>

					<div class="form-group">
						<label class="col-sm-2 control-label" for="gender">Gender:</label>
						<div class="col-sm-2">
							{{ Form::select('gender', $genders, Input::old('gender', $employee->gender), array('class' => 'form-control mousetrap '.user_form_size(), 'id' => 'gender')) }}
							{{ Form::errorMsg('gender') }}
						</div>
					</div>
				</fieldset>

				<fieldset class="mb20">
					<legend>Additional Information</legend>

					<div class="form-group">
						<label class="col-sm-2 control-label">Photo</label>
						<div class="col-sm-10">
							<div class="fileupload fileupload-new" data-provides="fileupload">
								<div class="input-append">
									<div class="uneditable-input">
										<i class="glyphicon glyphicon-picture fileupload-exists"></i>
										<span class="fileupload-preview"></span>
									</div>
									<span class="btn btn-default btn-file">
										<span class="fileupload-new">Select photo</span>
										<span class="fileupload-exists">Change</span>
										<input type="file" name="photo" />
									</span>
									<a href="#" class="btn btn-default fileupload-exists" data-dismiss="fileupload">Remove</a>
								</div>
							</div>

							{{ Form::errorMsg('photo') }}
						</div>
					</div>

					<div class="form-group">
						<label class="col-sm-2 control-label" for="hire_date">Hire date:</label>
						<div class="col-sm-2">
							<div class="input-group">
								<span class="input-group-addon"><i class="fa fa-calendar-o"></i></span>
								<input type="text" class="form-control simpleDate mousetrap" id="hire_date" name="hire_date" value="{{{ mysql_date_to_client(Input::old('hire_date', $employee->hire_date)) }}}">
								{{ Form::errorMsg('wage') }}
							</div>
						</div>

						<label class="col-sm-2 control-label" for="termination_date">Termination date:</label>
						<div class="col-sm-2">
							<div class="input-group">
								<span class="input-group-addon"><i class="fa fa-calendar-o"></i></span>
								<input type="text" class="form-control simpleDate mousetrap" id="termination_date" name="termination_date" value="{{{ mysql_date_to_client(Input::old('termination_date', $employee->termination_date)) }}}">
							</div>
							{{ Form::errorMsg('wage_benefit') }}
						</div>
					</div>

				</fieldset>

				<fieldset>
					<legend>Employee Wage Information</legend>

					<div class="form-group is-required">
						<label class="col-sm-2 control-label" for="wage">Regular Wage:</label>
						<div class="col-sm-2">
							<div class="input-group">
								<span class="input-group-addon">$</span>
								<input type="number" min="0" step="any" id="wage" name="wage" class="form-control text-right mousetrap {{{ user_form_size() }}}" value="{{{ nd_number_format(Input::old('wage', $employee->wage)) }}}">
							</div>
							{{ Form::errorMsg('wage') }}
						</div>

						<label class="col-sm-2 control-label" for="wage_benefit">Regular Fringe Benefits:</label>
						<div class="col-sm-2">
							<div class="input-group">
								<span class="input-group-addon">$</span>
								<input type="number" min="0" step="any" id="wage_benefit" name="wage_benefit" class="form-control text-right mousetrap ndc_form_trail_element {{{ user_form_size() }}}" value="{{{ nd_number_format(Input::old('wage_benefit', $employee->wage_benefit)) }}}">
							</div>
							{{ Form::errorMsg('wage_benefit') }}
						</div>
					</div>
					@if(show_overtime())
					<div class="form-group is-required">
						<label class="col-sm-2 control-label" for="overtime">Overtime Wage:</label>
						<div class="col-sm-2">
							<div class="input-group">
								<span class="input-group-addon">$</span>
								<input type="number" min="0" step="any" id="overtime" name="overtime" class="form-control text-right mousetrap {{{ user_form_size() }}}" value="{{{ nd_number_format(Input::old('overtime', $employee->overtime)) }}}">
							</div>
							{{ Form::errorMsg('overtime') }}
						</div>

						<label class="col-sm-2 control-label" for="overtime_benefit">Overtime Fringe Benefits:</label>
						<div class="col-sm-2">
							<div class="input-group">
								<span class="input-group-addon">$</span>
								<input type="number" min="0" step="any" id="overtime_benefit" name="overtime_benefit" class="form-control text-right mousetrap ndc_form_trail_element {{{ user_form_size() }}}" value="{{{ nd_number_format(Input::old('overtime_benefit', $employee->overtime_benefit)) }}}">
							</div>
							{{ Form::errorMsg('overtime_benefit') }}
						</div>
					</div>
					@else
					<input type="hidden" name="overtime" value="0.00">
					<input type="hidden" name="overtime_benefit" value="0.00">
					@endif
				</fieldset>

			</div>
			<div class="panel-footer col-sm-offset-2">
				<div class="btn-group mr5">
					<button type="submit" id="ndc_form_defaultaction" value="return" name="submit" class="btn btn-info">Update this employee</button>
				</div>
				<div class="btn-group mr5">
					<button type="reset" id="ndc_form_reset" class="btn btn-default">Reset</button>
				</div>
			</div>
		</div>
		{{ Form::close() }}
	</div>
</div>

@stop