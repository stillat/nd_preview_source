@extends('layouts.master')

@section('pageTitle')
<i class="fa fa-plus"></i> You are adding a new employee
@stop

@section('buttonBar')
<div class="btn-group pull-up-10">
	<a href="{{ Redirector::getRoute(route('employees.index')) }}" class="btn btn-danger ndc_form_leave">Cancel</a>
</div>
@stop

@section('content')

<div class="row">

	<div class="col-md-12">
		{{ Form::open(array('route' => 'employees.store', 'method' => 'post', 'class' => 'form-horizontal')) }}
		<div class="panel-primary panel-alt">
			<div class="panel-body">
				<fieldset class="mb20">
					<legend>Personal Information</legend>

					<div class="form-group">
						<label class="col-sm-2 control-label" for="code">Code:</label>
						<div class="col-sm-2">
							<div class="input-group">
								<span class="input-group-addon"><i class="glyphicon glyphicon-barcode"></i></span>
								<input type="text" id="code" name="code" class="form-control text-right mousetrap ndc_form_home_element {{{ user_form_size() }}}" value="{{{ Input::old('code', '') }}}" autofocus autocomplete="off">
							</div>
							{{ Form::errorMsg('code') }}
						</div>
					</div>

					<div class="form-group is-required">
						<label class="col-sm-2 control-label" for="first_name">First Name:</label>
						<div class="col-sm-8">
							<input type="text" id="first_name" name="first_name" data-resurrect="test" class="form-control mousetrap {{{ user_form_size() }}}" value="{{{ Input::old('first_name', '') }}}">
							{{ Form::errorMsg('first_name') }}
						</div>
					</div>

					<div class="form-group">
						<label class="col-sm-2 control-label" for="middle_name">Middle Name:</label>
						<div class="col-sm-8">
							<input type="text" id="middle_name" name="middle_name" class="form-control mousetrap {{{ user_form_size() }}}" value="{{{ Input::old('middle_name', '') }}}">
							{{ Form::errorMsg('middle_name') }}
						</div>
					</div>

					<div class="form-group is-required">
						<label class="col-sm-2 control-label" for="last_name">Last Name:</label>
						<div class="col-sm-8">
							<input type="text" id="last_name" name="last_name" class="form-control mousetrap {{{ user_form_size() }}}" value="{{{ Input::old('last_name', '') }}}">
							{{ Form::errorMsg('last_name') }}
						</div>
					</div>

					<div class="form-group">
						<label class="col-sm-2 control-label" for="gender">Gender:</label>
						<div class="col-sm-2">
							{{ Form::select('gender', $genders, Input::old('gender', 0), array('data-resurrect' => '1', 'id' => 'gender','class' => 'form-control mousetrap '.user_form_size())) }}
							{{ Form::errorMsg('gender') }}
						</div>
					</div>
				</fieldset>

				<fieldset>
					<legend>Employee Wage Information</legend>

					<div class="form-group is-required">
						<label class="col-sm-2 control-label" for="wage">Regular Wage:</label>
						<div class="col-sm-2">
							<div class="input-group">
								<span class="input-group-addon">$</span>
								<input type="number" min="0" step="any" id="wage" name="wage" class="form-control text-right {{{ user_form_size() }}}" value="{{{ nd_number_format(Input::old('wage', '0.00')) }}}">
							</div>
							{{ Form::errorMsg('wage') }}
						</div>

						<label class="col-sm-2 control-label" for="wage_benefit">Regular Fringe Benefits:</label>
						<div class="col-sm-2">
							<div class="input-group">
								<span class="input-group-addon">$</span>
								<input type="number" min="0" step="any" id="wage_benefit" name="wage_benefit" class="form-control text-right mousetrap ndc_form_trail_element {{{ user_form_size() }}}" value="{{{ nd_number_format(Input::old('wage_benefit', '0.00')) }}}">
							</div>
							{{ Form::errorMsg('wage_benefit') }}
						</div>
					</div>
					@if(show_overtime())
					<div class="form-group is-required">
						<label class="col-sm-2 control-label" for="overtime">Overtime Wage:</label>
						<div class="col-sm-2">
							<div class="input-group">
								<span class="input-group-addon">$</span>
								<input type="number" min="0" step="any" id="overtime" name="overtime" class="form-control text-right mousetrap {{{ user_form_size() }}}" value="{{{ nd_number_format(Input::old('overtime', '0.00')) }}}">
							</div>
							{{ Form::errorMsg('overtime') }}
						</div>

						<label class="col-sm-2 control-label" for="overtime_benefit">Overtime Fringe Benefits:</label>
						<div class="col-sm-2">
							<div class="input-group">
								<span class="input-group-addon">$</span>
								<input type="number" min="0" step="any" id="overtime_benefit" name="overtime_benefit" class="form-control text-right mousetrap ndc_form_trail_element {{{ user_form_size() }}}" value="{{{ nd_number_format(Input::old('overtime_benefit', '0.00')) }}}">
							</div>
							{{ Form::errorMsg('overtime_benefit') }}
						</div>
					</div>
					@else
					<input type="hidden" name="overtime" value="0.00">
					<input type="hidden" name="overtime_benefit" value="0.00">
					@endif
				</fieldset>

			</div>
			<div class="panel-footer col-sm-offset-2">
				<div class="btn-group mr5">
					<button type="submit" id="ndc_form_defaultaction" value="addNew" name="submit" class="btn btn-primary">Save and add another employee</button>
					<button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
						<span class="caret"></span>
						<span class="sr-only">Toggle Dropdown</span>
					</button>
					<ul class="dropdown-menu" role="menu">
						<li><button type="submit" id="ndc_form_secondaryaction" value="return" name="submit" class="btn btn-white btn-md">Save and add return to previous action</button></li>
					</ul>
				</div>

				<div class="btn-group mr5">
					<button type="reset" id="ndc_form_reset" class="btn btn-default">Reset</button>
				</div>
			</div>
		</div>
		{{ Form::close() }}
	</div>

</div>

@stop