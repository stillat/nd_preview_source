@extends('layouts.master')

@section('pageTitle')
<i class="fa fa-user"></i> Employees
@if(Input::get('search') == 1 && $foundResults)
    <span>Search Results</span>
@endif
@stop

@section('buttonBar')
    @if(Input::get('search') == 1 && $foundResults)
        <div class="btn-group pull-up-10">
            <a href="{{ action('App\Employees\EmployeeController@index') }}" class="btn btn-white"><i class="fa fa-th-list inline-icon"></i></a>
        </div>
    @endif
    <div class="btn-group pull-up-10">
        <a href="{{ action('App\Search\SearchController@getEmployeeSearchDialog') }}" data-toggle="modal" data-target="#modalHousing" class="btn btn-white"><i class="fa fa-search inline-icon"></i> Search Employees</a>
    @include('partials.generic_export', ['export_data' => 'res_employees', 'title' => 'employees', 'margin' => '85px'])
    <a href="{{ action('App\Employees\EmployeeController@create') }}" class="btn btn-white nd-add"><i class="fa fa-plus inline-icon"></i> New Employee</a>
</div>
@stop

@section('content')

    <div class="row hidden-print">
        <div class="col-md-12">
            {{ $employees->appends(array_except(Input::query(), Paginator::getPageName()))->links() }}
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            @include('app.employees.partials.table')
        </div>
    </div>

    <div class="row hidden-print">
        <div class="col-md-12">
            {{ $employees->appends(array_except(Input::query(), Paginator::getPageName()))->links() }}
        </div>
        @include('layouts.list_management')
    </div>


@include('layouts.dialogs.remove', array('title' => 'Remove Employee Resource', 'action' => 'App\Employees\EmployeeController@destroy'))
@stop

@section('additionalScripts')
    <script>var NDC_LM = 'employee';</script>
    <script src="{{{ app_url() }}}/assets/js/list_management.min.js{{{ UI::getBuster() }}}" defer></script>
@stop