@extends('layouts.master')

@section('pageTitle')
    <i class="fa fa-folder-open"></i> Projects
    @if(Input::get('search') == 1 && $foundResults)
        <span>Search Results</span>
    @endif
@stop

@section('buttonBar')
    @if(Input::get('search') == 1 && $foundResults)
        <div class="btn-group pull-up-10">
            <a href="{{ action('App\Projects\ProjectsController@index') }}" class="btn btn-white"><i
                        class="fa fa-th-list inline-icon"></i></a>
        </div>
    @endif
    <div class="btn-group pull-up-10">
        <a href="{{ action('App\Search\SearchController@getProjectsSearchDialog') }}" data-toggle="modal"
           data-target="#modalHousing" class="btn btn-white"><i class="fa fa-search inline-icon"></i> Search
            Projects</a>
        @include('partials.generic_export', ['export_data' => 'org_projects', 'title' => 'projects', 'margin' => '69px'])
        <a href="{{ action('App\Projects\ProjectsController@create') }}" class="btn btn-white nd-add"><i
                    class="fa fa-plus inline-icon"></i> New Project</a>
    </div>
@stop

@section('content')


    <div class="row hidden-print">
        <div class="col-md-12">
            {{ $projects->appends(array_except(Input::query(), Paginator::getPageName()))->links() }}
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            @include('app.projects.partials.table')
        </div>
    </div>

    <div class="row hidden-print">
        <div class="col-md-12">
            {{ $projects->appends(array_except(Input::query(), Paginator::getPageName()))->links() }}
        </div>
        @include('layouts.list_management')
    </div>

    @include('layouts.dialogs.remove', array('title' => 'Remove Project', 'action' => 'App\Projects\ProjectsController@destroy'))
@stop

@section('additionalScripts')
    <script>var NDC_LM = 'project';</script>
    <script src="{{{ app_url() }}}/assets/js/list_management.min.js{{{ UI::getBuster() }}}" defer></script>
@stop