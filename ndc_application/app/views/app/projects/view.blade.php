@extends('layouts.master')

@section('pageTitle')
<i class="fa fa-folder-open"></i> {{{ limit_code($project->code) }}} - <small>{{{ limit_name($project->name)  }}}</small>
@stop

@section('buttonBar')
<div class="btn-group n-mt-5">
    {{ HTML::modelNaviateToolbar('projects.show', $project->previous(), 'projects.index', 'projects.show', $project->next()) }}
</div>
<div class="btn-group n-mt-5">
    <a href="{{ route('projects.edit', array($project->id)) }}" class="btn btn-white"><i class="fa fa-edit inline-icon"></i> Update project</a>
</div>
@stop

@section('content')
<div class="row">
    <div class="col-md-12">
    @include('app.projects.partials.detail_table')
    </div>
</div>
@stop