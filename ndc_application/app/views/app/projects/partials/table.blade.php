<div class="table-responsive">
    <table class="table table-hover table-striped mb30 table-fixed">
        <thead>
        <tr>
            @if(!isset($hideFeatures))
                <th class="hidden-print" style="width:46px;">
                    <div class="ckbox ckbox-primary">
                        <input type="checkbox" id="batchToggleRecords">
                        <label for="batchToggleRecords"></label>
                    </div>
                </th>
            @endif
            <th class="wd-200">Code</th>
            <th class="wd-200">Name</th>
            <th>Description</th>
            <th>FEMA Project</th>
            <th class="wd-100 hidden-print text-right">
                <a href="{{{ action('App\Search\SortingController@getProjects') }}}" data-toggle="modal"
                   data-target="#modalHousing"><i class="fa fa-sort"></i>
                    <small>Sorting</small>
                </a>
            </th>
        </tr>
        </thead>
        <tbody>
        @foreach($projects as $project)
            @if ($project->deleted_at == null)
                <tr>
                    @if(!isset($hideFeatures))
                        <td class="hidden-print">
                            <div class="ckbox ckbox-primary">
                                <input type="checkbox" class="rm_multi" id="remCheck{{{ $project->id }}}"
                                       data-record="{{{ $project->id }}}">
                                <label for="remCheck{{{ $project->id }}}"></label>
                            </div>
                        </td>
                    @endif
                    <td>
                        <a href="{{ route('projects.show', array($project->id)) }}"><strong>{{{ limit_code($project->code) }}}</strong></a>
                    </td>
                    <td>
                        <a href="{{ route('projects.show', array($project->id)) }}">{{{ limit_name($project->name) }}}</a>
                    </td>
                    <td>
                        <a href="{{ route('projects.show', array($project->id)) }}">{{{ if_null_then_na(limit_description($project->description)) }}}</a>
                    </td>
                    <td>
                        <a href="{{ route('projects.show', array($project->id)) }}">{{ bool_to_icon($project->fema_project) }}</a>
                    </td>
                    <td class="table-action td-ignore">
                        <a href="{{ route('projects.edit', array($project->id)) }}"><i class="fa fa-pencil"></i></a>
                        <a class="cursor-pointer delete-row" data-rcontext="{{{ $project->id }}}"><i class="fa fa-trash-o"></i></a>
                    </td>
                </tr>
            @else
                <tr>
                    @if(!isset($hideFeatures))
                        <td class="hidden-print">
                            <div class="ckbox ckbox-primary">
                                <input type="checkbox" class="rm_multi" id="remCheck{{{ $project->id }}}"
                                       data-record="{{{ $project->id }}}">
                                <label for="remCheck{{{ $project->id }}}"></label>
                            </div>
                        </td>
                    @endif
                    <td>
                        <strong>{{{ limit_code($project->code) }}}</strong>
                    </td>
                    <td>
                        {{{ limit_name($project->name) }}}
                    </td>
                    <td>
                        {{{ if_null_then_na(limit_description($project->description)) }}}
                    </td>
                    <td>
                        {{ bool_to_icon($project->fema_project) }}
                    </td>
                    <td class="table-action td-ignore">
                        <a href="{{ action('App\UnTrashController@getProjects') }}?d={{ $project->id }}" class="tooltips" data-toggle="tooltip" data-placement="left" title="Restoring will un-delete the project">Restore</a>
                    </td>
                </tr>
            @endif
        @endforeach
        </tbody>
    </table>
</div>