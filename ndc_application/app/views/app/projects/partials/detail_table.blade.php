<div class="table-responsive">
    <table class="table table-hover table-striped mb30 table-fixed">
        <thead>
            <tr>
                <th class="wd-200">Code</th>
                <th class="wd-200">Name</th>
                <th>Description</th>
                <th>FEMA Project</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>
                    <strong>{{{ limit_code($project->code) }}}</strong>
                </td>
                <td>
                    {{{ limit_name($project->name) }}}
                </td>
                <td>
                    {{{ limit_description(if_null_then_na($project->description)) }}}
                </td>
                <td>
                    {{ bool_to_icon($project->fema_project) }}
                </td>
            </tr>
        </tbody>
    </table>
</div>