@extends('layouts.master')

@section('pageTitle')
<i class="fa ndc-bicycle"></i> {{{ limit_code($activity->code) }}} - <small>{{{ limit_name($activity->name) }}}</small>
@stop

@section('buttonBar')
<div class="btn-group pull-up-10">
    {{ HTML::modelNaviateToolbar('activities.show', $activity->previous(), 'activities.index', 'activities.show', $activity->next()) }}
</div>
<div class="btn-group pull-up-10">
    <a href="{{ route('activities.edit', array($activity->id)) }}" class="btn btn-white"><i class="fa fa-edit inline-icon"></i> Update activity</a>
</div>
@stop

@section('content')
<div class="row">
    <div class="col-md-12">
    @include('app.activities.partials.detail_table')
    </div>
</div>
@stop