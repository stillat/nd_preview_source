<div class="table-responsive">
	<table class="table table-hover inspector-holder">
		<thead>
			<tr>
				<th>Code</th>
				<th>Name</th>
				<th>Description</th>
			</tr>
		</thead>
		<tbody>
			@foreach($activities as $activityID => $activity)
			<tr>
				<td><strong>{{{ $activity->code }}}</strong></td>
				<td>{{{ $activity->name }}} <a class="cursor-pointer" data-toggle="replaceable" data-replacefrom="{{{ $activity->name }}}" data-copyto="[name='name']"><i class="fa fa-copy"></i> <span class="sr-only">Copy value</span></a></td>
				<td>{{{ $activity->description }}}
					@if (strlen($activity->description) > 0)
					<a class="cursor-pointer" data-toggle="replaceable" data-replacefrom="{{{ $activity->description }}}" data-copyto="[name='description']"><i class="fa fa-copy"></i> <span class="sr-only">Copy value</span></a>
					@endif
				</td>
			</tr>
			@endforeach
		</tbody>
	</table>
</div>

<a href="{{ action('App\Inspector\InspectorController@getClear', array('activities')) }}" data-rhistory="hide" data-target="#ndLoadExternal" data-rcontainer="#ajaxContentPanel" data-toggle="remote">Clear this list</a>