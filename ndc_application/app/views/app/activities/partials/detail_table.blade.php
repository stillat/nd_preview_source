<div class="table-responsive mb30">
    <table class="table table-hover table-fixed">
        <thead>
            <tr>
                <th class="wd-100">Code</th>
                <th>Name</th>
                <th>Description</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td><strong>{{{ limit_code($activity->code) }}}</strong></td>
                <td>{{{ limit_name($activity->name) }}}</td>
                <td>{{{ if_null_then_na(limit_description($activity->description)) }}}</td>
            </tr>
        </tbody>
    </table>
</div>