<div class="table-responsive mb30">
    <table class="table table-hover table-fixed">
        <thead>
        <tr>
            @if(!isset($hideFeatures))
                <th class="hidden-print" style="width:46px;">
                    <div class="ckbox ckbox-primary">
                        <input type="checkbox" id="batchToggleRecords">
                        <label for="batchToggleRecords"></label>
                    </div>
                </th>
            @endif
            <th class="wd-100">Code</th>
            <th>Name</th>
            <th>Description</th>
            <th class="wd-100 hidden-print text-right">
                <a href="{{{ action('App\Search\SortingController@getActivities') }}}" data-toggle="modal"
                   data-target="#modalHousing"><i class="fa fa-sort"></i>
                    <small>Sorting</small>
                </a>
            </th>
        </tr>
        </thead>
        <tbody>
        @foreach($activities as $activity)
            @if ($activity->deleted_at == null)
                <tr>
                    @if(!isset($hideFeatures))
                        <td class="hidden-print">
                            <div class="ckbox ckbox-primary">
                                <input type="checkbox" class="rm_multi" id="remCheck{{{ $activity->id }}}"
                                       data-record="{{{ $activity->id }}}">
                                <label for="remCheck{{{ $activity->id }}}"></label>
                            </div>
                        </td>
                    @endif
                    <td><a href="{{ route('activities.show', array($activity->id)) }}"><strong>{{{ limit_code($activity->code) }}}</strong></a></td>
                    <td><a href="{{ route('activities.show', array($activity->id)) }}">{{{ limit_name($activity->name) }}}</a></td>
                    <td><a href="{{ route('activities.show', array($activity->id)) }}">{{{ if_null_then_na(limit_description($activity->description)) }}}</a></td>
                    <td class="table-action td-ignore">
                        <a href="{{ route('activities.edit', array($activity->id)) }}"><i class="fa fa-pencil"></i></a>
                        <a class="cursor-pointer delete-row" data-rcontext="{{{ $activity->id }}}"><i class="fa fa-trash-o"></i></a>
                    </td>
                </tr>
            @else
                <tr>
                    @if(!isset($hideFeatures))
                        <td class="hidden-print">
                            <div class="ckbox ckbox-primary">
                                <input type="checkbox" class="rm_multi" id="remCheck{{{ $activity->id }}}"
                                       data-record="{{{ $activity->id }}}">
                                <label for="remCheck{{{ $activity->id }}}"></label>
                            </div>
                        </td>
                    @endif
                    <td><strong>{{{ limit_code($activity->code) }}}</strong></td>
                    <td>{{{ limit_name($activity->name) }}}</td>
                    <td>{{{ if_null_then_na(limit_description($activity->description)) }}}</td>
                    <td class="table-action td-ignore">
                        <a href="{{ action('App\UnTrashController@getActivities') }}?d={{ $activity->id }}" class="tooltips" data-toggle="tooltip" data-placement="left" title="Restoring will un-delete the activity">Restore</a>
                    </td>
                </tr>
            @endif
        @endforeach
        </tbody>
    </table>
</div>