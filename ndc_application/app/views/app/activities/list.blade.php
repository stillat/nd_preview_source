@extends('layouts.master')

@section('pageTitle')
    <i class="fa ndc-bicycle"></i> Activities
    @if(Input::get('search') == 1 && $foundResults)
        <span>Search Results</span>
    @endif
@stop

@section('buttonBar')
    @if(Input::get('search') == 1 && $foundResults)
        <div class="btn-group pull-up-10">
            <a href="{{ action('App\Employees\ActivityController@index') }}" class="btn btn-white"><i
                        class="fa fa-th-list inline-icon"></i></a>
        </div>
    @endif
    <div class="btn-group pull-up-10">
        <a href="{{ action('App\Search\SearchController@getActivitySearchDialog') }}" data-toggle="modal"
           data-target="#modalHousing" class="btn btn-white"><i class="fa fa-search inline-icon"></i> Search Activities</a>
        @include('partials.generic_export', ['export_data' => 'org_activity', 'title' => 'activities', 'margin' => '76px'])
        <a href="{{ action('App\Employees\ActivityController@create') }}" class="btn btn-white nd-add"><i
                    class="fa fa-plus inline-icon"></i> New Activity</a>
    </div>
@stop

@section('content')

    <div class="row hidden-print">

        <div class="col-md-12">{{ $activities->appends(array_except(Input::query(), Paginator::getPageName()))->links() }}</div>
    </div>

    <div class="row">
        <div class="col-md-12">
            @include('app.activities.partials.table')
        </div>
    </div>

    <div class="row hidden-print">
        <div class="col-md-12 ">
            {{ $activities->appends(array_except(Input::query(), Paginator::getPageName()))->links() }}
        </div>
        @include('layouts.list_management')
    </div>


    @include('layouts.dialogs.remove', array('title' => 'Remove Activity', 'action' => 'App\Employees\ActivityController@destroy'))
@stop

@section('additionalScripts')
    <script>var NDC_LM = 'activity';</script>
    <script src="{{{ app_url() }}}/assets/js/list_management.min.js{{{ UI::getBuster() }}}" defer></script>
@stop