@extends('layouts.master')

@section('pageTitle')
<i class="fa fa-plus"></i> You are adding a new department
@stop

@section('buttonBar')
<div class="btn-group pull-up-10">
	<a href="{{ Redirector::getRoute(route('departments.index')) }}" class="btn btn-danger ndc_form_leave">Cancel</a>
</div>
@stop

@section('content')

<div class="row">

	<div class="col-md-12">
		{{ Form::open(array('route' => 'departments.store', 'method' => 'post', 'class' => 'form-horizontal')) }}
		<div class="panel-primary panel-alt">
			<div class="panel-body">
				<div class="form-group">
					<label class="col-sm-2 control-label" for="code">Code:</label>
					<div class="col-sm-2">
						<div class="input-group">
							<span class="input-group-addon"><i class="glyphicon glyphicon-barcode"></i></span>
							<input type="text" id="code" name="code" class="form-control text-right ndc_form_home_element mousetrap {{{ user_form_size() }}}" value="{{{ Input::old('code', '') }}}" autofocus>
						</div>
						{{ Form::errorMsg('code') }}
					</div>
				</div>

				<div class="form-group is-required">
					<label class="col-sm-2 control-label" for="name">Name:</label>
					<div class="col-sm-8">
						<input type="text" id="name" name="name" class="form-control mousetrap {{{ user_form_size() }}}" value="{{{ Input::old('name', '') }}}">
						{{ Form::errorMsg('name') }}
					</div>
				</div>

				<div class="form-group is-required">
					<label class="col-sm-2 control-label" for="description">Description:</label>
					<div class="col-sm-8">
						<textarea id="description" name="description" class="form-control ndc_form_trail_element mousetrap {{{ user_form_size() }}}">{{{ Input::old('description', '') }}}</textarea>
						{{ Form::errorMsg('description') }}
					</div>
				</div>
			</div>
			<div class="panel-footer col-sm-offset-2">
				<div class="btn-group mr5">
					<button type="submit" id="ndc_form_defaultaction" value="addNew" name="submit" class="btn btn-primary">Save and add another department</button>
					<button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
						<span class="caret"></span>
						<span class="sr-only">Toggle Dropdown</span>
					</button>
					<ul class="dropdown-menu" role="menu">
						<li><button type="submit" id="ndc_form_secondaryaction" value="return" name="submit" class="btn btn-white btn-md">Save and return to previous action</button></li>
					</ul>
				</div>

				<div class="btn-group mr5">
					<button type="reset" id="ndc_form_reset" class="btn btn-default">Reset</button>
				</div>
			</div>
		</div>
		{{ Form::close() }}
	</div>

</div>

@stop