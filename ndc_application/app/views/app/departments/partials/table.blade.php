<div class="table-responsive">
    <table class="table table-hover table-striped mb30 table-fixed">
        <thead>
        <tr>
            @if(!isset($hideFeatures))
                <th class="hidden-print" style="width:46px;">
                    <div class="ckbox ckbox-primary">
                        <input type="checkbox" id="batchToggleRecords">
                        <label for="batchToggleRecords"></label>
                    </div>
                </th>
            @endif
            <th class="wd-200">Code</th>
            <th class="wd-200">Name</th>
            <th>Description</th>
            <th class="wd-100 hidden-print text-right">
                <a href="{{{ action('App\Search\SortingController@getDepartments') }}}" data-toggle="modal"
                   data-target="#modalHousing"><i class="fa fa-sort"></i>
                    <small>Sorting</small>
                </a>
            </th>
        </tr>
        </thead>
        <tbody>
        @foreach($departments as $department)
            @if ($department->deleted_at == null)
                <tr>
                    @if(!isset($hideFeatures))
                        <td class="hidden-print">
                            <div class="ckbox ckbox-primary">
                                <input type="checkbox" class="rm_multi" id="remCheck{{{ $department->id }}}"
                                       data-record="{{{ $department->id }}}">
                                <label for="remCheck{{{ $department->id }}}"></label>
                            </div>
                        </td>
                    @endif
                    <td>
                        <a href="{{ route('departments.show', array($department->id)) }}"><strong>{{{ limit_code($department->code) }}}</strong></a>
                    </td>
                    <td>
                        <a href="{{ route('departments.show', array($department->id)) }}">{{{ limit_name($department->name) }}}</a>
                    </td>
                    <td>
                        <a href="{{ route('departments.show', array($department->id)) }}">{{{ if_null_then_na(limit_description($department->description)) }}}</a>
                    </td>
                    <td class="table-action td-ignore">
                        <a href="{{ route('departments.edit', array($department->id)) }}"><i class="fa fa-pencil"></i></a>
                        <a class="cursor-pointer delete-row" data-rcontext="{{{ $department->id }}}"><i class="fa fa-trash-o"></i></a>
                    </td>
                </tr>
            @else
                <tr>
                    @if(!isset($hideFeatures))
                        <td class="hidden-print">
                            <div class="ckbox ckbox-primary">
                                <input type="checkbox" class="rm_multi" id="remCheck{{{ $department->id }}}"
                                       data-record="{{{ $department->id }}}">
                                <label for="remCheck{{{ $department->id }}}"></label>
                            </div>
                        </td>
                    @endif
                    <td>
                       <strong>{{{ limit_code($department->code) }}}</strong>
                    </td>
                    <td>
                        {{{ limit_name($department->name) }}}
                    </td>
                    <td>
                        {{{ if_null_then_na(limit_description($department->description)) }}}
                    </td>
                    <td class="table-action td-ignore">
                        <a href="{{ action('App\UnTrashController@getDepartments') }}?d={{ $department->id }}" class="tooltips" data-toggle="tooltip" data-placement="left" title="Restoring will un-delete the department">Restore</a>
                    </td>
                </tr>
            @endif
        @endforeach
        </tbody>
    </table>
</div>