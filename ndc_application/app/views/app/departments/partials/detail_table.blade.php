<div class="table-responsive">
    <table class="table table-hover table-striped mb30 table-fixed">
        <thead>
            <tr>
                <th class="wd-200">Code</th>
                <th class="wd-200">Name</th>
                <th>Description</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>
                  <strong>{{{ limit_code($department->code) }}}</strong>
                </td>
                <td>
                   {{{ limit_name($department->name) }}}
                </td>
                <td>
                    {{{ limit_description(if_null_then_na($department->description)) }}}
                </td>
            </tr>
        </tbody>
    </table>
</div>