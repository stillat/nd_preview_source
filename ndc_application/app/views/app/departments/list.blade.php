@extends('layouts.master')

@section('pageTitle')
<i class="fa fa-users"></i> Departments
@if(Input::get('search') == 1 && $foundResults)
    <span>Search Results</span>
@endif
@stop

@section('buttonBar')
    @if(Input::get('search') == 1 && $foundResults)
        <div class="btn-group pull-up-10">
            <a href="{{ action('App\DepartmentsController@index') }}" class="btn btn-white"><i class="fa fa-th-list inline-icon"></i></a>
        </div>
    @endif
<div class="btn-group pull-up-10">
    <a href="{{ action('App\Search\SearchController@getDepartmentSearchDialog') }}" data-toggle="modal" data-target="#modalHousing" class="btn btn-white"><i class="fa fa-search inline-icon"></i> Search Departments</a>
    @include('partials.generic_export', ['export_data' => 'org_departments', 'title' => 'departments', 'margin' => '88px'])
	<a href="{{ action('App\DepartmentsController@create') }}" class="btn btn-white nd-add"><i class="fa fa-plus inline-icon"></i> New Department</a>
</div>
@stop

@section('content')

    <div class="row hidden-print">
        <div class="col-md-12">
            {{ $departments->appends(array_except(Input::query(), Paginator::getPageName()))->links() }}
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            @include('app.departments.partials.table')
        </div>
    </div>

    <div class="row hidden-print">
        <div class="col-md-12">
            {{ $departments->appends(array_except(Input::query(), Paginator::getPageName()))->links() }}
        </div>
        @include('layouts.list_management')
    </div>


@include('layouts.dialogs.remove', array('title' => 'Remove Department', 'action' => 'App\DepartmentsController@destroy'))
@stop

@section('additionalScripts')
    <script>var NDC_LM = 'department';</script>
    <script src="{{{ app_url() }}}/assets/js/list_management.min.js{{{ UI::getBuster() }}}" defer></script>
@stop