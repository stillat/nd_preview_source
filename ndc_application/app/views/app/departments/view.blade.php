@extends('layouts.master')

@section('pageTitle')
<i class="fa fa-users"></i> {{{ limit_code($department->code) }}} - <small>{{{ limit_name($department->name) }}}</small>
@stop

@section('buttonBar')
<div class="btn-group pull-up-10">
    {{ HTML::modelNaviateToolbar('departments.show', $department->previous(), 'departments.index', 'departments.show', $department->next()) }}
</div>
<div class="btn-group pull-up-10">
    <a href="{{ route('departments.edit', array($department->id)) }}" class="btn btn-white"><i class="fa fa-edit inline-icon"></i> Update department</a>
</div>
@stop

@section('content')
<div class="row">
    <div class="col-md-12">
    @include('app.departments.partials.detail_table')
    </div>
</div>
@stop