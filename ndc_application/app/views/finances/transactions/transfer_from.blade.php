@extends('layouts.master')

@section('pageTitle')
<i class="fa ndcf-money-deposit"></i> Account Funds Transfer
@stop

@section('buttonBar')
<div class="btn-group">
	<a href="{{ Redirector::getRoute(route('finances.accounts.index')) }}" class="btn btn-danger">Cancel</a>
</div>
@stop

@section('content')
<div class="row">
	<div class="col-md-12">
		{{ Form::open(array('action' => 'App\Finances\Transactions\TransactionController@postTransfer', 'class' => 'form-horizontal')) }}
		<div class="panel panel-default">
			<div class="panel-body">
				<div class="form-group">
					<label class="col-sm-2 control-label">Destination Account</label>
					<div class="col-sm-5">
						<pre><strong>{{{ $destinationAccount[1] }}}</strong></pre>
					</div>
				</div>

				<div class="form-group">
					<label class="col-sm-2 control-label">Starting Account Account</label>
					<div class="col-sm-5">
						{{ Form::select('starting_account', $accounts, Input::old('starting_account'), array('id' => 'destination', 'class' => 'form-control chosen-select', 'data-placeholder' => 'Choose a destination account')) }}
					</div>
				</div>

				<div class="form-group">
					<label class="col-sm-2 control-label">Amount</label>
					<div class="col-sm-5">
						<div class="input-group">
							<span class="input-group-addon">
								$
							</span>
							<input type="number" step="any" class="form-control text-right" value="{{{ nd_number_format(Input::old('amount', 0)) }}}" name="amount" autofocus>
						</div>
					</div>
				</div>

				<div class="form-group">
					<label class="col-sm-2 control-label">Transfer Message</label>
					<div class="col-sm-5">
						<textarea class="form-control autosize" id="transferMessage" name="message" placeholder="Enter a message for this account funds transfer"></textarea>
					</div>
				</div>
			</div>
			<div class="panel-footer">
				<p>This is a manually initiated transfer and will automatically be <span class="label label-success">approved</span>.</p>
				<div class="btn-group mr5">
					<input type="hidden" name="destination_account" value="{{{ $destinationAccount[0] }}}">
					<button type="submit" value="return" name="submit" class="btn btn-primary"><i class="fa ndcf-money-deposit"></i> Transfer</button>
				</div>
				<div class="btn-group mr5">
					<button type="reset" class="btn btn-default">Reset</button>
				</div>
			</div>
		</div>
		{{ Form::close() }}
	</div>
</div>
@stop