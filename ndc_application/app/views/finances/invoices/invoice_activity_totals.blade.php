@foreach($workRecords as $record)
    <tr>
        <td>
            {{{ $record->getActivity()->code }}}
            @if (strlen($record->getActivityDescription()) > 0)
                - {{{ $record->getActivityDescription() }}}
            @endif
        </td>
        <td class="text-right" data-type="numeric">{{{ $record->rawData->total_invoice }}}</td>
    </tr>
@endforeach