@extends('layouts.master')

@section('pageTitle')
    <i class="fa ndcf-bill"></i> Invoice: {{{ $invoice->invoice_number }}} Payments
@stop

@section('buttonBar')
    <div class="btn-group n-mt-5">
        {{ HTML::modelNaviateToolbar('finances.invoices.payments.show', $neighbors['prev'], 'finances.invoices.index', 'finances.invoices.payments.show', $neighbors['next']) }}
    </div>
    <div class="btn-group n-mt-5">
        <a class="btn btn-white" href="{{ action('App\Finances\Invoices\InvoiceController@show', [$invoice->id]) }}">View
            Invoice</a>
        <a class="btn btn-white"
           href="{{ action('App\Finances\Invoices\InvoicePaymentsController@create', [$invoice->id]) }}">Add Payment</a>
    </div>
@stop

@section('content')

    <ul class="nav nav-tabs hidden-print">
        <li><a href="{{ action('App\Finances\Invoices\InvoiceController@show', [$invoice->id]) }}"><strong>Invoice Details</strong></a></li>
        <li class="active"><a href="#"><strong>Payments</strong></a>
        </li>
        <li><a href="{{ action('App\Finances\Invoices\InvoiceRecordsController@index', [$invoice->id]) }}"><strong>Work Records</strong></a></li>
    </ul>
    <div class="tab-content mb30">
        <div class="tab-pane active">


            <?php $transactionTabTitle = $invoice->invoice_number . ' Payments Processed'; ?>
            <div class="row">
                <div class="col-sm-4">
                    <dl class="dl-horizontal">
                        <dt>Invoice Number</dt>
                        <dd>{{{ $invoice->invoice_number }}}</dd>
                        <dt>Invoice Date</dt>
                        <dd>{{{ $invoicePeriod }}}</dd>
                        <dt>Due Date</dt>
                        <dd>{{{ (new Carbon($invoice->due_date))->format('F d, Y') }}}</dd>
                    </dl>
                </div>
                <div class="col-sm-3">
                    <dl>
                        <dt>Invoice Terms</dt>
                        <dd>{{{ $invoice->invoice_terms }}}</dd>
                    </dl>
                </div>
                <div class="col-sm-2">
                    <dl>
                        <dt>Client Notes</dt>
                        <dd>{{{ $invoice->client_notes }}}</dd>
                    </dl>
                </div>
                <div class="col-sm-3">
                    <dl class="dl-horizontal">
                        <dt>Invoice Total</dt>
                        <dd class="text-right">{{{ nd_number_format($invoice->invoice_total) }}}</dd>
                        <dt>Payments</dt>
                        <dd class="text-right">{{{ nd_number_format($invoice->payments_received) }}}</dd>
                        <dt>Balance</dt>
                        <dd class="text-right">{{{ nd_number_format($invoice->balance) }}}</dd>
                    </dl>
                </div>
                <div class="col-sm-12 pull-up-10">
                    <hr/>
                </div>
            </div>
            @include('finances.invoices.partials.delinquency_notice')
            @include('finances.accounts.partials.transactions')

        </div>
    </div>
@stop

@section('additionalScripts')
    <script src="{{{ app_url() }}}/assets/js/dep/rcolor.js" defer></script>
    <script src="{{{ app_url() }}}/assets/js/work-parts/color_management.js" defer></script>

    <script src="{{{ app_url() }}}/assets/js/finances/accounts.js" defer></script>
@stop