@extends('layouts.master')

@section('pageTitle')
    <i class="fa ndcf-bill"></i> New Payment for Invoice: {{{ $invoice->invoice_number }}}
@stop

@section('buttonBar')
    <div class="btn-group n-mt-5">
        <a href="{{ Redirector::getRoute(action('App\Finances\Invoices\InvoicePaymentsController@index', [$invoice->id])) }}"
           class="btn btn-danger ndc_form_leave">Cancel</a>
    </div>
@stop

@section('content')
    <div class="row">
        <div class="col-sm-4">
            <dl class="dl-horizontal">
                <dt>Invoice Number</dt>
                <dd>{{{ $invoice->invoice_number }}}</dd>
                <dt>Invoice Date</dt>
                <dd>{{{ $invoicePeriod }}}</dd>
                <dt>Due Date</dt>
                <dd>{{{ (new Carbon($invoice->due_date))->format('F d, Y') }}}</dd>
            </dl>
        </div>
        <div class="col-sm-3">
            <dl>
                <dt>Invoice Terms</dt>
                <dd>{{{ $invoice->invoice_terms }}}</dd>
            </dl>
        </div>
        <div class="col-sm-2">
            <dl>
                <dt>Client Notes</dt>
                <dd>{{{ $invoice->client_notes }}}</dd>
            </dl>
        </div>
        <div class="col-sm-3">
            <dl class="dl-horizontal">
                <dt>Invoice Total</dt>
                <dd class="text-right">{{{ nd_number_format($invoice->invoice_total) }}}</dd>
                <dt>Payments</dt>
                <dd class="text-right">{{{ nd_number_format($invoice->payments_received) }}}</dd>
                <dt>Balance</dt>
                <dd class="text-right">{{{ nd_number_format($invoice->balance) }}}</dd>
            </dl>
        </div>
        <div class="col-sm-12 pull-up-10"><hr/></div>
    </div>

    @include('finances.invoices.partials.delinquency_notice')
    @if ($invoice->balance <= 0)
    <div class="row">
        <div class="col-sm-12">
            <div class="alert alert-info">
                This invoice has a balance of <strong>{{{ nd_number_format($invoice->balance) }}}</strong> and currently does not require payment. Payments can still be added to this invoice, however.
            </div>
        </div>
    </div>
    @endif

    <div class="row">
        <div class="col-sm-12">
            {{ Form::open(array('action' => ['App\Finances\Invoices\InvoicePaymentsController@store', $invoice->id], 'method' => 'post', 'class' => 'form-horizontal')) }}
            <div class="panel-body">
                <div class="form-group">
                    <label class="col-sm-2 control-label"><strong><i class="fa ndcf-account-payable" style="font-size: 17px;"></i> Bill To Account</strong></label>
                    <div class="col-sm-4">
                        <p class="form-control-static">{{{ $invoice->billedToAccount }}}</p>
                    </div>
                    <label class="col-sm-2 control-label"><strong><i class="fa ndcf-accounts-receivable" style="font-size: 17px;"></i> Pay To Account</strong></label>
                    <div class="col-sm-4">
                        <p class="form-control-static">{{{ $invoice->paidToAccount  }}}</p>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Payment Amount</label>
                    <div class="col-sm-5">
                        <div class="input-group">
							<span class="input-group-addon">
								$
							</span>
                            <input type="number" step="any" class="form-control text-right" value="{{{ nd_number_format(Input::old('amount', Input::get('initialize_payment', 0))) }}}" name="amount" autofocus>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-2 control-label">Payment Notes</label>
                    <div class="col-sm-5">
                        <textarea class="form-control autosize" id="transferMessage" name="message" placeholder="Enter a message for this payment"></textarea>
                    </div>
                </div>
            </div>
            <div class="panel-footer col-sm-offset-2">
                <p>This is a manually initiated payment. It's associated transaction will automatically be <span class="label label-success">approved</span>.</p>
                <div class="btn-group mr5">
                    <button type="submit" value="return" name="submit" class="btn btn-primary"><i class="fa ndcf-money-transfer"></i> Process payment</button>
                </div>
                <div class="btn-group mr5">
                    <button type="reset" class="btn btn-default">Reset</button>
                </div>
            </div>
            {{ Form::close() }}
        </div>
    </div>
@stop