<!DOCTYPE html>
<html>
<head>
    <title>ND Counties</title>
    {{ UI::getCss() }}
</head>
<body class="signin">
<div class="container text-center">
    <div style="max-width: 370px;padding: 15px;margin: 0 auto;">
        <h2 class="form-signin-heading"><i class="fa fa-search"></i> No Invoice Results</h2>

        <p>The invoice search engine did not find any invoices. Consider widening the search criteria or filters.</p>
        <br>
        <a class="btn btn-white" id="closeWindow">Close this window</a>
    </div>
    <div class="text-left">
    </div>
    <hr>
    <a href="https://ndcounties.com/app">ND Counties</a>
</div>
<script>
    function init() {
        document.getElementById('closeWindow').onclick = function () {
            window.close();
        };
    }
    window.onload = init;
</script>
</body>
</html>