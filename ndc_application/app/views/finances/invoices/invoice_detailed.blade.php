@foreach($workRecords as $record)
    @if($record->getRecordTotals()->employeeCombined > 0)
        <tr>
            <td>
                <div><strong>Employee: ({{{ $record->getEmployee()->code }}})
                        - {{{ $record->getEmployee()->lastName.', '.$record->getEmployee()->firstName }}}</strong>
                </div>
                @if(strlen($record->getActivityDescription()) > 0)
                    <small>{{{ $record->getActivityDescription() }}}</small>
                @else
                    <small>Cost for hours worked.</small>
                @endif
            </td>
            <td class="text-right" data-type="numeric">{{{ ($record->getRecordTotals()->employeeTotalHours) }}}</td>
            <td class="text-right"><strong>WND</strong></td>
            <td class="text-right" data-type="numeric">{{{ ($record->getRecordTotals()->employeeCombined) }}}</td>
        </tr>
    @endif
    @if($record->getRecordTotals()->miscBilling > 0)
        <tr>
            <td>
                <div><strong>Miscellaneous Billing</strong></div>
                <small>{{{ $record->getMiscBilling()->description }}}</small>
            </td>
            <td class="text-right" data-type="numeric">{{{ ($record->getMiscBilling()->quantity) }}}
                ({{{ $reader->getMeasurementUnit($record->getMiscBilling()->unit_id)->name}}})
            </td>
            <td class="text-right" data-type="numeric">{{{ ($record->getMiscBilling()->rate) }}}</td>
            <td class="text-right" data-type="numeric">{{{ ($record->getRecordTotals()->miscBilling) }}}</td>
        </tr>
    @endif
    @if ($record->getProperty()->work_data != null)
        @foreach($record->getProperty()->work_data as $adapter => $data)
            {{ $manager->formatPropertyInvoice($adapter, [$record,$data]) }}
        @endforeach
    @endif
    @foreach($record->getEquipmentUnits() as $equipment)
        <tr>
            <td>
                <div>
                    <strong>{{{ $equipment->equipment->code }}}</strong> {{{ $equipment->equipment->name }}}
                </div>
                <small>Equipment Unit</small>
            </td>
            <td class="text-right">{{{ nd_number_format($equipment->total_quantity) }}}
                ({{{ $equipment->equipment->unit->name }}})
            </td>
            <td class="text-right">{{{ nd_number_format($equipment->historic_cost) }}}</td>
            <td class="text-right">{{{ nd_number_format($equipment->total_cost) }}}</td>
        </tr>
        @foreach($equipment->fuels as $fuel)
            <tr>
                <td>
                    <div><strong>{{{ $fuel->fuel->code }}}</strong> {{{ $fuel->fuel->name }}}</div>
                    <small>Fuel for {{{ $equipment->equipment->code }}}</small>
                </td>
                <td class="text-right">{{{ nd_number_format($fuel->total_quantity) }}}
                    ({{{ $fuel->fuel->unit->name }}})
                </td>
                <td class="text-right">{{{ nd_number_format($fuel->historic_cost) }}}</td>
                <td class="text-right">{{{ nd_number_format($fuel->total_cost) }}}</td>
            </tr>
        @endforeach
    @endforeach
    @foreach($record->getMaterials() as $material)
        <tr>
            <td>
                <div>
                    <strong>{{{ $material->material->code }}}</strong> {{{ $material->material->name }}}
                </div>
                <small>Material</small>
            </td>
            <td class="text-right">{{{ nd_number_format($material->total_quantity) }}}
                ({{{ $material->material->unit->name }}})
            </td>
            <td class="text-right">{{{ nd_number_format($material->historic_cost) }}}</td>
            <td class="text-right">{{{ nd_number_format($material->total_cost) }}}</td>
        </tr>
    @endforeach
@endforeach