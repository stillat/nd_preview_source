@extends('layouts.master')

@section('pageTitle')
<i class="fa ndcf-bill"></i> {{{ $viewModes[$currentViewMode] }}}
@stop

@section('buttonBar')
    <div class="btn-group n-mt-5" style="margin-top: -7px;">
        <div style="width: 200px; display: inline-block; margin-left: 5px;">
            {{ Form::select('view', $viewModes, $currentViewMode, array('class' => 'form-control nd-filter n-mt-5', 'style' => 'margin-top: 10px;', 'id' => 'invoiceStatusChooser','data-placeholder' => 'Filter invoices')) }}
        </div>
    </div>
    <div class="btn-group n-mt-5">
        @include('partials.generic_export', ['export_data' => 'fs_invoices', 'title' => 'finance invoices', 'margin' => '-89px'])
    </div>
    <div class="btn-group n-mt-5">
        <a class="btn btn-white mr10" href="{{{ action('App\Work\WorkLogsController@create') }}}"><i class="fa fa-coffee inline-icon"></i> Add Work Record</a>
        <a class="btn btn-white" href="{{{ action('App\Finances\AccountsController@index') }}}"><i class="fa ndcf-accounts-book inline-icon"></i> Manage Accounts</a>
    </div>
@stop

@section('content')

<div class="row">
    <div class="col-sm-12">
        {{ $invoices->links() }}
    </div>
    <div class="col-sm-12">
        @include('finances.invoices.partials.table')
    </div>
    <div class="col-sm-12">
        {{ $invoices->links() }}
    </div>
</div>

@stop

@section('additionalScripts')
    <script src="{{ app_url() }}/assets/js/finances/invoice.js" defer></script>
@stop