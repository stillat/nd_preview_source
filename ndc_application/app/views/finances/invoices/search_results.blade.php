@extends('layouts.master')

@section('pageTitle')
    <i class="fa fa-search"></i> Invoice Search Results
@stop

@section('additionalStyles')
    <link rel="stylesheet" href="{{{ app_url() }}}/assets/css/reporting.css"/>
    @if($settings->report_print_orientation == 2)
        <style>
            @media print {
                @page {
                    size: landscape
                }
            }
        </style>
    @endif
@stop

@section('content')

    <div class="row">
        <div class="col-sm-12">
            @foreach($invoices as $invoice)
                <?php // lk($invoice); ?>
                <table class="table table-hover table-bordered">
                    <tbody>
                    <tr>
                        <th>Invoice #</th>
                        <td class="text-right"><a
                                    href="{{{ action('App\Finances\Invoices\InvoiceController@show', array($invoice->id)) }}}">{{{ $invoice->invoice_number }}}</a>
                        </td>
                        <th>Invoice Period</th>
                        <td class="text-right">{{{ (new Carbon($invoice->invoice_start))->format('F d, Y') }}}
                            - {{{ (new Carbon($invoice->invoice_end))->format('F d, Y') }}}</td>
                        <th>Due Date</th>
                        <td class="text-right">{{{ (new Carbon($invoice->due_date))->format('F d, Y') }}}</td>
                    </tr>
                    <tr>
                        <th>Bill To Account</th>
                        <td class="text-right">{{{ $invoice->billedToAccount }}}</td>
                        <th>Pay To Account</th>
                        <td class="text-right">{{{ $invoice->paidToAccount }}}</td>
                        <th>Invoice Total</th>
                        <td class="text-right" data-type="numeric">{{{ $invoice->invoice_total }}}</td>
                    </tr>
                    @if(strlen($invoice->client_notes) > 0)
                        <tr>
                            <th>Invoice Notes</th>
                            <td class="text-right">{{{ $invoice->client_notes }}}</td>
                        </tr>
                    @endif
                    <tr>
                        <td colspan="6">
                            <table class="table table-bordered table-hover {{ $breakdownClass }}">
                                <tbody>
                                @foreach($workRecords[$invoice->id] as $record)
                                    @if ($showActivities)
                                        <tr>
                                            <th class="wd-200">Activity</th>
                                            <td>{{{ $record->getActivity()->code.' - '.$record->getActivityDescription() }}}</td>
                                            <td class="text-right"
                                                data-type="numeric">{{{ $record->rawData->total_invoice }}}</td>
                                        </tr>
                                    @endif
                                    @if ($showProp)
                                        <tr>
                                            <th class="wd-200">Property</th>
                                            <td colspan="2">{{ $record->getReportPropertyDisplay() }}</td>
                                        </tr>
                                    @endif
                                    @if ($showEmployees)
                                        <tr>
                                            <th class="wd-200">Employee</th>
                                            <td colspan="2">{{{ $record->getEmployee()->code }}}</td>
                                        </tr>
                                    @endif
                                    @if ($showMaterials)
                                        @if (count($record->getMaterials()) > 0)
                                            <tr>
                                                <td colspan="3">
                                                    <table class="table table-hover {{ $breakdownClass }}">
                                                        <thead>
                                                        <th class="wd-100">Material</th>
                                                        <th class="text-right">Quantity</th>
                                                        </thead>
                                                        <tbody>
                                                        @foreach ($record->getMaterials() as $material)
                                                            <tr>
                                                                <td>{{{ $material->material->code }}}</td>
                                                                <td class="text-right"
                                                                    data-type="numeric">{{ $material->total_quantity }}</td>
                                                            </tr>
                                                        @endforeach
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                        @endif
                                    @endif
                                    @if ($showEquips)
                                        @if (count($record->getEquipmentUnits()) > 0)
                                            <tr>
                                                <td colspan="3">

                                                    <table class="table table-hover table-condensed {{ $breakdownClass }}">
                                                        <thead>
                                                        <tr>
                                                            <th>Equipment Unit</th>
                                                            <th>Odometer</th>
                                                            <th class="text-right">Quantity</th>
                                                        </tr>
                                                        </thead>
                                                        @foreach($record->getEquipmentUnits() as $equipment)
                                                            <tbody>
                                                            <tr>
                                                                <td>
                                                                    <strong>{{{ $equipment->equipment->code }}}</strong> {{{ $equipment->equipment->name }}}
                                                                </td>
                                                                <td>
                                                                    @if($equipment->equipment->odometer == null)
                                                                        &nbsp;
                                                                    @else
                                                                        {{{ $equipment->equipment->odometer->odometer_reading }}}
                                                                    @endif
                                                                </td>
                                                                <td class="text-right"><span
                                                                            data-type="numeric">{{{ $equipment->total_quantity }}}</span>
                                                                    ({{{ $equipment->equipment->unit->name }}})
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td colspan="3">
                                                                    <table class="table table-hover table-condensed">
                                                                        <thead>
                                                                        <tr>
                                                                            <th>Fuel</th>
                                                                            <th class="text-right">Quantity</th>
                                                                            <th>Unit</th>
                                                                        </tr>
                                                                        </thead>
                                                                        <tbody>
                                                                        @foreach($equipment->fuels as $fuel)
                                                                            <tr>
                                                                                <td>
                                                                                    <strong>{{{ $fuel->fuel->code }}}</strong> {{{ $fuel->fuel->name }}}
                                                                                </td>
                                                                                <td class="text-right"><span
                                                                                            data-type="numeric">{{{ $fuel->total_quantity }}}</span>
                                                                                </td>
                                                                                <td>{{{ $fuel->fuel->unit->name }}}</td>
                                                                            </tr>
                                                                        @endforeach
                                                                        </tbody>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            </tbody>
                                                        @endforeach
                                                    </table>
                                                </td>
                                            </tr>
                                        @endif
                                    @endif

                                    <tr style="margin: 0px; padding:0px;border-top:2px solid rgb(211, 206, 206);">
                                        <td colspan="3" style="padding:0px;"></td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </td>
                    </tr>
                    </tbody>
                </table>
            @endforeach
        </div>
    </div>

@stop