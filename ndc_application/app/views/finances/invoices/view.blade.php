@extends('layouts.master')
@section('pageTitle')
    <i class="fa ndcf-bill"></i> Invoice: {{{ $invoice->invoice_number }}}
    <span class="label hidden-print">
        @if($invoice->invoice_status != 5)
            <i class="fa fa-unlock"></i> Unlocked
        @else
            <i class="fa fa-lock"></i> Locked
        @endif
    </span>
@stop
@section('buttonBar')
    <div class="btn-group n-mt-5">
        {{ HTML::modelNaviateToolbar('finances.invoices.show', $neighbors['prev'], 'finances.invoices.index', 'finances.invoices.show', $neighbors['next']) }}
    </div>
    <div class="btn-group n-mt-5">
        <a class="btn btn-white"
           href="{{ action('App\Finances\Invoices\InvoicePaymentsController@index', [$invoice->id]) }}">Payments</a>
    </div>
    <div class="btn-group n-mt-5">
        <a class="btn btn-white" href="#" id="printInvoice"><i class="fa fa-print inline-icon"></i> Print Invoice</a>
    </div>
    <div class="btn-group n-mt-5">
        <button type="button" class="btn btn-white dropdown-toggle" data-toggle="dropdown">
            <i class="fa fa-wrench inline-icon"></i> <span class="caret"></span>
        </button>
        <ul class="dropdown-menu" role="menu" style="margin-left: -140px;">
            @if($invoice->invoice_status != 5)
                <li><a href="{{ action('App\Finances\Invoices\InvoiceController@getLockInvoice', [$invoice->id]) }}"
                       data-toggle="modal" data-target="#modalHousing"><i class="fa fa-lock inline-icon"></i> Lock this
                        invoice</a></li>
            @else
                <li><a href="{{ action('App\Finances\Invoices\InvoiceController@getUnlockInvoice', [$invoice->id]) }}"
                       data-toggle="modal" data-target="#modalHousing"><i class="fa fa-unlock inline-icon"></i> Unlock
                        this invoice</a></li>
            @endif
            <li class="divider"></li>
            <li><a href="{{ action('App\Finances\Invoices\InvoiceController@getUpdateAccounts', [$invoice->id]) }}"
                   data-toggle="modal" data-target="#modalHousing"><i class="fa ndcf-accounts-book inline-icon"></i>
                    Change invoice accounts</a></li>
            <li><a href="{{ action('App\Finances\Invoices\InvoiceController@getUpdateInvoiceNumber', [$invoice->id]) }}"
                   data-toggle="modal" data-target="#modalHousing"><i class="fa fa-pencil inline-icon"></i> Change
                    Invoice Number</a></li>
        </ul>
    </div>
@stop
@section('content')

    @include('finances.invoices.partials.delinquency_notice')

    <ul class="nav nav-tabs hidden-print">
        <li class="active"><a href="#"><strong>Invoice Details</strong></a></li>
        <li><a href="{{ action('App\Finances\Invoices\InvoicePaymentsController@index', [$invoice->id]) }}"><strong>Payments</strong></a></li>
        <li><a href="{{ action('App\Finances\Invoices\InvoiceRecordsController@index', [$invoice->id]) }}"><strong>Work Records</strong></a></li>
    </ul>
    <div class="tab-content mb30">
        <div class="tab-pane active" id="home">
            @include('finances.invoices.invoice_details')
        </div>
    </div>
@stop
@section('additionalScripts')
    <script src="{{ app_url() }}/assets/js/finances/invoice.js" defer></script>
@stop