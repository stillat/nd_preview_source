<div>
    <div class="panel-body">
        <div class="row" id="invoiceTable">
            <div class="col-sm-6">
                <h5 class="subtitle mb10">Billed Account</h5>
                <address>
                    <strong>{{{ $invoice->billedToAccount }}}</strong><br>
                    {{ HTML::addressBlock($invoice->billedAddressLine1, $invoice->billedAddressLine2, $invoice->billedCity, $invoice->billedState, $invoice->billedZip, $invoice->billedCountry) }}
                </address>
            </div>
            <div class="col-sm-6 text-right">
                <h5 class="subtitle mb10">Invoice No.</h5>
                <h4 class="text-primary">{{{ $invoice->invoice_number }}}</h4>
                <address>
                    <strong>{{{ $invoice->paidToAccount  }}}</strong>
                    {{ HTML::addressBlock(
                    $invoice->paidAddressLine1, $invoice->paidAddressLine2,
                    $invoice->paidCity, $invoice->paidState, $invoice->paidZip,
                    $invoice->paidCountry
                    ) }}
                </address>
                <p><strong>Invoice Date:</strong> {{{ $invoicePeriod }}}</p>

                <p>
                    <strong>Due Date:</strong> {{{ (new Carbon($invoice->due_date))->format('F d, Y') }}}
                    @if(Carbon::now()->gt(new Carbon($invoice->due_date)) && $invoice->balance > 0)
                        <span class="label label-danger">Past Due</span>
                    @endif
                </p>
            </div>
        </div>
        <div class="table-responsive">
            <table class="table table-condensed" id="invoiceItemsTable">
                <thead>
                <tr>
                    <th>Description</th>
                    <th class="text-right">Cost</th>
                </tr>
                </thead>
                <tbody>
                @include('finances.invoices.invoice_activity_totals')
                </tbody>
            </table>
        </div>
        <div class="row">
            <div class="col-sm-6">
                @if(strlen($invoice->client_notes)>0)
                    <h4>Client Notes</h4>
                    <p class="ww">{{{ $invoice->client_notes }}}</p>
                @endif
            </div>
            <div class="col-sm-6">
                @if(strlen($invoice->invoice_terms)>0)
                    <h4>Invoice Terms</h4>
                    <p class="ww">{{{ $invoice->invoice_terms }}}</p>
                @endif
            </div>
        </div>
        <table class="table table-total" style="width: 20%">
            <tbody>
            <tr class="<?php if ($invoice->balance > 0) {
    echo 'danger';
} else {
    echo 'success';
} ?>">
                <td class="text-left"><strong>TOTAL :</strong></td>
                <td data-type="numeric">{{{ ($invoice->invoice_total) }}}</td>
            </tr>
            <tr class="<?php if ($invoice->payments_received < $invoice->invoice_total) {
    echo 'danger';
} else {
    echo 'success';
} ?>">
                <td class="text-left"><strong><a
                                href="{{ action('App\Finances\Invoices\InvoicePaymentsController@index', [$invoice->id]) }}">PAYMENTS
                            :</a></strong></td>
                <td data-type="numeric">{{{ ($invoice->payments_received) }}}</td>
            </tr>
            <tr class="<?php if ($invoice->balance > 0) {
    echo 'danger';
} else {
    echo 'success';
} ?>">
                <td class="text-left"><strong>BALANCE :</strong></td>
                <td data-type="numeric">
                    {{{ ($invoice->balance) }}}
                </td>
            </tr>
            </tbody>
        </table>
    </div>
</div>