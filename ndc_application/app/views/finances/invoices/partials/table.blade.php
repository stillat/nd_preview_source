<div class="table-responsive">
    <table class="table table-hover">
        <thead>
        <tr>
            <th class="wd-100">Invoice Start</th>
            <th>Invoice End</th>
            <th>Invoice Number</th>
            <th>Bill To</th>
            <th>Pay To</th>
            <th>Status</th>
            <th>Due Date</th>
            <th class="text-right">Total Discount</th>
            <th class="text-right">Total Tax</th>
            <th class="text-right">Invoice Total</th>
            <th class="text-right">Payments Made</th>
            <th class="text-right">Balance</th>
            <th class="wd-100 hidden-print text-right">
                <a href="{{{ action('App\Search\SortingController@getFinanceInvoices') }}}" data-toggle="modal"
                   data-target="#modalHousing"><i class="fa fa-sort"></i>
                    <small>Sorting</small>
                </a>
            </th>
        </tr>
        </thead>
        <tbody>
        @foreach($invoices as $invoice)
            <tr>
                <td><a class="d_block"
                       href="{{{ action('App\Finances\Invoices\InvoiceController@show', array($invoice->id)) }}}">{{{ (new Carbon($invoice->invoice_start))->format('Y-m-d') }}}</a>
                </td>

                <td><a class="d_block"
                       href="{{{ action('App\Finances\Invoices\InvoiceController@show', array($invoice->id)) }}}">{{{ (new Carbon($invoice->invoice_end))->format('Y-m-d') }}}</a>
                </td>
                <td><a class="d_block"
                       href="{{{ action('App\Finances\Invoices\InvoiceController@show', array($invoice->id)) }}}">{{{ limit_name($invoice->invoice_number) }}}</a>
                </td>
                <td><a class="d_block"
                       href="{{{ action('App\Finances\Invoices\InvoiceController@show', array($invoice->id)) }}}">{{{ $invoice->billedToAccount }}}</a>
                </td>
                <td><a class="d_block"
                       href="{{{ action('App\Finances\Invoices\InvoiceController@show', array($invoice->id)) }}}">{{{ $invoice->paidToAccount }}}</a>
                </td>
                <td>
                    <a class="d_block"
                       href="{{{ action('App\Finances\Invoices\InvoiceController@show', array($invoice->id)) }}}">
                        @if($invoice->invoice_status != 5)
                            <span class="label label-default"><i class="fa fa-unlock"></i> Unlocked</span>
                        @else
                            <span class="label label-default"><i class="fa fa-lock"></i> Locked</span>
                        @endif

                        @if($invoice->is_closed)
                            <span class="label label-danger">Closed</span>
                        @else
                            <span class="label label-success">Open</span>
                        @endif

                        @if(Carbon::now()->gt(new Carbon($invoice->due_date)) && $invoice->cached_invoice_balance > 0)
                            <span class="label label-danger">Past Due</span>
                        @endif
                    </a>
                </td>
                <td><a class="d_block"
                       href="{{{ action('App\Finances\Invoices\InvoiceController@show', array($invoice->id)) }}}">{{{ (new Carbon($invoice->due_date))->format('F d, Y') }}}</a>
                </td>
                <td class="text-right"><a class="d_block"
                                          href="{{{ action('App\Finances\Invoices\InvoiceController@show', array($invoice->id)) }}}"><span data-type="numeric">{{{ ($invoice->invoice_discount_total) }}}</span></a>
                </td>
                <td class="text-right"><a class="d_block"
                                          href="{{{ action('App\Finances\Invoices\InvoiceController@show', array($invoice->id)) }}}"><span data-type="numeric">{{{ ($invoice->invoice_tax_amount) }}}</span></a>
                </td>
                <td class="text-right"><a class="d_block"
                                          href="{{{ action('App\Finances\Invoices\InvoiceController@show', array($invoice->id)) }}}"><span data-type="numeric">{{{ ($invoice->invoice_total) }}}</span></a>
                </td>
                <td class="text-right"><a class="d_block"
                                          href="{{{ action('App\Finances\Invoices\InvoiceController@show', array($invoice->id)) }}}"><span data-type="numeric">{{{ ($invoice->invoice_payments) }}}</span></a>
                </td>
                <td class="text-right"><a class="d_block"
                                          href="{{{ action('App\Finances\Invoices\InvoiceController@show', array($invoice->id)) }}}"><span data-type="numeric">{{{ ($invoice->cached_invoice_balance) }}}</span></a>
                </td>
                <td class="text-right"><a
                            href="{{{ action('App\Finances\Invoices\InvoiceController@show', array($invoice->id)) }}}">View</a>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>