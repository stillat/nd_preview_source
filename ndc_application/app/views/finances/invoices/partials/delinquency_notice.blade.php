@if(Carbon::now()->gt(new Carbon($invoice->due_date)) && $invoice->balance > 0)
<div class="row">
    <div class="col-sm-12">
        <div class="alert alert-danger">
            <strong>Delinquency Notice</strong> This invoice is currently past due.
        </div>
    </div>
</div>
@endif