@extends('layouts.master')

@section('pageTitle')
    <i class="fa fa-search"></i> Invoice Search
@stop

@section('prependBody')
    {{ Form::open(['action' => 'App\Finances\Invoices\InvoiceController@postSearch', 'method' => 'POST', 'id' => 'invoiceSearchForm']) }}
    <input type="hidden" name="report_start_date" id="report_start_date" value="{{ $startDate }}"/>
    <input type="hidden" name="report_end_date" id="report_end_date" value="{{ $endDate }}"/>
    <input type="hidden" value="0" id="report_new_window" name="report_new_window">
@stop

@section('appendBody')
    {{ Form::close() }}
@stop

@section('buttonBar')
    <div id="daterange" class="btn btn-white n-mt-5"><i class="fa fa-calendar-o inline-icon"
                                                        style="margin-right:10px;"></i> <span> September 10, 2014 - September 24, 2014</span>
        <b class="caret"></b></div>
@stop

@section('content')

    <div id="createFormLoader">
        <div class="spinner"></div>
        <p style="margin-right:auto;margin-left: auto; width: 300px; text-align: center;">Getting things ready. Please
            wait.</p>
    </div>
    <div id="formHolder" style="display:none;">
        <div class="panel-body nopadding" style="padding-left: 10px !important; padding-right: 10px !important;">
            <ul id="mainTab" class="nav nav-tabs nopadding"
                style="background-color: white; border-bottom-width: 2px;border-bottom-style: solid;border-bottom-color: rgb(66, 139, 202);">
                <li class="active bb_property interaction_trigger" style="margin-bottom: -2px;"><a href="#lwFilters"
                                                                                                   data-toggle="tab"><strong><i
                                    class="fa fa-filter"></i> <span
                                    class="hidden-sm hidden-xs hidden-md">Filters</span></strong></a></li>
                <li class="bb_settings interaction_trigger" style="margin-bottom: -2px;"><a href="#lwOptions"
                                                                                            id="lwOptionsActivator"
                                                                                            data-toggle="tab"><strong><i
                                    class="fa fa-cogs"></i> <span
                                    class="hidden-sm hidden-xs hidden-md">Options</span></strong></a></li>

                <li class="pull-right live-help-step-four-selector" data-toggle="popover" data-trigger="hover"
                    data-placement="bottom" data-content="Search the invoices with provided filters"><a
                            class="btn btn-white" id="generateReportButton" style="color: #2980b9;"><i
                                class="fa fa-play"></i> Search Invoices</a></li>
                <li class="pull-right live-help-step-four-selector" data-toggle="popover" data-trigger="hover"
                    data-placement="bottom"
                    data-content="Search the invoices with the provided filters, and open the results in a new window">
                    <a class="btn btn-white" id="generateReportInNewWindowButton" style="color: #2980b9;"><i
                                class="fa fa-external-link-square"></i></a></li>
            </ul>
            <div class="tab-content nopadding">
                <div class="tab-pane active" id="lwFilters">
                    <table class="table table-hover table-bordered">
                        <thead>
                        <tr>
                            <th class="wd-200">Filter</th>
                            <th>Value</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td><strong>Invoice #:</strong></td>
                            <td>{{ Form::select('invoice_filters[]', $invoices, null, ['class' => 'chosen-select', 'id' => 'invoiceFilter', 'multiple' => 'multiple', 'data-placeholder' => 'Select your invoice numbers']) }}</td>
                        </tr>
                        <tr>
                            <td><strong>Bill To Account:</strong></td>
                            <td>{{ Form::select('bill_to_filters[]', $accounts, null, ['class' => 'chosen-select', 'id' => 'billToFilter', 'multiple' => 'multiple', 'data-placeholder' => 'Select your bill to accounts']) }}</td>
                        </tr>
                        <tr>
                            <td><strong>Pay To Account:</strong></td>
                            <td>{{ Form::select('pay_to_filters[]', $accounts, null, ['class' => 'chosen-select', 'id' => 'payToFilter', 'multiple' => 'multiple', 'data-placeholder' => 'Select your pay to accounts']) }}</td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>
                                <div class="col-sm-6">
                                    <div class="ckbox ckbox-default" style="margin-top: 12px;">
                                        <input type="checkbox" value="1" id="honorDateRange" name="honor_date_range">
                                        <label for="honorDateRange"> <i class="fa fa-calendar-o"></i> Use the date range
                                            when searching</label></div>
                                    <br>

                                    <p>When using the date range in a search, any invoice that has an invoice period outside
                                        of the selected date range will not be included. Generally, this option is not
                                        required if the invoice number/s is/are known.</p>
                                </div>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <div class="tab-pane" id="lwOptions">
                    <div class="col-sm-6">
                        <div class="ckbox ckbox-default" style="margin-top: 12px;">
                            <input type="checkbox" value="1" id="showActivities" name="show_activities" checked="checked">
                            <label for="showActivities"> <i class="fa fa-bicycle"></i> Show activities on results (individual record totals are displayed with activites)
                            </label>
                        </div>
                        <div class="ckbox ckbox-default" style="margin-top: 12px;">
                            <input type="checkbox" value="1" id="showEmployees" name="show_employees">
                            <label for="showEmployees"> <i class="fa fa-user"></i> Show employees on results
                            </label>
                        </div>
                        <div class="ckbox ckbox-default" style="margin-top: 12px;">
                            <input type="checkbox" value="1" id="showProperty" name="show_property">
                            <label for="showProperty"> <i class="fa fa-home"></i> Show property on results
                            </label>
                        </div>
                        <div class="ckbox ckbox-default" style="margin-top: 12px;">
                            <input type="checkbox" value="1" id="showMaterials" name="show_materials">
                            <label for="showMaterials"> <i class="fa fa-cubes"></i> Show materials on results
                            </label>
                        </div>
                        <div class="ckbox ckbox-default" style="margin-top: 12px;">
                            <input type="checkbox" value="1" id="showEquipmentAndFuels" name="show_equips">
                            <label for="showEquipmentAndFuels"> <i class="fa fa-truck"></i> Show equipment units and fuels on results
                            </label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('additionalScripts')
    <script>
        var startDate = '{{ $startDate }}';
        var endDate = '{{ $endDate }}';
    </script>
    <script src="{{{ app_url() }}}/assets/js/dep/chosen.jquery.min.js" defer></script>
    <script src="{{{ app_url() }}}/assets/js/search_invoices.js{{ UI::getBuster() }}" }} defer></script>
@stop