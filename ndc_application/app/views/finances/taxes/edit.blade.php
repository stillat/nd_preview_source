@extends('layouts.master')

@section('pageTitle')
<i class="fa fa-plus"></i> <strong>{{{ limit_name($taxRate->tax_name) }}}</strong>
@stop

@section('buttonBar')
<div class="btn-group pull-up-10">
	<a href="{{ Redirector::getRoute(route('finances.tax-rates.index')) }}" class="btn btn-danger ndc_form_leave">Cancel</a>
</div>
@stop

@section('content')

<div class="row">

	<div class="col-md-12">
		{{ Form::open(array('route' => array('finances.tax-rates.update', $taxRate->id), 'method' => 'put', 'class' => 'form-horizontal')) }}
		<div class="panel-primary panel-alt">
			<div class="panel-body">
				<div class="form-group is-required">
					<label class="col-sm-2 control-label" for="name">Name:</label>
					<div class="col-sm-8">
						<input type="text" id="name" name="tax_name" autofocus class="form-control mousetrap {{{ user_form_size() }}}" value="{{{ Input::old('name', $taxRate->tax_name) }}}">
						{{ Form::errorMsg('name') }}
					</div>
				</div>

				<div class="form-group is-required">
					<label class="col-sm-2 control-label" for="cost">Tax Rate:</label>
					<div class="col-sm-3">
						<div class="input-group">
							<span class="input-group-addon">%</span>
							<input type="number" min="0" step="any" id="cost" name="tax_rate" class="form-control text-right mousetrap {{{ user_form_size() }}}" value="{{{ nd_number_format(Input::old('cost', nd_number_format($taxRate->tax_rate))) }}}">
						</div>
						{{ Form::errorMsg('cost') }}
					</div>
				</div>

				<fieldset>
					<div class="form-group">
						<div class="col-sm-offset-2 col-sm-10">
							<div class="checkbox">
								<label>
									{{ Form::checkbox('affects_all', '0', $taxRate->affects_all) }} This tax rate affects everything
								</label>
							</div>
						</div>
					</div>
					<div class="form-group">
						<div class="col-sm-offset-2 col-sm-10">
							<div class="checkbox">
								<label>
									{{ Form::checkbox('affects_employees', '0', $taxRate->affects_employees) }} This tax rate affects employees
								</label>
							</div>
						</div>
						<div class="col-sm-offset-2 col-sm-10">
							<div class="checkbox">
								<label>
									{{ Form::checkbox('affects_equipments', '0', $taxRate->affects_equipments) }} This tax rate affects equipment units
								</label>
							</div>
						</div>
						<div class="col-sm-offset-2 col-sm-10">
							<div class="checkbox">
								<label>
									{{ Form::checkbox('affects_fuels', '0', $taxRate->affects_fuels) }} This tax rate affects fuels
								</label>
							</div>
						</div>
						<div class="col-sm-offset-2 col-sm-10">
							<div class="checkbox">
								<label>
									{{ Form::checkbox('affects_materials', '0', $taxRate->affects_materials) }} This tax rate affects materials
								</label>
							</div>
						</div>
					</div>
				</fieldset>

			</div>
			<div class="panel-footer col-sm-offset-2">
				<div class="btn-group mr5">
					<button type="submit" value="return" name="submit" class="btn btn-primary">Update this tax rate</button>
				</div>

				<div class="btn-group mr5">
					<button type="reset" id="ndc_form_reset" class="btn btn-default">Reset</button>
				</div>
			</div>
		</div>
		{{ Form::close() }}
	</div>

</div>

@stop