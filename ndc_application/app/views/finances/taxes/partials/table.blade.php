<div class="table-responsive">
    <table class="table table-hover mb30 table-fixed">
        <thead>
        <tr>
            <th>Name</th>
            <th class="wd-100 text-right">Tax Rate</th>
            <th class="wd-150 text-right">Affects Employees</th>
            <th class="wd-200 text-right">Affects Equipment Units</th>
            <th class="wd-150 text-right">Affects Fuels</th>
            <th class="wd-150 text-right">Affects Materials</th>
            <th class="wd-100 hidden-print text-right">
                <a href="{{{ action('App\Search\SortingController@getTaxRates') }}}" data-toggle="modal"
                   data-target="#modalHousing"><i class="fa fa-sort"></i>
                    <small>Sorting</small>
                </a>
            </th>
        </tr>
        </thead>
        <tbody>
        @foreach($taxRates as $taxRate)
            @if ($taxRate->deleted_at == null)
                <tr>
                    <td><strong>{{{ limit_name($taxRate->tax_name) }}}</strong></td>
                    <td class="text-right">{{{ nd_number_format($taxRate->tax_rate) }}}</td>
                    <td class="text-right">{{ bool_to_icon($taxRate->affects_employees) }}</td>
                    <td class="text-right">{{ bool_to_icon($taxRate->affects_equipments) }}</td>
                    <td class="text-right">{{ bool_to_icon($taxRate->affects_fuels) }}</td>
                    <td class="text-right">{{ bool_to_icon($taxRate->affects_materials) }}</td>
                    <td class="table-action td-ignore">
                        <a href="{{ route('finances.tax-rates.edit', array($taxRate->id)) }}"><i class="fa fa-pencil"></i></a>
                        <a class="cursor-pointer delete-row" data-rcontext="{{{ $taxRate->id }}}"><i class="fa fa-trash-o"></i></a>
                    </td>
                </tr>
            @else
                <tr>
                    <td><strong>{{{ limit_name($taxRate->tax_name) }}}</strong></td>
                    <td class="text-right">{{{ nd_number_format($taxRate->tax_rate) }}}</td>
                    <td class="text-right">{{ bool_to_icon($taxRate->affects_employees) }}</td>
                    <td class="text-right">{{ bool_to_icon($taxRate->affects_equipments) }}</td>
                    <td class="text-right">{{ bool_to_icon($taxRate->affects_fuels) }}</td>
                    <td class="text-right">{{ bool_to_icon($taxRate->affects_materials) }}</td>
                    <td class="table-action td-ignore">
                        <a href="{{ action('App\UnTrashController@getFsTaxRates') }}?d={{ $taxRate->id }}" class="tooltips" data-toggle="tooltip" data-placement="left" title="Restoring will un-delete the tax rate">Restore</a>
                    </td>
                </tr>
            @endif
        @endforeach
        </tbody>
    </table>
</div>