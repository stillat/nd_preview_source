@extends('layouts.master')

@section('pageTitle')
<i class="fa ndcf-balance-02"></i> Tax Rates
@if(Input::get('search') == 1 && $foundResults)
	<span>Search Results</span>
@endif
@stop

@section('buttonBar')
	@if(Input::get('search') == 1 && $foundResults)
		<div class="btn-group n-mt-5">
			<a href="{{ action('App\Finances\Taxes\TaxesController@index') }}" class="btn btn-white"><i class="fa fa-th-list inline-icon"></i></a>
		</div>
	@endif
<div class="btn-group n-mt-5">
	<a href="{{ action('App\Search\SearchController@getFsTaxRatesSearchDialog') }}" data-toggle="modal" data-target="#modalHousing" class="btn btn-white"><i class="fa fa-search inline-icon"></i> Search Tax Rates</a>
    @include('partials.generic_export', ['export_data' => 'fs_tax_rates', 'title' => 'finance tax rates', 'margin' => '45px'])
    <a href="{{ action('App\Finances\Taxes\TaxesController@create') }}" class="btn btn-white nd-add"><i class="fa fa-plus inline-icon"></i> New Tax Rate</a>
</div>
@stop

@section('content')

<div class="row">
	
	<div class="col-sm-12">
		{{ $taxRates->appends(array_except(Input::query(), Paginator::getPageName()))->links() }}
	</div>

	<div class="col-sm-12">
		@include('finances.taxes.partials.table')
	</div>

	<div class="col-sm-12">
		{{ $taxRates->appends(array_except(Input::query(), Paginator::getPageName()))->links() }}
	</div>
</div>

@include('layouts.dialogs.remove', array('title' => 'Remove Tax Rate', 'action' => 'App\Finances\Taxes\TaxesController@destroy'))
@stop