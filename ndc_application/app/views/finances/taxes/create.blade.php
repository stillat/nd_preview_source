@extends('layouts.master')

@section('pageTitle')
<i class="fa fa-plus"></i> You are adding a new tax rate
@stop

@section('buttonBar')
<div class="btn-group pull-up-10">
	<a href="{{ Redirector::getRoute(route('finances.tax-rates.index')) }}" class="btn btn-danger ndc_form_leave">Cancel</a>
</div>
@stop

@section('content')

<div class="row">

	<div class="col-md-12">
		{{ Form::open(array('route' => 'finances.tax-rates.store', 'method' => 'post', 'class' => 'form-horizontal')) }}
		<div class="panel-primary panel-alt">
			<div class="panel-body">
				<div class="form-group is-required">
					<label class="col-sm-2 control-label" for="name">Name:</label>
					<div class="col-sm-8">
						<input type="text" id="name" name="tax_name" autofocus class="form-control mousetrap ndc_form_home_element {{{ user_form_size() }}}" value="{{{ Input::old('name', '') }}}">
						{{ Form::errorMsg('name') }}
					</div>
				</div>

				<div class="form-group is-required">
					<label class="col-sm-2 control-label" for="cost">Tax Rate:</label>
					<div class="col-sm-3">
						<div class="input-group">
							<span class="input-group-addon">%</span>
							<input type="number" min="0" step="any" id="cost" name="tax_rate" class="form-control text-right mousetrap ndc_form_trail_element {{{ user_form_size() }}}" value="{{{ nd_number_format(Input::old('cost', '0.00')) }}}">
						</div>
						{{ Form::errorMsg('cost') }}
					</div>
				</div>

				<fieldset>
					<div class="form-group">
						<div class="col-sm-offset-2 col-sm-10">
							<div class="checkbox">
								<label>
									<input type="checkbox" name="affects_all"> This tax rate affects everything
								</label>
							</div>
						</div>
					</div>
					<div class="form-group">
						<div class="col-sm-offset-2 col-sm-10">
							<div class="checkbox">
								<label>
									<input type="checkbox" name="affects_employees"> This tax rate affects employees
								</label>
							</div>
						</div>
						<div class="col-sm-offset-2 col-sm-10">
							<div class="checkbox">
								<label>
									<input type="checkbox" name="affects_equipments"> This tax rate affects equipment units
								</label>
							</div>
						</div>
						<div class="col-sm-offset-2 col-sm-10">
							<div class="checkbox">
								<label>
									<input type="checkbox" name="affects_fuels"> This tax rate affects fuels
								</label>
							</div>
						</div>
						<div class="col-sm-offset-2 col-sm-10">
							<div class="checkbox">
								<label>
									<input type="checkbox" name="affects_materials"> This tax rate affects materials
								</label>
							</div>
						</div>
					</div>
				</fieldset>

			</div>
			<div class="panel-footer">
				<div class="btn-group mr5 col-sm-offset-2">
					<button type="submit" id="ndc_form_defaultaction" value="addNew" name="submit" class="btn btn-primary" id="ndc_form_defaultaction">Save and add another tax rate</button>
					<button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
						<span class="caret"></span>
						<span class="sr-only">Toggle Dropdown</span>
					</button>
					<ul class="dropdown-menu" role="menu">
						<li><button type="submit" id="ndc_form_secondaryaction" value="return" name="submit" class="btn btn-white btn-md" id="ndc_form_secondaryaction">Save and return to previous action</button></li>
					</ul>
				</div>

				<div class="btn-group mr5">
					<button type="reset" id="ndc_form_reset" class="btn btn-default" id="ndc_form_reset">Reset</button>
				</div>
			</div>
		</div>
		{{ Form::close() }}
	</div>

</div>

@stop