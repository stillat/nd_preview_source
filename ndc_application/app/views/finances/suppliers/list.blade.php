@extends('layouts.master')

@section('pageTitle')
<i class="fa fa-shopping-cart"></i> Suppliers
@stop

@section('buttonBar')
<div class="btn-group">
	<a href="{{ action('App\Finances\SuppliersController@create') }}" class="btn btn-default"><i class="fa fa-plus inline-icon"></i> New Supplier</a>
</div>
@stop

@section('content')
<div class="row">
	
	<div class="col-sm-12">
		{{ $suppliers->links() }}
	</div>

	<div class="col-sm-12">
		@include('finances.suppliers.partials.table')
	</div>

	<div class="col-sm-12">
		{{ $suppliers->links() }}
	</div>

</div>

@include('layouts.dialogs.remove', array('title' => 'Remove Supplier', 'action' => 'App\Finances\SuppliersController@destroy'))
@stop