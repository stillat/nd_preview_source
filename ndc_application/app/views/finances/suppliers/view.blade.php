@extends('layouts.master')

@section('pageTitle')
<i class="fa fa-shopping-cart"></i> <strong>{{{ $supplier->code }}}</strong>: {{{ $supplier->name }}}
@stop

@section('buttonBar')
<div class="btn-group pull-up-10">
	{{ HTML::modelNaviateToolbar('finances.suppliers.show', $supplier->previous(), 'finances.suppliers.index', 'finances.suppliers.show', $supplier->next()) }}
	<a href="#" class="btn btn-white"><i class="fa fa-files-o inline-icon"></i> New Note</a>
</div>
@stop

@section('content')

<div class="row">
	<div class="col-sm-3">
		<img src="{{ app_url().'/'.$supplier->large_image }}" class="thumbnail img-responsive" alt="">
	</div>
	<div class="col-sm-9">
		<div class="profile-header">
			<h2 class="profile-name"><strong>{{{ $supplier->code }}}</strong>: {{{ $supplier->name }}}</h2>
			<div class="profile-location">
				<strong><i class="fa fa-barcode"></i> Code:</strong> {{{ $supplier->code }}}
			</div>
			<div class="profile-location">
				<strong>Active:</strong> {{{ client_active($supplier->active) }}}
			</div>
			<div class="profile-location">
				<strong><i class="fa fa-map-marker"></i> Addresses:</strong> {{{ $supplier->addresses->count() }}} (<a href="#">view all addresses</a>)
			</div>
			<div class="profile-location">
				<strong><i class="fa fa-phone"></i> Phone Numbers:</strong> {{{ $supplier->numbers->count() }}} (<a href="#">view all phone numbers</a>)
			</div>
			<div class="profile-location">
				<strong><i class="fa fa-files-o"></i> Notes:</strong> {{{ $supplier->notes->count() }}} (<a href="{{ route('finances.suppliers.notes.index', array($supplier->id)) }}">view all notes</a>)
			</div>
			<div class="mb20"></div>
			<a class="btn btn-success mr5"><i class="fa fa-files-o"></i> New Supplier Note</a>
			<a href="{{ route('finances.suppliers.edit', array($supplier->id)) }}" class="btn btn-white"><i class="fa fa-edit"></i> Update supplier</a>
		</div>

		<div class="panel panel-default">
			<div class="panel-heading">
				<h3 class="panel-title"><i class="fa fa-phone"></i> Primary Contact Information</h3>
			</div>
			<div class="panel-body">
				@if($supplier->primary_address_id !== 0)
				<address>
					{{{ $supplier->primaryAddress->address_line_1 }}}, {{{ $supplier->primaryAddress->address_line_2 }}}<br>
					{{{ $supplier->primaryAddress->city.' '.$supplier->primaryAddress->state.' '.$supplier->primaryAddress->zip_code }}}<br>
					{{{ $supplier->primaryAddress->country }}}
				</address>
				@else
				<p><i class="fa fa-map-marker"></i> You do not have a primary address set for this supplier.</p>
				@endif

				@if ($supplier->primary_phone_id !== 0)

				@else
				<p><i class="fa fa-phone"></i> You do not have a primary phone number set for this supplier.</p>
				@endif
			</div>
		</div>
	</div>
</div>

<div class="row">
	<div class="col-sm-12">
		<ul class="nav nav-tabs">
			<li class="active"><a href="#notes" data-toggle="tab"><strong><i class="fa fa-files-o"></i> Recent Notes</strong></a></li>
		</ul>
		<div class="tab-content mb30">
			<div class="tab-pane active widget-messaging" id="notes">
				<ul>
					<li>
						<small class="pull-right">Dec 10</small>
						<h4 class="sender">Jennier Lawrence</h4>
						<small>Lorem ipsum dolor sit amet...</small>
					</li>
					<li>
						<small class="pull-right">Dec 9</small>
						<h4 class="sender">Marsha Mellow</h4>
						<small>Lorem ipsum dolor sit amet...</small>
					</li>
					<li>
						<small class="pull-right">Dec 9</small>
						<h4 class="sender">Holly Golightly</h4>
						<small>Lorem ipsum dolor sit amet...</small>
					</li>
				</ul>
			</div>
		</div>
	</div>
</div>
@stop