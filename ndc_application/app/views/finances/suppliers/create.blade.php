@extends('layouts.master')

@section('pageTitle')
<i class="fa fa-plus"></i> You are adding a new supplier
@stop

@section('buttonBar')
<div class="btn-group pull-up-10">
	<a href="{{ Redirector::getRoute(route('finances.suppliers.index')) }}" class="btn btn-danger">Cancel</a>
</div>
@stop

@section('content')

<div class="row">

	<div class="col-md-12">
		{{ Form::open(array('route' => 'finances.suppliers.store', 'method' => 'post', 'class' => 'form-horizontal')) }}
		<div class="panel panel-primary">
			<div class="panel-body">
				<div class="form-group">
					<label class="col-sm-2 control-label">Code:</label>
					<div class="col-sm-2">
						<div class="input-group">
							<span class="input-group-addon"><i class="glyphicon glyphicon-barcode"></i></span>
							<input type="text" name="code" class="form-control text-right {{{ user_form_size() }}}" value="{{{ Input::old('code', '') }}}" autofocus>
							{{ Form::errorMsg('code') }}
						</div>
					</div>
				</div>

				<div class="form-group is-required">
					<label class="col-sm-2 control-label">Name:</label>
					<div class="col-sm-8">
						<input type="text" name="name" class="form-control {{{ user_form_size() }}}" value="{{{ Input::old('name', '') }}}">
						{{ Form::errorMsg('name') }}
					</div>
				</div>

				<div class="form-group">
					<label class="col-sm-2 control-label"></label>
					<div class="col-sm-6">
						<div class="checkbox block"><label>{{ Form::checkbox('active', 1,Input::old('active', true)) }} Active</label></div>
					</div>
				</div>

			</div><!-- panel-body -->
			<div class="panel-footer">
				<div class="btn-group mr5">
					<button type="submit" value="addNew" name="submit" class="btn btn-primary">Save and add another supplier</button>
					<button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
						<span class="caret"></span>
						<span class="sr-only">Toggle Dropdown</span>
					</button>
					<ul class="dropdown-menu" role="menu">
						<li><button type="submit" value="return" name="submit" class="btn btn-white btn-md">Save and return to previous action</button></li>
					</ul>
				</div>

				<div class="btn-group mr5">					
					<button type="reset" class="btn btn-default">Reset</button>
				</div>
			</div><!-- panel-footer -->
		</div><!-- panel-default -->
		{{ Form::close() }}
	</div>

</div>
@stop