<div class="table-responsive">
	<table class="table table-hover inspector-holder">
		<thead>
			<tr>
				<th>Code</th>
				<th>Name</th>
			</tr>
		</thead>
		<tbody>
			@foreach($suppliers as $supplierID => $supplier)
			<tr>
				<td><strong>{{{ $supplier->code }}}</strong></td>
				<td class="td-ignore">{{{ $supplier->name }}} <a class="cursor-pointer" data-toggle="replaceable" data-replacefrom="{{{ $supplier->name }}}" data-copyto="[name='name']"><i class="fa fa-copy"></i> <span class="sr-only">Copy value</span></a></td>
			</tr>
			@endforeach
		</tbody>
	</table>
</div>

<a href="{{ action('App\Inspector\InspectorController@getClear', array('fs_suppliers')) }}" data-rhistory="hide" data-target="#ndLoadExternal" data-rcontainer="#ajaxContentPanel" data-toggle="remote">Clear this list</a>