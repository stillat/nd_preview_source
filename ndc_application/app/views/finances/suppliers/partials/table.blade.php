<div class="table-responsive">
	<table class="table table-hover mb30 table-fixed">
		<thead>
			<tr>
				<th class="wd-100">Code</th>
				<th>Name</th>
				<th>Created</th>
				<th>Last Updated</th>
				<th class="wd-100"></th>
			</tr>
		</thead>
		<tbody>
			@foreach($suppliers as $supplier)
			<tr>
				<td>
					<a href="{{ route('finances.suppliers.show', array($supplier->id)) }}"><strong>{{{ $supplier->code }}}</strong></a>
				</td>
				<td>
					<a href="{{ route('finances.suppliers.show', array($supplier->id)) }}">{{{ $supplier->name }}}</a>
				</td>
				<td>
					<a href="{{ route('finances.suppliers.show', array($supplier->id)) }}">{{{ mysql_date_to_client($supplier->created_at) }}}</a>
				</td>
				<td>
					<a href="{{ route('finances.suppliers.show', array($supplier->id)) }}">{{{ if_null_then(mysql_date_to_client($supplier->updated_at), 'Not available') }}}</a>
				</td>
				<td class="table-action td-ignore">
					<a href="{{ route('finances.suppliers.edit', array($supplier->id)) }}"><i class="fa fa-pencil"></i></a>
					<a class="cursor-pointer delete-row" data-rcontext="{{{ $supplier->id }}}"><i class="fa fa-trash-o"></i></a>
				</td>
			</tr>
			@endforeach
		</tbody>
	</table>
</div>