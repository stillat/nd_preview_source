@extends('layouts.master')

@section('pageTitle')
<i class="fa fa-pencil"></i> <strong>{{{ $supplier->code }}}</strong> - {{{ $supplier->name }}}
@stop

@section('buttonBar')
<div class="btn-group pull-up-10">
	{{ HTML::modelNaviateToolbar('finances.suppliers.edit', $supplier->previous(), 'finances.suppliers.index', 'finances.suppliers.edit', $supplier->next()) }}
</div>
<div class="btn-group pull-up-10">
	<a href="{{ Redirector::getRoute(route('finances.suppliers.index')) }}" class="btn btn-danger">Cancel</a>
</div>
@stop

@section('content')

<div class="row">
	<div class="col-md-12">
		{{ Form::open(array('route' => array('finances.suppliers.update', $supplier->id), 'files' => true, 'method' => 'put', 'class' => 'form-horizontal')) }}
		<div class="panel panel-success">
			<div class="panel-body">
				<div class="form-group">
					<label class="col-sm-2 control-label">Code:</label>
					<div class="col-sm-2">
						<div class="input-group">
							<span class="input-group-addon"><i class="glyphicon glyphicon-barcode"></i></span>
							<input type="text" name="code" class="form-control text-right {{{ user_form_size() }}}" value="{{{ Input::old('code', $supplier->code) }}}" autofocus>
							{{ Form::errorMsg('code') }}
						</div>
					</div>
				</div>

				<div class="form-group is-required">
					<label class="col-sm-2 control-label">Name:</label>
					<div class="col-sm-8">
						<input type="text" name="name" class="form-control {{{ user_form_size() }}}" value="{{{ Input::old('name', $supplier->name) }}}">
						{{ Form::errorMsg('name') }}
					</div>
				</div>

				<div class="form-group">
					<label class="col-sm-2 control-label"></label>
					<div class="col-sm-6">
						<div class="checkbox block"><label>{{ Form::checkbox('active', 1, Input::old('active', $supplier->active)) }} Active</label></div>
					</div>
				</div>

				<div class="form-group">
					<label class="col-sm-2 control-label">Photo</label>
					<div class="col-sm-10">
						<div class="fileupload fileupload-new" data-provides="fileupload">
							<div class="input-append">
								<div class="uneditable-input">
									<i class="glyphicon glyphicon-picture fileupload-exists"></i>
									<span class="fileupload-preview"></span>
								</div>
								<span class="btn btn-default btn-file">
									<span class="fileupload-new">Select photo</span>
									<span class="fileupload-exists">Change</span>
									<input type="file" name="photo" />
								</span>
								<a href="#" class="btn btn-default fileupload-exists" data-dismiss="fileupload">Remove</a>
							</div>
						</div>

						{{ Form::errorMsg('photo') }}
					</div>
				</div>

			</div>
			<div class="panel-footer">
				<div class="btn-group mr5">
					<button type="submit" value="return" name="submit" class="btn btn-info">Update this supplier</button>
				</div>
				<div class="btn-group mr5">
					<button type="reset" class="btn btn-default">Reset</button>
				</div>
			</div>
		</div>
	</div>
</div>
@stop