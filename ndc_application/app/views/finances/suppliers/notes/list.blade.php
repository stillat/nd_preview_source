@extends('layouts.master')

@section('pageTitle')
<i class="fa fa-files-o"></i> <strong>{{{ $supplier->code }}}</strong>: {{{ $supplier->name }}}
@stop

@section('buttonBar')
<div class="btn-group pull-up-10">
	<a href="{{ route('finances.suppliers.notes.create', array($supplier->id)) }}" class="btn btn-white"><i class="fa fa-files-o inline-icon"></i> New Note</a>
</div>
@stop

@section('content')

<div class="row hidden-xs">
	<div class="col-sm-1">
		<img src="{{ app_url().'/'.$supplier->thumbnail_image }}" class="thumbnail img-responsive" alt="">
	</div>
	<div class="col-sm-11">
		<div class="profile-header">
			<h2 class="profile-name"><strong>{{{ $supplier->code }}}</strong>: {{{ $supplier->name }}}</h2>
			<div class="profile-location">
				<strong><i class="fa fa-barcode"></i> Code:</strong> {{{ $supplier->code }}}
			</div>
			<div class="profile-location">
				<strong>Active:</strong> {{{ client_active($supplier->active) }}}
			</div>
		</div>
	</div>
</div>

<div class="row">
	<div class="col-sm-12">
		<div class="panel panel-default panel-alt widget-messaging">
			<div class="panel-body">
				<ul>
					@foreach($notes as $note)
					<li>
						<small class="pull-right">{{{ mysql_date_to_client($note->created_at) }}}</small>
						<a href="{{ route('finances.suppliers.notes.show', array($supplier->id, $note->id)) }}"><p>{{ strip_tags($note->note, '') }}</p></a>
					</li>
					@endforeach
				</ul>
			</div>
			<div class="panel-footer">
				{{ $notes->links() }}
			</div>
		</div>

	</div>
</div>

@stop