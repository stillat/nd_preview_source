@extends('layouts.master')

@section('pageTitle')
<i class="fa fa-files-o"></i> New note for '<strong>{{{ $supplier->code }}}</strong>: {{{ $supplier->name }}}'
@stop

@section('buttonBar')
<div class="btn-group pull-up-10">
	<a href="{{ Redirector::getRoute(route('finances.suppliers.notes.index')) }}" class="btn btn-danger">Cancel</a>
</div>
@stop

@section('content')

<div class="row">
	
	<div class="col-sm-12">
		
		{{ Form::open(array('route' => array('finances.suppliers.notes.store', $supplier->id), 'method' => 'post', 'class' => 'form-horizontal')) }}
		<div class="panel panel-success">
			<div class="panel-heading">
				<h4 class="panel-title">You are adding a new supplier.</h4>
			</div>
			<div class="panel-body">
				<textarea id="wysiwyg" name="note" placeholder="Enter note here..." class="form-control" rows="10"></textarea>
			</div>
			<div class="panel-footer">
				<div class="btn-group mr5">
					<button type="submit" value="addNew" name="submit" class="btn btn-success">Save and add another note</button>
					<button type="button" class="btn btn-success dropdown-toggle" data-toggle="dropdown">
						<span class="caret"></span>
						<span class="sr-only">Toggle Dropdown</span>
					</button>
					<ul class="dropdown-menu" role="menu">
						<li><button type="submit" value="return" name="submit" class="btn btn-white btn-md">Save and return to previous action</button></li>
					</ul>
				</div>

				<div class="btn-group mr5">					
					<button type="reset" class="btn btn-default">Reset</button>
				</div>
			</div>
		</div>
		{{ Form::close() }}

	</div>

</div>

@stop

@section('additionalScripts')
<script src="{{ app_url() }}/assets/js/wysihtml5-0.3.0.min.js"></script>
<script src="{{ app_url() }}/assets/js/bootstrap-wysihtml5.js"></script>
<script>
	$(document).ready(function()
	{
		$('#wysiwyg').wysihtml5({
			stylesheets: false,
			image: false,
			link: false
		});
	});
</script>
@stop