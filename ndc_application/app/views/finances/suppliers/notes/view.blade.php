@extends('layouts.master')

@section('pageTitle')
<i class="fa fa-files-o"></i> <strong>{{{ $supplier->code }}}</strong>: {{{ $supplier->name }}}
@stop

@section('content')

<div class="row hidden-xs">
	<div class="col-sm-1">
		<img src="{{ app_url().'/'.$supplier->thumbnail_image }}" class="thumbnail img-responsive" alt="">
	</div>
	<div class="col-sm-11">
		<div class="profile-header">
			<h2 class="profile-name"><strong>{{{ $supplier->code }}}</strong>: {{{ $supplier->name }}}</h2>
			<div class="profile-location">
				<strong><i class="fa fa-barcode"></i> Code:</strong> {{{ $supplier->code }}}
			</div>
			<div class="profile-location">
				<strong>Active:</strong> {{{ client_active($supplier->active) }}}
			</div>
		</div>
	</div>
</div>

<div class="row">
	
	<div class="col-sm-12">

		<div class="panel panel-default">
			<div class="panel-body">

				<div class="pull-right">
					<div class="btn-group mr10">
						{{ Form::open(array('route' => array('finances.suppliers.notes.destroy', $supplier->id), 'method' => 'delete')) }}
						<input type="hidden" name="noteid" value="{{{ $note->id }}}">
						<button class="btn btn-white tooltips" type="submit" data-toggle="tooltip" title="" data-original-title="Delete"><i class="glyphicon glyphicon-trash"></i></button>
						{{ Form::close() }}
					</div>

				</div><!-- pull-right -->

				<div class="btn-group mr10">
					<h4><span class="media-meta pull-right"><strong>Created</strong>: {{{ mysql_date_to_client($note->created_at) }}} <strong>Updated</strong>: {{{ if_null_then(mysql_date_to_client($note->updated_at), 'Not available') }}}</span></h4>
				</div>

				<div class="read-panel">

					{{ st($note->note) }}

				</div>

			</div>
		</div>

	</div>

</div>

@stop