@extends('layouts.master')

@section('pageTitle')
    <i class="fa fa-pencil"></i> <strong>{{{ limit_code($account->code) }}}</strong> - {{{ limit_name($account->name) }}}
@stop

@section('buttonBar')
    <div class="btn-group pull-up-10">
        {{ HTML::modelNaviateToolbar('finances.accounts.edit', $account->previous(), 'finances.accounts.index', 'finances.accounts.edit', $account->next()) }}
    </div>
    <div class="btn-group pull-up-10">
        <a href="{{ Redirector::getRoute(route('finances.accounts.index')) }}" class="btn btn-danger">Cancel</a>
    </div>
@stop


@section('content')

    <div class="row">

        <div class="col-md-12">
            {{ Form::open(array('route' => array('finances.accounts.update', $account->id), 'method' => 'put', 'class' => 'form-horizontal')) }}
            <div class="panel-primary panel-alt">
                <div class="panel-body">
                    <fieldset>
                        <legend>General Account Information</legend>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Category</label>

                            <div class="col-sm-5">
                                {{ Form::select('category', $categories, Input::old('category', $account->category_id), array('class' => 'form-control chosen-select', 'data-placeholder' => 'Choose a category')) }}
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Code:</label>
                            <div class="col-sm-4">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="glyphicon glyphicon-barcode"></i></span>
                                    <input type="text" name="code"
                                           class="form-control text-right {{{ user_form_size() }}}"
                                           value="{{{ Input::old('code', $account->code) }}}">
                                </div>
                                {{ Form::errorMsg('code') }}
                            </div>
                        </div>

                        <div class="form-group is-required">
                            <label class="col-sm-2 control-label">Name:</label>

                            <div class="col-sm-8">
                                <input type="text" name="name" class="form-control {{{ user_form_size() }}}"
                                       value="{{{ Input::old('name', $account->name) }}}" autofocus>
                                {{ Form::errorMsg('name') }}
                            </div>
                        </div>

                        <div class="form-group is-required">
                            <label class="col-sm-2 control-label">Description:</label>

                            <div class="col-sm-8">
                                <input type="text" name="description" class="form-control {{{ user_form_size() }}}"
                                       value="{{{ Input::old('description', $account->description) }}}">
                                {{ Form::errorMsg('description') }}
                            </div>
                        </div>
                        <div class="ckbox ckbox-default col-sm-offset-2">
                            {{ Form::checkbox('cash_account', 1, Input::old('cash_account', $account->is_cash_account), ['id' => 'cash_account']) }}
                            <label for="cash_account">
                                This is a cash account <a href="#" data-toggle="popover" data-trigger="hover" title="Cash Accounts" data-content="Select this option if the account you are creating is a cash account. Cash accounts are generally represent accounts that you do not have control over, such as a partner's business."><i class="fa fa-question-circle"></i> What's this?</a>
                            </label>
                        </div>
                    </fieldset>
                    <fieldset>
                        <legend>Contact Information</legend>
                        <div class="form-group is-required">
                            <label class="col-sm-2 control-label" for="address_line_1">Address Line 1:</label>

                            <div class="col-sm-6">
                                <input type="text" id="address_line_1" name="address_line_1"
                                       class="form-control mousetrap {{{ user_form_size() }}}"
                                       value="{{{ Input::old('address_line_1', $account->address_line_1) }}}">
                                {{ Form::errorMsg('address_line_1') }}
                            </div>
                        </div>
                        <div class="form-group is-required">
                            <label class="col-sm-2 control-label" for="address_line_2">Address Line 2:</label>

                            <div class="col-sm-6">
                                <input type="text" id="address_line_2" name="address_line_2"
                                       class="form-control mousetrap {{{ user_form_size() }}}"
                                       value="{{{ Input::old('address_line_2', $account->address_line_2) }}}">
                                {{ Form::errorMsg('address_line_2') }}
                            </div>
                        </div>
                        <div class="form-group is-required">
                            <label class="col-sm-2 control-label" for="city">City:</label>

                            <div class="col-sm-4">
                                <input type="text" id="city" name="city"
                                       class="form-control mousetrap {{{ user_form_size() }}}"
                                       value="{{{ Input::old('city', $account->city) }}}">
                                {{ Form::errorMsg('city') }}
                            </div>
                        </div>
                        <div class="form-group is-required">
                            <label class="col-sm-2 control-label" for="state">State:</label>

                            <div class="col-sm-4">
                                <input type="text" id="state" name="state"
                                       class="form-control mousetrap {{{ user_form_size() }}}"
                                       value="{{{ Input::old('state', $account->state) }}}">
                                {{ Form::errorMsg('state') }}
                            </div>
                        </div>
                        <div class="form-group is-required">
                            <label class="col-sm-2 control-label" for="country">Country:</label>

                            <div class="col-sm-4">
                                <input type="text" id="country" name="country"
                                       class="form-control mousetrap {{{ user_form_size() }}}"
                                       value="{{{ Input::old('country', $account->country) }}}">
                                {{ Form::errorMsg('country') }}
                            </div>
                        </div>
                        <div class="form-group is-required">
                            <label class="col-sm-2 control-label" for="zip">Zip:</label>

                            <div class="col-sm-2">
                                <input type="text" id="zip" name="zip"
                                       class="form-control mousetrap {{{ user_form_size() }}}"
                                       value="{{{ Input::old('zip', $account->zip) }}}">
                                {{ Form::errorMsg('zip') }}
                            </div>
                        </div>
                    </fieldset>


                </div>
                <div class="panel-footer col-sm-offset-2">
                    <div class="btn-group mr5">
                        <button type="submit" value="return" name="submit" class="btn btn-primary">Update this account
                        </button>
                    </div>
                    <div class="btn-group mr5">
                        <button type="reset" class="btn btn-default">Reset</button>
                    </div>
                </div>
            </div>
            {{ Form::close() }}
        </div>

    </div>
@stop