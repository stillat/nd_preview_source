@extends('layouts.master')

@section('pageTitle')
    <i class="fa ndcf-accounts-book"></i> Accounts
    @if(Input::get('search') == 1 && $foundResults)
        <span>Search Results</span>
    @else
        @if (isset($categories[$currentCategory]))
            <small>{{{ $categories[$currentCategory] }}}</small>
        @else
            <small>All Accounts</small>
        @endif
    @endif

@stop

@section('buttonBar')
    <div <?php if (count($categories) > 2) {
        echo "class='n-mt-5'";
    } ?>>
        @if((Input::get('search') != 1 && !$foundResults) && count($categories) > 2)
            <div class="btn-group n-mt-5" style="margin-top:-7px;">
                <div style="width: 200px; display: inline-block; margin-left: 5px;">
                    {{ Form::select('category', $categories, $currentCategory, array('class' => 'form-control nd-filter n-mt-5', 'style' => 'margin-top: 10px;', 'id' => 'categorychooser','data-placeholder' => 'Choose a category')) }}
                </div>
            </div>
        @endif
        <div class="btn-group n-mt-5">
            @if(Input::get('search') == 1 && $foundResults)
                <a href="{{ action('App\Finances\AccountsController@index') }}" class="btn btn-white"><i class="fa fa-th-list inline-icon"></i></a>
            @endif
            <a href="{{ action('App\Search\SearchController@getFsAccountsSearchDialog') }}" data-toggle="modal"
               data-target="#modalHousing" class="btn btn-white"><i class="fa fa-search inline-icon"></i> Search
                Accounts</a>
            @include('partials.generic_export', ['export_data' => 'fs_accounts', 'title' => 'finance accounts', 'margin' => '41px'])
        </div>
        <div class="btn-group n-mt-5">
            <a href="{{ action('App\Finances\AccountsController@create') }}" class="btn btn-white nd-add"><i
                        class="fa fa-plus inline-icon"></i> New Account</a>
            <button type="button" class="btn btn-white dropdown-toggle" data-toggle="dropdown">
                <span class="caret"></span>
                <span class="sr-only">Toggle Dropdown</span>
            </button>
            <ul class="dropdown-menu" role="menu" style="margin-left: -33px;">
                <li><a href="{{ action('App\Finances\AccountsCategoryController@create') }}"><i
                                class="fa fa-tag inline-icon"></i> New Account Category</a></li>
            </ul>
        </div>
    </div>
@stop

@section('content')

    <div class="row">

        <div class="col-sm-12">
            {{ $accounts->appends(Input::all())->links() }}
        </div>

        <div class="col-sm-12">
            @include('finances.accounts.partials.table')
        </div>

        <div class="col-sm-12">
            {{ $accounts->appends(Input::all())->links() }}
        </div>

    </div>

    @include('layouts.dialogs.remove', array('title' => 'Remove Account', 'action' => 'App\Finances\AccountsController@destroy'))
@stop

@section('additionalScripts')
    <script src="{{{ app_url() }}}/assets/js/finances/accounts.js" defer></script>
@stop