{{ Form::open(['action' => 'App\UtilityController@postApplyBatchTransactionStatus']) }}

@yield('customTransactionFields')
<div class="row">
    <div class="col-sm-12">
        <ul class="nav nav-tabs">
            <li class="active"><a href="#home" data-toggle="tab"><strong><?php if (isset($transactionTabTitle)) {
    echo $transactionTabTitle;
} else {
    echo 'Recent Transactions';
} ?></strong></a></li>
            <li class="dropdown pull-right">
                <a href="#" data-toggle="dropdown" class="dropdown-toggle">
                    <i class="fa fa-download inline-icon"></i> Export <span class="caret"></span>
                </a>
                @if(isset($account))
                    <ul role="menu" class="dropdown-menu pull-right">
                        <li>
                            <a href="{{ export_csv('fs_accounts_transactions', 'list', ['account' => $account->id]).$transactionRestrictionString }}">Export
                                this page</a></li>
                        <li><a href="{{ export_csv('fs_accounts_transactions', 'list', ['account' => $account->id]) }}">Export
                                all transactions</a></li>
                    </ul>
                @endif
                @if(isset($invoice))
                    <ul role="menu" class="dropdown-menu pull-right">
                        <li>
                            <a href="{{ export_csv('fs_invoice_payments', 'list', ['invoice' => $invoice->id]).$transactionRestrictionString }}">Export
                                this page</a></li>
                        <li><a href="{{ export_csv('fs_invoice_payments', 'list', ['invoice' => $invoice->id]) }}">Export
                                all payments</a></li>
                    </ul>
                @endif
            </li>
        </ul>
        <div class="tab-content mb30">
            <div class="tab-pane active" id="home">
                @if (count($transactions->all()) > 10)
                    <div class="row">
                        <div class="col-sm-12">
                            {{ $transactions->links() }}
                        </div>
                    </div>
                @endif
                <div class="table-responsive">
                    <table class="table table-hover table-striped mb30 work_table" id="transactionsTable">
                        <thead>
                        <tr>
                            <th class="hidden-print" style="width:46px;">
                                <div class="ckbox ckbox-primary">
                                    <input type="checkbox" id="batchStatusToggleAll">
                                    <label for="batchStatusToggleAll"></label>
                                </div>
                            </th>
                            <th class="wd-100">
                                Batch
                                <a data-content="We automatically generate batch numbers for every transaction to keep things organized. Each transaction will have exactly two entries; both with the same batch number."
                                   data-placement="top" data-toggle="popover" data-container="body"
                                   class="popovers cursor-pointer" data-original-title="" title="">
                                    <i class="fa fa-question"></i>
                                </a>
                            </th>
                            <th>Date</th>
                            <th>Description</th>
                            <th>From</th>
                            <th>Paid To</th>
                            <th class="wd-100">Status</th>
                            <th class="text-right">Amount</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($transactions->all() as $transaction)
                            <tr>
                                <td class="hidden-print">
                                    <div class="ckbox ckbox-primary">
                                        <input type="checkbox" class="up_status"
                                               id="batchStatus{{{ $transaction->id }}}" name="transaction[]"
                                               value="{{{ $transaction->id }}}"
                                               data-transaction="{{{ $transaction->id }}}">
                                        <label for="batchStatus{{{ $transaction->id }}}"></label>
                                    </div>
                                </td>
                                <td data-html="true"
                                    data-content="{{ substr(e($transaction->batch), 0, 25).' '.substr(e($transaction->batch), -25, 19).' <strong>'.substr(e($transaction->batch), -6).'</strong>' }}"
                                    data-placement="right" data-toggle="popover" data-container="body"
                                    class="popovers cursor-pointer" data-original-title="" title="">
                                    <strong>{{{ substr($transaction->batch, -6) }}}</strong></td>
                                <td>{{{ mysql_date_to_client($transaction->created_at) }}}</td>
                                <td>{{{ $transaction->description }}}</td>
                                <td>
                                    <a href="{{{ action('App\Finances\AccountsController@show', array($transaction->beginAccount->id)) }}}">{{{ $transaction->beginAccount->name }}}</a>
                                </td>
                                <td>
                                    <a href="{{{ action('App\Finances\AccountsController@show', array($transaction->destination->id)) }}}">{{{ $transaction->destination->name }}}</a>
                                </td>
                                <td>{{ transaction_label($transaction->transaction_status) }}</td>
                                <?php

                                $classContext = '';

                                if (isset($account)) {
                                    if ($transaction->beginAccount->id == $account->id) {
                                        $classContext = 'danger';
                                    } elseif ($transaction->destination->id == $account->id) {
                                        $classContext = 'success';
                                    }
                                }

                                ?>
                                <td class="{{ $classContext }} text-right">{{{ nd_number_format($transaction->amount) }}}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        {{ $transactions->links() }}
                    </div>
                    <div class="col-sm-12">
                        <div class="form-group">
                            <label for="batchStatusSelector" class="col-sm-1 control-label" style="margin-top: 10px;">Batch
                                Status:</label>

                            <div class="col-sm-2">
                                {{ Form::select('transaction_status', $transactionStatuses, 1, ['class' => 'form-control', 'id' => 'batchStatusSelector']) }}
                            </div>
                            <button type="submit" class="btn btn-white" style="display: inline-block;">Apply</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
{{ Form::close() }}