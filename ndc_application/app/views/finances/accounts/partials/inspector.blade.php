<div class="table-responsive">
	<table class="table table-hover inspector-holder">
		<thead>
			<tr>
				<th>Code</th>
				<th>Name</th>
				<th>Description</th>
			</tr>
		</thead>
		<tbody>
			@foreach($accounts as $accountID => $account)
			<tr>
				<td><strong>{{{ $account->code }}}</strong></td>
				<td class="td-ignore">{{{ $account->name }}} <a class="cursor-pointer" data-toggle="replaceable" data-replacefrom="{{{ $account->name }}}" data-copyto="[name='name']"><i class="fa fa-copy"></i> <span class="sr-only">Copy value</span></a></td>
				<td class="td-ignore">{{{ $account->description }}} <a class="cursor-pointer" data-toggle="replaceable" data-replacefrom="{{{ $account->description }}}" data-copyto="[name='description']"><i class="fa fa-copy"></i> <span class="sr-only">Copy value</span></a></td>
			</tr>
			@endforeach
		</tbody>
	</table>
</div>

<a href="{{ action('App\Inspector\InspectorController@getClear', array('fs_accounts')) }}" data-rhistory="hide" data-target="#ndLoadExternal" data-rcontainer="#ajaxContentPanel" data-toggle="remote">Clear this list</a>