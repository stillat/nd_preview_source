<div class="table-responsive">
    <table class="table table-hover table-striped mb30 table-fixed work_table">
        <thead>
        <tr>
            <th style="width: 5px;max-width:5px;"></th>
            <th class="wd-200">Code/Account #</th>
            <th>Name</th>
            <th>Category</th>
            <th>Description</th>
            <th class="text-right">Spent to Date</th>
            <th class="text-right">Balance</th>
            <th class="wd-100 hidden-print text-right">
                <a href="{{{ action('App\Search\SortingController@getFinanceAccounts') }}}" data-toggle="modal"
                   data-target="#modalHousing"><i class="fa fa-sort"></i>
                    <small>Sorting</small>
                </a>
            </th>
        </tr>
        </thead>
        <tbody>
        @foreach($accounts as $account)
            @if ($account->deleted_at == null)
                <tr>
                    <td style="background-color: {{{ $account->color }}}; border-right: 1px solid rgb(218, 218, 218);" class="text-center">
                        <a href="{{ url('finances/accounts?sort='.$account->category_id) }}" style="width:100%;height: 100%;text-decoration: none;">&nbsp;&nbsp;</a>
                    </td>
                    <td>
                        <a href="{{ route('finances.accounts.show', array($account->id)) }}"><strong>{{{ limit_description($account->prefix.'-'.$account->code) }}}</strong></a>
                    </td>
                    <td>
                        <a href="{{ route('finances.accounts.show', array($account->id)) }}">{{{ limit_name($account->name) }}}</a>
                    </td>
                    <td>
                        <a href="{{ url('finances/accounts?sort='.$account->category_id) }}">{{{ limit_name($account->category) }}}</a>
                    </td>
                    <td>
                        <a href="{{ route('finances.accounts.show', array($account->id)) }}">{{{ if_null_then_na(limit_description($account->description)) }}}</a>
                    </td>
                    <td class="text-right">
                        <a href="{{ route('finances.accounts.show', array($account->id)) }}">{{{ nd_number_format(-1 * $account->balance) }}}</a>
                    </td>
                    <td class="text-right">
                        <a href="{{ route('finances.accounts.show', array($account->id)) }}">{{{ nd_number_format($account->balance) }}}</a>
                    </td>
                    <td class="table-action td-ignore">
                        <a href="{{ route('finances.accounts.edit', array($account->id)) }}"><i class="fa fa-pencil"></i></a>
                        <a class="cursor-pointer delete-row" data-rcontext="{{{ $account->id }}}"><i class="fa fa-trash-o"></i></a>
                    </td>
                </tr>
            @else
                <tr>
                    <td style="background-color: {{{ $account->color }}}; border-right: 1px solid rgb(218, 218, 218);" class="text-center">
                        <a style="width:100%;height: 100%;text-decoration: none;">&nbsp;&nbsp;</a>
                    </td>
                    <td>
                        <strong>{{{ limit_description($account->prefix.'-'.$account->code) }}}</strong>
                    </td>
                    <td>
                        {{{ limit_name($account->name) }}}
                    </td>
                    <td>
                        {{{ limit_name($account->category) }}}
                    </td>
                    <td>
                        {{{ if_null_then_na(limit_description($account->description)) }}}
                    </td>
                    <td class="text-right">
                        {{{ nd_number_format(-1 * $account->balance) }}}
                    </td>
                    <td class="text-right">
                        {{{ nd_number_format($account->balance) }}}
                    </td>
                    <td class="table-action td-ignore">
                        <a href="{{ action('App\UnTrashController@getFsAccounts') }}?d={{ $account->id }}" class="tooltips" data-toggle="tooltip" data-placement="left" title="Restoring will un-delete the finance account">Restore</a>
                    </td>
                </tr>
            @endif
        @endforeach
        </tbody>
    </table>
</div>