@extends('layouts.modal')

@section('header')
<i class="glyphicon glyphicon-transfer"></i> Transfer funds from this account
@stop

@section('body')
<div class="panel panel-default">
	<div class="panel-body panel-body-nopadding">
		<div class="form-group">
			<label class="col-sm-3 control-label">Service Account</label>
			<div class="col-sm-5">
				
			</div>
		</div>
	</div>
	<div class="panel-footer">
		<button type="submit" class="btn btn-primary">Submit</button>
	</div>
</div>

@stop