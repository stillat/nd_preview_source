@extends('layouts.master')
@section('pageTitle')
    <i class="fa fa-plus"></i> You are adding a new account
@stop
@section('buttonBar')
    <div class="btn-group pull-up-10">
        <a href="{{ Redirector::getRoute(route('finances.accounts.index')) }}" class="btn btn-danger ndc_form_leave">Cancel</a>
    </div>
@stop
@section('content')

    <div class="row">
        <div class="col-md-12">
            {{ Form::open(array('route' => 'finances.accounts.store', 'method' => 'post', 'class' => 'form-horizontal')) }}
            <div class="panel-primary panel-alt">
                <div class="panel-body">
                    <fieldset>
                        <legend>General Account Information</legend>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Category</label>

                            <div class="col-sm-5">
                                {{ Form::select('category', $categories, Input::old('category'), array('class' => 'form-control chosen-select mousetrap', 'data-placeholder' => 'Choose a category')) }}
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Code:</label>
                            <div class="col-sm-4">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="glyphicon glyphicon-barcode"></i></span>
                                    <input type="text" name="code"
                                           class="form-control text-right mousetrap ndc_form_home_element {{{ user_form_size() }}}"
                                           value="{{{ Input::old('code', '') }}}" autofocus>
                                </div>
                                {{ Form::errorMsg('code') }}
                            </div>
                        </div>
                        <div class="form-group is-required">
                            <label class="col-sm-2 control-label">Name:</label>

                            <div class="col-sm-8">
                                <input type="text" name="name" class="form-control mousetrap {{{ user_form_size() }}}"
                                       value="{{{ Input::old('name', '') }}}">
                                {{ Form::errorMsg('name') }}
                            </div>
                        </div>
                        <div class="form-group is-required">
                            <label class="col-sm-2 control-label">Description:</label>

                            <div class="col-sm-8">
                                <input type="text" name="description"
                                       class="form-control mousetrap ndc_form_trail_element {{{ user_form_size() }}}"
                                       value="{{{ Input::old('description', '') }}}">
                                {{ Form::errorMsg('description') }}
                            </div>
                        </div>
                        <div class="ckbox ckbox-default col-sm-offset-2">
                            {{ Form::checkbox('cash_account', 1, Input::old('cash_account', false), ['id' => 'cash_account']) }}
                            <label for="cash_account">
                                This is a cash account <a href="#" data-toggle="popover" data-trigger="hover" title="Cash Accounts" data-content="Select this option if the account you are creating is a cash account. Cash accounts are generally represent accounts that you do not have control over, such as a partner's business."><i class="fa fa-question-circle"></i> What's this?</a>
                            </label>
                        </div>
                    </fieldset>
                    <fieldset>
                        <legend>Contact Information</legend>
                        <div class="form-group is-required">
                            <label class="col-sm-2 control-label" for="address_line_1">Address Line 1:</label>

                            <div class="col-sm-6">
                                <input type="text" id="address_line_1" name="address_line_1"
                                       class="form-control mousetrap {{{ user_form_size() }}}"
                                       value="{{{ Input::old('address_line_1', '') }}}">
                                {{ Form::errorMsg('address_line_1') }}
                            </div>
                        </div>
                        <div class="form-group is-required">
                            <label class="col-sm-2 control-label" for="address_line_2">Address Line 2:</label>

                            <div class="col-sm-6">
                                <input type="text" id="address_line_2" name="address_line_2"
                                       class="form-control mousetrap {{{ user_form_size() }}}"
                                       value="{{{ Input::old('address_line_2', '') }}}">
                                {{ Form::errorMsg('address_line_2') }}
                            </div>
                        </div>
                        <div class="form-group is-required">
                            <label class="col-sm-2 control-label" for="city">City:</label>

                            <div class="col-sm-4">
                                <input type="text" id="city" name="city"
                                       class="form-control mousetrap {{{ user_form_size() }}}"
                                       value="{{{ Input::old('city', '') }}}">
                                {{ Form::errorMsg('city') }}
                            </div>
                        </div>
                        <div class="form-group is-required">
                            <label class="col-sm-2 control-label" for="state">State:</label>

                            <div class="col-sm-4">
                                <input type="text" id="state" name="state"
                                       class="form-control mousetrap {{{ user_form_size() }}}"
                                       value="{{{ Input::old('state', '') }}}">
                                {{ Form::errorMsg('state') }}
                            </div>
                        </div>
                        <div class="form-group is-required">
                            <label class="col-sm-2 control-label" for="country">Country:</label>

                            <div class="col-sm-4">
                                <input type="text" id="country" name="country"
                                       class="form-control mousetrap {{{ user_form_size() }}}"
                                       value="{{{ Input::old('country', '') }}}">
                                {{ Form::errorMsg('country') }}
                            </div>
                        </div>
                        <div class="form-group is-required">
                            <label class="col-sm-2 control-label" for="zip">Zip:</label>

                            <div class="col-sm-2">
                                <input type="text" id="zip" name="zip"
                                       class="form-control mousetrap {{{ user_form_size() }}}"
                                       value="{{{ Input::old('zip', '') }}}">
                                {{ Form::errorMsg('zip') }}
                            </div>
                        </div>
                    </fieldset>
                </div>
                <div class="panel-footer col-sm-offset-2">
                    <div class="btn-group mr5">
                        <button type="submit" value="addNew" name="submit" class="btn btn-primary"
                                id="ndc_form_defaultaction">Save and add another account
                        </button>
                        <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
                            <span class="caret"></span>
                            <span class="sr-only">Toggle Dropdown</span>
                        </button>
                        <ul class="dropdown-menu" role="menu">
                            <li>
                                <button type="submit" value="return" name="submit" class="btn btn-white btn-md"
                                        id="ndc_form_secondaryaction">Save and return to previous action
                                </button>
                            </li>
                        </ul>
                    </div>
                    <div class="btn-group mr5">
                        <button type="reset" class="btn btn-default" id="ndc_form_reset">Reset</button>
                    </div>
                </div>
            </div>
            {{ Form::close() }}
        </div>
    </div>
@stop