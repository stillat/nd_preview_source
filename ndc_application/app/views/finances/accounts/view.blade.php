@extends('layouts.master')

@section('pageTitle')
<i class= ></i> <strong>{{{ limit_code($account->code)  }}}</strong>: {{{ limit_name($account->name) }}}
@stop

@section('buttonBar')
<div class="btn-group pull-up-10">
	{{ HTML::modelNaviateToolbar('finances.accounts.show', $account->previous(), 'finances.accounts.index', 'finances.accounts.show', $account->next()) }}
</div>
<div class="btn-group pull-up-10">
	<button type="button" class="btn btn-white dropdown-toggle" data-toggle="dropdown">
		Account Funds <span class="caret"></span>
	</button>
	<ul class="dropdown-menu" role="menu" style="margin-left: -115px;">
		<li><a href="{{ action('App\Finances\Transactions\TransactionController@getTransferTo', $account->id) }}"><i class="fa ndcf-money-deposit inline-icon"></i> Add funds to this account</a></li>
		<li><a href="{{ action('App\Finances\Transactions\TransactionController@getTransfer', $account->id) }}"><i class="fa ndcf-money-transfer inline-icon"></i> Transfer funds from this account</a></li>
	</ul>
</div>

@stop

@section('content')
<div class="row">
    <div class="col-sm-9">
        <strong>{{{ $account->name }}}</strong>
        {{ HTML::addressBlock($account->address_line_1, $account->address_line_2, $account->city, $account->state, $account->zip, $account->country) }}
    </div>
    <div class="col-sm-3 text-right">
        <dl class="dl-horizontal">
            <dt><i class="fa fa-barcode"></i> Code</dt><dd>{{{ $account->code }}}</dd>
            <dt>Balance</dt><dd>{{{ nd_number_format($account->balance) }}}</dd>
        </dl>
    </div>
	<div class="col-sm-12 mt10">
		<div class="profile-header">
			<div class="mb20"></div>
            <div class="btn-group">
                <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
                    Transfer Account Funds <span class="caret"></span>
                </button>
                <ul class="dropdown-menu" role="menu">
                    <li><a href="{{ action('App\Finances\Transactions\TransactionController@getTransferTo', $account->id) }}"><i class="fa ndcf-money-deposit inline-icon"></i> Add funds to this account</a></li>
                    <li><a href="{{ action('App\Finances\Transactions\TransactionController@getTransfer', $account->id) }}"><i class="fa ndcf-money-transfer inline-icon"></i> Transfer funds from this account</a></li>
                </ul>
                <a href="{{ route('finances.accounts.edit', $account->id) }}" class="btn btn-white"><i class="fa fa-edit"></i> Update account</a>
            </div>

		</div>
	</div>
</div>
@include('finances.accounts.partials.transactions')
@stop

@section('additionalScripts')
<script src="{{{ app_url() }}}/assets/js/finances/accounts.js" defer></script>
@stop