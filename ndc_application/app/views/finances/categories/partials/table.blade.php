<div class="table-responsive">
    <table class="table table-hover table-striped mb30 table-fixed">
        <thead>
        <tr>
            <th class="wd-200">Code</th>
            <th>Name</th>
            <th>Description</th>
            <th class="wd-100"></th>
        </tr>
        </thead>
        <tbody>
        @foreach($categories as $category)
            <tr>
                <td><a href="{{ action('App\Finances\AccountsController@index') }}?sort={{ $category->id }}">{{{ limit_code($category->code) }}}</a></td>
                <td><a href="{{ action('App\Finances\AccountsController@index') }}?sort={{ $category->id }}">{{{ limit_name($category->name) }}}</a></td>
                <td><a href="{{ action('App\Finances\AccountsController@index') }}?sort={{ $category->id }}">{{{ if_null_then_na(limit_description($category->description)) }}}</a></td>
                <td class="table-action td-ignore">
                    <a href="{{{ action('App\Finances\AccountsCategoryController@edit', array($category->id)) }}}"><i
                                class="fa fa-pencil"></i></a>
                    <a class="cursor-pointer delete-row" href="#" data-rcontext="{{{ $category->id }}}"><i
                                class="fa fa-trash-o"></i></a>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>