@extends('layouts.master')

@section('pageTitle')
<i class="fa fa-tag"></i> Account Categories
@stop

@section('buttonBar')
<div class="btn-group">
	<a href="{{ action('App\Finances\AccountsCategoryController@create') }}" class="btn btn-white n-mt-5 nd-add"><i class="fa fa-plus inline-icon"></i> New Account Category</a>
</div>
@stop

@section('content')

<div class="row">

    <div class="col-sm-12">
        {{ $categories->links() }}
    </div>

    <div class="col-sm-12">
        @include('finances.categories.partials.table')
    </div>

    <div class="col-sm-12">
        {{ $categories->links() }}
    </div>

</div>

@include('layouts.dialogs.remove', array('title' => 'Remove Account Category', 'action' => 'App\Finances\AccountsCategoryController@destroy'))
@stop