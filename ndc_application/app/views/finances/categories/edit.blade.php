@extends('layouts.master')

@section('pageTitle')
<i class="fa fa-pencil"></i> <strong>{{{ limit_code($category->code) }}}</strong> - {{{ limit_name($category->name)  }}}
@stop

@section('buttonBar')
<div class="btn-group pull-up-10">
	{{ HTML::modelNaviateToolbar('finances.categories.edit', $category->previous(), 'finances.categories.index', 'finances.categories.edit', $category->next()) }}
</div>
<div class="btn-group pull-up-10">
	<a href="{{ Redirector::getRoute(route('finances.categories.index')) }}" class="btn btn-danger ndc_form_leave">Cancel</a>
</div>
@stop

@section('content')

<div class="row">

	<div class="col-md-12">
		{{ Form::open(array('route' => array('finances.categories.update', $category->id), 'method' => 'put', 'class' => 'form-horizontal')) }}
		<div class="panel-primary panel-alt">
			<div class="panel-body">

				<div class="form-group">
					<label class="col-sm-2 control-label">Code:</label>
					<div class="col-sm-4">
						<div class="input-group">
							<span class="input-group-addon"><i class="glyphicon glyphicon-barcode"></i></span>
							<input type="text" name="code" class="form-control mousetrap ndc_form_home_element text-right {{{ user_form_size() }}}" value="{{{ Input::old('code', $category->code) }}}">
						</div>
                        {{ Form::errorMsg('code') }}
                    </div>
				</div>

				<div class="form-group is-required">
					<label class="col-sm-2 control-label">Name:</label>
					<div class="col-sm-8">
						<input type="text" name="name" class="form-control mousetrap {{{ user_form_size() }}}" value="{{{ Input::old('name', $category->name) }}}" autofocus>
						{{ Form::errorMsg('name') }}
					</div>
				</div>

				<div class="form-group is-required">
					<label class="col-sm-2 control-label">Description:</label>
					<div class="col-sm-8">
						<input type="text" name="description" class="form-control mousetrap {{{ user_form_size() }}}" value="{{{ Input::old('description', $category->description) }}}">
						{{ Form::errorMsg('description') }}
					</div>
				</div>

				<div class="form-group">
                    <label class="col-sm-2 control-label">Color</label>
                	<div class="col-sm-8">
                	    <input type="text" name="color" class="form-control mousetrap ndc_form_trail_element colorpicker-input" id="colorpicker" value="{{{ Input::old('color', $category->color) }}}">
                		<span id="colorSelector" class="colorselector">
                		    <span style="background-color: {{{ Input::old('color', $category->color) }}};"></span>
                	    </span>
                	</div>
                </div>



			</div>
			<div class="panel-footer col-sm-offset-2">
				<div class="btn-group mr5">
					<button type="submit" value="return" name="submit" class="btn btn-info" id="ndc_form_defaultaction">Update this account category</button>
				</div>
				<div class="btn-group mr5">
					<button type="reset" class="btn btn-default" id="ndc_form_reset">Reset</button>
				</div>
			</div>
		</div>
		{{ Form::close() }}
	</div>

</div>
@stop

@section('additionalStyles')
<link rel="stylesheet" type="text/css" href="{{{ app_url() }}}/assets/css/colorpicker.css">
@stop

@section('additionalScripts')
<script src="{{ app_url() }}/assets/js/dep/colorpicker.js" defer></script>
<script src="{{ app_url() }}/assets/js/finances/categories.js" defer></script>
@stop