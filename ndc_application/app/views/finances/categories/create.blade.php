@extends('layouts.master')

@section('pageTitle')
<i class="fa fa-plus"></i> You are adding a new account category
@stop

@section('buttonBar')
<div class="btn-group pull-up-10">
	<a href="{{ Redirector::getRoute(route('finances.accounts.index')) }}" class="btn btn-danger ndc_form_leave">Cancel</a>
</div>
@stop


@section('content')

<div class="row">

	<div class="col-md-12">
		{{ Form::open(array('route' => 'finances.categories.store', 'method' => 'post', 'class' => 'form-horizontal')) }}
		<div class="panel-primary panel-alt">
			<div class="panel-body">
				<div class="form-group">
					<label class="col-sm-2 control-label">Code:</label>
					<div class="col-sm-4">
						<div class="input-group">
							<span class="input-group-addon"><i class="glyphicon glyphicon-barcode"></i></span>
							<input type="text" name="code" class="form-control text-right mousetrap ndc_form_home_element {{{ user_form_size() }}}" value="{{{ Input::old('code', '') }}}" autofocus>
						</div>
                        {{ Form::errorMsg('code') }}
                    </div>
				</div>

				<div class="form-group is-required">
					<label class="col-sm-2 control-label">Name:</label>
					<div class="col-sm-8">
						<input type="text" name="name" class="form-control mousetrap {{{ user_form_size() }}}" value="{{{ Input::old('name', '') }}}">
						{{ Form::errorMsg('name') }}
					</div>
				</div>

				<div class="form-group is-required">
					<label class="col-sm-2 control-label">Description:</label>
					<div class="col-sm-8">
						<input type="text" name="description" class="form-control mousetrap ndc_form_trail_element {{{ user_form_size() }}}" value="{{{ Input::old('description', '') }}}">
						{{ Form::errorMsg('description') }}
					</div>
				</div>

				<div class="form-group">
					<label class="col-sm-2 control-label">Color</label>
					<div class="col-sm-8">
						<input type="text" name="color" class="form-control colorpicker-input" id="colorpicker" value="{{{ Input::old('color', 'transparent') }}}">
						<span id="colorSelector" class="colorselector">
							<span style="background-color: {{{ Input::old('color', 'transparent') }}};"></span>
						</span>
					</div>
				</div>

			</div>
			<div class="panel-footer col-sm-offset-2">
				<div class="btn-group mr5">
					<button type="submit" value="addNew" name="submit" class="btn btn-primary" id="ndc_form_defaultaction">Save and add another account category</button>
					<button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
						<span class="caret"></span>
						<span class="sr-only">Toggle Dropdown</span>
					</button>
					<ul class="dropdown-menu" role="menu">
						<li><button type="submit" value="return" name="submit" class="btn btn-white btn-md" id="ndc_form_secondaryaction">Save and return to previous action</button></li>
					</ul>
				</div>

				<div class="btn-group mr5">					
					<button type="reset" id="ndc_form_reset" class="btn btn-default ndc_form_reset">Reset</button>
				</div>
			</div>
		</div>
		{{ Form::close() }}
	</div>

</div>
@stop

@section('additionalStyles')
<link rel="stylesheet" type="text/css" href="{{{ app_url() }}}/assets/css/colorpicker.css">
@stop

@section('additionalScripts')
<script src="{{ app_url() }}/assets/js/dep/colorpicker.js" defer></script>
<script src="{{ app_url() }}/assets/js/finances/categories.js" defer></script>
@stop