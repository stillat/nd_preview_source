@extends('data_tools.master')

@section('settingContent')
    <h3><strong>Reassign Records</strong></h3>
    @include('data_tools.reassign.notice')

    <div class="table-responsive">
        <table class="table table-hover table-striped">
            <thead>
            <tr>
                <th>Record Type</th>
                <th class="text-right wd-200"></th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td><i class="fa ndc-bicycle"></i> Activity</td>
                <td><a class="btn btn-white btn-block"
                       href="{{ action('App\DataTools\ReassignRecordsController@getReassign', [1]) }}">Reassign
                        Activities</a></td>
            </tr>
            <tr>
                <td><i class="fa fa-users"></i> Department</td>
                <td><a class="btn btn-white btn-block"
                       href="{{ action('App\DataTools\ReassignRecordsController@getReassign', [2]) }}">Reassign
                        Departments</a></td>
            </tr>
            <tr>
                <td><i class="fa fa-map-marker"></i> District</td>
                <td><a class="btn btn-white btn-block"
                       href="{{ action('App\DataTools\ReassignRecordsController@getReassign', [3]) }}">Reassign
                        Districts</a></td>
            </tr>
            <tr>
                <td><i class="fa fa-user"></i> Employee</td>
                <td><a class="btn btn-white btn-block"
                       href="{{ action('App\DataTools\ReassignRecordsController@getReassign', [4]) }}">Reassign
                        Employees</a></td>
            </tr>
            <tr>
                <td><i class="ndc-fuel"></i> Fuel</td>
                <td><a class="btn btn-white btn-block"
                       href="{{ action('App\DataTools\ReassignRecordsController@getReassign', [5]) }}">Reassign
                        Fuels</a></td>
            </tr>
            <tr>
                <td><i class="fa fa-cubes"></i> Material</td>
                <td><a class="btn btn-white btn-block"
                       href="{{ action('App\DataTools\ReassignRecordsController@getReassign', [6]) }}">Reassign
                        Materials</a></td>
            </tr>
            <tr>
                <td><i class="fa fa-folder-open"></i> Project</td>
                <td><a class="btn btn-white btn-block"
                       href="{{ action('App\DataTools\ReassignRecordsController@getReassign', [7]) }}">Reassign
                        Projects</a></td>
            </tr>
            </tbody>
        </table>
    </div>
@stop