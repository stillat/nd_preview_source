@extends('data_tools.master')

@section('settingContent')
    <h3><strong>Reassign {{ $reassignTitle }}</strong></h3>
    @include('data_tools.reassign.notice')
    {{ Form::open(['action' => ['App\DataTools\ReassignRecordsController@postReassign', $mode], 'method' => 'post', 'class' => 'form-horizontal']) }}
    <div class="table-responsive">
        <table class="table table-hover table-striped">
            <thead>
            <tr>
                <th>Start {{{ Str::singular($reassignTitle) }}}</th>
                <th>Destination {{{ Str::singular($reassignTitle)  }}}</th>
                <th class="wd-100 text-right"><a class="btn btn-block btn-white" id="addReassignedRecordRow"><i
                                class="fa fa-plus"></i> Add {{{ Str::singular($reassignTitle)  }}}</a></th>
            </tr>
            </thead>
            <tbody id="reassignedRecordsHolder">

            </tbody>
        </table>

        <div class="btn-group mr5">
            <button type="submit" id="ndc_form_defaultaction" value="return" name="submit" class="btn btn-primary">
                Reassign the selected {{{ $reassignTitle }}}</button>
        </div>
    </div>
    <input type="hidden" name="mode" value="{{{ $mode }}}">
    {{ Form::close() }}
@stop

@section('additionalScripts')

    <script type="text/html" id="reassignRecord_tmpl">
        <%* createdReassignRecords++ %>
        <tr data-reassign="<%:~getReassignedRecordCount()%>" id="reassignSelector<%:~getReassignedRecordCount()%>">
            <td><input type="hidden" class="form-control mousetrap" name="start_records[]" id="startRecord<%:~getReassignedRecordCount()%>"></td>
            <td><input type="hidden" class="form-control mousetrap" name="finish_records[]" id="finishRecord<%:~getReassignedRecordCount()%>"></td>
            <td><a class="btn btn-link btn-block remove_reassign" data-reassign="<%:~getReassignedRecordCount()%>">Remove</a></td>
        </tr>
    </script>
    <script>
        var REASSIGN_MODE = {{{ $mode }}};
    </script>
    <script src="{{{ app_url() }}}/assets/js/jsrender.min.js" defer></script>
    <script src="{{{ app_url() }}}/assets/js/data/reassign.min.js" defer></script>
@stop