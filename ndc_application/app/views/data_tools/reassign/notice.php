<p>Record reassignment allows you to update all work records at once by converting one record type to another. For
    example, using record reassignment all occurrences of one type of fuel can be turned into another fuel. This is
    useful for managing data that was imported from older versions or legacy systems. Record reassignment is
    permanent and <strong>cannot</strong> be undone.</p>