@extends('data_tools.master')

@section('settingContent')
        <h3><strong>Export Data</strong></h3>
        <p>The Export Data tools allow you to create lists in a comma separated values (CSV) file, which can be opened by many applications such as Microsoft&reg; Excel. Exporting with aggregate data will allow for the greatest flexibility in the data that is generated and exported.</p>
        {{ Form::open(['action' => 'App\DataTools\DataExportController@getRunExport', 'method' => 'get', 'class' => 'form-horizontal']) }}
            <div class="form-group">
                <label for="export_data" class="col-sm-2 control-label text-right">What would you like to
                    export?</label>

                <div class="col-sm-6">
                        {{ Form::select('export_data', $exportItems, 'org_activity', ['id' => 'export_data', 'class' => 'chosen-select form-control'])}}
                </div>
            </div>
            <div class="radio col-sm-offset-2">
                <div class="rdio rdio-default">
                    <input type="radio" name="export_type" id="exportOnlyLists" value="list" checked>
                    <label for="exportOnlyLists"><i class="fa fa-list"></i> Export only the list data</label>
                </div>
            </div>
            <div class="radio col-sm-offset-2 mb15">
                <div class="rdio rdio-default">
                    <input type="radio" name="export_type" id="exportWithAggregate" value="aggregate">
                    <label for="exportWithAggregate"><i class="fa fa-bar-chart-o"></i> Export list data with aggregate data</label>
                </div>
            </div>

            <div id="aggregate_data_holder" style="display: none;">
                <fieldset>
                    <p class="col-sm-offset-1"><strong>Date Range</strong></p>
                    <div id="daterange" class="btn btn-white n-mt-5 col-sm-offset-2 mb15"><i class="fa fa-calendar-o inline-icon" style="margin-right:10px;"></i> <span> September 10, 2014 - September 24, 2014</span> <b class="caret"></b></div>
                </fieldset>
                <fieldset>
                    <p class="col-sm-offset-1"><strong><i class="fa fa-user"></i> Employee Information</strong></p>
                    <div class="checkbox col-sm-offset-2">
                        <div class="ckbox ckbox-default">
                            <input type="checkbox" id="chkEmployeeRegularHours" name="employee_regular_hours" value="1">
                            <label for="chkEmployeeRegularHours">Employee Regular Hours</label>
                        </div>
                        <div class="ckbox ckbox-default">
                            <input type="checkbox" id="chkEmployeeRegularCost" name="employee_regular_cost" value="1">
                            <label for="chkEmployeeRegularCost">Employee Regular Cost</label>
                        </div>
                        <div class="ckbox ckbox-default">
                            <input type="checkbox" id="chkEmployeeRegularBenefits" name="employee_regular_benefits" value="1">
                            <label for="chkEmployeeRegularBenefits">Employee Regular Benefits</label>
                        </div>
                    </div>
                    <div class="checkbox col-sm-offset-2">
                        <div class="ckbox ckbox-default">
                            <input type="checkbox" id="chkEmployeeOvertimeHours" name="employee_overtime_hours" value="1">
                            <label for="chkEmployeeOvertimeHours">Employee Overtime Hours</label>
                        </div>
                        <div class="ckbox ckbox-default">
                            <input type="checkbox" id="chkEmployeeOvertimeCost" name="employee_overtime_cost" value="1">
                            <label for="chkEmployeeOvertimeCost">Employee Overtime Cost</label>
                        </div>
                        <div class="ckbox ckbox-default">
                            <input type="checkbox" id="chkEmployeeOvertimeBenefits" name="employee_overtime_benefits" value="1">
                            <label for="chkEmployeeOvertimeBenefits">Employee Overtime Benefits</label>
                        </div>
                    </div>
                </fieldset>
                <fieldset>
                    <p class="col-sm-offset-1"><strong><i class="fa fa-truck"></i> Equipment Units</strong></p>
                    <div class="checkbox col-sm-offset-2">
                        <div class="ckbox ckbox-default">
                            <input type="checkbox" id="chkEquipmentQuantity" name="equipment_quantity" value="1">
                            <label for="chkEquipmentQuantity">Equipment Quantity</label>
                        </div>
                        <div class="ckbox ckbox-default">
                            <input type="checkbox" id="chkEquipmentCost" name="equipment_cost" value="1">
                            <label for="chkEquipmentCost">Equipment Cost</label>
                        </div>
                    </div>
                </fieldset>
                <fieldset>
                    <p class="col-sm-offset-1"><strong><i class="fa fa-cube"></i> Fuels and Materials</strong></p>
                    <div class="checkbox col-sm-offset-2">
                        <div class="ckbox ckbox-default">
                            <input type="checkbox" id="chkMaterialQuantity" name="material_quantity" value="1">
                            <label for="chkMaterialQuantity">Material Quantity</label>
                        </div>
                        <div class="ckbox ckbox-default">
                            <input type="checkbox" id="chkMaterialCost" name="material_cost" value="1">
                            <label for="chkMaterialCost">Material Cost</label>
                        </div>
                    </div>
                    <div class="checkbox col-sm-offset-2">
                        <div class="ckbox ckbox-default">
                            <input type="checkbox" id="chkFuelQuantity" name="fuel_quantity" value="1">
                            <label for="chkFuelQuantity">Fuel Quantity</label>
                        </div>
                        <div class="ckbox ckbox-default">
                            <input type="checkbox" id="chkFuelCost" name="fuel_cost" value="1">
                            <label for="chkFuelCost">Fuel Cost</label>
                        </div>
                    </div>
                </fieldset>
                <fieldset>
                    <p class="col-sm-offset-1"><strong>Miscellaneous</strong></p>
                    <div class="checkbox col-sm-offset-2">
                        <div class="ckbox ckbox-default">
                            <input type="checkbox" id="chkPropertyCost" name="property_cost" value="1">
                            <label for="chkPropertyCost">Property Cost</label>
                        </div>
                        <div class="ckbox ckbox-default">
                            <input type="checkbox" id="chkMiscBillingQuantity" name="billing_quantity" value="1">
                            <label for="chkMiscBillingQuantity">Miscellaneous Billing Quantity</label>
                        </div>
                        <div class="ckbox ckbox-default">
                            <input type="checkbox" id="chkMiscBillingCost" name="billing_cost" value="1">
                            <label for="chkMiscBillingCost">Miscellaneous Billing Cost</label>
                        </div>
                    </div>
                </fieldset>
            </div>

            <div class="col-sm-offset-2 mt10">
                <input type="hidden" id="report_start_date" name="start_date" value="{{ $startDate }}">
                <input type="hidden" id="report_end_date" name="end_date" value="{{ $endDate }}">
                <button type="submit" class="btn btn-primary"><i class="glyphicon glyphicon-export"></i> Export Data</button>
            </div>
        </form>
@stop

@section('additionalScripts')
    <script>
        var startDate = '{{ $startDate }}';
        var endDate = '{{ $endDate }}';
    </script>
    <script src="{{{ app_url() }}}/assets/js/dep/chosen.jquery.min.js" defer></script>
    <script src="{{{ app_url() }}}/assets/js/data/export.min.js" defer></script>
@stop