@extends('layouts.master')

@section('pageTitle')
    <i class="fa fa-wrench"></i> Data Tools
@stop

@section('additionalStyles')
    <style>.contentpanel {
            padding-top: 0px;
        }

        .contentpanel  label {
            margin-left: 15px;
            margin-top: 2px;
        }

        .inner-content-panel {
            padding-top: 20px;
        }

        li.active > a {
            color: black;
            font-weight: bold;
        }

        fieldset > legend {
            padding-left: 15px;
        }

        fieldset > p {
            padding-left: 30px;
        }</style>
@stop

@section('content')

    <div class="row">
        <div class="col-sm-3 inner-content-panel" style="border-right: 1px solid #d3d7db; min-height: 30px;">
            <h4><strong>General</strong></h4>
            <?php
            $routePathToRestore = Menu::getRoutePath();

            if (Str::startsWith(Request::url(), url('data-tools/reassign-records'))) {
                Menu::setRoutePath(url('data-tools/reassign-records'));
            } else {
                Menu::setRoutePath(Request::url());
            }

            ?>
            {{ Menu::render('data_tools_general') }}
            <?php
            Menu::setRoutePath($routePathToRestore);
            ?>
        </div>
        @yield('formOpen')
        <div class="col-sm-8 inner-content-panel" style="padding-right:10px;padding-top:8px;">
            <h3><strong>@yield('settingTitle')</strong></h3>
            @yield('settingContent')
        </div>
        @yield('formClose')

    </div>
@stop