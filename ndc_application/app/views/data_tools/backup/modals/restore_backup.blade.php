<div class="modal fade" id="restoreBackupDialog" tabindex="-1" role="dialog" aria-labelledby="restoreBackupDialogLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true" style="margin-top: 10px;">&times;</button>
                <h4 id="restoreBackupDialogLabel"><i class="fa fa-refresh"></i> Restore Backup</h4>
            </div>
            <div class="modal-body">
                Are you sure you want to restore your data from a backup at this time? Data restoration can take a significant amount of time to complete.
            </div>
            <div class="modal-footer">
                {{ Form::open(['action' => 'App\DataTools\BackupController@postRestoreBackup'], ['method' => 'post']) }}
                <input type="hidden" name="restore_key" id="restoreContext">
                <button type="submit" class="btn btn-white"><i class="fa fa-refresh"></i> Yes, Restore Backup Now</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                {{ Form::close() }}
            </div>
        </div>
    </div>
</div>