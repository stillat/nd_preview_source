<div class="modal fade" id="uploadBackupModal" tabindex="-1" role="dialog" aria-labelledby="uploadBackupModalLabel" aria-hidden="true">
    {{ Form::open(['action' => 'App\DataTools\BackupController@postUploadBackup', 'files' => true]) }}
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true" style="margin-top: 10px;">&times;</button>
                <h4 id="uploadBackupModalLabel"><i class="fa fa-cloud-upload"></i> Upload Existing Backup</h4>
            </div>
            <div class="modal-body">
                Uploading large backups can take a long time. You are permitted to only upload valid ND Counties restoration files. Invalid restoration files will automatically be removed and will still count against the account's upload limit.

                <div class="fileupload fileupload-new" data-provides="fileupload">
                    <div class="input-append">
                        <div class="uneditable-input" style="width: 71%;">
                            <i class="glyphicon glyphicon-file fileupload-exists"></i>
                            <span class="fileupload-preview"></span>
                        </div>
                    <span class="btn btn-default btn-file">
                      <span class="fileupload-new">Select backup file</span>
                      <span class="fileupload-exists">Change</span>
                      <input type="file" name="existing_backup" />
                    </span>
                        <a href="#" class="btn btn-default fileupload-exists" data-dismiss="fileupload">Remove</a>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                {{ Form::open(['action' => 'App\DataTools\BackupController@postCreate'], ['method' => 'post']) }}
                <button type="submit" class="btn btn-white"><i class="fa fa-cloud-upload"></i> Yes, Upload this Backup</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                {{ Form::close() }}
            </div>
        </div>
    </div>
    {{ Form::close() }}
</div>