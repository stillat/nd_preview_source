<div class="modal fade" id="createBackupModal" tabindex="-1" role="dialog" aria-labelledby="createBackupModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true" style="margin-top: 10px;">&times;</button>
                <h4 id="createBackupModalLabel"><i class="fa fa-compress"></i> Create Backup Now</h4>
            </div>
            <div class="modal-body">
                Are you sure you want to create a backup at this time? Creating backups can take a considerable amount of time, depending on how much data your account is using. New data cannot be entered while a back up is being created.
            </div>
            <div class="modal-footer">
                {{ Form::open(['action' => 'App\DataTools\BackupController@postCreate'], ['method' => 'post']) }}
                <button type="submit" class="btn btn-white"><i class="fa fa-compress"></i> Yes, Create Backup</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                {{ Form::close() }}
            </div>
        </div>
    </div>
</div>