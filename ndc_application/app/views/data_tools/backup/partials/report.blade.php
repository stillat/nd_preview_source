<div class="row">
    <div class="col-sm-4">
        <div class="panel <?php if (($statistics->createdBackupsByUsers + $statistics->createdBackupsByAdmins) > 0) {
    echo 'panel-success';
} else {
            echo 'panel-danger';
        } ?> panel-stat">
            <div class="panel-heading">

                <div class="stat">
                    <div class="row">
                        <div class="col-xs-4">
                            <i class="fa fa-compress"></i>
                        </div>
                        <div class="col-xs-8">
                            <small class="stat-label">Backups Created</small>
                            <h1>{{ ($statistics->createdBackupsByUsers + $statistics->createdBackupsByAdmins) }}</h1>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <div class="col-sm-4">
        <div class="panel panel-primary panel-stat">
            <div class="panel-heading">
                <div class="stat">
                    <div class="row">
                        <div class="col-xs-4">
                            <i class="fa fa-refresh"></i>
                        </div>
                        <div class="col-xs-8">
                            <small class="stat-label">Backups Restored</small>
                            <h1>{{ ($statistics->restoredBackupsByUsers + $statistics->restoredBackupsByAdmins) }}</h1>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <div class="col-sm-4">
        <div class="panel <?php if ($failedOperations == 0) {
    echo 'panel-success';
} else {
            echo 'panel-warning';
        } ?> panel-stat">
            <div class="panel-heading">
                <div class="stat">
                    <div class="row">
                        <div class="col-xs-4">
                            <i class="fa fa-warning"></i>
                        </div>
                        <div class="col-xs-8">
                            <small class="stat-label">Failed Backup Operations</small>
                            <h1>{{ $failedOperations }}</h1>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="well text-center">
        <p>All available action limitations will be reset on:</p>
        <p class="lead">{{ Carbon::now()->addMonth(1)->firstOfMonth()->format('l jS \\of F Y ') }}</p>
    </div>
</div>
<div class="row">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">Account Status</h3>
        </div>
        <table class="table table-striped table-hover">
            <tbody>
            <tr>
                <td><strong>Restore Backups Remaining</strong>:</td>
                <td class="text-right">{{ HTML::remainingLabel(nd_number_format_difference_to_zero($systemInformation->backup_restore_backups -  $statistics->restoredBackupsByUsers)) }}</td>
            </tr>
            <tr>
                <td><strong>Create Backups Remaining</strong>:</td>
                <td class="text-right">{{ HTML::remainingLabel(nd_number_format_difference_to_zero($systemInformation->backup_create_limits -  $statistics->createdBackupsByUsers)) }}</td>
            </tr>
            <tr>
                <td><strong>Upload Backups Remaining</strong>:</td>
                <td class="text-right">{{ HTML::remainingLabel(nd_number_format_difference_to_zero($systemInformation->backup_upload_limits -  $statistics->uploadedBackupsByUsers)) }}</td>
            </tr>
            <tr>
                <td><strong>Backup Downloads</strong></td>
                <td class="text-right">{{ HTML::availableLabel(!$currentState->download_disabled) }}</td>
            </tr>
            <tr>
                <td><strong>Backup Removal</strong></td>
                <td class="text-right">{{ HTML::availableLabel(!$currentState->remove_disabled) }}</td>
            </tr>
            </tbody>
        </table>
    </div>
</div>