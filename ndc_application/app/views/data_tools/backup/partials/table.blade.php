<div class="table-responsive">
    <table class="table table-hover table-striped">
        <thead>
        <tr>
            <th>Backup</th>
            <th>Size</th>
            <th>Generated On</th>
            <th></th>
            <th class="wd-200">
                @if($databaseOperationsDisabled || $currentState->upload_disabled)
                    <button class="btn btn-white btn-block" disabled><i
                                class="fa fa-compress"></i> Upload Existing Backup
                    </button>
                @else
                    <a class="btn btn-white btn-block" data-toggle="modal" data-target="#uploadBackupModal"><i
                                class="fa fa-cloud-upload"></i> Upload Existing Backup</a>
                @endif
            </th>
            <th class="wd-200">
                @if($databaseOperationsDisabled || $currentState->create_disabled)
                    <button class="btn btn-white btn-block" disabled><i
                                class="fa fa-compress"></i> Create Backup Now
                    </button>
                @else
                    <a class="btn btn-white btn-block" data-toggle="modal" data-target="#createBackupModal"><i
                                class="fa fa-compress"></i> Create Backup Now</a>
                @endif
            </th>
        </tr>
        </thead>
        <tbody>
        @foreach($backups as $backup)
            <tr>
                <td>{{ $backup->getName() }}</td>
                <td>{{ UI::formatSizeUnits($backup->getSize()) }}</td>
                <td>{{ $backup->getCreateDate()->diffForHumans() }}</td>
                @if($databaseOperationsDisabled || $currentState->restore_disabled)
                    <td><a class="btn btn-link disabled"><i class="fa fa-refresh"></i> Restore Backup</a></td>
                @else
                    <td><a class="btn btn-link restore-row" data-restorecontext="{{{ $backup->getName() }}}"><i
                                    class="fa fa-refresh"></i> Restore Backup</a></td>
                @endif
                @if($databaseOperationsDisabled || $currentState->remove_disabled)
                    <td><a class="btn btn-link disabled"><i class="fa fa-trash-o"></i> Remove Backup</a></td>
                @else
                    <td><a class="btn btn-link delete-row" data-rcontext="{{{ $backup->getName() }}}"><i
                                    class="fa fa-trash-o"></i> Remove Backup</a></td>
                @endif
                @if($currentState->download_disabled)
                    <td><a class="btn btn-link disabled"><i
                                    class="fa fa-cloud-download"></i> Download Backup</a></td>
                @else
                    <td><a class="btn btn-link"
                           href="{{ action('App\DataTools\BackupController@getDownload', [$backup->getName()]) }}"><i
                                    class="fa fa-cloud-download"></i> Download Backup</a></td>
                @endif
            </tr>
        @endforeach
        </tbody>
    </table>
</div>

