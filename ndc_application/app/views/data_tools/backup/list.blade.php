@extends('data_tools.master')

@section('settingContent')
    <h3><strong>Backup &amp; Restore Data</strong></h3>
    <p>Data backup and restoration puts you and your organization in control of your data durability and available.
        Generating backups and restoring from previous backups is a time consuming process, and most account actions may
        be disabled during a backup or restoration. Most accounts are limited in the number of monthly backups and
        restorations that can be performed.</p>

    @if($databaseOperationsDisabled)
        <div class="alert alert-info">
            <p>A backup is currently being created or restored. Backup operations cannot be completed until the
                currently running process is completed. While this operations completes data features will be restored
                once the maintenance has completed.</p>
        </div>
    @endif

    <ul class="nav nav-tabs">
        <li class="active"><a href="#backups" data-toggle="tab"><strong>Backups</strong></a></li>
        <li><a href="#report" data-toggle="tab"><strong>Report (This Month)</strong></a></li>
    </ul>

    <div class="tab-content mb30">
        <div class="tab-pane active" id="backups">
            @include('data_tools.backup.partials.table')
        </div>
        <div class="tab-pane" id="report">
            @include('data_tools.backup.partials.report')
        </div>
    </div>

    @include('data_tools.backup.modals.create_backup')
    @include('data_tools.backup.modals.restore_backup')
    @include('data_tools.backup.modals.upload_backup')

    @include('layouts.dialogs.remove', array('title' => 'Remove Backup', 'content' => 'Are you sure you want to remove this backup file? Once removed, backup files cannot be restored.', 'agreeText' => 'Remove Backup File', 'action' => 'App\DataTools\BackupController@deleteBackup'))
@stop

@section('additionalScripts')
    <script src="{{ app_url() }}/assets/js/data/backup.min.js" defer></script>
@stop