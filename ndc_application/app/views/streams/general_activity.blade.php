<div class="media act-media">
    @if($user->id == 0)
        <img src="{{ app_url() }}/assets/img/cloud.png" class="media-object act-thumb pull-left" alt="ND Counties" />
    @else
        <img class="media-object act-thumb pull-left"
             src="{{ $avatarPresenter->getAvatar($user->id, $user->email, 2) }}" alt="">
    @endif
    <div class="media-body act-media-body">
        <strong>
            @if($user->id == Auth::user()->id)
                You
            @else
                {{{ str_limit($user->first_name.' '.$user->last_name, 11, '...') }}}
            @endif
        </strong> {{{ $verb }}} {{ $context }}<br>
        <small class="text-muted">{{ (new Carbon($event->performed_on))->diffForHumans() }}</small>
    </div>
</div>