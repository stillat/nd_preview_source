<tr>
    <td>{{{ (new Carbon($adjustment->date_adjusted)) }}}</td>
    <td class="employee-photo-holder hidden-print">
    @if($adjustment->adjustment_context == 0 && $adjustment->adjustment_value == 1)
    <img src="{{ app_url() }}/assets/img/cloud.png" alt="ND Counties" style="border-radius: 0px;border: none;width: 26px;"/> ND Counties
    @else
    <img src="{{ $avatarPresenter->getAvatar($user->id, $user->email, 2) }}" alt="{{{ $user->first_name.' '.$user->last_name }}}"/> {{{ str_limit($user->first_name.' '.$user->last_name, 10) }}}
    @endif
    </td>
    <td class="hidden-print">{{ $description }}</td>
    <td class="text-right">{{{ nd_number_format($adjustment->adjustment_amount) }}}</td>
</tr>