<div class="media act-media sys-notification">
    <i class="fa fa-bell animated forever pulse fa-2x text-danger media-object act-thumb pull-left"></i>
    <div class="media-body act-media-body">
        <strong><i class="fa fa-bug"></i> {{{ $context }}}</strong> <small class="text-muted">{{ (new Carbon($event->performed_on))->diffForHumans() }}</small>
        <p class="read-more" style="word-wrap: break-word;">{{ $meta->message }}</p>
    </div>
</div>