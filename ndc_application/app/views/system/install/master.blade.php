<!DOCTYPE html>
<html>
<head>
  <title>ND Counties Enterprise Installation</title>
  {{ UI::getCss() }}
  <link rel="stylesheet" href="{{ app_url() }}/assets/css/install.css">
</head>
<body class="install">
  @yield('startForm')
  <div class="container">
    <div class="row">
      <div class="col-md-12 col-sm-12 text-center">
        <h2 class="form-signin-heading"><i class="fa fa-cloud" style="margin-right: 5px;"></i> ND Counties <small>Enterprise Installation</small></h2>
      </div>
      <div class="col-md-3 col-sm-3">
        <ul class="nav nav-pills nav-stacked" style="max-width: 260px;">
        @yield('stepsBar')  
        </ul>
      </div>
      <div class="col-md-9 col-sm-9 panel panel-default">
        <div class="panel-body">
          @yield('content')
        </div>
      </div>

      <div class="col-md-offset-3 col-sm-offset-3 col-md-9 col-sm-9 panel panel-default">
        <div class="panel-body">
          <ul class="pager no-margin">
            @yield('pagers')
          </ul>
        </div>
      </div>
    </div>
  </div>
  @yield('endForm')
  {{ UI::getJs() }}
  <script src="{{ app_url() }}/assets/js/bootstrap-wizard.min.js"></script>
</body>
</html>