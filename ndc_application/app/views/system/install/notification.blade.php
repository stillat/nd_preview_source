@extends('system.install.master')

@section('content')
<h1>Your system needs to be installed</h1>
<p>To use the ND Counties system, you must first install it on your servers. To start the installation process, click or tap "Start Installation" below.</p>
@stop

@section('pagers')
<li class="next"><a href="{{ action('System\SetupController@getIndex') }}">Start Installation <i class="fa fa-chevron-circle-right"></i></a></li>
@stop