@extends('system.install.master')

@section('stepsBar')
<li><a>Installation Preparation</a></li>
<li><a>Central Database Setup</a></li>
<li class="active"><a>Administrator Account</a></li>
<li><a>Service Account Setup</a></li>
<li><a>Complete Setup</a></li>
@stop

@section('startForm')
{{ Form::open(array('action' => 'System\SetupController@postCreateAdmin', 'method' => 'POST', 'class' => 'form-horizontal')) }}
@stop

@section('endForm')
{{ Form::close() }}
@stop

@section('content')

<h3>Initial Administrator Account</h3>
<hr>
<p>You need to create the initial administrator account for use with the system.</p>

<div class="form-group">
<label class="col-sm-2 control-label" for="fname">First Name:</label>
  <div class="col-sm-8">
    <input type="text" name="fname" id="fname" class="form-control" placeholder="Your first name">
</div>
</div>

<div class="form-group">
<label class="col-sm-2 control-label" for="lname">Last Name:</label>
  <div class="col-sm-8">
    <input type="text" name="lname" id="lname" class="form-control" placeholder="Your last name">
</div>
</div>

<div class="form-group">
<label class="col-sm-2 control-label" for="email">Email/Log-in:</label>
  <div class="col-sm-8">
    <input type="email" name="email" id="email" class="form-control" placeholder="You need this for notifications and to log in">
</div>
</div>

<div class="form-group">
<label class="col-sm-2 control-label" for="password">Password:</label>
  <div class="col-sm-8">
    <input type="text" name="password" id="password" class="form-control" placeholder="Choose a secure password you will remember">
</div>
</div>

@stop


@section('pagers')
<li class="previous disabled"><a href="{{ action('System\SetupController@getSetupDatabase') }}" disabled="disabled">Install Initial Database Systems</a></li>
<li class="next"><button class="btn btn-white pull-right" type="submit">Create Administrator Account</button></li>
@stop
