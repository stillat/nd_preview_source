@extends('system.install.master')

@section('stepsBar')
<li><a>Installation Preparation</a></li>
<li><a>Central Database Setup</a></li>
<li><a>Administrator Account</a></li>
<li><a>Service Account Setup</a></li>
<li class="active"><a>Complete Setup</a></li>
@stop

@section('content')
<h3>Your installation has completed</h3>
<hr>
<p>The system is now ready to be used. There may be additional settings that have to be configured within the application interface.</p>
<div class="row">
    <div class="col-md-12 col-sm-12">
        <img src="{{ app_url() }}/assets/img/concept.jpg" class="img-responsive" style="width: 100%; max-height: 300px;">
    </div>
</div>
@stop

@section('pagers')
<li><a href="{{ url('login') }}" class="btn btn-primary btn-lg btn-block">Continue to log in for the first time</a></li>
@stop