@extends('system.install.master')

@section('stepsBar')
<li><a>Installation Preparation</a></li>
<li><a>Central Database Setup</a></li>
<li class="active"><a>Administrator Account</a></li>
<li><a>Service Account Setup</a></li>
<li><a>Complete Setup</a></li>
@stop

@section('startForm')
{{ Form::open(array('action' => 'System\SetupController@postCreateAdmin', 'method' => 'POST', 'class' => 'form-horizontal')) }}
@stop

@section('endForm')
{{ Form::close() }}
@stop

@section('content')

<h3>Configure your accounts security</h3>
<hr>
<p>Because this is an administrator account, it is requred that two-factor authentication is used.</p>

<form class="form-horizontal">
	<div class="form-group">
		<label class="col-sm-2 control-label">Your Key:</label>
		<div class="col-sm-8">
			<input type="text" class="form-control" value="{{{ $skey }}}">
		</div>
	</div>
</form>

<form class="form-horizontal">
	<div class="form-group">
		<label class="col-sm-2 control-label">or scan this:</label>
		<div class="col-sm-8">
			<img src="{{ $qrCode }}" alt="">
		</div>
	</div>
</form>

@stop


@section('pagers')
<li class="next"><a href="{{ action('System\SetupController@getCreateServiceAccount') }}" class="btn btn-white pull-right" type="submit">Create Initial Service Account</a></li>
@stop
