@extends('system.install.master')

@section('stepsBar')
<li><a>Installation Preparation</a></li>
<li><a>Central Database Setup</a></li>
<li><a>Administrator Account</a></li>
<li class="active"><a>Service Account Setup</a></li>
<li><a>Complete Setup</a></li>
@stop

@section('startForm')
{{ Form::open(array('action' => 'System\SetupController@postCreateServiceAccount', 'method' => 'POST', 'class' => 'form-horizontal')) }}
@stop

@section('endForm')
{{ Form::close() }}
@stop

@section('content')

<h3>Initial Service Account Creation</h3>
<hr>
<p>The system needs at least one service level account to function properly. You need to create this now.</p>

<div class="form-group">
<label class="col-sm-2 control-label" for="aname">Account Name:</label>
  <div class="col-sm-8">
    <input type="text" name="aname" id="aname" class="form-control" placeholder="Service Account Name">
    <br><br>
    <input type="checkbox" name="defaultd" class="form-control"> Install demo data.
</div>
</div>

@stop

@section('pagers')
<li class="previous"><a href="{{ action('System\SetupController@getCreateAdmin') }}">Add Another Administrator</a></li>
<li class="next"><button class="btn btn-white pull-right" type="submit">Create Service Level Account</button></li>
@stop
