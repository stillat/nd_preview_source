@extends('system.install.master')

@section('stepsBar')
<li class="active"><a>Installation Preparation</a></li>
<li><a>Central Database Setup</a></li>
<li><a>Administrator Account</a></li>
<li><a>Service Account Setup</a></li>
<li><a>Complete Setup</a></li>
@stop

@section('content')

<h3>Before you begin your installation</h3>
<hr>
<div class="row">
    <div class="col-md-3 col-sm-3">
        <img src="{{ app_url() }}/assets/img/concept.jpg" class="img-responsive">
    </div>
    <div class="col-md-9 col-sm-9">
        <p>Welcome to the ND Counties <strong>Enterprise</strong> level product. When using this product, your company, organization or local government offices can rest assured that they are receiving the same quality product as the managed, cloud-based product. Our enterprise product provides the same level of quality with the added resassurance that everything runs behind your organizations firewalls on your own hardware for maximum control.</p>
        <p>To ensure the smoothest possible installation of this system, please read and ensure that the following items have been addressed before continuing with this automated installation system.</p>
    </div>
</div>

<h4><i class="fa fa-globe text-info"></i> Check your <strong>regional</strong> configuration</h4>
<p>Double check that your web servers and database servers are running with compatible time zone configurations.</p>
<p>The <strong>timezone</strong> configuration for the ND Counties Enterprise product is currently set to "<code>{{{ $config['appTimezone'] }}}</code>".</p>
<p>The public <strong>URL</strong> for the product is currently set to "<code>{{{ $config['appUrl'] }}}</code>".</p>

<h4><i class="fa fa-hdd-o text-info"></i> Check your <strong>database</strong> configuration</h4>
<p>Ensure that your system administrator has properly configured a database server that the ND Counties Enterprise product can connect to.</p>
<p>Right now the system is set to use the "<code>{{{ $config['dbConnection'] }}}</code>" connection. In addition, the following settings have been set:</p>
<ul class="list-unstyled">
    <li><strong>Host</strong>: <code>{{{ mask($config['dbConnectionDetails']['host'], null, strlen($config['dbConnectionDetails']['host'])-(strlen($config['dbConnectionDetails']['host'])/2)) }}}</code> <a title="" data-placement="right" data-toggle="tooltip" class="tooltips" type="button" data-original-title="We mask this for your security"><i class="fa fa-question-circle"></i></a></li>
    <li><strong>Database</strong>: <code>{{{ $config['dbConnectionDetails']['database'] }}}</code></li>
    <li><strong>Username</strong>: <code>{{{ $config['dbConnectionDetails']['username'] }}}</code></li>
    <li><strong>Password</strong>: <code>{{{ mask($config['dbConnectionDetails']['password'], null, strlen($config['dbConnectionDetails']['password'])-(strlen($config['dbConnectionDetails']['password'])/4)) }}}</code> <a title="" data-placement="right" data-toggle="tooltip" class="tooltips" type="button" data-original-title="We mask this for your security"><i class="fa fa-question-circle"></i></a></li>
</ul>

@if($config['dbConnectionDetails']['driver'] == 'mysql' and $config['dbConnectionDetails']['password'] == 'root')
<div class="alert alert-warning">
  <strong>Warning!</strong> It is recommended that you change the default password. You can learn more about this here: <a href="http://dev.mysql.com/doc/refman/5.5/en/resetting-permissions.html" target="_blank">http://dev.mysql.com/doc/refman/5.5/en/resetting-permissions.html <i class="fa fa-external-link"></i></a>
</div>
@endif

<h4><i class="fa fa-lock text-info"></i> Check your <strong>security</strong> configuration</h4>
<p>It is <strong>important</strong> that your system administrator has generated a unique, non-disclosed key for use with the ND Counties Enterprise level product. Do <strong>not</strong> reuse this key, or share it. Only reuse this key if you need to ensure password logins remain consistent between servers.</p>
<ul class="list-unstyled">
    <li><strong>Key</strong>: <code>{{{ mask($config['appKey'], null, strlen($config['appKey'])-(strlen($config['appKey'])/8)) }}}</code> <a title="" data-placement="right" data-toggle="tooltip" class="tooltips" type="button" data-original-title="We mask this for your security"><i class="fa fa-question-circle"></i></a></li>
</ul>
<hr>
<p>When you are ready to begin the installation process, click "Start Installation" below.</p>

@stop

@section('pagers')
<li class="next"><a href="{{ action('System\SetupController@getSetupDatabase') }}">Start Installation <i class="fa fa-chevron-circle-right"></i></a></li>
@stop