@extends('system.install.master')

@section('stepsBar')
<li><a>Installation Preparation</a></li>
<li class="active"><a>Central Database Setup</a></li>
<li><a>Administrator Account</a></li>
<li><a>Service Account Setup</a></li>
<li><a>Complete Setup</a></li>
@stop

@section('content')
<h3>Database Setup</h3>
<hr>
<p>The installer will attempt to create to create the database systems required to use the product. This installer will not overwrite database tables if they already exist.</p>
@stop


@section('pagers')
<li class="previous"><a href="{{ action('System\SetupController@getIndex') }}">Back</a></li>
<li class="next"><a href="{{ action('System\SetupController@getInstallDatabaseSystems') }}">Install Central Database System</a></li>
@stop