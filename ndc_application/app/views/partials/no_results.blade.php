<div class="col-sm-5 col-sm-offset-3 well">
    <div class="row">
        <div class="col-sm-3">
            <h1><span class="fa-stack fa-lg">
                <i class="fa fa-coffee fa-stack-1x" style="font-size: 37px;margin-left: -6px;margin-top: -4px;"></i>
                <i class="fa fa-search fa-stack-2x"></i>
            </span></h1>
        </div>
        <div class="col-sm-9">
            <h2>No work records</h2>
            <p>There are no work records to show right now. This could be because there are no records, or because the year or search filters are to specific or narrow.</p>

            <br>
            <a class="btn btn-block btn-primary" href="{{ action('App\Work\WorkLogsController@create') }}">Add a new work record</a>
        </div>
    </div>
</div>