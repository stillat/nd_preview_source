<div class="col-sm-5 col-sm-offset-3 well" style="margin-top: 50px;">
    <div class="row">
        <div class="col-sm-3">
            <h1><span class="fa-stack fa-lg">
                <i class="{{ $icon }} fa-stack-1x" style="font-size: 37px;margin-left: -6px;margin-top: -4px;"></i>
                <i class="fa fa-search fa-stack-2x"></i>
            </span></h1>
        </div>
        <div class="col-sm-9">
            <h2>No Search Results Available</h2>
            <p>There are no search results to display right now. This could be because no records exist, or because the search terms are to specific.</p>
        </div>
    </div>
</div>