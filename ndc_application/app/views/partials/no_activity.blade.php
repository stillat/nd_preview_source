<div class="col-sm-5 col-sm-offset-3 well">
    <div class="row">
        <div class="col-sm-3">
            <h1><span class="fa-stack fa-lg">
                <i class="fa ndc-bicycle fa-stack-1x" style="font-size: 37px;margin-left: -6px;margin-top: -4px;"></i>
                <i class="fa fa-search fa-stack-2x"></i>
            </span></h1>
        </div>
        <div class="col-sm-9">
            <h2>No recent activity</h2>
            <p>There is no recent activity to show here. Any changes to work records or any supporting records (such as activities or employees) will also be reported here by anyone who uses the account. Changes to personal accounts are not displayed here.</p>
        </div>
    </div>
</div>