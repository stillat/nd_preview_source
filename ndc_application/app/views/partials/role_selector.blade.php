<div id="roleselectorholder">
    <div class="dropdown">
        <button class="btn btn-white btn-block dropdown-toggle" type="button" id="dropDownRoleSelector" data-toggle="dropdown" aria-expanded="true">
            {{ $serviceRoles[$currentRole] }}
            <span class="caret"></span>
        </button>
        <ul class="dropdown-menu" role="menu" aria-labelledby="dropDownRoleSelector" id="roleSelectorDropDown">
            <iframe style="display: none;" name="role_change_target"></iframe>
            @foreach($serviceRoles as $id => $role)
            <li role="presentation"><a role="menuitem" tabindex="-1" data-mtoggle="{{ $id }}" target="role_change_target" href="{{{ action('App\UtilityController@getSwitchRole', array($id)) }}}">{{ $role }}</a></li>
            @endforeach
        </ul>
    </div>
</div>