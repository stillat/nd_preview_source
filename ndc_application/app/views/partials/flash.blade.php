@if(Session::has('flash_message'))
<div class="row" id="flashMessageHolder" data-alert="flash-message" data-level="{{ Session::get('flash_message_style', 'alert-info') }}">
	<div class="col-md-12">
		<div class="alert {{ Session::get('flash_message_style', 'alert-info') }} nomargin">
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
			@if(Session::has('flash_message_title'))
			<h4>{{ Session::get('flash_message_title') }}</h4>
			@endif
			
			<p class="read-more">{{ Session::get('flash_message') }}</p>

			@if(Session::has('flash_message_info'))
			<p>For more information about this message see: <a href="{{ Session::get('flash_message_info') }}" target="_blank">{{ Session::get('flash_message_info') }}</a>.</p>
			@endif

		</div>
	</div>
</div>
<?php Session::forget('flash_message'); ?>
@endif