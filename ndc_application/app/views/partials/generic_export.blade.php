<button type="button" class="btn btn-white dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
    <i class="fa fa-download inline-icon"></i> Export <span class="caret"></span>
</button>
<ul class="dropdown-menu" role="menu" <?php if (isset($margin)) { echo "style='margin-left: ".$margin."'"; } ?>>
    <li><a href="{{ export_csv($export_data).$restrictionString }}">Export this page</a></li>
    <li><a href="{{ export_csv($export_data) }}">Export all {{{ $title }}}</a></li>
</ul>