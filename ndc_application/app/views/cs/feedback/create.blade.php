@extends('layouts.master')

@section('pageTitle')
<i class="fa fa-comments-o"></i> Welcome to the feedback center. <small>What would you like to share today?</small>
@stop


@section('content')
<div class="row">
    {{ Form::open(array('route' => 'customer-service.feedback.store', 'method' => 'post', 'class' => 'form-horizontal')) }}
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4 class="panel-title">You can leave your feedback by completing the form below.</h4>
        </div>
        <div class="panel-body">
         <div class="col-sm-8">
             <div class="form-group is-required">
                <label class="col-sm-2 control-label">Subject:</label>
                <div class="col-sm-8">
                    <input type="text" name="subject" class="form-control {{{ user_form_size() }}}" value="{{{ Input::old('subject', '') }}}">
                    {{ Form::errorMsg('subject') }}
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-2 control-label">Category:</label>
                <div class="col-sm-6">
                    {{ Form::select('category_id', $categories, 0, array('style' => 'width: 100%;', 'class' => 'form-control mousetrap chosen-select'.user_form_size())) }}
                    {{ Form::errorMsg('category_id') }}
                </div>
            </div>

            <div class="form-group is-required">
                <label class="col-sm-2 control-label">Description:</label>
                <div class="col-sm-8">
                <textarea id="wysiwyg" name="message" placeholder="Enter your feedback message here ..." class="form-control autogrow" rows="10">{{{ Input::old('message', '') }}}</textarea>
                    {{ Form::errorMsg('message') }}
                </div>
            </div>

            <div class="form-group col-sm-offset-2">
               <div class="btn-group mr5">
                <button type="submit" value="addNew" name="submit" class="btn btn-primary">Save and add another activity</button>
            </div>

            <div class="btn-group mr5">
                <button type="reset" class="btn btn-default">Reset</button>
            </div>
        </div>
    </div>
    <div class="col-sm-4">
        <h3>Need some help?</h3>
        <p>When submitting feedback about the ND Counties service, be sure that all messages and subject lines are clear and descriptive. The more information you can give us about a certain problem, the better the chance that we will be able to help resolve any issues or understand your requests.</p>
        <p><img src="{{ app_url() }}/assets/img/idea.jpg" class="img-responsive"></p>
    </div>
</div>
</div>
{{ Form::close() }}
</div>

@stop