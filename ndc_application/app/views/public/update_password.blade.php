<!DOCTYPE html>
<html>
<head>
	<title>ND Counties</title>
    {{ UI::getCss() }}
    <?php  echo $__env->make('integration.universal_analytics'); ?>
</head>
<body class="signin">
    <div class="container text-center">
      {{ Form::open(array('method' => 'post', 'action' => ['HomeController@postUpdatePassword', $token], 'class' => 'form-signin')) }}
        <h2><img src="{{{ app_url() }}}/assets/img/dark.png" alt="ND Counties" style="width: 60px; vertical-align: middle; margin-top: -8px"> Update Password</h2>

      <input type="email" name="email" class="form-control input-lg" placeholder="Email address" value="{{{ Input::old('email', '') }}}" required autofocus>
     {{ Form::errorMsg('email') }}

      <input type="password" name="password" class="form-control input-lg" placeholder="Password" required>
      {{ Form::errorMsg('password') }}
      <input type="password" name="password_confirmation" class="form-control input-lg" placeholder="Confirm new password" required>
      {{ Form::errorMsg('password_confirmation') }}
      <br>
      <button class="btn btn-lg btn-primary btn-block" type="submit">Reset My Password</button>
      {{ Form::close() }}
      <hr>
      <a href="{{{ url('/') }}}">ND Counties</a>
  </div>
</body>
</html>