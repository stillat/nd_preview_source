<!DOCTYPE html>
<html>
<head>
	<title>ND Counties</title>
    {{ UI::getCss() }}
  <?php  echo $__env->make('integration.universal_analytics'); ?>
</head>
<body class="signin">
    <div class="container text-center">
      {{ Form::open(array('method' => 'post', 'route' => 'authorize', 'class' => 'form-signin')) }}
      <h2><img src="{{{ app_url() }}}/assets/img/dark.png" alt="ND Counties" style="width: 60px; vertical-align: middle; margin-top: -8px"> Code Request</h2>
      <p>You're almost there! Because of your account's status, you must also provide an authorization code to access this account.</p>
      @if(Session::has('loginFailed'))
      <div class="alert alert-danger">
          The provided username and password combination was incorrect.
      </div>
      @endif
      @if(Session::has('userLoggedOut'))
      <div class="alert alert-info">
          You were successfully logged out of your account.
      </div>
      @endif
      @if(Session::has('serviceAccountsRevoked'))
      <div class="alert alert-warning">
          Your service level account privleges have been revoked for this session. If you believe this is an error, contact ND Counties Customer Support.
      </div>
      @endif
      <input type="text" name="oacode" class="form-control input-lg" placeholder="Authorization Code" required autofocus>
      <br>
      <button class="btn btn-lg btn-primary btn-block" type="submit"><i class="fa fa-key"></i> Continue with Log In</button>
      {{ Form::close() }}
      <hr>
      <a href="{{{ url('/') }}}">ND Counties</a> | <a href="{{{ action('HomeController@getResetPassword') }}}">Lost your password?</a>
  </div>
</body>
</html>