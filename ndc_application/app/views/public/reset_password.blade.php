<!DOCTYPE html>
<html>
<head>
	<title>ND Counties</title>
    {{ UI::getCss() }}
  <?php  echo $__env->make('integration.universal_analytics'); ?>
</head>
<body class="signin">
    <div class="container text-center">
      {{ Form::open(array('method' => 'post', 'action' => 'HomeController@postResetPassword', 'class' => 'form-signin')) }}
      <h2><img src="{{{ app_url() }}}/assets/img/dark.png" alt="ND Counties" style="width: 60px; vertical-align: middle; margin-top: -8px"> Password Reset</h2>

      <input type="email" name="email" class="form-control input-lg" placeholder="Email address" value="{{{ Input::old('email', '') }}}" required autofocus>
      <br>
      <button class="btn btn-lg btn-primary btn-block" type="submit">Send reset information</button>
      {{ Form::close() }}
      <hr>
      <a href="{{{ url('/') }}}">ND Counties</a> | <a href="{{{ action('HomeController@getResetPassword') }}}">Lost your password?</a>
  </div>
</body>
</html>