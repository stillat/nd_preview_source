<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE-edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
    <link rel="shortcut icon" href="{{ Config::get('app.url') }}/favicon.ico" type="image/ico">
    <script src="{{{ app_url() }}}/assets/js/pace.min.js{{ UI::getBuster() }}"></script>
    @include('integration.universal_analytics')

    @if($currentAccount !== null)
        <title>{{{ Str::limit($currentAccount->account_name, 10) }}} - @yield('title', 'ND Counties')</title>
    @else
        <title>@yield('title', 'ND Counties')</title>
    @endif
    {{ UI::getCss() }}
    @yield('additionalStyles')

    <!--[if lt IE 9]>
        <script src="{{ app_url() }}/assets/js/html5shiv.js{{ UI::getBuster() }}"></script>
        <script src="{{ app_url() }}/assets/js/respond.min.js{{ UI::getBuster() }}"></script>
        <![endif]-->
</head>

<body class="stickyheader  @yield('extraBodyClass')"

        @yield('extraBody')>

@yield('prependBody')

<section>

    <div class="leftpanel sticky-leftpanel hidden-print">


        <div class="logopanel">


            <a href="{{ Config::get('app.url') }}" class="ndc-logo"><h1><span style="color:#EAEAEA;margin-right:5px;"><i class="fa fa-cloud"></i> ND</span> Counties</h1></a>


        </div>


        <div class="leftpanelinner">


            <div class="visible-xs hidden-sm hidden-md hidden-lg">


                <div class="media userlogged" style="margin-top: 10px;">


                    <img style="border-color: #2980b9;" src="{{{ $userProfileImage }}}" class="media-object">


                    <div class="media-body" style="color: black;">


                        <h4>{{{ str_limit($authUser->first_name .' '.$authUser->last_name, 15, '...') }}}</h4>


                        <span>{{{ str_limit($authUser->email, 30, '...') }}}</span>


                    </div>


                </div>


                <h5 class="sidebartitle actitle">Account</h5>


                <ul class="nav nav-pills nav-stacked nav-bracket mb30">


                    @if($showChooseAccount)



                        <li><a href="{{ action('App\ServiceAccountController@getDestroy') }}"><i


                                        class="fa fa-cloud"></i> Choose another account</a></li>



                        <li class="divider"></li>



                    @endif


                    <li><a href="{{ url('/settings/user/contact') }}"><i class="fa fa-user"></i> <span>My Profile Settings</span></a>


                    </li>


                    <li><a href="{{ route('logout') }}"><i class="glyphicon glyphicon-log-out"></i> Log Out</a></li>


                </ul>


            </div>


            <h4 class="sidebartitle" style="font-size:12px;"><strong><i
                            class="fa fa-cloud"></i> {{{ Str::limit($currentAccount->account_name, 15) }}}</strong></h4>


            <h5 class="sidebartitle">Explore</h5>

            @if(Auth::user()->admin || !wl_reset_settings()->wl_enable_invoice_compatibility)
            @section('roleSelector')
                @include('partials.role_selector')
            @show
            @endif

            @section('leftMenu')

                <?php $routePathToRestore = Menu::getRoutePath(); UI::correctMenu(); ?>



                {{ $mainMenu }}



                <?php Menu::setRoutePath($routePathToRestore); ?>



            @show


        </div>


    </div>


    <div class="mainpanel">


        <div class="headerbar hidden-print">


            <div class="header-left">


                <div class="topnav">


                    <a class="menutoggle" id="menuToggler"><i class="fa fa-bars"></i></a>


                    <span class="ndc_device_logo">ND Counties</span>


                    <ul class="nav nav-horizontal hidden-xs hidden-sm">

                        @if($currentAccount !== null)

                            <li data-toggle="popover" data-trigger="hover" data-placement="bottom"
                                data-content="Return to the general information dashboard"><a href="{{ url('/') }}"><i
                                            class="fa fa-dashboard"></i></a></li>

                            <li data-toggle="popover" data-trigger="hover" data-placement="bottom"
                                data-content="View work records"><a href="{{ route('work-logs.index') }}"
                                                                    class="tp-icon"><i class="fa fa-coffee"></i><span
                                            class="badge"
                                            style="right: 0px;top: 10px;line-height: 13px;background-color: rgba(217, 223, 223, 0);position: absolute;"><i
                                                class="fa fa-table"></i></span></a></li>

                            <li data-toggle="popover" data-trigger="hover" data-placement="bottom"
                                data-content="Add a new work record"><a href="{{ route('work-logs.create') }}"
                                                                        class="tp-icon"><i
                                            class="fa fa-coffee"></i><span class="badge"
                                                                           style="right: 0px;top: 10px;line-height: 13px;background-color: rgba(217, 223, 223, 0);position: absolute;"><i
                                                class="fa fa-plus"></i></span></a></li>

                            <li data-toggle="popover" data-trigger="hover" data-placement="bottom"
                                data-content="Reporting and Search"><a
                                        href="{{ action('App\Reporting\ReportsController@getRun') }}"><i
                                            class="ndc-chart-pie"></i></a></li>
                            <li data-toggle="popover" data-trigger="hover" data-placement="bottom"
                                data-content="Search Only"><a
                                        href="{{ action('App\Reporting\ReportsController@getRun') }}?searchOnly=true"><i
                                            class="fa fa-search"></i></a></li>

                        @endif

                    </ul>


                </div>


            </div>


            <div class="header-right">


                <ul class="headermenu">


                    @if($authUser->admin)



                        <li><a href="{{ action('Admin\Utilities\SystemWideUtilitiesController@getPostNotification') }}"
                               data-toggle="modal" data-target="#modalHousing"
                               class="btn btn-default dropdown-toggle tp-icon"


                               style="padding-top: 15px; color: white;"><i class="fa fa-bell" data-toggle="popover"
                                                                           data-trigger="hover" data-placement="bottom"
                                                                           data-container="body"
                                                                           data-content="Post a notification to users"></i></a>


                        </li>



                    @endif
                    <li class="hidden-xs">


                        <div class="btn-group">


                            <button type="button" class="btn dropdown-toggle" data-toggle="dropdown">

                                <img class="profile-image" src="{{ $userProfileImage }}" style="background: white;"/>

                                {{{ str_limit($authUser->first_name.' '.$authUser->last_name, 15, '...') }}}

                                <span class="caret"></span>

                            </button>


                            <ul class="dropdown-menu dropdown-menu-usermenu pull-right">


                                @if($showChooseAccount)



                                    <li><a href="{{ action('App\ServiceAccountController@getDestroy') }}"><i


                                                    class="fa fa-cloud"></i> Choose another account</a></li>



                                    <li class="divider"></li>



                                @endif


                                @if(Auth::user()->admin || !wl_reset_settings()->wl_enable_invoice_compatibility)
                                @foreach($serviceRoles as $id => $role)
                                    <li>
                                        <a role="menuitem" data-mtoggle="{{ $id }}" target="role_change_target"
                                           href="{{{ action('App\UtilityController@getSwitchRole', array($id)) }}}">{{ $role }}</a>
                                    </li>
                                @endforeach
                                @endif


                                <li class="divider"></li>


                                <li><a href="{{ url('/settings/user/contact') }}"><i class="fa fa-user"></i> My Profile


                                        Settings</a></li>


                                <li class="divider"></li>


                                <li><a href="{{ route('logout') }}"><i class="glyphicon glyphicon-log-out"></i> Log Out</a>


                                </li>


                            </ul>


                        </div>


                    </li>


                </ul>


            </div>


        </div>


        @section('pageHeader')



            <div class="pageheader" id="systemPageHeader">


                <div class="row">


                    <h2 style="font-weight:normal;">


                        @yield('pageTitle')


                    </h2>


                    <div class="breadcrumb-wrapper hidden-print" style="top: 20px;">


                        @yield('buttonBar')


                    </div>


                </div>


                @yield('headerExtra')


            </div>



        @show



        @include('partials.messages.database_maintenance')


        <div class="contentpanel">


            @yield('content')


        </div>


    </div>


    <div class="rightpanel"></div>


</section>


<div class="modal fade" id="modalHousing"></div>


@yield('appendBody')



@include('layouts.settings')



{{ UI::getJs() }}







@yield('additionalScripts')


</body>


</html>



