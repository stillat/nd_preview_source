<div class="modal fade" id="removeResourceDialog" tabindex="-1" role="dialog" aria-labelledby="removeResourceDialogLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 id="removeResourceDialogLabel"><i class="fa fa-trash"></i> {{{ $title }}}</h4>
      </div>
      <div class="modal-body">
        @if(isset($content))
        {{ $content }}
        @else
        Are you sure you want to remove this resource?
        @endif
      </div>
      <div class="modal-footer">
      {{ Form::open(array('action' => $action, 'method' => 'delete')) }}
        <input type="hidden" name="removeContext" id="removeContext">
        <input type="hidden" name="removeMultiple" id="removeMultipleFlag" value="0">
        @if (isset($extraFields))
        @foreach($extraFields as $fieldName => $value)
        {{ Form::hidden($fieldName, $value) }}
        @endforeach
        @endif
        <input type="hidden" name="redirectList" id="removeRedirectList" value="<?php
        if (isset($redirectToList)) {
            echo '1';
        } else {
            echo '0';
        }
        ?>">
        <button type="submit" class="btn btn-danger">
        @if (isset($agreeText))
        {{ $agreeText }}
        @else
        Remove this resource
        @endif
        </button>
        <button type="button" class="btn btn-default" id="removeContextCancel" data-dismiss="modal">Cancel</button>
        {{ Form::close() }}
      </div>
    </div>
  </div>
</div>