<div class="modal-dialog">
    {{ Form::open(['action' => 'App\Search\SearchController@'.$action, 'method' => 'post', 'class' => 'form-horizontal'])  }}
    <div class="modal-content">
        <div class="modal-header">
            <a class="close" data-dismiss="modal">&times;</a>

            <h3><i class="fa fa-search"></i> {{{ $title }}} Search</h3>
        </div>
        <div class="modal-body">
            <fieldset>
                <div class="input-grouping">
                    <label id="changeOrgLabel" class="col-sm-3 control-label"
                           style="margin-top: 6px; text-align: right;">Search Terms</label>
                    <div class="col-sm-9">
                        <div class="input-group"><span class="input-group-addon"><i
                                        class="fa fa-search"></i></span>

                            <input type="text" name="q" id="searchBox"
                                   class="form-control mousetrap" placeholder="Enter your search terms here" value="{{{ $oldValue }}}">
                        </div>
                    </div>
                    <div class="ckbox ckbox-default col-sm-offset-3 col-sm-9" style="margin-left: 150px;
    margin-top: 15px;">
                        <input type="checkbox" name="it" id="includeTrashed" <?php if($includeTrashedChecked) { echo 'checked="checked"'; } ?>>
                        <label for="includeTrashed">
                            Include previously deleted items in search results
                        </label>
                    </div>
                </div>
            </fieldset>
        </div>
        <div class="modal-footer">
            <button type="submit" class="btn btn-primary"><i class="fa fa-search"></i> Search</button>
            <a class="btn btn-default" data-dismiss="modal">Close</a>
        </div>
    </div>
    {{ Form::close() }}
</div>

<script>$(document).ready(function(){$('#searchBox').focus(); });</script>