@extends('layouts.modal')
<?php $okayText = 'Export Range'; ?>
@section('header')
    <i class="fa fa-download"></i> Export Work Records Date Range
@stop

@section('formOpen')
    {{ Form::open(['action' => 'App\DataTools\DataExportController@getRunExport', 'method' => 'get', 'class' => 'form-horizontal']) }}
@stop

@section('formClose')
    {{ Form::close() }}
@stop

@section('body')
    <input type="hidden" id="report_start_date" name="start_date" value="{{ $startDate }}">
    <input type="hidden" id="report_end_date" name="end_date" value="{{ $endDate }}">
    <input type="hidden" id="export_data" name="export_data" value="work_records">
    <input type="hidden" id="export_type" name="export_type" value="list">
    <fieldset>
        <p class="col-sm-offset-2"><strong>Date Range</strong></p>
        <div id="daterange" class="btn btn-white n-mt-5 col-sm-offset-2 mb15"><i class="fa fa-calendar-o inline-icon" style="margin-right:10px;"></i> <span> September 10, 2014 - September 24, 2014</span> <b class="caret"></b></div>
    </fieldset>
    <script>
        $(document).ready(function(){
            makeDateRangePicker();

            $('#default_submit').click(function()
            {
                $('#modalHousing').modal('hide');
                return;
            });
        });
    </script>
@stop