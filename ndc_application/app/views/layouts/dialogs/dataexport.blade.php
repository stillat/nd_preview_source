<div class="modal fade" id="removeResourceDialog" tabindex="-1" role="dialog" aria-labelledby="removeResourceDialogLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 id="removeResourceDialogLabel">{{ $title }}</h4>
      </div>
      <div class="modal-body">
        Are you sure you want to remove this resource?
      </div>
      <div class="modal-footer">
      {{ Form::open(array('action' => $action, 'method' => 'delete')) }}
        <input type="hidden" name="removeContext" id="removeContext">
        <button type="submit" class="btn btn-danger">Remove this resource</button>
        <button type="button" class="btn btn-default" id="removeContextCancel" data-dismiss="modal">Cancel</button>
        {{ Form::close() }}
      </div>
    </div>
  </div>
</div>