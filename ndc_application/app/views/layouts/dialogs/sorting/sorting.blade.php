<script src="{{{ app_url() }}}/assets/js/pace.min.js{{ UI::getBuster() }}"></script>
<div class="modal-dialog">
    {{ Form::open(['action' => 'App\Search\SortingController@'.$action, 'method' => 'post'])  }}
    <div class="modal-content">
        <div class="modal-header">
            <a class="close" data-dismiss="modal">&times;</a>

            <h3><i class="fa fa-sort"></i> {{{ $title }}}</h3>
        </div>
        <div class="modal-body nopadding" style="max-height: 400px !important;overflow: auto;">
            <div class="table-responsive">
                <table class="table table-bordered table-striped table-hover">
                    <thead>
                    <tr>
                        <th style="width: 300px;">Sorting Column</th>
                        <th style="width: 300px;">Sort Order</th>
                        <th class="pull-right"><a class="btn btn-white" id="addSortColumnActivator"><i class="fa fa-plus"></i> Sort Another Column</a></th>
                    </tr>
                    </thead>
                    <tbody id="sortColumnHolder">

                    </tbody>
                </table>
            </div>
        </div>
        <div class="modal-footer">
            <button type="submit" name="real_action" value="reset" class="btn btn-link pull-left"><i class="fa fa-refresh"></i> Reset {{{ $title }}}</button>
            <button type="submit" name="real_action" value="update" class="btn btn-primary">Update {{{ $title  }}}</button>
            <a class="btn btn-default" data-dismiss="modal">Close</a>
        </div>
    </div>
    {{ Form::close() }}
</div>

<script type="text/html" id="sortingColumn_tmpl">
    <%* sortingCount++ %>
    <tr data-sorting="<%:~getSortingCount()%>" id="sortingSelector<%:~getSortingCount()%>">
        <td>
            <select name="sorting_names[]" id="sortingName<%:~getSortingCount()%>" class="form-control" data-sorting="<%:~getSortingCount()%>">
                @foreach($sortingColumns as $value => $text)
                    <option value="{{{ $value }}}" <%:~isSelected('{{{ $value }}}', column)%>>{{{ $text }}}</option>
                @endforeach
            </select>
        </td>
        <td>
            <select name="sorting_modes[]" id="sortingMode<%:~getSortingCount()%>" class="form-control" data-sorting="<%:~getSortingCount()%>">
                @foreach($sortingModes as $value => $text)
                    <option value="{{{ $value }}}" <%:~isSelected('{{ $value }}', mode)%>>{{{ $text }}}</option>
                @endforeach
            </select>

        </td>
        <td><a class="btn btn-link remove_sorting" data-sorting="<%:~getSortingCount()%>" style="float:right;text-align:right;"><i class="fa fa-delete-o"></i> Remove this sorting column</a></td>
    </tr>
</script>
<script>
    var NDC_CURRENT_SORTING_SETTINGS = {{ json_encode($settings) }};
</script>
<script src="{{{ app_url() }}}/assets/js/jsrender.min.js" defer></script>
<script src="{{{ app_url() }}}/assets/js/sorting_system.min.js{{ UI::getBuster() }}"></script>