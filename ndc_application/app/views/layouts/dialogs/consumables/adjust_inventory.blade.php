@extends('layouts.modal')
<?php $okayText = 'Adjust Inventory Level'; ?>
@section('header')
    <i class="fa ndcf-weight-scale"></i> Adjust Inventory Level
@stop

@section('body')
    <p>You are about to manually adjust the inventory level for this consumable.</p>
    <div class="form-group">
        <label class="col-sm-2 control-label" for="adjustmentAmount">Adjustment</label>
        <div class="col-sm-10">
            <input type="number" step="any" class="form-control" id="adjustmentAmount" name="adjustment" placeholder="Adjustment amount" autofocus="autofocus"/>
        </div>
    </div>
    <script>
        $(document).ready(function(){
           $('#adjustmentAmount').focus();
        });
    </script>
@stop

@section('formOpen')
    {{ Form::open(['action' => ['App\Consumables\ConsumableUtilitiesController@postAdjustInventoryLevel', $consumable], 'method' => 'post', 'class' => 'form-horizontal']) }}
@stop

@section('formClose')
    {{ Form::close() }}
@stop