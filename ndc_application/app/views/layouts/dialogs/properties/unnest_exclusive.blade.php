@extends('layouts.modal')
<?php $okayText = 'Un-nest this exclusive property'; ?>
@section('header')
    <i class="fa fa-level-up"></i> Un-nest this property
@stop

@section('formOpen')
    {{ Form::open(['action' => ['App\UtilityController@postUnNestExclusiveProperty', $nestedProperty->id], 'method' => 'post', 'class' => 'form-horizontal']) }}
@stop

@section('formClose')
    {{ Form::close() }}
@stop

@section('body')
    <p>You are about to un-nest <strong>{{{ $nestedProperty->code.' - '.$nestedProperty->name }}}</strong> and make it available in the main list.</p>
    <p>You are about to un-nest this property. This property is currently only available to it's parent property.
        After it is no longer nested, it will become available in the main properties list.</p>
@stop