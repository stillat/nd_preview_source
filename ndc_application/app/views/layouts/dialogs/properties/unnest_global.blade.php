@extends('layouts.modal')
<?php $okayText = 'Un-nest this property'; ?>
@section('header')
    <i class="fa fa-level-up"></i> Un-nest this property
@stop

@section('formOpen')
    {{ Form::open(['action' => ['App\UtilityController@postUnNestGlobalProperty', $parentProperty->id, $nestedProperty->id], 'method' => 'post', 'class' => 'form-horizontal']) }}
@stop

@section('formClose')
    {{ Form::close() }}
@stop

@section('body')
    <p>You are about to un-nest <strong>{{{ $nestedProperty->code.' - '.$nestedProperty->name }}}</strong> from <strong>{{{ $parentProperty->code.' - '.$parentProperty->name }}}</strong>.</p>
    <p>You are about to un-nest this property. This property will still be available in the properties list.</p>
@stop