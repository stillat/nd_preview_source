@extends('layouts.modal')
<?php $okayText = 'Change Invoice Number'; ?>
@section('header')
    <i class="fa ndcf-bill inline-icon"></i> Change Invoice Number
@stop

@section('formOpen')
    {{ Form::open(['action' => ['App\Finances\Invoices\InvoiceController@postUpdateInvoiceNumber', $invoice->id], 'method' => 'post', 'class' => 'form-horizontal']) }}
@stop

@section('formClose')
    {{ Form::close() }}
@stop

@section('body')
    <p>You are about to update the invoice number for this invoice. You will need to use the new invoice number when searching for this invoice in the future. The old invoice number will become available for any new invoices.</p>

    <div class="form-group">
        <label class="col-sm-3 control-label">Invoice Number</label>
        <div class="col-sm-8">
            <input type="text" class="form-control" name="invoice_number" placeholder="Enter a new invoice number" value="{{{ $invoice->invoice_number }}}" autofocus="autofocus">
        </div>
    </div>

@stop