@extends('layouts.modal')
<?php $okayText = 'Change Invoice Accounts'; ?>
@section('header')
    <i class="fa ndcf-accounts-book inline-icon"></i> Update Invoice Accounts
@stop

@section('formOpen')
    {{ Form::open(['action' => ['App\Finances\Invoices\InvoiceController@postUpdateAccounts', $invoice->id], 'method' => 'post', 'class' => 'form-horizontal']) }}
@stop

@section('formClose')
    {{ Form::close() }}
@stop

@section('body')
    <p>You are about to update the accounts for this invoice. Updating the accounts will affect the account balances of
        all finance accounts involved, and will apply and only transactions to the new accounts. This change can take
        several minutes to complete.</p>

    <div class="form-group">
        <label class="col-sm-3 control-label">Bill To Account</label>

        <div class="col-sm-8">
            <div class="input-group">
                <span class="input-group-addon">
                    <i class="fa ndcf-account-payable" style="font-size:17px;"></i>
                </span>
                {{ Form::select('bill_to_account', $accounts, $invoice->account_number, array('id' => 'destination', 'class' => 'form-control chosen-select', 'data-placeholder' => 'Choose a new bill to account account')) }}
            </div>
        </div>
    </div>

    <div class="form-group">
        <label class="col-sm-3 control-label">Pay To Account</label>

        <div class="col-sm-8">
            <div class="input-group">
                <span class="input-group-addon">
                    <i class="fa ndcf-accounts-receivable" style="font-size:17px;"></i>
                </span>
                {{ Form::select('pay_to_account', $accounts, $invoice->paid_to_account, array('id' => 'destination', 'class' => 'form-control chosen-select', 'data-placeholder' => 'Choose a new pay to account')) }}
            </div>
        </div>
    </div>

@stop