@extends('layouts.modal')
<?php $okayText = 'Lock Invoice'; ?>
@section('header')
    <i class="fa fa-lock"></i> Lock Invoice
@stop

@section('body')
    <p>You are about to lock this invoice. You cannot add new work records to a locked invoice. Any work records on this
        invoice cannot be updated while this invoice is locked. Locked invoices can still accept payments.</p>
@stop

@section('formOpen')
    {{ Form::open(['action' => ['App\Finances\Invoices\InvoiceController@postLockInvoice', $invoiceID]]) }}
@stop

@section('formClose')
    {{ Form::close() }}
@stop