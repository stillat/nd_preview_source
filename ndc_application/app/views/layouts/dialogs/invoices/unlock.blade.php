@extends('layouts.modal')
<?php $okayText = 'Unlock Invoice'; ?>
@section('header')
    <i class="fa fa-unlock"></i> Unlock Invoice
@stop

@section('body')
    <p>You are about to unlock this invoice. Unlocked invoices can be modified and have work records added or removed. This may allow unwanted changes to this invoice.</p>
@stop

@section('formOpen')
    {{ Form::open(['action' => ['App\Finances\Invoices\InvoiceController@postUnlockInvoice', $invoiceID]]) }}
@stop

@section('formClose')
    {{ Form::close() }}
@stop