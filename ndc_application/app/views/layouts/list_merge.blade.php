<div class="modal fade" id="changeOrgData" tabindex="-1" role="dialog" aria-labelledby="changeOrgDataLabel"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            {{ Form::open(['action' => 'App\UtilityController@postMergeRemoveData', 'method' => 'post']) }}
            <div class="modal-header">
                <button type="button" class="close modal-close" data-dismiss="modal"
                        aria-hidden="true">&times;</button>
                <h4 id="changeOrgDataLabel"></h4>
            </div>
            <div class="modal-body">
                <p id="changeOrgHelpText"></p>
                <fieldset>
                    <div class="input-grouping">
                        <label id="changeOrgLabel" class="col-sm-3 control-label" style="margin-top: 7px; text-align: right;"></label>
                        <div class="col-sm-9">
                            <div class="input-group">
                                <span class="input-group-addon" id="changeOrgIcon"></span>
                                <input type="text" name="new_org_data" id="orgDataSelector" class="form-control mousetrap " value="0">
                            </div>
                        </div>
                    </div>
                </fieldset>
            </div>
            <div class="modal-footer">
                <input type="hidden" name="changeOrgContext" id="changeOrgContext">
                <input type="hidden" name="changeOrgAction" id="changeOrgAction" value="-1">
                <input type="hidden" name="changeOrgExtra" id="changeOrgExtra">
                <button type="submit" class="btn btn-primary">Merge records</button>
                <button type="button" class="btn btn-default modal-close" data-dismiss="modal">Cancel</button>
            </div>
            {{ Form::close() }}
        </div>
    </div>
</div>