<div class="modal-dialog">
	<div class="modal-content">
        @yield('formOpen')
		<div class="modal-header">
			<a class="close" data-dismiss="modal">&times;</a>
			<h3>
				@yield('header')
			</h3>
		</div>
		<div class="modal-body">
			@yield('body')
		</div>
		@section('footer')
		<div class="modal-footer">
			<button type="submit" id="default_submit" class="btn btn-primary"><?php if (isset($okayText)) {
    echo $okayText;
} else {
    echo 'Save Changes';
} ?></button>
			<a class="btn btn-default" data-dismiss="modal">Close</a>
		</div>
		@show
		@yield('formClose')
	</div>
</div>