<script>
    <?php

        /** This part handles the new flash system. */
        echo 'var FLASH_AVAILABLE = '.(int)Session::has('flash_message').';';
    echo 'var FLASH_TITLE = '.json_encode(Session::get('flash_message_title', '')).';';
    echo 'var FLASH_STYLE = '.json_encode(Session::get('flash_message_style', 'info')).';';
    echo 'var FLASH_BODY = '.json_encode(Session::get('flash_message', '')).';';
        Session::forget('flash_message');

    echo Cache::rememberForever('ndc_js_hk_scriptAPI', function () {
      $variables = '';

      $variables .= 'var NDC_HOME_HK_API = \''.url('/').'\';';
      $variables .= 'var NDC_DISTRICT_HK_API = \''.url('districts').'\';';
      $variables .= 'var NDC_DEPTS_HK_API = \''.url('departments').'\';';
      $variables .= 'var NDC_PROPERTIES_HK_API = \''.url('properties').'\';';
      $variables .= 'var NDC_EQUIPMENT_HK_API = \''.url('equipment-units').'\';';
      $variables .= 'var NDC_RECORD_BASE_HK_API = \''.url('work-logs').'\';';
      $variables .= 'var NDC_CREATE_WORK_RECORD_HK_API = \''.action('App\Work\WorkLogsController@create').'\';';

      $variables .= 'var NDC_HK_FINANCE_ACCOUNT = \''.action('App\Finances\AccountsController@index').'\';';
    $variables .= 'var NDC_HK_FINANCE_INVOICE = \''.action('App\Finances\Invoices\InvoiceController@index').'\';';

    $variables .= 'var NDC_UTIL_ROLE = \''.action('App\UtilityController@getSwitchRole').'\';';
    $variables .= 'var NDC_UTIL_ROLE_ACC = \''.action('App\UtilityController@getSwitchRole', [0]).'\';';
    $variables .= 'var NDC_UTIL_ROLE_FIN = \''.action('App\UtilityController@getSwitchRole', [1]).'\';';

    $variables .= 'var NDC_SPROV_MEASUREMENT_UNIT = \''.action('App\Search\InlineProviderController@getMeasurementUnits').'\';';

    $variables .= 'var NDC_SPROV_EMPLOYEE = \''.action('App\Search\InlineProviderController@getEmployees').'\';';
    $variables .= 'var NDC_SPROV_ACTIVITY = \''.action('App\Search\InlineProviderController@getActivities').'\';';
    $variables .= 'var NDC_SPROV_DEPARTMENT = \''.action('App\Search\InlineProviderController@getDepartments').'\';';
    $variables .= 'var NDC_SPROV_DISTRICT = \''.action('App\Search\InlineProviderController@getDistricts').'\';';
    $variables .= 'var NDC_SPROV_PROJECT = \''.action('App\Search\InlineProviderController@getProjects').'\';';

        $variables .= 'var NDC_SPROV_PROPERTIES = \''.action('App\Search\InlineProviderController@getProperties').'\';';
        $variables .= 'var NDC_SPROV_PROPERTIES_DIRECT_ROAD = \''.action('App\Search\InlineProviderController@getDirectRoads').'\';';
        $variables .= 'var NDC_SPROV_PROPERTIES_DIRECT_GENERIC = \''.action('App\Search\InlineProviderController@getDirectGeneric').'\';';
        $variables .= 'var NDC_SPROV_PROPERTIES_DIRECT_EQUIPMENT = \''.action('App\Search\InlineProviderController@getDirectEquipmentUnits').'\';';
    $variables .= 'var NDC_SPROV_ROAD_SURFACE_TYPES = \''.action('App\Search\InlineProviderController@getRoadSurfaceTypes').'\';';

    $variables .= 'var NDC_SPROV_MATERIAL = \''.action('App\Search\InlineProviderController@getMaterials').'\';';
    $variables .= 'var NDC_SPROV_FUEL = \''.action('App\Search\InlineProviderController@getFuels').'\';';
    $variables .= 'var NDC_SPROV_EQUIPMENT = \''.action('App\Search\InlineProviderController@getEquipmentUnits').'\';';

    $variables .= 'var NDC_SPROV_FINANCE_INVOICES = \''.action('App\Search\InlineProviderController@getInvoices').'\';';

    $variables .= 'var NDC_SPROV_FINANCE_ACCOUNT = \''.action('App\Search\InlineProviderController@getFinanceAccounts').'\';';
    $variables .= 'var NDC_SPROV_FINANCE_TAX_RATES = \''.action('App\Search\InlineProviderController@getTaxRates').'\';';

    return $variables;
    });


    echo Cache::remember('ndc_county_settings_provider'.$currentAccount->id, 10, function () use ($currentAccount) {
        $countySettings = '';

        $countySettings .= 'var COUNTY_SETTINGS_NUMBER_FORMAT = '.county_settings()['data_decimal_places'].';';
        $countySettings .= 'var COUNTY_SETTINGS_LOCATION_DATA = \''.$currentAccount->zip.' '.$currentAccount->state.'\';';

        return $countySettings;
    });

    echo 'var NDC_USER_SETTING_SHOW_ANIMATION_ON_CHANGE = \''.user_settings()['show_animation_on_form_change'].'\';';
    echo 'var NDC_USER_CURRENT_ROLE = \''.Session::get('userRole', 0).'\';';

    ?>
</script>