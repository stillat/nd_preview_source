<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE-edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
    <link rel="shortcut icon" href="{{ Config::get('app.url') }}/favicon.ico" type="image/ico">

    <script src="{{{ app_url() }}}/assets/js/pace.min.js{{ UI::getBuster() }}"></script>
    @include('integration.universal_analytics')
    @if($currentAccount !== null)
        <title>{{{ Str::limit($currentAccount->account_name, 10) }}} - @yield('title', 'ND Counties')</title>
    @else
        <title>@yield('title', 'ND Counties')</title>
    @endif

    {{ UI::getCss() }}

    @yield('additionalStyles')

    <!--[if lt IE 9]>
    <script src="{{ app_url() }}/assets/js/html5shiv.js{{ UI::getBuster() }}"></script>
    <script src="{{ app_url() }}/assets/js/respond.min.js{{ UI::getBuster() }}"></script>
    <![endif]-->

</head>



<body class="horizontal-menu @yield('extraBodyClass')"
        @yield('extraBody')>

@yield('prependBody')

<section>
    <div class="mainpanel">
          @section('pageHeader')

            <div class="pageheader" id="systemPageHeader" style="-webkit-box-shadow: -1px 2px 5px 0px rgba(211,215,219,1);-moz-box-shadow: -1px 2px 5px 0px rgba(211,215,219,1);box-shadow: -1px 2px 5px 0px rgba(211,215,219,1);">

                <div class="row">

                    <h2 style="font-weight:normal;">

                        @yield('pageTitle')

                    </h2>

                    <div class="breadcrumb-wrapper hidden-print" style="top: 20px;">

                        @yield('buttonBar')

                    </div>

                </div>

                @yield('headerExtra')

            </div>

        @show
        @include('partials.messages.database_maintenance')
        <div class="contentpanel">
            @yield('content')
        </div>

    </div>
    <div class="rightpanel"></div>

</section>

<div class="modal fade" id="modalHousing"></div>
@yield('appendBody')
@include('layouts.settings')
{{ UI::getJs() }}
@yield('additionalScripts')
</body>
</html>

