<?php

/*
|--------------------------------------------------------------------------
| Register The Laravel Class Loader
|--------------------------------------------------------------------------
|
| In addition to using Composer, you may use the Laravel class loader to
| load your controllers and models. This is useful for keeping all of
| your classes in the "global" namespace without Composer updating.
|
*/

use Illuminate\Database\Eloquent\ModelNotFoundException;

ClassLoader::addDirectories(array(

    app_path().'/commands',
    app_path().'/controllers',
    app_path().'/models',
    app_path().'/database/seeds',

));

/*
|--------------------------------------------------------------------------
| Application Error Logger
|--------------------------------------------------------------------------
|
| Here we will configure the error logger setup for the application which
| is built on top of the wonderful Monolog library. By default we will
| build a basic log file setup which creates a single file for logs.
|
*/

Log::useFiles(storage_path().'/logs/laravel.log');

/*
|--------------------------------------------------------------------------
| Application Error Handler
|--------------------------------------------------------------------------
|
| Here you may handle any errors that occur in your application, including
| logging them or displaying custom views for specific errors. You may
| even register several error handlers to handle different types of
| exceptions. If nothing is returned, the default error view is
| shown, which includes a detailed stack trace during debug.
|
*/

if (Config::get('app.debug') == false) {
    App::error(function (ModelNotFoundException $exception, $code) {
        return Response::view('errors.404', [], 404);
    });
}

App::error(function (Exception $exception, $code) {
    Log::error($exception);

    // If we are in debug mode, display the Laravel handler.
    if (Config::get('app.debug') == true) {
        return;
    }


    switch ($code) {
        case 404:
            return Response::view('errors.404', array(), 404);
        case 500:
            return Response::view('errors.report.report_error');
    }

});

/*
|--------------------------------------------------------------------------
| Maintenance Mode Handler
|--------------------------------------------------------------------------
|
| The "down" Artisan command gives you the ability to put an application
| into maintenance mode. Here, you will define what is displayed back
| to the user if maintenance mode is in effect for the application.
|
*/

App::down(function () {
    if ($_SERVER['HTTP_USER_AGENT'] !== '940e12173a25e60712caf5d5579a03557415602ce52590e9f1f583f1f75ef3be') {
        return Response::view('errors.maintenance', array(), 503);
    }
});

/*
|--------------------------------------------------------------------------
| Require The Filters File
|--------------------------------------------------------------------------
|
| Next we will load the filters file for the application. This gives us
| a nice separate location to store our route and application filter
| definitions instead of putting them all in the main routes file.
|
*/

require app_path().'/filters.php';

/*
|--------------------------------------------------------------------------
| Require The Events File
|--------------------------------------------------------------------------
|
*/

require app_path().'/events.php';

Debugbar::startMeasure('helpers', 'Creating helpers');
require app_path().'/helpers.php';
Debugbar::stopMeasure('helpers');

Debugbar::startMeasure('cmacro', 'Creating macros');
require app_path().'/macros.php';
Debugbar::stopMeasure('cmacro');
