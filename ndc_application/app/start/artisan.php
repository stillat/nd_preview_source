<?php

/*
|--------------------------------------------------------------------------
| Register The Artisan Commands
|--------------------------------------------------------------------------
|
| Each available Artisan command must be registered with the console so
| that it is available to be called. We'll register every command so
| the console gets access to each of the command object instances.
|
*/

// This will include our handy developer tools!
if (App::environment() != 'production') {
    require app_path().'/ndcounties/misc/developer_tools.php';
}
