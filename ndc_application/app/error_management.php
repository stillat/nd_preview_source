<?php

use Illuminate\Database\Eloquent\ModelNotFoundException;
use ParadoxOne\NDCounties\Database\Repositories\WorkEntries\WorkEntryNotFoundException;
use ParadoxOne\NDCounties\ErrorManagement\InvalidAccountManager;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

App::error(function (Exception $exception, $code) {
  if (App::environment('local') == false) {
      if (!$exception instanceof NotFoundHttpException) {
          $err = '';

          if (Auth::user()) {
              $err .= '<h1>User Information</h1><br>'.' -- https://ndcounties.com/app/admin/users/'.Auth::user()->id.' -- https://ndcounties.com/app/admin/accounts/'.Auth::user()->last_tenant.'<pre>';
              $user = new stdClass();
              $user->{"User ID"} = Auth::user()->id;
              $user->{"First Name"} = Auth::user()->first_name;
              $user->{"Last Name"} = Auth::user()->last_name;
              $user->{"Email Address"} = Auth::user()->email;
              $user->{"Last Accessed Account"} = Auth::user()->last_tenant;
              $err .= json_encode($user, JSON_PRETTY_PRINT).'</pre>';
          }

          $err .= '<h3>Error Message</h3>';
          $err .= '<pre>'.$exception->getMessage().'</pre>';
          $err .= '<h3>Error File/Line/Code</h3>';
          $err .= '<pre>'.$exception->getFile().'::LINE'.$exception->getLine().'::CODE'.$exception->getCode().'</pre>';
          $err .= '<h3>Exception</h3>';
          $err .= '<pre>'.$exception.'</pre>';

          $err .= '<h3>Trace</h3>';
          $err .= '<pre>'.$exception->getTraceAsString().'</pre>';
          \Mail::send('error', ['err' => $err], function ($message) {
          $message->to('errors@ndcounties.com')->subject('Error Report');
      });
      }
  }
});


App::error(function (ModelNotFoundException $exception) {
   return View::make('errors.404');
});


App::error(function (WorkEntryNotFoundException $exception) {
    return View::make('errors.404_work_record');
});

App::error(function (ErrorException $exception) {
   if (InvalidAccountManager::isInvalidAccountManager($exception)) {
       return InvalidAccountManager::handle();
   }
});

App::error(function (PDOException $exception) {
   if (InvalidAccountManager::isInvalidAccountManager($exception)) {
       return InvalidAccountManager::handle();
   }
});
