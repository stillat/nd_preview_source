<?php

// This line is here just to make sure that no "variable not defined"
// errors appear when automating ND Counties from the command line, such
// as when running tests.
use Illuminate\Support\Facades\Auth;

$NDC_INSTALLED = false;

// The setup lock file contains the logic to load the
// install/setup system appropriately.
require 'ndcounties/misc/setup_lock.php';

if ($NDC_INSTALLED) {

    // Include the menu only if the system is installed.
    require app_path().'/menu.php';
    require app_path().'/error_management.php';

    // We only need to include the `security.php` file when the system
    // is installed. This file contains the routes for the log in/out
    // systems.
    require 'ndcounties/misc/security.php';

    require 'ndcounties/misc/tenant_fixes.php';

    // Include the application listeners.
    require 'ndcounties/misc/listeners.php';

    // This will include the administrative routes for the system.
    require 'ndcounties/misc/admin_routes.php';

    // If the system is flagged as installed, we need to include
    // the actual application routes. These routes contain the usual
    // routes that users expect of the ND Counties system.
    require 'ndcounties/misc/application_routes.php';
}
