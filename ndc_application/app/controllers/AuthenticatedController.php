<?php

use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\View;
use Jenssegers\Agent\Facades\Agent;
use ParadoxOne\NDCounties\Processes\ProcessManager;

class AuthenticatedController extends BaseController
{
    protected $userSettings = null;

    protected $processIsRunning = false;

    protected $currentRunningProcess = 0;

    public function __construct()
    {
        $this->beforeFilter('auth');

        if (Auth::user() == null) {
            return Redirect::to('login');
        }

        Debugbar::startMeasure('launching', 'Starting Tenant');

        Tenant::assumeTenant(Auth::user()->last_tenant);

        Debugbar::stopMeasure('launching');

        Debugbar::startMeasure('lastSeen', 'Last Seen Code');

        if (Session::has('userLastSeen')) {
            $lastSeenDate = Session::get('userLastSeen');

            $currentDate = Carbon::now();
            $currentDate->subMinutes(10);

            if ($lastSeenDate->lt($currentDate)) {
                Session::forget('userLastSeen');
            }
        } else {
            $lastSeenDate = Carbon::now();

            $user = Auth::user();
            $user->last_seen = $lastSeenDate;
            $user->save();

            // Set the session date.
            Session::set('userLastSeen', $lastSeenDate);
        }
        Debugbar::stopMeasure('lastSeen');

        $this->userSettings = (object)user_settings();

        $accountRepo = App::make('ParadoxOne\NDCounties\Contracts\AccountRepositoryInterface');
        $account = $accountRepo->getAccount(Auth::user()->last_tenant);

        $accountRepo = null;
        unset($accountRepo);
        if ($account->process_running != ProcessManager::PROCESS_NONE) {
            $this->processIsRunning = true;
            $this->currentRunningProcess = $account->process_running;
        }
        View::share('databaseOperationsDisabled', $this->shouldInsertsAndUpdatesBeDisabled());
    }

    public function flagSystemResourceUpdate()
    {
        UI::warning('You cannot modify a default system resource');
        return Redirect::back();
    }

    public function isRunningProcess()
    {
        return $this->processIsRunning;
    }

    public function getCurrentProcess()
    {
        return $this->currentRunningProcess;
    }

    /**
     * Determines if all inserts and updates should be disabled.
     *
     * @return bool
     */
    protected function shouldInsertsAndUpdatesBeDisabled()
    {
        return ProcessManager::isDatabaseProcess($this->getCurrentProcess());
    }

    protected function flagDatabaseMaintenance()
    {
        UI::info('Creating, updated and removing records is not available until ongoing data operations have completed.', 'Data Maintenance');
        return Redirect::back();
    }

    public function flagSystemResourceRemove()
    {
        UI::danger('You cannot remove a default system resource');
        return Redirect::back();
    }

    public function requiresServiceAccount()
    {
        $this->beforeFilter('accountRedirect', array('except' => array('missingMethod')));
    }

    public function isMobileOrTablet()
    {
        if (Agent::isMobile() || Agent::isTablet()) {
            return true;
        }

        return false;
    }

    public function requireDesktop()
    {
        return View::make('app.work.not_supported');
    }
}
