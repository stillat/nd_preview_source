<?php namespace App\Projects;

use AuthenticatedController;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use ParadoxOne\NDCounties\UI\UIBuilder as UI;
use Tenant;
use Redirector;
use ParadoxOne\NDCounties\Contracts\Projects\ProjectRepositoryInterface;

class ProjectsController extends AuthenticatedController
{
    protected $projectsRepository;

    public function __construct(ProjectRepositoryInterface $projectRepository)
    {
        parent::__construct();
        $this->requiresServiceAccount();
        
        $this->projectsRepository = $projectRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        Redirector::logCurrent();

        $foundResults = false;
        
        if (Input::get('search', false) == 1) {
            $ids = Cache::get(search_cache_key('project_search'));
            if (is_array($ids) && count($ids) > 0) {
                $foundResults = true;
                $projects = $this->projectsRepository->getAllWithTrashed()->whereIn('id', $ids)->paginate(user_per_page());
            } else {
                $projects = $this->projectsRepository->getProjects()->paginate(user_per_page());
                UI::info('Search contained no results.', 'Search Projects');
            }
        } else {
            $projects = $this->projectsRepository->getProjects()->paginate(user_per_page());
        }

        return View::make('app.projects.list')->with('projects', $projects)->with('restrictionString', make_restrictions_string(array_pluck($projects, 'id')))->with('foundResults', $foundResults);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        if ($this->shouldInsertsAndUpdatesBeDisabled()) {
            return $this->flagDatabaseMaintenance();
        }

        return View::make('app.projects.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        if ($this->shouldInsertsAndUpdatesBeDisabled()) {
            return $this->flagDatabaseMaintenance();
        }

        $project = $this->projectsRepository->create(Input::all());

        if ($project === false) {
            UI::inputErrors();
            return Redirect::back()->withErrors($this->projectsRepository->errors())->withInput();
        } else {
            UI::success('Project successfully added.', 'New Project');

            if (Input::get('submit') == 'return') {
                return Redirect::to(Redirector::getUrl(route('projects.index')));
            } else {
                if ($this->userSettings->auto_clear_forms) {
                    return Redirect::back();
                } else {
                    return Redirect::back()->withInput(Input::except('code', 'name', 'description'));
                }
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        $project = $this->projectsRepository->getModelById($id);

        Redirector::logCurrent();
        
        return View::make('app.projects.view')->with('project', $project);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        if ($this->shouldInsertsAndUpdatesBeDisabled()) {
            return $this->flagDatabaseMaintenance();
        }

        if ($id == 0) {
            return $this->flagSystemResourceUpdate();
        }
                
        $project = $this->projectsRepository->getModelById($id);
        return View::make('app.projects.edit')->with('project', $project);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {
        if ($this->shouldInsertsAndUpdatesBeDisabled()) {
            return $this->flagDatabaseMaintenance();
        }

        if ($id == 0) {
            return $this->flagSystemResourceUpdate();
        }

        $project = $this->projectsRepository->update($id, Input::all());

        if ($project === false) {
            UI::inputErrors();
            return Redirect::back()->withErrors($this->projectsRepository->errors())->withInput();
        } else {
            UI::success('Project successfully updated.', 'Update Project');
            return Redirect::to(Redirector::getUrl(route('projects.index')));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        if ($this->shouldInsertsAndUpdatesBeDisabled()) {
            return $this->flagDatabaseMaintenance();
        }

        $id = Input::get('removeContext');

        $multiFlag = Input::get('removeMultiple', 0);

        if ($multiFlag == '1') {
            $id = explode(',', $id);
            $id = array_strip_values($id, 0);
        } else {
            if ($id == 0) {
                return $this->flagSystemResourceRemove();
            }
        }

        if (!is_array($id)) {
            $id = (array)$id;
        }

        $removedSuccessfully = $this->projectsRepository->remove($id);

        if ($removedSuccessfully) {
            UI::success('Project removed.', 'Remove Project');
        } else {
            UI::danger('There was a problem removing the project.', 'Remove Project');
        }

        return Redirect::back();
    }
}
