<?php namespace App;

use Illuminate\Support\Facades\Cache;
use View;
use Auth;
use Redirect;
use Session;
use UI;
use Redirector;
use Input;
use AuthenticatedController;
use ParadoxOne\NDCounties\Contracts\DepartmentsRepositoryInterface;

class DepartmentsController extends AuthenticatedController
{
    protected $departmentRepository;

    public function __construct(DepartmentsRepositoryInterface $departmentRepository)
    {
        parent::__construct();
        $this->requiresServiceAccount();
        
        $this->departmentRepository = $departmentRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        Redirector::logCurrent();

        $foundResults = false;

        if (Input::get('search', false) == 1) {
            $ids = Cache::get(search_cache_key('department_search'));
            if (is_array($ids) && count($ids) > 0) {
                $foundResults = true;
                $departments = $this->departmentRepository->getAllWithTrashed()->whereIn('id', $ids)->paginate(user_per_page());
            } else {
                $departments = $this->departmentRepository->getDepartments()->paginate(user_per_page());
                UI::info('Search contained no results.', 'Search Departments');
            }
        } else {
            $departments = $this->departmentRepository->getDepartments()->paginate(user_per_page());
        }

        return View::make('app.departments.list')->with('departments', $departments)->with('restrictionString', make_restrictions_string(array_pluck($departments, 'id')))->with('foundResults', $foundResults);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        if ($this->shouldInsertsAndUpdatesBeDisabled()) {
            return $this->flagDatabaseMaintenance();
        }

        return View::make('app.departments.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        if ($this->shouldInsertsAndUpdatesBeDisabled()) {
            return $this->flagDatabaseMaintenance();
        }

        $department = $this->departmentRepository->create(Input::all());

        if ($department === false) {
            UI::inputErrors();
            return Redirect::back()->withErrors($this->departmentRepository->errors())->withInput();
        } else {
            UI::success('Department successfully added.', 'New District');

            if (Input::get('submit') == 'return') {
                return Redirect::to(Redirector::getURL(route('departments.index')));
            } else {
                if ($this->userSettings->auto_clear_forms) {
                    return Redirect::back();
                } else {
                    return Redirect::back()->withInput(Input::except('code', 'name', 'description'));
                }
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        $department = $this->departmentRepository->getModelById($id);

        Redirector::logCurrent();
        
        return View::make('app.departments.view')->with('department', $department);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        if ($this->shouldInsertsAndUpdatesBeDisabled()) {
            return $this->flagDatabaseMaintenance();
        }

        if ($id == 0) {
            return $this->flagSystemResourceUpdate();
        }

        $department = $this->departmentRepository->getModelById($id);

        return View::make('app.departments.edit')->with('department', $department);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {
        if ($this->shouldInsertsAndUpdatesBeDisabled()) {
            return $this->flagDatabaseMaintenance();
        }

        if ($id == 0) {
            return $this->flagSystemResourceUpdate();
        }

        $department = $this->departmentRepository->update($id, Input::all());

        if ($department === false) {
            UI::inputErrors();
            return Redirect::back()->withErrors($this->departmentRepository->errors())->withInput();
        } else {
            UI::success('Department successfully updated.', 'Update Department');
            return Redirect::to(Redirector::getUrl(route('departments.index')));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        if ($this->shouldInsertsAndUpdatesBeDisabled()) {
            return $this->flagDatabaseMaintenance();
        }

        $id = Input::get('removeContext');

        $multiFlag = Input::get('removeMultiple', 0);

        if ($multiFlag == '1') {
            $id = explode(',', $id);
            $id = array_strip_values($id, 0);
        } else {
            if ($id == 0) {
                return $this->flagSystemResourceRemove();
            }
        }

        if (!is_array($id)) {
            $id = (array)$id;
        }

        $removedSuccessfully = $this->departmentRepository->remove($id);

        if ($removedSuccessfully) {
            UI::success('Department removed.', 'Remove Department');
        } else {
            UI::danger('There was a problem removing the department.', 'Remove Department');
        }

        return Redirect::back();
    }
}
