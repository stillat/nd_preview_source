<?php namespace App;

use Illuminate\Support\Facades\Cache;
use View;
use Auth;
use Redirect;
use Session;
use UI;
use Redirector;
use Input;
use AuthenticatedController;
use ParadoxOne\NDCounties\Contracts\DistrictsRepositoryInterface;

class DistrictsController extends AuthenticatedController
{
    protected $districtsRepository;

    public function __construct(DistrictsRepositoryInterface $districtsRepository)
    {
        parent::__construct();
        $this->requiresServiceAccount();
        
        $this->districtsRepository = $districtsRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        Redirector::logCurrent();
        $foundResults = false;
        if (Input::get('search', false) == 1) {
            $ids = Cache::get(search_cache_key('district_search'));
            if (is_array($ids) && count($ids) > 0) {
                $foundResults = true;
                $districts = $this->districtsRepository->getAllWithTrashed()->whereIn('id', $ids)->paginate(user_per_page());
            } else {
                $districts = $this->districtsRepository->getDistricts()->paginate(user_per_page());
                UI::info('Search contained no results.', 'Search Districts');
            }
        } else {
            $districts = $this->districtsRepository->getDistricts()->paginate(user_per_page());
        }

        return View::make('app.districts.list')->with('districts', $districts)->with('restrictionString', make_restrictions_string(array_pluck($districts, 'id')))->with('foundResults', $foundResults);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        if ($this->shouldInsertsAndUpdatesBeDisabled()) {
            return $this->flagDatabaseMaintenance();
        }

        return View::make('app.districts.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        if ($this->shouldInsertsAndUpdatesBeDisabled()) {
            return $this->flagDatabaseMaintenance();
        }

        $district = $this->districtsRepository->create(Input::all());

        if ($district === false) {
            UI::inputErrors();
            return Redirect::back()->withErrors($this->districtsRepository->errors())->withInput();
        } else {
            UI::success('District successfully added.', 'New District');

            if (Input::get('submit') == 'return') {
                return Redirect::to(Redirector::getURL(route('districts.index')));
            } else {
                if ($this->userSettings->auto_clear_forms) {
                    return Redirect::back();
                } else {
                    return Redirect::back()->withInput(Input::except('code', 'name', 'description'));
                }
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        Redirector::logCurrent();

        $district = $this->districtsRepository->getModelById($id);

        return View::make('app.districts.view')->with('district', $district);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        if ($this->shouldInsertsAndUpdatesBeDisabled()) {
            return $this->flagDatabaseMaintenance();
        }

        if ($id == 0) {
            return $this->flagSystemResourceUpdate();
        }

        $district = $this->districtsRepository->getModelById($id);

        return View::make('app.districts.edit')->with('district', $district);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {
        if ($this->shouldInsertsAndUpdatesBeDisabled()) {
            return $this->flagDatabaseMaintenance();
        }

        if ($id == 0) {
            return $this->flagSystemResourceUpdate();
        }

        $district = $this->districtsRepository->update($id, Input::all());

        if ($district === false) {
            UI::inputErrors();
            return Redirect::back()->withErrors($this->districtsRepository->errors())->withInput();
        } else {
            UI::success('District successfully updated.', 'Update District');
            return Redirect::to(Redirector::getUrl(route('districts.index')));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        if ($this->shouldInsertsAndUpdatesBeDisabled()) {
            return $this->flagDatabaseMaintenance();
        }

        $id = Input::get('removeContext');

        $multiFlag = Input::get('removeMultiple', 0);

        if ($multiFlag == '1') {
            $id = explode(',', $id);
            $id = array_strip_values($id, 0);
        } else {
            if ($id == 0) {
                return $this->flagSystemResourceRemove();
            }
        }

        if (!is_array($id)) {
            $id = (array)$id;
        }

        $removedSuccessfully = $this->districtsRepository->remove($id);

        if ($removedSuccessfully) {
            UI::success('District removed.', 'Remove District');
        } else {
            UI::danger('There was a problem removing the district.', 'Remove District');
        }

        return Redirect::back();
    }
}
