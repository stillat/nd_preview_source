<?php namespace App\EquipmentUnits;

use AuthenticatedController;
use View;
use Auth;
use Redirect;
use Session;
use UI;
use Redirector;
use FormPersist;
use Input;
use ParadoxOne\NDCounties\Contracts\EquipmentUnits\UnitsRepositoryInterface;
use ParadoxOne\NDCounties\Contracts\UnitRepositoryInterface as MeasurementUnitsRepository;

class UnitsController extends AuthenticatedController
{
    protected $equipmentUnitsRepository;

    protected $measurementUnitsRepository;

    public function __construct(UnitsRepositoryInterface $equipmentUnitsRepository, MeasurementUnitsRepository $measurementUnitsRepository)
    {
        parent::__construct();
        $this->requiresServiceAccount();

        $this->equipmentUnitsRepository = $equipmentUnitsRepository;
        $this->measurementUnitsRepository = $measurementUnitsRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        Redirector::logCurrent();

        $units = $this->equipmentUnitsRepository->getUnits()->paginate(user_per_page());

        $measurementUnits = $this->measurementUnitsRepository->getUnits()->lists('name', 'id');

        return View::make('app.equipment.list')->with('equipmentUnits', $units)->with('measurementUnits', $measurementUnits);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $measurementUnits = $this->measurementUnitsRepository->getUnits()->lists('name', 'id');

        return View::make('app.equipment.create')->with('measurementUnits', $measurementUnits);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        $equipmentUnit = $this->equipmentUnitsRepository->create(Input::all());

        if ($equipmentUnit === false) {
            UI::inputErrors();
            return Redirect::back()->withErrors($this->equipmentUnitsRepository->errors())->withInput();
        } else {
            FormPersist::setActiveAccount(\Auth::user()->last_tenant);
            FormPersist::persist('eunits', $equipmentUnit->id, (object)$equipmentUnit);

            UI::success('Equipment unit successfully added.', 'New Equipment Unit');

            if (Input::get('submit') == 'return') {
                return Redirect::to(Redirector::getURL(route('equipment-units.index')));
            } else {
                return Redirect::back()->withInput(Input::except('code', 'name', 'description'));
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        $equipmentUnit = $this->equipmentUnitsRepository->getModelById($id);
        $measurementUnit = $this->measurementUnitsRepository->getModelById($equipmentUnit->unit_id);

        Redirector::logCurrent();

        return View::make('app.equipment.view')->with('equipmentUnit', $equipmentUnit)->with('measurementUnit', $measurementUnit);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        if ($id == 0) {
            return $this->flagSystemResourceUpdate();
        }

        $equipmentUnit = $this->equipmentUnitsRepository->getModelById($id);

        $measurementUnits = $this->measurementUnitsRepository->getUnits()->lists('name', 'id');

        return View::make('app.equipment.edit')->with('equipmentUnit', $equipmentUnit)->with('measurementUnits', $measurementUnits);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {
        if ($id == 0) {
            return $this->flagSystemResourceUpdate();
        }

        $equipmentUnit = $this->equipmentUnitsRepository->update($id, Input::all());

        if ($equipmentUnit === false) {
            UI::inputErrors();
            return Redirect::back()->withErrors($this->equipmentUnitsRepository->errors())->withInput();
        } else {
            UI::success('Equipment unit successfully updated');
            return Redirect::to(Redirector::getUrl(route('equipment-units.index')));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $id = Input::get('removeContext');
        
        if ($id == 0) {
            return $this->flagSystemResourceRemove();
        }
        
        $removedSuccessfully = $this->equipmentUnitsRepository->remove(array(Input::get('removeContext')));

        if ($removedSuccessfully) {
            UI::success('Equipment unit removed');
        } else {
            UI::danger('There was a problem removing the equipment unit');
        }
        
        return Redirect::back();
    }
}
