<?php namespace App\Search;

use AuthenticatedController;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Response;
use ParadoxOne\NDCounties\Contracts\ActivityRepositoryInterface;
use ParadoxOne\NDCounties\Contracts\DepartmentsRepositoryInterface;
use ParadoxOne\NDCounties\Contracts\DistrictsRepositoryInterface;
use ParadoxOne\NDCounties\Contracts\EmployeeRepositoryInterface;
use ParadoxOne\NDCounties\Contracts\EquipmentUnits\UnitsRepositoryInterface as EquipmentUnitRepositoryInterface;
use ParadoxOne\NDCounties\Contracts\Finance\AccountRepositoryInterface;
use ParadoxOne\NDCounties\Contracts\Finance\Invoices\InvoiceReaderRepositoryInterface;
use ParadoxOne\NDCounties\Contracts\Finance\Taxes\TaxRateRepositoryInterface;
use ParadoxOne\NDCounties\Contracts\Projects\ProjectRepositoryInterface;
use ParadoxOne\NDCounties\Contracts\Properties\PropertiesRepositoryInterface;
use ParadoxOne\NDCounties\Contracts\Properties\PropertyTypesRepositoryInterface;
use ParadoxOne\NDCounties\Contracts\Properties\Roads\SurfaceTypeRepositoryInterface;
use ParadoxOne\NDCounties\Contracts\Resources\FuelRepositoryInterface;
use ParadoxOne\NDCounties\Contracts\Resources\MaterialRepositoryInterface;
use ParadoxOne\NDCounties\Contracts\UnitRepositoryInterface;

class InlineProviderController extends AuthenticatedController
{
    /**
     * The employee repository implementation.
     *
     * @var ParadoxOne\NDCounties\Contracts\EmployeeRepositoryInterface
     */
    protected $employeeRepository;

    /**
     * The activity repository implementation.
     *
     * @var ParadoxOne\NDCounties\Contracts\ActivityRepositoryInterface
     */
    protected $activityRepository;

    /**
     * The department repository implementation.
     *
     * @var ParadoxOne\NDCounties\Contracts\DepartmentsRepositoryInterface
     */
    protected $departmentsRepository;

    /**
     * The districts repository implementation.
     *
     * @var ParadoxOne\NDCounties\Contracts\DistrictsRepositoryInterface
     */
    protected $districtsRepository;

    /**
     * The equipment units repository implementation.
     *
     * @var ParadoxOne\NDCounties\Contracts\EquipmentUnits\UnitsRepositoryInterface
     */
    protected $equipmentUnitsRepository;

    /**
     * The projects repository implementation.
     *
     * @var ParadoxOne\NDCounties\Contracts\Projects\ProjectRepositoryInterface
     */
    protected $projectsRepository;

    /**
     * The properties repository implementation.
     *
     * @var ParadoxOne\NDCounties\Contracts\Properties\PropertiesRepositoryInterface
     */
    protected $propertiesRepository;

    /**
     * The property types repository implementation.
     *
     * @var ParadoxOne\NDCounties\Contracts\Properties\PropertyTypesRepositoryInterface
     */
    protected $propertyTypesRepository;

    /**
     * The fuels repository implementation.
     *
     * @var ParadoxOne\NDCounties\Contracts\Resources\FuelRepositoryInterface
     */
    protected $fuelsRepository;

    /**
     * The materials repository implementation.
     *
     * @var ParadoxOne\NDCounties\Contracts\Resources\MaterialRepositoryInterface
     */
    protected $materialsRepository;

    /**
     * The finance account repository implementation.
     *
     * @var ParadoxOne\NDCounties\Contracts\Finance\AccountRepositoryInterface
     */
    protected $financeAccountRepository;

    /**
     * The tax rate repository implementation.
     *
     * @var ParadoxOne\NDCounties\Contracts\Finance\Taxes\TaxRateRepositoryInterface
     */
    protected $taxRateRepository;

    /**
     * The invoice reader repository implementation.
     *
     * @var ParadoxOne\NDCounties\Contracts\Finance\Invoices\InvoiceReaderRepositoryInterface
     */
    protected $invoiceReaderRepository;

    /**
     * The road surface type repository implementation.
     *
     * @var ParadoxOne\NDCounties\Contracts\Properties\Roads\SurfaceTypeRepositoryInterface
     */
    protected $roadSurfaceTypeRepository;

    /**
     * The measuremewnt unit repository implementation.
     *
     * @var ParadoxOne\NDCounties\Contracts\UnitRepositoryInterface;
     */
    protected $measurementUnitRepository;

    public function __construct(EmployeeRepositoryInterface $employeeRepositoryInterface, ActivityRepositoryInterface $activityRepository, DepartmentsRepositoryInterface $departmentsRepository, DistrictsRepositoryInterface $districtsRepository, EquipmentUnitRepositoryInterface $equipmentUnitsRepository,
                                ProjectRepositoryInterface $projectsRepository, PropertiesRepositoryInterface $propertiesRepository, PropertyTypesRepositoryInterface $propertyTypesRepository, FuelRepositoryInterface $fuelsRepository, MaterialRepositoryInterface $materialsRepository,
                                AccountRepositoryInterface $financeAccountRepository, TaxRateRepositoryInterface $taxRateRepository, InvoiceReaderRepositoryInterface $invoiceReaderRepository,
                                SurfaceTypeRepositoryInterface $roadSurfaceTypeRepository, UnitRepositoryInterface $measurementUnitRepository)
    {
        parent::__construct();

        $this->employeeRepository        = $employeeRepositoryInterface;
        $this->activityRepository        = $activityRepository;
        $this->departmentsRepository     = $departmentsRepository;
        $this->districtsRepository       = $districtsRepository;
        $this->equipmentUnitsRepository  = $equipmentUnitsRepository;
        $this->projectsRepository        = $projectsRepository;
        $this->propertiesRepository      = $propertiesRepository;
        $this->propertyTypesRepository   = $propertyTypesRepository;
        $this->fuelsRepository           = $fuelsRepository;
        $this->materialsRepository       = $materialsRepository;
        $this->financeAccountRepository  = $financeAccountRepository;
        $this->taxRateRepository         = $taxRateRepository;
        $this->invoiceReaderRepository   = $invoiceReaderRepository;
        $this->roadSurfaceTypeRepository = $roadSurfaceTypeRepository;
        $this->measurementUnitRepository = $measurementUnitRepository;
    }

    /**
     * Builds the search results response
     *
     * @param  array $collection
     * @return JSON
     */
    private function buildResponse(array $collection)
    {
        $responseResults = array();

        foreach ($collection as $result) {
            $responseResults[] = array_merge(
                array(
                    'id'   => $result->id,
                    'text' => '(' . $result->code . ') ' . $result->name
                ),
                get_object_vars($result)
            );
        }

        return Response::json($responseResults);
    }

    /**
     * Returns the employee search results.
     *
     * @return array  JSON Array
     */
    public function getEmployees()
    {
        $searchTerms = Input::get('q');

        return $this->buildResponse($this->employeeRepository->search($searchTerms));
    }

    /**
     * Returns the activity search results.
     *
     * @return array  JSON Array
     */
    public function getActivities()
    {
        $searchTerms = Input::get('q');

        return $this->buildResponse($this->activityRepository->search($searchTerms));
    }

    /**
     * Returns the department search results.
     *
     * @return array  JSON Array
     */
    public function getDepartments()
    {
        $searchTerms = Input::get('q');

        return $this->buildResponse($this->departmentsRepository->search($searchTerms));
    }

    /**
     * Returns the district search results.
     *
     * @return array  JSON Array
     */
    public function getDistricts()
    {
        $searchTerms = Input::get('q');

        return $this->buildResponse($this->districtsRepository->search($searchTerms));
    }

    /**
     * Returns the equipment unit search results.
     *
     * @return array  JSON Array
     */
    public function getEquipmentUnits()
    {
        $searchTerms = Input::get('q');

        return $this->buildResponse($this->equipmentUnitsRepository->search($searchTerms));
    }

    /**
     * Returns the project search results.
     *
     * @return array  JSON Array
     */
    public function getProjects()
    {
        $searchTerms = Input::get('q');

        return $this->buildResponse($this->projectsRepository->search($searchTerms));
    }

    private function getPropertyResponseResults(&$properties)
    {
        $responseResults = [];
        foreach ($properties as $property) {
            if ($property->type->id == 2) {
                // If a property has a type id of 2, it is actually an equipment unit.
                $responseResults[] = [
                    'id'            => 'equi_' . $property->id,
                    'text'          => '(' . $property->code . ') ' . $property->name,
                    'is_equipment'  => true,
                    'children'      => [],
                    'property_type' => -1,
                    'has_children'  => false,
                    'adapters'      => []
                ];
            } else {
                $childrenArray = [];
                $hasChildren   = false;

                if ($property->children->count() > 0) {
                    foreach ($property->children as $child) {
                        $childrenArray[] = [
                            'id'            => 'prop_' . $child->id,
                            'text'          => '(' . $child->code . ') ' . $child->name,
                            'is_equipment'  => false,
                            'has_children'  => false,
                            'property_type' => $child->type->id,
                            'adapters'      => $child->type->adapters->lists('adapter')
                        ];
                    }

                    $hasChildren = true;
                }

                $responseResults[] = [
                    'id'            => 'prop_' . $property->id,
                    'text'          => '(' . $property->code . ') ' . $property->name,
                    'is_equipment'  => false,
                    'children'      => $childrenArray,
                    'has_children'  => $hasChildren,
                    'property_type' => $property->type->id,
                    'adapters'      => $property->type->adapters->lists('adapter')
                ];
            }
        }

        return $responseResults;
    }

    public function getDirectGeneric()
    {
        $searchTerms = Input::get('q');
        $properties = $this->propertiesRepository->searchGeneric($searchTerms);
        return $this->getPropertyResponseResults($properties);
    }

    public function getDirectEquipmentUnits()
    {
        $searchTerms = Input::get('q');
        $properties = $this->propertiesRepository->searchEquipmentUnits($searchTerms);
        return $this->getPropertyResponseResults($properties);
    }

    public function getDirectRoads()
    {
        $searchTerms = Input::get('q');
        // This will hold all the results we will send back to the client.

        // Hold all of the properties that actually matched the search terms (well, duh).
        $properties = $this->propertiesRepository->searchRoads($searchTerms);

        return Response::json($this->getPropertyResponseResults($properties));
    }

    /**
     * Returns the property search results.
     *
     * @return array  JSON Array
     */
    public function getProperties()
    {
        $searchTerms = Input::get('q');

        $type = Input::get('t', 0);

        // This will hold all the results we will send back to the client.
        $responseResults = array();

        // Hold all of the properties that actually matched the search terms (well, duh).
        $properties = $this->propertiesRepository->search($searchTerms, $type);


        foreach ($properties as $property) {
            if ($property->type->id == 2) {
                // If a property has a type id of 2, it is actually an equipment unit.
                $responseResults[] = [
                    'id'            => 'equi_' . $property->id,
                    'text'          => '(' . $property->code . ') ' . $property->name,
                    'is_equipment'  => true,
                    'children'      => [],
                    'property_type' => -1,
                    'has_children'  => false,
                    'adapters'      => []
                ];
            } else {
                $childrenArray = [];
                $hasChildren   = false;

                if ($property->children->count() > 0) {
                    foreach ($property->children as $child) {
                        $childrenArray[] = [
                            'id'            => 'prop_' . $child->id,
                            'text'          => '(' . $child->code . ') ' . $child->name,
                            'is_equipment'  => false,
                            'has_children'  => false,
                            'property_type' => $child->type->id,
                            'adapters'      => $child->type->adapters->lists('adapter')
                        ];
                    }

                    $hasChildren = true;
                }

                $responseResults[] = [
                    'id'            => 'prop_' . $property->id,
                    'text'          => '(' . $property->code . ') ' . $property->name,
                    'is_equipment'  => false,
                    'children'      => $childrenArray,
                    'has_children'  => $hasChildren,
                    'property_type' => $property->type->id,
                    'adapters'      => $property->type->adapters->lists('adapter')
                ];
            }
        }

        return Response::json($responseResults);
    }

    /**
     * Returns the property type search results.
     *
     * @return array  JSON Array
     */
    public function getPropertyTypes()
    {
        $searchTerms = Input::get('q');

        return $this->buildResponse($this->propertyTypesRepository->search($searchTerms));
    }

    /**
     * Returns the fuel search results.
     *
     * @return array  JSON Array
     */
    public function getFuels()
    {
        $searchTerms = Input::get('q');

        return $this->buildResponse($this->fuelsRepository->search($searchTerms));
    }

    /**
     * Returns the material search results.
     *
     * @return array  JSON Array
     */
    public function getMaterials()
    {
        $searchTerms = Input::get('q');

        return $this->buildResponse($this->materialsRepository->search($searchTerms));
    }

    /**
     * Returns the finance account search results.
     *
     * @return array  JSON Array
     */
    public function getFinanceAccounts()
    {
        $searchTerms = Input::get('q');

        return $this->buildResponse($this->financeAccountRepository->search($searchTerms));
    }

    /**
     * Returns the finance tax rates search results.
     *
     * @return array  JSON Array
     */
    public function getTaxRates()
    {
        $searchTerms = Input::get('q');

        return $this->buildResponse($this->taxRateRepository->search($searchTerms));
    }

    /**
     * Returns the invoice search results.
     *
     * @return array  JSON Array
     */
    public function getInvoices()
    {
        $searchTerms = Input::get('q');

        $responseResults = array();

        foreach ($this->invoiceReaderRepository->search($searchTerms) as $result) {
            $isDisabled = false;

            if ($result->invoice_status == 5) {
                $isDisabled = true;
            }

            if (!$isDisabled) {
                if ($result->is_closed) {
                    $isDisabled = true;
                }
            }

            $invoiceResult = ['id'   => $result->id,
                              'text' => '(' . $result->code . ') ' . $result->name,
            'disabled' => $isDisabled];

            $responseResults[] = array_merge(
                $invoiceResult,
                get_object_vars($result)
            );
        }

        return Response::json($responseResults);
    }

    public function getRoadSurfaceTypes()
    {
        $searchTerms = Input::get('q');

        return $this->buildResponse($this->roadSurfaceTypeRepository->search($searchTerms));
    }

    public function getMeasurementUnits()
    {
        $searchTerms = Input::get('q');

        return $this->buildResponse($this->measurementUnitRepository->search($searchTerms));
    }
}
