<?php namespace App\Search;

use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Response;
use ParadoxOne\NDCounties\Contracts\Reporting\ReportMetaDefinitionRepositoryInterface;

class ExistenceController extends \AuthenticatedController
{
    protected $reportRepository;

    public function __construct(ReportMetaDefinitionRepositoryInterface $reportRepositoryInterface)
    {
        parent::__construct();

        $this->reportRepository = $reportRepositoryInterface;
    }


    public function getReport($reportName)
    {
        $exclude = [];

        if (Input::has('dontcheck')) {
            $exclude[] = Input::get('dontcheck');
        }

        $reportExists = $this->reportRepository->reportExistsByName($reportName, $exclude);

        return Response::json(['exists' => $reportExists]);
    }
}
