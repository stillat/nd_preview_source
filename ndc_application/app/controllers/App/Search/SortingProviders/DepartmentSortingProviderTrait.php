<?php namespace App\Search\SortingProviders;

use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use ParadoxOne\NDCounties\UI\UIBuilder;
use Illuminate\Support\Facades\View;
use ParadoxOne\NDCounties\Redirector;
use ParadoxOne\NDCounties\Settings\SortingSettingsManager;

trait DepartmentSortingProviderTrait
{
    public function getDepartments()
    {
        $settings = $this->getSortingSettings(SortingSettingsManager::DEPARTMENTS_TABLE_SETTING_NAME);

        $columns = [
            'date_created' => 'Date Created',
            'last_updated' => 'Last Updated',
            'code' => 'Code',
            'name' => 'Name',
            'desc' => 'Description'
        ];

        return View::make('layouts.dialogs.sorting.sorting')
            ->with('title', 'Department Sorting')->with('settings', $settings)
            ->with('sortingModes', $this->getSortingModes())
            ->with('action', 'postDepartments')
            ->with('sortingColumns', $columns);
    }

    public function postDepartments()
    {
        if (Input::get('real_action') == 'reset') {
            UIBuilder::success('Department sorting settings reset.', 'Reset Sorting Settings');
            $this->sortingManager->forget(SortingSettingsManager::DEPARTMENTS_TABLE_SETTING_NAME);
        } else {
            $newSettings = $this->constructSettingsArray();
            $this->sortingManager->set(SortingSettingsManager::DEPARTMENTS_TABLE_SETTING_NAME,
                json_encode($newSettings));
            UIBuilder::success('Department sorting order updated.', 'Update Sorting Settings');
        }


        return Redirect::to(Redirector::getURL());
    }
}
