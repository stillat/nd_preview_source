<?php namespace App\Search\SortingProviders;

use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\View;
use ParadoxOne\NDCounties\Redirector;
use ParadoxOne\NDCounties\Settings\SortingSettingsManager;
use ParadoxOne\NDCounties\UI\UIBuilder;

trait FinanceTaxRatesSortingProviderTrait
{
    public function getTaxRates()
    {
        $settings = $this->getSortingSettings(SortingSettingsManager::FS_TAX_RATES_TABLE_SETTING_NAME);

        $columns = [
            'date_created' => 'Date Created',
            'last_updated' => 'Date Updated',
            'name' => 'Tax Name',
            'tax_rate' => 'Tax Rate',
            'a_employee' => 'Affects Employees',
            'a_materials' => 'Affects Materials',
            'a_equipment' => 'Affects Equipment Units'
        ];


        return View::make('layouts.dialogs.sorting.sorting')
            ->with('title', 'Tax Rate Sorting')->with('settings', $settings)
            ->with('sortingModes', $this->getSortingModes())
            ->with('action', 'postTaxRates')
            ->with('sortingColumns', $columns);
    }

    public function postTaxRates()
    {
        if (Input::get('real_action') == 'reset') {
            UIBuilder::success('Tax rate sorting settings reset.', 'Reset Sorting Settings');
            $this->sortingManager->forget(SortingSettingsManager::FS_TAX_RATES_TABLE_SETTING_NAME);
        } else {
            $newSettings = $this->constructSettingsArray();
            $this->sortingManager->set(SortingSettingsManager::FS_TAX_RATES_TABLE_SETTING_NAME,
                json_encode($newSettings));
            UIBuilder::success('Tax rate sorting order updated.', 'Update Sorting Settings');
        }


        return Redirect::to(Redirector::getURL());
    }
}
