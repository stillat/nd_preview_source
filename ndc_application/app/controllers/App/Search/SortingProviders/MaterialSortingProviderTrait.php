<?php namespace App\Search\SortingProviders;

use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\View;
use ParadoxOne\NDCounties\Redirector;
use ParadoxOne\NDCounties\Settings\SortingSettingsManager;
use ParadoxOne\NDCounties\UI\UIBuilder;

trait MaterialSortingProviderTrait
{
    public function getMaterials()
    {
        $settings = $this->getSortingSettings(SortingSettingsManager::MATERIALS_TABLE_SETTING_NAME);

        $columns = [
            'date_created' => 'Date Created',
            'last_updated' => 'Last Updated',
            'code' => 'Code',
            'name' => 'Name',
            'desc' => 'Description',
            'cost' => 'Unit Cost'
        ];

        return View::make('layouts.dialogs.sorting.sorting')
            ->with('title', 'Material Sorting')->with('settings', $settings)
            ->with('sortingModes', $this->getSortingModes())
            ->with('action', 'postMaterials')
            ->with('sortingColumns', $columns);
    }

    public function postMaterials()
    {
        if (Input::get('real_action') == 'reset') {
            UIBuilder::success('Material sorting settings reset.', 'Reset Sorting Settings');
            $this->sortingManager->forget(SortingSettingsManager::MATERIALS_TABLE_SETTING_NAME);
        } else {
            $newSettings = $this->constructSettingsArray();
            $this->sortingManager->set(SortingSettingsManager::MATERIALS_TABLE_SETTING_NAME,
                json_encode($newSettings));
            UIBuilder::success('Material sorting order updated.', 'Update Sorting Settings');
        }


        return Redirect::to(Redirector::getURL());
    }
}
