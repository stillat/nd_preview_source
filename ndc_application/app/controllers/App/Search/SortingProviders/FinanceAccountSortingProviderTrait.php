<?php namespace App\Search\SortingProviders;

use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\View;
use ParadoxOne\NDCounties\Redirector;
use ParadoxOne\NDCounties\UI\UIBuilder;
use ParadoxOne\NDCounties\Settings\SortingSettingsManager;

trait FinanceAccountSortingProviderTrait
{
    public function getFinanceAccounts()
    {
        $settings = $this->getSortingSettings(SortingSettingsManager::FS_ACCOUNTS_TABLE_SETTING_NAME);

        $columns = [
          'date_created' => 'Date Created',
            'last_updated' => 'Date Updated',
            'code' => 'Code',
            'name' => 'Account Name',
            'category' => 'Account Category',
            'balance' => 'Balance'
        ];

        return View::make('layouts.dialogs.sorting.sorting')
        ->with('title', 'Finance Account Sorting')->with('settings', $settings)
        ->with('sortingModes', $this->getSortingModes())
        ->with('action', 'postFinanceAccounts')
        ->with('sortingColumns', $columns);
    }

    public function postFinanceAccounts()
    {
        if (Input::get('real_action') == 'reset') {
            UIBuilder::success('Finance sorting settings reset.', 'Reset Sorting Settings');
            $this->sortingManager->forget(SortingSettingsManager::FS_ACCOUNTS_TABLE_SETTING_NAME);
        } else {
            $newSettings = $this->constructSettingsArray();
            $this->sortingManager->set(SortingSettingsManager::FS_ACCOUNTS_TABLE_SETTING_NAME,
                json_encode($newSettings));
            UIBuilder::success('Finance sorting order updated.', 'Update Sorting Settings');
        }


        return Redirect::to(Redirector::getURL());
    }
}
