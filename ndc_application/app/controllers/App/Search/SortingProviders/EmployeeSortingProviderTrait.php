<?php namespace App\Search\SortingProviders;

use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Input;
use ParadoxOne\NDCounties\Redirector;
use ParadoxOne\NDCounties\Settings\SortingSettingsManager;
use ParadoxOne\NDCounties\UI\UIBuilder;

trait EmployeeSortingProviderTrait
{
    public function getEmployees()
    {
        $settings = $this->getSortingSettings(SortingSettingsManager::EMPLOYEES_TABLE_SETTING_NAME);

        $columns = [
            'date_created' => 'Date Created',
            'last_updated' => 'Last Updated',
            'code' => 'Code',
            'name' => 'Name',
            'first_name' => 'First Name',
            'last_name' => 'Last Name',
            'middle_name' => 'Middle Name',
            'hire_date' => 'Hire Date',
            'termination_date' => 'Termination Date',
            'regular_wage' => 'Regular Wage',
            'regular_benefits' => 'Regular Benefits',
            'overtime_wage' => 'Overtime Wage',
            'overtime_benefits' => 'Overtime Benefits'
        ];

        return View::make('layouts.dialogs.sorting.sorting')
            ->with('title', 'Employee Sorting')->with('settings', $settings)
            ->with('sortingModes', $this->getSortingModes())
            ->with('action', 'postEmployees')
            ->with('sortingColumns', $columns);
    }

    public function postEmployees()
    {
        if (Input::get('real_action') == 'reset') {
            UIBuilder::success('Employee sorting settings reset.', 'Reset Sorting Settings');
            $this->sortingManager->forget(SortingSettingsManager::EMPLOYEES_TABLE_SETTING_NAME);
        } else {
            $newSettings = $this->constructSettingsArray();
            $this->sortingManager->set(SortingSettingsManager::EMPLOYEES_TABLE_SETTING_NAME,
                json_encode($newSettings));
            UIBuilder::success('Employee sorting order updated.', 'Update Sorting Settings');
        }


        return Redirect::to(Redirector::getURL());
    }
}
