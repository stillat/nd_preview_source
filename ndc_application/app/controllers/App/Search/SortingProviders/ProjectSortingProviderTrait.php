<?php namespace App\Search\SortingProviders;

use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\View;
use ParadoxOne\NDCounties\Redirector;
use ParadoxOne\NDCounties\Settings\SortingSettingsManager;
use ParadoxOne\NDCounties\UI\UIBuilder;

trait ProjectSortingProviderTrait
{
    public function getProjects()
    {

        $settings = $this->getSortingSettings(SortingSettingsManager::PROJECTS_TABLE_SETTING_NAME);

        $columns = [
            'date_created' => 'Date Created',
            'last_updated' => 'Last Updated',
            'code' => 'Code',
            'name' => 'Name',
            'desc' => 'Description',
            'is_fema' => 'FEMA Status'
        ];

        return View::make('layouts.dialogs.sorting.sorting')
            ->with('title', 'Project Sorting')->with('settings', $settings)
            ->with('sortingModes', $this->getSortingModes())
            ->with('action', 'postProjects')
            ->with('sortingColumns', $columns);
    }

    public function postProjects()
    {
        if (Input::get('real_action') == 'reset') {
            UIBuilder::success('Project sorting settings reset.', 'Reset Sorting Settings');
            $this->sortingManager->forget(SortingSettingsManager::PROJECTS_TABLE_SETTING_NAME);
        } else {
            $newSettings = $this->constructSettingsArray();
            $this->sortingManager->set(SortingSettingsManager::PROJECTS_TABLE_SETTING_NAME,
                json_encode($newSettings));
            UIBuilder::success('Project sorting order updated.', 'Update Sorting Settings');
        }


        return Redirect::to(Redirector::getURL());
    }
}
