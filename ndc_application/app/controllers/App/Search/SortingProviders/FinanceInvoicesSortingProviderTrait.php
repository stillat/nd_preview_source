<?php namespace App\Search\SortingProviders;

use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\View;
use ParadoxOne\NDCounties\Redirector;
use ParadoxOne\NDCounties\Settings\SortingSettingsManager;
use ParadoxOne\NDCounties\UI\UIBuilder;

trait FinanceInvoicesSortingProviderTrait
{
    public function getFinanceInvoices()
    {
        $settings = $this->getSortingSettings(SortingSettingsManager::FS_INVOICES_TABLE_SETTING_NAME);

        $columns = [
            'invoice_number' => 'Invoice Number',
            'due_date' => 'Date Due',
            'discount_rate' => 'Discount Rate',
            'billed_account' => 'Billed Account',
            'paid_account' => 'Paid Account',
            'invoice_total' => 'Invoice Total',
            'total_materials' => 'Materials (Total)',
            'total_equipments' => 'Equipment Units (Total)',
            'total_employees' => 'Employees (Total)',
            'total_fuels' => 'Fuels (Total)',
            'tax_total' => 'Tax (Total)',
            'sub_total' => 'Sub Total',
            'date_created' => 'Date Created',
            'last_updated' => 'Date Updated'
        ];

        return View::make('layouts.dialogs.sorting.sorting')
            ->with('title', 'Invoice Sorting')->with('settings', $settings)
            ->with('sortingModes', $this->getSortingModes())
            ->with('action', 'postFinanceInvoices')
            ->with('sortingColumns', $columns);

    }

    public function postFinanceInvoices()
    {
        if (Input::get('real_action') == 'reset') {
            UIBuilder::success('Invoice sorting settings reset.', 'Reset Sorting Settings');
            $this->sortingManager->forget(SortingSettingsManager::FS_INVOICES_TABLE_SETTING_NAME);
        } else {
            $newSettings = $this->constructSettingsArray();
            $this->sortingManager->set(SortingSettingsManager::FS_INVOICES_TABLE_SETTING_NAME,
                json_encode($newSettings));
            UIBuilder::success('Invoice sorting order updated.', 'Update Sorting Settings');
        }


        return Redirect::to(Redirector::getURL());
    }
}
