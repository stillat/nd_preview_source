<?php namespace App\Search\SortingProviders;

use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\View;
use ParadoxOne\NDCounties\Redirector;
use ParadoxOne\NDCounties\Settings\SortingSettingsManager;
use ParadoxOne\NDCounties\UI\UIBuilder;

trait FuelSortingProviderTrait
{
    public function getFuels()
    {
        $settings = $this->getSortingSettings(SortingSettingsManager::FUELS_TABLE_SETTING_NAME);

        $columns = [
            'date_created' => 'Date Created',
            'last_updated' => 'Last Updated',
            'code' => 'Code',
            'name' => 'Name',
            'desc' => 'Description',
            'cost' => 'Unit Cost'
        ];

        return View::make('layouts.dialogs.sorting.sorting')
            ->with('title', 'Fuels Sorting')->with('settings', $settings)
            ->with('sortingModes', $this->getSortingModes())
            ->with('action', 'postFuels')
            ->with('sortingColumns', $columns);
    }

    public function postFuels()
    {
        if (Input::get('real_action') == 'reset') {
            UIBuilder::success('Fuel sorting settings reset.', 'Reset Sorting Settings');
            $this->sortingManager->forget(SortingSettingsManager::FUELS_TABLE_SETTING_NAME);
        } else {
            $newSettings = $this->constructSettingsArray();
            $this->sortingManager->set(SortingSettingsManager::FUELS_TABLE_SETTING_NAME,
                json_encode($newSettings));
            UIBuilder::success('Fuel sorting order updated.', 'Update Sorting Settings');
        }


        return Redirect::to(Redirector::getURL());
    }
}
