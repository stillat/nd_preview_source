<?php namespace App\Search\SortingProviders;

use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Input;
use ParadoxOne\NDCounties\Redirector;
use ParadoxOne\NDCounties\Settings\SortingSettingsManager;
use ParadoxOne\NDCounties\UI\UIBuilder;

trait ActivitySortingProviderTrait
{
    public function getActivities()
    {
        //$formattedSettings = $this->getGeneralSettings(SortingSettingsManager::ACTIVITIES_TABLE_SETTING_NAME);

        //return View::make('layouts.dialogs.sorting.activity')->with('sortSettings', $formattedSettings);
        $settings = $this->getSortingSettings(SortingSettingsManager::ACTIVITIES_TABLE_SETTING_NAME);

        $activityColumns = [
            'date_created' => 'Date Created',
            'last_updated' => 'Last Updated',
            'code' => 'Code',
            'name' => 'Name',
            'desc' => 'Description'
        ];

        return View::make('layouts.dialogs.sorting.sorting')
            ->with('title', 'Activity Sorting')->with('settings', $settings)
            ->with('sortingModes', $this->getSortingModes())
            ->with('action', 'postActivities')
            ->with('sortingColumns', $activityColumns);
    }

    public function postActivities()
    {
        if (Input::get('real_action') == 'reset') {
            UIBuilder::success('Activity sorting settings reset.', 'Reset Sorting Settings');
            $this->sortingManager->forget(SortingSettingsManager::ACTIVITIES_TABLE_SETTING_NAME);
        } else {
            $newSettings = $this->constructSettingsArray();
            $this->sortingManager->set(SortingSettingsManager::ACTIVITIES_TABLE_SETTING_NAME,
                                       json_encode($newSettings));
            UIBuilder::success('Activity sorting order updated.', 'Update Sorting Settings');
        }


        return Redirect::to(Redirector::getURL());
    }
}
