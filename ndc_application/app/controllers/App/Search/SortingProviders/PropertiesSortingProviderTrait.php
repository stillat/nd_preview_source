<?php namespace App\Search\SortingProviders;

use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\View;
use ParadoxOne\NDCounties\Redirector;
use ParadoxOne\NDCounties\Settings\SortingSettingsManager;
use ParadoxOne\NDCounties\UI\UIBuilder;

trait PropertiesSortingProviderTrait
{
    public function getProperties()
    {
        $settings = $this->getSortingSettings(SortingSettingsManager::PROPERTIES_TABLE_SETTING_NAME);

        $columns = [
          'date_created' => 'Date Created',
            'last_updated' => 'Date Updated',
            'code' => 'Code',
            'name' => 'Property Name',
            'desc' => 'Description',
            'type' => 'Property Type'
        ];


        return View::make('layouts.dialogs.sorting.sorting')
            ->with('title', 'Property Sorting')->with('settings', $settings)
            ->with('sortingModes', $this->getSortingModes())
            ->with('action', 'postProperties')
            ->with('sortingColumns', $columns);
    }

    public function postProperties()
    {
        if (Input::get('real_action') == 'reset') {
            UIBuilder::success('Property sorting settings reset.', 'Reset Sorting Settings');
            $this->sortingManager->forget(SortingSettingsManager::PROPERTIES_TABLE_SETTING_NAME);
        } else {
            $newSettings = $this->constructSettingsArray();
            $this->sortingManager->set(SortingSettingsManager::PROPERTIES_TABLE_SETTING_NAME,
                json_encode($newSettings));
            UIBuilder::success('Property sorting order updated.', 'Update Sorting Settings');
        }


        return Redirect::to(Redirector::getURL());
    }
}
