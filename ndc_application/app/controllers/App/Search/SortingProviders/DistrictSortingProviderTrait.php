<?php namespace App\Search\SortingProviders;

use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\View;
use ParadoxOne\NDCounties\UI\UIBuilder;
use ParadoxOne\NDCounties\Redirector;
use ParadoxOne\NDCounties\Settings\SortingSettingsManager;

trait DistrictSortingProviderTrait
{
    public function getDistricts()
    {
        $settings = $this->getSortingSettings(SortingSettingsManager::DISTRICTS_TABLE_SETTING_NAME);

        $columns = [
            'date_created' => 'Date Created',
            'last_updated' => 'Last Updated',
            'code' => 'Code',
            'name' => 'Name',
            'desc' => 'Description'
        ];

        return View::make('layouts.dialogs.sorting.sorting')
            ->with('title', 'District Sorting')->with('settings', $settings)
            ->with('sortingModes', $this->getSortingModes())
            ->with('action', 'postDistricts')
            ->with('sortingColumns', $columns);
    }

    public function postDistricts()
    {
        if (Input::get('real_action') == 'reset') {
            UIBuilder::success('District sorting settings reset.', 'Reset Sorting Settings');
            $this->sortingManager->forget(SortingSettingsManager::DISTRICTS_TABLE_SETTING_NAME);
        } else {
            $newSettings = $this->constructSettingsArray();
            $this->sortingManager->set(SortingSettingsManager::DISTRICTS_TABLE_SETTING_NAME,
                json_encode($newSettings));
            UIBuilder::success('District sorting order updated.', 'Update Sorting Settings');
        }


        return Redirect::to(Redirector::getURL());
    }
}
