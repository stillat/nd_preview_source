<?php namespace App\Search;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\View;
use ParadoxOne\NDCounties\Contracts\ActivityRepositoryInterface;
use ParadoxOne\NDCounties\Contracts\DepartmentsRepositoryInterface;
use ParadoxOne\NDCounties\Contracts\DistrictsRepositoryInterface;
use ParadoxOne\NDCounties\Contracts\EmployeeRepositoryInterface;
use ParadoxOne\NDCounties\Contracts\Finance\Taxes\TaxRateRepositoryInterface;
use ParadoxOne\NDCounties\Contracts\Projects\ProjectRepositoryInterface;
use ParadoxOne\NDCounties\Contracts\Properties\PropertiesRepositoryInterface;
use ParadoxOne\NDCounties\Contracts\Resources\FuelRepositoryInterface;
use ParadoxOne\NDCounties\Contracts\Resources\MaterialRepositoryInterface;
use ParadoxOne\NDCounties\Database\Repositories\Finance\AccountRepository;

class SearchController extends \AuthenticatedController
{
    protected $activitiesRepository;

    protected $departments;

    protected $districts;

    protected $projects;

    protected $employees;

    protected $fuels;

    protected $materials;

    protected $properties;

    protected $financeAccounts;

    protected $taxRates;

    public function __construct(ActivityRepositoryInterface $activities, DepartmentsRepositoryInterface $departments, DistrictsRepositoryInterface $districts,
                                ProjectRepositoryInterface $projects, EmployeeRepositoryInterface $employees, FuelRepositoryInterface $fuels, MaterialRepositoryInterface $materials, PropertiesRepositoryInterface $properties,
                                AccountRepository $financeAccounts, TaxRateRepositoryInterface $taxRates)
    {
        parent::__construct();
        $this->activitiesRepository = $activities;
        $this->departments          = $departments;
        $this->districts            = $districts;
        $this->projects             = $projects;
        $this->employees            = $employees;
        $this->fuels                = $fuels;
        $this->materials            = $materials;
        $this->properties           = $properties;
        $this->financeAccounts      = $financeAccounts;
        $this->taxRates             = $taxRates;
    }

    private function getResponse($title, $listUrl, $controllerMethod, $searchResults, $icon, $itemURL)
    {

        return View::make('app.search.master')->with('listURL', $listUrl)->with('searchURL', $controllerMethod)
                   ->with('searchResults', $searchResults)->with('icon', $icon)->with('title', $title)
                   ->with('itemURL', $itemURL);
    }

    private function hasInput()
    {
        if (Input::get('q', null) != null && strlen(Input::get('q')) > 0) {
            return true;
        }

        return false;
    }

    public function getActivitySearchDialog()
    {
        return $this->makeSearchDialogResponse('Activities', 'postSearchActivities');
    }

    public function getDepartmentSearchDialog()
    {
        return $this->makeSearchDialogResponse('Departments', 'postSearchDepartments');
    }

    public function getDistrictsSearchDialog()
    {
        return $this->makeSearchDialogResponse('Districts', 'postSearchDistricts');
    }

    public function getProjectsSearchDialog()
    {
        return $this->makeSearchDialogResponse('Projects', 'postSearchProjects');
    }

    public function getFuelsSearchDialog()
    {
        return $this->makeSearchDialogResponse('Fuels', 'postSearchFuels');
    }

    public function getMaterialsSearchDialog()
    {
        return $this->makeSearchDialogResponse('Materials', 'postSearchMaterials');
    }

    public function getEmployeeSearchDialog()
    {
        return $this->makeSearchDialogResponse('Employees', 'postSearchEmployees');
    }

    public function getPropertySearchDialog()
    {
        return $this->makeSearchDialogResponse('Properties', 'postSearchProperties');
    }

    public function getFsAccountsSearchDialog()
    {
        return $this->makeSearchDialogResponse('Finance Accounts', 'postFsAccountsSearch');
    }

    public function getFsTaxRatesSearchDialog()
    {
        return $this->makeSearchDialogResponse('Tax Rates', 'postFsTaxRatesSearch');
    }

    private function getCacheKey($key)
    {
        return Auth::user()->id.'us'.Auth::user()->last_tenant.$key;
    }

    public function postFsTaxRatesSearch()
    {
        $searchTerms = Input::get('q');
        $records = $this->taxRates->search($searchTerms);
        $ids = array_pluck($records, 'id');
        Cache::forget($this->getCacheKey('fs_tax_rates_search'));
        $this->saveOldTerms('Tax Rates', $searchTerms);
        Cache::rememberForever($this->getCacheKey('fs_tax_rates_search'), function() use($ids) {
            return $ids;
        });
        return Redirect::to(action('App\Finances\Taxes\TaxesController@index', ['search' => true]));
    }

    public function postFsAccountsSearch()
    {
        $searchTerms = Input::get('q');
        $records = $this->financeAccounts->search($searchTerms);
        $ids = array_pluck($records, 'id');
        Cache::forget($this->getCacheKey('fs_account_search'));
        $this->saveOldTerms('Finance Accounts', $searchTerms);
        Cache::rememberForever($this->getCacheKey('fs_account_search'), function() use($ids) {
            return $ids;
        });
        return Redirect::to(action('App\Finances\AccountsController@index', ['search' => true]));
    }

    public function postSearchProperties()
    {
        $searchTerms = Input::get('q');
        $records = $this->properties->search($searchTerms);
        $ids = array_pluck($records, 'id');
        Cache::forget($this->getCacheKey('property_search'));
        $this->saveOldTerms('Properties', $searchTerms);
        Cache::rememberForever($this->getCacheKey('property_search'), function() use($ids) {
            return $ids;
        });
        return Redirect::to(action('App\Properties\PropertiesController@index', ['search' => true]));
    }

    public function postSearchEmployees()
    {
        $searchTerms = Input::get('q');
        $records = $this->employees->search($searchTerms);
        $ids = array_pluck($records, 'id');
        Cache::forget($this->getCacheKey('employee_search'));
        $this->saveOldTerms('Employees', $searchTerms);
        Cache::rememberForever($this->getCacheKey('employee_search'), function() use($ids) {
            return $ids;
        });
        return Redirect::to(action('App\Employees\EmployeeController@index', ['search' => true]));
    }

    public function postSearchDepartments()
    {
        $searchTerms = Input::get('q');
        $records = $this->departments->search($searchTerms);
        $ids = array_pluck($records, 'id');
        Cache::forget($this->getCacheKey('department_search'));
        $this->saveOldTerms('Departments', $searchTerms);
        Cache::rememberForever($this->getCacheKey('department_search'), function() use($ids) {
            return $ids;
        });
        return Redirect::to(action('App\DepartmentsController@index', ['search' => true]));
    }

    public function postSearchActivities()
    {
        $searchTerms = Input::get('q');
        $records = $this->activitiesRepository->search($searchTerms);
        $ids = array_pluck($records, 'id');
        Cache::forget($this->getCacheKey('activity_search'));
        $this->saveOldTerms('Activities', $searchTerms);
        Cache::rememberForever($this->getCacheKey('activity_search'), function() use($ids) {
            return $ids;
        });
        return Redirect::to(action('App\Employees\ActivityController@index', ['search' => true]));
    }

    public function postSearchDistricts()
    {
        $searchTerms = Input::get('q');
        $records = $this->districts->search($searchTerms);
        $ids = array_pluck($records, 'id');
        Cache::forget($this->getCacheKey('district_search'));
        $this->saveOldTerms('Districts', $searchTerms);
        Cache::rememberForever($this->getCacheKey('district_search'), function() use($ids) {
            return $ids;
        });
        return Redirect::to(action('App\DistrictsController@index', ['search' => true]));
    }

    public function postSearchProjects()
    {
        $searchTerms = Input::get('q');
        $records = $this->projects->search($searchTerms);
        $ids = array_pluck($records, 'id');
        Cache::forget($this->getCacheKey('project_search'));
        $this->saveOldTerms('Projects', $searchTerms);
        Cache::rememberForever($this->getCacheKey('project_search'), function() use($ids) {
            return $ids;
        });
        return Redirect::to(action('App\Projects\ProjectsController@index', ['search' => true]));
    }

    public function postSearchFuels()
    {
        $searchTerms = Input::get('q');
        $records = $this->fuels->search($searchTerms);
        $ids = array_pluck($records, 'id');
        Cache::forget($this->getCacheKey('fuel_search'));
        $this->saveOldTerms('Fuels', $searchTerms);
        Cache::rememberForever($this->getCacheKey('fuel_search'), function() use($ids) {
            return $ids;
        });
        return Redirect::to(action('App\Fuels\FuelController@index', ['search' => true]));
    }

    public function postSearchMaterials()
    {
        $searchTerms = Input::get('q');
        $records = $this->materials->search($searchTerms);
        $ids = array_pluck($records, 'id');
        Cache::forget($this->getCacheKey('material_search'));
        $this->saveOldTerms('Materials', $searchTerms);
        Cache::rememberForever($this->getCacheKey('material_search'), function() use($ids) {
            return $ids;
        });
        return Redirect::to(action('App\Materials\MaterialController@index', ['search' => true]));
    }

    private function saveOldTerms($search, $terms)
    {
        Session::put($this->getCacheKey($search).'search_terms', $terms);
        Session::put($this->getCacheKey($search).'search_terms_old_items', Input::has('it'));
    }

    private function makeSearchDialogResponse($title, $action)
    {
        $oldValue = Session::get($this->getCacheKey($title).'search_terms', '');
        $includeTrashedChecked = Session::get($this->getCacheKey($title).'search_terms_old_items', false);
        return View::make('layouts.dialogs.search')->with('title', $title)->with('action', $action)->with('oldValue', $oldValue)
            ->with('includeTrashedChecked', $includeTrashedChecked);
    }


}
