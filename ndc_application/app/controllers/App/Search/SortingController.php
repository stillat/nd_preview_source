<?php namespace App\Search;

use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\View;
use ParadoxOne\NDCounties\Redirector;
use ParadoxOne\NDCounties\Settings\SortingSettingsManager;
use ParadoxOne\NDCounties\UI\UIBuilder;

class SortingController extends \AuthenticatedController
{
    use SortingProviders\ActivitySortingProviderTrait;
    use SortingProviders\DepartmentSortingProviderTrait;
    use SortingProviders\DistrictSortingProviderTrait;
    use SortingProviders\ProjectSortingProviderTrait;
    use SortingProviders\FuelSortingProviderTrait;
    use SortingProviders\MaterialSortingProviderTrait;
    use SortingProviders\PropertiesSortingProviderTrait;
    use SortingProviders\FinanceAccountSortingProviderTrait;
    use SortingProviders\FinanceTaxRatesSortingProviderTrait;
    use SortingProviders\FinanceInvoicesSortingProviderTrait;
    use SortingProviders\EmployeeSortingProviderTrait;

    protected $sortingManager;

    public function __construct(SortingSettingsManager $manager)
    {
        parent::__construct();
        $this->sortingManager = $manager;
    }

    public function getSortingModes()
    {
        return [
            'asc' => 'Ascending',
            'desc' => 'Descending',
            'none' => 'Don\'t sort this column'
        ];
    }

    protected function constructSettingsArray()
    {
        $columns = Input::get('sorting_names');
        $modes = Input::get('sorting_modes');
        return array_combine($columns, $modes);
    }

    protected function getSortingSettings($settingName)
    {
        $settings = json_decode($this->sortingManager->get($settingName), true);
        $newSettings = [];

        foreach ($settings as $column => $order) {
            $setting = new \stdClass;
            $setting->column = $column;
            $setting->setting = $order;
            $newSettings[] = $setting;
        }

        return $newSettings;
    }

    public function getWork()
    {

        $settings = $this->getSortingSettings(SortingSettingsManager::WORK_LOG_TABLE_SETTING_NAME);

        $columns = [
          'date_created' => 'Date Created',
            'updated_at' => 'Date Updated',
            'property' => 'Road',
            'work_date' => 'Work Date',
            'employee_name' => 'Employee',
            'employee_regular_hours' => 'Employee Regular Hours',
            'employee_overtime_hours' => 'Employee Overtime Hours',
            'employee_cost' => 'Employee Cost',
            'project' => 'Project',
            'activity' => 'Activity',
            'district' => 'District',
            'department' => 'Department'
        ];

        return View::make('layouts.dialogs.sorting.sorting')
            ->with('title', 'Work Record Sorting')->with('settings', $settings)
            ->with('sortingModes', $this->getSortingModes())
            ->with('action', 'postWork')
            ->with('sortingColumns', $columns);
    }

    public function postWork()
    {
        if (Input::get('real_action') == 'reset') {
            UIBuilder::success('Work record sorting settings reset.', 'Reset Sorting Settings');
            $this->sortingManager->forget(SortingSettingsManager::WORK_LOG_TABLE_SETTING_NAME);
        } else {
            $newSettings = $this->constructSettingsArray();
            $this->sortingManager->set(SortingSettingsManager::WORK_LOG_TABLE_SETTING_NAME,
                json_encode($newSettings));
            UIBuilder::success('Work record sorting order updated.', 'Update Sorting Settings');
        }


        return Redirect::to(Redirector::getURL());
    }
}
