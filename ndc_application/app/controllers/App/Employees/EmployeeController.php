<?php namespace App\Employees;

use Illuminate\Support\Facades\Cache;
use Service\Employee;
use Session;
use View;
use Redirect;
use UI;
use Input;
use Redirector;
use ParadoxOne\NDCounties\Contracts\GenderRepositoryInterface;
use ParadoxOne\NDCounties\Contracts\EmployeeRepositoryInterface;
use ParadoxOne\NDCounties\IdentificationCode as IDCode;

class EmployeeController extends \AuthenticatedController
{
    protected $genderRepository;
    protected $employeeRepository;
    protected $activityRepository;

    public function __construct(GenderRepositoryInterface $genderRepo, EmployeeRepositoryInterface $employeeRepo)
    {
        parent::__construct();
        $this->requiresServiceAccount();

        $this->genderRepository = $genderRepo;
        $this->employeeRepository = $employeeRepo;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        Redirector::logCurrent();

        $foundResults = false;

        if (Input::get('search', false) == 1) {
            $ids = Cache::get(search_cache_key('employee_search'));
            if (is_array($ids) && count($ids) > 0) {
                $foundResults = true;
                $employees = $this->employeeRepository->getAllWithTrashed()->whereIn('id', $ids)->paginate(user_per_page());
            } else {
                $employees = $this->employeeRepository->getAll()->paginate(user_per_page());
                UI::info('Search contained no results.', 'Search Employees');
            }
        } else {
            $employees = $this->employeeRepository->getAll()->paginate(user_per_page());
        }

        return View::make('app.employees.list')->with('employees', $employees)->with('restrictionString', make_restrictions_string(array_pluck($employees, 'id')))->with('foundResults', $foundResults);
        ;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        if ($this->shouldInsertsAndUpdatesBeDisabled()) {
            return $this->flagDatabaseMaintenance();
        }

        $genders = $this->genderRepository->getGenders();
        return View::make('app.employees.create')->with('genders', $genders);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        if ($this->shouldInsertsAndUpdatesBeDisabled()) {
            return $this->flagDatabaseMaintenance();
        }

        $employee = $this->employeeRepository->create(Input::all());

        if ($employee === false) {
            UI::inputErrors();
            return Redirect::back()->withErrors($this->employeeRepository->errors())->withInput();
        } else {
            UI::success('Employee successfully added.', 'New Employee Resource');

            if (Input::get('submit') == 'return') {
                return Redirect::to(Redirector::getURL(route('employees.index')));
            } else {
                if ($this->userSettings->auto_clear_forms) {
                    return Redirect::back();
                } else {
                    return Redirect::back()->withInput(Input::except('first_name', 'middle_name', 'last_name', 'code', 'gender'));
                }
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        $employee = $this->employeeRepository->getModelById($id);

        Redirector::logCurrent();
        return View::make('app.employees.view')->with('employee', $employee);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        if ($this->shouldInsertsAndUpdatesBeDisabled()) {
            return $this->flagDatabaseMaintenance();
        }

        if ($id == 0) {
            return $this->flagSystemResourceUpdate();
        }
                
        $genders = $this->genderRepository->getGenders();

        $employee = $this->employeeRepository->getModelById($id);
        return View::make('app.employees.edit')->with('genders', $genders)->with('employee', $employee);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {
        if ($this->shouldInsertsAndUpdatesBeDisabled()) {
            return $this->flagDatabaseMaintenance();
        }

        if ($id == 0) {
            return $this->flagSystemResourceUpdate();
        }

        $employee = $this->employeeRepository->update($id, Input::all());

        if ($employee === false) {
            UI::inputErrors();
            return Redirect::back()->withErrors($this->employeeRepository->errors())->withInput();
        } else {
            UI::success('Employee successfully updated');
            return Redirect::to(Redirector::getURL(route('employees.index')));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        if ($this->shouldInsertsAndUpdatesBeDisabled()) {
            return $this->flagDatabaseMaintenance();
        }

        $id = Input::get('removeContext');

        $multiFlag = Input::get('removeMultiple', 0);

        if ($multiFlag == '1') {
            $id = explode(',', $id);
            $id = array_strip_values($id, 0);
        } else {
            if ($id == 0) {
                return $this->flagSystemResourceRemove();
            }
        }

        if (!is_array($id)) {
            $id = (array)$id;
        }

        $removedSuccessfully = $this->employeeRepository->remove($id);

        if ($removedSuccessfully) {
            UI::success('Employee resource removed.', 'Remove Employee');
        } else {
            UI::danger('There was a problem removing the employee.', 'Remove Employee');
        }

        return Redirect::back();
    }
}
