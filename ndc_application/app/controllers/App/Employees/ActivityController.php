<?php namespace App\Employees;

use View;
use Redirect;
use UI;
use Input;
use Redirector;
use Session;
use Service\Activity;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use ParadoxOne\NDCounties\Contracts\GenderRepositoryInterface;
use ParadoxOne\NDCounties\Contracts\ActivityRepositoryInterface;
use ParadoxOne\NDCounties\IdentificationCode as IDCode;

class ActivityController extends \AuthenticatedController
{
    protected $genderRepository;
    protected $employeeRepository;
    protected $activityRepository;

    public function __construct(GenderRepositoryInterface $genderRepo,
        ActivityRepositoryInterface $activityRepo)
    {
        parent::__construct();
        $this->requiresServiceAccount();

        $this->genderRepository = $genderRepo;
        $this->activityRepository = $activityRepo;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        Redirector::logCurrent();
        $foundResults = false;
        if (Input::get('search', false) == 1) {
            $ids = Cache::get(search_cache_key('activity_search'));
            if (is_array($ids) && count($ids) > 0) {
                $foundResults = true;
                $activities = $this->activityRepository->getAllWithTrashed()->whereIn('id', $ids)->paginate(user_per_page());
            } else {
                $activities = $this->activityRepository->getAll()->paginate(user_per_page());
                UI::info('Search contained no results.', 'Search Activities');
            }
        } else {
            $activities = $this->activityRepository->getAll()->paginate(user_per_page());
        }

        return View::make('app.activities.list')
            ->with('activities', $activities)
            ->with('restrictionString', make_restrictions_string(array_pluck($activities, 'id')))
            ->with('foundResults', $foundResults);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        if ($this->shouldInsertsAndUpdatesBeDisabled()) {
            return $this->flagDatabaseMaintenance();
        }

        return View::make('app.activities.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        if ($this->shouldInsertsAndUpdatesBeDisabled()) {
            return $this->flagDatabaseMaintenance();
        }

        $activity = $this->activityRepository->create(Input::all());

        if ($activity === false) {
            UI::inputErrors();
            return Redirect::back()->withErrors($this->activityRepository->errors())->withInput();
        } else {
            UI::success('Activity successfully added.', 'New Activity Resource');

            if (Input::get('submit') == 'return') {
                return Redirect::to(Redirector::getURL(route('activities.index')));
            } else {
                if ($this->userSettings->auto_clear_forms) {
                    return Redirect::back();
                } else {
                    return Redirect::back()->withInput(Input::except('code', 'name', 'description'));
                }
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        $activity = $this->activityRepository->getModelById($id);

        Redirector::logCurrent();

        return View::make('app.activities.view')->with('activity', $activity);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        if ($this->shouldInsertsAndUpdatesBeDisabled()) {
            return $this->flagDatabaseMaintenance();
        }

        if ($id == 0) {
            return $this->flagSystemResourceUpdate();
        }

        $activity = $this->activityRepository->getModelById($id);
        return View::make('app.activities.edit')->with('activity', $activity);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {
        if ($this->shouldInsertsAndUpdatesBeDisabled()) {
            return $this->flagDatabaseMaintenance();
        }

        if ($id == 0) {
            return $this->flagSystemResourceUpdate();
        }

        $activity = $this->activityRepository->update($id, Input::all());

        if ($activity === false) {
            UI::inputErrors();
            return Redirect::back()->withErrors($this->activityRepository->errors())->withInput();
        } else {
            UI::success('Activity successfully updated.', 'Update Activity');
            return Redirect::to(Redirector::getURL(route('activities.index')));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        if ($this->shouldInsertsAndUpdatesBeDisabled()) {
            return $this->flagDatabaseMaintenance();
        }

        $id = Input::get('removeContext');

        $multiFlag = Input::get('removeMultiple', 0);

        if ($multiFlag == '1') {
            $id = explode(',', $id);
            $id = array_strip_values($id, 0);
        } else {
            if ($id == 0) {
                return $this->flagSystemResourceRemove();
            }
        }

        if (!is_array($id)) {
            $id = (array)$id;
        }
        
        $removedSuccessfully = $this->activityRepository->remove($id);
        
        if ($removedSuccessfully) {
            UI::success('Activity resource removed', 'Remove Activity');
        } else {
            UI::danger('There was a problem removing the activity', 'Remove Activity');
        }

        return Redirect::back();
    }
}
