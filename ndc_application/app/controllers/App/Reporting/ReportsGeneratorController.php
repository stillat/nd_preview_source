<?php

namespace App\Reporting;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\View;
use ParadoxOne\NDCounties\Contracts\WorkEntries\WorkEntryReaderInterface;
use ParadoxOne\NDCounties\Reporting\ReportManager;
use ParadoxOne\NDCounties\Settings\ReportSettingsManager;

class ReportsGeneratorController extends \MinimalAuthenticatedController
{
    use \ParadoxOne\NDCounties\Reporting\Managers\SettingsManagerTrait;

    protected $reportManager;

    protected $workReader;

    protected $isSearchMode;

    protected $request;

    protected $settingsManager;

    public function __construct(
        ReportManager $manager,
        WorkEntryReaderInterface $reader,
        Request $request,
        ReportSettingsManager $settingsManager
    ) {
        parent::__construct();
        $this->settingsManager = $settingsManager;
        $this->reportManager = $manager;
        $this->workReader = $reader;
        $this->reportManager->setReader($this->workReader);
        $this->request = $request;
        $this->reportManager->request = $request;
        $this->reportManager->specialGroupColumnName = $this->request->get('special_grouping_name');
        $this->reportManager->specialGroupFilters = $this->getSpecialGroupPrimaryFilters();
        $this->reportManager->specialGroupSecondaryFilters = $this->getSpecialGroupSecondaryFilters();
        $this->reportManager->reportSettings = $this->getSettingsWithModifications();

        $this->isSearchMode = $this->request->has('search_records_instead');

        $this->reportManager->setReportDates((new Carbon($this->request->get('report_start_date')))->toDateString(),
            (new Carbon($this->request->get('report_end_date')))->toDateString());

        $this->reportManager->constraintNames = $this->request->get('constraint_names', []);
        $this->reportManager->constraintModes = $this->request->get('constraint_modes', []);
        $this->reportManager->constraintValues = $this->request->get('constraint_values', []);
        $this->reportManager->genericGroupingColumn = $this->request->get('generic_grouping_column');
        $this->reportManager->setUIGroupColumn($this->request->get('group_report_column', 'none'));
        $this->reportManager->materialFilters = $this->request->has('material_filters', null);
        $this->reportManager->materialMode = $this->request->has('materialMode');

        $this->reportManager->setEngineInternalMode($this->request->get('internal_reporting_mode', 0));
        $this->reportManager->setOrganizationStrategy($this->request->get('org_strategy', 0));
        $this->reportManager->setEquation($this->request->get('equation', 0));
        $this->reportManager->setGrandTotalBlock($this->request->get('block', 0));
        $this->reportManager->setReportID($this->request->get('report_mdef', 1));
    }

    public function postGenerate()
    {
        $this->reportManager->setReport('test_report'); // TODO: calls to setReport(<NAME>) should always be internal.

        $records = $this->reportManager->buildRecords();
        lk($records);

        return View::make('app.reporting.cheetah_view')
                   ->with('databaseOperationsDisabled', false)
                   ->with('manager', $this->reportManager);
    }

    private function getSpecialGroupPrimaryFilters()
    {
        $primaryFilters = [];
        switch ($this->request->get('special_grouping_column')) {

            case 'activity_id':
                $primaryFilters = $this->request->get('activity_filters');
                break;
            case 'department_id':
                $primaryFilters = $this->request->get('department_filters');
                break;
            case 'district_id':
                $primaryFilters = $this->request->get('district_filters');
                break;
            case 'employee_id':
                $primaryFilters = $this->request->get('employee_filters');
                break;
            case 'project_id':
                $primaryFilters = $this->request->get('project_filters');
                break;
            case 'property_id':
                $primaryFilters = $this->request->get('property_filters');
                break;
        }

        if ($primaryFilters !== null) {
            foreach ($primaryFilters as $key => $v) {
                $primaryFilters[$key] = intval($v);
            }

            return $primaryFilters;
        }

        return [];
    }

    private function getSpecialGroupSecondaryFilters()
    {
        $filters = $this->request->get('special_group_filters');

        if ($filters !== null) {
            foreach ($filters as $key => $value) {
                $filters[$key] = (int) $value;
            }

            return $filters;
        }

        return [];
    }

    private function applyReportingFilter($reportEngineSetting, $requestFilterName, $requestMode)
    {
        // If the input actually has the $requestFilterName, we can proceed.
        if ($this->request->has($requestFilterName)) {
            $includeMode = ($this->request->get($requestMode, 'inc') == 'inc') ? true : false;
            $this->reportManager->applyFilter($reportEngineSetting, $this->request->get($requestFilterName),
                $includeMode);
        }
    }
}
