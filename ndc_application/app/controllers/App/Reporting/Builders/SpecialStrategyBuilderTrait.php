<?php namespace App\Reporting\Builders;

use ParadoxOne\NDCounties\UI\UIBuilder as UI;
use Illuminate\Support\Facades\Input;

trait SpecialStrategyBuilderTrait
{
    /**
     * Gets the primary group filters used by the special report.
     *
     * @return array|mixed
     */
    private function getSpecialGroupPrimaryFilters()
    {
        $primaryFilters = [];

        switch (Input::get('special_grouping_column')) {

            case 'activity_id':
                $primaryFilters = Input::get('activity_filters');
                break;
            case 'department_id':
                $primaryFilters = Input::get('department_filters');
                break;
            case 'district_id':
                $primaryFilters = Input::get('district_filters');
                break;
            case 'employee_id':
                $primaryFilters = Input::get('employee_filters');
                break;
            case 'project_id':
                $primaryFilters = Input::get('project_filters');
                break;
            case 'property_id':
                $primaryFilters = Input::get('property_filters');
                break;
        }

        foreach ($primaryFilters as $key => $v) {
            $primaryFilters[$key] = intval($v);
        }

        return $primaryFilters;
    }

    /**
     * Gets the special group column name.
     *
     * @return mixed
     */
    private function getSpecialGroupColumnName()
    {
        return Input::get('special_grouping_column');
    }

    /**
     * Gets the special group name.
     *
     * @return mixed|string
     */
    private function getSpecialGroupName()
    {
        $groupName = Input::get('special_grouping_name');

        if (strlen($groupName) == 0) {
            return 'OTHER';
        }

        return $groupName;
    }

    /**
     * Gets the secondary group filters used by the special report.
     *
     * @return array|mixed
     */
    private function getSpecialGroupSecondaryFilters()
    {
        $filters = Input::get('special_group_filters');

        foreach ($filters as $key => $value) {
            $filters[$key] = (int)$value;
        }

        return $filters;
    }

    /**
     * Applies a filter to the report constructor.
     *
     * This function will find any related filters and apply them as necessary for group by, etc.
     *
     * @param $columnName
     */
    private function applyFilterToReportConstructor($columnName)
    {
        // Now we need to find the zeroth filter for the special grouping.
        $availableFilters =
            ['activity_filters', 'department_filters', 'district_filters', 'employee_filters', 'project_filters',
             'property_filters'];

        unset($availableFilters[array_search(str_replace('_id', '_filters', $columnName), $availableFilters)]);

        $alreadyFoundFilter = false;

        foreach ($availableFilters as $filter) {
            if (!$alreadyFoundFilter) {
                if (Input::has($filter)) {
                    $values = Input::get($filter);
                    if (count($values) > 0) {
                        $this->reportConstructor->applyFilter(str_replace('_filters', '_id', $filter), $values,
                                                              true);
                        $alreadyFoundFilter = true;
                    }
                }
            }
        }
    }

    private function applySpecialStrategy()
    {
        if (Input::has('special_group_filters')) {
            $primaryFilters = $this->getSpecialGroupPrimaryFilters();

            // This checks if there are any primary filters. If not, we need to report that there are no
            // primary filters in use and cannot generate the report.
            if (count($primaryFilters) == 0) {
                UI::info('In order to run a report with a special grouping, primary filters must be set.', 'Cannot Run Special Report');

                return false;
            }

            $columnName = $this->getSpecialGroupColumnName();

            $this->applyFilterToReportConstructor($columnName);

            // Set the special report information on the report constructor.
            $this->reportConstructor->specialReportGrouping($columnName, $primaryFilters,
                                                            $this->getSpecialGroupSecondaryFilters(), '',
                                                            $this->getSpecialGroupName());
        } else {
            UI::info('In order to run a report with a special grouping, special group filters need to be specified.', 'Cannot Run Special Report');

            return false;
        }

        // If we got to this point, the initial configuration passes have succeeded.
        return true;
    }
}
