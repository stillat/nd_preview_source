<?php namespace App\Reporting\Builders;

trait BreakdownHelpersBuilderTrait
{
    /**
     * Generates a breakdown class based on the settings.
     *
     * @param $settings
     * @return string
     */
    protected function getBreakdownClass(&$settings)
    {
        $breakDownClass = '';

        if ($settings->show_borders_on_breakdowns) {
            $breakDownClass .= 'report-bordered';
        }

        if ($settings->record_breakdown_style == 2) {
            $breakDownClass .= ' report-centered';
        } else {
            $breakDownClass .= ' table-responsive';
        }

        return $breakDownClass;
    }
}
