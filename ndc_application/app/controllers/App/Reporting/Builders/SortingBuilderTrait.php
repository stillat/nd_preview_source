<?php namespace App\Reporting\Builders;

trait SortingBuilderTrait
{

    public function getSortingColumns()
    {
        return [
            'date_created' => 'Date Created',
            'last_updated' => 'Date Updated',
            'work_date' => 'Work Date',
            'employee_name' => 'Employee',
            'employee_regular_hours' => 'Employee Hours',
            'employee_overtime_hours' => 'Employee Overtime Hours',
            'employee_cost' => 'Employee Cost',
            'project' => 'Project',
            'activity' => 'Activity',
            'district' => 'District',
            'department' => 'Department',
            'property' => 'Road'
        ];
    }

    public function getSortingModes()
    {
        return [
            'asc' => 'Ascending',
            'desc' => 'Descending',
            'none' => 'Don\'t sort this column'
        ];
    }

}
