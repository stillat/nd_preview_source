<?php namespace App\Reporting\Builders;

use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Auth;

trait RecordRetrievalBuilderTrait
{
    protected $cachedSettings = null;

    private function getCachedSettings()
    {
        if ($this->cachedSettings == null) {
            $this->cachedSettings = $this->settingsManager->getReportSettings();
        }

        return $this->cachedSettings;
    }

    private function getActivities()
    {
        $returnedActivities = Cache::rememberForever('reportActivities_'.Auth::user()->last_tenant, function () {
            $returnedActivities = [];

            if ($this->getCachedSettings()->show_removed_filters) {
                $activities = $this->activityRepository->getAllWithTrashed()->get();
            } else {
                $activities = $this->activityRepository->getAll()->get();
            }


            foreach ($activities as $activity) {
                $returnedActivities[$activity->id] = $this->formatter->activity($activity);
            }

            unset($activities);

            return $returnedActivities;
        });

        return $returnedActivities;
    }

    private function getDepartments()
    {
        $returnedDepartments = Cache::rememberForever('reportDepartments_'.Auth::user()->last_tenant, function () {
            $returnedDepartments = [];

            if ($this->getCachedSettings()->show_removed_filters) {
                $departments = $this->departmentsRepository->getAllWithTrashed()->get();
            } else {
                $departments = $this->departmentsRepository->getAll()->get();
            }

            foreach ($departments as $dept) {
                $returnedDepartments[$dept->id] = $this->formatter->department($dept);
            }

            unset($departments);

            return $returnedDepartments;
        });

        return $returnedDepartments;
    }

    private function getDistricts()
    {
        $returnedDistricts = Cache::rememberForever('reportDistricts_'.Auth::user()->last_tenant, function () {
            $returnedDistricts = [];

            if ($this->getCachedSettings()->show_removed_filters) {
                $districts = $this->districtsRepository->getAllWithTrashed()->get();
            } else {
                $districts = $this->districtsRepository->getAll()->get();
            }

            foreach ($districts as $dist) {
                $returnedDistricts[$dist->id] = $this->formatter->district($dist);
            }

            unset($districts);

            return $returnedDistricts;
        });

        return $returnedDistricts;
    }

    private function getEmployees()
    {
        $returnedEmployees = Cache::rememberForever('reportEmployees_'.Auth::user()->last_tenant, function () {
            $returnedEmployees = [];

            if ($this->getCachedSettings()->show_removed_filters) {
                $employees = $this->employeeRepository->getAllWithTrashed()->get();
            } else {
                $employees = $this->employeeRepository->getAll()->get();
            }

            foreach ($employees as $emp) {
                $emp->name                   = $emp->last_name . ', ' . $emp->first_name;
                $returnedEmployees[$emp->id] = $this->formatter->employee($emp);
            }

            unset($employees);

            return $returnedEmployees;
        });

        return $returnedEmployees;
    }

    private function getEquipmentUnits()
    {
        $returnedUnits = Cache::rememberForever('reportEquipments_'.Auth::user()->last_tenant, function () {
            $returnedUnits = [];

            if ($this->getCachedSettings()->show_removed_filters) {
                $units = $this->propertiesRepository->getAllWithTrashed()->where('property_type', '=', 2)->get();
            } else {
                $units = $this->propertiesRepository->getAll()->where('property_type', '=', 2)->get();
            }

            foreach ($units as $unit) {
                $returnedUnits[$unit->id] = $this->formatter->property($unit);
            }
            unset($units);

            return $returnedUnits;
        });

        return $returnedUnits;
    }

    private function getMaterials()
    {
        $returnedMaterials = Cache::rememberForever('reportMaterials_'.Auth::user()->last_tenant, function () {
            $returnedMaterials = [];

            if ($this->getCachedSettings()->show_removed_filters) {
                $materials = $this->materialsRepository->getAllWithTrashed()->get();
            } else {
                $materials = $this->materialsRepository->getAll()->get();
            }

            foreach ($materials as $material) {
                $returnedMaterials[$material->id] = $this->formatter->material($material);
            }
            unset($materials);

            return $returnedMaterials;
        });

        return $returnedMaterials;
    }

    private function getProjects()
    {
        $returnedProjects = Cache::rememberForever('reportProjects_'.Auth::user()->last_tenant, function () {
            $returnedProjects = [];

            if ($this->getCachedSettings()->show_removed_filters) {
                $projects = $this->projectRepository->getAllWithTrashed()->get();
            } else {
                $projects = $this->projectRepository->getAll()->get();
            }

            foreach ($projects as $project) {
                $returnedProjects[$project->id] = $this->formatter->project($project);
            }
            unset($projects);

            return $returnedProjects;
        });

        return $returnedProjects;
    }

    private function getPropertyTypes()
    {
        //$propertyTypes = $this->propertyTypesRepository->getPropertyTypes()->lists('code', 'id');
        $propertyTypes = $this->propertyTypesRepository->getPropertyTypesForReportScope();
        return $propertyTypes;
    }

    private function getRoads()
    {
        $returnedProperties = Cache::rememberForever('reportPropertiesRoad_'.Auth::user()->last_tenant, function () {
            $returnedProperties = [];
            $setting            = $this->formatter->getSettings('property');

            if ($this->getCachedSettings()->show_removed_filters) {
                $properties = $this->propertiesRepository->getAllWithTrashed()->whereIn('property_type', [0, 1])->get();
            } else {
                $properties = $this->propertiesRepository->getAll()->whereIn('property_type', [0, 1])->get();
            }


            foreach ($properties as $property) {
                $returnedProperties[$property->id] = [$this->formatter->property($property), $property->type->id];
            }
            unset($properties);
            return $returnedProperties;
        });

        return $returnedProperties;
    }

    private function getProperties()
    {
        $returnedProperties = Cache::rememberForever('reportProperties_'.Auth::user()->last_tenant, function () {
            $returnedProperties = [];
            $setting            = $this->formatter->getSettings('property');

            if ($this->getCachedSettings()->show_removed_filters) {
                $properties = $this->propertiesRepository->getAllWithTrashed()->get();
            } else {
                $properties = $this->propertiesRepository->getAll()->get();
            }

            foreach ($properties as $property) {
                if ($property->property_type == 2) {
                    switch ($setting) {
                        case 2:
                            $property->code .= ' (Equipment)';
                            break;
                        case 3:
                        default:
                            $property->name .= ' (Equipment)';
                            break;
                    }
                }
                $returnedProperties[$property->id] = [$this->formatter->property($property), $property->type->id];
            }
            unset($properties);

            return $returnedProperties;
        });

        return $returnedProperties;
    }
}
