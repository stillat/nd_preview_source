<?php namespace App\Reporting\Builders;

trait ConstraintsBuilderTrait
{
    private function getFilterModes()
    {
        return [
          'inc' => 'Include',
          'exc' => 'Exclude'
        ];
    }

    private function getConstraintModes()
    {
        return [
          '=' => 'is exactly',
          '>' => 'is greater than',
          '<' => 'is less than',
          '>=' => 'is greater than or equal to',
          '<=' => 'is less than or equal to'
        ];
    }

    private function getConstraints()
    {
        return [
          'emp_regular_hours' => 'Employee regular hours',
          'emp_overtime_hours' => 'Employee overtime hours',
          'emp_regular_total' => 'Employee regular hours cost',
          'emp_overtime_total' => 'Employee overtime hours cost',
          'emp_combined_total' => 'Employee cost (combined)',

          'material_total' => 'Material cost',
          'equipment_cost' => 'Equipment cost',
          'fuel_cost' => 'Fuel cost',

          'material_quantity' => 'Material quantity',
          'fuel_quantity' => 'Fuel quantity',
          'equipment_quantity' => 'Equipment quantity',

          'mbilling_total' => 'Miscellaneous billing cost',
          'mbilling_quantity' => 'Miscellaneous billing quantity'
        ];
    }
}
