<?php namespace App\Reporting\Builders;

use Illuminate\Support\Facades\Input;

trait ConstraintManagerBuilderTrait
{

    private function buildSortingArray()
    {
        $names = Input::get('sorting_names');
        $modes = Input::get('sorting_modes');

        if (count($names) !== count($modes)) {
            return [];
        }

        if (count($names) == 0) {
            return [];
        }

        return array_combine(Input::get('sorting_names'), Input::get('sorting_modes'));
    }

    private function buildConstraintsArray()
    {
        if (Input::has('constraint_names')) {
            $constraints = Input::get('constraint_names');
            $constraintModes = Input::get('constraint_modes');
            $constraintValues = Input::get('constraint_values');

            $returnedConstraints = [];

            for ($i = 0; $i < count($constraints); $i++) {
                $returnedConstraints[] = [$constraints[$i], $constraintModes[$i], $constraintValues[$i]];
            }

            return $returnedConstraints;
        }

        return [];
    }
}
