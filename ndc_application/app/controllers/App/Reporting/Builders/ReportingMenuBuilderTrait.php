<?php namespace App\Reporting\Builders;

use Illuminate\Support\Str;
use Illuminate\Support\Facades\Auth;
use ParadoxOne\NDCounties\Reporting\Services\ReportExtensionListService;
use Stillat\Menu\Support\Facades\Menu;

trait ReportingMenuBuilderTrait
{
    private function createReportingMenu()
    {
        $reportCategories = $this->reportCategoryRepository->getCategories();
        $reports          = $this->metaDefinitionsRepository->getDefinitions()->whereIn('system_reports.account_id', [0, Auth::user()->last_tenant])->get();
        $reports          = ReportExtensionListService::getList($reports);

        $reportMenu =
            Menu::handle('reportingMenu', 'Reporting Menu', ['class' => 'nav nav-pills nav-stacked nav-bracket', 'id' => 'reportingMenu']);

        $reportMenu->addItem('#redirectBack', '<i class="fa fa-arrow-left"></i> <span>Back to Previous Action</span>',
                             ['id' => 'goback']);

        unset($reportCategories[6]);

        // Force "My Reports" to be on top
        $reportCategories = [6 => 'My Reports'] + $reportCategories;

        foreach ($reportCategories as $category => $categoryName) {
            $categoryReports = array_pluck_where($reports, 'report_category', $category);

            $categoryMenu = Menu::handle('reportingCategoryMenu' . $category,
                                         '<i class="fa" ><span style="margin-left: -11px;font-weight: bolder;font-family: sans-serif;color: rgb(176, 176, 176);">' .
                                         strtoupper(substr($categoryName, 0, 1)) .
                                         '</span></i><span class="badge badge-primary">' . count($categoryReports) .
                                         '</span><span> ' . $categoryName . '</span>');

            foreach ($categoryReports as $report => $reportData) {
                $categoryMenu->addItem('#'.md5('category'.$category.'_report'.$report).Str::quickRandom(32), '<i class="'.e($reportData->getIcon()).'"></i> '.e($reportData->getName()),
                                       [
                                           'data' => [
                                               'report-toggle' => 'true',
                                               'extension' => (string)$reportData->isExtension(),
                                               'report' => $reportData->getReportID(),
                                               'limited' => (string)$reportData->isLimited(),
                                               'notify'  => e($reportData->getNotificationMessage()),
                                               'propertylimits' => $reportData->getPropertyTypeLimits(),
                                               'srlauto' => $reportData->getEncodedSettings(),
                                               'description' => base64_encode($reportData->getDescription()),
                                               'report-name' => $reportData->getName()
                                           ]
                                       ]);
            }

            $reportMenu->addMenu($categoryMenu);
        }
    }
}
