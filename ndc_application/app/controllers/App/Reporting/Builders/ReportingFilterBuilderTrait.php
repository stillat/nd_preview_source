<?php namespace App\Reporting\Builders;

use Illuminate\Support\Facades\Input;

trait ReportingFilterBuilderTrait
{
    /**
     * Applies the filters to the reporting engine.
     * @param $reportEngineSetting
     * @param $requestFilterName
     * @param $requestMode
     */
    private function applyReportingFilter($reportEngineSetting, $requestFilterName, $requestMode)
    {
        $includeMode = (Input::get($requestMode, 'inc') == 'inc') ? true : false;
        // If the input actually has the $requestFilterName, we can proceed.
        if (!is_array($requestFilterName)) {
            if (Input::has($requestFilterName)) {
                $this->reportConstructor->applyFilter($reportEngineSetting, Input::get($requestFilterName), $includeMode);
            }
        } else {
                $filters = [];
                foreach ($requestFilterName as $filterName) {
                    $filters = array_merge($filters, Input::get($filterName, []));
                }
                $filters = array_unique($filters);
                $this->reportConstructor->applyFilter($reportEngineSetting, $filters, $includeMode);
        }
    }
}
