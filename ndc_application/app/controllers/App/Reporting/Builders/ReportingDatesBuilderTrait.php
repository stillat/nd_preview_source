<?php namespace App\Reporting\Builders;

use Carbon\Carbon;
use Illuminate\Support\Facades\Session;

trait ReportingDatesBuilderTrait
{
    private function getReportingDates()
    {
        $oldStartDate = null;
        $oldEndDate   = null;

        if (Session::has('reporting_old_date')) {
            $dates        = Session::get('reporting_old_date');
            $oldStartDate = $dates['start'];
            $oldEndDate   = $dates['end'];
        } else {
            $oldStartDate = Carbon::now()->toDateString();
            $oldEndDate   = Carbon::now()->toDateString();
        }

        return [$oldStartDate, $oldEndDate];
    }
}
