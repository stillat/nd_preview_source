<?php

namespace App;

use AuthenticatedController;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Str;
use ParadoxOne\NDCounties\UI\UIBuilder;

class UnTrashController extends AuthenticatedController
{

    private function displaySuccessMessage($name, $title, $count, $prefix = '')
    {
        if (Session::has('failedRestoredRecords')) {
            $messageBody = 'Some records could not be restored. The following records could be restored because there are existing conflicts:<br><br><ul class="list-unstyled">';
            foreach (Session::get('failedRestoredRecords') as $record) {
                $recordName = '';
                if (property_exists($record,'code')) {
                    $recordName = $record->code;
                }elseif (property_exists($record, 'name')) {
                    $recordName = $record->name;
                }
                $messageBody.= '<li><strong>'.$recordName.'</strong></li>';
            }
            $messageBody .= '</ul>';
            UIBuilder::warning($messageBody, $title);
            Session::forget('failedRestoredRecords');
        } else {
            UIBuilder::success($prefix.Str::plural($name, $count).' successfully restored.', $title);
        }
    }

    private function displayErrorMessage($name, $title, $count, $prefix = '')
    {
        UIBuilder::danger('There was a problem restoring the '.$prefix.Str::plural(Str::lower($name), $count).'.', $title);
    }

    public function getActivities()
    {
        $unTrash = (array)Input::get('d');
        /** @var \ParadoxOne\NDCounties\Contracts\ActivityRepositoryInterface $activities */
        $activities = App::make('ParadoxOne\NDCounties\Contracts\ActivityRepositoryInterface');
        $success = $activities->unTrash($unTrash);
        if ($success) {
            $this->displaySuccessMessage('Activity', 'Restore Activities', count($unTrash));
        } else {
            $this->displayErrorMessage('Activity', 'Restore Activities', count($unTrash));
        }

        return Redirect::back();
    }

    public function getDepartments()
    {
        $unTrash = (array)Input::get('d');
        /** @var \ParadoxOne\NDCounties\Contracts\DepartmentsRepositoryInterface $departments */
        $departments = App::make('ParadoxOne\NDCounties\Contracts\DepartmentsRepositoryInterface');
        $success = $departments->unTrash($unTrash);
        if ($success) {
            $this->displaySuccessMessage('Department', 'Restore Departments', count($unTrash));
        } else {
            $this->displayErrorMessage('Department', 'Restore Departments', count($unTrash));
        }

        return Redirect::back();
    }

    public function getDistricts()
    {
        $unTrash = (array)Input::get('d');
        /** @var \ParadoxOne\NDCounties\Contracts\DistrictsRepositoryInterface $districts */
        $districts = App::make('ParadoxOne\NDCounties\Contracts\DistrictsRepositoryInterface');
        $success = $districts->unTrash($unTrash);
        if ($success) {
            $this->displaySuccessMessage('District', 'Restore Districts', count($unTrash));
        } else {
            $this->displayErrorMessage('District', 'Restore Districts', count($unTrash));
        }

        return Redirect::back();
    }

    public function getProjects()
    {
        $unTrash = (array)Input::get('d');
        /** @var \ParadoxOne\NDCounties\Contracts\Projects\ProjectRepositoryInterface $projects */
        $projects = App::make('ParadoxOne\NDCounties\Contracts\Projects\ProjectRepositoryInterface');
        $success = $projects->unTrash($unTrash);
        if ($success) {
            $this->displaySuccessMessage('Project', 'Restore Projects', count($unTrash));
        } else {
            $this->displayErrorMessage('Project', 'Restore Projects', count($unTrash));
        }

        return Redirect::back();
    }

    public function getFuels()
    {
        $unTrash = (array)Input::get('d');
        /** @var \ParadoxOne\NDCounties\Contracts\Resources\FuelRepositoryInterface $fuels */
        $fuels = App::make('ParadoxOne\NDCounties\Contracts\Resources\FuelRepositoryInterface');
        $success = $fuels->unTrash($unTrash);
        if ($success) {
            $this->displaySuccessMessage('Fuel', 'Restore Fuels', count($unTrash));
        } else {
            $this->displayErrorMessage('Fuel', 'Restore Fuels', count($unTrash));
        }

        return Redirect::back();
    }

    public function getMaterials()
    {
        $unTrash = (array)Input::get('d');
        /** @var \ParadoxOne\NDCounties\Contracts\Resources\MaterialRepositoryInterface $materials */
        $materials = App::make('ParadoxOne\NDCounties\Contracts\Resources\MaterialRepositoryInterface');
        $success = $materials->unTrash($unTrash);
        if ($success) {
            $this->displaySuccessMessage('Material', 'Restore Materials', count($unTrash));
        } else {
            $this->displayErrorMessage('Material', 'Restore Materials', count($unTrash));
        }

        return Redirect::back();
    }

    public function getEmployees()
    {
        // ParadoxOne\NDCounties\Contracts\EmployeeRepositoryInterface
        $unTrash = (array)Input::get('d');
        /** @var \ParadoxOne\NDCounties\Contracts\EmployeeRepositoryInterface $employees */
        $employees = App::make('ParadoxOne\NDCounties\Contracts\EmployeeRepositoryInterface');
        $success = $employees->unTrash($unTrash);
        if ($success) {
            $this->displaySuccessMessage('Employee', 'Restore Employees', count($unTrash));
        } else {
            $this->displayErrorMessage('Employee', 'Restore Employees', count($unTrash));
        }

        return Redirect::back();
    }

    public function getProperties()
    {
        // ParadoxOne\NDCounties\Contracts\Properties\PropertiesRepositoryInterface
        $unTrash = (array)Input::get('d');
        /** @var \ParadoxOne\NDCounties\Contracts\Properties\PropertiesRepositoryInterface $properties */
        $properties = App::make('ParadoxOne\NDCounties\Contracts\Properties\PropertiesRepositoryInterface');
        $success = $properties->unTrash($unTrash);
        if ($success) {
            $this->displaySuccessMessage('Property', 'Restore Properties', count($unTrash));
        } else {
            $this->displayErrorMessage('Property', 'Restore Properties', count($unTrash));
        }

        return Redirect::back();
    }

    public function getFsAccounts()
    {
        // ParadoxOne\NDCounties\Contracts\Finance\AccountRepositoryInterface
        $unTrash = (array)Input::get('d');
        /** @var \ParadoxOne\NDCounties\Contracts\Finance\AccountRepositoryInterface $accounts */
        $accounts = App::make('ParadoxOne\NDCounties\Contracts\Finance\AccountRepositoryInterface');
        $success = $accounts->unTrash($unTrash);
        if ($success) {
            $this->displaySuccessMessage('Account', 'Restore Finance Accounts', count($unTrash), 'Finance ');
        } else {
            $this->displayErrorMessage('Account', 'Restore Finance Accounts', count($unTrash), 'Finance ');
        }

        return Redirect::back();
    }

    public function getFsTaxRates()
    {
        // ParadoxOne\NDCounties\Contracts\Finance\Taxes\TaxRateRepositoryInterface
        $unTrash = (array)Input::get('d');
        /** @var \ParadoxOne\NDCounties\Contracts\Finance\Taxes\TaxRateRepositoryInterface $taxRates */
        $taxRates = App::make('ParadoxOne\NDCounties\Contracts\Finance\Taxes\TaxRateRepositoryInterface');
        $success = $taxRates->unTrash($unTrash);
        if ($success) {
            $this->displaySuccessMessage('Rate', 'Restore Tax Rates', count($unTrash), 'Tax ');
        } else {
            $this->displayErrorMessage('Rate', 'Restore Tax Rates', count($unTrash), 'Tax ');
        }

        return Redirect::back();
    }

}