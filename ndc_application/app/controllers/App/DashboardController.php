<?php namespace App;

use App;
use Auth;
use AuthenticatedController;
use ParadoxOne\NDCounties\Contracts\ActivityStreamRepositoryInterface;
use ParadoxOne\NDCounties\UI\Streams\Activity\UserActivityStream;
use Redirect;
use Redirector;
use Session;
use UI;
use View;

class DashboardController extends AuthenticatedController
{
    /**
     * @var ActivityStreamRepositoryInterface
     */
    private $activityStreamRepository;
    /**
     * @var UserActivityStream
     */
    private $activityStream;

    public function __construct(ActivityStreamRepositoryInterface $activityStreamRepository, UserActivityStream $activityStream)
    {
        parent::__construct();
        $this->beforeFilter('accountRedirect', array('except' => array('getSignout')));
        $this->activityStreamRepository = $activityStreamRepository;
        $this->activityStream           = $activityStream;
    }

    public function getIndex()
    {
        Redirector::logCurrent();

        return View::make('app.dashboard');
    }

    public function getRecentActivity()
    {
        $streamedData = $this->activityStreamRepository->getRecentActivity(Auth::user()->last_tenant)->paginate(30);

        $firstStream = $streamedData->splice(0, 10)->toArray();
        $secondStream = $streamedData->splice(10, 10)->toArray();
        $thirdStream = $streamedData->splice(20, 10)->toArray();

        if (count($firstStream) == 0) {
            $firstStream = null;
        }
        if (count($secondStream) == 0) {
            $secondStream = null;
        }
        if (count($thirdStream) == 0) {
            $thirdStream = null;
        }


        $this->activityStream->setStreamData($streamedData);

        if ($firstStream != null) {
            $this->activityStream->setStreamData($firstStream);
            $firstStream = $this->activityStream->getStream();
        }
        if ($secondStream != null) {
            $this->activityStream->setStreamData($secondStream);
            $secondStream = $this->activityStream->getStream();
        }
        if ($thirdStream != null) {
            $this->activityStream->setStreamData($thirdStream);
            $thirdStream = $this->activityStream->getStream();
        }

        Redirector::logCurrent();

        return View::make('app.recent_activity_stream')->with('streamedData', $streamedData)
                ->with('firstStream', $firstStream)->with('secondStream', $secondStream)->with('thirdStream', $thirdStream);
    }

    public function getSignout()
    {
        Auth::logout();
        Session::flash('userLoggedOut', true);

        return Redirect::to('login');
    }
}
