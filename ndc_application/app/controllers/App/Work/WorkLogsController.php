<?php namespace App\Work;

use AuthenticatedController;
use AutomatedWorkInvoiceSubscriber;
use Carbon;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Paginator;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\View;
use ParadoxOne\NDCounties\Contracts\Finance\Invoices\InvoiceReaderRepositoryInterface;
use ParadoxOne\NDCounties\Contracts\Properties\PropertyTypesRepositoryInterface;
use ParadoxOne\NDCounties\Contracts\WorkEntries\WorkEntryDestroyerInterface;
use ParadoxOne\NDCounties\Contracts\WorkEntries\WorkEntryReaderInterface;
use ParadoxOne\NDCounties\Contracts\WorkEntries\WorkEntryUpdaterRepositoryInterface;
use ParadoxOne\NDCounties\Contracts\WorkEntries\WorkEntryWriterRepositoryInterface;
use ParadoxOne\NDCounties\Database\Adapters\AdapterManager;
use ParadoxOne\NDCounties\Database\Repositories\WorkEntries\DynamicConsumableHydrator;
use ParadoxOne\NDCounties\Database\Repositories\WorkEntries\Updaters\DynamicUpdaterConsumableHydrator;
use ParadoxOne\NDCounties\Database\Repositories\WorkEntries\Updaters\LockedRecordException;
use ParadoxOne\NDCounties\UserSessionManager;
use Redirector;
use UI;

class WorkLogsController extends AuthenticatedController
{
    protected $hydrator = null;



    protected $workRepository = null;

    protected $adapterManager = null;

    protected $typesRepository = null;

    protected $userSessionManager = null;

    protected $workReader = null;

    protected $workDestroyer = null;

    protected $workUpdater = null;

    protected $updateHydrator = null;

    protected $invoiceReader = null;

    protected $isSearchMode = false;

    public function __construct(

        DynamicConsumableHydrator $hydrator,

        WorkEntryWriterRepositoryInterface $workRepository,

        AdapterManager $adapterManager,

        PropertyTypesRepositoryInterface $typesRepository,

        UserSessionManager $userSessionManager,

        WorkEntryReaderInterface $workReader,

        WorkEntryDestroyerInterface $destroyer,

        InvoiceReaderRepositoryInterface $invoiceReader,

        WorkEntryUpdaterRepositoryInterface $workEntryUpdater,

        DynamicUpdaterConsumableHydrator $updateHydrator

    ) {
        parent::__construct();

        $this->requiresServiceAccount();



        $this->hydrator           = $hydrator;

        $this->updateHydrator     = $updateHydrator;

        $this->workRepository     = $workRepository;

        $this->adapterManager     = $adapterManager;

        $this->typesRepository    = $typesRepository;

        $this->userSessionManager = $userSessionManager;

        $this->workReader         = $workReader;

        $this->workDestroyer      = $destroyer;

        $this->invoiceReader      = $invoiceReader;

        $this->workUpdater        = $workEntryUpdater;

        $this->workUpdater->setPretendMode(false);
    }



    /**

     * Display a listing of the resource.

     *

     * @return Response

     */

    public function index()
    {
        Redirector::logCurrent();



        $currentPage = Input::get('page', 1);



        $this->isSearchMode = Input::has('search');



        Paginator::setCurrentPage($currentPage);
        $this->workReader->reader()->setLegacyMode(false)->withDefaults();



        $workRecords = [];

        if (!$this->isSearchMode) {
            // We do not want to restrict the active year when we are viewing
            // records from a search.
            $activeYear = user_settings()['active_year'];
            $this->workReader->reader()->whereYear($activeYear);
            //$this->workReader->reader()->forcedIndex = false;
            $workRecords = $this->workReader->setPagination(user_per_page(), $currentPage)->getRecords();
        } else {
            // We need to get all the work record IDs from the search. Amazing.
            //  $workRecordIDs = Input::get('restrictions');

            $workRecordIDs = Cache::get(Auth::user()->last_tenant . Auth::user()->id . '_work_record_search', []);

            if (count($workRecordIDs) > 0) {
                $this->workReader->reader()->whereWorkEntriesID($workRecordIDs);

                $workRecords = $this->workReader->setPagination(user_per_page(), $currentPage)->getRecords();
            } else {
                $workRecords = [];
            }
        }



        $recordIDString = make_restrictions_string($this->workReader->reader()->getWorkRecordIDs());





        if (Session::has('reporting_old_date')) {
            $dates        = Session::get('report_old_date');

            $oldStartDate = $dates['start'];

            $oldEndDate   = $dates['end'];
        } else {
            $oldStartDate = Carbon::now()->toDateString();

            $oldEndDate   = Carbon::now()->toDateString();
        }



        $paginator = Paginator::make($workRecords, $this->workReader->count(), user_per_page());



        $metaInformation                 = new \stdClass;

        $metaInformation->equipmentCount = 0;

        $metaInformation->materialCount  = $this->workReader->reader()->getHighestMaterialCount();

        $metaInformation->fuelCount      = $this->workReader->reader()->getHighestFuelCount();



        $formatter              = App::make('formatter');

        $recordOverrideSettings = !$formatter->getSettings('useOnRecords', 'useOnRecords');

        $header = $this->workReader->reader()->getReportPropertyHeader();


        return View::make('app.work.list')->with('workRecords', $workRecords)

                   ->with('workMeta', $metaInformation)->with('paginator', $paginator)

                   ->with('workYears', $this->workReader->reader()->getWorkYears())

                   ->with('isSortingDisabled', $this->workReader->reader()->isSortingDisabled())

                   ->with('workRecordIDs', $recordIDString)->with('startDate', $oldStartDate)

                   ->with('endDate', $oldEndDate)

                   ->with('formatter', $formatter)->with('recordOverrideSettings', $recordOverrideSettings)

                   ->with('searchMode', $this->isSearchMode)
                   ->with('reportHeader', $header);
    }





    /**

     * Show the form for creating a new resource.

     *

     * @return Response

     */

    public function create()
    {
        if ($this->shouldInsertsAndUpdatesBeDisabled()) {
            return $this->flagDatabaseMaintenance();
        }

        if ($this->isMobileOrTablet()) {
            return $this->requireDesktop();
        }
        // Get the work adapters.
        $propertyTypes = $this->typesRepository->getPropertyTypes()->get();
        $adapters = $this->adapterManager->getSortedAdapters();
        $workRecordResetSettings = App::make('\ParadoxOne\NDCounties\Settings\WorkRecordSettingsManager');

        $lastRecord = $this->workReader->getLastSavedRecordID();

        return View::make('app.work.create')
                   ->with('lastRecordID' ,$lastRecord)
                   ->with('types', $propertyTypes)->with('propertyAdapters', $adapters)
                   ->with('workDate', $this->userSessionManager->getWorkDate())
                   ->with('dueDate', $this->userSessionManager->getDueDate())
                   ->with('workResetSettings', $workRecordResetSettings->getWorkRecordResetSettings());
    }



    /**

     * Store a newly created resource in storage.

     *

     * @return Response

     */

    public function store()
    {
        if (!Request::ajax()) {
           App::abort(404);
        }



        $this->hydrator->setRawData(Input::all());

        $this->hydrator->hydrateConsumables();



        $workDate = new Carbon(Input::get('record_date'));



        $includeInReports = true;



        if (Input::has('exclude_from_reports')) {
            $includeInReports = false;
        }



        $this->workRepository->setWorkDate($workDate->toDateTimeString());

        $this->workRepository->setEmployee(Input::get('record_employee', 0));

        $this->workRepository->setHoursWorked(Input::get('regular_hours_worked'), Input::get('overtime_hours_worked'));

        $this->workRepository->setHistoricWageData(Input::get('hourly_rate'), Input::get('overtime_rate'));

        $this->workRepository->setHistoricFringeBenefits(Input::get('hourly_benefits'),

            Input::get('overtime_benefits'));



        $this->workRepository->setActivity(Input::get('record_activity', 0));

        $this->workRepository->setDepartment(Input::get('record_department', 0));

        $this->workRepository->setProject(Input::get('record_project', 0));

        $this->workRepository->setDistrict(Input::get('record_district', 0));

        $this->workRepository->setProperty(Input::get('record_actual_property', 0));

        $this->workRepository->setOrganizationProperty(Input::get('record_organization_actual_property', 0));

        $this->workRepository->setOrganizationPropertyContext(Input::get('record_organization_property_context', 0));



        $this->workRepository->setExcludedFromReports($includeInReports);



        $this->workRepository->setMiscBillingQuantity(Input::get('misc_billing_quantity', 0));

        $this->workRepository->setMiscBillingUnit(Input::get('misc_billing_unit', 9));

        $this->workRepository->setMiscBillingRate(Input::get('misc_billing_rate', 0));

        $this->workRepository->setMiscBillingDescription(Input::get('misc_billing_description', ''));

        $this->workRepository->setIsCreditInventoryMode(Input::has('credit_inventory'));



        // Set whether or not the employee is taxed.

        if (Input::has('employee_taxable')) {
            $this->workRepository->setEmployeeIsTaxed(true);
        }



        $this->workRepository->setPropertyContext(Input::get('record_property_context', 0));



        $this->workRepository->setActivityDescription(Input::get('activity_description'));



        /**

         * if (Input::get('invoice_override', 0) == 1) {

        $this->workRepository->setInvoiceOverrideTotal(Input::get('invoice_override_total', 0));

        } else {

        $this->workRepository->setInvoiceOverrideTotal(false);

        }

         */

        $this->workRepository->setInvoiceOverrideTotal(Input::get('invoice_override_total', 0));



        foreach ($this->hydrator->getConsumablesArray() as $consumable) {
            $this->workRepository->attachConsumable($consumable);
        }



        $result = $this->workRepository->commit();



        // Now we shoul

        $cacheKey       = Auth::user()->last_tenant . '_available_years';

        $availableYears = Cache::get($cacheKey, null);



        if ($availableYears !== null) {
            if (!in_array($workDate->year, $availableYears)) {
                Cache::forget($cacheKey);
            }
        }



        unset($cacheKey);

        unset($availableYears);



        if ($result) {

            // Here we will update some session stuff.

            $this->userSessionManager->setWorkDate(Input::get('record_date'));

            $this->userSessionManager->setDueDate(Input::get('billing_invoice_due_date'));

            $id = $this->workReader->getLastSavedRecordID();

            return Response::json(['success' => true, 'record' => $id]);
        }



        return Response::json(['success' => false]);
    }





    /**

     * Display the specified resource.

     *

     * @param  int $id

     * @return Response

     */

    public function show($id)
    {
        $record = $this->workReader->getSingleRecord($id);



        Redirector::logCurrent();



        return View::make('app.work.view')->with('record', $record)

                   ->with('reader', $this->workReader)

                   ->with('manager', $this->adapterManager);
    }





    /**

     * Show the form for editing the specified resource.

     *

     * @param  int $id

     * @return Response

     */

    public function edit($id)
    {
        if ($this->shouldInsertsAndUpdatesBeDisabled()) {
            return $this->flagDatabaseMaintenance();
        }



        if ($this->isMobileOrTablet()) {
            return $this->requireDesktop();
        }



        $record = $this->workReader->getSingleRecord($id);



        if ($record->rawData->record_status == 5) {
            UI::danger('The invoice this record belongs to is locked. This means the record cannot be updated or removed.');



            return Redirect::to(\ParadoxOne\NDCounties\Redirector::getURL(action('App\Work\WorkLogsController@show',

                [$id])));
        }



        $invoice = $this->invoiceReader->getInvoiceForWorkRecord($id);

        // Get the work adapters.

        $propertyTypes = $this->typesRepository->getPropertyTypes()->get();



        $adapters = $this->adapterManager->getSortedAdapters();



        // Reconstruct an invoice date for convenience.

        $invoiceDate    = $record->rawData->work_date;

        $invoiceDueDate = $record->rawData->work_date;



        if ($invoice != null) {
            $invoiceDueDate = $invoice->due_date;
        }



        $createLike = false;



        if (Input::get('mode', 'null') == 'create-like') {
            $createLike = true;
        }





        $workRecordResetSettings = App::make('\ParadoxOne\NDCounties\Settings\WorkRecordSettingsManager');

        return View::make('app.work.update')->with('record', $record)

                   ->with('types', $propertyTypes)->with('propertyAdapters', $adapters)

                   ->with('reader', $this->workReader)

                   ->with('invoice', $invoice)

                   ->with('invoiceDate', new Carbon($invoiceDate))

                   ->with('dueDate', new Carbon($invoiceDueDate))

                   ->with('manager', $this->adapterManager)

                   ->with('createLike', $createLike)

                   ->with('workResetSettings', $workRecordResetSettings->getWorkRecordResetSettings());
    }





    /**

     * Update the specified resource in storage.

     *

     * @param  int $id

     * @return Response

     */

    public function update($id)
    {
        if (!Request::ajax()) {
            App::abort(404);
        }



        $this->updateHydrator->setRawData(Input::all());

        $this->updateHydrator->hydrateConsumables();



        $workDate = new Carbon(Input::get('record_date'));



        $includeInReports = true;



        if (Input::has('exclude_from_reports')) {
            $includeInReports = false;
        }



        $this->workUpdater->setWorkEntryID($id);

        $this->workUpdater->setWorkDate($workDate->toDateTimeString());

        $this->workUpdater->setEmployee(Input::get('record_employee', 0));

        $this->workUpdater->setHoursWorked(Input::get('regular_hours_worked'), Input::get('overtime_hours_worked'));

        $this->workUpdater->setHistoricWageData(Input::get('hourly_rate'), Input::get('overtime_rate'));

        $this->workUpdater->setHistoricFringeBenefits(Input::get('hourly_benefits'),

            Input::get('overtime_benefits'));



        $this->workUpdater->setActivity(Input::get('record_activity', 0));

        $this->workUpdater->setDepartment(Input::get('record_department', 0));

        $this->workUpdater->setProject(Input::get('record_project', 0));

        $this->workUpdater->setDistrict(Input::get('record_district', 0));

        $this->workUpdater->setProperty(Input::get('record_actual_property', 0));



        $this->workUpdater->setOrganizationProperty(Input::get('record_organization_actual_property', 0));

        $this->workUpdater->setOrganizationPropertyContext(Input::get('record_organization_property_context', 0));



        $this->workUpdater->setExcludedFromReports($includeInReports);



        $this->workUpdater->setMiscBillingQuantity(Input::get('misc_billing_quantity', 0));

        $this->workUpdater->setMiscBillingUnit(Input::get('misc_billing_unit', 9));

        $this->workUpdater->setMiscBillingRate(Input::get('misc_billing_rate', 0));

        $this->workUpdater->setMiscBillingDescription(Input::get('misc_billing_description', ''));

        $this->workRepository->setIsCreditInventoryMode(Input::has('credit_inventory'));





        // Set whether or not the employee is taxed.

        $this->workUpdater->setEmployeeIsTaxed(Input::has('employee_taxable'));



        $this->workUpdater->setPropertyContext(Input::get('record_property_context', 0));



        $this->workUpdater->setActivityDescription(Input::get('activity_description'));



        /**

         * if (Input::get('invoice_override', 0) == 1) {

        $this->workUpdater->setInvoiceOverrideTotal(Input::get('invoice_override_total', 0));

        } else {

        $this->workUpdater->setInvoiceOverrideTotal(false);

        }

         */





        $this->workUpdater->setInvoiceOverrideTotal(Input::get('invoice_override_total', 0));



        // Set the removed material records.

        if (Input::has('removed_material_records')) {
            foreach (Input::get('removed_material_records') as $record) {
                $this->workUpdater->notifyMaterialRemoval($record);
            }
        }



        // Set the removed fuel records.

        if (Input::has('removed_fuel_records')) {
            foreach (Input::get('removed_fuel_records') as $record) {
                $this->workUpdater->notifyFuelRemoval($record);
            }
        }



        // Set the removed equipment records.

        if (Input::has('removed_equipment_records')) {
            foreach (Input::get('removed_equipment_records') as $record) {
                $this->workUpdater->notifyEquipmentUnitRemoval($record);
            }
        }



        // Attach all the consumables the consumables.

        foreach ($this->updateHydrator->getConsumablesArray() as $consumable) {
            $this->workUpdater->attachConsumable($consumable);
        }



        try {
            $result = $this->workUpdater->commit();
        } catch (LockedRecordException $lockedException) {
            UI::danger('The invoice this record belongs to is locked. This means the record cannot be updated or removed.');



            return Response::json(['success' => false]);
        }



        if ($result) {

            // Here we will update some session stuff.

            $this->userSessionManager->setWorkDate(Input::get('record_date'));

            $this->userSessionManager->setDueDate(Input::get('billing_invoice_due_date'));



            UI::success('The work record was updated successfully.', 'Work Record Updated');



            return Response::json(['success' => true]);
        }



        return Response::json(['success' => false]);
    }





    /**

     * Remove the specified resource from storage.

     *

     * @param  int $id

     * @return Response

     */

    public function destroy($id)
    {
        if ($this->shouldInsertsAndUpdatesBeDisabled()) {
            return $this->flagDatabaseMaintenance();
        }



        $id        = Input::get('removeContext');

        $multiFlag = Input::get('removeMultiple', 0);



        if ($multiFlag == '1') {
            $id = explode(',', $id);
        }

        // Remove the work record.

        $removedSuccessfully = $this->workDestroyer->destroy($id, false);

        if ($removedSuccessfully) {
            if ($this->workDestroyer->someRecordsLocked) {
                UI::success('Some work records were locked, and therefore were not removed. All other records that could be removed were.');
            } else {
                UI::success('Work record removed');
            }
        } else {
            UI::danger('There was a problem removing the work record.');
        }



        if (Input::get('redirectList', 0) == 1) {
            return Redirect::action('App\Work\WorkLogsController@index');
        } else {
            return Redirect::back();
        }
    }
}
