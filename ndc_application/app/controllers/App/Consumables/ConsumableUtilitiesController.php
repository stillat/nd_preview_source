<?php namespace App\Consumables;

use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use UI;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\View;
use ParadoxOne\NDCounties\Contracts\InventoryManagement\AdjustmentRepositoryInterface;

class ConsumableUtilitiesController extends \AuthenticatedController
{
    protected $adjustmentRepository;

    public function __construct(AdjustmentRepositoryInterface $adjustmentRepository)
    {
        parent::__construct();
        $this->requiresServiceAccount();

        $this->adjustmentRepository = $adjustmentRepository;
    }

    public function getAdjustInventoryLevel($consumableID)
    {
        return View::make('layouts.dialogs.consumables.adjust_inventory')->with('consumable', $consumableID);
    }

    public function postAdjustInventoryLevel($consumableID)
    {
        if ($this->shouldInsertsAndUpdatesBeDisabled()) {
            return $this->flagDatabaseMaintenance();
        }

        $adjustmentAmount = Input::get('adjustment');

        if (!is_numeric($adjustmentAmount)) {
            UI::danger('The inventory adjustment failed: the adjustment amount must be a number.', 'Manually Adjust Inventory Level');
        }

        $adjustmentData = new \stdClass;
        $adjustmentData->date_adjusted      = Carbon::now();
        $adjustmentData->adjusted_by        = Auth::user()->id;
        $adjustmentData->consumable_id      = $consumableID;
        $adjustmentData->adjustment_context = 1;
        $adjustmentData->adjustment_value   = 0;
        $adjustmentData->adjustment_amount  = $adjustmentAmount;

        $success = $this->adjustmentRepository->addAdjustment($adjustmentData);

        if ($success) {
            UI::success('Inventory adjusted successfully.', 'Manually Adjust Inventory Level');
        } else {
            UI::danger('There was an unknown error adjusted the inventory', 'Manually Adjust Inventory Level');
        }

        return Redirect::back();
    }
}
