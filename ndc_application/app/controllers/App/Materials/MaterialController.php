<?php namespace App\Materials;

use Illuminate\Support\Facades\Cache;
use View;
use Validator;
use Redirect;
use UI;
use Input;
use Tenant;
use App;
use Session;
use Redirector;
use ParadoxOne\NDCounties\Contracts\UnitRepositoryInterface;
use ParadoxOne\NDCounties\Contracts\Resources\MaterialRepositoryInterface;

class MaterialController extends \AuthenticatedController
{
    protected $materialRepository;

    protected $unitRepository;

    public function __construct(MaterialRepositoryInterface $materialRepository, UnitRepositoryInterface $unitRepository)
    {
        parent::__construct();
        $this->requiresServiceAccount();
        
        $this->materialRepository = $materialRepository;
        $this->unitRepository = $unitRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        Redirector::logCurrent();

        $foundResults = false;

        if (Input::get('search', false) == 1) {
            $ids = Cache::get(search_cache_key('material_search'));
            if (is_array($ids) && count($ids) > 0) {
                $foundResults = true;
                $materials = $this->materialRepository->getAllWithTrashed()->whereIn('id', $ids)->paginate(user_per_page());
            } else {
                $materials = $this->materialRepository->getMaterials()->paginate(user_per_page());
                UI::info('Search contained no results.', 'Search Materials');
            }
        } else {
            $materials = $this->materialRepository->getMaterials()->paginate(user_per_page());
        }

        $units = $this->unitRepository->getUnits()->lists('name', 'id');

        return View::make('app.materials.list')->with('materials', $materials)->with('units', $units)->with('restrictionString', make_restrictions_string(array_pluck($materials, 'id')))->with('foundResults', $foundResults);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        if ($this->shouldInsertsAndUpdatesBeDisabled()) {
            return $this->flagDatabaseMaintenance();
        }

        $units = $this->unitRepository->getUnits()->lists('name', 'id');

        return View::make('app.materials.create')->with('units', $units);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        if ($this->shouldInsertsAndUpdatesBeDisabled()) {
            return $this->flagDatabaseMaintenance();
        }

        $material = $this->materialRepository->create(Input::all());

        if ($material === false) {
            UI::inputErrors();
            return Redirect::back()->withErrors($this->materialRepository->errors())->withInput();
        } else {
            UI::success('Material successfully added.', 'New Material Resource');

            if (Input::get('submit') == 'return') {
                return Redirect::to(Redirector::getURL(route('materials.index')));
            } else {
                if ($this->userSettings->auto_clear_forms) {
                    return Redirect::back();
                } else {
                    return Redirect::back()->withInput(Input::except('code', 'name', 'description'));
                }
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        $material = $this->materialRepository->getModelById($id);

        Redirector::logCurrent();

        $unit = $this->unitRepository->getModelById($material->unit_id);

        return View::make('app.materials.view')->with('material', $material)->with('unit', $unit);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        if ($this->shouldInsertsAndUpdatesBeDisabled()) {
            return $this->flagDatabaseMaintenance();
        }

        if ($id == 1) {
            return $this->flagSystemResourceUpdate();
        }

        $material = $this->materialRepository->getModelById($id);

        $units = $this->unitRepository->getUnits()->lists('name', 'id');

        return View::make('app.materials.edit')->with('units', $units)->with('consumable', $material);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {
        if ($this->shouldInsertsAndUpdatesBeDisabled()) {
            return $this->flagDatabaseMaintenance();
        }

        if ($id == 1) {
            return $this->flagSystemResourceUpdate();
        }

        $material = $this->materialRepository->update($id, Input::all());

        if ($material === false) {
            UI::inputErrors();
            return Redirect::back()->withErrors($this->materialRepository->errors())->withInput();
        } else {
            UI::success('Material successfully updated.', 'Update Material');
            return Redirect::to(Redirector::getUrl(route('materials.index')));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        if ($this->shouldInsertsAndUpdatesBeDisabled()) {
            return $this->flagDatabaseMaintenance();
        }

        $id = Input::get('removeContext');

        $multiFlag = Input::get('removeMultiple', 0);

        if ($multiFlag == '1') {
            $id = explode(',', $id);
            $id = array_strip_values($id, [0,1]);
        } else {
            if ($id == 0) {
                return $this->flagSystemResourceRemove();
            }
        }

        if (!is_array($id)) {
            $id = (array)$id;
        }
        
        $removedSuccessfully = $this->materialRepository->remove($id);

        if ($removedSuccessfully) {
            UI::success('Material resource removed.', 'Remove Material');
        } else {
            UI::danger('There was a problem removing the material..', 'Remove Material');
        }

        return Redirect::back();
    }
}
