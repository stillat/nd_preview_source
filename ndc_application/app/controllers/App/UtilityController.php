<?php namespace App;

use App;
use AuthenticatedController;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\App as LaravelApplication;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\View;
use Redirect;
use Session;
use UI;

class UtilityController extends AuthenticatedController
{
    /**
     * Allows users to switch between roles.
     *
     * @param  int $role
     * @return void
     */
    public function getSwitchRole($role)
    {
        if (!in_array($role, array(0, 1, 2))) {
            // TODO:: Throw an exception or 404 message.
            App::abort(404);
        }

        Session::put('userRole', $role);
    }

    public function getChangeActiveYear($year)
    {
        $reservedFilters = ['all', 'lm', 'tm', 'today'];

        if (!in_array($year, $reservedFilters)) {
            $validYears = Cache::get(Auth::user()->last_tenant . '_available_years');

            if (!in_array(trim($year), $validYears)) {
                UI::danger('The active year supplied is not a valid work year.', 'Cannot switch active work year.');

                return Redirect::back();
            }
        }

        $userSettings = Auth::user()->settings;

        switch ($year) {
            case 'all':
                $userSettings->active_year = -1;
                break;
            case 'lm':
                $userSettings->active_year = -2;
                break;
            case 'tm':
                $userSettings->active_year = -3;
                break;
            case 'today':
                $userSettings->active_year = -4;
                break;
            default:
                $userSettings->active_year = $year;
                break;
        }

        $userSettings->update();

        Session::put('userSettings', $userSettings->toArray());

        return Redirect::action('App\Work\WorkLogsController@index');
    }

    public function getPropertyAttachChildProperty()
    {
        return View::make('app.properties.dialogs.add-existing-property');
    }

    public function postApplyBatchTransactionStatus()
    {
        $success = true;

        if (Input::has('transaction')) {
            $transactions = Input::get('transaction');
            $transactionRepository =
                LaravelApplication::make('ParadoxOne\NDCounties\Contracts\Finance\TransactionRepositoryInterface');

            $success =
                $transactionRepository->updateTransactionStatuses($transactions, Input::get('transaction_status'));

            if ($success) {
                $transactionRepository->refreshAccountBalances();
            }
        }

        if ($success) {
            UI::success('Transactions Update', 'The transaction statuses were successfully updated.');
        } else {
            UI::danger('Transactions Update', 'The transaction statuses were not successfully updated.');
        }

        return Redirect::back();
    }

    public function deleteDestroyNestedProperty()
    {
        try {
            $propertiesRepository =
                App::make('ParadoxOne\NDCounties\Contracts\Properties\PropertiesRepositoryInterface');

            $property = $propertiesRepository->getModelByID(Input::get('removeContext'));

            if ($property->exclusively_owned == true) {
                $propertiesRepository->detachChildFromAll($property->id);
                $propertiesRepository->remove([$property->id]);

                UI::success('Exclusive property was removed', 'Child Property Removed Successfully');
            } else {
                // Need to find a way to pass in the parent property id here.
                $propertiesRepository->detachChildProperty(Input::get('parentProperty'), $property->id);
                UI::success('Nested property was removed from parent property.',
                    'Nested Property Removed Successfully');
            }

            return Redirect::back();
        } catch (\Exception $e) {
            // Do something here eventually.
        }

        UI::danger('There was an unknown problem removing the child property.', 'Child Property Was Not Removed');

        return Redirect::back();
    }

    public function getChangeDefaultEquation($equationID)
    {
        $equationRepository = App::make('ParadoxOne\NDCounties\Contracts\Reporting\ReportEquationsRepositoryInterface');
        $newEquation = $equationRepository->getEquation($equationID);

        return View::make('app.reporting.user_custom.equations.dialogs.makeDefault')->with('newEquation', $newEquation);
    }

    public function getChangeDefaultTotalBlock($totalBlock)
    {
        $blockRepository = App::make('ParadoxOne\NDCounties\Contracts\Reporting\ReportTotalBlocksRepositoryInterface');
        $newBlock = $blockRepository->getTotalBlock($totalBlock);

        return View::make('app.reporting.user_custom.totalblocks.dialogs.makeDefault')->with('newBlock', $newBlock);
    }

    public function postChangeDefaultTotalBlock()
    {
        $blockRepository = App::make('ParadoxOne\NDCounties\Contracts\Reporting\ReportTotalBlocksRepositoryInterface');
        $changed = $blockRepository->setDefaultTotalBlockForAccount(Auth::user()->last_tenant, Input::get('block_id'));

        if ($changed) {
            UI::success('The default report total block was successfully changed');
        } else {
            UI::danger('There was a problem changing the default report total block');
        }

        return Redirect::back();
    }

    public function postChangeDefaultEquation()
    {
        $equationRepository = App::make('ParadoxOne\NDCounties\Contracts\Reporting\ReportEquationsRepositoryInterface');

        $changed =
            $equationRepository->setDefaultEquationForAccount(Auth::user()->last_tenant, Input::get('equation_id'));

        if ($changed) {
            UI::success('The default equation was successfully changed.');
        } else {
            UI::danger('There was a problem creating the default equation.');
        }

        return Redirect::back();
    }

    public function getUnNestGlobalProperty($parentPropertyID, $nestedPropertyID)
    {
        $propertiesRepository = LaravelApplication::make('ParadoxOne\NDCounties\Contracts\Properties\PropertiesRepositoryInterface');
        $properties = $propertiesRepository->getPropertiesIn([$parentPropertyID, $nestedPropertyID]);

        return View::make('layouts.dialogs.properties.unnest_global')->with('parentProperty',
            array_pluck_where($properties, 'id', $parentPropertyID)[0])->with('nestedProperty',
            array_pluck_where($properties, 'id', $nestedPropertyID)[0]);
    }

    public function postUnNestGlobalProperty($parentPropertyID, $nestedPropertyID)
    {
        $propertiesRepository = LaravelApplication::make('ParadoxOne\NDCounties\Contracts\Properties\PropertiesRepositoryInterface');

        $success = $propertiesRepository->unNestGlobalProperty($parentPropertyID, $nestedPropertyID);

        if ($success) {
            UI::success('Property was successfully un-nested.', 'Un-nest Property');
        } else {
            UI::danger('There was a problem un-nesting the property.', 'Un-nest Property');
        }

        return Redirect::back();
    }

    public function getUnNestExclusiveProperty($nestedPropertyID)
    {
        $propertiesRepository = LaravelApplication::make('ParadoxOne\NDCounties\Contracts\Properties\PropertiesRepositoryInterface');
        $properties = $propertiesRepository->getPropertiesIn([$nestedPropertyID]);

        return View::make('layouts.dialogs.properties.unnest_exclusive')->with('nestedProperty', $properties[0]);
    }

    public function postUnNestExclusiveProperty($nestedPropertyID)
    {
        $propertiesRepository = LaravelApplication::make('ParadoxOne\NDCounties\Contracts\Properties\PropertiesRepositoryInterface');

        $success = $propertiesRepository->unNestExclusiveProperty($nestedPropertyID);

        if ($success) {
            UI::success('The property was successfully un-nested. It is now also available in the main properties list.',
                'Un-nest Exclusive Property');
        } else {
            UI::danger('There was a problem un-nesting the property.', 'Un-nest Exclusive Property');
        }

        return Redirect::back();
    }

    public function postConvertRoadsToGenerics()
    {
        $selectedRoads = explode(',', Input::get('convertRoadToGenerics', []));

        if (count($selectedRoads) == 0) {
            UI::info('No roads selected.', 'Bulk Action Not Taken');
            return Redirect::back();
        }

        /** @var \ParadoxOne\NDCounties\Contracts\WorkEntries\WorkEntryBatchUpdateRepositoryInterface $batchUpdater */
        $batchUpdater = LaravelApplication::make('ParadoxOne\NDCounties\Contracts\WorkEntries\WorkEntryBatchUpdateRepositoryInterface');

        $success = $batchUpdater->batchConvertRoadsToGenericProperties($selectedRoads);

        if ($success) {
            UI::success('The selected roads were converted to generic properties.', 'Record Batch Update');
        } else {
            UI::danger('There was a problem converting the selected roads to generic properties.', 'Record Batch Update');
        }

        return Redirect::back();
    }

    public function postMergeRemoveData()
    {
        /** @var \ParadoxOne\NDCounties\Contracts\WorkEntries\WorkEntryBatchUpdateRepositoryInterface $batchUpdater */
        $batchUpdater = LaravelApplication::make('ParadoxOne\NDCounties\Contracts\WorkEntries\WorkEntryBatchUpdateRepositoryInterface');

        $records = explode(',', Input::get('changeOrgContext', []));

        if (count($records) == 0) {
            UI::info('No records selected.', 'Bulk Action Not Taken');
            return Redirect::back();
        }
        $orgData = Input::get('new_org_data');
        $action = Input::get('changeOrgAction');
        $success = false;

        switch ($action) {
            case 'activity':
                $success = $batchUpdater->batchMergeAndRemoveActivities($orgData, $records);
                break;
            case 'road':
                $success = $batchUpdater->batchUpdateRoads($records, mb_substr($orgData, 5));
                break;
            case 'project':
                $success = $batchUpdater->batchMergeAndRemoveProjects($orgData, $records);
                break;
            case 'district':
                $success = $batchUpdater->batchMergeAndRemoveDistricts($orgData, $records);
                break;
            case 'department':
                $success = $batchUpdater->batchMergeAndRemoveDepartments($orgData, $records);
                break;
            case 'employee':
                $success = $batchUpdater->batchMergeAndRemoveEmployees($orgData, $records);
                break;
            case 'fuel':
                $success = $batchUpdater->batchMergeAndRemoveFuels($orgData, $records);
                break;
            case 'material':
                $success = $batchUpdater->batchMergeAndRemoveMaterials($orgData, $records);
                break;
            case 'property':
                $propertyTypeToMerge = Input::get('changeOrgExtra', null);
                if (!in_array($propertyTypeToMerge, [0, 1, 2])) {
                    $success = false;
                    break;
                }
                switch ($propertyTypeToMerge) {
                    case 0:
                        $success = $batchUpdater->batchMergeAndRemoveGenericProperties($orgData, $records);
                        break;
                    case 1:
                        $success = $batchUpdater->batchMergeAndRemoveRoads($orgData, $records);
                        break;
                    case 2:
                        $success = $batchUpdater->batchMergeAndRemoveEquipmentUnits($orgData, $records);
                        break;
                }
                break;
        }

        if ($success) {
            UI::success('Records merged and removed.', 'Record Batch Update');
        } else {
            UI::danger('There was a problem merging and removing the records.', 'Record Batch Update');
        }

        return Redirect::back();
    }

    public function postChangeRecordOrgData()
    {
        $batchUpdater = LaravelApplication::make('ParadoxOne\NDCounties\Contracts\WorkEntries\WorkEntryBatchUpdateRepositoryInterface');

        $records = explode(',', Input::get('changeOrgContext', []));

        if (count($records) == 0) {
            UI::info('No records selected.', 'Bulk Action Not Taken');
            return Redirect::back();
        }
        $orgData = Input::get('new_org_data');
        $action = Input::get('changeOrgAction');
        $success = false;

        switch ($action) {
            case 'activity':
                $success = $batchUpdater->batchUpdateActivities($records, $orgData);
                break;
            case 'road':
                $success = $batchUpdater->batchUpdateRoads($records, mb_substr($orgData, 5));
                break;
            case 'project':
                $success = $batchUpdater->batchUpdateProject($records, $orgData);
                break;
            case 'district':
                $success = $batchUpdater->batchUpdateDistrict($records, $orgData);
                break;
            case 'department':
                $success = $batchUpdater->batchUpdateDepartment($records, $orgData);
                break;
        }

        if ($success) {
            UI::success('Work records updated.', 'Update Work Record Batch Update');
        } else {
            UI::danger('There was a problem updating the work records.', 'Update Work Record Batch Update');
        }

        return Redirect::back();
    }

    public function postChangeRecordDates()
    {
        $batchUpdater = LaravelApplication::make('ParadoxOne\NDCounties\Contracts\WorkEntries\WorkEntryBatchUpdateRepositoryInterface');

        $records = explode(',', Input::get('changeDatesContext', []));

        if (count($records) == 0) {
            UI::info('No records selected.', 'Bulk Action Not Taken');
            return Redirect::back();
        }
        $date = Input::get('record_date');
        $success = $batchUpdater->batchUpdateDates($records, $date);

        if ($success) {
            UI::success('Work record dates updated.', 'Update Work Record Dates');
        } else {
            UI::danger('There was a problem updating the work record dates.', 'Update Work Record Dates');
        }

        return Redirect::back();

    }

    public function postChangeRecordVisibility()
    {
        $visibilityManager = LaravelApplication::make('ParadoxOne\NDCounties\Contracts\WorkEntries\WorkEntryVisibilityRepositoryInterface');

        $records = explode(',', Input::get('visibilityContext', []));

        if (count($records) == 0) {
            UI::info('No records selected.', 'Bulk Action Not Taken');
            return Redirect::back();
        }

        $success = false;
        $action = Input::get('visibilityAction');

        if (!in_array($action, [0, 1])) {
            UI::danger('Supplied action is invalid.', 'Record Visibility Not Updated');
            return Redirect::back();
        }

        if ($action == 1) {
            $success = $visibilityManager->showRecordsToReports($records);
        } else {
            $success = $visibilityManager->hideRecordsFromReports($records);
        }

        if ($success) {
            UI::success('Work record report visibility updated.', 'Update Work Record Visibility');
        } else {
            UI::danger('There was a problem updating work record report visibility.', 'Update Work Record Visibility');
        }

        return Redirect::back();
    }

    public function getClearEmptyFuelMaterialRecords()
    {
        $batchUpdater = LaravelApplication::make('ParadoxOne\NDCounties\Contracts\WorkEntries\WorkEntryBatchUpdateRepositoryInterface');

        $success = $batchUpdater->batchRemoveEmptyConsumableRecords();

        if ($success) {
            UI::success('Empty fuel and material records were removed.', 'Remove Empty Records');
        } else {
            UI::danger('Unable to remove empty fuel and material records' . 'Remove Empty Records Failed');
        }

        return Redirect::back();
    }

}
