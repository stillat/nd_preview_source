<?php namespace App\Finances\Transactions;

use UI;
use AuthenticatedController;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use ParadoxOne\NDCounties\Contracts\Finance\AccountRepositoryInterface;
use ParadoxOne\NDCounties\Contracts\Finance\TransactionRepositoryInterface;

class TransactionController extends AuthenticatedController
{
    protected $transactionRepository;

    protected $accountRepository;

    public function __construct(TransactionRepositoryInterface $transactionRepository,
                                AccountRepositoryInterface $accountRepository)
    {
        parent::__construct();
        $this->requiresServiceAccount();
        
        $this->transactionRepository = $transactionRepository;
        $this->accountRepository = $accountRepository;
    }

    public function getIndex()
    {
    }

    public function getTransfer($startAccount)
    {
        if ($this->shouldInsertsAndUpdatesBeDisabled()) {
            return $this->flagDatabaseMaintenance();
        }

        $accounts = $this->accountRepository->getAccounts()->get();
        
        $accountList = array();

        foreach ($accounts as $account) {
            $accountList[$account->id] = $account->prefix.$account->code.' - '.$account->name;
        }

        $activeAccount = array_pull($accountList, $startAccount);

        unset($accounts);

        return View::make('finances.transactions.transfer')
                     ->with('accounts', $accountList)
                     ->with('startingAccount', array($startAccount, $activeAccount));
    }

    public function getTransferTo($destinationAccount)
    {
        if ($this->shouldInsertsAndUpdatesBeDisabled()) {
            return $this->flagDatabaseMaintenance();
        }

        $accounts = $this->accountRepository->getAccounts()->get();

        $accountList = array();

        foreach ($accounts as $account) {
            $accountList[$account->id] = $account->prefix.$account->code.' - '.$account->name;
        }

        $activeAccount = array_pull($accountList, $destinationAccount);

        unset($accounts);

        return View::make('finances.transactions.transfer_from')
                   ->with('accounts', $accountList)
                   ->with('destinationAccount', array($destinationAccount, $activeAccount));
    }

    public function postTransfer()
    {
        if ($this->shouldInsertsAndUpdatesBeDisabled()) {
            return $this->flagDatabaseMaintenance();
        }

        try {
            // We will pass 0 as the work record id here.
            $this->transactionRepository->transferAmountToAccount(
                Input::get('starting_account'),
                Input::get('destination_account'),
                Input::get('amount'),
                \Auth::user()->id,
                Input::get('message'),
                Input::get('transaction_status', 1), 0);

            UI::success('Account funds transaction completed.', 'Transfer Success');
            return Redirect::back();
        } catch (\Exception $e) {
        }

        UI::danger('There was an issue transferring the account funds.', 'Transfer Failure');
        return Redirect::back();
    }
}
