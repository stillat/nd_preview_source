<?php namespace App\Finances;

use Session;
use Redirect;
use UI;
use Input;
use Redirector;
use AuthenticatedController;
use Illuminate\Support\Facades\View;
use ParadoxOne\NDCounties\Contracts\Finance\SupplierRepositoryInterface;

class SuppliersController extends AuthenticatedController
{
    protected $supplierRepository;

    public function __construct(SupplierRepositoryInterface $supplierRepository)
    {
        parent::__construct();
        $this->requiresServiceAccount();
        
        $this->supplierRepository = $supplierRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        Redirector::logCurrent();

        $suppliers = $this->supplierRepository->getSuppliers()->paginate(user_per_page());

        return View::make('finances.suppliers.list')->with('suppliers', $suppliers);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return View::make('finances.suppliers.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        $supplier = $this->supplierRepository->create(Input::all());

        if ($supplier === false) {
            UI::inputErrors();
            return Redirect::back()->withErrors($this->supplierRepository->errors())->withInput();
        } else {
            UI::success('Supplier successfully added.', 'New Supplier');

            if (Input::get('submit') == 'return') {
                return Redirect::to(Redirector::getURL(route('finances.suppliers.index')));
            } else {
                return Redirect::back();
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        Redirector::logCurrent();

        $supplier = $this->supplierRepository->getModelById($id);

        return View::make('finances.suppliers.view')->with('supplier', $supplier);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $supplier = $this->supplierRepository->getModelById($id);

        return View::make('finances.suppliers.edit')->with('supplier', $supplier);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {
        $supplier = $this->supplierRepository->update($id, Input::all());

        if ($supplier === false) {
            UI::inputErrors();
            return Redirect::back()->withErrors($this->supplierRepository->errors())->withInput();
        } else {
            UI::success('Supplier successfully updated');
            return Redirect::to(Redirector::getURL(route('finances.suppliers.index')));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $this->supplierRepository->remove(array(Input::get('removeContext')));
        UI::success('Supplier removed');
        return Redirect::back();
    }
}
