<?php namespace App\Finances\Invoices;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\View;
use ParadoxOne\NDCounties\Contracts\Finance\AccountRepositoryInterface;
use ParadoxOne\NDCounties\Contracts\Finance\Invoices\InvoiceReaderRepositoryInterface;
use ParadoxOne\NDCounties\Contracts\Finance\Invoices\InvoiceUpdaterRepositoryInterface;
use ParadoxOne\NDCounties\Contracts\WorkEntries\WorkEntryReaderInterface;
use ParadoxOne\NDCounties\Database\Adapters\AdapterManager;
use ParadoxOne\NDCounties\Database\DuplicateValueException;
use ParadoxOne\NDCounties\InvalidFormatException;
use ParadoxOne\NDCounties\Redirector;
use ParadoxOne\NDCounties\UI\UIBuilder;
use UI;

class InvoiceController extends \AuthenticatedController
{
    use \App\Reporting\Builders\ReportingDatesBuilderTrait;
    use \App\Reporting\Builders\BreakdownHelpersBuilderTrait;

    protected $invoiceReaderRepository;

    protected $invoiceUpdaterRepository;

    protected $workReader;

    protected $adapterManager;

    protected $accountRepository;


    public function __construct(
        InvoiceReaderRepositoryInterface $invoiceReaderRepository,
        WorkEntryReaderInterface $workReader,
        AdapterManager $adapterManager,
        InvoiceUpdaterRepositoryInterface $invoiceUpdaterRepository,
        AccountRepositoryInterface $accountRepositoryInterface
    ) {
        parent::__construct();
        $this->requiresServiceAccount();
        $this->invoiceReaderRepository  = $invoiceReaderRepository;
        $this->workReader               = $workReader;
        $this->adapterManager           = $adapterManager;
        $this->invoiceUpdaterRepository = $invoiceUpdaterRepository;
        $this->accountRepository        = $accountRepositoryInterface;

        // Disable forced indexes, basically.
        $this->workReader->reader()->forcedIndex = false;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        Redirector::logCurrent();

        $currentViewMode = Input::get('view', 0);

        if (!in_array($currentViewMode, [0, 1, 2])) {
            $currentViewMode = 0;
        }

        $invoices = $this->invoiceReaderRepository->getInvoices($currentViewMode)->paginate(user_per_page());

        $viewModes = [
            0 => 'Open Invoices',
            1 => 'Closed Invoices',
            2 => 'All Invoices'
        ];

        return View::make('finances.invoices.list')->with('invoices', $invoices)->with('viewModes', $viewModes)
                   ->with('currentViewMode', $currentViewMode)
                   ->with('restrictionString', make_restrictions_string(array_pluck($invoices, 'id')));
    }


    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        $invoice = $this->invoiceReaderRepository->getInvoiceByID($id);

        if ($invoice === null) {
            throw new ModelNotFoundException;
        }

        Redirector::logCurrent();

        $workRecords = $this->workReader->reader()->withDefaults()->forInvoice($id)->getRecords();

        $workDates = [];

        foreach ($workRecords as $record) {
            $workDates[] = $record->getWorkDate();
        }

        $invoicePeriod = date_period($workDates);

        $neighbors = $this->invoiceReaderRepository->getNeighbors($id);

        return View::make('finances.invoices.view')->with('invoice', $invoice)->with('workRecords', $workRecords)
                   ->with('reader', $this->workReader)
                   ->with('manager', $this->adapterManager)
                   ->with('invoicePeriod', $invoicePeriod)
                   ->with('neighbors', $neighbors);
    }

    public function getUpdateAccounts($invoiceID)
    {
        $invoice = $this->invoiceReaderRepository->getInvoiceByID($invoiceID);

        if ($invoice === null) {
            throw new ModelNotFoundException;
        }

        $accounts = $this->accountRepository->getAccounts()->get();

        $accountList = array();

        foreach ($accounts as $account) {
            $accountList[$account->id] = $account->prefix . $account->code . ' - ' . $account->name;
        }

        unset($accounts);

        return View::make('layouts.dialogs.invoices.change_accounts')->with('invoice', $invoice)
                   ->with('accounts', $accountList);
    }

    public function postUpdateAccounts($invoiceID)
    {
        if ($this->shouldInsertsAndUpdatesBeDisabled()) {
            return $this->flagDatabaseMaintenance();
        }

        $invoice = $this->invoiceReaderRepository->getInvoiceByID($invoiceID);

        if ($invoice === null) {
            throw new ModelNotFoundException;
        }

        $newBillToAccount = intval(Input::get('bill_to_account'));
        $newPayToAccount  = intval(Input::get('pay_to_account'));

        if ($invoice->account_number == $newBillToAccount && $invoice->paid_to_account == $newPayToAccount) {
            UI::info('Invoice accounts were not changed. New accounts were the same.', 'Change Invoice Accounts');

            return Redirect::back();
        }

        $success =
            $this->invoiceUpdaterRepository->changeInvoiceAccounts($invoiceID, $newBillToAccount, $newPayToAccount);

        if ($success) {
            UI::success('Invoice accounts were successfully changed.', 'Change Invoice Accounts');
        } else {
            UI::danger('There was an unexpected problem changing invoice accounts.', 'Change Invoice Accounts');
        }

        return Redirect::back();
    }

    public function getUpdateInvoiceNumber($invoiceID)
    {
        $invoice = $this->invoiceReaderRepository->getInvoiceByID($invoiceID);

        if ($invoice === null) {
            throw new ModelNotFoundException;
        }

        return View::make('layouts.dialogs.invoices.change_invoice_number')->with('invoice', $invoice);
    }

    public function postUpdateInvoiceNumber($invoiceID)
    {
        if ($this->shouldInsertsAndUpdatesBeDisabled()) {
            return $this->flagDatabaseMaintenance();
        }

        try {
            $updateInvoiceNumberSuccess =
                $this->invoiceUpdaterRepository->changeInvoiceNumber($invoiceID, Input::get('invoice_number'));

            if ($updateInvoiceNumberSuccess) {
                UI::success('The invoice number was updated successfully.', 'Change Invoice Number');
            } else {
                UI::danger('There was a problem updating the invoice number', 'Change Invoice Number');
            }
        } catch (DuplicateValueException $duplicateInvoiceNumberException) {
            UI::danger('That invoice number already is in use and cannot be used on another invoice.',
                'Change Invoice Number');
        } catch (InvalidFormatException $invalidFormatException) {
            UI::danger('There was a problem with the formatting of the new invoice number. The invoice number was not changed.',
                'Change Invoice Number');
        } catch (\Exception $e) {
            UI::danger('There was a problem updating the invoice number', 'Change Invoice Number');
        }

        return Redirect::back();
    }

    public function getLockInvoice($invoiceID)
    {
        return View::make('layouts.dialogs.invoices.lock')->with('invoiceID', $invoiceID);
    }

    public function getUnlockInvoice($invoiceID)
    {
        return View::make('layouts.dialogs.invoices.unlock')->with('invoiceID', $invoiceID);
    }

    public function postLockInvoice($invoiceID)
    {
        if ($this->shouldInsertsAndUpdatesBeDisabled()) {
            return $this->flagDatabaseMaintenance();
        }

        $success = $this->invoiceUpdaterRepository->lockInvoice($invoiceID);

        if ($success) {
            UI::success('The invoice was successfully locked.');
        } else {
            UI::danger('There was a problem locking the invoice.');
        }

        return Redirect::back();
    }

    public function postUnlockInvoice($invoiceID)
    {
        if ($this->shouldInsertsAndUpdatesBeDisabled()) {
            return $this->flagDatabaseMaintenance();
        }

        $success = $this->invoiceUpdaterRepository->unlockInvoice($invoiceID);

        if ($success) {
            UI::success('The invoice was successfully unlocked.');
        } else {
            UI::danger('There was a problem unlocking the invoice.');
        }

        return Redirect::back();
    }

    public function getSearch()
    {
        $invoices = $this->invoiceReaderRepository->getInvoices()->lists('invoice_number', 'id');
        $accounts = $this->accountRepository->getAccounts()->lists('prefixed_name', 'id');
        list($oldStartDate, $oldEndDate) = $this->getReportingDates();

        return View::make('finances.invoices.search')->with('invoices', $invoices)->with('accounts', $accounts)
            ->with('startDate', $oldStartDate)
            ->with('endDate', $oldEndDate);
    }

    public function postSearch()
    {
        $searchMode = (bool)Input::get('report_new_window', 0);

        $startDate = Input::get('report_start_date', null);
        $endDate = Input::get('report_end_date', null);

        if (!Input::has('honor_date_range')) {
            $startDate = null;
            $endDate = null;
        }

        $foundInvoices = $this->invoiceReaderRepository->filterInvoices(Input::get('invoice_filters', []),
            Input::get('pay_to_filters', []),
            Input::get('bill_to_filters'), [],
            $startDate,
            $endDate)->get();

        if (count($foundInvoices) == 0) {
            if (!$searchMode) {
                UIBuilder::info('There were no invoices that match the search criteria.', 'No Search Results');
                return Redirect::back();
            } else {
                return View::make('finances.invoices.no_results');
            }
        }

        $invoiceIDs = array_pluck_unique($foundInvoices, 'id');

        $workRecords = $this->workReader->reader()->withDefaults()->forInvoice($invoiceIDs); //->getRecords();
        if (is_array($workRecords) && count($workRecords) == 0) {
            if (!$searchMode) {
                UIBuilder::info('There were no invoices that match the search criteria.', 'No Search Results');
                return Redirect::back();
            } else {
                return View::make('finances.invoices.no_results');
            }
        } else {
            $workRecords = $workRecords->getRecords();
        }

        $invoiceWorkRecords = [];

        foreach ($workRecords as $record) {
            if (!isset($invoiceWorkRecords[$record->rawData->invoice_id])) {
                $invoiceWorkRecords[$record->rawData->invoice_id] = [];
            }
            $invoiceWorkRecords[$record->rawData->invoice_id][] = $record;
        }

        unset($workRecords);
        unset($invoiceIDs);

        $showActivities = Input::has('show_activities');
        $showEmployees = Input::has('show_employees');
        $showMaterials = Input::has('show_materials');
        $showEquips = Input::has('show_equips');
        $showProp = Input::has('show_property');

        $manager = App::make('ParadoxOne\NDCounties\Settings\ReportSettingsManager');
        $settings = $manager->getReportSettings();
        $breakdownClass = $this->getBreakdownClass($settings);

        unset($manager);

        return View::make('finances.invoices.search_results')->with('workRecords', $invoiceWorkRecords)
            ->with('invoices', $foundInvoices)
            ->with('showActivities', $showActivities)
            ->with('showEmployees', $showEmployees)
            ->with('showMaterials', $showMaterials)
            ->with('showProp', $showProp)
            ->with('showEquips', $showEquips)
            ->with('breakdownClass', $breakdownClass)
            ->with('settings', $settings);
    }
}
