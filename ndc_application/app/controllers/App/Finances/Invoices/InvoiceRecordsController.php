<?php namespace App\Finances\Invoices;

use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Paginator;
use ParadoxOne\NDCounties\Contracts\Finance\Invoices\InvoiceReaderRepositoryInterface;
use ParadoxOne\NDCounties\Contracts\WorkEntries\WorkEntryReaderInterface;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use ParadoxOne\NDCounties\Redirector;
use Illuminate\Support\Facades\View;

class InvoiceRecordsController extends \AuthenticatedController
{
    protected $workReader;

    protected $invoiceReader;

    public function __construct(WorkEntryReaderInterface $reader, InvoiceReaderRepositoryInterface $invoiceReader)
    {
        parent::__construct();
        $this->requiresServiceAccount();
        $this->workReader = $reader;
        $this->invoiceReader = $invoiceReader;
    }

    public function index($invoiceID)
    {
        $invoice = $this->invoiceReader->getInvoiceByID($invoiceID);

        if ($invoice === null) {
            throw new ModelNotFoundException;
        }

        Redirector::logCurrent();

        $workRecords = $this->workReader->reader()->withDefaults()->forInvoice($invoiceID)->getRecords();
        $workDates = [];

        foreach ($workRecords as $record) {
            $workDates[] = $record->getWorkDate();
        }

        $invoicePeriod = date_period($workDates);
        $neighbors = $this->invoiceReader->getNeighbors($invoiceID);

        $metaInformation                 = new \stdClass;
        $metaInformation->equipmentCount = 0;
        $metaInformation->materialCount  = $this->workReader->reader()->getHighestMaterialCount();
        $metaInformation->fuelCount      = $this->workReader->reader()->getHighestFuelCount();

        $paginator = Paginator::make($workRecords, $this->workReader->count(), user_per_page());
        $formatter              = App::make('formatter');
        $recordOverrideSettings = $formatter->getSettings('useOnRecords', 'useOnRecords');
        $header = $this->workReader->reader()->getReportPropertyHeader();
        return View::make('finances.invoices.records')
            ->with('paginator', $paginator)
            ->with('workRecords', $workRecords)->with('invoice', $invoice)
            ->with('formatter', $formatter)
            ->with('recordOverrideSettings', $recordOverrideSettings)
            ->with('neighbors', $neighbors)->with('workMeta', $metaInformation)
            ->with('reportHeader', $header);
    }
}
