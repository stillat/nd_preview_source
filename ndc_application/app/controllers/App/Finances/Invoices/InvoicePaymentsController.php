<?php namespace App\Finances\Invoices;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\View;
use ParadoxOne\NDCounties\Contracts\Finance\Invoices\InvoiceReaderRepositoryInterface;
use ParadoxOne\NDCounties\Contracts\Finance\Invoices\InvoiceUpdaterRepositoryInterface;
use ParadoxOne\NDCounties\Contracts\Finance\TransactionRepositoryInterface;
use ParadoxOne\NDCounties\Contracts\Finance\TransactionStatusRepositoryInterface;
use ParadoxOne\NDCounties\Contracts\WorkEntries\WorkEntryReaderInterface;
use ParadoxOne\NDCounties\Redirector;
use UI;

class InvoicePaymentsController extends \AuthenticatedController
{
    protected $invoiceReaderRepository;

    protected $workReader;

    protected $transactionRepository;

    protected $invoiceUpdaterRepository;

    protected $transactionStatusRepository;

    public function __construct(InvoiceReaderRepositoryInterface $invoiceReader, WorkEntryReaderInterface $workReader,
                                TransactionRepositoryInterface $transactionRepository, TransactionStatusRepositoryInterface $transactionStatusRepository,
                                InvoiceUpdaterRepositoryInterface $invoiceUpdaterRepository)
    {
        parent::__construct();
        $this->requiresServiceAccount();

        $this->invoiceReaderRepository     = $invoiceReader;
        $this->workReader                  = $workReader;
        $this->transactionRepository       = $transactionRepository;
        $this->transactionStatusRepository = $transactionStatusRepository;
        $this->invoiceUpdaterRepository    = $invoiceUpdaterRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index($invoiceID)
    {
        $invoice = $this->invoiceReaderRepository->getInvoiceByID($invoiceID);

        if ($invoice === null) {
            throw new ModelNotFoundException;
        }

        Redirector::logCurrent();

        $workRecords = $this->workReader->reader()->withDefaults()->forInvoice($invoiceID)->getRecords();

        $workDates = [];

        foreach ($workRecords as $record) {
            $workDates[] = $record->getWorkDate();
        }

        $invoicePeriod = date_period($workDates);

        $neighbors = $this->invoiceReaderRepository->getNeighbors($invoiceID);

        $transactions = $this->transactionRepository->getInvoiceTransactions($invoiceID, $invoice->paid_to_account)
                                                    ->paginate(user_per_page());

        return View::make('finances.invoices.payments.list')->with('invoice', $invoice)
                   ->with('invoicePeriod', $invoicePeriod)
                   ->with('neighbors', $neighbors)
                   ->with('transactions', $transactions)
                   ->with('transactionStatuses', $this->transactionStatusRepository->getStatuses())
                   ->with('transactionRestrictionString', make_restrictions_string(array_pluck($transactions, 'id')));
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create($invoiceID)
    {
        if ($this->shouldInsertsAndUpdatesBeDisabled()) {
            return $this->flagDatabaseMaintenance();
        }

        $invoice = $this->invoiceReaderRepository->getInvoiceByID($invoiceID);

        if ($invoice === null) {
            throw new ModelNotFoundException;
        }

        $workRecords = $this->workReader->reader()->withDefaults()->forInvoice($invoiceID)->getRecords();

        $workDates = [];

        foreach ($workRecords as $record) {
            $workDates[] = $record->getWorkDate();
        }

        $invoicePeriod = date_period($workDates);

        return View::make('finances.invoices.payments.create')->with('invoice', $invoice)
                   ->with('invoicePeriod', $invoicePeriod);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store($invoiceID)
    {
        if ($this->shouldInsertsAndUpdatesBeDisabled()) {
            return $this->flagDatabaseMaintenance();
        }

        $invoice = $this->invoiceReaderRepository->getInvoiceByID($invoiceID);

        try {
            $this->transactionRepository->transferAmountToAccount(
                $invoice->account_number,
                $invoice->paid_to_account,
                Input::get('amount'),
                Auth::user()->id,
                Input::get('message'),
                Input::get('transaction_status', 1), 0,
                1,
                $invoice->id
            );

            $this->invoiceUpdaterRepository->syncInvoiceBalance($invoiceID);

            UI::success('Payment processed successfully.');

            return Redirect::back();
        } catch (\Exception $e) {
            lk($e);
        }

        UI::danger('There was an issue processing the payment.');

        return Redirect::back();
    }
}
