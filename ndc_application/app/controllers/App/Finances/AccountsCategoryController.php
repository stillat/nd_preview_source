<?php namespace App\Finances;

use Session;
use Redirect;
use UI;
use Input;
use Redirector;
use AuthenticatedController;
use Illuminate\Support\Facades\View;
use ParadoxOne\NDCounties\Contracts\Finance\AccountCategoriesRepositoryInterface;

class AccountsCategoryController extends AuthenticatedController
{
    protected $categoryRepository;

    public function __construct(AccountCategoriesRepositoryInterface $categoryRepo)
    {
        parent::__construct();
        $this->requiresServiceAccount();
        
        $this->categoryRepository = $categoryRepo;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        Redirector::logCurrent();

        $categories = $this->categoryRepository->getCategories()->paginate(user_per_page());

        return View::make('finances.categories.list')->with('categories', $categories);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        if ($this->shouldInsertsAndUpdatesBeDisabled()) {
            return $this->flagDatabaseMaintenance();
        }

        return View::make('finances.categories.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        if ($this->shouldInsertsAndUpdatesBeDisabled()) {
            return $this->flagDatabaseMaintenance();
        }

        $category = $this->categoryRepository->create(Input::all());

        if ($category === false) {
            UI::inputErrors();
            return Redirect::back()->withErrors($this->categoryRepository->errors())->withInput();
        } else {
            UI::success('Category successfully added.', 'New Account Category');

            if (Input::get('submit') == 'return') {
                return Redirect::to(Redirector::getURL(route('finances.accounts.index')));
            } else {
                return Redirect::back();
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        $category = $this->categoryRepository->getModelById($id);
        Redirector::logCurrent();

        return View::make('finances.categories.view')->with('category', $category);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        if ($this->shouldInsertsAndUpdatesBeDisabled()) {
            return $this->flagDatabaseMaintenance();
        }

        if ($id == 0) {
            return $this->flagSystemResourceUpdate();
        }

        $category = $this->categoryRepository->getModelById($id);

        return View::make('finances.categories.edit')->with('category', $category);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {
        if ($this->shouldInsertsAndUpdatesBeDisabled()) {
            return $this->flagDatabaseMaintenance();
        }

        if ($id == 0) {
            return $this->flagSystemResourceUpdate();
        }

        $category = $this->categoryRepository->update($id, Input::all());

        if ($category === false) {
            UI::inputErrors();
            return Redirect::back()->withErrors($this->categoryRepository->errors())->withInput();
        } else {
            UI::success('Account category successfully updated');
            return Redirect::to(Redirector::getURL(route('finances.categories.index')));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        if ($this->shouldInsertsAndUpdatesBeDisabled()) {
            return $this->flagDatabaseMaintenance();
        }

        $id = Input::get('removeContext');

        if ($id == 0) {
            return $this->flagSystemResourceRemove();
        }

        $removeSuccessfully = $this->categoryRepository->remove(array($id));

        if ($removeSuccessfully) {
            UI::success('Finance account category removed successfully');
        } else {
            UI::danger('There was a problem remove the finance account category');
        }

        return Redirect::back();
    }
}
