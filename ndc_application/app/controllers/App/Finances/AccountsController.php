<?php namespace App\Finances;

use AuthenticatedController;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\View;
use Input;
use ParadoxOne\NDCounties\Contracts\Finance\AccountCategoriesRepositoryInterface;
use ParadoxOne\NDCounties\Contracts\Finance\AccountRepositoryInterface;
use ParadoxOne\NDCounties\Contracts\Finance\TransactionRepositoryInterface;
use ParadoxOne\NDCounties\Contracts\Finance\TransactionStatusRepositoryInterface;
use Redirect;
use Redirector;
use UI;

class AccountsController extends AuthenticatedController
{
    protected $accountRepository;

    protected $accountCategoryRepository;

    protected $transactionRepository;

    protected $transactionStatusRepository;

    public function __construct(AccountRepositoryInterface $accountRepository, AccountCategoriesRepositoryInterface $accountCategoryRepository,
                                TransactionRepositoryInterface $transactionRepository, TransactionStatusRepositoryInterface $transactionStatusRepository)
    {
        parent::__construct();
        $this->requiresServiceAccount();

        $this->accountRepository           = $accountRepository;
        $this->accountCategoryRepository   = $accountCategoryRepository;
        $this->transactionRepository       = $transactionRepository;
        $this->transactionStatusRepository = $transactionStatusRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        Redirector::logCurrent();

        $sort = Input::get('sort', null);

        $foundResults = false;

        if (Input::get('search', false) == 1) {
            $ids = Cache::get(search_cache_key('fs_account_search'));
            if (is_array($ids) && count($ids) > 0) {
                $foundResults = true;
                $accounts = $this->accountRepository->getAccountsWithTrashed()->whereIn('fs_accounts.id', $ids)->paginate(user_per_page());
            } else {
                if ($sort === null or $sort == -1) {
                    $accounts = $this->accountRepository->getAccounts()->paginate(user_per_page());
                } else {
                    $accounts = $this->accountRepository->getAccounts()->where('category_id', '=', intval($sort))
                        ->paginate(user_per_page());
                }
                UI::info('Search contained no results', 'Search Finance Accounts');
            }
        } else {
            if ($sort === null or $sort == -1) {
                $accounts = $this->accountRepository->getAccounts()->paginate(user_per_page());
            } else {
                $accounts = $this->accountRepository->getAccounts()->where('category_id', '=', intval($sort))
                    ->paginate(user_per_page());
            }
        }

        $categories = $this->accountCategoryRepository->getCategories()->lists('name', 'id');
        $categories = [-1 => 'All Account Categories'] + $categories;


        return View::make('finances.accounts.list')
                   ->with('accounts', $accounts)
                   ->with('categories', $categories)
                   ->with('currentCategory', $sort)
                   ->with('restrictionString', make_restrictions_string(array_pluck($accounts, 'id')))
                   ->with('foundResults', $foundResults);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        if ($this->shouldInsertsAndUpdatesBeDisabled()) {
            return $this->flagDatabaseMaintenance();
        }

        $categories = $this->accountCategoryRepository->getCategories()->lists('name', 'id');

        return View::make('finances.accounts.create')->with('categories', $categories);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        if ($this->shouldInsertsAndUpdatesBeDisabled()) {
            return $this->flagDatabaseMaintenance();
        }

        $account = $this->accountRepository->create(Input::all());

        if ($account === false) {
            UI::inputErrors();

            return Redirect::back()->withErrors($this->accountRepository->errors())->withInput();
        } else {
            UI::success('Account successfully added.', 'New Account');

            if (Input::get('submit') == 'return') {
                return Redirect::to(Redirector::getURL(route('finances.accounts.index')));
            } else {
                return Redirect::back();
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        $account = $this->accountRepository->getModelById($id);
        Redirector::logCurrent();

        $transactions =
            $this->transactionRepository->getAccountTransactions($id)->groupBy('batch')->paginate(user_per_page());

        return View::make('finances.accounts.view')->with('account', $account)
                   ->with('transactions', $transactions)
                   ->with('transactionStatuses', $this->transactionStatusRepository->getStatuses())
            ->with('transactionRestrictionString', make_restrictions_string(array_pluck($transactions, 'id')));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id)
    {
        if ($this->shouldInsertsAndUpdatesBeDisabled()) {
            return $this->flagDatabaseMaintenance();
        }

        if ($id == 0) {
            return $this->flagSystemResourceUpdate();
        }

        $account = $this->accountRepository->getModelByID($id);

        $categories = $this->accountCategoryRepository->getCategories()->lists('name', 'id');

        return View::make('finances.accounts.edit')->with('account', $account)->with('categories', $categories);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return Response
     */
    public function update($id)
    {
        if ($this->shouldInsertsAndUpdatesBeDisabled()) {
            return $this->flagDatabaseMaintenance();
        }

        if ($id == 0) {
            return $this->flagSystemResourceUpdate();
        }

        $account = $this->accountRepository->update($id, Input::all());

        if ($account === false) {
            UI::inputErrors();

            return Redirect::back()->withErrors($this->accountRepository->errors())->withInput();
        } else {
            UI::success('Account successfully updated');

            return Redirect::to(Redirector::getURL(route('finances.accounts.index')));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        if ($this->shouldInsertsAndUpdatesBeDisabled()) {
            return $this->flagDatabaseMaintenance();
        }

        $id = Input::get('removeContext');

        if ($id == 0) {
            return $this->flagSystemResourceRemove();
        }

        $this->accountRepository->remove(array($id));
        UI::success('Account removed');

        return Redirect::back();
    }
}
