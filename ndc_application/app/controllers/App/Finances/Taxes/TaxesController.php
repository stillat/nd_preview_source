<?php namespace App\Finances\Taxes;

use Illuminate\Support\Facades\Cache;
use UI;
use Redirector;
use AuthenticatedController;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use ParadoxOne\NDCounties\Contracts\Finance\Taxes\TaxRateRepositoryInterface;

class TaxesController extends AuthenticatedController
{
    protected $taxRateRepository = null;

    public function __construct(TaxRateRepositoryInterface $taxRateRepository)
    {
        parent::__construct();
        $this->requiresServiceAccount();

        $this->taxRateRepository = $taxRateRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        Redirector::logCurrent();

        $foundResults = false;

        if (Input::get('search', false) == 1) {
            $ids = Cache::get(search_cache_key('fs_tax_rates_search'));
            if (is_array($ids) && count($ids) > 0) {
                $foundResults = true;
                $taxRates = $this->taxRateRepository->getTaxRatesWithTrashed()->whereIn('id', $ids)->paginate(user_per_page());
            } else {
                $taxRates = $this->taxRateRepository->getTaxRates()->paginate(user_per_page());
                UI::info('Search contained no results.', 'Search Tax Rates');
            }
        } else {
            $taxRates = $this->taxRateRepository->getTaxRates()->paginate(user_per_page());
        }

        return View::make('finances.taxes.list')->with('taxRates', $taxRates)->with('restrictionString', make_restrictions_string(array_pluck($taxRates, 'id')))->with('foundResults', $foundResults);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        if ($this->shouldInsertsAndUpdatesBeDisabled()) {
            return $this->flagDatabaseMaintenance();
        }

        return View::make('finances.taxes.create');
    }


    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        if ($this->shouldInsertsAndUpdatesBeDisabled()) {
            return $this->flagDatabaseMaintenance();
        }

        $taxRate = $this->taxRateRepository->create(Input::all());

        if ($taxRate === false) {
            UI::inputErrors();
            return Redirect::back()->withErrors($this->taxRateRepository->errors())->withInput();
        } else {
            UI::success('Tax rate successfully added.', 'New Tax Rate');

            if (Input::get('submit') == 'return') {
                return Redirect::to(Redirector::getUrl(route('finances.tax-rates.index')));
            } else {
                if ($this->userSettings->auto_clear_forms) {
                    return Redirect::back();
                } else {
                    return Redirect::back()->withInput(Input::except('tax_name'));
                }
            }
        }
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        return View::make('finances.taxes.view');
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        if ($this->shouldInsertsAndUpdatesBeDisabled()) {
            return $this->flagDatabaseMaintenance();
        }

        if ($id == 0) {
            return $this->flagSystemResourceUpdate();
        }

        $taxRate = $this->taxRateRepository->getTaxRateById($id);

        return View::make('finances.taxes.edit')->with('taxRate', $taxRate);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {
        if ($this->shouldInsertsAndUpdatesBeDisabled()) {
            return $this->flagDatabaseMaintenance();
        }

        if ($id == 0) {
            return $this->flagSystemResourceUpdate();
        }

        $taxRate = $this->taxRateRepository->update($id, Input::all());

        if ($taxRate === false) {
            UI::inputErrors();
            return Redirect::back()->withErrors($this->taxRateRepository->errors())->withInput();
        } else {
            UI::success('Tax rate successfully updated.', 'Update Tax Rate');
            return Redirect::to(Redirector::getUrl(route('finances.tax-rates.index')));
        }
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        if ($this->shouldInsertsAndUpdatesBeDisabled()) {
            return $this->flagDatabaseMaintenance();
        }

        $id = Input::get('removeContext');
        
        if ($id == 0) {
            return $this->flagSystemResourceRemove();
        }

        $removedSuccessfully = $this->taxRateRepository->remove(array(Input::get('removeContext')));

        if ($removedSuccessfully) {
            UI::success('Tax rate removed.', 'Remove Tax Rate');
        } else {
            UI::danger('There was a problem removing the tax rate.', 'Remove Tax Rate');
        }

        return Redirect::back();
    }
}
