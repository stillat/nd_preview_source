<?php namespace App\Finances;

use AuthenticatedController;
use Redirector;
use Illuminate\Support\Facades\View;

class DashboardController extends AuthenticatedController
{
    public function __construct()
    {
        parent::__construct();
        $this->requiresServiceAccount();
    }

    public function getIndex()
    {
        Redirector::logCurrent();
        return View::make('app.dashboard');
    }
}
