<?php namespace App\Finances;

use Session;
use Redirect;
use UI;
use Input;
use Redirector;
use AuthenticatedController;
use Illuminate\Support\Facades\View;
use ParadoxOne\NDCounties\Contracts\Finance\SupplierRepositoryInterface;

class SupplierNotesController extends AuthenticatedController
{
    protected $supplierRepository;

    public function __construct(SupplierRepositoryInterface $supplierRepository)
    {
        parent::__construct();
        $this->requiresServiceAccount();
        
        $this->supplierRepository = $supplierRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index($id)
    {
        Redirector::logCurrent();

        $supplier = $this->supplierRepository->getModelById($id);
        $notes    = $this->supplierRepository->getNotes($id)->orderBy('created_at', 'desc')->paginate(user_per_page());

        return View::make('finances.suppliers.notes.list')->with('supplier', $supplier)->with('notes', $notes);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create($id)
    {
        $supplier = $this->supplierRepository->getModelById($id);
        return View::make('finances.suppliers.notes.create')->with('supplier', $supplier);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store($id)
    {
        $note = $this->supplierRepository->createNote($id, Input::all());

        if ($note === false) {
            UI::inputErrors();
            return Redirect::back()->withErrors($this->supplierRepository->errors())->withInput();
        } else {
            UI::success('Supplier successfully added.', 'New Supplier');

            if (Input::get('submit') == 'return') {
                return Redirect::to(Redirector::getURL(route('finances.suppliers.index')));
            } else {
                return Redirect::back();
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id, $noteid)
    {
        $supplier = $this->supplierRepository->getModelById($id);
        $note = $this->supplierRepository->getNote($noteid);

        return View::make('finances.suppliers.notes.view')->with('supplier', $supplier)->with('note', $note);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        dd('test');
    }
}
