<?php namespace App\Finances\Utilities;

use AuthenticatedController;
use Illuminate\Support\Facades\View;

class TransactionUtilitiesController extends AuthenticatedController
{
    public function __construct()
    {
        parent::__construct();
        $this->requiresServiceAccount();
    }

    public function getAddFunds()
    {
        return View::make('finances.accounts.dialogs.addfunds');
    }

    public function getTransferFunds()
    {
        return View::make('finances.accounts.dialogs.transfer');
    }
}
