<?php namespace App\Settings;

use AuthenticatedController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\View;
use ParadoxOne\NDCounties\Contracts\AccountRepositoryInterface;
use ParadoxOne\NDCounties\Redirector;
use ParadoxOne\NDCounties\Settings\CountyFormattingSettingsManager;
use ParadoxOne\NDCounties\Settings\ReportSettingsManager;
use UI;

class ReportSettingsController extends AuthenticatedController
{
    protected $reportSettingsManager;

    protected $formattingSettingsManager;

    protected $accountRepository;

    public function __construct(
        ReportSettingsManager $reportSettingsManager,
        CountyFormattingSettingsManager $manager,
        AccountRepositoryInterface $accountRepository
    ) {
        parent::__construct();
        $this->reportSettingsManager     = $reportSettingsManager;
        $this->formattingSettingsManager = $manager;
        $this->accountRepository         = $accountRepository;
    }

    public function getBreakdown()
    {
        Redirector::logCurrent();

        $reportSettings = $this->reportSettingsManager->getReportSettings();

        $displayModes = [
            1 => 'Full Width (Default)',
            2 => 'Centered'
        ];

        return View::make('app.settings.unified.reports.breakdown')->with('reportSettings', $reportSettings)
                   ->with('displayModes', $displayModes);
    }

    public function getBuilder()
    {
        Redirector::logCurrent();

        $reportSettings = $this->reportSettingsManager->getReportSettings();

        return View::make('app.settings.unified.reports.builder')->with('reportSettings', $reportSettings);
    }

    public function postBuilder()
    {
        $settings                         = $this->buildReportSettingsArray();
        $settings['show_removed_filters'] = Input::get('show_removed_filters', 0);

        $success = $this->reportSettingsManager->updateReportSettings(json_encode($settings));

        if ($success) {
            Event::fire('settings.report.builder.updated');
            UI::success('Report builder settings updated successfully.');
        } else {
            UI::danger('There was a problem updating the report builder settings.');
        }

        return Redirect::to(Redirector::getURL(url('/reports/builder')));
    }

    private function buildReportSettingsArray()
    {
        $formattedSettings = $this->reportSettingsManager->getReportSettings();

        return get_object_vars($formattedSettings);
    }

    public function postBreakdown()
    {
        $settings = $this->buildReportSettingsArray();

        $settings['show_employee_wage']                  = Input::get('show_employee_wage', 0);
        $settings['show_employee_hours_worked']                  = Input::get('show_employee_hours_worked', 0);
        $settings['show_material_breakdown']             = Input::get('show_material_breakdown', 0);
        $settings['show_material_breakdown_total']       = Input::get('show_material_breakdown_total', 0);
        $settings['show_equipment_unit_breakdown']       = Input::get('show_equipment_unit_breakdown', 0);
        $settings['show_equipment_unit_breakdown_total'] = Input::get('show_equipment_unit_breakdown_total', 0);
        $settings['show_fuel_breakdown']                 = Input::get('show_fuel_breakdown', 0);
        $settings['show_fuel_breakdown_total']           = Input::get('show_fuel_breakdown_total', 0);
        $settings['show_property_breakdown']             = Input::get('show_property_breakdown', 0);
        $settings['show_property_breakdown_total']       = Input::get('show_property_breakdown_total', 0);
        $settings['show_equipment_unit_rate']            = Input::get('show_equipment_unit_rate', 0);
        $settings['show_borders_on_breakdowns']          = Input::get('show_borders_on_breakdowns', 0);

        $mode = Input::get('record_breakdown_style', 1);

        if (!in_array($mode, [1, 2])) {
            $mode = 1;
        }

        $settings['record_breakdown_style'] = $mode;

        $success = $this->reportSettingsManager->updateReportSettings(json_encode($settings));

        if ($success) {
            Event::fire('settings.report.breakdown.updated');
            UI::success('Report data breakdown settings updated successfully.');
        } else {
            UI::danger('There was a problem updating the report data breakdown settings.');
        }

        return Redirect::to(Redirector::getURL(url('/reports/breakdown')));
    }

    public function getDescription()
    {
        Redirector::logCurrent();

        $reportSettings = $this->reportSettingsManager->getReportSettings();

        return View::make('app.settings.unified.reports.description')->with('reportSettings', $reportSettings);
    }

    public function postDescription()
    {
        $settings = $this->buildReportSettingsArray();

        $settings['always_show_billing_description']   = Input::get('always_show_billing_description', 0);
        $settings['always_show_activity_description']  = Input::get('always_show_activity_description', 0);
        $settings['only_show_descriptions_if_entered'] = Input::get('only_show_descriptions_if_entered', 0);

        $success = $this->reportSettingsManager->updateReportSettings(json_encode($settings));

        if ($success) {
            Event::fire('settings.report.description.updated');
            UI::success('Report descriptions settings updated successfully.');
        } else {
            UI::danger('There was a problem updating the report data descriptions settings.');
        }

        return Redirect::to(Redirector::getURL(url('/reports/description')));
    }

    public function getDisplay()
    {
        Redirector::logCurrent();

        $validDisplaySettings = [
            1 => 'CODE - Name',
            3 => 'Name',
            2 => 'CODE'
        ];

        $validTransformations = [
            1 => 'As Entered',
            2 => 'UPPERCASE',
            3 => 'lowercase'
        ];

        return View::make('app.settings.unified.reports.display')
                   ->with('validDisplaySettings', $validDisplaySettings)
                   ->with('validTransformations', $validTransformations)
                   ->with('formattingSettings',
                       json_decode($this->formattingSettingsManager->get(CountyFormattingSettingsManager::COUNTY_FORMATTING_SETTING_NAME)));
    }

    public function getHeadingDisplay()
    {
        Redirector::logCurrent();

        $validDisplaySettings = [
            1 => 'CODE - Name',
            3 => 'Name',
            2 => 'CODE'
        ];

        $validTransformations = [
            1 => 'As Entered',
            2 => 'UPPERCASE',
            3 => 'lowercase'
        ];

        return View::make('app.settings.unified.reports.heading_display')
            ->with('validDisplaySettings', $validDisplaySettings)
            ->with('validTransformations', $validTransformations)
            ->with('formattingSettings',
                json_decode($this->formattingSettingsManager->get('ct_formatting_heading')));
    }

    public function postDisplay()
    {
        try {
            $result = $this->accountRepository->updateAccountDisplaySettings(Input::all());

            if ($result) {
                Event::fire('settings.report.display.updated');
                UI::success('Report display settings updated successfully.');
            } else {
                throw new \Exception('Just bail');
            }
        } catch (\Exception $e) {
            UI::danger('There was a problem updating the report display settings.');
        }

        return Redirect::to(Redirector::getURL(url('/reports/display')));
    }

    public function postHeadingDisplay()
    {
        try {
            $result = $this->accountRepository->updateAccountHeadingDisplaySettings(Input::all());

            if ($result) {

                Event::fire('settings.report.display.updated');
                UI::success('Report display settings updated successfully.');
            } else {
                throw new \Exception('Just bail');
            }
        } catch (\Exception $e) {
            UI::danger('There was a problem updating the report display settings.');
        }

        return Redirect::to(Redirector::getURL(url('/reports/heading-display')));
    }

    public function getPrint()
    {
        Redirector::logCurrent();

        $reportSettings = $this->reportSettingsManager->getReportSettings();

        $orientations = [
            1 => 'Portrait (Default)',
            2 => 'Landscape'
        ];

        $marginSettings = [
            1 => 'Use Browser Settings (Default)',
            2 => 'Narrow'
        ];

        return View::make('app.settings.unified.reports.printing')->with('reportSettings', $reportSettings)
                   ->with('orientations', $orientations)->with('marginSettings', $marginSettings);
    }

    public function postPrint()
    {
        $settings = $this->buildReportSettingsArray();

        $settings['report_print_address']               = Input::get('report_print_address', 0);
        $settings['report_print_filters']               = Input::get('report_print_filters', 1);
        $settings['report_print_generator']             = Input::get('report_print_generator', 0);
        $settings['report_print_report_engine_version'] = Input::get('report_print_report_engine_version', 0);

        $orientation = Input::get('report_print_orientation', 1);

        $marginSettings = Input::get('report_print_margin_settings', 1);

        if (!in_array($orientation, [1, 2])) {
            $orientation = 1;
        }

        if (!in_array($marginSettings, [1, 2])) {
            $marginSettings = 1;
        }

        $settings['report_print_orientation']     = $orientation;
        $settings['report_print_margin_settings'] = $marginSettings;

        $success = $this->reportSettingsManager->updateReportSettings(json_encode($settings));

        if ($success) {
            Event::fire('settings.report.print.updated');
            UI::success('Report print settings updated successfully.');
        } else {
            UI::danger('There was a problem updating the report print settings.');
        }

        return Redirect::to(Redirector::getURL(url('/reports/print')));
    }
}
