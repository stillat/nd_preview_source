<?php namespace App\Settings;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use UI;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\View;
use ParadoxOne\NDCounties\Contracts\Reporting\ReportEquationsRepositoryInterface;

class ReportEquationsController extends \AuthenticatedController
{
    /**
     * The equation repository interface.
     *
     * @var ParadoxOne\NDCounties\Contracts\Reporting\ReportEquationsRepositoryInterface
     */
    protected $equationRepository;

    public function __construct(ReportEquationsRepositoryInterface $equationRepository)
    {
        parent::__construct();
        $this->equationRepository = $equationRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $defaultEquations = $this->equationRepository->getDefaultEquationForAccount(Auth::user()->last_tenant);
        $equations = $this->equationRepository->getEquations()->paginate(user_per_page());

        return View::make('app.reporting.user_custom.equations.list')->with('equations', $equations)
            ->with('defaultEquation', $defaultEquations);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return View::make('app.reporting.user_custom.equations.create');
    }


    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        $success = $this->equationRepository->createEquation(Input::get('account_id'), array_merge(Input::all(), ['account_id' => Auth::user()->last_tenant]));

        if ($success) {
            UI::success('Equation created successfully');
        } else {
            UI::danger('There was a problem creating the equation');
            return Redirect::back()->withInput()->withErrors($this->equationRepository->errors());
        }

        return Redirect::back();
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        $equation = $this->equationRepository->getEquation($id);

        if ($equation == null) {
            throw new ModelNotFoundException;
        }
        if (!in_array($equation->account_id, [0, Auth::user()->last_tenant])) {
            UI::danger('You do not have access rights to that equation');
            return Redirect::back();
        }

        return View::make('app.reporting.user_custom.equations.show')->with('equation', $equation);
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $equation = $this->equationRepository->getEquation($id);

        if ($equation == null) {
            throw new ModelNotFoundException;
        }
        if (!in_array($equation->account_id, [0, Auth::user()->last_tenant])) {
            UI::danger('You do not have access rights to that equation');
            return Redirect::back();
        }

        return View::make('app.reporting.user_custom.equations.edit')->with('equation', $equation);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {
        $equation = $this->equationRepository->getEquation($id);

        if ($equation == null) {
            throw new ModelNotFoundException;
        }
        if (!in_array($equation->account_id, [0, Auth::user()->last_tenant])) {
            UI::danger('You do not have access rights to that equation');
            return Redirect::back();
        }

        $updated = $this->equationRepository->updateEquation($id, Input::all());

        if ($updated) {
            UI::success('Report equation updated successfully');
        } else {
            UI::danger('There was a problem updating the report equation');
            return Redirect::back()->withErrors($this->equationRepository->errors())->withInput();
        }

        return Redirect::back();
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $equation = $this->equationRepository->getEquation(Input::get('removeContext'));

        if ($equation == null) {
            throw new ModelNotFoundException;
        }
        if (!in_array($equation->account_id, [Auth::user()->last_tenant])) {
            UI::danger('You do not have access rights to that equation');
            return Redirect::back();
        }

        $removedSuccessfully = $this->equationRepository->removeEquation(Input::get('removeContext'));

        if ($removedSuccessfully) {
            UI::success('Equation was removed successfully');
        } else {
            UI::danger('There was a problem removing the equation');
        }

        return Redirect::back();
    }
}
