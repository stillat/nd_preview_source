<?php namespace App\Settings;

use AuthenticatedController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\View;
use ParadoxOne\NDCounties\Contracts\AccountRepositoryInterface;
use ParadoxOne\NDCounties\Contracts\Finance\TransactionStatusRepositoryInterface;
use ParadoxOne\NDCounties\Redirector;
use ParadoxOne\NDCounties\Settings\InventorySettingsManager;
use UI;

class AccountSettingsController extends AuthenticatedController
{
    protected $accountRepository;

    protected $transactionStatusRepository;

    protected $inventorySettingsManager;


    public function __construct(AccountRepositoryInterface $accountRepository, TransactionStatusRepositoryInterface $transactionStatusRepository, InventorySettingsManager $inventorySettingsManager)
    {
        parent::__construct();
        $this->accountRepository           = $accountRepository;
        $this->transactionStatusRepository = $transactionStatusRepository;
        $this->inventorySettingsManager    = $inventorySettingsManager;
    }

    private function buildInventorySettingsArray()
    {
        $inventorySettings = $this->inventorySettingsManager->getInventorySettings();
        return get_object_vars($inventorySettings);
    }

    public function getIndex()
    {
        Redirector::logCurrent();

        return View::make('app.settings.unified.account');
    }

    public function postIndex()
    {
        $contactSettingsSuccess =
            $this->accountRepository->updateAccountContactSettings(Auth::user()->last_tenant, Input::all());

        if ($contactSettingsSuccess === false) {
            UI::inputErrors();

            return Redirect::back()->withErrors($this->accountRepository->errors())->withInput();
        } else {
            // Clear the cache settings.
            Cache::forget('ndc_county_settings_provider' . Auth::user()->last_tenant);
            UI::success('Contact information was updated successfully');
            Event::fire('settings.account.contact.updated');
            return Redirect::to(Redirector::getURL(url('/settings/account')));
        }
    }

    public function getBehavior()
    {
        Redirector::logCurrent();

        $accountSettings = $this->accountRepository->getAccountSettings(Auth::user()->last_tenant);

        return View::make('app.settings.unified.behavior')->with('accountSettings', $accountSettings)
                   ->with('transactionStatuses', $this->transactionStatusRepository->getStatuses());
    }

    public function postBehavior()
    {
        $accountDataSuccess =
            $this->accountRepository->updateAccountDataSettings(Auth::user()->last_tenant, Input::all());

        if (!$accountDataSuccess) {
            UI::inputErrors();

            return Redirect::back()->withErrors($this->accountRepository->errors())->withInput();
        } else {
            UI::success('System behaviors updated successfully.');

            Event::fire('settings.account.behavior.updated');
            return Redirect::to(Redirector::getURL(url('/settings/account/behavior')));
        }
    }

    public function getInventory()
    {
        Redirector::logCurrent();

        $inventorySettings = $this->inventorySettingsManager->getInventorySettings();

        return View::make('app.settings.unified.inventory')->with('inventorySettings', $inventorySettings);
    }

    public function postInventory()
    {
        $settings = $this->buildInventorySettingsArray();
        $settings['auto_deduct_on_record_create'] = Input::has('auto_deduct_on_record_create');
        $settings['show_inventory_management_in_consumables'] = Input::has('show_inventory_management_in_consumables');
        $settings['convert_removed_work_inventory_items_to_system'] = Input::has('convert_removed_work_inventory_items_to_system');

        $success = $this->inventorySettingsManager->updateInventorySettings(json_encode($settings));



        if ($success) {
            Event::fire('county.settings.updated', [$settings]);
            Event::fire('settings.account.inventory.updated');
            UI::success('Inventory Management settings updated successfully.');
        } else {
            UI::danger('There was a problem updating the inventory management settings.');
        }

        return Redirect::to(Redirector::getURL(url('/account/inventory')));
    }
}
