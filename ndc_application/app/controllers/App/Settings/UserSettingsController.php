<?php namespace App\Settings;

use AuthenticatedController;
use Cartalyst\Sentry\Facades\Laravel\Sentry;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\View;
use Illuminate\Support\MessageBag;
use ParadoxOne\NDCounties\Contracts\UserRepositoryInterface;
use ParadoxOne\NDCounties\Redirector;
use ParadoxOne\NDCounties\Settings\SortingSettingsManager;
use ParadoxOne\NDCounties\Settings\WorkRecordSettingsManager;
use ParadoxOne\NDCounties\UI\AvatarPresenter;
use UI;

class UserSettingsController extends AuthenticatedController
{
    protected $userRepository;

    protected $avatarPresenter;

    protected $sortingManager;

    protected $workRecordResetSettingManager;

    protected $validFormSizes = [
        2 => 'Smaller',
        1 => 'Let the browser decide (default)',
        3 => 'Larger'
    ];

    private function buildWorkRecordSettingsArray()
    {
        $formattedSettings = $this->workRecordResetSettingManager->getWorkRecordResetSettings();

        return get_object_vars($formattedSettings);
    }

    public function __construct(
        UserRepositoryInterface $userRepository,
        AvatarPresenter $presenter,
        SortingSettingsManager $sortingManager,
        WorkRecordSettingsManager $workRecordSettingsManager
    ) {
        parent::__construct();
        $this->userRepository = $userRepository;
        $this->avatarPresenter = $presenter;
        $this->sortingManager = $sortingManager;
        $this->workRecordResetSettingManager = $workRecordSettingsManager;
    }

    public function getAccessibility()
    {
        Redirector::logCurrent();

        $workRecordSettings = $this->workRecordResetSettingManager->getWorkRecordResetSettings();

        return View::make('app.settings.unified.users.accessibility')->with('formSizes', $this->validFormSizes)
            ->with('userSettings', $this->userSettings)
            ->with('workResetSettings', $workRecordSettings);
    }

    public function postAccessibility()
    {
        $autoExpandMoreOptions = Input::has('auto_expand_more_options');
        $autoClearGeneralForms = Input::has('auto_clear_general_forms');
        $showAnimationOnFormChange = Input::has('show_animation_on_form_change');

        $itemsPerPage = intval(Input::get('number_of_items_per_page', 10));

        $userFormSize = 1;

        if (in_array(Input::get('form_size', 1), array_keys($this->validFormSizes))) {
            $userFormSize = intval(Input::get('form_size'));
        }

        $settingDataArray = [];
        $settingDataArray['items_per_page'] = $itemsPerPage;
        $settingDataArray['form_size'] = $userFormSize;
        $settingDataArray['auto_expand_more_options'] = $autoExpandMoreOptions;
        $settingDataArray['auto_clear_forms'] = $autoClearGeneralForms;
        $settingDataArray['show_animation_on_form_change'] = $showAnimationOnFormChange;

        // Now we need to update the work record form settings.
        $settings = $this->buildWorkRecordSettingsArray();
        $settings['wl_reset_employee'] = Input::has('wl_reset_employee');
        $settings['wl_reset_work_date'] = Input::has('wl_reset_work_date');
        $settings['wl_reset_activity'] = Input::has('wl_reset_activity');
        $settings['wl_reset_department'] = Input::has('wl_reset_department');
        $settings['wl_reset_district'] = Input::has('wl_reset_district');
        $settings['wl_reset_property'] = Input::has('wl_reset_property');
        $settings['wl_reset_materials_and_items'] = Input::has('wl_reset_materials_and_items');
        $settings['wl_reset_settings'] = Input::has('wl_reset_settings');
        $settings['wl_reset_equipment_and_fuels'] = Input::has('wl_reset_equipment_and_fuels');
        $settings['wl_reset_only_fuels'] = Input::has('wl_reset_only_fuels');
        $settings['wl_keep_only_first_equip'] = Input::has('wl_keep_only_first_equip');
        $settings['wl_reset_project'] = Input::has('wl_reset_project');
        $settings['wl_return_to_general'] = Input::has('wl_return_to_general');
        $settings['wl_refresh_employee'] = Input::has('wl_refresh_employee');
        $settings['wl_enable_equipment_compatibility'] = Input::has('wl_enable_equipment_compatibility');
        $settings['wl_enable_invoice_compatibility'] = Input::has('wl_enable_invoice_compatibility');

        $settings['wl_optional_employee_overtime'] = Input::has('wl_optional_employee_overtime');
        $settings['wl_optional_district'] = Input::has('wl_optional_district');


        $success = $this->userRepository->updateUserSettings(Auth::user()->id, $settingDataArray);
        $this->workRecordResetSettingManager->updateWorkRecordResetSettings(json_encode($settings));

        if ($success) {
            Event::fire('settings.user.accessibility.updated');
            Session::forget('userSettings');
            UI::success('Accessibility settings successfully updated.', 'Accessibility Settings');

            return Redirect::to(Redirector::getURL(url('settings/user/accessibility')));
        } else {
            UI::danger('There was a problem updating the accessibility settings.', 'Accessibility Settings');

            return Redirect::back();
        }
    }

    public function getContact()
    {
        Redirector::logCurrent();

        $currentUser = Auth::user();

        return View::make('app.settings.unified.users.contact')->with('currentUser', $currentUser);
    }

    public function postContact()
    {
        $success = $this->userRepository->update(\Auth::user()->id, Input::all());

        if ($success) {
            UI::success('Your contact information was updated successfully.');

            if (Input::hasFile('profile_image')) {
                $imageSuccess = $this->avatarPresenter->createProfileImageFromInput('profile_image', Auth::user()->id,
                    Input::get('email'));

                if (!$imageSuccess) {
                    UI::danger('We updated your contact information, but there was a problem updating your profile image');
                }
            }

            Event::fire('settings.user.contact.updated');

            return Redirect::to(Redirector::getURL(url('/settings/user/contact')));
        } else {
            UI::inputErrors();

            return Redirect::back()->withInput()->withErrors($this->userRepository->errors());
        }
    }

    public function getChangePassword()
    {
        Redirector::logCurrent();

        return View::make('app.settings.unified.users.password');
    }

    public function postChangePassword()
    {
        // Check if user's password is correct.
        $user = Sentry::getUserProvider()->findById(Auth::user()->id);

        $errors = new MessageBag;

        if ($user->checkPassword(Input::get('current_password'))) {
            $newPassword = Input::get('new_password');
            $newPasswordConfirmation = Input::get('new_password_confirmation');

            if ($newPassword != $newPasswordConfirmation) {
                UI::danger('There was a problem updating your password.');

                $errors->add('new_password_confirmation', 'The new passwords did not match.');

                return Redirect::back()->withErrors($errors);
            } else {
                $success = $this->userRepository->resetPassword(Auth::user()->id, Input::get('new_password'));

                if (!$success) {
                    UI::danger('There was an unexpected problem updating your password.');

                    return Redirect::back();
                } else {
                    UI::success('Your password was successfully updated.');
                    Event::fire('settings.user.password.updated');

                    return Redirect::to(Redirector::getURL(url('settings/user/change-password')));
                }
            }
        } else {
            UI::danger('There was a problem updating your password.');

            $errors->add('current_password', 'The provided password was invalid');

            return Redirect::back()->withErrors($errors);
        }
    }

    public function getSorting()
    {
        Redirector::logCurrent();

        return View::make('app.settings.unified.users.sorting');
    }

    public function postSorting()
    {
        try {
            $this->sortingManager->forgetAll();

            Event::fire('settings.user.sorting.deleted');
            UI::success('Sorting settings were successfully reset.');

            return Redirect::to(Redirector::getURL(url('settings/user/sorting')));
        } catch (\Exception $e) {
        }

        UI::danger('There was a problem resetting your sorting settings.');

        return Redirect::back();
    }
}
