<?php namespace App\Settings;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use UI;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\View;
use ParadoxOne\NDCounties\Contracts\Reporting\ReportTotalBlocksRepositoryInterface;

class ReportTotalBlocksController extends \AuthenticatedController
{
    /**
     * The total blocks implementation.
     *
     * @var ParadoxOne\NDCounties\Contracts\Reporting\ReportTotalBlocksRepositoryInterface
     */
    protected $totalBlocksRepository;

    public function __construct(ReportTotalBlocksRepositoryInterface $totalBlocksRepository)
    {
        parent::__construct();
        $this->totalBlocksRepository = $totalBlocksRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $totalBlocks = $this->totalBlocksRepository->getTotalBlocks()->paginate(user_per_page());

        $defaultTotalBlock = $this->totalBlocksRepository->getDefaultTotalBlockForAccount(Auth::user()->last_tenant);

        return View::make('app.reporting.user_custom.totalblocks.list')->with('totalBlocks', $totalBlocks)
            ->with('defaultTotalBlock', $defaultTotalBlock);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return View::make('app.reporting.user_custom.totalblocks.create');
    }


    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        $success = $this->totalBlocksRepository->create(array_merge(Input::all(), ['account_id' => Auth::user()->last_tenant]));

        if ($success) {
            UI::success('The total block was successfully created');
        } else {
            UI::danger('There was a problem creating the total block');
            return Redirect::back()->withErrors($this->totalBlocksRepository->errors())->withInput();
        }

        return Redirect::back();
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        $totalBlock = $this->totalBlocksRepository->getTotalBlock($id);

        if ($totalBlock == null) {
            throw new ModelNotFoundException;
        }

        if (!in_array($totalBlock->account_id, [0, Auth::user()->last_tenant])) {
            UI::danger('You do not have access to that report total block');
            return Redirect::back();
        }

        return View::make('app.reporting.user_custom.totalblocks.view')->with('totalBlock', $totalBlock);
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $totalBlock = $this->totalBlocksRepository->getTotalBlock($id);

        if ($totalBlock == null) {
            throw new ModelNotFoundException;
        }

        if (!in_array($totalBlock->account_id, [0, Auth::user()->last_tenant])) {
            UI::danger('You do not have access to that report total block');
            return Redirect::back();
        }

        return View::make('app.reporting.user_custom.totalblocks.edit')->with('totalBlock', $totalBlock);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {
        $totalBlock = $this->totalBlocksRepository->getTotalBlock($id);

        if ($totalBlock == null) {
            throw new ModelNotFoundException;
        }

        if (!in_array($totalBlock->account_id, [0, Auth::user()->last_tenant])) {
            UI::danger('You do not have access to that report total block');
            return Redirect::back();
        }

        $updated = $this->totalBlocksRepository->update($id, Input::all());

        if ($updated) {
            UI::success('Report total block updated successfully');
        } else {
            UI::danger('There was a problem updating the report total block');
            return Redirect::back()->withErrors($this->totalBlocksRepository->errors())->withInput();
        }

        return Redirect::back();
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $totalBlock = $this->totalBlocksRepository->getTotalBlock(Input::get('removeContext'));

        if ($totalBlock == null) {
            throw new ModelNotFoundException;
        }

        if (!in_array($totalBlock->account_id, [0, Auth::user()->last_tenant])) {
            UI::danger('You do not have access to that report total block');
            return Redirect::back();
        }
        $removedSuccessfully = $this->totalBlocksRepository->remove([Input::get('removeContext')]);
        if ($removedSuccessfully) {
            UI::success('Report total block removed successfully');
        } else {
            UI::danger('There was a problem removing the report total block');
        }

        return Redirect::back();
    }
}
