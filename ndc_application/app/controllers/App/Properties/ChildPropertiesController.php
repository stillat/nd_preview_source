<?php namespace App\Properties;

use ParadoxOne\NDCounties\Contracts\Properties\PropertyCategoriesRepositoryInterface;
use UI;
use Redirector;
use AuthenticatedController;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Response;
use ParadoxOne\NDCounties\Database\Adapters\AdapterManager;
use ParadoxOne\NDCounties\Contracts\Properties\PropertiesRepositoryInterface;
use ParadoxOne\NDCounties\Contracts\Properties\PropertyTypesRepositoryInterface;

class ChildPropertiesController extends AuthenticatedController
{
    protected $propertiesRepository;

    protected $typesRepository;

    protected $categoriesRepository;

    public function __construct(PropertiesRepositoryInterface $propertiesRepository, PropertyTypesRepositoryInterface $typesRepository,
                                AdapterManager $adapterManager, PropertyCategoriesRepositoryInterface $propertyCategories)
    {
        parent::__construct();
        $this->requiresServiceAccount();

        $this->propertiesRepository = $propertiesRepository;
        $this->typesRepository = $typesRepository;
        $this->adapterManager       = $adapterManager;
        $this->categoriesRepository = $propertyCategories;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create($parentPropertyID)
    {
        if ($this->shouldInsertsAndUpdatesBeDisabled()) {
            return $this->flagDatabaseMaintenance();
        }

        if ($parentPropertyID == 0) {
            return $this->flagSystemResourceUpdate();
        }

        $parentProperty = $this->propertiesRepository->getModelById($parentPropertyID);

        $propertyTypes = $this->typesRepository->getPropertyTypes()->get();

        $propertyAdapters = $this->adapterManager->getSortedAdapters();

        $categories = $this->categoriesRepository->getCategories()->lists('category_name', 'id');


        return View::make('app.properties.create')
        ->with('parentProperty', $parentProperty)
        ->with('types', $propertyTypes)
        ->with('propertyAdapters', $propertyAdapters)->with('mode', 'child')->with('categories', $categories);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store($parentPropertyID)
    {
        if ($this->shouldInsertsAndUpdatesBeDisabled()) {
            return $this->flagDatabaseMaintenance();
        }

        if ($parentPropertyID == 0) {
            return $this->flagSystemResourceUpdate();
        }

        $childProperty = $this->propertiesRepository->createChildProperty($parentPropertyID, Input::all());

        if ($childProperty === false) {
            UI::inputErrors();
            return Redirect::back()->withErrors($this->propertiesRepository->errors())->withInput();
        } else {
            UI::success('Child property successfully added.', 'New Child Property');

            if (Input::get('submit') == 'return') {
                return Redirect::to(Redirector::getURL(route('properties.show', $parentPropertyID)));
            } else {
                if ($this->userSettings->auto_clear_forms) {
                    return Redirect::back();
                } else {
                    return Redirect::back()->withInput(Input::except('code', 'name', 'description'));
                }
            }
        }
    }
}
