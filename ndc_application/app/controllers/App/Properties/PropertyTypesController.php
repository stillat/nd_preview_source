<?php namespace App\Properties;

use UI;
use Redirector;
use AuthenticatedController;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Response;
use ParadoxOne\NDCounties\Database\Adapters\AdapterManager;
use ParadoxOne\NDCounties\Contracts\Properties\PropertyTypesRepositoryInterface;

class PropertyTypesController extends \AdminController
{
    protected $propertyTypesRepository;

    protected $adapterManager;

    public function __construct(PropertyTypesRepositoryInterface $propertyTypesRepository, AdapterManager $adapterManager)
    {
        parent::__construct();
        $this->requiresServiceAccount();
        
        $this->propertyTypesRepository = $propertyTypesRepository;

        $this->adapterManager = $adapterManager;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        Redirector::logCurrent();


        $propertyTypes = $this->propertyTypesRepository->getPropertyTypes()->paginate(user_per_page());

        return View::make('app.properties.types.list')->with('propertyTypes', $propertyTypes);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        if ($this->shouldInsertsAndUpdatesBeDisabled()) {
            return $this->flagDatabaseMaintenance();
        }

        $propertyAdapters = $this->adapterManager->getAdapters();

        return View::make('app.properties.types.create')->with('propertyAdapters', $propertyAdapters);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        if ($this->shouldInsertsAndUpdatesBeDisabled()) {
            return $this->flagDatabaseMaintenance();
        }

        $adapters = Input::get('propertyAdapters');

        if ($adapters == null) {
            UI::danger('A property type must have property data associated with it.', 'Select property data to track');
            return Redirect::back()->withInput();
        }

        $type = $this->propertyTypesRepository->create(Input::all());

        if ($type === false) {
            UI::inputErrors();
            return Redirect::back()->withErrors($this->propertyTypesRepository->errors())->withInput();
        } else {
            UI::success('Property type successfully added.', 'New Property Type');

            if (Input::get('submit') == 'return') {
                return Redirect::to(Redirector::getURL(route('property-types.index')));
            } else {
                if ($this->userSettings->auto_clear_forms) {
                    return Redirect::back();
                } else {
                    return Redirect::back()->withInput(Input::except('code', 'name', 'description'));
                }
            }
        }
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        Redirector::logCurrent();

        $propertyType = $this->propertyTypesRepository->getModelById($id);

        $propertyAdapters = $this->adapterManager->getAdapters();
        
        return View::make('app.properties.types.view')->with('propertyType', $propertyType)->with('adapters', $propertyType->adapters)->with('propertyAdapters', $propertyAdapters);
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        if ($this->shouldInsertsAndUpdatesBeDisabled()) {
            return $this->flagDatabaseMaintenance();
        }

        if ($id == 0) {
            return $this->flagSystemResourceUpdate();
        }

        $propertyType = $this->propertyTypesRepository->getModelById($id);

        $propertyAdapters = $this->adapterManager->getAdapters();

        return View::make('app.properties.types.edit')->with('propertyType', $propertyType)->with('adapters', $propertyType->adapters)->with('propertyAdapters', $propertyAdapters);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {
        if ($this->shouldInsertsAndUpdatesBeDisabled()) {
            return $this->flagDatabaseMaintenance();
        }

        if ($id == 0) {
            return $this->flagSystemResourceUpdate();
        }

        $propertyType = $this->propertyTypesRepository->update($id, Input::all());

        if ($propertyType === false) {
            UI::inputErrors();
            return Redirect::back()->withErrors($this->propertyTypesRepository->errors())->withInput();
        } else {
            UI::success('Property type successfully updated');
            return Redirect::to(Redirector::getUrl(route('property-types.index')));
        }
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        if ($this->shouldInsertsAndUpdatesBeDisabled()) {
            return $this->flagDatabaseMaintenance();
        }

        $id = Input::get('removeContext');
        
        if ($id == 0) {
            return $this->flagSystemResourceRemove();
        }
        
        $removedSuccessfully = $this->propertyTypesRepository->remove(array($id));

        if ($removedSuccessfully) {
            UI::success('Property type removed');
        } else {
            UI::danger('There was a problem removing the property type');
        }

        return Redirect::back();
    }
}
