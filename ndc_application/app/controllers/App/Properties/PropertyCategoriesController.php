<?php namespace App\Properties;

use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\View;
use ParadoxOne\NDCounties\Contracts\Properties\PropertyCategoriesRepositoryInterface;
use ParadoxOne\NDCounties\Redirector;
use UI;

class PropertyCategoriesController extends \AuthenticatedController
{
    protected $categoriesRepository;

    public function __construct(PropertyCategoriesRepositoryInterface $categoriesRepository)
    {
        parent::__construct();
        $this->categoriesRepository = $categoriesRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $categories = $this->categoriesRepository->getCategories()->paginate(user_per_page());
        Redirector::logCurrent();
        return View::make('app.properties.categories.list')->with('categories', $categories);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        if ($this->shouldInsertsAndUpdatesBeDisabled()) {
            return $this->flagDatabaseMaintenance();
        }

        return View::make('app.properties.categories.create');
    }


    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        if ($this->shouldInsertsAndUpdatesBeDisabled()) {
            return $this->flagDatabaseMaintenance();
        }

        $category = $this->categoriesRepository->create(Input::all());

        if ($category === false) {
            UI::inputErrors();

            return Redirect::back()->withErrors($this->categoriesRepository->errors())->withInput();
        } else {
            UI::success('Property category successfully added.', 'New Property Category');

            if (Input::get('submit') == 'return') {
                return Redirect::to(Redirector::getURL(route('property-categories.index')));
            } else {
                return Redirect::back();
            }
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id)
    {
        if ($this->shouldInsertsAndUpdatesBeDisabled()) {
            return $this->flagDatabaseMaintenance();
        }

        if ($id == 0) {
            return $this->flagSystemResourceUpdate();
        }

        $category = $this->categoriesRepository->getCategory($id);

        return View::make('app.properties.categories.edit')->with('category', $category);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return Response
     */
    public function update($id)
    {
        if ($this->shouldInsertsAndUpdatesBeDisabled()) {
            return $this->flagDatabaseMaintenance();
        }

        if ($id == 0) {
            return $this->flagSystemResourceUpdate();
        }

        $category = $this->categoriesRepository->update($id, Input::all());

        if ($category === false) {
            UI::inputErrors();

            return Redirect::back()->withErrors($this->categoriesRepository->errors())->withInput();
        } else {
            UI::success('Property category successfully updated.');
            return Redirect::to(Redirector::getURL(route('property-categories.index')));
        }
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        if ($this->shouldInsertsAndUpdatesBeDisabled()) {
            return $this->flagDatabaseMaintenance();
        }

        $id = Input::get('removeContext');

        if ($id == 0) {
            return $this->flagSystemResourceRemove();
        }

        $removedSuccessfully = $this->categoriesRepository->remove(array(Input::get('removeContext')));

        if ($removedSuccessfully) {
            UI::success('Property category resource removed');
        } else {
            UI::danger('There was a problem removing the property category.');
        }

        return Redirect::back();
    }
}
