<?php namespace App\Properties;

use AuthenticatedController;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\View;
use ParadoxOne\NDCounties\Contracts\Properties\PropertiesRepositoryInterface;
use ParadoxOne\NDCounties\Contracts\Properties\PropertyCategoriesRepositoryInterface;
use ParadoxOne\NDCounties\Contracts\Properties\PropertyTypesRepositoryInterface;
use ParadoxOne\NDCounties\Database\Adapters\AdapterManager;
use Redirector;
use UI;

class PropertiesController extends AuthenticatedController
{
    protected $propertiesRepository;

    protected $typesRepository;

    protected $categoriesRepository;

    protected $adapterManager;

    public function __construct(PropertiesRepositoryInterface $propertiesRepository, PropertyTypesRepositoryInterface $typesRepository,
                                AdapterManager $adapterManager, PropertyCategoriesRepositoryInterface $propertyCategories)
    {
        parent::__construct();
        $this->requiresServiceAccount();

        $this->propertiesRepository = $propertiesRepository;
        $this->typesRepository      = $typesRepository;
        $this->adapterManager       = $adapterManager;
        $this->categoriesRepository = $propertyCategories;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Response
     */
    public function index()
    {
        Redirector::logCurrent();

        $sort = Input::get('sort', null);
        $typeFilter = Input::get('filter', null);

        $properties = array();

        $foundResults = false;

        if (Input::get('search', false) == 1) {
            $ids = Cache::get(search_cache_key('property_search'));
            if (is_array($ids) && count($ids) > 0) {
                $foundResults = true;
                $properties = $this->propertiesRepository->getAllWithTrashed()->whereIn('properties.id', $ids)->paginate(user_per_page());
            } else {
                if ($sort === null or $sort == -1) {
                    $properties = $this->propertiesRepository->getProperties()->where('exclusively_owned', '=', false);
                } else {
                    $properties = $this->propertiesRepository->getProperties()->where('category_id', '=', intval($sort))
                        ->where('exclusively_owned', '=', false);
                }

                if ($typeFilter !== null) {
                    $properties = $properties->where('property_type', '=', $typeFilter);
                }

                $properties = $properties->paginate(user_per_page());
                UI::info('Search contained no results.', 'Search Properties');
            }
        } else {
            if ($sort === null or $sort == -1) {
                $properties = $this->propertiesRepository->getProperties()->where('exclusively_owned', '=', false);
            } else {
                $properties = $this->propertiesRepository->getProperties()->where('category_id', '=', intval($sort))
                    ->where('exclusively_owned', '=', false);
            }

            if ($typeFilter !== null) {
                $properties = $properties->where('property_type', '=', $typeFilter);
            }

            $properties = $properties->paginate(user_per_page());
        }


        $types = $this->typesRepository->getPropertyTypes()->lists('name', 'id');

        $categories = $this->categoriesRepository->getCategories()->lists('category_name', 'id');
        $categories = [-1 => 'All Property Categories'] + $categories;

        $additionalData = [
            'headers'  => '',
            'adapters' => '',
        ];

        if (Input::get('sort', -1) >= 0) {
            if (count($properties) > 0) {
                $additionalData['adapters'] = $properties[0]->type->adapters->lists('adapter');
                $additionalData['headers']  = $this->adapterManager->getListHeaders($additionalData['adapters']);
            }
        }

        return View::make('app.properties.list')
                   ->with('properties', $properties)
                   ->with('propertyTypes', $types)
                   ->with('currentType', $sort)
                   ->with('categories', $categories)
                   ->with('additionalData', $additionalData)
                   ->with('manager', $this->adapterManager)
            ->with('foundResults', $foundResults)
                   ->with('restrictionString', make_restrictions_string(array_pluck($properties, 'id')));
        ;
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        if ($this->shouldInsertsAndUpdatesBeDisabled()) {
            return $this->flagDatabaseMaintenance();
        }

        $propertyTypes = $this->typesRepository->getPropertyTypes()->get();

        $propertyAdapters = $this->adapterManager->getSortedAdapters();

        $categories = $this->categoriesRepository->getCategories()->lists('category_name', 'id');

        return View::make('app.properties.create')->with('types', $propertyTypes)
                   ->with('propertyAdapters', $propertyAdapters)->with('mode', 'normal')
                   ->with('categories', $categories);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        if ($this->shouldInsertsAndUpdatesBeDisabled()) {
            return $this->flagDatabaseMaintenance();
        }

        $property = $this->propertiesRepository->create(Input::all());

        if ($property === false) {
            UI::inputErrors();

            return Redirect::back()->withErrors($this->propertiesRepository->errors())->withInput();
        } else {
            UI::success('Property successfully added.', 'New Property');

            if (Input::get('submit') == 'return') {
                return Redirect::to(Redirector::getURL(route('properties.index')));
            } else {
                if ($this->userSettings->auto_clear_forms) {
                    return Redirect::back();
                } else {
                    return Redirect::back()->withInput(Input::except('code', 'name', 'description'));
                }
            }
        }
    }


    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        $property = $this->propertiesRepository->getModelById($id);
        $adapters = $property->type->adapters->lists('id');

        $adapterHeaders = $this->adapterManager->getListHeaders($adapters);
        $adapterData    = $this->adapterManager->getListData($adapters, $property);

        Redirector::logCurrent();


        return View::make('app.properties.view')->with('property', $property)
                   ->with('childProperties', $property->children)->with('adapterHeaders', $adapterHeaders)
                   ->with('adapterData', $adapterData);
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id)
    {
        if ($this->shouldInsertsAndUpdatesBeDisabled()) {
            return $this->flagDatabaseMaintenance();
        }

        if (in_array($id, [0, 1])) {
            return $this->flagSystemResourceUpdate();
        }

        $property = $this->propertiesRepository->getModelById($id);

        $types = $this->typesRepository->getPropertyTypes()->lists('name', 'id');

        $categories = $this->categoriesRepository->getCategories()->lists('category_name', 'id');

        $propertyAdapters = $this->adapterManager->getSortedAdapters();

        return View::make('app.properties.edit')->with('property', $property)->with('types', $types)
                   ->with('propertyAdapters', $propertyAdapters)
                   ->with('categories', $categories);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return Response
     */
    public function update($id)
    {
        if ($this->shouldInsertsAndUpdatesBeDisabled()) {
            return $this->flagDatabaseMaintenance();
        }

        if (in_array($id, [0, 1])) {
            return $this->flagSystemResourceUpdate();
        }

        $property = $this->propertiesRepository->update($id, Input::all());

        if ($property === false) {
            UI::inputErrors();

            return Redirect::back()->withErrors($this->propertiesRepository->errors())->withInput();
        } else {
            UI::success('Property successfully updated.', 'Update Property');

            return Redirect::to(Redirector::getUrl(route('properties.index')));
        }
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        if ($this->shouldInsertsAndUpdatesBeDisabled()) {
            return $this->flagDatabaseMaintenance();
        }

        $id = Input::get('removeContext');

        $multiFlag = Input::get('removeMultiple', 0);

        if ($multiFlag == '1') {
            $id = explode(',', $id);
            $id = array_strip_values($id, [0,1]);
        } else {
            if ($id == 0) {
                return $this->flagSystemResourceRemove();
            }
        }

        if (!is_array($id)) {
            $id = (array)$id;
        }

        $removedSuccessfully = $this->propertiesRepository->remove($id);

        if ($removedSuccessfully) {
            UI::success('Property removed.', 'Remove Property');
        } else {
            UI::danger('There was a problem removing the property.', 'Remove Property');
        }

        return Redirect::back();
    }
}
