<?php namespace App\Properties\Roads;

use UI;
use Redirector;
use AuthenticatedController;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Response;
use ParadoxOne\NDCounties\Contracts\Properties\Roads\SurfaceTypeRepositoryInterface;

class SurfaceTypesController extends AuthenticatedController
{
    protected $surfaceTypeRepository;

    public function __construct(SurfaceTypeRepositoryInterface $surfaceTypeRepository)
    {
        parent::__construct();
        $this->requiresServiceAccount();

        $this->surfaceTypeRepository = $surfaceTypeRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        Redirector::logCurrent();

        $surfaceTypes = $this->surfaceTypeRepository->getSurfaceTypes()->paginate(user_per_page());

        return View::make('app.properties.roads.surfacetypes.list')->with('surfaceTypes', $surfaceTypes);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return View::make('app.properties.roads.surfacetypes.create');
    }


    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        $surfaceType = $this->surfaceTypeRepository->create(Input::all());

        if ($surfaceType === false) {
            UI::inputErrors();
            return Redirect::back()->withErrors($this->surfaceTypeRepository->errors())->withInput();
        } else {
            UI::success('Road surface type successfully added.', 'New Road Surface Type');

            if (Input::get('submit') == 'return') {
                return Redirect::to(Redirector::getURL(route('road-surface-types.index')));
            } else {
                if ($this->userSettings->auto_clear_forms) {
                    return Redirect::back();
                } else {
                    return Redirect::back()->withInput(Input::except('code', 'name', 'description'));
                }
            }
        }
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        Redirector::logCurrent();

        $surfaceType = $this->surfaceTypeRepository->getModelById($id);

        return View::make('app.properties.roads.surfacetypes.view')->with('surfaceType', $surfaceType);
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        if ($id == 0) {
            return $this->flagSystemResourceUpdate();
        }

        $surfaceType = $this->surfaceTypeRepository->getModelById($id);

        return View::make('app.properties.roads.surfacetypes.edit')->with('surfaceType', $surfaceType);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {
        if ($id == 0) {
            return $this->flagSystemResourceUpdate();
        }
        
        $surfaceType = $this->surfaceTypeRepository->update($id, Input::all());

        if ($surfaceType === false) {
            UI::inputErrors();
            return Redirect::back()->withErrors($this->surfaceTypeRepository->errors())->withInput();
        } else {
            UI::success('Surface type successfully updated');
            return Redirect::to(Redirector::getURL(route('road-surface-types.index')));
        }
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $id = Input::get('removeContext');

        if ($id == 0) {
            return $this->flagSystemResourceRemove();
        }

        $removedSuccessfully = $this->surfaceTypeRepository->remove(array($id));

        if ($removedSuccessfully) {
            UI::success('Surface type removed');
        } else {
            UI::danger('There was a problem removing the surface type');
        }

        return Redirect::back();
    }
}
