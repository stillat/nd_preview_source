<?php namespace App\Properties;

use UI;
use Redirector;
use AuthenticatedController;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Response;
use ParadoxOne\NDCounties\Database\Adapters\AdapterManager;
use ParadoxOne\NDCounties\Contracts\Properties\PropertiesRepositoryInterface;

class NestedPropertiesController extends AuthenticatedController
{
    protected $propertiesRepository;

    protected $adapterManager;

    public function __construct(PropertiesRepositoryInterface $propertiesRepository, AdapterManager $adapterManager)
    {
        parent::__construct();
        $this->requiresServiceAccount();

        $this->propertiesRepository = $propertiesRepository;
        $this->adapterManager       = $adapterManager;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create($parentPropertyID)
    {
        if ($this->shouldInsertsAndUpdatesBeDisabled()) {
            return $this->flagDatabaseMaintenance();
        }

        if ($parentPropertyID == 0) {
            return $this->flagSystemResourceUpdate();
        }
        
        $parentProperty = $this->propertiesRepository->getModelById($parentPropertyID);

        return View::make('app.properties.children.nested.create')
               ->with('parentProperty', $parentProperty);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store($parentPropertyID)
    {
        if ($this->shouldInsertsAndUpdatesBeDisabled()) {
            return $this->flagDatabaseMaintenance();
        }

        if ($parentPropertyID == 0) {
            return $this->flagSystemResourceUpdate();
        }
        
        $nestedProperties = Input::get('nestedProperty');

        if (count($nestedProperties) == 0 or (count($nestedProperties) == 1 && $nestedProperties[0] == "")) {
            UI::info('There were no properties to nest; and no changes were made.', 'Attach Nested Property');
            return Redirect::to(Redirector::getURL(route('properties.show', $parentPropertyID)));
        }

        $success = $this->propertiesRepository->attachChildProperty($parentPropertyID, $nestedProperties);

        if ($success == false) {
            UI::inputErrors();
            return Redirect::back();
        } else {
            $message = 'The properties were successfully nested.';

            if ($this->propertiesRepository->duplicateChildrenDuringAttachment) {
                $message .= ' Some properties were not added, because they are already nested to under this property.';
            }

            UI::success($message, 'Nested Properties Added');
            return Redirect::to(Redirector::getURL(route('properties.show', $parentPropertyID)));
        }
    }
}
