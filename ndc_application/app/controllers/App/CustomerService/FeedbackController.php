<?php namespace App\CustomerService;

use Auth;
use Redirect;
use UI;
use Input;
use Redirector;
use AuthenticatedController;
use Illuminate\Support\Facades\View;
use ParadoxOne\NDCounties\Contracts\CustomerService\FeedbackMessagesRepositoryInterface;

class FeedbackController extends AuthenticatedController
{
    protected $feedbackCategories = array(
        '0' => 'Other/General',
        '1' => 'Feature Request',
        '2' => 'Training',
        '3' => 'Software Bug',
        '4' => 'Product Enhancement',
        );

    protected $feedbackRepository;

    public function __construct(FeedbackMessagesRepositoryInterface $feedbackRepository)
    {
        parent::__construct();
        $this->feedbackRepository = $feedbackRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        Redirector::logCurrent();

        return View::make('cs.feedback.create')->with('categories', $this->feedbackCategories);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        $messageInformation = array_merge(array('user_id' => Auth::user()->id), Input::all());

        $message = $this->feedbackRepository->create($messageInformation);

        if ($message === false) {
            UI::inputErrors();
            return Redirect::back()->withErrors($this->feedbackRepository->errors())->withInput();
        } else {
            UI::success('We\'ve received your message. If you would like to add another message, you can do so below.', 'Your feedback has been received');
            return Redirect::back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
