<?php namespace App;

use \Service\CountySettings;
use AuthenticatedController;
use View;
use Redirect;
use Cache;
use Auth;
use Input;
use UI;
use Session;
use Tenant;
use ParadoxOne\NDCounties\Contracts\UserRepositoryInterface;
use ParadoxOne\NDCounties\Contracts\AccountRepositoryInterface;

class ServiceAccountController extends AuthenticatedController
{
    protected $usersRepository;
    protected $accountRepository;

    public function __construct(UserRepositoryInterface $userRepository, AccountRepositoryInterface $accountRepository)
    {
        parent::__construct();

        $this->usersRepository = $userRepository;
        $this->accountRepository = $accountRepository;
    }

    public function getChoose()
    {
        Cache::forget(Auth::user()->id.'_currentAccount');

        $serviceAccounts = array();

        if (Auth::user()->admin == false) {
            $serviceAccounts = $this->accountRepository->getAccounts(Auth::user()->id)->get();
        } else {
            $serviceAccounts = $this->accountRepository->getAllAccounts()->get();
        }

        return View::make('app.accounts.choose')->with('serviceAccounts', $serviceAccounts)->with('user', Auth::user());
    }

    public function postChoose()
    {
        $serviceAccount = Input::get('account', null);
        $serviceAccountID = Input::get('accountid', null);

        if ($serviceAccount == null or $serviceAccountID == null) {
            UI::danger('An invalid service account was supplied.', 'Service Account Connection Failed');
            return Redirect::back();
        } else {
            $account = $this->accountRepository->getAccount($serviceAccountID);
            if ($account->active == 0) {
                if (!Auth::user()->admin) {
                    UI::warning('You can not connect to that account. It has been marked as disabled.');
                    return Redirect::back();
                }
            }
        }

        UI::success('Succesfully switched accounts.', 'You\'ve changed accounts');

        // Update the users last tenant field.
        $user = Auth::user();
        $user->last_tenant = $serviceAccountID;
        $user->save();

        Tenant::assumeTenant($serviceAccountID);

        // This refreshes the user's settings.
        $settings = CountySettings::find(1);

        if ($settings === null) {
            // This will install settings for an account that is missing them
            // for some reason.
            $settings = $this->accountRepository->addSettingsToAccount();
        }

        Cache::forget(Auth::user()->id.'_currentAccount');
        Session::put('accountSettings', $settings->toArray());
        return Redirect::to('/');
    }

    public function getDestroy()
    {
        UI::info('You may now choose another account to use.');

        $user = Auth::user();
        $user->last_tenant = 0;
        $user->save();

        return Redirect::to('service/accounts/choose');
    }
}
