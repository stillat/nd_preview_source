<?php namespace App\Fuels;

use Illuminate\Support\Facades\Cache;
use View;
use Validator;
use Redirect;
use UI;
use Input;
use Tenant;
use App;
use Session;
use Redirector;
use ParadoxOne\NDCounties\Contracts\Resources\FuelRepositoryInterface;
use ParadoxOne\NDCounties\Contracts\UnitRepositoryInterface;

class FuelController extends \AuthenticatedController
{
    protected $fuelRepository;

    protected $unitRepository;

    public function __construct(FuelRepositoryInterface $fuelRepository, UnitRepositoryInterface $unitRepository)
    {
        parent::__construct();
        $this->requiresServiceAccount();
        
        $this->fuelRepository = $fuelRepository;
        $this->unitRepository = $unitRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        Redirector::logCurrent();

        $foundResults = false;

        if (Input::get('search', false) == 1) {
            $ids = Cache::get(search_cache_key('fuel_search'));
            if (is_array($ids) && count($ids) > 0) {
                $foundResults = true;
                $fuels = $this->fuelRepository->getAllWithTrashed()->whereIn('id', $ids)->paginate(user_per_page());
            } else {
                $fuels = $this->fuelRepository->getFuels()->paginate(user_per_page());
                UI::info('Search contained no results.', 'Search Fuels');
            }
        } else {
            $fuels = $this->fuelRepository->getFuels()->paginate(user_per_page());
        }

        $units = $this->unitRepository->getUnits()->lists('name', 'id');

        return View::make('app.fuels.list')->with('fuels', $fuels)->with('units', $units)->with('restrictionString', make_restrictions_string(array_pluck($fuels, 'id')))->with('foundResults', $foundResults);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        if ($this->shouldInsertsAndUpdatesBeDisabled()) {
            return $this->flagDatabaseMaintenance();
        }

        $units = $this->unitRepository->getUnits()->lists('name', 'id');

        return View::make('app.fuels.create')->with('units', $units);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        if ($this->shouldInsertsAndUpdatesBeDisabled()) {
            return $this->flagDatabaseMaintenance();
        }

        $fuel = $this->fuelRepository->create(Input::all());

        if ($fuel === false) {
            UI::inputErrors();
            return Redirect::back()->withErrors($this->fuelRepository->errors())->withInput();
        } else {
            UI::success('Fuel successfully added.', 'New Fuel Resource');

            if (Input::get('submit') == 'return') {
                return Redirect::to(Redirector::getURL(route('fuels.index')));
            } else {
                if ($this->userSettings->auto_clear_forms) {
                    return Redirect::back();
                } else {
                    return Redirect::back()->withInput(Input::except('code', 'name', 'description'));
                }
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        $fuel = $this->fuelRepository->getModelById($id);

        Redirector::logCurrent();

        $unit = $this->unitRepository->getModelById($fuel->unit_id);

        return View::make('app.fuels.view')->with('fuel', $fuel)->with('unit', $unit);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        if ($this->shouldInsertsAndUpdatesBeDisabled()) {
            return $this->flagDatabaseMaintenance();
        }

        if ($id == 0) {
            return $this->flagSystemResourceUpdate();
        }
                
        $fuel = $this->fuelRepository->getModelById($id);

        $units = $this->unitRepository->getUnits()->lists('name', 'id');

        return View::make('app.fuels.edit')->with('units', $units)->with('consumable', $fuel);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {
        if ($this->shouldInsertsAndUpdatesBeDisabled()) {
            return $this->flagDatabaseMaintenance();
        }

        if ($id == 0) {
            return $this->flagSystemResourceUpdate();
        }

        $fuel = $this->fuelRepository->update($id, Input::all());

        if ($fuel === false) {
            UI::inputErrors();
            return Redirect::back()->withErrors($this->fuelRepository->errors())->withInput();
        } else {
            UI::success('Fuel successfully updated.', 'Update Fuel');
            return Redirect::to(Redirector::getUrl(route('fuels.index')));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        if ($this->shouldInsertsAndUpdatesBeDisabled()) {
            return $this->flagDatabaseMaintenance();
        }

        $id = Input::get('removeContext');

        $multiFlag = Input::get('removeMultiple', 0);

        if ($multiFlag == '1') {
            $id = explode(',', $id);
            $id = array_strip_values($id, [0,1]);
        } else {
            if ($id == 0) {
                return $this->flagSystemResourceRemove();
            }
        }

        if (!is_array($id)) {
            $id = (array)$id;
        }

        $removedSuccessfully = $this->fuelRepository->remove($id);

        if ($removedSuccessfully) {
            UI::success('Fuel resource removed.', 'Remove Fuel');
        } else {
            UI::danger('There was a problem removing the fuel.', 'Remove Fuel');
        }

        return Redirect::back();
    }
}
