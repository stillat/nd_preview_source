<?php namespace App\Inspector;

use AuthenticatedController;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Session;
use ParadoxOne\NDCounties\Support\Facades\FormPersist;

class InspectorController extends AuthenticatedController
{
    public function __construct()
    {
        parent::__construct();
        $this->requiresServiceAccount();
        
        FormPersist::setActiveAccount(\Auth::user()->last_tenant);
    }

    public function getIndex()
    {
        return 'Inspector Running';
    }

    public function getEmployees()
    {
        $persistedData = FormPersist::getData('employees');
        return View::make('app.employees.partials.inspect')->with('employees', $persistedData);
    }

    public function getActivities()
    {
        $persistedData = FormPersist::getData('activities');
        return View::make('app.activities.partials.inspect')->with('activities', $persistedData);
    }

    public function getFSSuppliers()
    {
        $persistedData = FormPersist::getData('fs_suppliers');
        return View::make('finances.suppliers.partials.inspector')->with('suppliers', $persistedData);
    }

    public function getFSAccounts()
    {
        $persistedData = FormPersist::getData('fs_accounts');
        return View::make('finances.accounts.partials.inspector')->with('accounts', $persistedData);
    }

    public function getClear($key)
    {
        FormPersist::forget($key);
    }
}
