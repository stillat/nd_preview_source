<?php namespace App\DataTools;

use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\View;
use ParadoxOne\NDCounties\Database\Management\Reassignment\ReassignmentRepositoryFactory;
use UI;

class ReassignRecordsController extends \AuthenticatedController
{
    const MODE_ACTIVITY   = 1;
    const MODE_DEPARTMENT = 2;
    const MODE_DISTRICT   = 3;
    const MODE_EMPLOYEE   = 4;
    const MODE_FUEL       = 5;
    const MODE_MATERIAL   = 6;
    const MODE_PROJECT    = 7;

    /**
     * Holds the valid modes.
     *
     * @var array
     */
    private $validModes = [];

    /**
     * Holds the assignment titles.
     *
     * @var array
     */
    private $assignmentTitles = [
        1 => 'Activities',
        2 => 'Departments',
        3 => 'Districts',
        4 => 'Employees',
        5 => 'Fuels',
        6 => 'Materials',
        7 => 'Projects'
    ];

    public function __construct()
    {
        parent::__construct();


        // Constructs the valid modes from class constants.
        $this->validModes =
            [self::MODE_ACTIVITY, self::MODE_DEPARTMENT, self::MODE_DISTRICT, self::MODE_EMPLOYEE, self::MODE_FUEL,
             self::MODE_MATERIAL, self::MODE_PROJECT];
    }

    public function getIndex()
    {
        return View::make('data_tools.reassign.reassign');
    }

    /**
     * Checks if a given mode is valid.
     *
     * @param $mode
     * @return bool
     */
    private function isValidMode($mode)
    {
        return in_array($mode, $this->validModes);
    }

    /**
     * Gets the string representation of a given mode.
     *
     * @param $mode
     * @return mixed
     */
    private function getAssignmentTitle($mode)
    {
        return $this->assignmentTitles[$mode];
    }

    public function getReassign($mode)
    {
        if (!$this->isValidMode($mode)) {
            UI::danger('The provided record type is not valid.', 'Reassign Records Failed To Load');

            return Redirect::to('data-tools/reassign-records/');
        }

        return View::make('data_tools.reassign.select')->with('reassignTitle', $this->getAssignmentTitle($mode))
                   ->with('mode', $mode);
    }

    public function postReassign($mode)
    {
        if (!$this->isValidMode($mode)) {
            UI::danger('The provided record type is not valid.', 'Reassign Records Failed To Load');

            return Redirect::to('data-tools/reassign-records/');
        }

        $startRecords  = Input::get('start_records');
        $finishRecords = Input::get('finish_records');

        if (count($startRecords) != count($finishRecords)) {
            UI::danger('There was an issue reassigning the records.', 'Reassign Records Failed');

            return Redirect::to('data-tools/reassign-records/');
        }

        try {
            $repository = ReassignmentRepositoryFactory::getRepository($mode);
            $success    = $repository->reassignRecordPairs($startRecords, $finishRecords);

            if ($success) {
                UI::info('The requested records were successfully reassigned.', 'Reassign Records Successfully');
                return Redirect::to('data-tools/reassign-records/');
            }
        } catch (\Exception $e) {
            lk($e);
        }

        UI::danger('There were problems reassigning the requested records.', 'Reassign Records Failed');
        return Redirect::to('data-tools/reassign-records/');
    }
}
