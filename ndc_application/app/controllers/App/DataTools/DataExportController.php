<?php namespace App\DataTools;

use Carbon\Carbon;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\View;
use ParadoxOne\NDCounties\Database\Exporters\ExportManager;

class DataExportController extends \AuthenticatedController
{
    protected $exportManager;

    protected $exportItems = [
        'org_activity'    => 'Activities',
        'org_departments' => 'Departments',
        'org_districts'   => 'Districts',
        'org_projects'    => 'Projects',
        'res_employees'   => 'Employees',
        'res_fuels'       => 'Fuels',
        'res_materials'   => 'Materials',
        'gen_properties'  => 'Properties',
        'gen_properties_equipments'  => 'Equipment Units',
        'work_records'    => 'Work Records',
        'fs_accounts'     => 'Finance Accounts',
        'fs_invoices'     => 'Invoices',
        'fs_tax_rates'    => 'Tax Rates'
    ];

    public function __construct(ExportManager $manager)
    {
        parent::__construct();
        $this->requiresServiceAccount();
        $this->exportManager = $manager;
    }

    public function getExportRecordRange()
    {
        if (Session::has('reporting_old_date')) {
            $dates        = Session::get('report_old_date');
            $oldStartDate = $dates['start'];
            $oldEndDate   = $dates['end'];
        } else {
            $oldStartDate = Carbon::now()->toDateString();
            $oldEndDate   = Carbon::now()->toDateString();
        }

        return View::make('layouts.dialogs.export.work_records_range')
                   ->with('startDate', $oldStartDate)
                   ->with('endDate', $oldEndDate);
    }

    public function getIndex()
    {
        if (Session::has('reporting_old_date')) {
            $dates        = Session::get('report_old_date');
            $oldStartDate = $dates['start'];
            $oldEndDate   = $dates['end'];
        } else {
            $oldStartDate = Carbon::now()->toDateString();
            $oldEndDate   = Carbon::now()->toDateString();
        }

        return View::make('data_tools.export.generate')->with('exportItems', $this->exportItems)
                   ->with('startDate', $oldStartDate)
                   ->with('endDate', $oldEndDate);
    }

    public function getRunExport()
    {
        App::make('debugbar')->disable();
        $this->exportManager->applySettings(Input::all());

        return $this->exportManager->export();
    }
}
