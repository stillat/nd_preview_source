<?php namespace App\DataTools;

use Carbon\Carbon;
use DebugBar\DebugBar;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Str;
use ParadoxOne\NDCounties\Contracts\Data\BackupLogRepositoryInterface;
use ParadoxOne\NDCounties\Contracts\Data\BackupRepositoryInterface;
use ParadoxOne\NDCounties\Contracts\SystemInformationRepositoryInterface;
use ParadoxOne\NDCounties\Database\Management\BackupLocator;
use ParadoxOne\NDCounties\Database\Management\Encryption\Cryptographer;
use ParadoxOne\NDCounties\Downloaders\ClientDownloader;
use ParadoxOne\NDCounties\UI\Loaders\BackupReportLoader;
use Symfony\Component\Debug\Debug;
use UI;

class BackupController extends \AuthenticatedController
{
    protected $backupManager = null;

    protected $backupLogger = null;

    protected $systemInformationRepository = null;

    protected $cachedSystemInformation = null;

    protected $cachedSystemStatistics = null;

    public function __construct(BackupRepositoryInterface $backupManager, BackupLogRepositoryInterface $backupLogger,
                                SystemInformationRepositoryInterface $systemInformation)
    {
        parent::__construct();
        $this->backupManager               = $backupManager;
        $this->backupLogger                = $backupLogger;
        $this->systemInformationRepository = $systemInformation;
        // Set the account for the backup manager.
        $this->backupManager->setTenantAccount(Auth::user()->last_tenant);
    }

    private function convertToBytes($from){
        $number=substr($from,0,-2);
        switch(strtoupper(substr($from,-2))){
            case "KB":
                return $number*1024;
            case "MB":
                return $number*pow(1024,2);
            case "GB":
                return $number*pow(1024,3);
            case "TB":
                return $number*pow(1024,4);
            case "PB":
                return $number*pow(1024,5);
            default:
                return $from;
        }
    }

    private function getInformation()
    {
        if ($this->cachedSystemInformation == null) {
            $this->cachedSystemInformation = $this->systemInformationRepository->getSystemInformation();
        }

        return $this->cachedSystemInformation;
    }

    private function getStatistics()
    {
        if ($this->cachedSystemStatistics == null) {
            $this->cachedSystemStatistics = $this->backupLogger->getBackupStatistics(Auth::user()->last_tenant);
        }

        return $this->cachedSystemStatistics;
    }

    private function getCurrentBackupState($statistics, $systemInformation)
    {
        $currentState                    = new \stdClass;
        $currentState->restore_disabled  = false;
        $currentState->upload_disabled   = false;
        $currentState->download_disabled = false;
        $currentState->create_disabled   = false;
        $currentState->remove_disabled   = false;

        if (Auth::user()->admin) {
            return $currentState;
        }

        // Check availability of uploads.
        if (($statistics->uploadedBackupsByUsers + $statistics->failedUploadedBackupsByUsers) >= $systemInformation->backup_upload_limits) {
            $currentState->upload_disabled = true;
        }

        // Check restores.
        if ($statistics->restoredBackupsByUsers >= $systemInformation->backup_restore_backups) {
            $currentState->restore_disabled = true;
        }

        // Check create.
        if ($statistics->createdBackupsByUsers >= $systemInformation->backup_create_limits) {
            $currentState->create_disabled = true;
        }

        return $currentState;
    }

    private function getFailedOperations($statistics)
    {
        return $statistics->failedRemovedBackupsByUsers +
               $statistics->failedRemovedBackupsByAdmins +
               $statistics->failedRestoredBackupsByUsers +
               $statistics->failedRestoredBackupsByAdmins +
               $statistics->failedCreatedBackupsByUsers +
               $statistics->failedCreatedBackupsByAdmins +
               $statistics->failedUploadedBackupsByUsers +
               $statistics->failedUploadedBackupsByAdmins;
    }

    public function getIndex()
    {
        $backups     = $this->backupManager->getBackups();
        $statistics  = $this->getStatistics();
        $information = $this->getInformation();
        $backupState = $this->getCurrentBackupState($statistics, $information);

        // Load some HTML macros we will need.
        with(new BackupReportLoader());

        return View::make('data_tools.backup.list')->with('backups', $backups)->with('statistics', $statistics)
                   ->with('systemInformation', $information)->with('currentState', $backupState)
                   ->with('failedOperations', $this->getFailedOperations($this->getStatistics()));
    }

    public function postCreate()
    {
        $backupState = $this->getCurrentBackupState($this->getStatistics(), $this->getInformation());

        if ($backupState->create_disabled) {
            UI::info('Backup not created. Your account has reached its backup creation limit.', 'Create Backup');

            return Redirect::back();
        }

        $backupCreated = $this->backupManager->createBackup();

       // echo 'Creating backup file. This can take a significant amount of time. There is no need to stay on this page...<br><br><br><br>';
       // ob_flush();

        if ($backupCreated) {
            UI::success('Backup was successfully created.', 'Create Backup');
        } else {
            UI::danger('There was an issue creating the backup file.', 'Create Backup Failed');
        }

        return Redirect::back();
    }

    public function postRestoreBackup()
    {
        $backupState = $this->getCurrentBackupState($this->getStatistics(), $this->getInformation());

        if ($backupState->restore_disabled) {
            UI::info('Backup not restored. Your account has reached its backup restore limit.', 'Restore Backup File');

            return Redirect::back();
        }

        $restoreSuccess = $this->backupManager->restoreBackup(Input::get('restore_key'));

        if ($restoreSuccess) {
            UI::info('System was successfully restored.', 'Restore Backup File');
        } else {
            UI::danger('There was a problem restoring the backup file.', 'Restore Backup File');
        }

        return Redirect::back();
    }

    public function deleteBackup()
    {
        $backupState = $this->getCurrentBackupState($this->getStatistics(), $this->getInformation());

        if ($backupState->remove_disabled) {
            UI::info('Removing backup files is currently unavailable.', 'Remove Backup File');

            return Redirect::back();
        }

        $backup = Input::get('removeContext', '');

        $removedSuccessfully = $this->backupManager->removeBackup($backup);

        if ($removedSuccessfully) {
            UI::info('Backup file was successfully removed.', 'Remove Backup File');
        } else {
            UI::danger('There was a problem removing the backup file.', 'Remove Backup File Failed');
        }

        return Redirect::back();
    }

    public function getDownload($backupKey)
    {
        $backupState = $this->getCurrentBackupState($this->getStatistics(), $this->getInformation());

        if ($backupState->download_disabled) {
            UI::info('Downloading backup files is currently unavailable.', 'Download Backup File Failed');

            return Redirect::back();
        }

        $backupLocation = new BackupLocator;
        $location       = $backupLocation->getBackupLocation(Auth::user()->last_tenant);

        $fileLocation = realpath($location . '/' . $backupKey);


        if (strlen($fileLocation) < strlen($location)) {
            UI::danger('Download of backup file failed. Invalid file location detected.', 'Download Backup File Failed');
            return Redirect::back();
        } else {
            if (file_exists($fileLocation)) {
                $cryptographer = new Cryptographer;
                $cryptographer->setOutputFolder($location);
                $newFileDownloadPath = $cryptographer->encrypt($fileLocation);

                if ($newFileDownloadPath !== false) {
                    header("Content-disposition: attachment; filename=\"" . basename($newFileDownloadPath) . "\"");
                    ClientDownloader::download($newFileDownloadPath);

                    $cryptographer->cleanupEncrypt();
                } else {
                    UI::danger('Download of backup file failed. Cryptography services unavailable.', 'Download Backup File Failed');
                    return Redirect::back();
                }
            } else {
                UI::danger('Download of backup file failed. Backup file does not exist.', 'Download Backup File Failed');
                return Redirect::back();
            }
        }
    }

    public function postUploadBackup()
    {
        $backupState = $this->getCurrentBackupState($this->getStatistics(), $this->getInformation());

        if ($backupState->upload_disabled) {
            UI::info('Backup not uploaded. Your account has reached it\'s backup upload limit.',
                     'Upload Existing Backup Failed');

            return Redirect::back();
        } else {
            if (Input::hasFile('existing_backup')) {
                $backupFile     = Input::file('existing_backup');
                $backupFileName = $backupFile->getClientOriginalName();

                //DIF_2015-03-07 20_12_47.

                try {
                    $backupDate = new Carbon(substr($backupFileName,4,10));
                    $checkDate = new Carbon('2015-10-17');
                    if ($backupDate->lt($checkDate)) {
                        UI::info('File format "lt101715" no longer supported.', 'Upload Existing Backup Failed');
                        return Redirect::back();
                    }
                } catch (\Exception $e) {
                    UI::info('Backup not loaded. Invalid backup file supplied.', 'Upload Existing Backup Failed');
                    return Redirect::back();
                }


                if (Str::endsWith($backupFileName, '.ndsql')) {
                    try {
                        $success = $this->backupManager->prepareUploadedBackup($backupFile);
                        if ($success) {
                            $this->backupLogger->logUpload(Auth::user(), $backupFileName, Auth::user()->last_tenant);
                            UI::info('Backup uploaded and processed successfully.', 'Existing Backup Uploaded');
                            return Redirect::back();
                        }

                        $this->backupLogger->logUploadFailed(Auth::user(), $backupFileName, Auth::user()->last_tenant);
                        UI::info('Backup not uploaded. Invalid backup file provided.', 'Upload Existing Backup Failed');

                        return Redirect::back();
                    } catch (\Exception $e) {
                        $this->backupLogger->logUploadFailed(Auth::user(), $backupFileName, Auth::user()->last_tenant);
                        UI::info('Backup not uploaded. Invalid backup file provided.', 'Upload Existing Backup Failed');

                        return Redirect::back();
                    }
                } else {
                    $this->backupLogger->logUploadFailed(Auth::user(), $backupFileName, Auth::user()->last_tenant);
                    UI::info('Backup not uploaded. Invalid backup file provided.', 'Upload Existing Backup Failed');

                    return Redirect::back();
                }
            } else {
                // A lovely check to see the upload size was larger than the allowed size.
                if (isset($_SERVER['CONTENT_LENGTH'])
                    && (int) $_SERVER['CONTENT_LENGTH'] > $this->convertToBytes(ini_get('post_max_size'))) {
                    UI::warning('Backup not uploaded. Backup file size is too large.', 'Upload Existing Backup Failed');
                    return Redirect::back();
                }
                    UI::info('Backup not uploaded. No backup file provided.', 'Upload Existing Backup Failed');
                return Redirect::back();
            }
        }
    }
}
