<?php

use Base32\Base32;
use Cartalyst\Sentry\Facades\Laravel\Sentry;
use Illuminate\Filesystem\FileNotFoundException;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Password;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\View;
use Otp\Otp;
use ParadoxOne\NDCounties\Contracts\AccountRepositoryInterface;
use ParadoxOne\NDCounties\Contracts\UserRepositoryInterface;

class HomeController extends BaseController
{
    private $usersRepository;

    private $accountRepository;

    public function __construct(UserRepositoryInterface $usersRepository, AccountRepositoryInterface $accountRepository)
    {
        $this->usersRepository   = $usersRepository;
        $this->accountRepository = $accountRepository;
    }

    private function performLoginProcess()
    {
        Session::forget('userLastSeen');
        Session::forget('accountSettings');
        Session::forget('userSettings');

        Cache::forget(Auth::user()->id . '_currentAccount');


        $userSettings = Auth::user()->settings;

        $user = Auth::user();


        if ($userSettings == null) {
            UI::info('We automatically repaired your user settings');
            $userSettings = $this->usersRepository->addSettingsToAccount(Auth::user()->id);
        }


        Session::put('userSettings', $userSettings->toArray());


        $accounts = $this->usersRepository->getAccounts(Auth::user()->id)->get();

        if (count($accounts) == 1) {
            $activeTenant = $accounts[0];

            if ($activeTenant->tenant_active == false) {
                Auth::logout();

                return View::make('errors.inactive');
            }

            $user->last_tenant = $activeTenant->id;
            $user->save();
            Tenant::assumeTenant($user->last_tenant);

            //$settings = Service\CountySettings::find(1);
            $settings = Service\CountySettings::firstOrCreate([
                                                                  'use_districts' => 0,
                                                                  'show_employee_overtime' => 1,
                                                                  'data_decimal_places' => 4,
                                                                  'invoice_default_status' => 1,
                                                                  'auto_process_invoice' => 0
                                                              ]);

            Session::put('accountSettings', $settings->toArray());
            Session::put('singleAccountUser', true);
        } elseif (count($accounts) > 1) {
            // This will allow users to resume a tenant from a previous
            // session.
            if ($user->last_tenant == 0) {
                // Just bail here if there are more than one account.
                // This avoids having to do all the computation below and then another
                // unnecessary redirect.
                Session::put('singleAccountUser', false);

                return Redirect::action('App\ServiceAccountController@getChoose');
            } else {
                // Start the tenant service and then set the
                // user settings.
                Tenant::assumeTenant($user->last_tenant);

                // $settings = Service\CountySettings::find(1);
                $settings = Service\CountySettings::firstOrCreate([
                                                                      'use_districts' => 0,
                                                                      'show_employee_overtime' => 1,
                                                                      'data_decimal_places' => 4,
                                                                      'invoice_default_status' => 1,
                                                                      'auto_process_invoice' => 0
                                                                  ]);
                Session::put('accountSettings', $settings->toArray());
                Session::put('singleAccountUser', false);
            }
        } elseif (count($accounts) == 0) {
            if ($user->admin == 1) {
                if ($user->last_tenant == 0) {
                    // Just bail here if there are more than one account.
                    // This avoids having to do all the computation below and then another
                    // unnecessary redirect.
                    Session::put('singleAccountUser', false);

                    return Redirect::action('App\ServiceAccountController@getChoose');
                } else {
                    // Start the tenant service and then set the
                    // user settings.
                    \Tenant::assumeTenant($user->last_tenant);
                    $settings = Service\CountySettings::firstOrCreate([
                        'use_districts' => 0,
                        'show_employee_overtime' => 1,
                        'data_decimal_places' => 4,
                        'invoice_default_status' => 1,
                        'auto_process_invoice' => 0
                                                                      ]);
                    Session::put('accountSettings', $settings->toArray());
                    Session::put('singleAccountUser', false);
                }

                // If the user is a system administrator then let's just
                // redirect them to their active role.

                return Redirect::to('admin');
                // return Redirect::to(action('App\UtilityController@getSwitchRole', array(2)));
            } else {
                // If there are no service accounts and the user is not an administrator,
                // redirect them to a page that indicates they have no active service accounts.
                Auth::logout();

                return View::make('errors.inactive');
                exit;
            }
        }


        $intendedURL = Session::get('url.intended', '');

        /*
         | Since ND Counties restricts access to the 'logout' (allowing only signed in users)
         | a user may attempt to access the /logout route when they are not signed in. If they
         | do this, then Laravel will redirect them to the /logout page after they sign in. This
         | will log them out and will continue untill they clear their browser history. This little
         | code block solves that problem by checking to see if the intended URL ends with signout
         | or signin. If it does, we will just redirect to the dashboard.
         */
        if ($intendedURL == route('logout')) {
            return Redirect::to('/');
        }

        return Redirect::intended('/');
    }

    /**
     * Displays the ND Counties sign in page.
     *
     * @return View
     */
    public function getLogin()
    {
        return View::make('public.signin');
    }

    public function getResetPassword()
    {
        return View::make('public.reset_password');
    }

    public function getUpdatePassword($token)
    {
        return View::make('public.update_password')->with('token', $token);
    }

    public function postUpdatePassword($token)
    {
        $user         = Sentry::findUserByLogin(Input::get('email'));
        $tokenSuccess = $user->checkResetPasswordCode($token);

        if ($tokenSuccess) {
            $validator = Validator::make(Input::all(), [
                'email'                 => 'required',
                'password'              => 'required|min:6',
                'password_confirmation' => 'required|same:password'
            ]);

            if ($validator->fails()) {
                return Redirect::back()->withErrors($validator)->withInput(Input::except('password',
                                                                                         'password_confirmation'));
            }

            if ($user->attemptResetPassword($token, Input::get('password'))) {
                Session::flash('reset_success', true);
                try {
                    Mail::send('emails.auth.reminder_reset', compact('user'), function ($mail) use ($user) {
                        $mail->to($user->getReminderEmail());
                    });
                } catch (Exception $e) {
                }
            } else {
                Session::flash('reset_failed', true);
            }
        } else {
            Session::flash('reset_failed', true);
        }

        return Redirect::to('login');
    }

    public function postResetPassword()
    {
        try {
            $credentials = ['email' => Input::get('email'), 'password' => ''];
            $user        = Sentry::findUserByLogin(Input::get('email'));
            $token       = $user->getResetPasswordCode();
            Mail::send('emails.auth.reminder', compact('user', 'token'), function ($mail) use ($user) {
                $mail->to($user->getReminderEmail());
            });

            Session::flash('reminder_sent', true);
        } catch (Exception $e) {
            Session::flash('reminder_not_sent', true);
        }

        return Redirect::to('login');
    }

    public function getInactive()
    {
        return View::make('errors.inactive');
    }

    /**
     * Handles the ND Counties login process.
     *
     * @return mixed
     */
    public function postLogin()
    {
        $credentials = array(
            'email'    => Input::get('email'),
            'password' => Input::get('password')
        );


        if (Auth::attempt($credentials)) {
            if (Auth::user()->admin == true and Config::get('crms.requireAdminTwoFactor', true)) {
                return Redirect::to('authorize');
            } else {
                return $this->performLoginProcess();
            }
        } else {
            Session::flash('loginFailed', true);

            return Redirect::to('login')->withInput(Input::except('password'));
        }
    }

    public function getAuthorizationCode()
    {
        $user = Auth::user();
        Auth::logout();
        Session::put('oathuser', $user);

        return View::make('public.authorize');
    }

    public function postAuthorizationCode()
    {
        $otp  = new Otp;
        $user = Session::get('oathuser', null);
        Session::forget('oathuser');
        if ($user != null) {
            if ($otp->checkTotp(Base32::decode($user->secret), Input::get('oacode'))) {
                Auth::login($user);

                return $this->performLoginProcess();
            } else {
                Auth::logout();
                Session::flash('loginFailed', true);

                return Redirect::to('login');
            }
        } else {
            Session::flash('loginFailed', true);

            return Redirect::to('login');
        }
    }
}
