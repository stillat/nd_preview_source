<?php namespace System;

use Illuminate\Support\Facades\File;
use ParadoxOne\NDCounties\Database\Repositories\AccountRepository;
use Tenant;
use Otp\Otp;
use App\Account;
use BaseController;
use Service\CountySettings;
use Otp\GoogleAuthenticator;
use App\Tenant as TenantModel;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Redirect;
use ParadoxOne\NDCounties\NDCApplication;
use ParadoxOne\NDCounties\Contracts\UserRepositoryInterface;

class SetupController extends BaseController
{
    protected $userRepository;

    protected $accountRepository;

    public function __construct(UserRepositoryInterface $userRepository, AccountRepository $accountRepository)
    {
        $this->userRepository = $userRepository;
        $this->accountRepository = $accountRepository;

        // Adjust the PHP timeout for the setup in seconds.
        // 120 Seconds = 2 Minutes
        @set_time_limit(0);
    }

    private function configureHTACCESSBasedOnEnvironment()
    {
        $newRules = '';
        if (!App::environment('production')) {
            $newRules = file_get_contents(app_path().'/ndcounties/config_stubs/dev_htaccess.txt');
        } else {
            $newRules = file_get_contents(app_path().'/ndcounties/config_stubs/prod_htaccess.txt');
        }

        if (file_exists(public_path().'\.htaccess') && strlen($newRules) > 0) {
            $overwriteHandler = fopen(public_path().'\.htaccess', "r+");
            ftruncate($overwriteHandler, 0);
            file_put_contents(public_path().'\.htaccess', $newRules);
        }
    }

    public function getIndex()
    {
        // This bit of code solves an annoying problem where the setup handler
        // does not start correctly.
        File::cleanDirectory(storage_path().'/sessions/');
        git_ignore(storage_path().'/sessions/.gitignore');

        $configurationValues = array();
        $configurationValues['env'] = App::environment();
        $configurationValues['dbConnection'] = Config::get('database.default');
        $configurationValues['version'] = Config::get('crms.version');
        $configurationValues['vname'] = Config::get('crms.name');

        // This just returns database connection details.
        $defaultConnection = Config::get('database.connections');
        $defaultConnection = $defaultConnection[$configurationValues['dbConnection']];

        $configurationValues['dbConnectionDetails'] = $defaultConnection;

        $configurationValues['appKey'] = Config::get('app.key');
        $configurationValues['appTimezone'] = Config::get('app.timezone');
        $configurationValues['appUrl'] = Config::get('app.urlsssssss');


        return View::make('system.install.steps.intro')->with('config', $configurationValues);
    }

    public function getComplete()
    {
        // The system needs to be flagged as installed.
        NDCApplication::flagInstalled();
        return View::make('system.install.steps.complete');
    }

    public function getCreateServiceAccount()
    {
        return View::make('system.install.steps.sac');
    }

    public function postCreateServiceAccount()
    {
        $accountName = Input::get('aname');

        $defaultData = false;

        if (Input::has('defaultd')) {
            $defaultData = true;
        }

        $account = new Account;
        $account->account_name = $accountName;
        $account->tenant_id = 0;

        $account->save();

        $this->accountRepository->addContactSettingsToAccount($account->id);

        Tenant::createTenant($account->id);

        $createdTenant = TenantModel::where('tenant_name', '=', Tenant::getTierNameWithPrefix($account->id))->firstOrFail();

        $account->tenant_id = $createdTenant->id;
        $account->updateUniques();

        // Here we will actually install the ND Counties database tables. :)
        $migrator = App::make('stillat.database.tenant.migrator');
        $migrationsPath = realpath(app_path().'/database/tenants');

        $migrator->run($migrationsPath, false);

        Tenant::assumeTenant($account->id);
        $countySettings = new CountySettings;
        $countySettings->save();
        define('IS_INSTALLATION', true);
        $notAvailableSeeder = \App::make('\Installation\NotAvailableSeeder');
        $notAvailableSeeder->run();

        if ($defaultData) {
            $demoSeeder = \App::make('\Installation\DemoDataSeeder');
            $demoSeeder->setTenantAccount($account->id);
            $demoSeeder->run();
        }

        $this->configureHTACCESSBasedOnEnvironment();

        return Redirect::to(action('System\SetupController@getComplete'));
    }

    public function getCreateAdmin()
    {
        return View::make('system.install.steps.admin');
    }

    public function postCreateAdmin()
    {
        try {
            $secretKey = GoogleAuthenticator::generateRandom();

            $admin = $this->userRepository->create(array(
                'first_name' => Input::get('fname'),
                'last_name' => Input::get('lname'),
                'email' => Input::get('email'),
                'password' => Input::get('password'),
                'secret'   => $secretKey,
                'admin' => true
            ));

            return Redirect::to(action('System\SetupController@getConfigureSecurity'));
        } catch (Exception $e) {
        }
    }

    public function getConfigureSecurity()
    {
        $lastUser = $this->userRepository->getLastCreatedUser();
        $skey = $lastUser->secret;
        $emailAddress = $lastUser->email;

        $qrCode = GoogleAuthenticator::getQrCodeUrl('totp', "ND Counties\n".$emailAddress, $skey);

        return View::make('system.install.steps.configure_security')->with('skey', $skey)->with('qrCode', $qrCode);
    }

    public function getSetupDatabase()
    {
        return View::make('system.install.steps.database');
    }

    public function getInstallDatabaseSystems()
    {
        try {
            Artisan::call('migrate');
            Artisan::call('tenant:install');
            Artisan::call('db:seed');

            return Redirect::to(action('System\SetupController@getCreateAdmin'));
        } catch (Exception $e) {
        }
    }
}
