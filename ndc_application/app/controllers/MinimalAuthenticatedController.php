<?php

class MinimalAuthenticatedController extends Controller
{
    public function __construct()
    {
        $this->beforeFilter('auth');

        if (Auth::user() == null) {
            return Redirect::to('login');
        }

        // Debugbar::startMeasure('launching', 'Starting Tenant');

        Tenant::assumeTenant(Auth::user()->last_tenant);
        $this->userSettings = (object)user_settings();
    }
}
