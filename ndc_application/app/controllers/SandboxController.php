<?php

use Hoa\Compiler\Llk\Llk;
use Hoa\Math\Visitor\Arithmetic;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\View;
use ParadoxOne\NDCounties\DataRedactionServices\StandardEngineFactory;
use ParadoxOne\NDCounties\Contracts\InventoryManagement\AdjustmentRepositoryInterface;
use ParadoxOne\NDCounties\Contracts\Reporting\ReportConstructorInterface;
use ParadoxOne\NDCounties\Contracts\Reporting\ReportEquationsRepositoryInterface;
use ParadoxOne\NDCounties\Contracts\WorkEntries\WorkEntryReaderInterface;
use ParadoxOne\NDCounties\Database\Exporters\CSVExporters\ActivityExporter;
use ParadoxOne\NDCounties\Settings\ReportSettingsManager;

class SandboxController extends AuthenticatedController
{
    public function __construct(
    ) {
        parent::__construct();
    }

    public function getIndex()
    {
        /** @var ParadoxOne\NDCounties\Contracts\WorkEntries\WorkEntryBatchUpdateRepositoryInterface $batchUpdater */
        $batchUpdater = App::make('ParadoxOne\NDCounties\Contracts\WorkEntries\WorkEntryBatchUpdateRepositoryInterface');
        $batchUpdater->batchCleanRemovedRecords();
    }
}
