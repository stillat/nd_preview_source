<?php namespace Admin;

use Redirect;
use UI;
use Input;
use Redirector;
use AuthenticatedController;
use Illuminate\Support\Facades\View;
use ParadoxOne\NDCounties\Contracts\UnitRepositoryInterface;

class UnitsController extends AuthenticatedController
{
    protected $unitRepository;

    public function __construct(UnitRepositoryInterface $unitRepository)
    {
        parent::__construct();

        $this->unitRepository = $unitRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $units = $this->unitRepository->getUnits()->paginate(user_per_page());

        return View::make('admin.units.list')->with('units', $units);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return View::make('admin.units.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        $unit = $this->unitRepository->create(Input::all());

        if ($unit === false) {
            UI::inputErrors();
            return Redirect::back()->withErrors($this->unitRepository->errors())->withInput();
        } else {
            if (Input::get('submit') == 'return') {
                return Redirect::to(Redirector::getURL(route('admin.units.index')));
            } else {
                return Redirect::back()->withInput(Input::except('code', 'name', 'description'));
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $this->unitRepository->remove(array(Input::get('removeContext')));
        UI::success('Service unit removed');
        return Redirect::back();
    }
}
