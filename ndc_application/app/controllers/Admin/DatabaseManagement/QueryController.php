<?php namespace Admin\DatabaseManagement;

use Illuminate\Database\Connection;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Str;
use Stillat\Database\Tenant\TenantManager;
use UI;

class QueryController extends \AdminController
{
    public function getIndex()
    {
        return View::make('admin.database.query');
    }

    public function postIndex()
    {
        if (Auth::validate(['email' => Auth::user()->email, 'password' => Input::get('current_password')]) &&
            Str::endsWith(Config::get('app.key'), Input::get('security_code'))
        ) {
            /** @var TenantManager $manager */
            $manager = TenantManager::instance();

            $sql = Input::get('code');

            $tenants = $manager->getRepository()->getTenants();


            $output = '';

            $finalQuery = '';

            $runQuery = '';

            foreach ($tenants as $tenant) {
                //$finalQuery .= '/** Start query block for '.$tenant->tenant_name.'*/'.PHP_EOL.PHP_EOL;
                $finalQuery .= 'USE `'.$tenant->tenant_name.'`;'.PHP_EOL;
                $finalQuery .= $sql.PHP_EOL;
                $runQuery .= 'USE '.$tenant->tenant_name.';';
                $runQuery .= $sql;
               // $finalQuery .= PHP_EOL.PHP_EOL.'/* End query block for '.$tenant->tenant_name.'*/'.PHP_EOL.PHP_EOL.PHP_EOL;
            }

            if (!Input::has('dry')) {
                DB::unprepared(DB::raw($runQuery));
                $defaultConnection = Config::get('database.connections.'.Config::get('database.default').'.database');
                DB::unprepared(DB::raw('USE '.$defaultConnection.';'));
            }

            Input::flash();

            return View::make('admin.database.query')->with('finalQuery', $finalQuery);
        } else {
            UI::danger('Invalid credentials.');
            return Redirect::back()->withInput();
        }
    }
}
