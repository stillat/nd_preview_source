<?php namespace Admin\LearningCenter;

use AdminController;
use App;
use UI;
use View;
use User;
use Input;
use Tenant;
use ParadoxOne\NDCounties\Contracts\CustomerService\ArticlesRepositoryInterface;

class ArticlesController extends AdminController
{
    protected $articlesRepository;

    public function __construct(ArticlesRepositoryInterface $articlesRepository)
    {
        parent::__construct();

        $this->articlesRepository = $articlesRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return View::make('admin.learningcenter.index');
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return View::make('admin.learningcenter.create');
    }


    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        $article = $this->articlesRepository->create(Input::all());

        dd($article);
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {
        //
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
