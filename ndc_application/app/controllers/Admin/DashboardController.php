<?php namespace Admin;

use AdminController;
use ParadoxOne\NDCounties\Contracts\AccountRepositoryInterface;
use ParadoxOne\NDCounties\Contracts\Analytics\SystemAnalyticsRepositoryInterface;
use ParadoxOne\NDCounties\Contracts\CustomerService\FeedbackMessagesRepositoryInterface;
use ParadoxOne\NDCounties\Contracts\UserRepositoryInterface;
use View;

class DashboardController extends AdminController
{
    protected $feedbackRepository;

    protected $accountRepository;

    protected $userRepository;

    protected $analytics;

    public function __construct(FeedbackMessagesRepositoryInterface $feedbackRepository, AccountRepositoryInterface $accountRepository,
                                UserRepositoryInterface $userRepository, SystemAnalyticsRepositoryInterface $analyticsRepository)
    {
        parent::__construct();
        $this->feedbackRepository = $feedbackRepository;
        $this->accountRepository  = $accountRepository;
        $this->userRepository     = $userRepository;
        $this->analytics          = $analyticsRepository;
    }

    public function getIndex()
    {
        $feedbackMessages = $this->feedbackRepository->count();
        $messages         = $this->feedbackRepository->getMessages(10);
        $accounts         = $this->accountRepository->getAllAccounts()->get();

        return View::make('admin.dashboard')->with('feedbackCount', $feedbackMessages)
                   ->with('accounts', $accounts)
                   ->with('feedbackMessages', $messages);
    }

    public function getAnalytics()
    {
        $systemAnalytics  = $this->analytics->getSystemUsageAnalytics();
        return View::make('admin.analytics')
                   ->with('systemAnalytics', $systemAnalytics);
    }
}
