<?php namespace Admin\Reports;

use Illuminate\Support\Facades\Redirect;
use UI;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Input;
use ParadoxOne\NDCounties\Contracts\Reporting\ReportEquationsRepositoryInterface;

class EquationsController extends \AdminController
{
    protected $equationRepository;

    public function __construct(ReportEquationsRepositoryInterface $equationRepository)
    {
        parent::__construct();

        $this->equationRepository = $equationRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $equations = $this->equationRepository->getEquations()->paginate(user_per_page());

        return View::make('admin.reports.equations.list')->with('equations', $equations);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return View::make('admin.reports.equations.create');
    }


    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        $success = $this->equationRepository->createEquation(Input::get('account_id'), Input::all());

        if ($success) {
            UI::success('Equation created successfully');
        } else {
            UI::danger('There was a problem creating the equation');
            return Redirect::back()->withInput()->withErrors($this->equationRepository->errors());
        }

        return Redirect::back();
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        $equation = $this->equationRepository->getEquation($id);

        return View::make('admin.reports.equations.show')->with('equation', $equation);
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $equation = $this->equationRepository->getEquation($id);
        return View::make('admin.reports.equations.edit')->with('equation', $equation);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {
        $updated = $this->equationRepository->updateEquation($id, Input::all());

        if ($updated) {
            UI::success('Report equation updated successfully');
        } else {
            UI::danger('There was a problem updating the report equation');
            return Redirect::back()->withErrors($this->equationRepository->errors())->withInput();
        }

        return Redirect::back();
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $removedSuccessfully = $this->equationRepository->removeEquation(Input::get('removeContext'));

        if ($removedSuccessfully) {
            UI::success('Equation was removed successfully');
        } else {
            UI::danger('There was a problem removing the equation');
        }

        return Redirect::back();
    }
}
