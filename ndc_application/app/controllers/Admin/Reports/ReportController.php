<?php namespace Admin\Reports;

use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\View;
use ParadoxOne\NDCounties\Contracts\Reporting\ReportCategoryRepositoryInterface;
use ParadoxOne\NDCounties\Contracts\Reporting\ReportEngineModesRepositoryInterface;
use ParadoxOne\NDCounties\Contracts\Reporting\ReportMetaDefinitionRepositoryInterface;
use ParadoxOne\NDCounties\Database\Repositories\AccountRepository;
use ParadoxOne\NDCounties\Settings\ReportSettingsManager;
use UI;

class ReportController extends \AdminController
{
    protected $rmdRepository;

    protected $reportCategoriesRepository;

    protected $engineModesRepository;

    protected $accountsRepository;

    /**
     * @param ReportMetaDefinitionRepositoryInterface $rmdRepository
     * @param ReportCategoryRepositoryInterface       $reportCategoriesRepository
     * @param AccountRepository                       $accountsRepository
     */
    public function __construct(ReportMetaDefinitionRepositoryInterface $rmdRepository, ReportCategoryRepositoryInterface $reportCategoriesRepository, AccountRepository $accountsRepository)
    {
        parent::__construct();
        $this->rmdRepository              = $rmdRepository;
        $this->reportCategoriesRepository = $reportCategoriesRepository;
        $this->accountsRepository = $accountsRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $definitions = $this->rmdRepository->getDefinitions()->paginate(user_per_page());
        $categories    = $this->reportCategoriesRepository->getCategories();

        return View::make('admin.reports.list')->with('definitions', $definitions)->with('categories', $categories);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $categories    = $this->reportCategoriesRepository->getCategories();
        $accounts = $this->accountsRepository->getAllAccounts()->lists('account_name', 'id');
        $accounts = [0 => 'All Accounts (Everyone)'] + $accounts;
        return View::make('admin.reports.management.create')->with('categories', $categories)->with('accounts', $accounts);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        $success = $this->rmdRepository->createDefinition(Input::all());

        if ($success) {
            UI::success('Report meta definition created successfully');
        } else {
            UI::danger('There was a problem creating the report meta definition.');

            return Redirect::back()->withInput()->withErrors($this->rmdRepository->errors());
        }

        return Redirect::back();
    }


    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        $definition = $this->rmdRepository->getDefinition($id);
        $account    = $this->accountsRepository->getAccountInformation($definition->getAccountID());

        return View::make('admin.reports.show')->with('definition', $definition)->with('account', $account);
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id)
    {
        $definition = $this->rmdRepository->getDefinition($id);
        $categories    = $this->reportCategoriesRepository->getCategories();
        $accounts = $this->accountsRepository->getAllAccounts()->lists('account_name', 'id');
        $accounts = [0 => 'All Accounts (Everyone)'] + $accounts;

        return View::make('admin.reports.management.edit')->with('definition', $definition)->with('categories', $categories)->with('accounts', $accounts);
        ;
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return Response
     */
    public function update($id)
    {
        $updated = $this->rmdRepository->updateDefinition($id, Input::all());

        if ($updated) {
            UI::success('Report meta definition updated successfully');
        } else {
            UI::danger('There were problems updating the report meta definition');

            return Redirect::back()->withErrors($this->rmdRepository->errors())->withInput();
        }

        return Redirect::back();
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        $removedSuccessfully = $this->rmdRepository->removeDefinition(Input::get('removeContext'));

        if ($removedSuccessfully) {
            UI::success('Report meta definition removed successfully');
        } else {
            UI::danger('Report meta definition was not removed');
        }

        return Redirect::back();
    }
}
