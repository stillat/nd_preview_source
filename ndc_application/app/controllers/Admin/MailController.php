<?php namespace Admin;

use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\View;
use ParadoxOne\NDCounties\Contracts\AccountRepositoryInterface;

class MailController extends \AdminController
{
    protected $users;

    protected $accounts;

    public function __construct(AccountRepositoryInterface $accounts)
    {
        parent::__construct();
        $this->accounts = $accounts;
    }

    public function getIndex()
    {
        $accounts = $this->accounts->getAllAccounts()->get();
        return View::make('admin.mail.index')->with('accounts', $accounts);
    }

    public function postIndex()
    {
        $accounts = Input::get('accounts');

        foreach ($accounts as $account) {
            $users = $this->accounts->getAccountUsers($account)->get();

            if (count($users) > 0) {
                lk($users);
            }
        }
    }
}
