<?php namespace Admin\Utilities;

use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Response;
use ParadoxOne\NDCounties\Contracts\Reporting\ReportMetaDefinitionRepositoryInterface;
use ParadoxOne\NDCounties\UI\UIBuilder;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;

class ReportsController extends \AdminController
{
    protected $metaRepo;

    public function __construct(ReportMetaDefinitionRepositoryInterface $metaRepository)
    {
        parent::__construct();
        $this->metaRepo = $metaRepository;
    }

    public function getInstallDefaultReports()
    {
        $success = $this->metaRepo->installDefaultReportingSystem();

        if ($success) {
            UIBuilder::success('Reports successful installed.');
        } else {
            UIBuilder::danger('There was an unexpected problem installing the reporting system.');
        }

        return Redirect::back();
    }

    public function getDownloadReports()
    {
        $result = $this->metaRepo->exportReportsForDownload();
        if ($result === false) {
            UIBuilder::danger('There was a problem downloaing the reports.', 'Download Failed');
            return Redirect::back();
        } else {
            return Response::download($result);
        }
    }

    public function postMergeReports()
    {
        if (Auth::validate(['email' => Auth::user()->email, 'password' => Input::get('current_password')]) &&
            Str::endsWith(Config::get('app.key'), Input::get('security_code'))
        ) {
            if (Input::hasFile('report_pak')) {
                try {
                    $pak = Input::file('report_pak');
                    $pakName = $pak->getClientOriginalName();
                    $contents = file_get_contents($pak->getRealPath());
                    $deleteStatement = 'DELETE FROM system_reports_default_totals where account_id = 0;
DELETE FROM system_reports where account_id = 0;
DELETE FROM system_reports_custom_values where account_id = 0;
DELETE FROM system_reports_total_blocks where account_id = 0;';
                    $runQuery = $deleteStatement.$contents;
                    $res = DB::unprepared(DB::raw($runQuery));
                    if ($res) {
                        UIBuilder::success('Report merge success', 'Report Merger Success');
                    } else {
                        UIBuilder::danger('Report merge failed.', 'Report Merger Failed');
                    }
                    return Redirect::back();
                } catch (\Exception $e) {
                    UIBuilder::danger('Report merge failed. '.$e->getMessage(), 'Report Merger Failed');
                    return Redirect::back();
                }
            }
        } else {
            UIBuilder::danger('The credentials you entered were invalid.', 'Invalid Credentials');
            return Redirect::back();
        }
    }

}
