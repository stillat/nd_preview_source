<?php namespace Admin\Utilities;

use AdminController;
use App;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Auth;
use Input;
use ParadoxOne\NDCounties\Contracts\UserRepositoryInterface;
use ParadoxOne\NDCounties\UI\AvatarPresenter;
use Redirect;
use Tenant;
use UI;
use User;
use View;

class UserController extends AdminController
{
    protected $userRepository;

    protected $avatarPresenter;

    public function __construct(UserRepositoryInterface $userRepository, AvatarPresenter $avatarPresenter)
    {
        parent::__construct();
        $this->userRepository  = $userRepository;
        $this->avatarPresenter = $avatarPresenter;
    }

    public function getRemoveProfileImage($userID)
    {
        $user = User::findOrFail($userID);

        $success = $this->avatarPresenter->removeProfileImage($userID, $user->email);

        if ($success) {
            UI::success('Profile image removed successfully.');
        } else {
            UI::danger('Profile image was not removed successfully.', 'Remove Profile Image');
        }

        return Redirect::back();
    }

    public function getResetPassword($userID)
    {
        $user = User::findOrFail($userID);

        return View::make('admin.dialogs.users.reset_password')->with('user', $user);
    }

    public function postResetPassword()
    {
        if (Auth::validate(['email' => Auth::user()->email, 'password' => Input::get('admin_password')])) {
            $success = $this->userRepository->resetPassword(Input::get('user'), Input::get('new_password'));

            if ($success) {
                UI::success('The password was changed successfully.', 'User Account Password Was Changed');
            } else {
                UI::danger('The password was not changed successfully.', 'User Account Password Was Not Changed');
            }
        } else {
            UI::danger('The provided administrator password was incorrect.', 'Invalid Credentials');
        }

        return Redirect::back();
    }

    public function getAssumeIdentity($user)
    {
        $user = User::findOrFail($user);
        Auth::login($user);
        return Redirect::to('/');
    }
}
