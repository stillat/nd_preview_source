<?php namespace Admin\Utilities;

use AdminController;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\View;
use ParadoxOne\NDCounties\Database\Management\SettingsMerger;
use ParadoxOne\NDCounties\Contracts\ActivityStreamRepositoryInterface;
use Service\WorkEntries\ConsumableRecord;
use Stillat\Database\Support\Facades\Tenant;
use UI;

class SystemWideUtilitiesController extends AdminController
{
    protected $clearExportsCommand;

    public function __construct(\ClearExportsCommand $command)
    {
        parent::__construct();
        $this->clearExportsCommand = $command;
    }

    public function getPostNotification()
    {
        $accounts = App::make('ParadoxOne\NDCounties\Contracts\AccountRepositoryInterface')->getAllAccounts()->where('tenants.active',
            '=', true)->lists('account_name', 'id');
        $accounts[0] = 'All Accounts';

        return View::make('admin.dialogs.accounts.notification')->with('currentAccounts', $accounts);
    }

    public function postPostNotification()
    {
        /** @var ActivityStreamRepositoryInterface $stream */
        $stream = App::make('ParadoxOne\NDCounties\Contracts\ActivityStreamRepositoryInterface');

        $success = $stream->postNotification(Input::get('account', 0), Input::get('message_type', 0),
            Input::get('message'));

        if ($success) {
            UI::success('Notification sent');
        } else {
            UI::danger('There was a problem sending the notification');
        }

        return Redirect::back();
    }

    public function getClearUserExports()
    {
        $this->clearExportsCommand->fire();
        UI::success('User exports removed from disk.');
        return Redirect::back();
    }

    public function getSyncUserSettings()
    {
        $merger = App::make('ParadoxOne\NDCounties\Database\Management\SettingsMerger');
        $success = $merger->merge();

        if ($success) {
            UI::success('Settings merged successfully.', 'Settings Merged');
        } else {
            UI::danger('There was an issue merging the settings.', 'Settings Merger Failed');
        }

        return Redirect::back();
    }

    public function getOptimize()
    {
        // Do not publicly add to dashboard for now.
        chdir(base_path());
        shell_exec('php artisan optimize --force');
    }

    public function getClearApplicationCache()
    {
        Artisan::call('cache:clear');
        $files = new Filesystem;

        foreach ($files->files(storage_path() . '/views') as $file) {
            $files->delete($file);
        }

        foreach ($files->files(storage_path() . '/debugbar') as $file) {
            $files->delete($file);
        }

        foreach ($files->files(storage_path() . '/sessions') as $file) {
            $files->delete($file);
        }

        UI::success('Application cache cleared');
        return Redirect::back();
    }

    public function getPatchOrphanedFuels()
    {
        $currentTenant = Auth::user()->last_tenant;
        set_time_limit(0);
        echo 'CONTACTING TENANT SERVICES<BR>';
        $tenants = Tenant::getRepository()->getTenants();

        foreach ($tenants as $tenant) {
            Tenant::assumeTenant($tenant->id);
            echo 'CONNECTION',$tenant->id,'<br>';
            // Get equipment records.
            $equips = ConsumableRecord::select('id')->where('consumable_type', '=', 2)->get()->toArray();
            $equips = array_pluck($equips, 'id');
            // Get the orphaned fuel records...
            $records = ConsumableRecord::where('consumable_type', '=', 3)->groupBy('work_entry_id');

            if (count($equips) > 0) {
                $records = $records->where(function ($q) use ($equips) {
                    $q->whereNotIn('bound_consumable_record', $equips);
                });
            }
            $records = $records->get();

            foreach ($records as $record) {
                echo 'Patching group ', $record->work_entry_id, '<BR>';
                $newEquip = new ConsumableRecord;
                $newEquip->consumable_type = 2;
                $newEquip->consumable_id = $record->associated_id;
                $newEquip->associated_type = 2;
                $newEquip->associated_type = 0;
                $newEquip->measurement_unit_id = 0;
                $newEquip->associated_id = 0;
                $newEquip->total_cost = 0;
                $newEquip->total_quantity = 0;
                $newEquip->historic_cost = 0;
                $newEquip->work_entry_id = $record->work_entry_id;
                $saved = $newEquip->save();
                echo 'Patched', $newEquip->id, '<BR>';
                $rows = ConsumableRecord::where('associated_id', '=', $record->associated_id)->where('work_entry_id',
                    '=', $record->work_entry_id)
                    ->where('consumable_type', '=', 3)
                    ->update(['bound_consumable_record' => $newEquip->id]);
                echo 'TOTAL PATCH', $rows, '<BR>';
            }
        }
        Tenant::assumeTenant($currentTenant);

    }

}
