<?php namespace Admin\Utilities;

use App;
use UI;
use View;
use User;
use Input;
use Tenant;
use App\Account;
use AdminController;
use Redirect;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Artisan;
use ParadoxOne\NDCounties\Contracts\AccountRepositoryInterface;

class TenantController extends AdminController
{
    protected $accountRepository;

    public function __construct(AccountRepositoryInterface $accountRepository)
    {
        parent::__construct();
        $this->accountRepository = $accountRepository;
    }

    public function getRemoveAccount($accountID)
    {
        $account = $this->accountRepository->getAccount($accountID);

        return View::make('admin.dialogs.accounts.remove')->with('account', $account);
    }

    public function postRemoveAccount($accountID)
    {
        if (Auth::validate(['email' => Auth::user()->email, 'password' => Input::get('confirm_password')])) {
            $success = $this->accountRepository->removeAccount($accountID);

            if ($success) {
                UI::info('Account removed.');
                return Redirect::to('admin/accounts');
            } else {
                UI::danger('Account not removed');
            }
        } else {
            UI::danger('Invalid credentials.');
        }
        return Redirect::back();
    }

    public function getMigrateTenants()
    {
        // Here we will actually install the ND Counties database tables. :)
        $migrator = App::make('stillat.database.tenant.migrator');
        $migrationsPath = realpath(app_path().'/database/tenants');

        $migrator->run($migrationsPath, false);

        $message = '';

        foreach ($migrator->getNotes() as $note) {
            $message .= "$note<br>";
        }

        UI::success($message, 'Migrated Successfully');
        return Redirect::back();
    }

    public function getRefreshTenants()
    {
        if (App::environment('production')) {
            UI::danger('Tenant refresh is not allowed in production mode.', 'Tenant refresh error');
            return Redirect::back();
        }

        $migrationsPath = realpath(app_path().'/database/tenants');

        Artisan::call('tenant:refresh', array('--path' => $migrationsPath));

        UI::success('Tenants refreshed.');
        return Redirect::back();
    }

    public function getRollbackTenants()
    {
        // Here we will actually install the ND Counties database tables. :)
        $migrator = App::make('stillat.database.tenant.migrator');
        $migrationsPath = realpath(app_path().'/database/tenants');

        $migrator->rollback($migrationsPath, false);

        $message = '';

        foreach ($migrator->getNotes() as $note) {
            $message .= "$note<br>";
        }

        UI::success($message, 'Rollback Successful');
        return Redirect::back();
    }

    public function getSyncAccount($userID)
    {
        $user = User::findOrFail($userID);

        // Would like to get a list of service accounts the current user does not
        // belong to.
        $accounts = Account::lists('account_name', 'tenant_id');

        return View::make('admin.dialogs.accounts.sync-account')->with('user', $user)->with('accounts', $accounts);
    }

    public function postSyncAccount()
    {
        $employee = Input::get('employee_id');

        $user = User::findOrFail($employee);

        if ($user->admin) {
            UI::warning('Administrator accounts have access to all service accounts.', 'Sync Unsuccessful');
            return Redirect::back();
        }

        $account  = Input::get('account');

        $repo = Tenant::getRepository();
        $repo->grantUserOnTenant($employee, $account);
        UI::success('The account synchronization was a success.', 'Sync Successful');
        return Redirect::back();
    }

    public function deleteSyncAccount()
    {
        $user = Input::get('user');

        $userObject = User::findOrFail($user);

        if ($userObject->admin) {
            UI::warning('Administrator accounts have access to all service accounts.', 'Sync Unsuccessful');
            return Redirect::back();
        }

        $tenant = Input::get('account');

        $repo = Tenant::getRepository();
        $repo->removeUserFromTenant($user, $tenant);

        // This will cause a decision to happen mid-session.
        $userObject->last_tenant = 0;
        $userObject->save();

        UI::success('Service De-synchronization was a success.', 'De-synchronization Successful');
        return Redirect::back();
    }

    public function postDeactivateAccount()
    {
        $this->accountRepository->setActiveStatus(Input::get('account'), false);
        UI::warning('Account deactivated successfully. Users will not be able to log into this account.');
        return Redirect::back();
    }

    public function postActivateAccount()
    {
        $this->accountRepository->setActiveStatus(Input::get('account'), true);
        UI::warning('Account deactivated successfully. Users will now be able to log into this account.');
        return Redirect::back();
    }
}
