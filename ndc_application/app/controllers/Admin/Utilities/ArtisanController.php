<?php namespace Admin\Utilities;

use AdminController;
use Illuminate\Foundation\Artisan;

class ArtisanController extends AdminController
{
    /**
     * The CLI instance.
     *
     * @var Illuminate\Console\Application
     */
    protected $artisan = null;

    public function __construct(Artisan $artisan)
    {
        parent::__construct();
        $this->artisan = $artisan;
    }

    public function getIndex()
    {
        return 'Artisan utility running: <span style="color: green;">OK</span>';
    }

    public function getOptimize()
    {
        $this->artisan->call('optimize', array('--force'));
    }
}
