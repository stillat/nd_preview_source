<?php namespace Admin;

use ParadoxOne\NDCounties\Contracts\ActivityStreamRepositoryInterface;
use ParadoxOne\NDCounties\UI\Streams\Activity\UserActivityStream;
use View;
use Validator;
use Redirect;
use UI;
use Input;
use User;
use UserSetting;
use ParadoxOne\NDCounties\Contracts\UserRepositoryInterface;

class UsersController extends \AdminController
{
    private $usersRepository;
    /**
     * @var ActivityStreamRepositoryInterface
     */
    private $activityStreamRepository;
    /**
     * @var UserActivityStream
     */
    private $activityStream;

    public function __construct(UserRepositoryInterface $userRepository, ActivityStreamRepositoryInterface $activityStreamRepository, UserActivityStream $activityStream)
    {
        parent::__construct();
        $this->usersRepository = $userRepository;
        $this->activityStreamRepository = $activityStreamRepository;
        $this->activityStream = $activityStream;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $userAccounts = $this->usersRepository->table()->select('*')->orderBy('last_name', 'asc')->paginate(user_per_page());

        $administrators = $this->usersRepository->table()->select('*')->orderBy('last_name', 'asc')->where('admin', '=', true)->get();

        return View::make('admin.users.list')
                     ->with('userAccounts', $userAccounts)
                     ->with('administrators', $administrators);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return View::make('admin.users.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        $rules = array(
            'first_name' => 'required|min:2',
            'last_name' => 'required|min:2',
            'email' => 'required|email|unique:users,email',
            'password' => 'required|min:5',
        );

        $validator = Validator::make(Input::all(), $rules);

        if ($validator->fails()) {
            UI::danger('The user account was not created.', 'There were input errors.');
            return Redirect::back()->withErrors($validator)->withInput();
        }

        $adminAccount = false;

        if (Input::has('admin')) {
            $adminAccount = true;
        }

        $user = array_merge(Input::all(), array('admin' => $adminAccount));

        $userObject = $this->usersRepository->create($user);
        $userSetting = new UserSetting;
        $userSetting->user_id = $userObject->id;
        $userSetting->save();
        
        UI::success('User '.$user['first_name'].' '.$user['last_name'].' was added.', 'New User Created');
        return Redirect::route('admin.users.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        $user = User::findOrFail($id);

        $serviceAccounts = $this->usersRepository->getAccounts($id)->get();
        $activityData = $this->activityStreamRepository->getRecentActivity($user->last_tenant)->where('associated_user', '=', $user->id)->paginate(20);
        $this->activityStream->setStreamData($activityData);
        return View::make('admin.users.show')->with('user', $user)->with('serviceAccounts', $serviceAccounts)->with('activityStream', $this->activityStream)->with('activityData', $activityData);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
