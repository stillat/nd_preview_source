<?php namespace Admin;

use App;
use App\Account;
use App\Tenant as TenantModel;
use Illuminate\Support\Facades\DB;
use Input;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\Facades\Cache;
use ParadoxOne\NDCounties\Contracts\AccountRepositoryInterface;
use ParadoxOne\NDCounties\Contracts\Data\BackupRepositoryInterface;
use ParadoxOne\NDCounties\Contracts\UserRepositoryInterface;
use ParadoxOne\NDCounties\Redirector;
use Redirect;
use Service\CountySettings;
use Stillat\Database\Tenant\TenantManager;
use Tenant;
use UI;
use View;

class AccountsController extends \AdminController
{
    private $accountsRepository;
    private $userRepository;
    private $backupManager;

    public function __construct(AccountRepositoryInterface $accountRepository, UserRepositoryInterface $userRepository, BackupRepositoryInterface $backupManager)
    {
        parent::__construct();
        $this->accountRepository = $accountRepository;
        $this->userRepository    = $userRepository;
        $this->backupManager     = $backupManager;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $serviceAccounts = $this->accountRepository->table()->select('accounts.account_name', 'accounts.created_at',
                                                                     'accounts.updated_at', 'tenants.tenant_name',
                                                                     'accounts.id', DB::raw('tenants.id as tenant_id'))
                                                   ->join('tenants', 'accounts.tenant_id', '=', 'tenants.id')
                                                   ->paginate(user_per_page());

        Redirector::logCurrent();
        return View::make('admin.accounts.list')
                   ->with('accounts', $serviceAccounts)
                   ->with('userRepository', $this->userRepository);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return View::make('admin.accounts.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        set_time_limit(0);

        $account               = new Account;
        $account->account_name = Input::get('account_name');
        $account->tenant_id    = 0; // Temporarily set to 0.

        if ($account->save() == false) {
            return Redirect::back()->withErrors($account->errors())->withInput();
        }

        $this->accountRepository->addContactSettingsToAccount($account->id);

        Tenant::createTenant($account->id);

        $createdTenant =
            TenantModel::where('tenant_name', '=', Tenant::getTierNameWithPrefix($account->id))->firstOrFail();

        $account->tenant_id = $createdTenant->id;
        $account->updateUniques();

        // Here we will actually install the ND Counties database tables. :)
        $migrator       = App::make('stillat.database.tenant.migrator');
        $migrationsPath = realpath(app_path() . '/database/tenants');

        $migrator->run($migrationsPath, false);
        Tenant::assumeTenant($account->id);

        $countySettings = new CountySettings;
        $countySettings->save();

        $notAvailableSeeder = \App::make('\Installation\NotAvailableSeeder');
        $notAvailableSeeder->run();

        // Restore the user's last tenant.
        Tenant::assumeTenant(\Auth::user()->last_tenant);

        UI::success('The account ' . $account->account_name . ' was created successfully.',
                    'Account created successfully.');

        return Redirect::route('admin.accounts.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        $account      = $this->accountRepository->getAccount($id);
        $accountUsers = $this->accountRepository->getAccountUsers($account->id)->get();

        $this->backupManager->setTenantAccount($account->tenant_id);
        $backups     = $this->backupManager->getBackups();

        $currentState                    = new \stdClass;
        $currentState->restore_disabled  = false;
        $currentState->upload_disabled   = false;
        $currentState->download_disabled = false;
        $currentState->create_disabled   = false;
        $currentState->remove_disabled   = false;
        Redirector::logCurrent();

        return View::make('admin.accounts.show')
            ->with('account', $account)->with('accountUsers', $accountUsers)
            ->with('backups', $backups);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return Response
     */
    public function update($id)
    {
        $contactSettingsSuccess =
            $this->accountRepository->updateAccountContactSettings($id, Input::all());

        if ($contactSettingsSuccess === false) {
            UI::inputErrors();

            return Redirect::back()->withErrors($this->accountRepository->errors())->withInput();
        } else {
            // Clear the cache settings.
            Cache::forget('ndc_county_settings_provider' . $id);

            UI::success('Contact information was updated successfully');
            Event::fire('settings.account.contact.updated');

            return Redirect::to(action('Admin\AccountsController@show', [$id]));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
