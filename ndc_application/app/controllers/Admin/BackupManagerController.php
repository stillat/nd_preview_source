<?php namespace Admin;

use Illuminate\Support\Facades\View;
use ParadoxOne\NDCounties\Contracts\AccountRepositoryInterface;

class BackupManagerController extends \AdminController
{
    protected $accountsRepository;

    public function __construct(AccountRepositoryInterface $accountsRepository)
    {
        parent::__construct();
        $this->accountsRepository = $accountsRepository;
    }

    public function getIndex()
    {
        $accounts = $this->accountsRepository->getAllAccounts()->get();
        return View::make('admin.backup_manager.list')->with('accounts', $accounts);
    }
}
