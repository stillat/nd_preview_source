<?php namespace Admin;

use App\Terms\Term;
use Redirect;
use UI;
use Input;
use Redirector;
use AuthenticatedController;
use Illuminate\Support\Facades\View;

class UpdateTermsController extends AuthenticatedController
{

    public function getIndex()
    {
        return View::make('admin.terms.create');
    }

    public function postIndex()
    {
        $term = Term::create(Input::all());
        $rules = Term::$rules;

        if ($term->save($rules)) {
            UI::success('Service Terms Updated');
            return Redirect::back();
        } else {
            return Redirect::to(action('App\UpdateTermsController@getIndex'))->withErrors($term->errors());
        }

    }

}