<?php

class AdminController extends AuthenticatedController
{
    public function __construct()
    {
        parent::__construct();
        $this->beforeFilter('admin');
    }
}
