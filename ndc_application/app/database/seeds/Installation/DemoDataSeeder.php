<?php namespace Installation;

use DB;
use Seeder;
use Stillat\Database\Tenant\TenantManager;

class DemoDataSeeder extends Seeder
{
    protected $tenantAccount = 0;

    public function setTenantAccount($account)
    {
        $this->tenantAccount = $account;
    }

    public function run()
    {
        $manager = TenantManager::instance();

        DB::connection($manager->getCurrentConnection())
          ->table('activities')->insert([
                                            [
                                                'id'          => 1,
                                                'code'        => 'Admn-01',
                                                'name'        => 'Administration',
                                                'description' => 'Perform Administration duties.',
                                            ],
                                            [
                                                'id'          => 2,
                                                'code'        => 'BH-01',
                                                'name'        => 'Backhoe Work',
                                                'description' => 'Work with Backhoe',
                                            ],
                                            [
                                                'id'          => 3,
                                                'code'        => 'BD-01',
                                                'name'        => 'Blade',
                                                'description' => 'Blade county roads',
                                            ],
                                            [
                                                'id'          => 4,
                                                'code'        => 'CV-01',
                                                'name'        => 'Culverts',
                                                'description' => 'Install and repair culverts',
                                            ],
                                            [
                                                'id'          => 5,
                                                'code'        => 'EQ-RP-01',
                                                'name'        => 'Equipment Repair',
                                                'description' => 'Repair equipment in field or the shop.',
                                            ],
                                            [
                                                'id'          => 6,
                                                'code'        => 'EQS-01',
                                                'name'        => 'Equipment Service',
                                                'description' => 'Service equipment at shop.',
                                            ],
                                        ]);

        DB::connection($manager->getCurrentConnection())
          ->table('employees')->insert([
                                           [
                                               'id'         => 1,
                                               'first_name' => 'Mark',
                                               'last_name'  => 'Johnson',
                                               'code'       => 'LSDAA',
                                               'gender'     => 1,
                                               'wage'       => 20,
                                               'overtime'   => 30
                                           ],
                                           [
                                               'id'         => 2,
                                               'first_name' => 'John',
                                               'last_name'  => 'Hansen',
                                               'code'       => 'RO0SV',
                                               'gender'     => 1,
                                               'wage'       => 40,
                                               'overtime'   => 60
                                           ],
                                           [
                                               'id'         => 3,
                                               'first_name' => 'Brent',
                                               'last_name'  => 'Anderson',
                                               'code'       => '9ZMC0',
                                               'gender'     => 1,
                                               'wage'       => 38,
                                               'overtime'   => 42
                                           ],
                                           [
                                               'id'         => 4,
                                               'first_name' => 'Mary',
                                               'last_name'  => 'Phillips',
                                               'code'       => '5YJMU',
                                               'gender'     => 2,
                                               'wage'       => 30,
                                               'overtime'   => 45
                                           ],
                                       ]);

        DB::connection($manager->getCurrentConnection())
          ->table('fuels')->insert([
                                       [
                                           'id'      => 1,
                                           'code'    => 'DS-1',
                                           'name'    => 'Diesel',
                                           'unit_id' => 1,
                                           'cost'    => 3.4
                                       ],
                                       [
                                           'id'      => 2,
                                           'code'    => 'UN-1',
                                           'name'    => 'Unleaded',
                                           'unit_id' => 1,
                                           'cost'    => 3.5
                                       ]
                                   ]);

        DB::connection($manager->getCurrentConnection())
          ->table('materials')->insert([
                                           ['id'   => 1, 'code' => 'SA-01', 'name' => 'SAND', 'unit_id' => 11,
                                            'cost' => 4],
                                           ['id'   => 3, 'code' => 'OBYRW', 'name' => 'Culverts', 'unit_id' => 5,
                                            'cost' => 42],
                                           ['id'   => 4, 'code' => 'IIGDE', 'name' => 'Pea Rock', 'unit_id' => 11,
                                            'cost' => 20],
                                           ['id'   => 5, 'code' => 'ZHDWN', 'name' => 'Tar', 'unit_id' => 1,
                                            'cost' => 2.9],
                                       ]);

        DB::connection($manager->getCurrentConnection())
          ->table('properties')->insert([
                                            ['id'          => 2, 'property_type' => 1, 'code' => 'Test',
                                             'description' => 'Test', 'name' => 'Test'],
                                            ['id'          => 3, 'property_type' => 1, 'code' => 'OMN0H',
                                             'description' => 'NA', 'name' => 'County Road 1G'],

                                            ['id'          => 4, 'property_type' => 2, 'code' => 'S2E3S',
                                             'description' => 'NA', 'name' => 'GMC TRUCK TANDEM'],
                                            ['id'          => 5, 'property_type' => 2, 'code' => 'I9MJJ',
                                             'description' => 'NA', 'name' => 'CHEVY TRUCK'],
                                        ]);

        DB::connection($manager->getCurrentConnection())
          ->table('property_road_properties')->insert([
                                                          ['id'          => 1, 'road_id' => 2, 'surface_type_id' => 0,
                                                           'road_length' => 200, 'unit_id' => 9],
                                                          ['id'          => 2, 'road_id' => 3, 'surface_type_id' => 0,
                                                           'road_length' => 200, 'unit_id' => 9]
                                                      ]);

        DB::connection($manager->getCurrentConnection())
          ->table('equipment_units')->insert([
                                                 ['id'            => 2, 'property_id' => 4, 'year' => '1988',
                                                  'make'          => 'GMC', 'model' => 'NA', 'serial_number' => 'NA',
                                                  'rental_rate'   => 55, 'unit_id' => 8,
                                                  'purchase_cost' => 13000, 'salvage_value' => 0, 'lifetime' => 0],
                                                 ['id'            => 3, 'property_id' => 5, 'year' => '1970',
                                                  'make'          => 'Chevy', 'model' => 'C-60',
                                                  'serial_number' => 'NA', 'rental_rate' => 55, 'unit_id' => 8,
                                                  'purchase_cost' => 3500, 'salvage_value' => 0, 'lifetime' => 0],
                                             ]);


        DB::connection($manager->getCurrentConnection())
          ->table('road_work_record_logs')->insert([
                                                       ['id'              => 1, 'work_record_id' => 1,
                                                        'surface_type_id' => 0, 'road_length' => 23, 'unit_id' => 9]
                                                   ]);

        DB::connection($manager->getCurrentConnection())
          ->table('work_entries')->insert([
                                              ['id'                                => 1,
                                               'work_date'                         => '2014-09-23 00:00:00',
                                               'employee_id'                       => 0, 'activity_id' => 0,
                                               'project_id'                        => 0,
                                               'department_id'                     => 0, 'district_id' => 0,
                                               'property_context'                  => 0, 'property_id' => 2,
                                               'activity_description'              => '',
                                               'employee_hours_regular_worked'     => 10,
                                               'employee_hours_overtime_worked'    => 0,
                                               'employee_historic_wage_regular'    => 30,
                                               'employee_historic_wage_overtime'   => 0,
                                               'employee_historic_fringe_regular'  => 0,
                                               'employee_historic_fringe_overtime' => 0,
                                               'misc_billing_quantity'             => 0, 'misc_billing_rate' => 0,
                                               'misc_unit_id'                      => 9, 'misc_description' => '',
                                               'total_materials_quantity'          => 0,
                                               'total_equipment_units_quantity'    => 0, 'total_fuels_quantity' => 0,
                                               'total_materials_cost'              => 0,
                                               'total_equipment_units_cost'        => 0,
                                               'total_fuels_cost'                  => 0,
                                               'total_equipment_and_fuels_cost'    => 0, 'total_misc_billing_cost' => 0,
                                               'total_employee_regular_cost'       => 300,
                                               'total_employee_overtime_cost'      => 0,
                                               'total_employee_combined_cost'      => 300, 'record_status' => 1,
                                               'exclude_from_report'               => 1,
                                               'created_at'                        => '2014-09-23 16:53:26',
                                               'updated_at'                        => '2014-09-23 16:53:26'
                                              ]
                                          ]);
    }
}
