<?php namespace Installation;

use DB;
use Illuminate\Database\Seeder;

class UnitsSeeder extends \Seeder
{
    public function run()
    {
        DB::table('units')->truncate();

        $units = array(
            array(
                'id' => 1,
                'name' => 'Gallons',
                'code' => 'gal',
                'description'  => 'Not available',
            ),
            array(
                'id' => 2,
                'name' => 'Quarts',
                'code' => 'qt',
                'description'  => 'Not available',
            ),
            array(
                'id' => 3,
                'name' => 'Ounces',
                'code' => 'oz',
                'description'  => 'Not available',
            ),
            array(
                'id' => 4,
                'name' => 'Yard',
                'code' => 'yd',
                'description'  => 'Not available',
            ),
            array(
                'id' => 5,
                'name' => 'Foot',
                'code' => 'ft',
                'description'  => 'Not available',
            ),
            array(
                'id' => 6,
                'name' => 'Each',
                'code' => 'ea',
                'description'  => 'Not available',
            ),
            array(
                'id' => 7,
                'name' => 'Minute',
                'code' => 'min',
                'description'  => 'Not available',
            ),
            array(
                'id' => 8,
                'name' => 'Hour',
                'code' => 'hr',
                'description'  => 'Not available',
            ),
            array(
                'id' => 9,
                'name' => 'No Unit',
                'code' => 'none',
                'description'  => 'Not available',
            ),
            array(
                'id' => 10,
                'name' => 'Tube',
                'code' => 'Tube',
                'description'  => 'Not available',
            ),
            array(
                'id' => 11,
                'name' => 'Cubic Yard',
                'code' => 'CY',
                'description'  => 'Cubic Yard',
            ),
            array(
                'id' => 12,
                'name' => 'Roll',
                'code' => 'ROLL',
                'description'  => 'Roll',
            ),
            array(
                'id' => 13,
                'name' => 'Ton',
                'code' => 'TON',
                'description'  => 'Ton',
            ),
            array(
                'id' => 14,
                'name' => 'Cubic Foot',
                'code' => 'CF',
                'description'  => 'Cubic Foot',
            ),
            array(
                'id' => 15,
                'name' => 'Corrugated Box',
                'code' => 'CRGTD BOX',
                'description'  => 'Corrugated boxes are commonly used as shipping containers',
            ),
            array(
                'id' => 16,
                'name' => 'Wooden Box',
                'code' => 'WDN BOX',
                'description'  => 'Cubic Foot',
            ),
            array(
                'id' => 17,
                'name' => 'Crate',
                'code' => 'CRATE',
                'description'  => '',
            ),
            array(
                'id' => 18,
                'name' => 'Intermediate bulk shipping container',
                'code' => 'IBC',
                'description'  => '',
            ),
            array(
                'id' => 19,
                'name' => 'Flexible Intermediate Bulk Container',
                'code' => 'FIBC',
                'description'  => '',
            ),
            array(
                'id' => 20,
                'name' => 'Bulk box',
                'code' => 'BULK BOX',
                'description'  => '',
            ),
            array(
                'id' => 21,
                'name' => 'Drum',
                'code' => 'DRUM',
                'description'  => '',
            ),
            array(
                'id' => 22,
                'name' => 'Pail',
                'code' => 'PAIL',
                'description'  => '',
            ),
            array(
                'id' => 23,
                'name' => 'Unit load device',
                'code' => 'ULD',
                'description'  => '',
            ),
            array(
                'id' => 24,
                'name' => 'Transit Case',
                'code' => 'TRANSIT CASE',
                'description'  => '',
            ),
            array(
                'id' => 25,
                'name' => 'Flight Case',
                'code' => 'FLIGHT CASE',
                'description'  => '',
            ),
            array(
                'id' => 26,
                'name' => 'Road Case',
                'code' => 'ROAD CASE',
                'description'  => '',
            ),
            array(
                'id' => 27,
                'name' => 'Nuclear flask',
                'code' => 'NUCLEAR FLASK',
                'description'  => '',
            ),
            array(
                'id' => 28,
                'name' => 'Inches',
                'code' => 'INCHES',
                'description'  => '',
            ),
            array(
                'id' => 29,
                'name' => 'Centimeters',
                'code' => 'CM',
                'description'  => '',
            ),
            array(
                'id' => 30,
                'name' => 'Millimeters',
                'code' => 'MM',
                'description'  => '',
            ),
            array(
                'id' => 31,
                'name' => 'Day',
                'code' => 'DAY',
                'description'  => '',
            ),
            array(
                'id' => 32,
                'name' => 'Second',
                'code' => 'SECOND',
                'description'  => '',
            ),
            array(
                'id' => 33,
                'name' => 'Year',
                'code' => 'YEAR',
                'description'  => '',
            ),
            array(
                'id' => 34,
                'name' => 'Decade',
                'code' => 'DECADE',
                'description'  => '',
            ),
            array(
                'id' => 35,
                'name' => 'Century',
                'code' => 'CENTURY',
                'description'  => '',
            ),
            array(
                'id' => 36,
                'name' => 'Rod',
                'code' => 'ROD',
                'description'  => '',
            ),
            array(
                'id' => 37,
                'name' => 'Furlong',
                'code' => 'FURLONG',
                'description'  => 'One furlong is 201.16800 meters, or one eight of a mile',
            ),
            array(
                'id' => 38,
                'name' => 'Mile',
                'code' => 'MILE',
                'description'  => '',
            ),
            array(
                'id' => 39,
                'name' => 'Decameter',
                'code' => 'DM',
                'description'  => '',
            ),
            array(
                'id' => 40,
                'name' => 'Hectometers',
                'code' => 'HECTOMETER',
                'description'  => '',
            ),
            array(
                'id' => 41,
                'name' => 'Kilometers',
                'code' => 'KILOMTER',
                'description'  => '',
            ),
            array(
                'id' => 42,
                'name' => 'Ampere',
                'code' => 'AMP (E)',
                'description'  => '',
            ),
            array(
                'id' => 43,
                'name' => 'Volt',
                'code' => 'VOLT (E)',
                'description'  => '',
            ),
            array(
                'id' => 44,
                'name' => 'Ohm',
                'code' => 'OHM (E)',
                'description'  => '',
            ),
            array(
                'id' => 45,
                'name' => 'Watt',
                'code' => 'WATT (E)',
                'description'  => '',
            ),
            array(
                'id' => 46,
                'name' => 'Decibel-milliwate',
                'code' => 'dBm (E)',
                'description'  => '',
            ),
            array(
                'id' => 47,
                'name' => 'Decibel-Watt',
                'code' => 'dBw (E)',
                'description'  => '',
            ),
            array(
                'id' => 48,
                'name' => 'Volt-Ampere-Reactive',
                'code' => 'VOLT-AMPERE-REACTIVE (E)',
                'description'  => '',
            ),
            array(
                'id' => 49,
                'name' => 'Volt-Ampere',
                'code' => 'VOLT-AMPERE (E)',
                'description'  => '',
            ),
            array(
                'id' => 50,
                'name' => 'Farad',
                'code' => 'FARAD (E)',
                'description'  => '',
            ),
            array(
                'id' => 51,
                'name' => 'Henry',
                'code' => 'HENRY (E)',
                'description'  => '',
            ),
            array(
                'id' => 52,
                'name' => 'Coulomb',
                'code' => 'COULOMB (E)',
                'description'  => '',
            ),
            array(
                'id' => 53,
                'name' => 'Ampere-Hour',
                'code' => 'AMPERE-HOUR (E)',
                'description'  => '',
            ),
            array(
                'id' => 54,
                'name' => 'Joule',
                'code' => 'JOULE (E)',
                'description'  => '',
            ),
            array(
                'id' => 55,
                'name' => 'Kilowatt-hour',
                'code' => 'KILOWATT-HOUR (E)',
                'description'  => '',
            ),
            array(
                'id' => 56,
                'name' => 'Electron-volt',
                'code' => 'ELECTRON-VOLT (E)',
                'description'  => '',
            ),
            array(
                'id' => 57,
                'name' => 'Ohm-meter',
                'code' => 'OHM-METER (E)',
                'description'  => '',
            ),
            array(
                'id' => 58,
                'name' => 'Tesla',
                'code' => 'TESLA (E)',
                'description'  => '',
            ),
            array(
                'id' => 59,
                'name' => 'Gauss',
                'code' => 'GAUSS (E)',
                'description'  => '',
            ),
            array(
                'id' => 60,
                'name' => 'Weber',
                'code' => 'WEBER (E)',
                'description'  => '',
            ),
            array(
                'id' => 61,
                'name' => 'Decibel',
                'code' => 'DECIBEL',
                'description'  => '',
            ),
            array(
                'id' => 62,
                'name' => 'Square Yard',
                'code' => 'SQUARE YARD',
                'description'  => '',
            ),
            array(
                'id' => 63,
                'name' => 'Square Meter',
                'code' => 'SQUARE METER',
                'description'  => '',
            ),
            array(
                'id' => 64,
                'name' => 'Square Mile',
                'code' => 'SQUARE MILE',
                'description'  => '',
            ),
            array(
                'id' => 65,
                'name' => 'Pounds',
                'code' => 'POUND(S)',
                'description' => ''
            ),
            array(
                'id' => 66,
                'name' => 'Slug',
                'code' => 'SLUG(S)',
                'description' => ''
            ),
            array(
                'id' => 67,
                'name' => 'Planck mass',
                'code' => 'PLANCK MASS',
                'description' => ''
            )
        );

        DB::table('units')->insert($units);
    }
}
