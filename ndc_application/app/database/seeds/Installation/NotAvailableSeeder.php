<?php namespace Installation;

use Carbon;
use DB;
use Illuminate\Database\Seeder;
use Stillat\Database\Tenant\TenantManager;

class NotAvailableSeeder extends Seeder
{
    const NOT_AVAILABLE_CODE = "NA";
    const NOT_AVAILABLE_NAME = "Not Available";
    const NOT_AVAILABLE_DESC = "The requested information is not available for this record, or does not apply.";

    public function run()
    {
        DB::connection('mysql')->statement('SET SESSION sql_mode=\'NO_AUTO_VALUE_ON_ZERO\'');

        if (defined('IS_INSTALLATION')) {
            DB::connection('mysql')->table('accounts')->insert(array(
                                                                   'id'           => 0,
                                                                   'account_name' => 'Global Settings',
                                                                   'tenant_id'    => 0
                                                               ));
        }


        $manager = TenantManager::instance();

        DB::connection($manager->getCurrentConnection())->statement('SET SESSION sql_mode=\'NO_AUTO_VALUE_ON_ZERO\'');

        $genericRecord = array(
            'id'          => 0,
            'name'        => self::NOT_AVAILABLE_NAME,
            'code'        => self::NOT_AVAILABLE_CODE,
            'description' => self::NOT_AVAILABLE_NAME
        );

        $consumableRecord = array_merge(
            $genericRecord, array(
                              'unit_id' => 9
                          )
        );

        DB::connection($manager->getCurrentConnection())->table('activities')->insert($genericRecord);
        DB::connection($manager->getCurrentConnection())->table('departments')->insert($genericRecord);
        DB::connection($manager->getCurrentConnection())->table('districts')->insert($genericRecord);

        DB::connection($manager->getCurrentConnection())
          ->table('consumables')->insert([
                                             [
                                                 'id'                 => 1,
                                                 'code'               => 'NA',
                                                 'name'               => 'Not Available',
                                                 'description'        => 'Not Available',
                                                 'cost'               => 0,
                                                 'unit_id'            => 9,
                                                 'tracking_inventory' => false,
                                                 'consumable_context' => 1
                                             ],
                                             [
                                                 'id'                 => 0,
                                                 'code'               => 'NA',
                                                 'name'               => 'Not Available',
                                                 'description'        => 'Not Available',
                                                 'cost'               => 0,
                                                 'unit_id'            => 9,
                                                 'tracking_inventory' => false,
                                                 'consumable_context' => 2
                                             ]
                                         ]);


        DB::connection($manager->getCurrentConnection())->table('projects')->insert($genericRecord);
        DB::connection($manager->getCurrentConnection())->table('properties')->insert($genericRecord);
        DB::connection($manager->getCurrentConnection())->table('property_types')->insert($genericRecord);
        DB::connection($manager->getCurrentConnection())->table('property_road_surface_types')->insert($genericRecord);

        // Default equipment record(s).
        DB::connection($manager->getCurrentConnection())->table('properties')->insert([
                                                                                          'id'                => 1,
                                                                                          'property_type'     => 2,
                                                                                          'code'              => 'NA - EQUIPMENT',
                                                                                          'name'              => 'Not Available - Equipment',
                                                                                          'description'       => 'Not Available',
                                                                                          'exclusively_owned' => 0
                                                                                      ]);
        DB::connection($manager->getCurrentConnection())->table('equipment_units')->insert([
                                                                                               'property_id'   => 1,
                                                                                               'serial_number' => self::NOT_AVAILABLE_NAME,
                                                                                               'purchase_cost' => 0,
                                                                                               'salvage_value' => 0,
                                                                                               'unit_id'       => 9,
                                                                                               'rental_rate'   => 0,
                                                                                               'lifetime'      => 0

                                                                                           ]);

        DB::connection($manager->getCurrentConnection())->table('property_categories')->insert([
            'id' => 0,
            'category_name' => 'Uncategorized'
                                                                                               ]);

        DB::connection($manager->getCurrentConnection())->table('employees')
          ->insert(array(
                       'id'          => 0,
                       'code'        => self::NOT_AVAILABLE_CODE,
                       'first_name'  => self::NOT_AVAILABLE_CODE,
                       'middle_name' => self::NOT_AVAILABLE_CODE,
                       'last_name'   => self::NOT_AVAILABLE_CODE
                   ));

        // FINANCE ACCOUNTS
        DB::connection($manager->getCurrentConnection())->table('fs_accounts_categories')
          ->insert(array(
                       'id'          => 0,
                       'code'        => 'CASH ACCOUNTS',
                       'name'        => 'CASH ACCOUNTS',
                       'description' => 'These are cash accounts.',
                       'color'       => 'transparent',
                   ));

        DB::connection($manager->getCurrentConnection())->table('fs_accounts')
          ->insert(array(
                       'id'              => 0,
                       'code'            => 'CASH BOOK',
                       'name'            => 'CASH BOOK',
                       'description'     => 'The default system cash book account.',
                       'balance'         => 0.00,
                       'category_id'     => 0,
                       'is_cash_account' => 1
                   ));

        DB::connection($manager->getCurrentConnection())->table('fs_tax_rates')
          ->insert(array(
                       'id'                 => 0,
                       'tax_name'           => 'NONE',
                       'tax_rate'           => 0.0,
                       'affects_all'        => 1,
                       'affects_employees'  => 1,
                       'affects_equipments' => 1,
                       'affects_fuels'      => 1,
                       'affects_materials'  => 1,
                   ));

        // Create the county meta data record.
        DB::connection($manager->getCurrentConnection())->table('county_meta')
          ->insert(array(
                       'id'                             => 0,
                       'record_highest_fuel_count'      => 0,
                       'record_highest_material_count'  => 0,
                       'record_highest_equipment_count' => 0
                   ));

        DB::connection($manager->getCurrentConnection())->table('property_adapters')
          ->insert(array(
                       'id'          => 1,
                       'property_id' => 1,
                       'adapter'     => 1
                   ));

        DB::connection($manager->getCurrentConnection())->table('property_adapters')
          ->insert(array(
                       'id'          => 2,
                       'property_id' => 2,
                       'adapter'     => 2
                   ));

        DB::connection($manager->getCurrentConnection())->table('property_types')
          ->insert(array(
                       'id'          => 1,
                       'code'        => 'ROAD',
                       'name'        => 'ROAD',
                       'description' => 'The default road property type.'
                   ));

        DB::connection($manager->getCurrentConnection())->table('property_types')
          ->insert(array(
                       'id'          => 2,
                       'code'        => 'EQUIPMENT UNIT',
                       'name'        => 'EQUIPMENT UNIT',
                       'description' => 'The default equipment unit property type.'
                   ));
    }
}
