<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePropertyRoadPropertiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('property_road_properties', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('road_id')->unsigned();
            $table->integer('surface_type_id')->unsigned()->default(0);
            $table->decimal('road_length', 16, 6)->default(0);
            $table->integer('unit_id')->unsigned()->default(0);

            $table->index('road_id');
            $table->index('surface_type_id');
            $table->index('unit_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('property_road_properties');
    }
}
