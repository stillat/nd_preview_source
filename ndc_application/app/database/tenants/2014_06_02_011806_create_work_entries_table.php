<?php

use Carbon\Carbon;
use Illuminate\Support\Facades\Lang;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWorkEntriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('work_entries', function (Blueprint $table) {
            $table->increments('id');

            // Identification information.

            $table->dateTime('work_date')->default(Carbon::now());

            $table->integer('employee_id')->unsigned()->default(0);
            $table->integer('activity_id')->unsigned()->default(0);
            $table->integer('project_id')->unsigned()->default(0);
            $table->integer('department_id')->unsigned()->default(0);
            $table->integer('district_id')->unsigned()->default(0);

            $table->tinyInteger('property_context')->unsigned()->default(0);

            $table->integer('property_id')->unsigned()->default(0);

            // Activity-centric values.
            $table->string('activity_description', 6553)->default(Lang::get('system.not_available'));

            // Employee-centric values.
            $table->decimal('employee_hours_regular_worked', 16, 6)->default(0);
            $table->decimal('employee_hours_overtime_worked', 16, 6)->default(0);
            
            // Historic employee data.
            $table->decimal('employee_historic_wage_regular', 16, 6)->default(0);
            $table->decimal('employee_historic_wage_overtime', 16, 6)->default(0);
            $table->decimal('employee_historic_fringe_regular', 16, 6)->default(0);
            $table->decimal('employee_historic_fringe_overtime', 16, 6)->default(0);

            // Miscellaneous Billing
            $table->decimal('misc_billing_quantity', 16, 6)->default(0);
            $table->decimal('misc_billing_rate', 16, 6)->default(0);
            $table->integer('misc_unit_id')->unsigned()->default(0);
            $table->string('misc_description', 6553)->default(Lang::get('system.not_available'));

            // Calculated values.
            $table->decimal('total_materials_quantity', 16, 6)->default(0);
            $table->decimal('total_equipment_units_quantity', 16, 6)->default(0);
            $table->decimal('total_fuels_quantity', 16, 6)->default(0);

            $table->decimal('total_materials_cost', 16, 6)->default(0);
            $table->decimal('total_equipment_units_cost', 16, 6)->default(0);
            $table->decimal('total_fuels_cost', 16, 6)->default(0);
            $table->decimal('total_equipment_and_fuels_cost', 16, 6)->default(0);

            $table->decimal('total_misc_billing_cost', 16, 6)->default(0);
            $table->decimal('total_property_cost', 16, 6)->default(0);

            $table->decimal('total_employee_regular_cost', 16, 6)->default(0);
            $table->decimal('total_employee_overtime_cost', 16, 6)->default(0);
            $table->decimal('total_employee_combined_cost', 16, 6)->default(0);

            $table->tinyInteger('record_status')->unsigned()->default(1);
            $table->boolean('exclude_from_report')->default(false);
            
            // Date settings.
            $table->timestamps();
            $table->softdeletes();

            /**
            // Indexes
            $table->index('work_date');
            $table->index('employee_id');
            $table->index('activity_id');
            $table->index('project_id');
            $table->index('department_id');
            $table->index('district_id');
            $table->index('property_id');

            $table->index('created_at');
            $table->index('updated_at');
             * */

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('work_entries');
    }
}
