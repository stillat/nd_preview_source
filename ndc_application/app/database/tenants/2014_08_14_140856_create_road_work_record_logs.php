<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateRoadWorkRecordLogs extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('road_work_record_logs', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('work_record_id')->unsigned();

            $table->integer('surface_type_id')->unsigned()->default(0);
            $table->decimal('road_length', 16, 6)->default(0);
            $table->decimal('unit_id')->unsigned()->default(0);

            $table->index('surface_type_id');
            $table->index('unit_id');
            $table->index('work_record_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('road_work_record_logs');
    }
}
