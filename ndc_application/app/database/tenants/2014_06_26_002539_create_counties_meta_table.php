<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCountiesMetaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('county_meta', function (Blueprint $table) {
            $table->increments('id');
            $table->smallInteger('record_highest_fuel_count')->default(0);
            $table->smallInteger('record_highest_material_count')->default(0);
            $table->smallInteger('record_highest_equipment_count')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('county_meta');
    }
}
