<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEquipmentWorkRecordRepairs extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('equipment_work_record_repairs', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('work_record_id')->unsigned();
            $table->string('work_order_number', 500)->default('');
            $table->decimal('shop_costs', 16, 6)->default(0);
            $table->decimal('purchased_costs', 16, 6)->default(0);
            $table->decimal('commercial_costs', 16, 6)->default(0);
            $table->decimal('commercial_labor_costs', 16, 6)->default(0);
            $table->string('work_description', 6553)->default('');
            $table->index('work_record_id');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('equipment_work_record_repairs');
    }
}
