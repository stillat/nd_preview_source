<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmployeesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employees', function (Blueprint $table) {
            $table->increments('id');

            // Identifying properties.
            $table->string('first_name');
            $table->string('middle_name');
            $table->string('last_name');
            $table->string('code');
            $table->integer('gender')->default(0)->unisgned();
            $table->dateTime('hire_date')->nullable();
            $table->dateTime('termination_date')->nullable();
            
            $table->string('profile_image')->default('assets/img/profile.jpg');
            $table->string('large_image')->default('assets/img/profile.jpg');

            // Financial properties.
            $table->decimal('wage', 16, 6)->default(0.00);
            $table->decimal('overtime', 16, 6)->default(0.0);
            $table->decimal('wage_benefit', 16, 6)->default(0.00);
            $table->decimal('overtime_benefit', 16, 6)->default(0.00);

            $table->index('code');

            // Additional properties.
            $table->softdeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('employees');
    }
}
