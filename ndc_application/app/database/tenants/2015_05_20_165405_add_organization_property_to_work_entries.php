<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class AddOrganizationPropertyToWorkEntries extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('work_entries', function (Blueprint $table) {
            $table->integer('property_organization_id')->unsigned()->default(0);
            $table->tinyInteger('property_organization_context')->unsigned()->default(0);
           // $table->index('property_organization_id', 'property_organization_id_index');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('work_entries', function (Blueprint $table) {
            $table->dropColumn('property_organization_id');
            $table->dropColumn('property_organization_context');
           // $table->dropIndex('property_organization_id_index');
        });
    }
}
