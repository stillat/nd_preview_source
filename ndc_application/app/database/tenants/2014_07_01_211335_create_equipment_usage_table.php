<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEquipmentUsageTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('equipment_usage', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('equipment_id')->unsigned();
            $table->decimal('repair_cost', 17, 6)->default(0);
            $table->decimal('fuel_cost', 17, 6)->default(0);
            $table->decimal('hour_miles', 17, 6)->default(0);
            $table->decimal('rental_cost', 17, 6)->default(0);
            $table->index('equipment_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('equipment_usage');
    }
}
