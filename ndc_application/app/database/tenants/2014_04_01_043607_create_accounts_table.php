<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAccountsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fs_accounts', function (Blueprint $table) {
            $table->increments('id');
            $table->string('code');
            $table->string('name');
            $table->string('description')->default('');
            $table->decimal('balance', 20, 4)->default(0);
            $table->integer('category_id')->unsigned();
            $table->boolean('is_cash_account')->default(false);
            $table->boolean('is_operation_account')->default(false);
            $table->index('category_id');
            $table->index('code');
            $table->index('name');
            $table->index('is_cash_account');

            // Contact information.
            $table->string('address_line_1')->default('');
            $table->string('address_line_2')->default('');
            $table->string('city')->default('');
            $table->string('state')->default('');
            $table->string('country')->default('');
            $table->string('zip')->default('');

            $table->timestamps();
            $table->softdeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('fs_accounts');
    }
}
