<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInvoicesWorkRecords extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fs_invoice_work_records', function (Blueprint $table) {
            $table->integer('invoice_id');
            $table->integer('work_record_id');

            $table->index('invoice_id');
            $table->index('work_record_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('fs_invoice_work_records');
    }
}
