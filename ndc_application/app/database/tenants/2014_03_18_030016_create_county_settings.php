<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCountySettings extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     *
     */
    public function up()
    {
        Schema::create('county_settings', function (Blueprint $table) {
            $table->increments('id');
            $table->boolean('use_districts')->default(false);
            $table->boolean('show_employee_overtime')->default(true);
            $table->integer('data_decimal_places')->default(4);
            $table->tinyInteger('invoice_default_status')->unsigned()->default(1);
            $table->boolean('auto_process_invoice')->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('county_settings');
    }
}
