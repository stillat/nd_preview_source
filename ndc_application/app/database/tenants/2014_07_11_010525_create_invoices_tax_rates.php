<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInvoicesTaxRates extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fs_invoices_tax_rates', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->integer('tax_id')->unsigned()->default(0);
            $table->integer('invoice_id')->unsigned();

            $table->decimal('historic_tax_rate', 16, 6)->default(0.0);

            $table->decimal('tax_total', 16, 6)->default(0.0);

            $table->decimal('employee_total', 16, 6)->default(0.0);
            $table->decimal('fuel_total', 16, 6)->default(0.0);
            $table->decimal('equipment_total', 16, 6)->default(0.0);
            $table->decimal('material_total', 16, 6)->default(0.0);

            $table->index('tax_id');
            $table->index('invoice_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('fs_invoices_tax_rates');
    }
}
