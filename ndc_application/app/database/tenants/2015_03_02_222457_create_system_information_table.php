<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSystemInformationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('system_information_logs', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('backup_upload_limits')->default(2);
            $table->integer('backup_create_limits')->default(4);
            $table->integer('backup_restore_backups')->default(4);
            $table->integer('database_version')->default(0);
            $table->dateTime('configured_on');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('system_information_logs');
    }
}
