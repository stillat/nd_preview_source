<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNewIndexesOnEntries extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('work_entries', function (Blueprint $table) {
            $table->index(['activity_id', 'employee_id', 'project_id', 'department_id', 'district_id'], 'ACTIVITY');
            $table->index(['work_date', 'exclude_from_report'], 'WORK_DATE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('work_entries', function (Blueprint $table) {
            $table->dropIndex('ACTIVITY');
            $table->dropIndex('WORK_DATE');
        });
    }
}
