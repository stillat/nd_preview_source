<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePhoneNumbersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('phone_numbers', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('number_type')->unsigned()->default(1);
            $table->string('phone_number');
            
            $table->integer('numberable_id')->unsigned();
            $table->string('numberable_type');

            $table->index('numberable_type');
            $table->index('numberable_id');
            $table->index('number_type');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('phone_numbers');
    }
}
