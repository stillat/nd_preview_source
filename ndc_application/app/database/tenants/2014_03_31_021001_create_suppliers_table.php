<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSuppliersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fs_suppliers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->unique();
            $table->boolean('active')->default(1);
            $table->string('code')->unique();
            $table->integer('primary_address_id')->unsigned()->default(0);
            $table->integer('primary_phone_id')->unsigned()->default(0);

            $table->string('thumbnail_image')->default('assets/img/profile.jpg');
            $table->string('large_image')->default('assets/img/profile.jpg');

            // Indexes.
            $table->index('name');
            $table->index('active');
            $table->index('primary_address_id');
            $table->index('primary_phone_id');
            $table->index('code');

            // Additional.
            $table->timestamps();
            $table->softdeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('fs_suppliers');
    }
}
