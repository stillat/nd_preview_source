<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class AddPerformanceIndexes extends Migration
{
    protected $indexesToCreate = [
      'work_entries' => ['employee_id', 'activity_id', 'project_id', 'department_id', 'district_id', 'property_id', 'id'],
      'work_consumables' => ['consumable_type', 'consumable_id', 'associated_type', 'associated_id', 'bound_consumable_record', 'work_entry_id', 'deleted_at'],
      'properties' => ['code', 'id', 'property_type'],
      'consumables' => ['consumable_context', 'code', 'id'],
      'employees' => ['code', 'id'],
      'districts' => ['code', 'id'],
      'departments' => ['code', 'id'],
      'activities' => ['code', 'id'],
      'projects' => ['code', 'id']
    ];

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        /**
        foreach ($this->indexesToCreate as $tableToIndex => $columns)
        {
            Schema::table($tableToIndex, function(Blueprint $table) use ($tableToIndex, $columns)
            {
                $table->index($columns, 'perf15_'.$tableToIndex);
            });
        }
         */
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        /**
        foreach ($this->indexesToCreate as $tableToIndex => $columns)
        {
            Schema::table($tableToIndex, function(Blueprint $table) use ($tableToIndex)
            {
               $table->dropIndex('perf15_'.$tableToIndex);
            });
        }
         */
    }
}
