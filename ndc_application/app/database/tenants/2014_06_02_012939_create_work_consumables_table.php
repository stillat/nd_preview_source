<?php

use Illuminate\Support\Facades\Lang;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWorkConsumablesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('work_consumables', function (Blueprint $table) {
            $table->increments('id');

            // Identifying information.
            $table->string('consumable_type');
            $table->integer('consumable_id')->unsigned()->default(0);

            // Associated data
            $table->string('associated_type');
            $table->integer('associated_id')->unsigned()->default(0);

            $table->integer('bound_consumable_record')->unsigned()->default(0);

            // Reference information.
            $table->integer('work_entry_id')->unsigned()->default(0);

            // Unit information.
            $table->integer('measurement_unit_id')->unsigned()->default(9);

            // Trackables.
            $table->decimal('total_cost', 16, 6)->default(0);
            $table->decimal('total_quantity', 16, 6)->default(0);

            // Historic purposes.
            $table->decimal('historic_cost', 16, 6)->default(0);

            // Date settings.
            $table->timestamps();
            $table->softdeletes();

            // Taxation.
            $table->boolean('taxable')->default(false);
            $table->decimal('total_tax', 16, 6)->default(0);

            // Indexes
            $table->index('consumable_type');
            $table->index('consumable_id');

            $table->index('associated_type');
            $table->index('associated_id');

            $table->index('work_entry_id');

            $table->index('taxable');

            // Foreign key stuff.
            $table->foreign('work_entry_id')->references('id')->on('work_entries')->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('work_consumables');
    }
}
