<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInventoryAdjustmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('inventory_adjustments', function (Blueprint $table) {
            $table->increments('id');
            $table->dateTime('date_adjusted');
            $table->integer('adjusted_by')->unsigned();
            $table->integer('consumable_id')->unsigned();
            $table->integer('adjustment_context')->unsigned()->default(0);
            $table->integer('adjustment_value')->unsigned()->default(0);
            $table->decimal('adjustment_amount', 16, 6)->default(0);

            $table->index('adjusted_by');
            $table->index('consumable_id');
            $table->index('adjustment_value');
            $table->index('adjustment_context');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('inventory_adjustments');
    }
}
