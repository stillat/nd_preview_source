<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateEquipmentUnits extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('equipment_units', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('property_id')->unsigned();

            $table->integer('year')->default(0)->nullable();
            $table->string('make')->default('');
            $table->string('model')->default('');
            $table->string('serial_number')->default('');
            $table->decimal('rental_rate', 16, 6)->default(0.00);
            $table->integer('unit_id');
            $table->decimal('purchase_cost', 16, 6)->default(0.00);
            $table->decimal('salvage_value', 16, 6)->default(0.00);
            $table->float('lifetime')->default(10)->nullable();

            $table->index('property_id');
            $table->index('unit_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('equipment_units');
    }
}
