<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateConsumablesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('consumables', function (Blueprint $table) {
            $table->increments('id');

            // Identifying information.
            $table->string('code');
            $table->string('name');
            $table->string('description', 6553);

            $table->integer('unit_id')->unsigned()->default(9);
            $table->tinyInteger('consumable_context')->unsigned();
            $table->decimal('cost', 16, 6)->default(0.00);

            // Inventory stuff.
            $table->boolean('tracking_inventory')->default(false); // Indicates if the system is tracking inventory levels on this consumable.
            $table->decimal('desired_inventory_level', 16, 6)->default(0);
            $table->decimal('critical_inventory_level', 16, 6)->default(0);
            $table->decimal('current_inventory_level', 16, 6)->default(0);

            // Index stuff.
            $table->index('unit_id');
            $table->index('code');
            $table->index('consumable_context');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('consumables');
    }
}
