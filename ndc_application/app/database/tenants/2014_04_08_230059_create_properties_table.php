<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePropertiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('properties', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('property_type')->unsigned();
            $table->string('code');
            $table->string('description', 6553);
            $table->string('name');
            $table->integer('category_id')->unsigned()->default(0);

            // Exclusively owned properties should not appear in
            // standard queries and listings.
            $table->boolean('exclusively_owned')->default(false);

            $table->index('code');
            $table->index('category_id');
            $table->index('property_type');
            $table->softdeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
