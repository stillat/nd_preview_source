<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateAccountTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fs_transactions', function (Blueprint $table) {
            $table->increments('id');
            $table->string('batch');
            $table->integer('begin_account')->unsigned();
            $table->integer('end_account')->unsigned();
            $table->decimal('amount', 20, 6)->default(0);
            $table->integer('authorized_by')->unsigned();
            $table->integer('work_record_id')->unsigned();
            $table->integer('transaction_status')->unsigned()->default(0);
            $table->integer('associated_context')->default(0);
            $table->integer('associated_value')->default(0);
            $table->string('description', 6553)->default('');

            $table->boolean('is_primary_transaction')->default(false);

            $table->index('begin_account');
            $table->index('end_account');
            $table->index('work_record_id');
            $table->index('associated_value');
            $table->timestamps();
            $table->softdeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('fs_transactions');
    }
}
