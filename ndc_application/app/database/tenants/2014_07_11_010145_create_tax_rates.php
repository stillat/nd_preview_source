<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTaxRates extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fs_tax_rates', function (Blueprint $table) {
            $table->increments('id');
            
            $table->string('tax_name');
            $table->decimal('tax_rate', 16, 6);

            $table->boolean('affects_all')->default(false);
            $table->boolean('affects_employees')->default(false);
            $table->boolean('affects_equipments')->default(true);
            $table->boolean('affects_fuels')->default(true);
            $table->boolean('affects_materials')->default(true);

            $table->index('tax_name');

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('fs_tax_rates');
    }
}
