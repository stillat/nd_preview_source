<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInvoicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fs_invoices', function (Blueprint $table) {
            $table->increments('id');
            $table->datetime('due_date')->default(Carbon::now());
            $table->string('invoice_number', 200)->unique();
            $table->decimal('discount', 16, 6)->default(0.000);

            $table->integer('account_number')->unsigned();
            $table->integer('paid_to_account')->unsigned()->default(0);
            $table->dateTime('invoice_start')->nullable();
            $table->dateTime('invoice_end')->nullable();
            /**
             * The only reason these are defined here are because an invoice
             * can have multiple work records, so we cannot rely on the individual
             * work records for their totals. These are aggregated for future performance.
             */
            $table->decimal('invoice_total', 16, 6)->default(0.0);
            $table->decimal('invoice_total_materials', 16, 6)->default(0.0);
            $table->decimal('invoice_total_equipment_units', 16, 6)->default(0.0);
            $table->decimal('invoice_total_employee_costs', 16, 6)->default(0.0);
            $table->decimal('invoice_total_fuels', 16, 6)->default(0.0);
            $table->decimal('invoice_tax_amount', 16, 6)->default(0.0);
            $table->decimal('invoice_sub_total', 16, 6)->default(0.0);
            $table->decimal('invoice_discount_total', 16, 6)->default(0.0);
            $table->decimal('cached_invoice_balance', 16, 6)->default(0.0);
            $table->boolean('is_closed')->default(false);

            $table->tinyInteger('invoice_status')->unsigned()->default(0);

            $table->text('client_notes');
            $table->text('invoice_terms');

            $table->index('account_number');
            $table->index('paid_to_account');
            $table->index('due_date');
            $table->index('invoice_number');
            $table->index('invoice_status');
            $table->index('is_closed');
            $table->index('invoice_start');
            $table->index('invoice_end');

            $table->timestamps();
            $table->softdeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('fs_invoices');
    }
}
