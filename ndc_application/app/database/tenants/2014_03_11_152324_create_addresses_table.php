<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAddressesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('addresses', function (Blueprint $table) {
            $table->increments('id');
            $table->string('address_line_1');
            $table->string('address_line_2')->default('');
            $table->string('city');
            $table->string('zip_code');
            $table->string('state');
            $table->string('country');

            $table->integer('addressable_id')->unsigned();
            $table->string('addressable_type');

            $table->index('addressable_id');
            $table->index('addressable_type');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('addresses');
    }
}
