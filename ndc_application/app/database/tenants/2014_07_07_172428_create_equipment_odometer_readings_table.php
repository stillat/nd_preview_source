<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEquipmentOdometerReadingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('equipment_odometer_readings', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('equipment_id')->unsigned();
            $table->integer('odometer_reading')->unsigned();
            $table->integer('associated_record')->unsigned();
            $table->integer('associated_record_type')->unsigned();

            $table->index('associated_record_type');
            $table->index('associated_record');
            $table->index('equipment_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('equipment_odometer_readings');
    }
}
