<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePropertiesPropertiesPivot extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('property_property', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('parent_property_id')->unsigned();
            $table->integer('child_property_id')->unsigned();

            $table->index('parent_property_id');
            $table->index('child_property_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('property_property');
    }
}
