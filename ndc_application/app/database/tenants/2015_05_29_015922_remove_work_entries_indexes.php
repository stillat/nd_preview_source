<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class RemoveWorkEntriesIndexes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('work_entries', function (Blueprint $table) {
            /**
            * $table->dropIndex('work_entries_work_date_index');
             * $table->dropIndex('work_entries_employee_id_index');
             * $table->dropIndex('work_entries_work_date_index');
             * $table->dropIndex('work_entries_activity_id_index');
            * $table->dropIndex('work_entries_project_id_index');
            * $table->dropIndex('work_entries_department_id_index');
            * $table->dropIndex('work_entries_district_id_index');
            * $table->dropIndex('work_entries_property_id_index');
            * $table->dropIndex('perf15_work_entries');
            * $table->dropIndex('property_organization_id_index');
            */
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
