<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePropertyRoadSurfaceTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('property_road_surface_types', function (Blueprint $table) {
            $table->increments('id');
            $table->string('code');

            $table->string('name');
            $table->string('description', 6553);

            $table->index('code');

            $table->softdeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('property_road_surface_types');
    }
}
