<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUserTableSortSettings extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_application_settings', function (Blueprint $table) {
            $table->increments('id');
            $table->string('subsystem');
            $table->string('setting_name');
            $table->integer('user_id')->unsigned();
            $table->text('setting_value');
            $table->index('setting_name');
            $table->index('subsystem');
            $table->index('user_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('user_table_sort_settings');
    }
}
