<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSystemReportsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('system_reports', function (Blueprint $table) {
            $table->increments('id');
            $table->string('report_name');
            $table->string('report_description');
            $table->string('report_icon', 100)->default('ndc-chart-pie');
            $table->string('report_uid');
            $table->string('report_extends_uid');


            $table->integer('report_category')->default(0);
            $table->integer('account_id')->unsigned()->default(0);

            $table->text('srl_header')->nullable();
            $table->text('srl_body')->nullable();
            $table->text('srl_footer')->nullable();
            $table->text('srl_settings')->nullable();

            $table->index('report_uid');
            $table->index('report_extends_uid');
            $table->index('report_category');
            $table->index('account_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('system_reports');
    }
}
