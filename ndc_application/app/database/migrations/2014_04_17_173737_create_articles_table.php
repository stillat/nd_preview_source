<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateArticlesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('articles', function (Blueprint $table) {
            $table->increments('id');

            $table->engine = 'MyISAM';

            // Meta
            $table->string('slug')->unique();
            $table->string('title');
            $table->string('description');
            $table->string('keywords');
            $table->timestamps();
            $table->softdeletes();

            // Content
            $table->text('content_markdown');
            $table->text('content_html');

            // Indexes
            $table->index('slug');

            // NL Full text Search Index.

        });

        DB::statement('ALTER TABLE articles ADD FULLTEXT search(title, content_markdown, description, keywords)');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // Drops NL Full text Search Index.
        Schema::table('articles', function ($table) {
            $table->dropIndex('search');
        });

        Schema::drop('articles');
    }
}
