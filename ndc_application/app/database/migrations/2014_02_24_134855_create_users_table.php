<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('email')->unique();
            $table->string('password')->length(100);
            $table->string('secret')->length(100)->nullable()->default(null);
            $table->string('first_name');
            $table->string('last_name');
            $table->boolean('admin')->default(false);
            $table->dateTime('last_login')->default(Carbon::now());
            $table->dateTime('last_seen')->nullable();
            $table->string('profile_image')->default('assets/img/profile.jpg');

            $table->string('remember_token', 100)->nullable();
            $table->integer('last_tenant')->default(0)->unsigned();

            $table->softdeletes();
            $table->timestamps();
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('users');
    }
}
