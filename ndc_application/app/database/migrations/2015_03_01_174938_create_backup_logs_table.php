<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBackupLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('backup_logs', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('tenant_id')->unsigned()->default(0);
            $table->integer('created_by')->unsigned()->default(0);
            $table->string('backup_name');
            $table->integer('status')->default(0);
            $table->integer('created_by_admin')->unsigned()->default(0);
            $table->dateTime('created_on');
            $table->index('tenant_id');
            $table->index('created_on');
            $table->index('created_by');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('backup_logs');
    }
}
