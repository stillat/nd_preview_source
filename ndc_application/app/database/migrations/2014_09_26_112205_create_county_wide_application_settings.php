<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCountyWideApplicationSettings extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('county_wide_application_settings', function (Blueprint $table) {
            $table->increments('id');
            $table->string('subsystem');
            $table->string('setting_name');
            $table->integer('service_account_id')->unsigned();
            $table->text('setting_value');
            $table->index('setting_name');
            $table->index('subsystem');
            $table->index('service_account_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('county_wide_application_settings');
    }
}
