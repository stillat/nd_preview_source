<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSystemReportsDefaultTotals extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('system_reports_default_totals', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('account_id')->unsigned()->default(0);
            $table->boolean('is_default')->default(0);
            $table->string('total_setting_name');
            $table->text('formula');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('system_reports_default_totals');
    }
}
