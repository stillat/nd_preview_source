<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class CreateSystemReportsTotalBlocks extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('system_reports_total_blocks', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('description', 6553);
            $table->integer('account_id')->unsigned()->default(0);
            $table->boolean('is_default')->default(0);
            $table->text('value');

            $table->index('account_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('system_reports_total_blocks');
    }
}
