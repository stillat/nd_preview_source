<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAuditLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('audit_logs', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('associated_user')->unsigned()->default(0);
            $table->integer('account_id')->unsigned()->default(0);
            $table->integer('action_performed')->unsigned()->default(0);
            $table->integer('action_context')->unsigned()->default(0);
            $table->integer('action_class')->unsigned()->default(0);
            $table->text('action_meta');
            $table->dateTime('performed_on')->default(DB::raw('NOW()'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('audit_logs');
    }
}
