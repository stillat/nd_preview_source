<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTermUpdatesReadersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('required_terms_readers', function(Blueprint $table)
		{
			$table->integer('user_id');
			$table->integer('terms_update_id');
			$table->timestamps();
			$table->softDeletes(); // Deleted_at column will be used for the read date.
			$table->index(['user_id', 'terms_update_id']);
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('required_terms_readers');
	}

}
