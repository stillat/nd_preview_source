<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserSettings extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_settings', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->integer('items_per_page')->default(10)->unsigned();
            $table->integer('form_size')->default(1)->unsigned();
            $table->boolean('auto_expand_more_options')->default(false);
            $table->boolean('auto_clear_forms')->default(false);
            $table->boolean('show_animation_on_form_change')->default(true);
            $table->smallInteger('active_year')->default(with(Carbon::now())->year);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('user_settings');
    }
}
