<?php

$serviceAdminAttributes = ['data' => ['role' => 'sadmin']];

if (Auth::user() !== null and Auth::user()->admin == true) {
    $menu->addItem(url('admin/users'), '<i class="fa fa-user"></i> <span>User Accounts</span>', $serviceAdminAttributes);
    $menu->addItem(url('admin/accounts'), '<i class="fa fa-cloud"></i> <span>Service Accounts</span>', $serviceAdminAttributes);
    $menu->addItem(url('admin/units'), '<i class="fa ndcf-measure"></i> <span>Measurement Units</span>', $serviceAdminAttributes);


    $reportingAdminMenu = Menu::handle('adminReportingTools', '<i class="ndc-chart-pie"></i> <span>Reporting Tools</span>', $serviceAdminAttributes);
    $reportingAdminMenu->addItem(action('Admin\Reports\ReportController@index'), '<i class="ndc-chart-pie"></i> System Reports', $serviceAdminAttributes);
    $reportingAdminMenu->addItem(action('Admin\Reports\EquationsController@index'), '<i class="ndc-math" style="font-width:bold;"></i> Report Equations', $serviceAdminAttributes);


    $menu->addMenu($reportingAdminMenu);
    $menu->addItem(url('admin/database/query'), '<i class="fa fa-database"></i> <span>Query Runner</span>', $serviceAdminAttributes);
}
