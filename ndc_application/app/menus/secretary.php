<?php

$adminAssistantAttributes = ['data' => ['role' => 'assistant']];

$workLogs = Menu::handle('worklogs', '<i class="fa fa-coffee"></i> <span>Work Records</span>', $adminAssistantAttributes);
$workLogs->addItem(route('work-logs.create'), '<i class="fa fa-coffee"></i> Add Work Record', $adminAssistantAttributes);
$workLogs->addItem(route('work-logs.index'), '<i class="fa fa-list"></i> Work Records', $adminAssistantAttributes);
$workLogs->addItem(action('App\Reporting\ReportsController@getRun'), '<i class="ndc-chart-pie"></i> Reporting <small>&amp;</small> Search', $adminAssistantAttributes);
$workLogs->addItem(url('work-logs?search=true'), '<i class="fa fa-search"></i> View Last Search', $adminAssistantAttributes);

$properties = Menu::handle('properties', '<i class="fa fa-home"></i> <span>Property Management</span>', $adminAssistantAttributes);
$properties->addItem(route('properties.index', ['filter' => 0]), '<i class="fa fa-chevron-right"></i> Generic Properties', $adminAssistantAttributes);
$properties->addItem(route('properties.index', ['filter' => 1]), '<i class="fa fa-road"></i> Roads', $adminAssistantAttributes);
$properties->addItem(route('properties.index', ['filter' => 2]), '<i class="fa fa-truck"></i> Equipment Units', $adminAssistantAttributes);
$properties->addItem('#separator_ndc_x1', '<hr style="margin-top: 0px; margin-bottom: 0px;">');
$properties->addItem(route('properties.index'), '<i class="fa fa-chevron-right"></i> All Properties', $adminAssistantAttributes);
$properties->addItem(route('property-categories.index'), '<i class="fa fa-tag"></i> Manage Property Categories', $adminAssistantAttributes);
$properties->addItem(route('road-surface-types.index'), '<i class="fa fa-chevron-right"></i> Manage Road Surface Types', $adminAssistantAttributes);

$resources  = Menu::handle('resources', '<i class="fa fa-cube"></i> <span>Resource Management</span>', $adminAssistantAttributes);
$resources->addItem(route('employees.index'), '<i class="fa fa-user"></i> Employees', $adminAssistantAttributes);
$resources->addItem(route('fuels.index'), '<i class="ndc-menu ndc-fuel"></i> Fuels', $adminAssistantAttributes);
$resources->addItem(route('materials.index'), '<i class="fa fa-cubes"></i> Materials', $adminAssistantAttributes);

$organizationStructure  = Menu::handle('oStruct', '<i class="fa fa-sitemap"></i> <span>Organization Structure</span>', $adminAssistantAttributes);
$organizationStructure->addItem(route('activities.index'), '<i class="fa ndc-bicycle"></i> Activities', $adminAssistantAttributes);
$organizationStructure->addItem(route('departments.index'), '<i class="fa fa-users"></i> Departments', $adminAssistantAttributes);
$organizationStructure->addItem(route('districts.index'), '<i class="fa fa-map-marker district-menu"></i> Districts', $adminAssistantAttributes);
$organizationStructure->addItem(route('projects.index'), '<i class="fa fa-folder-open"></i> Projects', $adminAssistantAttributes);

$menu->addMenu($workLogs);
$menu->addMenu($properties);
$menu->addMenu($resources);
$menu->addMenu($organizationStructure);
