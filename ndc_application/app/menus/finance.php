<?php

$financeAttributes = ['data' => ['role' => 'finance']];

$accountsMenu =
    Menu::handle('fsaccounts', '<i class="fa ndcf-accounts-book"></i> <span>Accounts</span>', $financeAttributes);
$accountsMenu->addItem(url('finances/accounts'), '<i class="fa ndcf-accounts-book"></i> Manage Accounts',
    $financeAttributes);
$accountsMenu->addItem(action('App\Finances\AccountsCategoryController@index'),
    '<i class="fa fa-tag"></i> Manage Account Categories', $financeAttributes);

// This will be used as a placeholder only. Using the attributes to hide it. Nifty :)
$invoices =
    Menu::handle('invoices', '<i class="fa ndcf-bill"></i> <span>Invoices</span>', array('style' => 'display:none;'));
$menu->addMenu($accountsMenu);
$menu->addMenu($invoices);

$menu->addItem(action('App\Finances\Invoices\InvoiceController@index'),
    '<i class="fa ndcf-bill"></i> <span>Invoices</span>', $financeAttributes);
$menu->addItem(action('App\Finances\Invoices\InvoiceController@getSearch'), '<i class="fa fa-search"></i> <span>Search Invoices</span>', $financeAttributes);

//$menu->addItem(url('finances/suppliers'), '<i class="fa fa-shopping-cart"></i> <span>Suppliers</span>');
$menu->addItem(url('finances/tax-rates'), '<i class="fa ndcf-balance-02"></i> <span>Tax Rates</span>',
    $financeAttributes);
