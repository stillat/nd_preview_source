<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class BumpAssetVersionCommand extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'ndcounties:increment-assets';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Increments the asset version number.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $type = $this->argument('type');
        $currentVersion = \UI::$version;
        $currentVersion = str_replace('?v=', '', $currentVersion);
        $versionParts = explode('.', $currentVersion);

        switch ($type) {
            case 'm':
                $versionParts[0] += 1;
                break;
            case 'n':
                $versionParts[1] += 1;
                break;
            case 'p':
                $versionParts[2] += 1;
                break;
        }

        $newVersionString = implode('.', $versionParts);
        $this->info('Version '.$currentVersion.' will be updated to '.$newVersionString);
        $replace = 'public static $version = \'?v='.$currentVersion.'\';';
        $replaceWith = 'public static $version = \'?v='.$newVersionString.'\';';
        $uiBuilderPath = base_path().'\app\ndcounties\ParadoxOne\NDCounties\UI\UIBuilder.php';
        $contents = file_get_contents($uiBuilderPath);
        $contents = str_replace($replace, $replaceWith, $contents);


        try {
            chdir(base_path());
            $this->info(shell_exec('git stash'));
            file_put_contents($uiBuilderPath, $contents);
            $this->info(shell_exec('git add .'));
            $this->info(shell_exec('git commit -m "Incrementing assets from version '.escapeshellcmd($currentVersion).' to '.escapeshellcmd($newVersionString).'" '));
            $this->info(shell_exec('git stash pop'));
        } catch (\Exception $e) {
            $this->error($e->getMessage());
        }
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return array(
            array('type', InputArgument::REQUIRED, 'Version increment type. m for major. n for minor. p for patch'),
        );
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return [];
    }
}
