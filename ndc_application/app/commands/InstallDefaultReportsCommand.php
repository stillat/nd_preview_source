<?php

use Illuminate\Console\Command;
use ParadoxOne\NDCounties\Contracts\Reporting\ReportMetaDefinitionRepositoryInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class InstallDefaultReportsCommand extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'ndcounties:install-reports';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Installs the systems default reports.';

    protected $repo;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(ReportMetaDefinitionRepositoryInterface $metaRepo)
    {
        parent::__construct();
        $this->repo = $metaRepo;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $this->info('Beginning to install system reports...');

        $this->repo->installDefaultReportingSystem();

        $this->info('System reports installed.');
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return array();
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return array();
    }
}
