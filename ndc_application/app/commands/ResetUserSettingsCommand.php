<?php

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class ResetUserSettingsCommand extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'ndcounties:reset-user-settings';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Resets the user settings in a development environment.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        if (App::environment() == 'production') {
            $this->error('This command cannot be run in production. Just saved your job.');
            exit;
        }
        DB::connection('mysql')->statement(DB::raw('TRUNCATE user_application_settings;'));
        $this->info('Truncated user_application_settings');

        DB::connection('mysql')->statement(DB::raw('TRUNCATE user_settings;'));
        $this->info('Truncated user_settings');

        DB::connection('mysql')->statement(DB::raw('ALTER TABLE user_application_settings AUTO_INCREMENT = 1;'));
        $this->info('Reset user_application_settings indexes');

        DB::connection('mysql')->statement(DB::raw('ALTER TABLE user_settings AUTO_INCREMENT = 1;'));
        $this->info('Reset user_settings indexes');
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return array(
        );
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return array(
        );
    }
}
