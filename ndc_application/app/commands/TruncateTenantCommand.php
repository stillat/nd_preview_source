<?php

use Illuminate\Console\Command;
use Illuminate\Support\Facades\App;
use Stillat\Database\Support\Facades\Tenant;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;

class TruncateTenantCommand extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'ndcounties:truncate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Truncates a tenant in non-production environments.';

    protected $truncateTables = [
        'fs_invoice_work_records',
        'equipment_work_record_repairs',
        'equipment_odometer_readings',
        'fs_invoices',
        'fs_invoice_work_records',
        'fs_invoices',
        'fs_invoices_tax_rates',
        'fs_transactions',
        'road_work_record_logs',
        'work_consumables',
        'work_entries'
    ];

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        if (App::environment() == 'production') {
            $this->error('This command cannot be run in production. Just saved your job.');
            exit;
        }

        Tenant::AssumeTenant($this->argument('tenant'));
        DB::connection(Tenant::getCurrentConnection())->statement('SET FOREIGN_KEY_CHECKS = 0;');
        $this->info('Disabling foreign key constraints');
        Eloquent::unguard();

        foreach ($this->truncateTables as $table) {
            DB::connection(Tenant::getCurrentConnection())->statement(DB::raw('TRUNCATE '.$table.';'));
            $this->info('Truncated '.$table.'');

            DB::connection(Tenant::getCurrentConnection())->statement(DB::raw('ALTER TABLE '.$table.' AUTO_INCREMENT = 1;'));
            $this->info('Reset AUTO_INCREMENT index on '.$table);
        }

        DB::connection(Tenant::getCurrentConnection())->statement('SET FOREIGN_KEY_CHECKS = 1;');
        $this->info('Enabling foreign key constraints.');

        Eloquent::reguard();
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return array(
            array('tenant', InputArgument::REQUIRED, 'The tenant ID to truncate.'),
        );
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return array();
    }
}
