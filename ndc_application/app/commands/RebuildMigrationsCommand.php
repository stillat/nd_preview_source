<?php

use Illuminate\Console\Command;
use Illuminate\Database\Schema\Builder;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use Stillat\Database\Tenant\TenantManager;
use Symfony\Component\Console\Input\InputOption;

class RebuildMigrationsCommand extends Command
{
    protected $name = 'ndcounties:rebuild-migrations';

    protected $description = 'Rebuilds the migrations tables on all tenant accounts.';

    public function __construct()
    {
        parent::__construct();
    }

    public function fire()
    {
        /** @var Stillat\Database\Tenant\Migrations\TenantMigrator $migrator */
        $migrator = App::make('stillat.database.tenant.migrator');

        /** @var TenantManager $manager */
        $manager = TenantManager::instance();
        $tenants = $manager->getRepository()->getTenants();
        $files   = $migrator->getMigrationFiles(app_path() . '/database/tenants');

        $rebuildMigrations = [];

        $excludedTenants = [];

        $stopAt = null;

        foreach ($files as $file) {
            $rebuildMigrations[] = ['migration' => $file, 'batch' => 1];

            // Break out of the loop if we need to stop.
            if ($file == $stopAt) {
                break;
            }
        }

        if ($this->option('dry') == null) {
            foreach ($tenants as $tenant) {
                if (!in_array($tenant->tenant_name, $excludedTenants)) {
                    $manager->bootstrapConnectionByTenantName($tenant->tenant_name);
                    $manager->assumeTenant($tenant->id);
                    $connection = $manager->getCurrentConnection();


                    /** @var \Illuminate\Database\Query\Builder $db */
                    $db     = DB::connection($connection)->table('migrations');

                    /** @var Builder $schema */
                    $schema = DB::connection($connection)->getSchemaBuilder();

                    // Drop existing migrations table.
                    if ($schema->hasTable('migrations')) {
                        $schema->dropIfExists('migrations');
                        $this->info('Dropped migrations table on ' . $tenant->tenant_name);
                    }

                    // Create a new migrations table.
                    $schema->create('migrations', function ($table) {
                        $table->string('migration');
                        $table->integer('batch');
                    });

                    $this->info('Created a new migrations table on '.$tenant->tenant_name);
                    $db->insert($rebuildMigrations);
                    $this->info('Inserted '.count($rebuildMigrations).' migration records on '.$tenant->tenant_name);
                }
            }
        } else {
            $this->info('Dry run, no tables affected.');
        }

        if ($this->option('output') !== null) {
            $file = $this->option('output');
            $stub = 'INSERT INTO migrations(migration, batch) VALUES("%s", %d);';
            $content = '';

            foreach ($rebuildMigrations as $migration) {
                $content .= sprintf($stub, $migration['migration'], $migration['batch']);
            }

            file_put_contents($file, $content);
            $this->info('SQL Script written to '.$file);
        }
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return array();
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return array(
            ['dry', 'd', InputOption::VALUE_OPTIONAL, 'If set, no tables will be affected.', null],
            ['output', 'o', InputOption::VALUE_OPTIONAL, 'If set, an SQL script will be generated in the file', null]
        );
    }
}
