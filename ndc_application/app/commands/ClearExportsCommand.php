<?php

use Illuminate\Console\Command;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Support\Facades\App;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class ClearExportsCommand extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'ndcounties:clear-exports';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Clears all user\'s exports.';

    protected $files;

    protected $directory;

    public function __construct(Filesystem $files)
    {
        parent::__construct();
        $this->files = $files;

        $this->directory = storage_path().'/user_exports/';
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        if ($this->files->isDirectory($this->directory)) {
            foreach ($this->files->directories($this->directory) as $directory) {
                if (App::runningInConsole()) {
                    $this->info('Removing directory '.$directory);
                }
                $this->files->deleteDirectory($directory);
            }
        }
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return [];
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return [];
    }
}
