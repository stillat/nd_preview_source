<?php

use App\Account;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class QuickUninstallCommand extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'ndcounties:quick-uninstall';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Quickly uninstalls the system.';

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        if (App::environment() == 'production') {
            $this->error('This command cannot be run in production. Just saved your job.');
            exit;
        }

        try {
            $mainDatabase = Config::get('database.connections.'.Config::get('database.default').'.database');
            $accounts = DB::table('tenants')->select('tenant_name')->get();
            $dumps  = [];

            foreach ($accounts as $account) {
                $dumps[] = 'DROP DATABASE '.$account->tenant_name.';';
            }
            $dumps[] = 'DROP DATABASE '.$mainDatabase.';';
            $dumps[] = 'CREATE SCHEMA '.$mainDatabase.';';
            File::delete(storage_path('meta/ndc_installed'));

            foreach ($dumps as $dump) {
                DB::statement($dump);
            }

            Artisan::call('cache:clear');

            $this->info('System uninstalled');
        } catch (Exception $e) {
            $this->error('An error happened. Check if the system is already uninstalled');
        }
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return array(

        );
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return array(

        );
    }
}
