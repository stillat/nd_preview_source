<?php namespace WorkEntries;

include_once 'WriterTest.php';

class AccessorTest extends \WorkEntries\WriterTest
{
    public function testEmployeeSetWageWorks()
    {
        $this->makeWriter();
        $this->writer->setHistoricWageData(10, 15);
        $this->writer->setHistoricFringeBenefits(10, 15);

        $this->assertEquals(10, $this->writer->getHistoricRegularWage());
        $this->assertEquals(15, $this->writer->getHistoricOvertimeWage());

        $this->assertEquals(10, $this->writer->getHistoricRegularFringeBenefits());
        $this->assertEquals(15, $this->writer->getHistoricOvertimeFringeBenefits());
    }

    public function testSetEmployeeWorks()
    {
        $this->makeWriter();

        $this->writer->setEmployee(10);
        $this->assertEquals(10, $this->writer->getEmployee());


        $this->writer->setEmployee(0);
        $this->assertEquals(0, $this->writer->getEmployee());
    }

    public function testSetActivityWorks()
    {
        $this->makeWriter();

        $this->writer->setActivity(1);

        $this->assertEquals(1, $this->writer->getActivity());
    }

    public function testSetProjectWorks()
    {
        $this->makeWriter();

        $this->writer->setProject(10);

        $this->assertEquals(10, $this->writer->getProject());
    }

    public function testSetDepartmentWorks()
    {
        $this->makeWriter();
        $this->writer->setDepartment(20);
        $this->assertEquals(20, $this->writer->getDepartment());
    }

    public function testSetDistrictWorks()
    {
        $this->makeWriter();
        $this->writer->setDistrict(5);
        $this->assertEquals(5, $this->writer->getDistrict());
    }

    public function testSetPropertyWorks()
    {
        $this->makeWriter();
        $this->writer->setProperty(1);
        $this->assertEquals(1, $this->writer->getProperty());
    }

    public function testSetActivityDescriptionWorks()
    {
        $this->makeWriter();
        $description = "This is a description";

        $this->writer->setActivityDescription($description);

        $this->assertEquals($description, $this->writer->getActivityDescription());
    }

    public function testSetWorkDateWorks()
    {
        $this->makeWriter();

        // Just test a couple of them.
        $this->writer->setWorkDate('6/6/2014');
        $this->assertEquals('2014-06-06 00:00:00', $this->writer->getWorkDate());


        $this->writer->setWorkDate('5/2/2012');
        $this->assertEquals('2012-05-02 00:00:00', $this->writer->getWorkDate());


        $this->writer->setWorkDate('1/1/1995');
        $this->assertEquals('1995-01-01 00:00:00', $this->writer->getWorkDate());

        $this->writer->setWorkDate('8/14/2005');
        $this->assertEquals('2005-08-14 00:00:00', $this->writer->getWorkDate());
    }
}
