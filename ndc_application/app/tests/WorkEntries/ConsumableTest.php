<?php namespace WorkEntries;

include_once 'WriterTest.php';

class ConsumableTest extends \WorkEntries\WriterTest
{
    const UNION_EQUIP = 'ParadoxOne\NDCounties\Contracts\Unions\UnionConsumableEquipmentUnitInterface';
    const UNION_FUEL  = 'ParadoxOne\NDCounties\Contracts\Unions\UnionConsumableFuelInterface';
    const UNION_MATRL = 'ParadoxOne\NDCounties\Contracts\Unions\UnionConsumableMaterialInterface';

    public function testConsumableResolver()
    {
        $this->makeResolver();

        $this->assertEquals(self::UNION_EQUIP, $this->resolver->resolve(2));
        $this->assertEquals(self::UNION_FUEL, $this->resolver->resolve(3));
        $this->assertEquals(self::UNION_MATRL, $this->resolver->resolve(4));
        
        $this->assertEquals(false, $this->resolver->resolve(1));
        $this->assertEquals(false, $this->resolver->resolve(5));
    }

    public function testConsumableIsFunctions()
    {
        $this->makeResolver();

        $consumableEquipment = $this->getMock(self::UNION_EQUIP);
        $consumableFuel      = $this->getMock(self::UNION_FUEL);
        $consumableMaterial  = $this->getMock(self::UNION_MATRL);

        $this->assertEquals(true, $this->resolver->isMaterial($consumableMaterial));
        $this->assertEquals(true, 4);

        $this->assertEquals(true, $this->resolver->isFuel($consumableFuel));
        $this->assertEquals(true, 3);

        $this->assertEquals(true, $this->resolver->isEquipmentUnit($consumableEquipment));
        $this->assertEquals(true, 2);
    }

    public function testAttachConsuamblesAddsItem()
    {
        $this->makeWriter();

        $consumable = $this->getMock('\ParadoxOne\NDCounties\Contracts\WorkEntries\ConsumableRecordInterface');

        $this->writer->attachConsumable($consumable);
        $this->writer->attachConsumable($consumable);
        $this->writer->attachConsumable($consumable);
        $this->writer->attachConsumable($consumable);

        $this->assertCount(4, $this->writer->getConsumables());
    }
}
