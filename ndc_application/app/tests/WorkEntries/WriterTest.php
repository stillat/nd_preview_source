<?php namespace WorkEntries;

class WriterTest extends \TestCase
{
    protected $writer = null;

    protected $resolver = null;

    public function makeWriter()
    {
        if ($this->writer == null) {
            $this->writer = \App::make('ParadoxOne\NDCounties\Database\Repositories\WorkEntries\EloquentWorkEntryWriterRepository');
        }
    }

    public function makeResolver()
    {
        if ($this->resolver == null) {
            $this->resolver = \App::make('ParadoxOne\NDCounties\Contracts\Resolvers\ConsumableTypeResolverInterface');
        }
    }

    public function testGetRidOfWarning()
    {
    }
}
