<?php namespace WorkEntries;

include_once 'WriterTest.php';

class CalculationsTest extends \WorkEntries\WriterTest
{
    public function testRegularCostCalculationWorks()
    {
        $this->makeWriter();

        $this->writer->setHoursWorked(40, 0);
        $this->writer->setHistoricWageData(15, 0);
        $this->writer->setHistoricFringeBenefits(1, 0);
        $this->assertEquals('640.000000', $this->writer->calculateRegularHoursCost());

        // Make sure that the engine doesn't double up values.
        $this->assertEquals('640.000000', $this->writer->calculateRegularHoursCost());
        
        // Make sure the engine preserves information.
        $this->writer->setHistoricWageData(15.23, 0);
        $this->assertEquals('649.200000', $this->writer->calculateRegularHoursCost());

        $this->writer->setHoursWorked(30, 0);
        $this->writer->setHistoricWageData(25.48, 0);
        $this->writer->setHistoricFringeBenefits(2.879, 0);
        $this->assertEquals('850.770000', $this->writer->calculateRegularHoursCost());

        // Just test this with insane decimals.
        $this->writer->setHoursWorked(25.458, 0);
        $this->writer->setHistoricWageData(25.48, 0);
        $this->writer->setHistoricFringeBenefits(2.879, 0);
        $this->assertEquals('721.963422', $this->writer->calculateRegularHoursCost());

        // Test no benefits
        $this->writer->setHoursWorked(40, 0);
        $this->writer->setHistoricWageData(15, 0);
        $this->writer->setHistoricFringeBenefits(0, 0);
        $this->assertEquals('600.000000', $this->writer->calculateRegularHoursCost());
    }

    public function testOvertimeCostCalculationWorks()
    {
        $this->makeWriter();

        $this->writer->setHoursWorked(0, 40);
        $this->writer->setHistoricWageData(0, 15);
        $this->writer->setHistoricFringeBenefits(0, 1);
        $this->assertEquals('640.000000', $this->writer->calculateOvertimeHoursCost());

        // Make sure that the engine doesn't double up values.
        $this->assertEquals('640.000000', $this->writer->calculateOvertimeHoursCost());
        
        // Make sure the engine preserves information.
        $this->writer->setHistoricWageData(0, 15.23);
        $this->assertEquals('649.200000', $this->writer->calculateOvertimeHoursCost());

        $this->writer->setHoursWorked(0, 30);
        $this->writer->setHistoricWageData(0, 25.48);
        $this->writer->setHistoricFringeBenefits(0, 2.879);
        $this->assertEquals('850.770000', $this->writer->calculateOvertimeHoursCost());

        // Just test this with insane decimals.
        $this->writer->setHoursWorked(0, 25.458);
        $this->writer->setHistoricWageData(0, 25.48);
        $this->writer->setHistoricFringeBenefits(0, 2.879);
        $this->assertEquals('721.963422', $this->writer->calculateOvertimeHoursCost());

        // Test no benefits
        $this->writer->setHoursWorked(0, 40);
        $this->writer->setHistoricWageData(0, 15);
        $this->writer->setHistoricFringeBenefits(0, 0);
        $this->assertEquals('600.000000', $this->writer->calculateOvertimeHoursCost());
    }

    public function testRegularOvertimeCalculationWorks()
    {
        $this->makeWriter();

        // If the above tests pass, we can just double them here and make
        // sure this function also doubles. That is sufficient.

        $this->writer->setHoursWorked(40, 40);
        $this->writer->setHistoricWageData(15, 15);
        $this->writer->setHistoricFringeBenefits(1, 1);
        $this->assertEquals('1280.000000', $this->writer->caclulateCombinedHoursCost());
        $this->assertEquals('1280.000000', $this->writer->caclulateCombinedHoursCost());

        $this->writer->setHistoricWageData(15.23, 15.23);
        $this->assertEquals('1298.400000', $this->writer->caclulateCombinedHoursCost());

        $this->writer->setHoursWorked(30, 30);
        $this->writer->setHistoricWageData(25.48, 25.48);
        $this->writer->setHistoricFringeBenefits(2.879, 2.879);
        $this->assertEquals('1701.540000', $this->writer->caclulateCombinedHoursCost());

        // Just test this with insane decimals.
        $this->writer->setHoursWorked(25.458, 25.458);
        $this->writer->setHistoricWageData(25.48, 25.48);
        $this->writer->setHistoricFringeBenefits(2.879, 2.879);
        $this->assertEquals('1443.926844', $this->writer->caclulateCombinedHoursCost());

        // Test no benefits
        $this->writer->setHoursWorked(40, 40);
        $this->writer->setHistoricWageData(15, 15);
        $this->writer->setHistoricFringeBenefits(0, 0);
        $this->assertEquals('1200.000000', $this->writer->caclulateCombinedHoursCost());
    }
}
