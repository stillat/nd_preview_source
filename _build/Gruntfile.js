module.exports = function (grunt) {

    // Project configuration.
    grunt.initConfig({

        //Read the package.json (optional)
        pkg: grunt.file.readJSON('package.json'),

        // Metadata.
        meta: {},

        // Task configuration.
        concat: {
            /**
             * The Application JS
             * ------------------------------
             * Included on every page, includes basic UI interactions and logic.
             */
            concat_application_js: {
                src: [
                    '../public_html/app/assets/js/dep/jquery-1.10.2.min.js',
                    '../public_html/app/assets/js/dep/jquery-ui-1.10.3.min.js',
                    '../public_html/app/assets/js/dep/jquery-migrate-1.2.1.min.js',
                    '../public_html/app/assets/js/dep/bootstrap.min.js',
                    '../public_html/app/assets/js/dep/modernizr.min.js',
                    '../public_html/app/assets/js/dep/jquery.sparkline.min.js',
                    '../public_html/app/assets/js/dep/toggles.min.js',
                    '../public_html/app/assets/js/dep/retina.min.js',
                    '../public_html/app/assets/js/dep/jquery.cookies.js',
                    '../public_html/app/assets/js/flot/flot.min.js',
                    '../public_html/app/assets/js/flot/flot.resize.min.js',
                    '../public_html/app/assets/js/flot/flot.symbol.min.js',
                    '../public_html/app/assets/js/flot/flot.categories.min.js',
                    '../public_html/app/assets/js/flot/flot.pie.min.js',
                    '../public_html/app/assets/js/dep/morris.min.js',
                    '../public_html/app/assets/js/dep/raphael-2.1.0.min.js',
                    '../public_html/app/assets/js/select2/select2.min.js',
                    '../public_html/app/assets/js/dep/readmore.js',
                    '../public_html/app/assets/js/dep/bootstrap-filupload.min.js',
                    '../public_html/app/assets/js/dep/typeahead.js',
                    '../public_html/app/assets/js/dep/mousetrap.min.js',
                    '../public_html/app/assets/js/dep/jquery.autosize.min.js',
                    '../public_html/app/assets/js/dep/jquery.validate.min.js',
                    '../public_html/app/assets/js/validate_extend.js',
                    // Old Application.js here.
                    '../public_html/app/assets/js/app/parts/bootstrap-notify.js',
                    '../public_html/app/assets/js/app/parts/notifications.js',
                    '../public_html/app/assets/js/app/parts/tab_fix.js',
                    '../public_html/app/assets/js/app/parts/key_bindings.js',
                    '../public_html/app/assets/js/app/parts/cookies.js',
                    '../public_html/app/assets/js/app/parts/ui_bindings.js',
                    '../public_html/app/assets/js/app/parts/interactions.js',
                    '../public_html/app/assets/js/app/parts/flying-focus.js',
                    '../public_html/app/assets/js/app/parts/user_menu.js',
                    '../public_html/app/assets/js/app/parts/document_ready.js',
                    '../public_html/app/assets/js/dep/keys.js'
                ],
                dest: '../public_html/app/assets/js/ndcounties_ex.js'
            },
            concat_report_viewer: {
              src: [
                  '../public_html/app/assets/js/reporting/report_view.js'
              ],
                dest: '../public_html/app/assets/js/report_viewer.js'
            },
            /**
             * The Work Parts JavaScript File
             * ------------------------------
             * This javascript file is used for handling
             * interface logic when users are creating/updating work entries.
             */
            concat_work_parts: {
                src: [
                    '../public_html/app/assets/js/dep/moment.2.10.6.js',
                    '../public_html/app/assets/js/work-parts/bootstrap-datetimepicker.js',
                    '../public_html/app/assets/js/dep/offline.js',
                    '../public_html/app/assets/js/dep/rcolor.js',
                    '../public_html/app/assets/js/work-parts/color_management.js',
                    '../public_html/app/assets/js/work-parts/updater/update_setup.js',
                    '../public_html/app/assets/js/work-parts/workentry.js',
                    '../public_html/app/assets/js/work-parts/utils.js',
                    '../public_html/app/assets/js/work-parts/material.js',
                    '../public_html/app/assets/js/work-parts/fuels.js',
                    '../public_html/app/assets/js/work-parts/equip.js',
                    '../public_html/app/assets/js/work-parts/taxes.js',
                    '../public_html/app/assets/js/work-parts/invoice.js',
                    '../public_html/app/assets/js/work-parts/properties.js',
                    '../public_html/app/assets/js/work-parts/resets.js',
                    '../public_html/app/assets/js/work-parts/bindings.js',
                    '../public_html/app/assets/js/dep/jquery.form.min.js',
                    '../public_html/app/assets/js/work-parts/validation.js',
                    '../public_html/app/assets/js/work-parts/adapter_fw.js',
                    '../public_html/app/assets/js/work-parts/updater/property_watchers.js',
                    '../public_html/app/assets/js/work-parts/interaction.js',
                    '../public_html/app/assets/js/work-parts/utilities/urls.js',
                    '../public_html/app/assets/js/work-parts/compatibility.js',
                    '../public_html/app/assets/js/work-parts/datepickers.js',
                    '../public_html/app/assets/js/work-parts/updater/patches.js'
                ],
                dest: '../public_html/app/assets/js/workentry.js'
            },
            /**
             * Properties.JS
             * ------------------------------
             * Includes various utilities for interacting with
             * property adapters.
             */
            concat_properties_js: {
                src: [
                    '../public_html/app/assets/js/properties/ui-bindings.js',
                    '../public_html/app/assets/js/properties/document_ready.js'
                ],
                dest: 'setup_tests/properties.js'
            },
            concat_work_table: {
              src: [
                  '../public_html/app/assets/js/dep/moment.2.10.6.js',
                  '../public_html/app/assets/js/work-parts/bootstrap-datetimepicker.js',
                  '../public_html/app/assets/js/work-table/work_table.js',
                  '../public_html/app/assets/js/work-table/selections.js',
                  '../public_html/app/assets/js/work-table/change_date_spec.js',
              ],
                dest: '../public_html/app/assets/js/work_table.js'
            },
            concat_sorting_system: {
                src: [
                    '../public_html/app/assets/js/reporting/sorting.js',
                    '../public_html/app/assets/js/sorting-system/sorting.js'
                ],
                dest: '../public_html/app/assets/js/sorting_system.js'
            },
            concat_list_management: {
                src: [
                    '../public_html/app/assets/js/list-management/selection.js',
                    '../public_html/app/assets/js/list-management/manager.js'
                ],
                dest: '../public_html/app/assets/js/list_management.min.js'
            },
            /**
             * Report Builder
             * ------------------------------
             * Builds the JavaScript required for the reporting
             * engine selection/builder screen.
             */
            concat_report_builder_js: {
                src: [
                    '../public_html/app/assets/js/reporting/chosen.order.js',
                    '../public_html/app/assets/js/dep/bootbox.js',
                    '../public_html/app/assets/js/reporting/moment.js',
                    '../public_html/app/assets/js/reporting/daterangepicker.js',
                    '../public_html/app/assets/js/reporting/bootstrap-treeview.js',
                    '../public_html/app/assets/js/reporting/settings.js',
                    '../public_html/app/assets/js/reporting/report_select.js',
                    '../public_html/app/assets/js/reporting/link_actions.js',
                    '../public_html/app/assets/js/reporting/filters.js',
                    '../public_html/app/assets/js/reporting/submit.js',
                    '../public_html/app/assets/js/reporting/sorting.js',
                    '../public_html/app/assets/js/reporting/constraints.js',
                    '../public_html/app/assets/js/reporting/save_reports.js',
                    '../public_html/app/assets/js/reporting/spotlight.js',
                    '../public_html/app/assets/js/reporting/live_help.js',
                    '../public_html/app/assets/js/reporting/interaction.js',
                    '../public_html/app/assets/js/reporting/reporting.js'
                ],
                dest: '../public_html/app/assets/js/reporting.js'
            },
            concat_properties_view_js: {
                src: [
                    '../public_html/app/assets/js/properties-view/convert_to_road.js'
                ],
                dest: '../public_html/app/assets/js/properties_view.js'
            }
        },

        uglify: {
            options: {
                compress: {
                    drop_console: true
                }
            },
            my_target: {
                files: {
                    '../public_html/app/assets/js/reporting.min.js': '../public_html/app/assets/js/reporting.js',
                    '../public_html/app/assets/js/workentry.min.js': '../public_html/app/assets/js/workentry.js',
                    '../public_html/app/assets/js/work_table.min.js': '../public_html/app/assets/js/work_table.js',
                    '../public_html/app/assets/js/sorting_system.min.js': '../public_html/app/assets/js/sorting_system.js',
                    '../public_html/app/assets/js/min/ndcounties.min.js': '../public_html/app/assets/js/ndcounties_ex.js',
                    '../public_html/app/assets/js/report_viewer.min.js': '../public_html/app/assets/js/report_viewer.js'
                }
            }
        }
    });

    // These plugins provide necessary tasks.
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-uglify');

    // Default task
    grunt.registerTask('conact_js', ['concat:concat_work_parts', 'concat:concat_application_js', 'concat:concat_report_builder_js', 'concat_work_table', 'concat_sorting_system', 'concat_list_management']);
    grunt.registerTask('default', ['concat', 'uglify']);

};