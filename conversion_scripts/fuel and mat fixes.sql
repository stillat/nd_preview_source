-- Materials

set sql_safe_updates = 0;

UPDATE work_consumables
set consumable_id = 0
where consumable_type = 4
and consumable_id = 1;

-- Fuels

UPDATE work_consumables
set consumable_id = 1
where consumable_type = 3
and consumable_id = 0;

set sql_safe_updates = 1;