-- Barnes County Specific Updates

use ndcounty__genesisobfnackffzmconmaomdncebdafefcffn;
SET SQL_SAFE_UPDATES = 0;

UPDATE departments
SET code = name,
description = name
WHERE id in (14,15,16,17,18,19);

UPDATE work_entries
SET district_id = 2 where district_id = 12;
DELETE FROM districts where id =12;

UPDATE districts SET code = 'MISC' where id = 14;

UPDATE work_entries
SET district_id = 0 where district_id = 13;
DELETE FROM districts where id =13;

UPDATE projects
SET code = name
where length(code) = 36;

-- Employees


UPDATE `employees` SET `last_name`='DUFFY' WHERE `id`='1';
UPDATE `employees` SET `last_name`='TANGEN' WHERE `id`='4';
UPDATE `employees` SET `last_name`='JOHNSON' WHERE `id`='2';
UPDATE `employees` SET `last_name`='WAGNER' WHERE `id`='5';
UPDATE `employees` SET `last_name`='COMPSON' WHERE `id`='16';
UPDATE `employees` SET `last_name`='MCKENNA' WHERE `id`='18';
UPDATE `employees` SET `last_name`='LARSON' WHERE `id`='19';
UPDATE `employees` SET `code`='LOVELL' WHERE `id`='23';
UPDATE `employees` SET `code`='LEE' WHERE `id`='24';
UPDATE `employees` SET `last_name`='DUFFY' WHERE `id`='1';
UPDATE `employees` SET `last_name`='TANGEN' WHERE `id`='4';
UPDATE `employees` SET `last_name`='JOHNSON' WHERE `id`='2';
UPDATE `employees` SET `last_name`='WAGNER' WHERE `id`='5';
UPDATE `employees` SET `last_name`='COMPSON' WHERE `id`='16';
UPDATE `employees` SET `last_name`='MCKENNA' WHERE `id`='18';
UPDATE `employees` SET `last_name`='LARSON' WHERE `id`='19';
UPDATE `employees` SET `code`='LOVELL' WHERE `id`='23';
UPDATE `employees` SET `code`='LEE' WHERE `id`='24';
UPDATE `employees` SET `code`='RIEDESEL' WHERE `id`='25';
UPDATE `employees` SET `code`='SHERLOCK' WHERE `id`='26';
UPDATE `employees` SET `code`='GAASLAND' WHERE `id`='27';
UPDATE `employees` SET `code`='MCMILLAN' WHERE `id`='28';
UPDATE `employees` SET `code`='PETROWITZ' WHERE `id`='29';
UPDATE `employees` SET `code`='HUNTER' WHERE `id`='30';
UPDATE `employees` SET `code`='MAJERUS' WHERE `id`='31';
UPDATE `employees` SET `code`='MCALLISTER' WHERE `id`='32';
UPDATE `employees` SET `code`='BETTEN' WHERE `id`='33';
UPDATE `employees` SET `code`='KAMSTRA' WHERE `id`='34';
UPDATE `employees` SET `code`='CRUFF' WHERE `id`='35';
UPDATE `employees` SET `code`='WINING' WHERE `id`='36';
UPDATE `employees` SET `code`='EARLS' WHERE `id`='37';
UPDATE `employees` SET `code`='WINIGNS' WHERE `id`='38';
UPDATE `employees` SET `code`='MCLAFLIN' WHERE `id`='39';
UPDATE `employees` SET `code`='MCCALFLIN' WHERE `id`='40';


-- Materials is 4
-- Fuels is     3
-- Equips is    2

-- Consumables Fuels Default
UPDATE work_consumables SET consumable_id = 1
WHERE consumable_type = 3
AND consumable_id = 28;

UPDATE work_consumables SET consumable_id = 0
WHERE consumable_type = 4
AND consumable_id = 735;

UPDATE work_consumables SET consumable_id = 574
where id = 201424; -- fixes a record looked up manually in MDMS data.

delete from consumables where id = 735; --  remove weird record


-- PROPERTIE - EQUIPMENTS
-- NEED TO UPDATE equipment_odometer_readings
-- 				  work_entries
--	  			  work_consumables

-- Move original na record to new one
UPDATE equipment_odometer_readings set equipment_id = 1
WHERE equipment_id = 2;

update work_entries set property_id = 1
where property_id = 2 AND property_context = 1;

update work_consumables SET consumable_id = 1
	where consumable_id = 2 and consumable_type = 2;

-- REMOVE 257 NA RECORD
update work_entries SET property_id = 0
	WHERE property_id = 257;

	update work_entries set property_organization_id = 0
		where property_organization_id = 257;

delete from properties where id in(2,257);

	update properties set code = name where id in (99, 100);


		update properties set code = name,
			deleted_at = '0000-00-00 00:00:00'
			where id >= 101 and id <= 124;


UPDATE `properties` SET `code`='0227' WHERE `id`='356';
UPDATE `properties` SET `code`='0227' WHERE `id`='356';
UPDATE `properties` SET `code`='WOODLAND' WHERE `id`='357';
UPDATE `properties` SET `code`='MARYVALE' WHERE `id`='358';
UPDATE `properties` SET `code`='0238' WHERE `id`='359';
UPDATE `properties` SET `code`='0238 LUCCA' WHERE `id`='360';
UPDATE `properties` SET `code`='027N' WHERE `id`='361';
UPDATE `properties` SET `code`='PARKS' WHERE `id`='362';
UPDATE `properties` SET `code`='.' WHERE `id`='363';
UPDATE `properties` SET `code`='103' WHERE `id`='364';
UPDATE `properties` SET `code`='1' WHERE `id`='365';
UPDATE `properties` SET `code`='102' WHERE `id`='366';
UPDATE `properties` SET `code`='0219N' WHERE `id`='367';
UPDATE `properties` SET `code`='0207' WHERE `id`='368';
UPDATE `properties` SET `code`='0221' WHERE `id`='369';
UPDATE `properties` SET `code`='0217' WHERE `id`='370';
UPDATE `properties` SET `code`='CORP MAIN' WHERE `id`='371';
UPDATE `properties` SET `code`='02094S FR' WHERE `id`='372';
UPDATE `properties` SET `code`='0211' WHERE `id`='373';
UPDATE `properties` SET `code`='0208N' WHERE `id`='374';
UPDATE `properties` SET `code`='0232W' WHERE `id`='377';


update work_entries set property_organization_id = 268
	where property_organization_id = 376;


	delete from properties where id = 376;


		update work_entries set property_organization_id = 287
			where property_organization_id = 375;


	delete from properties where id = 375;

SET SQL_SAFE_UPDATES = 1;