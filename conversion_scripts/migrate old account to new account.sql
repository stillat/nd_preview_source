
-- <M> is the first record created in new version
-- <OID> is the old account id
-- <TID> is the new account id
-- <O> is the old account
-- <T> is the new account
-- <H> is the hub database
SET SQL_SAFE_UPDATES = 0;

UPDATE <H>.audit_logs SET account_id = <TID> where account_id = <OID>;
UPDATE <H>.system_reports SET account_id = <TID> where account_id = <OID>;
UPDATE <H>.county_wide_application_settings SET service_account_id = <TID> where service_account_id = <OID>;

UPDATE <H>.users SET last_tenant = <TID> where last_tenant = <OID>;

UPDATE <H>.tenant_accounts SET tenant_id = <TID> where tenant_id = <OID>;

INSERT INTO <T>.work_entries
SELECT * FROM <O>.work_entries
WHERE <O>.work_entries.id >= <M>;

INSERT INTO <T>.work_consumables
SELECT * FROM <O>.work_consumables
WHERE <O>.work_consumables.work_entry_id >= <M>;

INSERT INTO <T>.road_work_record_logs
SELECT * FROM <O>.road_work_record_logs
WHERE <O>.road_work_record_logs.work_record_id >= <M>;

INSERT INTO <T>.equipment_work_record_repairs
SELECT * FROM <O>.equipment_work_record_repairs
WHERE <O>.equipment_work_record_repairs.work_record_id >= <M>;

INSERT INTO <T>.equipment_odometer_readings
SELECT * FROM <O>.equipment_odometer_readings
WHERE <O>.equipment_odometer_readings.associated_record >= <M>;

SET SQL_SAFE_UPDATES = 1;