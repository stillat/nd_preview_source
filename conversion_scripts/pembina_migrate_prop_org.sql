-- ndcounty__genesiskzefdfcecobzemzkokoicqqcmnmbiifb is used as a placeholder for target database schema.
-- ndcounty_miacbdqndocaamezccoozbzaecmicdca is used as a placeholder for the origin database schema.
-- DROP SCHEMA IF EXISTS ndcounty__genesiskzefdfcecobzemzkokoicqqcmnmbiifb; -- Uncomment if you are testing.
CREATE SCHEMA IF NOT EXISTS ndcounty__genesiskzefdfcecobzemzkokoicqqcmnmbiifb;
USE ndcounty__genesiskzefdfcecobzemzkokoicqqcmnmbiifb;

-- CREATE THE TARGET TABLES

CREATE TABLE IF NOT EXISTS `activities` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(6553) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Not available',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `activities_code_index` (`code`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `addresses` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `address_line_1` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `address_line_2` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `city` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `zip_code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `state` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `country` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `addressable_id` int(10) unsigned NOT NULL,
  `addressable_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `addresses_addressable_id_index` (`addressable_id`),
  KEY `addresses_addressable_type_index` (`addressable_type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `consumables` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(6553) COLLATE utf8_unicode_ci NOT NULL,
  `unit_id` int(10) unsigned NOT NULL DEFAULT '9',
  `consumable_context` tinyint(3) unsigned NOT NULL,
  `cost` decimal(16,6) NOT NULL DEFAULT '0.000000',
  `tracking_inventory` tinyint(1) NOT NULL DEFAULT '0',
  `desired_inventory_level` decimal(16,6) NOT NULL DEFAULT '0.000000',
  `critical_inventory_level` decimal(16,6) NOT NULL DEFAULT '0.000000',
  `current_inventory_level` decimal(16,6) NOT NULL DEFAULT '0.000000',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `consumables_unit_id_index` (`unit_id`),
  KEY `consumables_code_index` (`code`),
  KEY `consumables_consumable_context_index` (`consumable_context`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `county_meta` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `record_highest_fuel_count` smallint(6) NOT NULL DEFAULT '0',
  `record_highest_material_count` smallint(6) NOT NULL DEFAULT '0',
  `record_highest_equipment_count` smallint(6) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `county_settings` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `use_districts` tinyint(1) NOT NULL DEFAULT '0',
  `show_employee_overtime` tinyint(1) NOT NULL DEFAULT '1',
  `data_decimal_places` int(11) NOT NULL DEFAULT '4',
  `invoice_default_status` tinyint(3) unsigned NOT NULL DEFAULT '1',
  `auto_process_invoice` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `departments` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(6553) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `departments_code_index` (`code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `districts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(6553) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `districts_code_index` (`code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `employees` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `first_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `middle_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `last_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `gender` int(11) NOT NULL DEFAULT '0',
  `hire_date` datetime DEFAULT NULL,
  `termination_date` datetime DEFAULT NULL,
  `profile_image` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'assets/img/profile.jpg',
  `large_image` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'assets/img/profile.jpg',
  `wage` decimal(16,6) NOT NULL DEFAULT '0.000000',
  `overtime` decimal(16,6) NOT NULL DEFAULT '0.000000',
  `wage_benefit` decimal(16,6) NOT NULL DEFAULT '0.000000',
  `overtime_benefit` decimal(16,6) NOT NULL DEFAULT '0.000000',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `employees_code_index` (`code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `equipment_odometer_readings` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `equipment_id` int(10) unsigned NOT NULL,
  `odometer_reading` int(10) unsigned NOT NULL,
  `associated_record` int(10) unsigned NOT NULL,
  `associated_record_type` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `equipment_odometer_readings_associated_record_type_index` (`associated_record_type`),
  KEY `equipment_odometer_readings_associated_record_index` (`associated_record`),
  KEY `equipment_odometer_readings_equipment_id_index` (`equipment_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `equipment_units` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `property_id` int(10) unsigned NOT NULL,
  `year` int(11) DEFAULT '0',
  `make` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `model` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `serial_number` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `rental_rate` decimal(16,6) NOT NULL DEFAULT '0.000000',
  `unit_id` int(11) NOT NULL,
  `purchase_cost` decimal(16,6) NOT NULL DEFAULT '0.000000',
  `salvage_value` decimal(16,6) NOT NULL DEFAULT '0.000000',
  `lifetime` float(8,2) DEFAULT '10.00',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `equipment_units_property_id_index` (`property_id`),
  KEY `equipment_units_unit_id_index` (`unit_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `equipment_usage` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `equipment_id` int(10) unsigned NOT NULL,
  `repair_cost` decimal(17,6) NOT NULL DEFAULT '0.000000',
  `fuel_cost` decimal(17,6) NOT NULL DEFAULT '0.000000',
  `hour_miles` decimal(17,6) NOT NULL DEFAULT '0.000000',
  `rental_cost` decimal(17,6) NOT NULL DEFAULT '0.000000',
  PRIMARY KEY (`id`),
  KEY `equipment_usage_equipment_id_index` (`equipment_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `equipment_work_record_repairs` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `work_record_id` int(10) unsigned NOT NULL,
  `work_order_number` varchar(500) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `shop_costs` decimal(16,6) NOT NULL DEFAULT '0.000000',
  `purchased_costs` decimal(16,6) NOT NULL DEFAULT '0.000000',
  `commercial_costs` decimal(16,6) NOT NULL DEFAULT '0.000000',
  `commercial_labor_costs` decimal(16,6) NOT NULL DEFAULT '0.000000',
  `work_description` varchar(6553) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `equipment_work_record_repairs_work_record_id_index` (`work_record_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `fs_accounts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `balance` decimal(20,4) NOT NULL DEFAULT '0.0000',
  `category_id` int(10) unsigned NOT NULL,
  `is_cash_account` tinyint(1) NOT NULL DEFAULT '0',
  `is_operation_account` tinyint(1) NOT NULL DEFAULT '0',
  `address_line_1` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `address_line_2` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `city` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `state` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `country` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `zip` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fs_accounts_category_id_index` (`category_id`),
  KEY `fs_accounts_code_index` (`code`),
  KEY `fs_accounts_name_index` (`name`),
  KEY `fs_accounts_is_cash_account_index` (`is_cash_account`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `fs_accounts_categories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `color` varchar(15) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'transparent',
  PRIMARY KEY (`id`),
  KEY `fs_accounts_categories_name_index` (`name`),
  KEY `fs_accounts_categories_code_index` (`code`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `fs_invoice_work_records` (
  `invoice_id` int(11) NOT NULL,
  `work_record_id` int(11) NOT NULL,
  KEY `fs_invoice_work_records_invoice_id_index` (`invoice_id`),
  KEY `fs_invoice_work_records_work_record_id_index` (`work_record_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `fs_invoices` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `due_date` datetime NOT NULL DEFAULT '2015-03-15 20:23:44',
  `invoice_number` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `discount` decimal(16,6) NOT NULL DEFAULT '0.000000',
  `account_number` int(10) unsigned NOT NULL,
  `paid_to_account` int(10) unsigned NOT NULL DEFAULT '0',
  `invoice_total` decimal(16,6) NOT NULL DEFAULT '0.000000',
  `invoice_total_materials` decimal(16,6) NOT NULL DEFAULT '0.000000',
  `invoice_total_equipment_units` decimal(16,6) NOT NULL DEFAULT '0.000000',
  `invoice_total_employee_costs` decimal(16,6) NOT NULL DEFAULT '0.000000',
  `invoice_total_fuels` decimal(16,6) NOT NULL DEFAULT '0.000000',
  `invoice_tax_amount` decimal(16,6) NOT NULL DEFAULT '0.000000',
  `invoice_sub_total` decimal(16,6) NOT NULL DEFAULT '0.000000',
  `invoice_discount_total` decimal(16,6) NOT NULL DEFAULT '0.000000',
  `cached_invoice_balance` decimal(16,6) NOT NULL DEFAULT '0.000000',
  `is_closed` tinyint(1) NOT NULL DEFAULT '0',
  `invoice_status` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `client_notes` text COLLATE utf8_unicode_ci NOT NULL,
  `invoice_terms` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `fs_invoices_invoice_number_unique` (`invoice_number`),
  KEY `fs_invoices_account_number_index` (`account_number`),
  KEY `fs_invoices_paid_to_account_index` (`paid_to_account`),
  KEY `fs_invoices_due_date_index` (`due_date`),
  KEY `fs_invoices_invoice_number_index` (`invoice_number`),
  KEY `fs_invoices_invoice_status_index` (`invoice_status`),
  KEY `fs_invoices_is_closed_index` (`is_closed`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `fs_invoices_tax_rates` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `tax_id` int(10) unsigned NOT NULL DEFAULT '0',
  `invoice_id` int(10) unsigned NOT NULL,
  `historic_tax_rate` decimal(16,6) NOT NULL DEFAULT '0.000000',
  `tax_total` decimal(16,6) NOT NULL DEFAULT '0.000000',
  `employee_total` decimal(16,6) NOT NULL DEFAULT '0.000000',
  `fuel_total` decimal(16,6) NOT NULL DEFAULT '0.000000',
  `equipment_total` decimal(16,6) NOT NULL DEFAULT '0.000000',
  `material_total` decimal(16,6) NOT NULL DEFAULT '0.000000',
  PRIMARY KEY (`id`),
  KEY `fs_invoices_tax_rates_tax_id_index` (`tax_id`),
  KEY `fs_invoices_tax_rates_invoice_id_index` (`invoice_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `fs_supplier_notes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `note` text COLLATE utf8_unicode_ci NOT NULL,
  `supplier_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `fs_supplier_notes_supplier_id_index` (`supplier_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `fs_suppliers` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `primary_address_id` int(10) unsigned NOT NULL DEFAULT '0',
  `primary_phone_id` int(10) unsigned NOT NULL DEFAULT '0',
  `thumbnail_image` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'assets/img/profile.jpg',
  `large_image` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'assets/img/profile.jpg',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `fs_suppliers_name_unique` (`name`),
  UNIQUE KEY `fs_suppliers_code_unique` (`code`),
  KEY `fs_suppliers_name_index` (`name`),
  KEY `fs_suppliers_active_index` (`active`),
  KEY `fs_suppliers_primary_address_id_index` (`primary_address_id`),
  KEY `fs_suppliers_primary_phone_id_index` (`primary_phone_id`),
  KEY `fs_suppliers_code_index` (`code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `fs_tax_rates` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `tax_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `tax_rate` decimal(16,6) NOT NULL,
  `affects_all` tinyint(1) NOT NULL DEFAULT '0',
  `affects_employees` tinyint(1) NOT NULL DEFAULT '0',
  `affects_equipments` tinyint(1) NOT NULL DEFAULT '1',
  `affects_fuels` tinyint(1) NOT NULL DEFAULT '1',
  `affects_materials` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fs_tax_rates_tax_name_index` (`tax_name`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `fs_transactions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `batch` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `begin_account` int(10) unsigned NOT NULL,
  `end_account` int(10) unsigned NOT NULL,
  `amount` decimal(20,6) NOT NULL DEFAULT '0.000000',
  `authorized_by` int(10) unsigned NOT NULL,
  `work_record_id` int(10) unsigned NOT NULL,
  `transaction_status` int(10) unsigned NOT NULL DEFAULT '0',
  `associated_context` int(11) NOT NULL DEFAULT '0',
  `associated_value` int(11) NOT NULL DEFAULT '0',
  `description` varchar(6553) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `is_primary_transaction` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fs_transactions_begin_account_index` (`begin_account`),
  KEY `fs_transactions_end_account_index` (`end_account`),
  KEY `fs_transactions_work_record_id_index` (`work_record_id`),
  KEY `fs_transactions_associated_value_index` (`associated_value`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `fuels` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(6553) COLLATE utf8_unicode_ci NOT NULL,
  `unit_id` int(10) unsigned NOT NULL,
  `cost` decimal(16,6) NOT NULL DEFAULT '0.000000',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `fuels_unit_id_index` (`unit_id`),
  KEY `fuels_code_index` (`code`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `gps_entries` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `latitude` float(8,2) NOT NULL,
  `longitude` float(8,2) NOT NULL,
  `altitude` float(8,2) NOT NULL,
  `date_recorded` datetime NOT NULL,
  `trackable_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `trackable_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `inv_storables` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `desired_inventory_level` decimal(16,4) NOT NULL DEFAULT '0.0000',
  `current_inventory_level` decimal(16,4) NOT NULL DEFAULT '0.0000',
  `context` int(10) unsigned NOT NULL DEFAULT '0',
  `context_value` int(10) unsigned NOT NULL DEFAULT '0',
  `unit_id` int(10) unsigned NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `inv_storables_context_value_index` (`context_value`),
  KEY `inv_storables_unit_id_index` (`unit_id`),
  KEY `inv_storables_context_index` (`context`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `inventory_adjustments` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `date_adjusted` datetime NOT NULL,
  `adjusted_by` int(10) unsigned NOT NULL,
  `consumable_id` int(10) unsigned NOT NULL,
  `adjustment_context` int(10) unsigned NOT NULL DEFAULT '0',
  `adjustment_value` int(10) unsigned NOT NULL DEFAULT '0',
  `adjustment_amount` decimal(16,6) NOT NULL DEFAULT '0.000000',
  PRIMARY KEY (`id`),
  KEY `inventory_adjustments_adjusted_by_index` (`adjusted_by`),
  KEY `inventory_adjustments_consumable_id_index` (`consumable_id`),
  KEY `inventory_adjustments_adjustment_value_index` (`adjustment_value`),
  KEY `inventory_adjustments_adjustment_context_index` (`adjustment_context`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `material_categories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(6553) COLLATE utf8_unicode_ci NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `material_categories_code_unique` (`code`),
  KEY `material_categories_code_index` (`code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `materials` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(6553) COLLATE utf8_unicode_ci NOT NULL,
  `unit_id` int(11) NOT NULL,
  `cost` decimal(17,6) NOT NULL DEFAULT '0.000000',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `materials_unit_id_index` (`unit_id`),
  KEY `materials_code_index` (`code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `phone_numbers` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `number_type` int(10) unsigned NOT NULL DEFAULT '1',
  `phone_number` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `numberable_id` int(10) unsigned NOT NULL,
  `numberable_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `phone_numbers_numberable_type_index` (`numberable_type`),
  KEY `phone_numbers_numberable_id_index` (`numberable_id`),
  KEY `phone_numbers_number_type_index` (`number_type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `projects` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(6553) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `fema_project` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `projects_code_index` (`code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `properties` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `property_type` int(10) unsigned NOT NULL,
  `code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(6553) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `category_id` int(10) unsigned NOT NULL DEFAULT '0',
  `exclusively_owned` tinyint(1) NOT NULL DEFAULT '0',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `properties_code_index` (`code`),
  KEY `properties_category_id_index` (`category_id`),
  KEY `properties_property_type_index` (`property_type`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `property_adapters` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `property_id` int(10) unsigned NOT NULL,
  `adapter` smallint(5) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `property_adapters_property_id_index` (`property_id`),
  KEY `property_adapters_adapter_index` (`adapter`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `property_categories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `category_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `property_categories_category_name_unique` (`category_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `property_property` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `parent_property_id` int(10) unsigned NOT NULL,
  `child_property_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `property_property_parent_property_id_index` (`parent_property_id`),
  KEY `property_property_child_property_id_index` (`child_property_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `property_road_properties` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `road_id` int(10) unsigned NOT NULL,
  `surface_type_id` int(10) unsigned NOT NULL DEFAULT '0',
  `road_length` decimal(16,6) NOT NULL DEFAULT '0.000000',
  `unit_id` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `property_road_properties_road_id_index` (`road_id`),
  KEY `property_road_properties_surface_type_id_index` (`surface_type_id`),
  KEY `property_road_properties_unit_id_index` (`unit_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `property_road_surface_types` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(6553) COLLATE utf8_unicode_ci NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `property_road_surface_types_code_index` (`code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `property_types` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(6553) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `property_types_code_unique` (`code`),
  KEY `property_types_code_index` (`code`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `road_work_record_logs` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `work_record_id` int(10) unsigned NOT NULL,
  `surface_type_id` int(10) unsigned NOT NULL DEFAULT '0',
  `road_length` decimal(16,6) NOT NULL DEFAULT '0.000000',
  `unit_id` decimal(8,2) unsigned NOT NULL DEFAULT '0.00',
  PRIMARY KEY (`id`),
  KEY `road_work_record_logs_surface_type_id_index` (`surface_type_id`),
  KEY `road_work_record_logs_unit_id_index` (`unit_id`),
  KEY `road_work_record_logs_work_record_id_index` (`work_record_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `system_information_logs` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `backup_upload_limits` int(11) NOT NULL DEFAULT '2',
  `backup_create_limits` int(11) NOT NULL DEFAULT '4',
  `backup_restore_backups` int(11) NOT NULL DEFAULT '4',
  `database_version` int(11) NOT NULL DEFAULT '0',
  `configured_on` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `work_consumables` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `consumable_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `consumable_id` int(10) unsigned NOT NULL DEFAULT '0',
  `associated_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `associated_id` int(10) unsigned NOT NULL DEFAULT '0',
  `bound_consumable_record` int(10) unsigned NOT NULL DEFAULT '0',
  `work_entry_id` int(10) unsigned NOT NULL DEFAULT '0',
  `measurement_unit_id` int(10) unsigned NOT NULL DEFAULT '9',
  `total_cost` decimal(16,6) NOT NULL DEFAULT '0.000000',
  `total_quantity` decimal(16,6) NOT NULL DEFAULT '0.000000',
  `historic_cost` decimal(16,6) NOT NULL DEFAULT '0.000000',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `taxable` tinyint(1) NOT NULL DEFAULT '0',
  `total_tax` decimal(16,6) NOT NULL DEFAULT '0.000000',
  PRIMARY KEY (`id`),
  KEY `work_consumables_consumable_type_index` (`consumable_type`),
  KEY `work_consumables_consumable_id_index` (`consumable_id`),
  KEY `work_consumables_associated_type_index` (`associated_type`),
  KEY `work_consumables_associated_id_index` (`associated_id`),
  KEY `work_consumables_work_entry_id_index` (`work_entry_id`),
  KEY `work_consumables_taxable_index` (`taxable`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `work_entries` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `work_date` datetime NOT NULL DEFAULT '2015-03-15 20:23:48',
  `employee_id` int(10) unsigned NOT NULL DEFAULT '0',
  `activity_id` int(10) unsigned NOT NULL DEFAULT '0',
  `project_id` int(10) unsigned NOT NULL DEFAULT '0',
  `department_id` int(10) unsigned NOT NULL DEFAULT '0',
  `district_id` int(10) unsigned NOT NULL DEFAULT '0',
  `property_context` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `property_id` int(10) unsigned NOT NULL DEFAULT '0',
  `activity_description` varchar(6553) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Not Available',
  `employee_hours_regular_worked` decimal(16,6) NOT NULL DEFAULT '0.000000',
  `employee_hours_overtime_worked` decimal(16,6) NOT NULL DEFAULT '0.000000',
  `employee_historic_wage_regular` decimal(16,6) NOT NULL DEFAULT '0.000000',
  `employee_historic_wage_overtime` decimal(16,6) NOT NULL DEFAULT '0.000000',
  `employee_historic_fringe_regular` decimal(16,6) NOT NULL DEFAULT '0.000000',
  `employee_historic_fringe_overtime` decimal(16,6) NOT NULL DEFAULT '0.000000',
  `misc_billing_quantity` decimal(16,6) NOT NULL DEFAULT '0.000000',
  `misc_billing_rate` decimal(16,6) NOT NULL DEFAULT '0.000000',
  `misc_unit_id` int(10) unsigned NOT NULL DEFAULT '0',
  `misc_description` varchar(6553) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Not Available',
  `total_materials_quantity` decimal(16,6) NOT NULL DEFAULT '0.000000',
  `total_equipment_units_quantity` decimal(16,6) NOT NULL DEFAULT '0.000000',
  `total_fuels_quantity` decimal(16,6) NOT NULL DEFAULT '0.000000',
  `total_materials_cost` decimal(16,6) NOT NULL DEFAULT '0.000000',
  `total_equipment_units_cost` decimal(16,6) NOT NULL DEFAULT '0.000000',
  `total_fuels_cost` decimal(16,6) NOT NULL DEFAULT '0.000000',
  `total_equipment_and_fuels_cost` decimal(16,6) NOT NULL DEFAULT '0.000000',
  `total_misc_billing_cost` decimal(16,6) NOT NULL DEFAULT '0.000000',
  `total_property_cost` decimal(16,6) NOT NULL DEFAULT '0.000000',
  `total_employee_regular_cost` decimal(16,6) NOT NULL DEFAULT '0.000000',
  `total_employee_overtime_cost` decimal(16,6) NOT NULL DEFAULT '0.000000',
  `total_employee_combined_cost` decimal(16,6) NOT NULL DEFAULT '0.000000',
  `record_status` tinyint(3) unsigned NOT NULL DEFAULT '1',
  `exclude_from_report` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `total_invoice` decimal(16,6) NOT NULL DEFAULT '0.000000',
  `employee_taxable` tinyint(1) NOT NULL DEFAULT '0',
  `property_organization_id` int(10) unsigned NOT NULL DEFAULT '0',
  `property_organization_context` tinyint(3) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `work_entries_work_date_index` (`work_date`),
  KEY `work_entries_activity_id_index` (`activity_id`),
  KEY `work_entries_project_id_index` (`project_id`),
  KEY `work_entries_department_id_index` (`department_id`),
  KEY `work_entries_district_id_index` (`district_id`),
  KEY `work_entries_property_id_index` (`property_id`),
  KEY `work_entries_employee_id_index` (`employee_id`,`activity_id`,`project_id`,`department_id`,`district_id`,`property_id`,`property_context`,`deleted_at`),
  KEY `terst` (`employee_id`,`activity_id`,`project_id`,`department_id`,`district_id`,`property_id`,`id`) USING BTREE,
  KEY `perf15_work_entries` (`employee_id`,`activity_id`,`project_id`,`department_id`,`district_id`,`property_id`,`id`),
  KEY `property_organization_id_index` (`property_organization_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



SET AUTOCOMMIT = 0; SET FOREIGN_KEY_CHECKS = 0; SET UNIQUE_CHECKS = 0;
SET SQL_SAFE_UPDATES = 0;
SET SESSION sql_mode='NO_AUTO_VALUE_ON_ZERO';

-- RESET DATA
-- RESET DATA
TRUNCATE `ndcounty__genesiskzefdfcecobzemzkokoicqqcmnmbiifb`.`activities`;
TRUNCATE `ndcounty__genesiskzefdfcecobzemzkokoicqqcmnmbiifb`.`addresses`;
TRUNCATE `ndcounty__genesiskzefdfcecobzemzkokoicqqcmnmbiifb`.`consumables`;
TRUNCATE `ndcounty__genesiskzefdfcecobzemzkokoicqqcmnmbiifb`.`county_meta`;
TRUNCATE `ndcounty__genesiskzefdfcecobzemzkokoicqqcmnmbiifb`.`county_settings`;
TRUNCATE `ndcounty__genesiskzefdfcecobzemzkokoicqqcmnmbiifb`.`departments`;
TRUNCATE `ndcounty__genesiskzefdfcecobzemzkokoicqqcmnmbiifb`.`districts`;
TRUNCATE `ndcounty__genesiskzefdfcecobzemzkokoicqqcmnmbiifb`.`employees`;
TRUNCATE `ndcounty__genesiskzefdfcecobzemzkokoicqqcmnmbiifb`.`equipment_odometer_readings`;
TRUNCATE `ndcounty__genesiskzefdfcecobzemzkokoicqqcmnmbiifb`.`equipment_units`;
TRUNCATE `ndcounty__genesiskzefdfcecobzemzkokoicqqcmnmbiifb`.`equipment_usage`;
TRUNCATE `ndcounty__genesiskzefdfcecobzemzkokoicqqcmnmbiifb`.`equipment_work_record_repairs`;
TRUNCATE `ndcounty__genesiskzefdfcecobzemzkokoicqqcmnmbiifb`.`fs_accounts`;
TRUNCATE `ndcounty__genesiskzefdfcecobzemzkokoicqqcmnmbiifb`.`fs_accounts_categories`;
TRUNCATE `ndcounty__genesiskzefdfcecobzemzkokoicqqcmnmbiifb`.`fs_invoice_work_records`;
TRUNCATE `ndcounty__genesiskzefdfcecobzemzkokoicqqcmnmbiifb`.`fs_invoices`;
TRUNCATE `ndcounty__genesiskzefdfcecobzemzkokoicqqcmnmbiifb`.`fs_invoices_tax_rates`;
TRUNCATE `ndcounty__genesiskzefdfcecobzemzkokoicqqcmnmbiifb`.`fs_supplier_notes`;
TRUNCATE `ndcounty__genesiskzefdfcecobzemzkokoicqqcmnmbiifb`.`fs_suppliers`;
TRUNCATE `ndcounty__genesiskzefdfcecobzemzkokoicqqcmnmbiifb`.`fs_tax_rates`;
TRUNCATE `ndcounty__genesiskzefdfcecobzemzkokoicqqcmnmbiifb`.`fs_transactions`;
TRUNCATE `ndcounty__genesiskzefdfcecobzemzkokoicqqcmnmbiifb`.`gps_entries`;
TRUNCATE `ndcounty__genesiskzefdfcecobzemzkokoicqqcmnmbiifb`.`inventory_adjustments`;
TRUNCATE `ndcounty__genesiskzefdfcecobzemzkokoicqqcmnmbiifb`.`migrations`;
TRUNCATE `ndcounty__genesiskzefdfcecobzemzkokoicqqcmnmbiifb`.`phone_numbers`;
TRUNCATE `ndcounty__genesiskzefdfcecobzemzkokoicqqcmnmbiifb`.`projects`;
TRUNCATE `ndcounty__genesiskzefdfcecobzemzkokoicqqcmnmbiifb`.`properties`;
TRUNCATE `ndcounty__genesiskzefdfcecobzemzkokoicqqcmnmbiifb`.`property_adapters`;
TRUNCATE `ndcounty__genesiskzefdfcecobzemzkokoicqqcmnmbiifb`.`property_categories`;
TRUNCATE `ndcounty__genesiskzefdfcecobzemzkokoicqqcmnmbiifb`.`property_property`;
TRUNCATE `ndcounty__genesiskzefdfcecobzemzkokoicqqcmnmbiifb`.`property_road_properties`;
TRUNCATE `ndcounty__genesiskzefdfcecobzemzkokoicqqcmnmbiifb`.`property_road_surface_types`;
TRUNCATE `ndcounty__genesiskzefdfcecobzemzkokoicqqcmnmbiifb`.`property_types`;
TRUNCATE `ndcounty__genesiskzefdfcecobzemzkokoicqqcmnmbiifb`.`road_work_record_logs`;
TRUNCATE `ndcounty__genesiskzefdfcecobzemzkokoicqqcmnmbiifb`.`system_information_logs`;
TRUNCATE `ndcounty__genesiskzefdfcecobzemzkokoicqqcmnmbiifb`.`work_consumables`;
TRUNCATE `ndcounty__genesiskzefdfcecobzemzkokoicqqcmnmbiifb`.`work_entries`;

-- MIGRATION SPECIFIC ALTER STATEMENTS
	-- This creates a temporary set of columns on the consumables so we can keep track of where each fuel/material originally came from.
	ALTER TABLE	ndcounty__genesiskzefdfcecobzemzkokoicqqcmnmbiifb.consumables  ADD origin_type     INT(10) DEFAULT 0;
	ALTER TABLE ndcounty__genesiskzefdfcecobzemzkokoicqqcmnmbiifb.consumables  ADD origin_id       INT(10) DEFAULT 0;
	ALTER TABLE ndcounty__genesiskzefdfcecobzemzkokoicqqcmnmbiifb.consumables  ADD origin_track    INT(10) DEFAULT 0;
  ALTER TABLE ndcounty__genesiskzefdfcecobzemzkokoicqqcmnmbiifb.work_entries ADD original_id    INT(10) DEFAULT 0;
  ALTER TABLE ndcounty__genesiskzefdfcecobzemzkokoicqqcmnmbiifb.work_entries ADD property_placed INT (10) DEFAULT 0;
  ALTER TABLE ndcounty__genesiskzefdfcecobzemzkokoicqqcmnmbiifb.work_entries ADD property_batched INT (10) DEFAULT 0;
-- END MIGRATION SPECIFIC ALTER STATEMENTS

-- COUNTY SETTINGS
	-- The following settings have been deprecated or have been moved
		-- Show empty fields in builder
		-- allow dervied focus
		-- use surface type
		-- auto expand more options
		-- records per page
	INSERT INTO ndcounty__genesiskzefdfcecobzemzkokoicqqcmnmbiifb.county_settings(id, use_districts, show_employee_overtime, data_decimal_places, invoice_default_status, auto_process_invoice)
	SELECT id, use_districts, show_employee_overtime, data_decimal_places, 1, 0 FROM ndcounty_miacbdqndocaamezccoozbzaecmicdca.county_settings;

-- DEFAULT ACTIVITIES
	INSERT INTO ndcounty__genesiskzefdfcecobzemzkokoicqqcmnmbiifb.activities(id, code, name, description) VALUES(0, 'NA', 'Not Available', 'Not Available');
-- USER    ACTIVITIES
-- Budgeted data has been deprecated.
	INSERT INTO ndcounty__genesiskzefdfcecobzemzkokoicqqcmnmbiifb.activities(id, code, name, description, deleted_at, created_at, updated_at)
	SELECT id, code, name, description, deleted_at, created_at, updated_at FROM ndcounty_miacbdqndocaamezccoozbzaecmicdca.activities WHERE id > 0;

-- DEFAULT DEPARTMENTS
	INSERT INTO ndcounty__genesiskzefdfcecobzemzkokoicqqcmnmbiifb.departments(id, code, name, description) VALUES (0, 'NA', 'Not Available', 'Not Available');
-- USER    DEPARTMENTS
	INSERT INTO ndcounty__genesiskzefdfcecobzemzkokoicqqcmnmbiifb.departments(id, code, name, description)
	SELECT id, code, name, description FROM ndcounty_miacbdqndocaamezccoozbzaecmicdca.departments WHERE id > 0;

-- DEFAULT DISTRICTS
	INSERT INTO ndcounty__genesiskzefdfcecobzemzkokoicqqcmnmbiifb.districts(id, code, name, description) VALUES (0, 'NA', 'Not Available', 'Not Available');
-- USER    DISTRICTS
	INSERT INTO ndcounty__genesiskzefdfcecobzemzkokoicqqcmnmbiifb.districts(id, code, name, description)
	SELECT id, code, name, description FROM ndcounty_miacbdqndocaamezccoozbzaecmicdca.districts WHERE id > 0;

-- DEFAULT EMPLOYEES
	INSERT INTO ndcounty__genesiskzefdfcecobzemzkokoicqqcmnmbiifb.employees(id, first_name, middle_name, last_name, code, wage, overtime, wage_benefit, overtime_benefit)
	                   VALUES(0, 'NA', 'NA', 'NA', 'NA', 0, 0, 0, 0);
-- USER    EMPLOYEES
	INSERT INTO ndcounty__genesiskzefdfcecobzemzkokoicqqcmnmbiifb.employees(id, first_name, middle_name, last_name, code, wage, overtime, wage_benefit, overtime_benefit)
	SELECT id, first_name, ' ', last_name, code, wage, overtime, wage_benefit, overtime_benefit FROM ndcounty_miacbdqndocaamezccoozbzaecmicdca.employees WHERE ndcounty_miacbdqndocaamezccoozbzaecmicdca.employees.id > 0;

-- START OF CONSUMABLES MIGRATION --
	-- Material context is 2
	-- Fuel context is     1

-- DEFAULT FUELS
  INSERT INTO consumables(id, code, name, description, unit_id, consumable_context, origin_type, origin_id, origin_track)
  VALUES (0, 'NA', 'Not Available', 'Not Available', 9, 2, 0, 0, 0);
-- DEFAULT MATERIALS
  INSERT INTO consumables(id, code, name, description, unit_id, consumable_context, origin_type, origin_id, origin_track)
  VALUES (1, 'NA', 'Not Available', 'Not Available', 9, 1, 0, 0, 0);
-- USER    FUELS
	INSERT INTO consumables(code, name, description, unit_id, cost, deleted_at, created_at, updated_at, consumable_context, origin_type, origin_id, origin_track)
	SELECT code, name, description, unit_id, cost, deleted_at, created_at, updated_at, 1, 0, id, 1 FROM ndcounty_miacbdqndocaamezccoozbzaecmicdca.fuels WHERE ndcounty_miacbdqndocaamezccoozbzaecmicdca.fuels.id > 0;


-- USER    MATERIALS
	INSERT INTO ndcounty__genesiskzefdfcecobzemzkokoicqqcmnmbiifb.consumables(code, name, description, unit_id, cost, deleted_at, created_at, updated_at, consumable_context, origin_type, origin_id, origin_track)
	SELECT code, name, description, unit_id, cost_per_unit, deleted_at, created_at, updated_at, 2, 1, id, 1 FROM ndcounty_miacbdqndocaamezccoozbzaecmicdca.materials WHERE ndcounty_miacbdqndocaamezccoozbzaecmicdca.materials.id > 0;
-- END   OF CONSUMABLES MIGRATION --

-- DEFAULT PROJECTS
	INSERT INTO ndcounty__genesiskzefdfcecobzemzkokoicqqcmnmbiifb.projects(id, code, name, description, fema_project) VALUES (0, 'NA', 'Not Available', 'Not Available', 0);
-- USER    PROJECTS
	INSERT INTO ndcounty__genesiskzefdfcecobzemzkokoicqqcmnmbiifb.projects(id, code, name, description, fema_project)
	SELECT id, code, name, description, is_fema FROM ndcounty_miacbdqndocaamezccoozbzaecmicdca.projects WHERE id > 0;

-- DEFAULT SURFACE TYPES
	INSERT INTO ndcounty__genesiskzefdfcecobzemzkokoicqqcmnmbiifb.property_road_surface_types(id, code, name, description) VALUES(0, 'NA', 'Not Available', 'Not Available');
-- USER    SURFACE TYPES
	INSERT INTO ndcounty__genesiskzefdfcecobzemzkokoicqqcmnmbiifb.property_road_surface_types(id, code, name, description)
	SELECT id, name, code, description FROM ndcounty_miacbdqndocaamezccoozbzaecmicdca.types WHERE id > 0;

-- START OF PROPERTIES MIGRATION --

	-- REQUIRED PROPERTY TYPES
		INSERT INTO ndcounty__genesiskzefdfcecobzemzkokoicqqcmnmbiifb.property_types(id, code, name, description) VALUES(0, 'NA', 'Not Available', 'Not Available');
		INSERT INTO ndcounty__genesiskzefdfcecobzemzkokoicqqcmnmbiifb.property_types(id, code, name, description) VALUES(1, 'ROAD', 'ROAD', 'The default road property type.');
		INSERT INTO ndcounty__genesiskzefdfcecobzemzkokoicqqcmnmbiifb.property_types(id, code, name, description) VALUES(2, 'EQUIPMENT UNIT', 'EQUIPMENT UNIT', 'The default equipment unit property type.');
	
	-- REQUIRED PROPERTY ADAPTERS
		INSERT INTO ndcounty__genesiskzefdfcecobzemzkokoicqqcmnmbiifb.property_adapters(id, property_id, adapter) VALUES(1, 1, 1);
		INSERT INTO ndcounty__genesiskzefdfcecobzemzkokoicqqcmnmbiifb.property_adapters(id, property_id, adapter) VALUES(2, 2, 2);

	-- REQUIRED PROPERTY CATEGORIES
		INSERT INTO ndcounty__genesiskzefdfcecobzemzkokoicqqcmnmbiifb.property_categories(id, category_name) VALUES(0, 'Uncategorized');

		ALTER TABLE ndcounty__genesiskzefdfcecobzemzkokoicqqcmnmbiifb.properties ADD original_id      INT(10) DEFAULT 0;
		ALTER TABLE ndcounty__genesiskzefdfcecobzemzkokoicqqcmnmbiifb.properties ADD original_type    INT(10) DEFAULT 0;

	-- DEFAULT EQUIPMENT
		INSERT INTO ndcounty__genesiskzefdfcecobzemzkokoicqqcmnmbiifb.properties(id, property_type, code, description, name) VALUES(1, 2, 'NA - EQUIPMENT', 'Not Available', 'Not Available - Equipment');
    INSERT INTO ndcounty__genesiskzefdfcecobzemzkokoicqqcmnmbiifb.equipment_units(id, property_id, year, make, model, serial_number, rental_rate, unit_id, purchase_cost, salvage_value, lifetime, created_at, updated_at) VALUES('1', '1', '0', '', '', 'Not Available', '0.000000', '9', '0.000000', '0.000000', '0.00', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
	-- USER    EQUIPMENT
	-- Equipment unit properties need to have data in the following tables
		-- properties      (main entry)
		-- equipment_units (equipment data)
	-- Property type: 2
		INSERT INTO ndcounty__genesiskzefdfcecobzemzkokoicqqcmnmbiifb.properties(property_type, code, description, name, original_id, original_type)
		SELECT 2, code, description, name, id, 3 FROM ndcounty_miacbdqndocaamezccoozbzaecmicdca.equipment;

		-- This will insert the equipment unit information into the `equipment_units` table.
		INSERT INTO ndcounty__genesiskzefdfcecobzemzkokoicqqcmnmbiifb.equipment_units
		(property_id, year, make, model, serial_number, rental_rate, unit_id, purchase_cost, salvage_value, lifetime, created_at, updated_at)
		
		SELECT ndcounty__genesiskzefdfcecobzemzkokoicqqcmnmbiifb.properties.id, oqp.year, oqp.make, oqp.model, oqp.serial_number, oqp.rental_rate,
			    oqp.unit_id, oqp.purchase_cost, oqp.salvage_value, oqp.lifetime, oqp.created_at, oqp.updated_at
		FROM ndcounty_miacbdqndocaamezccoozbzaecmicdca.equipment as oqp INNER JOIN ndcounty__genesiskzefdfcecobzemzkokoicqqcmnmbiifb.properties ON oqp.id = ndcounty__genesiskzefdfcecobzemzkokoicqqcmnmbiifb.properties.original_id WHERE ndcounty__genesiskzefdfcecobzemzkokoicqqcmnmbiifb.properties.original_type = 3;


	-- DEFAULT ROAD
		INSERT INTO ndcounty__genesiskzefdfcecobzemzkokoicqqcmnmbiifb.properties(id, property_type, code, description, name) VALUES(0, 0, 'NA', 'Not Available', 'Not Available');
	-- USER    ROAD
	-- Road properties need to have data in the following tables
		-- properties 			    (main entry)
		-- property_road_properties (road data)
	-- Property type: 1
		INSERT INTO ndcounty__genesiskzefdfcecobzemzkokoicqqcmnmbiifb.properties(property_type, code, description, name, original_id, original_type)
		SELECT 1, code, CONCAT('Road Property: ', name), name, id, 1 FROM ndcounty_miacbdqndocaamezccoozbzaecmicdca.roads;

		-- type_id is the original surface type
		-- road_type distinguished between city, township, etc. This has been removed.
		-- The original system did NOT have the concept of a length unit on a road. We will assume that the
		-- default length unit is 9 (No Unit).
		INSERT INTO ndcounty__genesiskzefdfcecobzemzkokoicqqcmnmbiifb.property_road_properties(road_id, surface_type_id, road_length, unit_id)
		SELECT ndcounty__genesiskzefdfcecobzemzkokoicqqcmnmbiifb.properties.id, ord.type_id, ord.length, 9
		FROM ndcounty_miacbdqndocaamezccoozbzaecmicdca.roads as ord INNER JOIN ndcounty__genesiskzefdfcecobzemzkokoicqqcmnmbiifb.properties ON ord.id = ndcounty__genesiskzefdfcecobzemzkokoicqqcmnmbiifb.properties.original_id WHERE ndcounty__genesiskzefdfcecobzemzkokoicqqcmnmbiifb.properties.original_type = 1;

-- END   OF PROPERTIES MIGRATION --

-- USER    WORK LOGS

  -- MAIN WORK ENTRIES

  -- First Pass: The first pass is used to gather general work log information.

      INSERT INTO ndcounty__genesiskzefdfcecobzemzkokoicqqcmnmbiifb.work_entries(
        work_date, employee_id, activity_id, project_id, department_id, district_id,
        activity_description, employee_hours_regular_worked, employee_hours_overtime_worked, employee_historic_wage_regular, employee_historic_wage_overtime,
        employee_historic_fringe_regular, employee_historic_fringe_overtime, misc_billing_quantity, misc_billing_rate, misc_unit_id,
        misc_description, created_at, updated_at,
        original_id)
      SELECT
      drl.date_entered, drl.employee_id, drl.activity_id, drl.project_id, drl.department_id, drl.district_id,
      dwl.misc_activity_description, dwl.hours_worked, dwl.overtime_hours_worked, dwl.hourly_rate_custom, dwl.overtime_rate_custom,
      dwl.fringe_hourly, dwl.fringe_overtime, dwl.misc_billing_quantity, dwl.misc_billing_rate, dwl.misc_billing_unit,
      dwl.misc_billing_description, dwl.created_at, dwl.updated_at,
      dwl.id
      FROM ndcounty_miacbdqndocaamezccoozbzaecmicdca.daily_work_report_logs as drl INNER JOIN ndcounty_miacbdqndocaamezccoozbzaecmicdca.daily_work_logs as dwl ON drl.work_log_id = dwl.id WHERE drl.deleted_at IS NULL;
      -- Need the NULL check here. Deleted records from the old version will NOT be migrated to the new system.

  -- Second Pass: The second pass will create the records in the target ndcounty__genesiskzefdfcecobzemzkokoicqqcmnmbiifb.road_work_record_logs table.
  --              The data comes from the origin ndcounty_miacbdqndocaamezccoozbzaecmicdca.daily_work_road_logs
      INSERT INTO ndcounty__genesiskzefdfcecobzemzkokoicqqcmnmbiifb.road_work_record_logs (
        work_record_id, surface_type_id, road_length, unit_id
      )
      SELECT
      twl.id, record_surface_type, record_section_length, 9
      FROM ndcounty_miacbdqndocaamezccoozbzaecmicdca.daily_work_road_logs as dwr INNER JOIN ndcounty__genesiskzefdfcecobzemzkokoicqqcmnmbiifb.work_entries AS twl ON dwr.daily_work_log_id = twl.original_id;
      -- We will default to 9 (No Unit) for the unit_id. This is a new feature that was added to the newest version.

  -- Third Pass:   The third pass will add the existing equipment records to the ndcounty__genesiskzefdfcecobzemzkokoicqqcmnmbiifb.work_consumables table.
    -- The consumable type is 2
    -- Consumable ID  must be id of property (equipment)
    -- Associated type is 2
    -- Associated id is   0
    -- The bound consumable record is 0
    INSERT INTO ndcounty__genesiskzefdfcecobzemzkokoicqqcmnmbiifb.work_consumables(
      consumable_type, associated_type, associated_id, bound_consumable_record,
      consumable_id,
      work_entry_id, measurement_unit_id, total_cost, total_quantity, historic_cost,
      created_at, updated_at, taxable, total_tax
    ) SELECT
      2, 2, 0, 0,
      (SELECT id FROM ndcounty__genesiskzefdfcecobzemzkokoicqqcmnmbiifb.properties WHERE original_type = 3 AND original_id = drl.equipment_id),
      twl.id, 9, drl.equipment_total_cost, drl.equipment_hours_miles, drl.equipment_report_rate,
      drl.created_at, drl.updated_at, 0, 0
    FROM ndcounty_miacbdqndocaamezccoozbzaecmicdca.daily_work_report_logs as drl INNER JOIN ndcounty__genesiskzefdfcecobzemzkokoicqqcmnmbiifb.work_entries AS twl ON drl.work_log_id = twl.original_id
    INNER JOIN ndcounty__genesiskzefdfcecobzemzkokoicqqcmnmbiifb.properties as tpr ON drl.equipment_id = tpr.original_id
    WHERE tpr.original_type = 3;

    -- The consumable_id of 0 needs to be changed to 1 for all equipment unit consumable records.
    UPDATE ndcounty__genesiskzefdfcecobzemzkokoicqqcmnmbiifb.work_consumables SET consumable_id = 1
    WHERE  ndcounty__genesiskzefdfcecobzemzkokoicqqcmnmbiifb.work_consumables.consumable_id = 0;


  -- Fifth Pass:   The fifth pass will add the existing fuel records to the ndcounty__genesiskzefdfcecobzemzkokoicqqcmnmbiifb.work_consumables table AND bind them to existing equipment records.
    -- The consumable type is  3
    -- The consumable id is the fuel id
    -- The associated type is  3
    -- The associated ID is the property id (equipment) of bound record (will be from same work entry id)
    -- The bound consumable record is the NEW consumable record with the SAME work entry id.
    INSERT INTO ndcounty__genesiskzefdfcecobzemzkokoicqqcmnmbiifb.work_consumables(
      consumable_type, associated_type, associated_id, bound_consumable_record,
      consumable_id,
      work_entry_id, measurement_unit_id, total_cost, total_quantity, historic_cost,
      created_at, updated_at, taxable, total_tax
    ) SELECT
      3, 3, wcons.consumable_id, wcons.id,
      cons.id,
      twl.id, 9, drf.fuel_total, drf.fuel_quantity, drf.fuel_cost,
      drf.created_at, drf.updated_at, 0, 0
    FROM ndcounty_miacbdqndocaamezccoozbzaecmicdca.daily_work_report_logs_fuels AS drf INNER JOIN ndcounty__genesiskzefdfcecobzemzkokoicqqcmnmbiifb.work_entries AS twl ON drf.daily_work_report_log_id = twl.original_id
    INNER JOIN ndcounty__genesiskzefdfcecobzemzkokoicqqcmnmbiifb.consumables AS cons on drf.fuel_id = cons.origin_id
    INNER JOIN ndcounty__genesiskzefdfcecobzemzkokoicqqcmnmbiifb.work_consumables AS wcons on twl.id = wcons.work_entry_id
    WHERE cons.origin_type = 0 AND cons.origin_track = 1
    AND   wcons.consumable_type = 2;
    -- The WCONS joined table will contain the related equipment record.


  -- Fourth Pass:  The fourth pass will add the existing material records to the ndcounty__genesiskzefdfcecobzemzkokoicqqcmnmbiifb.work_consumables table.
    -- The consumable type is 4
    -- The consumable id is the id of the material
    -- The asscoaited type is 4
    -- The associated id is 0
    -- The bound consumable record is 0
    INSERT INTO ndcounty__genesiskzefdfcecobzemzkokoicqqcmnmbiifb.work_consumables(
      consumable_type, associated_type, associated_id, bound_consumable_record,
      consumable_id,
      work_entry_id, measurement_unit_id, total_cost, total_quantity, historic_cost,
      created_at, updated_at, taxable, total_tax
    ) SELECT
      4, 4, 0, 0,
      cons.id,
      twl.id, 9, drm.record_total_cost, drm.record_quantity, drm.material_cost,
      drm.created_at, drm.updated_at, 0, 0
    FROM ndcounty_miacbdqndocaamezccoozbzaecmicdca.daily_work_materials as drm INNER JOIN ndcounty__genesiskzefdfcecobzemzkokoicqqcmnmbiifb.work_entries AS twl ON drm.daily_work_log_id = twl.original_id
    INNER JOIN ndcounty__genesiskzefdfcecobzemzkokoicqqcmnmbiifb.consumables as cons on drm.material_id = cons.origin_id
    WHERE cons.origin_type = 1 AND cons.origin_track = 1;

    -- This removes all the original empty road entries for NA to reduce data size.
    UPDATE ndcounty__genesiskzefdfcecobzemzkokoicqqcmnmbiifb.work_entries as wle
    INNER JOIN ndcounty_miacbdqndocaamezccoozbzaecmicdca.daily_work_report_logs AS drl ON wle.original_id = drl.work_log_id

    SET wle.property_batched = 1,
        wle.property_placed  = 1,
        wle.property_id      = 0,
        wle.property_context = 0
    WHERE drl.road_id = 0 AND drl.surface_type = 0 AND drl.road_surface_length = 0; -- This will find all "useless" road records.

    -- Now we need to remove all entries from the "road_work_record_logs" table that have been orphaned. We will use the 'property_batched'
    -- column to make this easier.
    DELETE FROM ndcounty__genesiskzefdfcecobzemzkokoicqqcmnmbiifb.road_work_record_logs
    WHERE  work_record_id IN (
      SELECT id FROM ndcounty__genesiskzefdfcecobzemzkokoicqqcmnmbiifb.work_entries WHERE ndcounty__genesiskzefdfcecobzemzkokoicqqcmnmbiifb.work_entries.property_batched = 1
    );

    -- Remove the batched status from the work entries table.
    UPDATE ndcounty__genesiskzefdfcecobzemzkokoicqqcmnmbiifb.work_entries as wle SET wle.property_batched = 0;

    -- Sixth Pass:   The sixth pass will collect any valid odometer readings and add them to the ndcounty__genesiskzefdfcecobzemzkokoicqqcmnmbiifb.equipment_odometer_readings table.

    INSERT INTO ndcounty__genesiskzefdfcecobzemzkokoicqqcmnmbiifb.equipment_odometer_readings (
      equipment_id, odometer_reading, associated_record, associated_record_type
    )
    SELECT
      eqp.id, dwel.odometer_reading, wle.id, 2
    FROM ndcounty_miacbdqndocaamezccoozbzaecmicdca.daily_work_equipment_logs AS dwel
    INNER JOIN ndcounty__genesiskzefdfcecobzemzkokoicqqcmnmbiifb.work_entries AS wle ON dwel.daily_work_log_id = wle.original_id
    INNER JOIN ndcounty__genesiskzefdfcecobzemzkokoicqqcmnmbiifb.properties   AS eqp ON dwel.equipment_id = eqp.original_id
    INNER JOIN ndcounty_miacbdqndocaamezccoozbzaecmicdca.daily_work_report_logs AS drl ON dwel.daily_work_log_id = drl.work_log_id
    WHERE eqp.original_type = 3
    AND   drl.road_id     = 0 -- This will prevent removing road records that need to be there.
    AND   dwel.equipment_id <> 0
    AND   (
      (dwel.shop_costs > 0 OR dwel.purchased_costs > 0 OR dwel.commercial_costs > 0 OR dwel.commercial_labor_costs > 0) OR
      (CHAR_LENGTH(dwel.work_description) > 0)
    );

    -- Now, any work entry that exists in the equipment odometer readings table must
    -- be "batched".
    UPDATE ndcounty__genesiskzefdfcecobzemzkokoicqqcmnmbiifb.work_entries
    SET    property_batched = 1
    WHERE  id IN (SELECT associated_record FROM ndcounty__genesiskzefdfcecobzemzkokoicqqcmnmbiifb.equipment_odometer_readings);

    -- At this point, any work entry that is in the batched state should be
    -- treated as an equipment property type.

    UPDATE ndcounty__genesiskzefdfcecobzemzkokoicqqcmnmbiifb.work_entries AS wle
    INNER JOIN ndcounty__genesiskzefdfcecobzemzkokoicqqcmnmbiifb.equipment_odometer_readings AS eor ON wle.id = eor.associated_record
    SET wle.property_context = 1,
        wle.property_id      = eor.equipment_id,
        wle.property_placed  = 1;

    -- At this point, all previous equipment repair records that did NOT have a road ID set
    -- will have been converted to the new system.

    -- Remove the batched status from the work entries table.
    UPDATE ndcounty__genesiskzefdfcecobzemzkokoicqqcmnmbiifb.work_entries as wle SET wle.property_batched = 0;


    -- This will get all equipment records that need to be SEPARATED
    -- from the original work record. It will inherit all the record dates
    -- and organizational records NOTHING ELSE.

    INSERT INTO ndcounty__genesiskzefdfcecobzemzkokoicqqcmnmbiifb.work_entries(
        work_date,
        employee_id,
        activity_id,
        project_id,
        department_id,
        district_id,
        activity_description,
        employee_hours_regular_worked,
        employee_hours_overtime_worked,
        employee_historic_wage_regular,
        employee_historic_wage_overtime,
        employee_historic_fringe_regular,
        employee_historic_fringe_overtime,
        misc_billing_quantity,
        misc_billing_rate,
        misc_unit_id,
        misc_description,
        created_at,
        updated_at,
        original_id,
        property_batched,
        property_id,
        property_context,
        property_placed) -- 24 Columns
    SELECT
      wle.work_date,
      wle.employee_id,
      wle.activity_id,
      wle.project_id,
      wle.department_id,
      wle.district_id,
      wle.activity_description,
      0,
      0,
      0,
      0,
      0,
      0,
      0,
      0,
      0,
      0,
      wle.created_at,
      wle.updated_at,
      wle.original_id,
      1,
      eqp.id,
      1,
      1                   -- 24 Columns
    FROM ndcounty_miacbdqndocaamezccoozbzaecmicdca.daily_work_equipment_logs AS dwe
    INNER JOIN ndcounty__genesiskzefdfcecobzemzkokoicqqcmnmbiifb.work_entries AS wle ON dwe.daily_work_log_id = wle.original_id
    INNER JOIN ndcounty__genesiskzefdfcecobzemzkokoicqqcmnmbiifb.properties   AS eqp ON dwe.equipment_id      = eqp.original_id
    INNER JOIN ndcounty_miacbdqndocaamezccoozbzaecmicdca.daily_work_report_logs AS drl ON dwe.daily_work_log_id = drl.work_log_id
    WHERE eqp.original_type   = 3
    AND   drl.road_id      <> 0
    AND   dwe.equipment_id <> 0
    AND   (
      (dwe.shop_costs > 0 OR dwe.purchased_costs > 0 OR dwe.commercial_costs > 0 OR dwe.commercial_labor_costs > 0) OR
      (CHAR_LENGTH(dwe.work_description) > 0)
    );

    -- This will add all of the equipment odometer readings (if any)
    -- to the ndcounty__genesiskzefdfcecobzemzkokoicqqcmnmbiifb.equipment_odometer_readings table that might need to be
    -- associated with the previously created equipment repair records (the separated ones).

    INSERT INTO ndcounty__genesiskzefdfcecobzemzkokoicqqcmnmbiifb.equipment_odometer_readings (
      equipment_id, odometer_reading, associated_record, associated_record_type
    )
    SELECT
      wle.property_id, dwe.odometer_reading, wle.id, 2
    FROM ndcounty_miacbdqndocaamezccoozbzaecmicdca.daily_work_equipment_logs AS dwe
    INNER JOIN ndcounty__genesiskzefdfcecobzemzkokoicqqcmnmbiifb.work_entries AS wle ON dwe.daily_work_log_id = wle.original_id
    INNER JOIN ndcounty_miacbdqndocaamezccoozbzaecmicdca.daily_work_report_logs AS drl ON dwe.daily_work_log_id = drl.work_log_id
    WHERE wle.property_batched = 1;

    -- Remove the batched status from the work entries table.
    UPDATE ndcounty__genesiskzefdfcecobzemzkokoicqqcmnmbiifb.work_entries as wle SET wle.property_batched = 0;


    -- At this point, we only need to be concerned with transforming the road properties and
    -- types. These should be able to be grabbed by querying all records that do not have the
    -- 'property_placed' flag set.

    UPDATE ndcounty__genesiskzefdfcecobzemzkokoicqqcmnmbiifb.work_entries SET property_batched = 1 WHERE property_placed = 0;

    -- Set the correct property context for roads.
    UPDATE ndcounty__genesiskzefdfcecobzemzkokoicqqcmnmbiifb.work_entries SET property_context = 0 WHERE property_batched = 1;

    -- Set the correct property ID for the road records.
    UPDATE ndcounty__genesiskzefdfcecobzemzkokoicqqcmnmbiifb.work_entries AS wle
    INNER JOIN ndcounty_miacbdqndocaamezccoozbzaecmicdca.daily_work_report_logs AS drl on wle.original_id = drl.work_log_id
    INNER JOIN ndcounty__genesiskzefdfcecobzemzkokoicqqcmnmbiifb.properties   AS rdd ON drl.road_id = rdd.original_id

    SET wle.property_id     = rdd.id,
        wle.property_placed = 1,
        wle.property_organization_id = rdd.id,
        wle.property_organization_context = 0

    WHERE rdd.original_type = 1;
      -- Need the original_type check here to make sure we actually grab a road
      -- property record.


    -- Remove the batched status from the work entries table.
    UPDATE ndcounty__genesiskzefdfcecobzemzkokoicqqcmnmbiifb.work_entries as wle SET wle.property_batched = 0;

  -- Eigth Pass:   The eigth pass will determine which previously entered ROAD properties would be better suited as a "NA" default property type.
  --               The criteria is any ROAD type that has NEVER utilized the ndcounty_miacbdqndocaamezccoozbzaecmicdca.daily_work_road_logs.record_section_length fields, etc.
 
    UPDATE ndcounty__genesiskzefdfcecobzemzkokoicqqcmnmbiifb.work_entries AS wle
    INNER JOIN ndcounty__genesiskzefdfcecobzemzkokoicqqcmnmbiifb.road_work_record_logs AS rwl ON wle.id = rwl.work_record_id

    SET wle.property_id      = 0,
        wle.property_context = 0,
        wle.property_batched = 1

    WHERE rwl.road_length = 0;

    -- This will remove all supporting records for the road records that do not need
    -- to be kept around.
    DELETE rwl.* FROM ndcounty__genesiskzefdfcecobzemzkokoicqqcmnmbiifb.road_work_record_logs AS rwl
    INNER JOIN ndcounty__genesiskzefdfcecobzemzkokoicqqcmnmbiifb.work_entries AS wle ON rwl.work_record_id = wle.id
    WHERE wle.property_batched = 1;

    -- Remove the batched status from the work entries table.
    UPDATE ndcounty__genesiskzefdfcecobzemzkokoicqqcmnmbiifb.work_entries as wle SET wle.property_batched = 0;

    -- Now, we need to build up the ndcounty__genesiskzefdfcecobzemzkokoicqqcmnmbiifb.equipment_work_record_repairs table data.
    -- This is the LAST table that needs to be built up.
    INSERT INTO ndcounty__genesiskzefdfcecobzemzkokoicqqcmnmbiifb.equipment_work_record_repairs(
      work_record_id,
      work_order_number,
      shop_costs,
      purchased_costs,
      commercial_costs,
      commercial_labor_costs,
      work_description -- 7 Columns
    )
    SELECT
    dwet.id,
    dwel.work_order_no,
    dwel.shop_costs,
    dwel.purchased_costs,
    dwel.commercial_costs,
    dwel.commercial_labor_costs,
    dwel.work_description -- 7 Columns
    FROM ndcounty_miacbdqndocaamezccoozbzaecmicdca.daily_work_equipment_logs AS dwel
    INNER JOIN ndcounty__genesiskzefdfcecobzemzkokoicqqcmnmbiifb.work_entries AS dwet ON dwel.daily_work_log_id = dwet.original_id
    WHERE dwet.property_context = 1; -- Isn't this handy.

    -- This part will update all equipment unit properties.
    UPDATE ndcounty__genesiskzefdfcecobzemzkokoicqqcmnmbiifb.work_entries AS wle
    INNER JOIN ndcounty_miacbdqndocaamezccoozbzaecmicdca.daily_work_report_logs AS dwl on wle.original_id = dwl.work_log_id
    INNER JOIN ndcounty__genesiskzefdfcecobzemzkokoicqqcmnmbiifb.properties AS prop ON dwl.equipment_id = prop.original_id
    SET wle.property_id = prop.id
    WHERE prop.original_type = 3 AND wle.property_context = 1;


  -- Ninth Pass:   The ninth pass will remove ALL supporting records that contribute no meaningful data to the records database.
  --               The main goal of this pass is to fee up as much space as possible while sill providing the same meaningful data.
  --               This pass will also remove ndcounty__genesiskzefdfcecobzemzkokoicqqcmnmbiifb.work_entries records that provide NO meaningful information.
  --               NO ACTUAL QUERIES SHOULD BE REQUIRED HERE, THIS SHOULD HAVE ALREADY BEEN ACCOMPLISHED BY THE PREVIOUS SCRIPTS.



  -- Tenth Pass:   The tenth pass will build up all aggregate fields, such as employee_cost, fuels_cost etc in the ndcounty__genesiskzefdfcecobzemzkokoicqqcmnmbiifb.work_entries table.

    -- Calculate the equipment unit property cost.
    UPDATE ndcounty__genesiskzefdfcecobzemzkokoicqqcmnmbiifb.work_entries AS wle
    INNER JOIN ndcounty__genesiskzefdfcecobzemzkokoicqqcmnmbiifb.equipment_work_record_repairs AS wleq ON wle.id = wleq.work_record_id
    SET wle.total_property_cost = (wleq.shop_costs + wleq.purchased_costs + wleq.commercial_costs + wleq.commercial_labor_costs);

    -- Calculate total misc billing cost.
    UPDATE ndcounty__genesiskzefdfcecobzemzkokoicqqcmnmbiifb.work_entries AS wle
    SET wle.total_misc_billing_cost = (wle.misc_billing_rate * wle.misc_billing_quantity);

    -- Calculate employee costs
    UPDATE ndcounty__genesiskzefdfcecobzemzkokoicqqcmnmbiifb.work_entries AS wle
    SET wle.total_employee_regular_cost = ((wle.employee_hours_regular_worked * wle.employee_historic_wage_regular) + 
                                           (wle.employee_hours_regular_worked * wle.employee_historic_fringe_regular)),

        wle.total_employee_overtime_cost = ((wle.employee_hours_overtime_worked * wle.employee_historic_wage_overtime) +
                                            (wle.employee_hours_overtime_worked * wle.employee_historic_fringe_overtime)),

        wle.total_employee_combined_cost = (wle.total_employee_regular_cost + wle.total_employee_overtime_cost);

    -- Consumables costs/quantities.
        UPDATE ndcounty__genesiskzefdfcecobzemzkokoicqqcmnmbiifb.work_entries AS wle
          SET wle.total_materials_quantity = (SELECT coalesce(SUM(wcc.total_quantity), 0) FROM ndcounty__genesiskzefdfcecobzemzkokoicqqcmnmbiifb.work_consumables AS wcc WHERE wcc.work_entry_id = wle.id AND wcc.consumable_type = 4),
              wle.total_materials_cost     = (SELECT coalesce(SUM(wcc.total_cost), 0) FROM ndcounty__genesiskzefdfcecobzemzkokoicqqcmnmbiifb.work_consumables AS wcc WHERE wcc.work_entry_id = wle.id AND wcc.consumable_type = 4),
              wle.total_fuels_quantity     = (SELECT coalesce(SUM(wcc.total_quantity), 0) FROM ndcounty__genesiskzefdfcecobzemzkokoicqqcmnmbiifb.work_consumables AS wcc WHERE wcc.work_entry_id = wle.id AND wcc.consumable_type = 3),
              wle.total_fuels_cost         = (SELECT coalesce(SUM(wcc.total_cost), 0) FROM ndcounty__genesiskzefdfcecobzemzkokoicqqcmnmbiifb.work_consumables AS wcc WHERE wcc.work_entry_id = wle.id AND wcc.consumable_type = 3),

              wle.total_equipment_units_quantity = (SELECT coalesce(SUM(wcc.total_quantity), 0) FROM ndcounty__genesiskzefdfcecobzemzkokoicqqcmnmbiifb.work_consumables AS wcc WHERE wcc.work_entry_id = wle.id AND wcc.consumable_type = 2),
              wle.total_equipment_units_cost     = (SELECT coalesce(SUM(wcc.total_cost), 0) FROM ndcounty__genesiskzefdfcecobzemzkokoicqqcmnmbiifb.work_consumables AS wcc WHERE wcc.work_entry_id = wle.id AND wcc.consumable_type = 2),
              wle.total_equipment_and_fuels_cost = (wle.total_fuels_cost + wle.total_equipment_units_cost);

  -- 11th  Pass:   The 11th pass will be responsible for finding all missing FUELS and creating a record for them labeled "MISSING FUEL #".
  -- 12th  Pass:   The 12th pass will be responsible for finding all missing MATERIALS and creating a record for them labeled "MISSING MATERIAL #".
  -- 13th  Pass:   The 13th pass will be responsible for finding all missing EQUIPMENT UNITS and creating a record for them labeled "MISSING EQUIPMENT UNIT #".
  -- 14th  Pass:   The 14th pass will be responsible for finding all missing PROPERTIES and creating a record for them labeled "MISSING PROPERTY #".
  --               The created missing properties will all have the "NA" default property type.
  -- IGNORE PASSES HERE. ^^^^^^^^^^

  -- 15th Step: Need to add the default values for the finance system. Yo.
  INSERT INTO ndcounty__genesiskzefdfcecobzemzkokoicqqcmnmbiifb.fs_accounts(id, code, name, description, balance, category_id, is_cash_account, is_operation_account) VALUES(
    0, 'CASH BOOK', 'CASH BOOK', 'The default system cash book account.', 0, 0, 1, 0
  );

  INSERT INTO ndcounty__genesiskzefdfcecobzemzkokoicqqcmnmbiifb.fs_accounts_categories(id, code, name, description, color) VALUES(
    0, 'CASH ACCOUNTS', 'CASH ACCOUNTS', 'These are cash accounts', 'transparent'
  );

  INSERT INTO ndcounty__genesiskzefdfcecobzemzkokoicqqcmnmbiifb.fs_tax_rates(id, tax_name, tax_rate, affects_all, affects_employees, affects_equipments, affects_fuels, affects_materials) VALUES(
    0, 'NONE', 0, 1, 1, 1, 1, 1
  );

  -- 16th Step: Need to build up the migrations table for compatibility.
  INSERT INTO ndcounty__genesiskzefdfcecobzemzkokoicqqcmnmbiifb.`migrations` VALUES ('2014_03_11_150051_create_employees_table',1),('2014_03_11_152212_create_phone_numbers_table',1),('2014_03_11_152324_create_addresses_table',1),('2014_03_18_030016_create_county_settings',1),('2014_03_18_050547_create_activities_table',1),('2014_03_31_021001_create_suppliers_table',1),('2014_03_31_140543_create_supplier_notes_table',1),('2014_04_01_043607_create_accounts_table',1),('2014_04_02_050422_create_account_categories_table',1),('2014_04_03_035033_create_account_transactions_table',1),('2014_04_05_140224_create_invoices_table',1),('2014_04_08_225901_create_property_types_table',1),('2014_04_08_230059_create_properties_table',1),('2014_04_08_230621_create_gps_entries',1),('2014_04_08_230957_create_equipment_units',1),('2014_04_08_231400_create_projects_table',1),('2014_04_08_231438_create_departments_table',1),('2014_04_08_231448_create_districts_table',1),('2014_05_07_201318_create_properties_properties_pivot',1),('2014_06_02_011806_create_work_entries_table',1),('2014_06_02_012939_create_work_consumables_table',1),('2014_06_26_002539_create_counties_meta_table',1),('2014_07_01_211335_create_equipment_usage_table',1),('2014_07_07_172428_create_equipment_odometer_readings_table',1),('2014_07_11_005557_create_invoices_work_records',1),('2014_07_11_010145_create_tax_rates',1),('2014_07_11_010525_create_invoices_tax_rates',1),('2014_07_24_174045_create_property_adapters_table',1),('2014_07_29_022805_create_property_road_surface_types_table',1),('2014_08_04_175954_create_property_road_properties_table',1),('2014_08_14_140856_create_road_work_record_logs',1),('2014_08_14_141006_create_equipment_work_record_repairs',1),('2014_10_20_151817_add_invoice_total_to_work_entries_table',1),('2014_10_26_180155_add_employee_tax_identifier_to_work_logs_tab',1),('2014_12_15_165117_create_consumables_table',1),('2014_12_19_174659_create_inventory_adjustments_table',1),('2015_02_24_153837_create_property_categories_table',1),('2015_03_02_222457_create_system_information_table',1),('2015_05_16_133543_add_performance_indexes',1),('2015_05_20_165405_add_organization_property_to_work_entries',1);

  -- At this point, the migration should be complete. The ndcounty_miacbdqndocaamezccoozbzaecmicdca.daily_work_report_corrupted_archives can be largely ignored because of passes 11-14.

-- REMOVE MIGRATION SPECIFIC ALTER STATEMENTS
ALTER TABLE	ndcounty__genesiskzefdfcecobzemzkokoicqqcmnmbiifb.consumables  DROP COLUMN origin_type;
ALTER TABLE ndcounty__genesiskzefdfcecobzemzkokoicqqcmnmbiifb.consumables  DROP COLUMN origin_id;
ALTER TABLE ndcounty__genesiskzefdfcecobzemzkokoicqqcmnmbiifb.consumables  DROP COLUMN origin_track;
ALTER TABLE ndcounty__genesiskzefdfcecobzemzkokoicqqcmnmbiifb.properties   DROP COLUMN original_type;
ALTER TABLE ndcounty__genesiskzefdfcecobzemzkokoicqqcmnmbiifb.properties   DROP COLUMN original_id;
ALTER TABLE ndcounty__genesiskzefdfcecobzemzkokoicqqcmnmbiifb.work_entries DROP COLUMN original_id;
ALTER TABLE  ndcounty__genesiskzefdfcecobzemzkokoicqqcmnmbiifb.work_entries DROP COLUMN property_placed;
ALTER TABLE ndcounty__genesiskzefdfcecobzemzkokoicqqcmnmbiifb.work_entries DROP COLUMN property_batched;

-- END REMOVE MIGRATION SPECIFIC ALTER STATEMENTS

SET AUTOCOMMIT = 1; SET FOREIGN_KEY_CHECKS = 1; SET UNIQUE_CHECKS = 1; SET SQL_SAFE_UPDATES = 1;

-- NDC Scrubber
-- Removes all useless records from an NDC database.
-- Run at your own risk. No warranties or guarantees of success.

-- ndcounty__genesiskzefdfcecobzemzkokoicqqcmnmbiifb is used as a placeholder for target database schema.
-- ndcounty_miacbdqndocaamezccoozbzaecmicdca is used as a placeholder for the origin database schema.

-- Remove equipment records from consumables WHERE the equipment is actually the property..

SET SQL_SAFE_UPDATES = 0;

-- Remove unused equipment consumable records.
DELETE wcc.* FROM ndcounty__genesiskzefdfcecobzemzkokoicqqcmnmbiifb.work_consumables AS wcc
INNER  JOIN ndcounty__genesiskzefdfcecobzemzkokoicqqcmnmbiifb.work_entries     AS wle ON wcc.work_entry_id = wle.id
WHERE  wcc.consumable_type = 2
AND    wcc.total_cost      = 0
AND    wcc.total_quantity  = 0
AND    wle.total_equipment_AND_fuels_cost = 0;


-- Remove all unused fuels AND materials. The criteria is that any material or fuel
-- that has no quantity AND no total cost is removed.
DELETE wcc.* FROM ndcounty__genesiskzefdfcecobzemzkokoicqqcmnmbiifb.work_consumables AS wcc
WHERE wcc.total_cost = 0
AND   wcc.total_quantity = 0
AND   wcc.consumable_type <> 2; -- This prevents equipment records from being removed.

-- Removes all odometer readings that are completely usless.
DELETE eor.* FROM ndcounty__genesiskzefdfcecobzemzkokoicqqcmnmbiifb.equipment_odometer_readings AS eor
WHERE  eor.odometer_reading = 0
OR     CHAR_LENGTH(eor.odometer_reading) = 0;

-- Removes all completely useless work records.
DELETE FROM ndcounty__genesiskzefdfcecobzemzkokoicqqcmnmbiifb.work_entries
  WHERE employee_hours_regular_worked = 0
  AND employee_hours_overtime_worked = 0
  AND misc_billing_quantity = 0
  AND total_materials_quantity = 0
  AND total_fuels_quantity = 0
  AND total_materials_cost = 0
  AND total_equipment_units_cost = 0
  AND total_fuels_cost = 0
  AND total_misc_billing_cost = 0
  AND total_property_cost = 0
  AND total_employee_regular_cost = 0
  AND total_employee_overtime_cost = 0;

-- Removes orphaned consumable records.
DELETE FROM ndcounty__genesiskzefdfcecobzemzkokoicqqcmnmbiifb.work_consumables WHERE work_entry_id NOT IN (SELECT id FROM ndcounty__genesiskzefdfcecobzemzkokoicqqcmnmbiifb.work_entries);
DELETE FROM ndcounty__genesiskzefdfcecobzemzkokoicqqcmnmbiifb.equipment_work_record_repairs WHERE work_record_id NOT IN (SELECT id FROM ndcounty__genesiskzefdfcecobzemzkokoicqqcmnmbiifb.work_entries);
DELETE FROM ndcounty__genesiskzefdfcecobzemzkokoicqqcmnmbiifb.road_work_record_logs WHERE work_record_id NOT IN (SELECT id FROM ndcounty__genesiskzefdfcecobzemzkokoicqqcmnmbiifb.work_entries);

SET SQL_SAFE_UPDATES = 1;