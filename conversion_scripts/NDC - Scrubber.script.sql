-- NDC Scrubber
-- Removes all useless records from an NDC database.
-- Run at your own risk. No warranties or guarantees of success.

-- <T> is used as a placeholder for target database schema.
-- <O> is used as a placeholder for the origin database schema.

-- Remove equipment records from consumables WHERE the equipment is actually the property..

SET SQL_SAFE_UPDATES = 0;

-- Remove unused equipment consumable records.
DELETE wcc.* FROM <T>.work_consumables AS wcc
INNER  JOIN <T>.work_entries     AS wle ON wcc.work_entry_id = wle.id
WHERE  wcc.consumable_type = 2
AND    wcc.total_cost      = 0
AND    wcc.total_quantity  = 0
AND    wle.total_equipment_AND_fuels_cost = 0;


-- Remove all unused fuels AND materials. The criteria is that any material or fuel
-- that has no quantity AND no total cost is removed.
DELETE wcc.* FROM <T>.work_consumables AS wcc
WHERE wcc.total_cost = 0
AND   wcc.total_quantity = 0
AND   wcc.consumable_type <> 2; -- This prevents equipment records from being removed.

-- Removes all odometer readings that are completely usless.
DELETE eor.* FROM <T>.equipment_odometer_readings AS eor
WHERE  eor.odometer_reading = 0
OR     CHAR_LENGTH(eor.odometer_reading) = 0;

-- Removes all completely useless work records.
DELETE FROM <T>.work_entries
	WHERE employee_hours_regular_worked = 0
	AND employee_hours_overtime_worked = 0
	AND misc_billing_quantity = 0
	AND total_materials_quantity = 0
	AND total_fuels_quantity = 0
	AND total_materials_cost = 0
	AND total_equipment_units_cost = 0
	AND total_fuels_cost = 0
	AND total_misc_billing_cost = 0
	AND total_property_cost = 0
	AND total_employee_regular_cost = 0
	AND total_employee_overtime_cost = 0;

-- Removes orphaned consumable records.
DELETE FROM <T>.work_consumables WHERE work_entry_id NOT IN (SELECT id FROM <T>.work_entries);
DELETE FROM <T>.equipment_work_record_repairs WHERE work_record_id NOT IN (SELECT id FROM <T>.work_entries);
DELETE FROM <T>.road_work_record_logs WHERE work_record_id NOT IN (SELECT id FROM <T>.work_entries);

SET SQL_SAFE_UPDATES = 1;