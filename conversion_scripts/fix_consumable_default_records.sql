-- Fixes the default consumable records.
UPDATE consumables
SET consumable_context = 2 WHERE id = 0;
UPDATE consumables
SET consumable_context = 1 WHERE id = 1;