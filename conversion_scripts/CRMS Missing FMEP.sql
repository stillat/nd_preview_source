-- FMEP
-- Create missing fuels, materials, roads and equipment units in the CRMS database system.
-- This script is typically ran before the "CRMS to NDC2.4 script"

-- <T> is used as a placeholder for target database schema.
-- <O> is used as a placeholder for the origin database schema.
USE <O>;

INSERT INTO <O>.fuels (
	id, code, name, description, unit_id, cost
)
SELECT
dwf.fuel_id, CONCAT('F', dwf.fuel_id), CONCAT('Missing Fuel #', dwf.fuel_id), CONCAT(dwf.fuel_id, ' - This fuel was missing when imported an automatically created.'), 9, 0
FROM <O>.daily_work_fuels AS dwf WHERE dwf.fuel_id NOT IN (SELECT id FROM <O>.fuels);

INSERT INTO <O>.materials(
	id, code, name, description, unit_id, cost_per_unit
)
SELECT
dwm.material_id, CONCAT('M', dwm.material_id), CONCAT('Missing Material #', dwm.material_id), CONCAT(dwm.material_id, ' - This material was missing when imported an automatically created.'), 9, 0
FROM <O>.daily_work_materials AS dwm WHERE dwm.material_id NOT IN (SELECT id FROM <O>.materials);

-- EQUIPMENT UNITS

-- ROADS